<?php
$footer_class='footer';
if($filename == MEB_FRONT) {

} else {
    $footer_class='footer footer-1';
}

$msg = $_SESSION["msg"];
$_SESSION["msg"]='';
?>

<div class="container <?php echo $footer_class; ?>">
    <div class="row">
        <div class="copy-right">© Powered by <span>Intelli- Touch Apps</span></div>
        <div class="bottom-link">
            <ul>
                <li class="bg-line-menu"><a href="<?php echo MEB_INDEX_PARAMETER.MEB_OPPORTUNITIES;?>">Opportunities</a></li>
                <li class="menu-last"><a href="<?php echo MEB_INDEX_PARAMETER.MEB_INVESTOR_NEWS;?>">Investor News</a></li>
                <li class="menu-last"><a href="<?php echo MEB_INDEX_PARAMETER.MEB_ABOUT_US;?>">​About Us</a></li>
                <li class="menu-last"><a href="<?php echo MEB_INDEX_PARAMETER.MEB_CONTACT_US;?>">Contact Us</a></li>
                <li class="menu-last"><a href="<?php echo MEB_INDEX_PARAMETER.MEB_TERMS;?>">Terms of Use</a></li>
                <li class="menu-last"><a href="<?php echo MEB_INDEX_PARAMETER.MEB_PRIVACY_POLICY;?>">Privacy Policy</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="global_loader" style="display: none;">
    <div class="pre_loader_main" >
        <img src="images/spinner.gif">
    </div>
</div>
<script type="text/javascript">

    $(document).bind("ajaxSend", function() {
        if(!$('#infscr-loading').is(':visible'))
            $(".global_loader").show();

    }).bind("ajaxComplete", function(respo){
        if(!$('#infscr-loading').is(':visible'))
            $(".global_loader").hide();
    });

    $("#directoryName").change(function() {
        var loginDirId = this.value;
        //alert(loginDirId);
        changesessionDirectory(loginDirId);
    });

    $(document).ready(function() {
        //$('input[type="checkbox"]').prettyCheckable();
        $('.tooltip').tooltipster();
        $('.fancybox').fancybox({
            'width':500,
            'height':500
        });

        $(".serviceTxt").next().addClass("close");

        $(".serviceTxt").on("click",function(){
            if($(this).next().hasClass("close"))
            {
                $(this).next().removeClass("close");
                $(this).next().addClass("open");
            }
            else
            {
                $(this).next().removeClass("open");
                $(this).next().addClass("close");
            }
        });

        msg='';
        msg='<?php echo $msg; ?>';
        if(msg!='') {
            alertify.success(msg);
            //var notification = alertify.notify(error_re, 'success', 5, function(){  console.log('dismissed'); });
        }
    });
</script>