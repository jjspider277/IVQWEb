<?php
	#################################################################
	$SECTION_FIELD_PREFIX='cop_';
	$SECTION_AUTO_ID=$_REQUEST[$SECTION_FIELD_PREFIX.'id'];
	$SECTION_WHERE=$SECTION_FIELD_PREFIX."id=".$SECTION_AUTO_ID."";
	$SECTION_VIEW_PAGE=MEB_VIEW_COUPONS;
	$SECTION_MANAGE_PAGE=MEB_MANAGE_COUPONS;
	$SECTION_TABLE= TBL_MEMBER_COUPONS;
	$SECTION_NAME='Coupons';
	#################################################################

	if(isset($_POST)){	
		$data = $_POST;	
	}
	if ($data['cop_submit'] == 'Save' || $data['cop_addNew'] == 'Add New') {
		$type = getMemberType();
        if($type!="Business") {
            $add_values[$SECTION_FIELD_PREFIX.'meb_id'] 		= trim($data["cop_meb_id"]);
        } else {
            $add_values[$SECTION_FIELD_PREFIX.'sub_id'] 		= trim(getFreeMemberSessionId());
        }
		$cop_logo_file_name= $_FILES['cop_logo_file']['name'];

  		$add_values[$SECTION_FIELD_PREFIX.'dir_id'] 		= trim($dirId);
		$add_values[$SECTION_FIELD_PREFIX.'title'] 			= trim($data["cop_title"]);
		$add_values[$SECTION_FIELD_PREFIX.'percentage'] 	= trim($data["cop_percentage"]);
		$add_values[$SECTION_FIELD_PREFIX.'coupon_code'] 	= trim($data["cop_coupon_code"]);
		$add_values[$SECTION_FIELD_PREFIX.'description'] 	= trim($data["cop_description"]);
		/*$add_values[$SECTION_FIELD_PREFIX.'barcode'] 		= trim($data["cop_barcode"]);*/
		$add_values[$SECTION_FIELD_PREFIX.'valid_from'] 		= date('Y-m-d', strtotime($data["cop_valid_from"]));
		$add_values[$SECTION_FIELD_PREFIX.'valid_thru'] 		= date('Y-m-d', strtotime($data["cop_valid_thru"]));
		$add_values[$SECTION_FIELD_PREFIX.'company_title'] 	= trim($data["cop_company_title"]);
		$add_values['buc_id'] 	= trim($data["buc_id"]);
		$status = "Active";

		include(ROOT_DIR.'include/Classes/class.barcode.php');

		if($action != 'Update'){
			/*$add_values[$SECTION_FIELD_PREFIX.'barcode'] 		= $barcode;*/
			$add_values[$SECTION_FIELD_PREFIX.'status'] 		= $status;									
		    $add_values[$SECTION_FIELD_PREFIX . 'created_id'] 	= $mebId;
    	    $add_values[$SECTION_FIELD_PREFIX . 'created_date'] = date("Y-m-d H:i:s");
    	    $insId = $db->insertData($SECTION_TABLE, $add_values);
    	    if(is_dir(UPLOAD_DIR_COUPONS_FILE."/".$insId) == false)
			{ 		
				mkdir(UPLOAD_DIR_COUPONS_FILE."/".$insId);
			}
			
    	    $bc = new Barcode39($data["cop_coupon_code"]);
			$barcode = $bc->draw(UPLOAD_DIR_COUPONS_FILE.$insId."/");
			$updatevalue[$SECTION_FIELD_PREFIX.'barcode'] 		= $barcode.".gif";
			$db->updateData($SECTION_TABLE, $updatevalue, "cop_id=".$insId);

			$Cop_Id=$insId;
    	    if($cop_logo_file_name!="") {
				if(is_dir(UPLOAD_DIR_COUPONS_FILE."/".$Cop_Id) == false) {
					mkdir(UPLOAD_DIR_COUPONS_FILE."/".$Cop_Id);
				}
				$bus_file = uploadIVAWithVideo(UPLOAD_DIR_COUPONS_FILE.'/'.$Cop_Id."/",$_FILES['cop_logo_file']);
				if($bus_file=="type_err") {
					$_SESSION["err_file"]="Please select only image OR video file";
					$db->delete("delete from ".$SECTION_TABLE." where cop_id=".$Cop_Id);
					$_SESSION['add_detail_error'][]=$_POST;
					$URL = getMemberURL($SECTION_MANAGE_PAGE,$action);
					redirect($URL);
					exit; 
				} else if($bus_file=="size_err") {
					$_SESSION["err_file"]="Please upload file less than 32MB file size!";
					$db->delete("delete from ".$SECTION_TABLE." where cop_id=".$Cop_Id);
					$_SESSION['add_detail_error'][]=$_POST;
					$URL = getMemberURL($SECTION_MANAGE_PAGE,$action);
					redirect($URL);
					exit;
				} else {
					$cop_logo_file = $bus_file;
					$updateData[$SECTION_FIELD_PREFIX.'logo_file'] 	= $cop_logo_file;
					$SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$Cop_Id;
					$db->updateData($SECTION_TABLE, $updateData, $SECTION_WHERE);
				}
			}
    	}else{
    	   	$add_values[$SECTION_FIELD_PREFIX . 'updated_id'] 			= $mebId;
        	$add_values[$SECTION_FIELD_PREFIX . 'updated_date'] 		= date("Y-m-d H:i:s");
        	if($data["edit_cop_code"]!=$data["cop_coupon_code"]) {
	        	unlink(UPLOAD_DIR_COUPONS_FILE.$SECTION_AUTO_ID."/".$data["edit_cop_barcode"]);
				$bc = new Barcode39($data["cop_coupon_code"]);
				$barcode = $bc->draw(UPLOAD_DIR_COUPONS_FILE.$SECTION_AUTO_ID."/");
				$add_values[$SECTION_FIELD_PREFIX.'barcode'] 		= $barcode.".gif";
			}
        	$GPDetail_result = $db->updateData($SECTION_TABLE, $add_values, $SECTION_WHERE);

        	$Cop_Id=$SECTION_AUTO_ID;
        	if($_POST['edit_cop_logo_file']=='') {
				$updateData[$SECTION_FIELD_PREFIX.'logo_file'] 	= '';
				$SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$Cop_Id;
				$db->updateData($SECTION_TABLE, $updateData, $SECTION_WHERE);
			}

    	    if($cop_logo_file_name!="")
			{
				if(is_dir(UPLOAD_DIR_COUPONS_FILE."/".$Cop_Id) == false)
				{ 			
					mkdir(UPLOAD_DIR_COUPONS_FILE."/".$Cop_Id);
				}
				$bus_file = uploadIVAWithVideo(UPLOAD_DIR_COUPONS_FILE.'/'.$Cop_Id."/",$_FILES['cop_logo_file']);
				if($bus_file=="type_err")
				{
					$_SESSION["err_file"]="Please select only image OR video file";
					$db->delete("delete from ".$SECTION_TABLE." where cop_id=".$Cop_Id);
					$_SESSION['add_detail_error'][]=$_POST;
					$URL = getMemberURL($SECTION_MANAGE_PAGE,$action);
					redirect($URL);
					exit; 
				}
				else if($bus_file=="size_err")
				{
					$_SESSION["err_file"]="Please upload file less than 32MB file size!";
					$db->delete("delete from ".$SECTION_TABLE." where cop_id=".$Cop_Id);
					$_SESSION['add_detail_error'][]=$_POST;
					$URL = getMemberURL($SECTION_MANAGE_PAGE,$action);
					redirect($URL);
					exit; 
				}
				else
				{
					$cop_logo_file = $bus_file;
					$updateData[$SECTION_FIELD_PREFIX.'logo_file'] 	= $cop_logo_file;
					$SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$Cop_Id;
					$db->updateData($SECTION_TABLE, $updateData, $SECTION_WHERE);
				}
			}
    	}
    	if($data['cop_addNew'] == 'Add New') {
			$URL = getMemberURL($SECTION_MANAGE_PAGE,$action); 
			redirect($URL);
			exit;
		} else {
			$URL = getMemberURL($SECTION_VIEW_PAGE);
            if($_POST['method']=='Update')
                $_SESSION["msg"] = 'Successfully updated coupon.';
            else
                $_SESSION["msg"] = 'Successfully created coupon.';
			redirect($URL);
			exit;
		}
	}
	if($action=='Edit')
	{
		$list_query = getSelectList($SECTION_TABLE,$SECTION_FIELD_PREFIX);
		$list_query .= "AND ".$SECTION_FIELD_PREFIX."id = '".$SECTION_AUTO_ID."'";
		$result_query = $db->select($list_query);
		$cop_title		=	$result_query[0][$SECTION_FIELD_PREFIX."title"];
		$cop_logo_file	=	$result_query[0][$SECTION_FIELD_PREFIX."logo_file"];
		$cop_percentage	=	$result_query[0][$SECTION_FIELD_PREFIX."percentage"];
		$cop_coupon_code=	$result_query[0][$SECTION_FIELD_PREFIX."coupon_code"];
		$cop_description=	$result_query[0][$SECTION_FIELD_PREFIX."description"];
		$cop_barcode	=	$result_query[0][$SECTION_FIELD_PREFIX."barcode"];
		$buc_id	=	$result_query[0]["buc_id"];
		$cop_company_title	=	$result_query[0][$SECTION_FIELD_PREFIX."company_title"];
		$cop_valid_from	=	date('m/d/Y',strtotime($result_query[0][$SECTION_FIELD_PREFIX."valid_from"]));
		$cop_valid_thru	=	date('m/d/Y',strtotime($result_query[0][$SECTION_FIELD_PREFIX."valid_thru"]));
		$action = "Update";
		$action_url = getMemberURL($SECTION_MANAGE_PAGE,$action,$SECTION_WHERE);
	}	
	if($action=='')
	{
		$action = "Add";
		$action_url = getMemberURL($SECTION_MANAGE_PAGE);
		
		if($_SESSION['add_detail_error'][0]!="")
		{
			foreach($_SESSION['add_detail_error'][0] as $key => $value)
			{
				$result_query[0][$key]=stripslashes($value);
			}
			$cop_title		=	$result_query[0][$SECTION_FIELD_PREFIX."title"];
			$cop_logo_file	=	$result_query[0]["logo_file"];
			$cop_percentage	=	$result_query[0][$SECTION_FIELD_PREFIX."percentage"];
			$cop_coupon_code=	$result_query[0][$SECTION_FIELD_PREFIX."coupons_code"];
			$cop_description=	$result_query[0][$SECTION_FIELD_PREFIX."description"];
			$cop_barcode	=	$result_query[0][$SECTION_FIELD_PREFIX."barcode"];
			unset($_SESSION['add_detail_error']);
		}
	}
?>

<div class="container search-group-section">
    <div class="row">
        <div class="step-list">
            <ul>
                <li><a href="javascript:void(0);" class="top_step_1 active">Step <span>1</span></a></li>
                <li><a href="javascript:void(0);" class="top_step_2">Step <span>2</span></a></li>
            </ul>
        </div>
    </div>
</div>

<div class="container step-page-section">
    <div class="row">

        <form name="manage_coupons" enctype="multipart/form-data" method="post" action="<?php echo $action_url; ?>" id="manage_coupons">
            <input name="cop_id" id="cop_id" value="<?php echo $SECTION_AUTO_ID; ?>" type="hidden" />
            <input name="cop_meb_id" id="cop_meb_id" value="<?php echo $mebId; ?>" type="hidden" />
            <input type="hidden" name="method" id="method" value="<?php echo $action; ?>" />
            <input type="hidden" name="cop_submit" id="cop_submit" value="Save" />

            <div class="form-main next_step_main_1">
                <div class="main-row">
                    <label>Coupons Title <?php echo getRequiredIcon()?></label>
                    <input type="text" name="cop_title" id="cop_title" value="<?php echo $cop_title;?>" />
                </div>
                <div class="main-row">
                    <label>Percentage <?php echo getRequiredIcon()?></label>
                    <input type="text" name="cop_percentage" id="cop_percentage" tabindex="2" value="<?php echo $cop_percentage;?>" />
                </div>
                <div class="main-row">
                    <label>Description <?php echo getRequiredIcon()?></label>
                    <input type="text" name="cop_description" id="cop_description" tabindex="3" value="<?php echo $cop_description;?>" />
                </div>
                <div class="main-row">
                    <label>Coupon Code <?php echo getRequiredIcon()?></label>
                    <input type="text" name="cop_coupon_code" id="cop_coupon_code" tabindex="4" value="<?php echo $cop_coupon_code;?>" />
                </div>

                <div class="step-button-row">
                    <div class="step-button"><a class="next_step_1" href="javascript:void(0);">Next</a></div>
                </div>
            </div>

            <div class="form-main next_step_main_2" style="display: none;">

                <div class="main-row">
                    <label>Coupons Valid From <?php echo getRequiredIcon()?></label>
                    <input type="text" name="cop_valid_from" id="cop_valid_from" tabindex="5" value="<?php echo $cop_valid_from; ?>" readonly="readonly" />
                </div>
                <div class="main-row">
                    <label>Coupons Valid To <?php echo getRequiredIcon()?></label>
                    <input type="text" name="cop_valid_thru" id="cop_valid_thru" tabindex="6" value="<?php echo $cop_valid_thru; ?>" readonly="readonly" />
                </div>
                <div class="main-row">
                    <label>Category <?php echo getRequiredIcon()?></label>
                    <?=getBusinessCategoryList("buc_id","buc_id",$buc_id); ?>
                    <!--<input type="text" name="cop_company_title" id="cop_company_title" tabindex="6" value="<?php echo $cop_company_title; ?>" readonly="readonly" />-->
                </div>
                <div class="main-row">
                    <label>Company Name <?php echo getRequiredIcon()?></label>
                    <input type="text" name="cop_company_title" id="cop_company_title" tabindex="6" value="<?php echo $cop_company_title; ?>" />
                </div>

                <div class="main-row">
                    <label>Logo file</label>
                    <input type="file" name="cop_logo_file" id="cop_logo_file" />
                    <?php if($_SESSION["err_file"]!="") {
                            echo '<label class="error">'.$_SESSION["err_file"].'</label>';
                            //$_SESSION["err_file"]="";
                        }
                        if($action=="Update" && $cop_logo_file!="") {
                            echo "<div class='msg_filesddd'>";
                            echo "<a id='vediosPlay' href='view_file_cop.php?filename=".$cop_logo_file."&cop_id=".$SECTION_AUTO_ID."' style='margin-top:5px;' class='fancybox fancybox.ajax' title='View' data-fancybox-type='ajax' >".$cop_logo_file."</a>";
                            echo "&nbsp;&nbsp;&nbsp;<a style='margin-top:5px;' id='removeLink' onclick='javascript:removeFile(edit_cop_logo_file);'>Remove</a>";
                            echo "<input type='hidden' id='edit_cop_logo_file' name='edit_cop_logo_file' value='".$cop_logo_file."' />";
                            echo "</div>";
                        }
                    ?>
                </div>

                <?php if($action=="Update" && $cop_barcode!="" ) { ?>
                    <div class="main-row">
                        <label>Bar Code <?php // echo getRequiredIcon()?></label>
                        <img src="<?php echo UPLOAD_WWW_COUPONS_FILE.$SECTION_AUTO_ID."/".$cop_barcode; ?>" alt="Barcode" title="Barcode">
                        <input type="hidden" name="edit_cop_barcode" value="<?php echo $cop_barcode;?>" />
                        <input type="hidden" name="edit_cop_code" value="<?php echo $cop_coupon_code;?>" />
                    </div>
                <?php } ?>

                <div class="step-button-row">
                    <div class="step-button-previous"><a class="pre_step" href="javascript:void(0);">Previous</a></div>
                    <div class="step-button-1"><a href="javascript:void(0);" onclick="$('#manage_coupons').submit();">Save</a></div>
                </div>

            </div>
        </form>

    </div>
</div>

<?php
$error_re='';
if($_SESSION['err_file']!='') {
    $error_re=$_SESSION['err_file'];
    $_SESSION['err_file']='';
}
?>

<script>

$(document).ready(function() {
    error_re='';
    error_re='<?php echo $error_re; ?>';
    if(error_re!='') {
        alertify.error(error_re);
        //var notification = alertify.notify(error_re, 'success', 5, function(){  console.log('dismissed'); });
    }

    edd = '<?php echo $action; ?>';
    if(edd=='Update'){
        $(document).prop('title', '<?php echo MEB_FILE_TITLE_EDIT_COUPONS; ?>');
    }

    $(".custom-select").each(function(){
        $(this).wrap("<span class='select-wrapper'></span>");
        $(this).after("<span class='holder'></span>");
    });
    $(".custom-select").change(function(){
        var selectedOption = $(this).find(":selected").text();
        $(this).next(".holder").text(selectedOption);
    }).trigger('change');

	$('.tooltip').tooltipster();
	
	
	$( "#cop_valid_from" ).datepicker({
        dateFormat: "mm/dd/yy",
        changeMonth: true,
        changeYear: true,
        buttonImageOnly: false,
        onSelect: function(selected) {
            $('#cop_valid_thru').datepicker('option', 'minDate', new Date(selected));
        },
    });
    $( "#cop_valid_thru" ).datepicker({
        dateFormat: "mm/dd/yy",
        changeMonth: true,
        changeYear: true,
        buttonImageOnly: false,
        onSelect: function(selected) {
            $('#cop_valid_from').datepicker('option', 'maxDate', new Date(selected));
        }
    });
		
	$('#cop_valid_from').datepicker('option', 'maxDate', new Date($('#cop_valid_thru').val()));
	$('#cop_valid_thru').datepicker('option', 'minDate', new Date($('#cop_valid_from').val()));
	
	$("#manage_coupons").validate({
		rules: {
			cop_title : {
				required : true
			},
			cop_percentage : {
				required : true,
				number : true,
                max: 100
			},
			cop_description : {
				required : true
			},
			cop_coupon_code : {
				required : true
				//number : true
			},
			cop_valid_from : {
				required : true
			},
			cop_valid_thru : {
				required : true
			},
			buc_id : {
				required : true
			},
			cop_company_title : {
				required : true
			}			
			/*,
			cop_barcode : {
				required : true
				//number : true
			}*/
		},
		messages: {
			cop_title : {
				required : "Please enter coupons title"
			},
			cop_percentage : {
				required : "Please enter coupons percentage",
				number : "Please enter only numbers"
			},
			cop_description : {
				required : "Please enter coupons description"
			},
			cop_coupon_code : {
				required : "Please enter coupons code"/*,
				number : "Please enter only numbers"*/
			},
			cop_valid_from : {
				required : "Please enter coupons valid from date"
			},
			cop_valid_thru : {
				required : "Please enter coupons valid to date"
			},
			buc_id : {
				required : "Please select category"
			},
			cop_company_title : {
				required : "Please enter company title"
			}
			
			/*,
			cop_barcode : {
				required : "Please enter coupons barcode"
				//number : "Please enter only numbers"
			}*/
		}
	});

    $(document).on('click', '.next_step_1, .top_step_2', function() {
        erro='';

        //$('.next_step_1').removeClass('error');

        $('html,body').animate({scrollTop: $('.step-list').offset().top-10},'slow');
        if($('#cop_title').val()=='') {
            $('#cop_title').addClass('error');
            erro='yes';
        }
        if($('#cop_percentage').val()=='') {
            $('#cop_percentage').addClass('error');
            erro='yes';
        }

        if($('#cop_description').val()=='') {
            $('#cop_description').addClass('error');
            erro='yes';
        }

        if($('#cop_coupon_code').val()=='') {
            $('#cop_coupon_code').addClass('error');
            erro='yes';
        }

        if(erro=='yes') {
            return false;
        } else {

            $('.top_step_1').removeClass('active');
            $('.top_step_2').addClass('active');

            $('.next_step_main_1').hide();
            $('.next_step_main_2').show();
        }
    });

    $(document).on('click', '.pre_step, .top_step_1', function(){
        $('html,body').animate({scrollTop: $('.step-list').offset().top-10},'slow');

        $('.top_step_1').addClass('active');
        $('.top_step_2').removeClass('active');

        $('.next_step_main_1').show();
        $('.next_step_main_2').hide();
    });

});
function removeFile($fileName)
{
     $hiddenId = $fileName.id     
     $("#vediosPlay").remove();
     $("#removeLink").remove();
     $('#'+$hiddenId).val('');
     $('#remove_image').val('true');
     $( "#bus_submit" ).trigger( "click" );
}
</script>