<?php 

$action=$_REQUEST['mod'];

$first_name=$_COOKIE['meb_firstname'];
$last_name=$_COOKIE['meb_lastname'];
$emaill=$_COOKIE['meb_username'];

if (!empty($action))
{
	$error="";
	if(empty($error))
	{
		$sql="select * from ".TBL_MEMBER." where meb_username ='".trim(addslashes($_POST['meb_username']))."' AND meb_status='Active'";
		$data = $db->select($sql);
		if(empty($data))
		{
            unset($add_values);
            $add_values['meb_dir_id']       = '167';
            $add_values['meb_name'] 	    = trim(addslashes($_POST['meb_firstname']));
            $add_values['meb_lastname'] 	= trim(addslashes($_POST['meb_lastname']));
            $add_values['meb_username'] 	= trim(addslashes($_POST['meb_username']));
            $add_values['meb_password'] 	= md5(trim(addslashes($_POST['meb_password'])));
            //$add_values['meb_phone']        = trim(addslashes($_POST['meb_phone']));;
            $add_values['meb_status'] 		= "Active";
            $add_values['meb_created_id'] 	= 0;
            $add_values['meb_created_date'] = date('Y-m-d H:i:s');
            $add_values['meb_updated_id']   = 0;
            $add_values['meb_updated_date'] = date('Y-m-d H:i:s');

            $insSubId = $db->insertData(TBL_MEMBER, $add_values);
            if($insSubId) {
                $_SESSION['msg'] = "Your registration is successfully processed.";
            }
            $URL=MEB_INDEX_PARAMETER.MEB_LOGIN;
            redirect($URL);
            exit;
		}
		else
		{
            $first_name=trim(addslashes($_POST['meb_firstname']));
            $last_name=trim(addslashes($_POST['meb_lastname']));
            $emaill=trim(addslashes($_POST['meb_username']));

			$error = "Email address already registered.";
		}
	}
}

$app_apk = '';
$app_ios = '';

$dir_fields = array("*");
$dir_where  = "";
$dirRes 	= $db->selectData(TBL_APP_SETTING,$dir_fields,$dir_where,$extra="",2);

if(count($dirRes)>0) {
	foreach($dirRes as $dirdata) {

		if($dirdata['meta'] == 'apk') {
			$app_apk = $dirdata['value'];
		}

		if($dirdata['meta'] == 'ipa') {
			$app_ios = $dirdata['value'];
		}
	}
}
?>

<div class="content-section-01 register-height">
	<div class="row">

		<div class="left-image-section">
			<img src="<?php echo ROOT_WWW; ?>/members/images/mobille-img.png" alt="">
		</div>

		<div class="right-form-section">
			<h1>User registration</h1>

			<form name='frmMember' method='post' action="<?php echo MEB_INDEX_PARAMETER.MEB_REGISTER."&mod=add";?>" id='frmMember'>

				<div class="right-form-section-form">
					<label class="error"><?php echo $error; ?></label>

                    <div class="error1">
                        <div class="inputText firstname"><input type="text" name="meb_firstname" id="meb_firstname" placeholder="Enter first name" value="<?php echo $first_name; ?>" /></div>
                        <span class="user-icon"></span>
                    </div>

                    <div class="error1">
                        <div class="inputText lastname"><input type="text" name="meb_lastname" id="meb_lastname" placeholder="Enter last name" value="<?php echo $last_name; ?>" /></div>
                        <span class="user-icon"></span>
                    </div>

					<div class="error1">
						<div class="inputText username"><input type="text" name="meb_username" id="meb_username" placeholder="Enter e-mail address" value="<?php echo $emaill; ?>" /></div>
						<span class="email-icon"></span>
					</div>

                    <div class="error1">
                        <div class="inputText username2"><input type="text" name="meb_username2" id="meb_username2" placeholder="Enter confirm e-mail address" /></div>
                        <span class="email-icon"></span>
                    </div>

					<div class="error1">
						<div class="inputText password"><input type="password" name="meb_password" id="meb_password" placeholder="Enter password" /></div>
						<span class="closed-icon"></span>
					</div>

                    <div class="error1">
                        <div class="inputText password2"><input type="password" name="meb_password2" id="meb_password2" placeholder="Enter confirm password" /></div>
                        <span class="closed-icon"></span>
                    </div>

                    <?php /* ?>
                    <div class="error1">
                        <div class="inputText phone"><input type="text" name="meb_phone" id="meb_phone" placeholder="Enter mobile number" /></div>
                        <span class="phone-icon"></span>
                    </div>

                    <div class="error1">
                        <div class="inputText phone2"><input type="text" name="meb_phone2" id="meb_phone2" placeholder="Enter confirm mobile number" /></div>
                        <span class="phone-icon"></span>
                    </div>
                    <?php */ ?>

				</div>

				<div class="button-section">
					<div style="display: none" class="remember"><input name="remember" id="remember" type="checkbox" value="1" data-label="Remember Me"  data-labelPosition="right"></div>
					<div style="display: none" class="forgot"><a href="<?php echo MEB_FOLDER_WWW.MEB_FORGOT;?>">Reset Password?</a></div>

					<div class="create-accoumt"><a href="<?php echo MEB_INDEX_PARAMETER.MEB_LOGIN?>">Login</a></div>
					<div class="login"><a href="javascript:void(0);" onclick="$('#frmMember').submit();">Create account<span class="login-arrow"></span></a></div>
				</div>

				<div class="google-play-section">
					<div class="apple-button"><a target="_blank" href="<?php echo $app_ios; ?>"><img src="images/app-store-1.jpg"></a></div>
					<div class="google-play-button"><a target="_blank" href="<?php echo $app_apk; ?>"><img src="images/google-play-1.jpg"></a></div>
				</div>

			</form>

		</div>
	</div>
</div>
<?php
$error_2=$error;
$error='';
?>
<script>
$(document).ready(function() {

    error_re_gr='';
    error_re_gr='<?php echo $error_2; ?>';
    if(error_re_gr!='') {
        alertify.error(error_re_gr);
    }

	$('input[type="checkbox"]').prettyCheckable();	
	$("#frmMember").validate({
		rules: {
            meb_firstname:"required",
            meb_lastname:"required",
            meb_username : {
                required : true,
                email : true
            },
            meb_username2 : {
                required : true,
                email : true,
                equalTo: "#meb_username"
            },
            meb_password : {
                required : true,
            },
            meb_password2 : {
                required : true,
                equalTo: "#meb_password"
            }
            /*,
            meb_phone : {
                required : true,
            },
            meb_phone2 : {
                required : true,
                equalTo: "#meb_phone"
            }*/
		}
	});
});
</script>
