<?php
	#################################################################
	$SECTION_FIELD_PREFIX='sub_';
	$SECTION_AUTO_ID=$_REQUEST[$SECTION_FIELD_PREFIX.'id'];
	$SECTION_WHERE=$SECTION_FIELD_PREFIX."id=".$SECTION_AUTO_ID."";
	$SECTION_VIEW_PAGE=MEB_VIEW_SUBSCRIBER;
	$SECTION_MANAGE_PAGE=MEB_MANAGE_SUBSCRIBER;
	$SECTION_TABLE=TBL_MEMBER_SUBSCRIBERS;
	$SECTION_NAME='Subscribers';

	//Directory Listing...
	$dir_fields = array("dir_id","dir_name");
	$dir_where  = "dir_status = 'Active'";
	$dirRes 	= $db->selectData(TBL_DIRECTORY,$dir_fields,$dir_where,$extra="",2);

	//State Listing...
	$state_fields = array("sta_id","sta_name");
	$state_where  = "sta_status = 'Active'";
	$staRes 	= $db->selectData(TBL_STATE,$state_fields,$state_where,$extra="",2);
	#################################################################
	if(isset($_POST)){	
		$data = $_POST;	
	}
 
	if ($data['sub_submit'] == 'Save' || $data['sub_addNew'] == 'Add New')
	{  
		unset($add_values);
		$add_values[$SECTION_FIELD_PREFIX.'meb_id'] 		= trim($mebId);
		$add_values[$SECTION_FIELD_PREFIX.'dir_id'] 		= trim($data["sub_dir"]);
		
		$add_values[$SECTION_FIELD_PREFIX.'name'] 			= trim($data["sub_name"]);
		if(isset($data['sub_type']) && $data['sub_type']=="Individual")
		{
			$add_values[$SECTION_FIELD_PREFIX.'title'] 			= "";
			$add_values[$SECTION_FIELD_PREFIX.'company'] 		= "";
			$add_values[$SECTION_FIELD_PREFIX.'password']		= "";
			//$add_values[$SECTION_FIELD_PREFIX.'status'] 		= "Active";
		}
		else
		{
			$add_values[$SECTION_FIELD_PREFIX.'title'] 			= trim($data["sub_title"]);
			$add_values[$SECTION_FIELD_PREFIX.'company'] 		= trim($data["sub_company"]);
			//$add_values[$SECTION_FIELD_PREFIX.'status'] 		= "Inactive";
		}
		$add_values[$SECTION_FIELD_PREFIX.'city'] 			= trim($data["sub_city"]);
		$add_values[$SECTION_FIELD_PREFIX.'email'] 			= trim($data["sub_email"]);
		if($data["sub_password"]!="")
		{
			$add_values[$SECTION_FIELD_PREFIX.'password'] 		= md5($data["sub_password"]);
		}
		$add_values[$SECTION_FIELD_PREFIX.'phone'] 			= trim($data["sub_phone"]);
		$add_values[$SECTION_FIELD_PREFIX.'zip'] 			= trim($data["sub_zip"]);
		$add_values[$SECTION_FIELD_PREFIX.'sta_id'] 		= $data["sub_state"];
		$add_values[$SECTION_FIELD_PREFIX.'question'] 		= trim($data["sub_question"]);
		$add_values[$SECTION_FIELD_PREFIX.'answer'] 		= trim($data["sub_answer"]);
		
		if($action != 'Update'){
			$add_values[$SECTION_FIELD_PREFIX.'type'] 			= $data["subscriber_flag"];
			$add_values[$SECTION_FIELD_PREFIX.'status'] 		= "Inactive";
			$add_values[$SECTION_FIELD_PREFIX . 'created_id'] 	= $mebId;
    	    $add_values[$SECTION_FIELD_PREFIX . 'created_date'] = date("Y-m-d H:i:s");	
    	    $GPDetail_result = $db->insertData($SECTION_TABLE, $add_values);
         if($GPDetail_result > 0)
         {
          unset($add_sbm_values);
          $add_sbm_values['sbm_sub_id'] 		    = $GPDetail_result;
          $add_sbm_values['sbm_dir_id'] 		    = $data['sub_dir'];
          $add_sbm_values['sbm_type']   		    = "Subscriber";		
          $add_sbm_values['sbm_status'] 		    = "Active";
          $add_sbm_values['sbm_created_id'] 	 = 0;
          $add_sbm_values['sbm_created_date'] = date('Y-m-d H:i:s');  
          $inssbmId = $db->insertData(TBL_MEMBER_SUBSCRIBE_DIRECTORY, $add_sbm_values); 
         }
			//$_SESSION['msg']  =   "Subscribers has been inserted successfully.";  
    	}else{
    		$add_values[$SECTION_FIELD_PREFIX.'status'] 		= $data["sub_status"];
    		$add_values[$SECTION_FIELD_PREFIX.'type'] 			= $data["sub_type"];
    	   	$add_values[$SECTION_FIELD_PREFIX . 'updated_id'] 	= $mebId;
        	$add_values[$SECTION_FIELD_PREFIX . 'updated_date'] = date("Y-m-d H:i:s");
        	$GPDetail_result = $db->updateData($SECTION_TABLE, $add_values, $SECTION_WHERE);
    		//$_SESSION['msg']  =   "Subscribers has been updated successfully.";  
    	}
    	if($data['sub_addNew'] == 'Add New')
		{
			$URL = getMemberURL($SECTION_MANAGE_PAGE); 
			redirect($URL);
			exit;
		}
		else
		{
			$URL = getMemberURL($SECTION_VIEW_PAGE); 
			redirect($URL);
			exit;
		}
	}
	if($action=='Edit')
	{
		$list_query = getSelectList($SECTION_TABLE,$SECTION_FIELD_PREFIX);
		$list_query .= "AND ".$SECTION_FIELD_PREFIX."id = '".$SECTION_AUTO_ID."'";
		$result_query = $db->select($list_query);

		$sub_dir		=	$result_query[0][$SECTION_FIELD_PREFIX."dir_id"];
		$sub_type		=	$result_query[0][$SECTION_FIELD_PREFIX."type"];
		$sub_status		=	$result_query[0][$SECTION_FIELD_PREFIX."status"];
		$sub_title		=	$result_query[0][$SECTION_FIELD_PREFIX."title"];
		$sub_company	=	$result_query[0][$SECTION_FIELD_PREFIX."company"];
		$sub_name		=	$result_query[0][$SECTION_FIELD_PREFIX."name"];
		$sub_email		=	$result_query[0][$SECTION_FIELD_PREFIX."email"];
		$sub_password	=	$result_query[0][$SECTION_FIELD_PREFIX."password"];
		$sub_phone		=	$result_query[0][$SECTION_FIELD_PREFIX."phone"];
		$sub_zip		=	$result_query[0][$SECTION_FIELD_PREFIX."zip"];
		$sub_city		=	$result_query[0][$SECTION_FIELD_PREFIX."city"];
		$sub_state		=	$result_query[0][$SECTION_FIELD_PREFIX."sta_id"];
		$sub_question	=	$result_query[0][$SECTION_FIELD_PREFIX."question"];
		$sub_answer	=	$result_query[0][$SECTION_FIELD_PREFIX."answer"];

		$action = "Update";
		$action_url = getMemberURL($SECTION_MANAGE_PAGE,$action,$SECTION_WHERE);
	}	
 

	if($action=='')
	{
		$action = "Add";
		$action_url = getMemberURL($SECTION_MANAGE_PAGE,$action);
	}
	$sub_dir = ($sub_dir==0 || $sub_dir=="") ? getMemberSessionDirId() : $sub_dir;
?>
<section>
<article id="page" >
	<header>
		<ul class="tab_links ">
    		<li><div class="tab_link_active inner">
    			<span><img src="<?php echo IMG_WWW; ?>directory.png"></span><h1>Subscribers</h1>
				<div class="view">
					<a href="<?php echo getMemberURL($SECTION_VIEW_PAGE); ?>" class="tooltip" title="back">
					<img src="<?php echo IMG_WWW; ?>back-orange.png"></a>
				</div>
			</div></li>    					
    	</ul>
	</header>
	<aside>		
		<div class="tab_content_holder directory">
  
  
			<div class="tab_content_holder_inner">
				<div class="block-part">
    <form name="manage_subscribers" method="post" action="<?php echo $action_url; ?>" id="manage_subscribers" autocomplete="off">
					<div class="main-row subscriber_section" style="width:100%;">
					<?php if($action=="Update") { 
						$disabled = "disabled='disabled'";
					?>
						<label for="Individual" class="checkbox"><input type="radio" name="subscriber_flag" value="Individual" id="rad_Individual" <?php if($sub_type=="Individual") { echo 'checked="checked"'; } echo $disabled; ?> />Individual</label> 
						
						<label for="Business" class="checkbox"><input type="radio" name="subscriber_flag" value="Business"  id="rad_Directory" <?php if($sub_type=="Business") { echo 'checked="checked"'; }  echo $disabled; ?> /> Business</label>						
					<?php }else {?>
    		<label for="Individual" class="checkbox"><input type="radio" name="subscriber_flag" value="Individual" id="rad_Individual" checked="checked" onclick="changeStatus(this.value);" />Individual</label> 
						
						<label for="Business" class="checkbox"><input type="radio" name="subscriber_flag" value="Business"  id="rad_Directory" onclick="changeStatus(this.value);" /> Business</label>						
				<?php }
    ?>
    </div>
					<div class="main-row" id="editdisable">
						<label>Directory Name</label>
						<select name="sub_dir" id="sub_dir" tabindex="1" alt="Type to search directory name">
							<option value="">Select Directory</option>
							<?php						
							for($i=0;$i<count($dirRes);$i++)
							{
								if($dirRes[$i]['dir_id']==$sub_dir) { $select="selected='selected'"; } else { $select=""; }
							?>
							<option value="<?php echo $dirRes[$i]['dir_id']; ?>" <?php echo $select; ?> ><?php echo $dirRes[$i]['dir_name']; ?></option>
							<?php
							}
							?>
						</select>
					</div>
					<div class="main-row">
						<label>Subscriber Name <?php echo getRequiredIcon()?></label>
						<input type="text" name="sub_name" id="sub_name" tabindex="2" value="<?php echo $sub_name;?>" />
					</div>
					<?php if($sub_type!="Individual") { ?>
					<div class="main-row dynamic">
						<label>Company Name</label>
						<input class="disable" type="text" name="sub_company" id="sub_company" tabindex="3" value="<?php echo $sub_company;?>" />
					</div>
					<div class="main-row dynamic">
						<label>Title</label>
						<input class="disable" type="text" name="sub_title" id="sub_title" tabindex="4" value="<?php echo $sub_title;?>" />
					</div>
					<?php } ?>
					<div class="main-row">
						<label>Email <?php echo getRequiredIcon()?></label>
						<!--<input type="text" tabindex="5" name="sub_email" id="sub_email" value="<?php echo $sub_email;?>" onblur="checkUserEmail(this.value,'<?php echo $SECTION_AUTO_ID; ?>','Subscriber');" />-->
      <input type="text" tabindex="5" name="sub_email" id="sub_email" value="<?php echo $sub_email;?>" onblur="checkSub(this.value);" />
						<div id="userEmailmsg"></div>
					</div>
					<div class="main-row">
						<label>Confirm Email <?php echo getRequiredIcon()?></label>
						<input type="text" tabindex="6" name="sub_confemail" id="sub_confemail" value="<?php echo $sub_email;?>" />
					</div>
					<?php if($sub_type!="Individual") { ?>
					<div class="main-row dynamic">
						<label>Password <?php echo getRequiredIcon()?></label>
						<input class="disable" type="password" tabindex="7" name="sub_password" id="sub_password" value="" />
					</div>
					<div class="main-row dynamic">
						<label>Confirm Password <?php echo getRequiredIcon()?></label>
						<input class="disable" type="password" tabindex="8" name="sub_confpass" id="sub_confpass" value="" />
					</div>
					<?php } ?>
					<div class="main-row">
						<label>City <?php echo getRequiredIcon()?></label>
						<input type="text" name="sub_city" id="sub_city" tabindex="9" value="<?php echo $sub_city;?>" />
					</div>
					<div class="main-row">
						<label>State <?php echo getRequiredIcon()?></label>
						<select name="sub_state" id="sub_state" tabindex="10" alt="Type to search state">
							<option value="">Select State</option>
							<?php						
							for($i=0;$i<count($staRes);$i++)
							{
								if($staRes[$i]['sta_id']==$sub_state) { $select="selected='selected'"; } else { $select=""; }
							?>
								<option value="<?php echo $staRes[$i]['sta_id']; ?>" <?php echo $select; ?> ><?php echo $staRes[$i]['sta_name']; ?></option>
							<?php
							}
							?>
						</select>
					</div>
					<div class="main-row">
						<label>Zip Code <?php echo getRequiredIcon()?></label>
						<input type="text" name="sub_zip" id="sub_zip" tabindex="11" value="<?php echo $sub_zip;?>" />
					</div>
					<div class="main-row">
						<label>Phone Number <?php echo getRequiredIcon()?></label>
						<input type="text" name="sub_phone" id="sub_phone" tabindex="12" value="<?php echo $sub_phone;?>" />
					</div>
					<?php if($sub_type!="Individual") { ?>
					<div class="main-row dynamic">
						<label>Secret Question <?php echo getRequiredIcon()?></label>
						<input class="disable" type="text" name="sub_question" id="sub_question" tabindex="13" value="<?php echo $sub_question;?>" />
					</div>
					<div class="main-row dynamic">
						<label>Answer <?php echo getRequiredIcon()?></label>
						<input class="disable" type="text" name="sub_answer" id="sub_answer" tabindex="14" value="<?php echo $sub_answer;?>" />
					</div>
					<?php } ?>
					<div class="main-row">
						<label>&nbsp;</label>
						<input name="sub_id" id="sub_id" value="<?php echo $SECTION_AUTO_ID; ?>" type="hidden">
						<input type="hidden" name="method" id="method" value="<?php echo $action; ?>" />
						<?php if($action=="Update") { ?>
						<input name="sub_type" id="sub_type" value="<?php echo $sub_type; ?>" type="hidden">
						<input name="sub_status" id="sub_status" value="<?php echo $sub_status; ?>" type="hidden">
						<?php } ?>
						<input type="submit" name="sub_submit" id="sub_submit" tabindex="15" value="Save" />
						<?php if($action!="Update")
						{?>
						<input name="sub_meb_id" id="sub_meb_id" value="<?php echo $mebId; ?>" type="hidden">
						
						<input type="submit" name="sub_addNew" id="sub_addNew" tabindex="16" value="Add New" />
						<?php } ?>
					</div>
				</form>
				</div>
				<div class="clr"></div>
			</div>
		</div>
	</aside>
</article>
</section>
<script type="text/javascript" src="<?php echo AJAX_FOLDER_WWW; ?>getAjaxAdmin.js"></script>
<script type="text/javascript">

var subType='<?php echo $sub_type;?>';

if(subType == 'Individual'){
changeStatus("Individual");
}else if(subType == 'Business'){
changeStatus("Business");
}else{
changeStatus("Individual");
}


function changeStatus(status)
{
	if(status=="Individual")
	{
		$(".dynamic").hide();
		$(".disable").attr({"disabled": true});
		//$("#sub_type").val('Individual');
  <?php if($action != "Update"){?>
		$("label.error").html("");
		$('input:text').val('');
  <?php }?>
	}
	else
	{
		$(".dynamic").show();
		$(".disable").attr({"disabled": false});
		//$("#sub_type").val('Business');
  <?php if($action != "Update"){?>
		$("label.error").html("");
		$('input:text').val('');
  <?php }?>
	}
}

$(document.body).on('click', "#sub_addNew,#sub_submit", function(e){

	if($("#userEmailmsg label").text().length > 5)
	{
		return false;		
	}
	else
	{
		return true;
	}
});
$(document).ready(function(){


	$('.tooltip').tooltipster();
	//$('.custom-combobox.custom-combobox-input').attr("tabindex", 4);
	$("#sub_email").focus(function() {
		$("#userEmailmsg label").html("");
	});
	$.validator.addMethod("phoneValidate", function(value, element) {
    	return this.optional(element) || /^[0-9\-\+\,\.\)\(\s]+$/i.test(value);
	}, "Only numbers,-,+ and , allowed");
	$("#manage_subscribers").validate({
		rules: {
			sub_dir : {
				required : true
			},
			sub_name : {
				required : true
			},
			sub_email : {
				required : true,
				email : true
			},
			sub_confemail : {
				required : true,
				email : true,
				equalTo : "#sub_email"
			},
			<?php if($action!="Update" || $sub_password=="") { ?>
			sub_password : {
				required : true
			},
			sub_confpass : {
				required : true,
				equalTo : "#sub_password"
			},
			<?php } ?>
			sub_phone : {
				required : true,
				phoneValidate : true
			},
			sub_zip : {
				required : true,
				digits : true
			},
			sub_city : {
				required : true
			},
			sub_state : {
				required : true
			}
			<?php if($action!="Update" && $sub_type!="Individual") { ?>,
			sub_question : {
				required : true
			},
			sub_answer : {
				required : true
			}
			<?php } ?>
		},
		messages: {
			sub_dir : {
				required : "Please select subscribers directory name"
			},
			sub_name : {
				required : "Please enter subscribers name"
			},
			sub_email : {
				required : "Please enter subscribers email",
				email : "Please enter valid subscribers email"
			},
			sub_confemail : {
				required : "Please enter subscribers email",
				email : "Please enter valid subscribers email",
				equalTo : "Please enter same subscribers email"
			},
			sub_password : {
				required : "Please enter subscribers password"
			},
			sub_confpass : {
				required : "Please enter subscribers password",
				equalTo : "Please enter same subscribers password"
			},
			sub_phone : {
				required : "Please enter subscribers phone no.",
				phoneValidate : "Please enter valid subscribers phone no."
			},
			sub_zip : {
				required : "Please enter subscribers zip code",
				digits : "Please enter valid subscribers zip code"
			},
			sub_city : {
				required : "Please enter subscribers city"
			},
			sub_state : {
				required : "Please select subscribers state"
			},
			sub_question : {
				required : "Please enter subscribers question"
			},
			sub_answer : {
				required : "Please select subscribers answer"
			}
		}
	});
	<?php if($action=="Update"){?>
		$("#editdisable span.custom-combobox input").attr({"disabled": true});
		$("#editdisable span.custom-combobox a.custom-combobox-toggle").hide();
		$("#editdisable span.custom-combobox input").attr({"value": $("span.custom-combobox input").attr("placeholder")});
		$("#editdisable span.custom-combobox a.custom-combobox-toggle").hide();
	<?php } ?>
	<?php if($action=="Update") { ?>
	radiobtn = $("input:radio[name='sub_type']:checked").val();
	if(radiobtn=="Individual")
	{
		$("#titlecompany").hide();
		$("#Password").hide();
		$("#sub_password,#sub_confpass").attr({"disabled": true});
	}
	else
	{
		$("#titlecompany").show();
		$("#Password").show();
		$("#sub_password,#sub_confpass").attr({"disabled": false});
	}
	<?php } ?>
});


function checkSub($subemail){
var $dirID = document.getElementById('sub_dir').value;
if($dirID > 0){
checkSubscriber($subemail,'','Member',$dirID);
}
else{
return false;
}
//alert($dirID);
}


</script>