<?php
	#################################################################
	$SECTION_FIELD_PREFIX='msg_';
	$SECTION_AUTO_ID=$_REQUEST[$SECTION_FIELD_PREFIX.'id'];
	$SECTION_WHERE=$SECTION_FIELD_PREFIX."id=".$SECTION_AUTO_ID."";
	$SECTION_VIEW_PAGE=MEB_VIEW_MESSAGING;
	$SECTION_MANAGE_PAGE=MEB_MANAGE_MESSAGING;
	$SECTION_TABLE=TBL_MEMBER_MESSAGING;
	$SECTION_NAME='Messaging';

	#################################################################

	if(isset($_POST)){	
		$data = $_POST;	
	}

    $action = $_REQUEST['method'];
   // echo $data['msg_save'];exit;
	if ($data['msg_save'] == 'Save Message') {

		if($mebType!="Business") {
			$add_values[$SECTION_FIELD_PREFIX.'meb_id'] 	= trim($data[$SECTION_FIELD_PREFIX."meb_id"]);
		} else {
			$add_values[$SECTION_FIELD_PREFIX.'sub_id'] 	= trim(getFreeMemberSessionId());
		}

        $add_values[$SECTION_FIELD_PREFIX.'dir_id'] 		= $dirId;
		$add_values[$SECTION_FIELD_PREFIX.'message'] 		= trim($data[$SECTION_FIELD_PREFIX."message"]);
        $add_values[$SECTION_FIELD_PREFIX.'company_title'] 	= trim($data["msg_company_title"]);
        $add_values['buc_id'] 	                            = trim($data["buc_id"]);

        $que = trim($data["msg_quetion"]);
        if($que=='yes')
            $que='1';
        else
            $que='0';
        $add_values[$SECTION_FIELD_PREFIX.'has_question'] 	= $que;

        if($data['msg_submit']== 'Publish now') {
            $add_values[$SECTION_FIELD_PREFIX.'type'] 		= "Published";
        } else {
            $add_values[$SECTION_FIELD_PREFIX.'type'] 		= "Draft";
        }

        $add_values[$SECTION_FIELD_PREFIX.'status'] 		= "Active";
        if($action != 'Update'){
            $add_values[$SECTION_FIELD_PREFIX.'created_id'] 	= $mebId;
            $add_values[$SECTION_FIELD_PREFIX.'created_date'] 	= date("Y-m-d H:i:s");
            $add_values[$SECTION_FIELD_PREFIX . 'updated_id'] 	= $mebId;
            $add_values[$SECTION_FIELD_PREFIX . 'updated_date'] = date("Y-m-d H:i:s");
            $GPDetail_result = $db->insertData($SECTION_TABLE, $add_values);
            $msgId = $GPDetail_result;

            if($que=='1') {
                $add_values_q['msg_id'] 	= $msgId;
                $add_values_q['user_id'] 	= $mebId;
                $add_values_q['question'] 	= trim($data["msg_quetion_title"]);
                $add_values_q['que_type'] 	= 'radio';
                $add_values_q['created_date'] 	= date("Y-m-d H:i:s");
                $add_values_q['edited_date']   = date("Y-m-d H:i:s");
                $GPQ_result = $db->insertData(TBL_QUESTION, $add_values_q);
                $QueId = $GPQ_result;
                if($QueId) {
                    $total_q='4';

                    for($qp=1;$qp<=$total_q;$qp++){
                        if(isset($_REQUEST['opt'.$qp])){
                            $add_values_op['que_id'] 	    = $QueId;
                            $add_values_op['opt_title'] 	= $_REQUEST['opt'.$qp];
                            $GPQ_resultd = $db->insertData(TBL_QUESTION_OPTION, $add_values_op);
                        }
                    }
                }
            }

            $_SESSION['msg']  =   "Broadcast has been inserted successfully.";
        } else {
            $mebId = $SECTION_AUTO_ID;
            $add_values[$SECTION_FIELD_PREFIX . 'updated_id'] 			= $mebId;
            $add_values[$SECTION_FIELD_PREFIX . 'updated_date'] 		= date("Y-m-d H:i:s");
            $GPDetail_result = $db->updateData($SECTION_TABLE, $add_values, $SECTION_WHERE);
            $msgId = $SECTION_AUTO_ID;

            if($que=='1') {
                $que_query = 'SELECT * FROM '.TBL_QUESTION.' WHERE msg_id="'.$msgId.'" AND user_id="'.getMemberSessionId().'"';
                $queResult  = $db->select($que_query);

                if(count($queResult)>0){
                    $queID = $queResult[0]['que_id'];

                    $add_values_qp['question'] 	= trim($data["msg_quetion_title"]);
                    $add_values_qp['edited_date']   = date("Y-m-d H:i:s");
                    $SECTION_WHERE_QP="que_id=".$queID."";
                    $GPQ_result = $db->updateData(TBL_QUESTION, $add_values_qp, $SECTION_WHERE_QP);
                } else {
                    $add_values_q['msg_id'] 	= $msgId;
                    $add_values_q['user_id'] 	= getMemberSessionId();
                    $add_values_q['question'] 	= trim($data["msg_quetion_title"]);
                    $add_values_q['que_type'] 	= 'radio';
                    $add_values_q['created_date'] 	= date("Y-m-d H:i:s");
                    $add_values_q['edited_date']   = date("Y-m-d H:i:s");
                    $GPQ_result = $db->insertData(TBL_QUESTION, $add_values_q);
                    $QueId = $GPQ_result;
                    if($QueId) {
                        $total_q='4';

                        for($qp=1;$qp<=$total_q;$qp++){
                            if(isset($_REQUEST['opt'.$qp])){
                                $add_values_op['que_id'] 	    = $QueId;
                                $add_values_op['opt_title'] 	= $_REQUEST['opt'.$qp];
                                $GPQ_resultd = $db->insertData(TBL_QUESTION_OPTION, $add_values_op);
                            }
                        }
                    }
                }
            } else {
                $que_query = 'SELECT * FROM '.TBL_QUESTION.' WHERE msg_id="'.$msgId.'" AND user_id="'.getMemberSessionId().'"';
                $queResult  = $db->select($que_query);

                if(count($queResult)>0){
                    $queID = $queResult[0]['que_id'];
                    $que_query = 'DELETE FROM '.TBL_QUESTION.' WHERE que_id="'.$queID.'"';
                    $db->select($que_query);

                    $que_query = 'DELETE FROM '.TBL_QUESTION_OPTION.' WHERE que_id="'.$queID.'"';
                    $db->select($que_query);
                }
            }
            $_SESSION['msg']  =   "Broadcast has been updated successfully.";
        }

		$messagefile	= $_FILES["msg_file"];
		$filename		= clean($messagefile['name']);
		$filetype		= $messagefile['type'];

        if(is_dir(UPLOAD_DIR_MESSAGING_FILE."/".$msgId) == false) {
			mkdir(UPLOAD_DIR_MESSAGING_FILE."/".$msgId);
		}

		if ($filename != "") {

			if (file_exists(UPLOAD_DIR_MESSAGING_FILE.$msgId."/".$filename)) {
				unlink(UPLOAD_DIR_MESSAGING_FILE.$msgId."/".$filename);
			}
			
			if(move_uploaded_file($_FILES['msg_file']['tmp_name'],UPLOAD_DIR_MESSAGING_FILE.$msgId."/".$filename)) {
				unlink(UPLOAD_DIR."temp/".$filename);
			}
		} else {
            $filename = $data["edit_msg_file"];
        }
        unset($update_values);
        $update_values[$SECTION_FIELD_PREFIX . 'file']		  		= $filename;
        $update_values[$SECTION_FIELD_PREFIX . 'updated_id']		= getMemberSessionId();
        $update_values[$SECTION_FIELD_PREFIX . 'updated_date']		= date('Y-m-d H:i:s');
        $SECTION_WHERE = "msg_id = '".$msgId."'";
        $GPDetail_result_update = $db->updateData($SECTION_TABLE, $update_values, $SECTION_WHERE);

        $URL = getMemberURL($SECTION_VIEW_PAGE); 
		redirect($URL);
    	exit;
	}

	if($action=='Edit' || $action=="Update") {
		$list_query     = getSelectList($SECTION_TABLE,$SECTION_FIELD_PREFIX);
		$list_query     .= "AND ".$SECTION_FIELD_PREFIX."id = '".$SECTION_AUTO_ID."'";
		$result_query   = $db->select($list_query);
        $cat_id         = $result_query[0]["buc_id"];
		$msg_message 	= trim($result_query[0][$SECTION_FIELD_PREFIX."message"]);
		$msg_file 		=	$result_query[0][$SECTION_FIELD_PREFIX."file"];
        $msg_company_title 	=	$result_query[0][$SECTION_FIELD_PREFIX."company_title"];
        $dir_quetion        =   $result_query[0][$SECTION_FIELD_PREFIX."has_question"];

        $que_query = 'SELECT * FROM '.TBL_QUESTION.' WHERE msg_id="'.$SECTION_AUTO_ID.'" AND user_id="'.getMemberSessionId().'"';
        $queResult  = $db->select($que_query);

        $msg_quetion_title='';
        $msg_quetion_id='';
        $queOptResult=array();
        if(count($queResult)>0) {
            $msg_quetion_title = $queResult[0]['question'];
            $msg_quetion_id = $queResult[0]['que_id'];

            $que_opt_query = 'SELECT * FROM '.TBL_QUESTION_OPTION.' WHERE que_id="'.$msg_quetion_id.'"';
            $queOptResult  = $db->select($que_opt_query);
        }

		$action         = "Update";
		$action_url     = getMemberURL($SECTION_MANAGE_PAGE,$action,$SECTION_WHERE);
	} else {
        $action = "Add";
    }
?>

<div class="container search-group-section">
    <div class="row">
        <div class="step-list">
            <ul>
                <li><a href="#" class="active"><?php if($action=="Update"){echo 'Edit Broadcast';}else{echo 'Create Broadcast';} ?></a></li>
            </ul>
        </div>
    </div>
</div>

<div class="container step-page-section">
    <div class="row">
        <form name="edit_msg" method="post" action="<?php echo $action_url; ?>" id="edit_msg" enctype="multipart/form-data">
            <?php if($SECTION_AUTO_ID!=''){
                echo '<input name="msg_id" id="msg_id" value="'.$SECTION_AUTO_ID.'" type="hidden">';
            } ?>
            <input name="msg_meb_id" id="msg_meb_id" value="<?php echo $mebId; ?>" type="hidden">
            <input type="hidden" value="Save Message" id="msg_save" name="msg_save">
            <input type="hidden" value="<?php echo $action; ?>" id="method" name="method">

            <div class="form-main">
                <div class="main-row">
                    <label>Category <?php echo getRequiredIcon()?></label>
                    <?=getBusinessCategoryList("buc_id","buc_id",$cat_id); ?>
                </div>

                <div class="first-col new-txt" >
                    <div class="main-row">
                        <label>Company Name <?php echo getRequiredIcon()?></label>
                        <input type="text" name="msg_company_title" id="msg_company_title" value="<?php echo $msg_company_title; ?>" />
                    </div>
                </div>

                <div class="first-col new-txt">
                    <div class="main-row">
                        <label>Message <?php echo getRequiredIcon()?></label>
                        <textarea name="msg_message" id="msg_message" value=""><?php echo $msg_message; ?></textarea>
                    </div>
                </div>
                <div class="bottomDiv newbtm">
                    <div class="main-row attach">
                        <label>Attach File</label>
                        <input type="file" id="msg_file" name="msg_file">
                        <?php
                        if($msg_file!="") {
                            echo '<div class="msg_filesddd">';
                            echo "<a id='vediosPlay1' href='view_file_message.php?filename=".$msg_file."&mas_id=".$SECTION_AUTO_ID."' style='margin-top:0px;' class='fancybox fancybox.ajax' title='View' data-fancybox-type='ajax' >".$msg_file."</a>";
                            echo "&nbsp;&nbsp;&nbsp;<a style='margin-top:5px;' id='removeLink' onclick='javascript:removeFile(edit_msg_file,\"vediosPlay1\");'>Remove</a>";
                            echo "<input type='hidden' id='edit_msg_file' name='edit_msg_file' value='".$msg_file."' />";
                            echo '</div>';
                        }
                        ?>
                    </div>
                </div>
                <?php
                    $checked='';
                    $style_dis='display: none;';
                    if($dir_quetion=='1') {
                        $checked = 'checked="checked"';
                        $style_dis='';
                    }
                ?>
                <div class="first-col new-txt">
                    <div class="checkbox">
                        <input type="checkbox" <?php echo $checked; ?> value="yes" data_id="<?php echo $msg_quetion_id; ?>" name="msg_quetion" id="msg_quetion" data-label="Survey" />
                        <label class="tooltip" for="msg_quetion" title="Survey"><span class="check-box-lable-text">Survey</span></label>
                    </div>
                </div>

                <div id="quetion_box_main" style="<?php echo $style_dis; ?>">
                    <div class="first-col new-txt">
                        <div class="main-row">
                            <label>Question <?php echo getRequiredIcon()?></label>
                            <input type="text" name="msg_quetion_title" id="msg_quetion_title" value="<?php echo $msg_quetion_title; ?>" />
                        </div>
                    </div>
                    <div class="first-col new-txt">
                        <div class="main-row">
                            <a href="javascript:void(0);" class="add_new_option">Add Answer</a>
                            <div id="TextBoxesGroup">
                                <?php
                                    $totqueOptResult='';
                                    count($queOptResult);
                                    if(count($queOptResult)>0) {
                                        $totqueOptResult = count($queOptResult);
                                        for($k=0,$q=1;$k<$totqueOptResult;$k++,$q++) {
                                            $title=$queOptResult[$k]['opt_title'];
                                            $ids=$queOptResult[$k]['opt_id'];
                                        ?>
                                            <div id="TextBoxDiv<?php echo $q; ?>">
                                                <label>Answer <?php echo $q; ?> * </label>
                                                <a class="remove_new_option" data_cou="<?php echo $q; ?>" href="javascript:void(0);">Remove</a>
                                                <input type="text" class="option_text_box" name="opt<?php echo $q; ?>" id="opt<?php echo $q; ?>"  data_id="<?php echo $ids; ?>" data_value="<?php echo $title; ?>" value="<?php echo $title; ?>" >
                                            </div>
                                        <?php
                                        }
                                    } else {
                                ?>
                                    <div id="TextBoxDiv1">
                                        <label>Answer 1 * </label>
                                        <a class="remove_new_option" data_cou="1" href="javascript:void(0);">Remove</a>
                                        <input type="text" class="option_text_box" name="opt1" id="opt1" value="" >
                                    </div>
                                    <div id="TextBoxDiv2">
                                        <label>Answer 2 * </label>
                                        <a class="remove_new_option" data_cou="2" href="javascript:void(0);">Remove</a>
                                        <input type="text" class="option_text_box" name="opt2" id="opt2" value="" >
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="step-button-row">
                    <div class="step-button-5">
                        <input type="submit" value="Publish now" id="publish_submit" name="msg_submit">
                    </div>
                    <div class="step-button-5 last">
                        <input type="submit" value="Publish later" id="publish_submit" name="msg_submit">
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<?php
$error_re='';
if($_SESSION['err_file']!='') {
    $error_re=$_SESSION['err_file'];
    $_SESSION['err_file']='';
}
?>

<script>
function removeFile($fileName,linkss) {
    $hiddenId = $fileName.id
    $("#"+linkss).remove();
    $("#removeLink").remove();
    $('#'+$hiddenId).val('');
    $('#remove_image').val('true');
    $( "#bus_submit" ).trigger( "click" );
}
qto='<?php echo $q; ?>';

if(qto!='')
    var counter = qto;
else
    var counter = 3;
$(document).ready(function(){
    error_re='';
    error_re='<?php echo $error_re; ?>';
    if(error_re!='') {
        alertify.error(error_re);
        //var notification = alertify.notify(error_re, 'success', 5, function(){  console.log('dismissed'); });
    }

    edd = '<?php echo $action; ?>';
    if(edd=='Update'){
        $(document).prop('title', '<?php echo MEB_FILE_TITLE_EDIT_MESSAGING; ?>');
    }

	$('.tooltip').tooltipster();
	$("#edit_msg").validate({
		rules: {
            buc_id : {
                required : true
            },
            msg_company_title : {
                required : true
            },
			msg_message : {
				required : true
			}
		},
		messages: {
			msg_message : {
				required : "Please enter message"
			}
		}
	});
    $(".custom-select").each(function(){
        $(this).wrap("<span class='select-wrapper'></span>");
        $(this).after("<span class='holder'></span>");
    });
    $(".custom-select").change(function(){
        var selectedOption = $(this).find(":selected").text();
        $(this).next(".holder").text(selectedOption);
    }).trigger('change');

    $(document).on('change', '#msg_quetion', function(){
        if ($('#msg_quetion').prop('checked')) {
            $('#quetion_box_main').slideDown();
        } else {
            $('#quetion_box_main').slideUp();
        }
    });

    $(".add_new_option").click(function () {
        if(counter>4){
            //alert("Only 4 options allow");
            alertify.notify("Only 4 options allow");
            return false;
        }

        var newTextBoxDiv = $(document.createElement('div')).attr("id", 'TextBoxDiv' + counter);
        newTextBoxDiv.after().html('<label>Answer '+ counter + ' * </label><a class="remove_new_option" data_cou="'+counter+'" href="javascript:void(0);">Remove</a>' + '<input type="text" data_id="" class="option_text_box" name="opt' + counter + '" id="opt' + counter + '" value="" >');
        newTextBoxDiv.appendTo("#TextBoxesGroup");
        counter++;
    });
});
ajax_url = '<?php echo ROOT_WWW; ?>'+'javascript/ajax/getMessagingSave.php';

$(document).on('click','.remove_new_option',function( ) {
    data_cou = $(this).attr('data_cou');

    if(counter==1){
        alertify.notify("No more option to remove");
        return false;
    }
    counter--;

    data_id= $('#opt'+data_cou).attr('data_id');
    if(data_id!=''){
        msg_id = '';
        if($('#msg_id').val())
            msg_id = $('#msg_id').val();
        if(msg_id!='') {
            data = 'data_id='+data_id+'&msg_id='+msg_id;
            $.ajax({
                url: ajax_url,
                type: 'POST',
                data: data+'&action=DeleteOpt',
                dataType: 'json'
            }).done(function(response) {

            });
        }
    }
    $("#TextBoxDiv" + data_cou).remove();
});

$(document).on('submit','#edit_msg',function( ){
    error='';
    $('#quetion_box_main').removeClass('error');

    if ($('#msg_quetion').prop('checked')) {
        if($('#msg_quetion_title').val()==''){
            $('#msg_quetion_title').addClass('error');
            error='yes';
        }

        var n = $( ".option_text_box" ).length;
        if(n) {
            $(".option_text_box").each(function() {
                if($(this).val()==''){
                    $(this).addClass('error');
                    error='yes';
                }
            });
        } else {
            alertify.notify("Please add option.");
            error='yes';
        }
    }

    if(error=='')
        return true;
    else
        return false;
});

$(document).on('blur', '.option_text_box', function() {
    data_val = $(this).attr('data_value');
    val = $(this).val();
    thiss = $(this);
    if(val!='') {
        if(val!=data_val) {
            msg_id = '';
            if($('#msg_id').val())
                msg_id = $('#msg_id').val();
            if (msg_id != '') {
                data_id = $(this).attr('data_id');
                qeu_id = $('#msg_quetion').attr('data_id');
                action = '';

                data = 'val=' + val + '&msg_id=' + msg_id;
                if (data_id != '') {
                    data += '&data_id=' + data_id + '&action=SaveChages';
                } else {
                    data += '&action=AddNewOpt' + '&que_id=' + qeu_id;
                    action = 'AddNewOpt';
                }

                $.ajax({
                    url: ajax_url,
                    type: 'POST',
                    data: data,
                    dataType: 'json'
                }).done(function (response) {
                    if (action == 'AddNewOpt') {
                        thiss.attr('data_id', response);
                    }
                });
            }
        }
    }
});

</script>