<script type="text/javascript" src="<?php echo AJAX_FOLDER_WWW; ?>getAjaxMembers.js"></script>
<?php 
	#################################################################
	$SECTION_FIELD_PREFIX='cop_';
	$SECTION_AUTO_ID=$_REQUEST[$SECTION_FIELD_PREFIX.'id'];
	$SECTION_WHERE=$SECTION_FIELD_PREFIX."id=".$SECTION_AUTO_ID."";
	$SECTION_VIEW_PAGE=MEB_VIEW_COUPONS;
	$SECTION_MANAGE_PAGE=MEB_MANAGE_COUPONS;
	$SECTION_TABLE= TBL_MEMBER_COUPONS;
	$SECTION='Coupons';

	//State Listing...
	$state_fields = array("sta_id","sta_name");
	$state_where  = "sta_status = 'Active'";
	$staRes 	= $db->selectData(TBL_STATE,$state_fields,$state_where,$extra="",2);
	#################################################################
?>
<script type="text/javascript" language="javascript">
$(document).ready(function(){
    getpagelistingNew('<?php echo $SECTION; ?>','<?php echo $SECTION_TABLE?>','<?php echo $SECTION_FIELD_PREFIX?>','<?php echo $SECTION_MANAGE_PAGE; ?>','','');

	$("#cop_valid_from").datepicker({
		dateFormat: "mm/dd/yy",
		changeMonth: true,
		changeYear: true,
		buttonImageOnly: false,
		onSelect: function(selected) 
		{						
			$('#cop_valid_thru').datepicker('option', 'minDate', new Date(selected));
		}
	});
	$("#cop_valid_thru").datepicker({
		dateFormat: "mm/dd/yy",
		changeMonth: true,
		changeYear: true,
		buttonImageOnly: false,
		onSelect: function(selected) 
		{						
			$('#cop_valid_from').datepicker('option', 'maxDate', new Date(selected));
		}
	});
	$('#cop_valid_from').datepicker('option', 'maxDate', new Date($('#cop_valid_thru').val()));
	$('#cop_valid_thru').datepicker('option', 'minDate', new Date($('#cop_valid_from').val()));
});
</script>

<div class="container search-group-section">
    <div class="row">
        <form name="search_cop" id="search_cop" action="" method="post" onsubmit="return false;">
            <div class="search-section-left">
                <input type="search" name="cop_title" id="cop_title" placeholder="Search" value="" />
                <div class="buttons_right">
                    <input type="submit" onclick="search_dir();" name="sub_search" id="con_search" value="" />
                    <input style="display: none;" type="button" id="bus_reset" onclick="resetForm('search_cop','<?php echo $SECTION; ?>','<?php echo $SECTION_TABLE?>','<?php echo $SECTION_FIELD_PREFIX?>','<?php echo $SECTION_MANAGE_PAGE; ?>');" value="Clear" />
                </div>
            </div>
        </form>
        <div class="search-section-right">
            <div class="coupons-text"><a href="javascript:void(0);">Coupons</a></div>
        </div>
    </div>
</div>

<div class="container member-page-setion-1">
    <div class="row">
        <input type="hidden" name="total_nage" id="total_page" value="">
        <input type="hidden" name="current_page" id="current_page" value="">

        <div class="result-section">
            <div class="result-text">Result</div>

            <div class="result-add-remove-icon">
                <?php $href_export = "export_excel.php?table=tbl_member_coupons&prifix=cop_"; ?>
                <a href="<?php echo $href_export; ?>" class="tooltip file-icon" title="Export"></a>
                <a class="tooltip pluse-icon" href="<?php echo MEB_INDEX_PARAMETER.$SECTION_MANAGE_PAGE; ?>" title="Add"></a>
            </div>
        </div>

        <div class="group-setion-row" id="updatediv">
            <div class="pre_loader_main" >
                <img src="images/spinner.gif">
            </div>
        </div>
        <a style="display: none;" id="next" href="index2.html">next page?</a>
    </div>
</div>

<?php
$error_re='';
if($_SESSION['msg']!='') {
    $error_re=$_SESSION['msg'];
    $_SESSION['msg']='';
}
?>

<script>
    error_re='';
    error_re='<?php echo $error_re; ?>';
    if(error_re!='') {
        alertify.success(error_re);
        //var notification = alertify.notify(error_re, 'success', 5, function(){  console.log('dismissed'); });
    }

    function search_dir() {
        search_key = $.trim($('#cop_title').val());
        $("#updatediv").html('');
        // $("#updatediv").infinitescroll("destroy");
        $("#updatediv").infinitescroll("unbind");
        getpagelistingNew('<?php echo $SECTION; ?>','<?php echo $SECTION_TABLE?>','<?php echo $SECTION_FIELD_PREFIX?>','<?php echo $SECTION_MANAGE_PAGE; ?>','',search_key);
    }

    function infinite_f() {
        $("#updatediv").infinitescroll("bind");
        total = $('#total_page').val();
        url = ajax_folder+"getCouponsList.php";

        search_key = $.trim($('#cop_title').val());
        url = url+'?fieldPrefix=cop_&managePage=manage_coupons&search_text='+search_key+'&tableName=tbl_member_coupons&xtraCondition=&order=&page=';

        $("#updatediv").infinitescroll({
            navSelector  	: "#next:last",
            nextSelector 	: "a#next:last",
            itemSelector 	: "#updatediv div.gr_on_view_card123",
            msgText         : "Loading group list...",
            debug		 	: true,
            dataType	 	: 'html',
            maxPage         : total,//$('#total_page').val(),

            path: function(index) {
                current = $('#current_page').val();
                current_next = parseInt(current)+ parseInt('1');
                //url = url+'?fieldPrefix=dir_&managePage=manage_directory&search_text=&tableName=tbl_directory&xtraCondition=&orderby=id&order=&page='+current_next;
                return url+current_next;
            },
            state: {
                isDestroyed: false,
                isDone: false,
                isDuringAjax : false
            }
            // behavior		: 'twitter',
            // appendCallback	: false, // USE FOR PREPENDING
            // pathParse     	: function( pathStr, nextPage ){ return pathStr.replace('2', nextPage ); }
        }, function(newElements, data, url){
            $('#current_page').val(current_next);
            $("#updatediv").append(newElements);
            $('.tooltip').tooltipster();
            $.fancybox.close();

            // $("#updatediv").prepend(newElements);
        });
    }

</script>