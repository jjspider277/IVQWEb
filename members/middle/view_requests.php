<script type="text/javascript" src="<?php echo AJAX_FOLDER_WWW; ?>getAjaxMembers.js"></script>
<?php 
#################################################################
    $SECTION_FIELD_PREFIX='sbm_';
	$SECTION_AUTO_ID=$_REQUEST[$SECTION_FIELD_PREFIX.'id'];
	$SECTION_WHERE=$SECTION_FIELD_PREFIX."id=".$SECTION_AUTO_ID."";
	$SECTION_VIEW_PAGE=MEB_VIEW_REQUESTS;
	$SECTION_MANAGE_PAGE=MEB_MANAGE_REQUESTS;
	
	$SECTION='Requests';

	$type = getMemberType();
    $SECTION_TABLE= TBL_MEMBER_SUBSCRIBE_DIRECTORY;
    $padding = "style='padding: 1.5px;'";

#################################################################

?>
<script type="text/javascript" language="javascript">
$(document).ready(function(){
    getpagelistingNew('<?php echo $SECTION; ?>','<?php echo $SECTION_TABLE?>','<?php echo $SECTION_FIELD_PREFIX?>','<?php echo $SECTION_MANAGE_PAGE; ?>','','');
});
</script>

<div class="container search-group-section">
    <div class="row">
        <form name="search_bus" id="search_bus" action="" method="post" onsubmit="return false;">
            <div class="search-section-left">
                <input type="search" name="search_text" id="search_text" placeholder="Search" class="">
                <div class="buttons_right">
                    <input type="submit" name="bus_search" id="bus_search" value="" onclick="search_mem()">
                </div>
            </div>
        </form>

        <div class="search-section-right">
            <div class="request-add-text"><a href="javascript:void(0);">Requests</a></div>
        </div>
    </div>
</div>

<div class="container member-page-setion-1">
    <div class="row">
        <input type="hidden" name="total_nage" id="total_page" value="">
        <input type="hidden" name="current_page" id="current_page" value="">

        <div class="result-section">
            <div class="result-text">Result</div>

            <div class="result-add-remove-icon">
                <?php $href_export = "export_excel.php?table=tbl_member_subscribe_directory&prifix=sbm_"; ?>
                <a href="<?php echo $href_export; ?>" class="file-icon tooltip" title="Export"></a>
            </div>

        </div>

        <div class="group-setion-row">
            <div class="group-setion-row" id="updatediv">
                <div class="pre_loader_main" >
                    <img src="images/spinner.gif">
                </div>
            </div>
        </div>
        <a style="display: none;" id="next" href="index2.html">next page?</a>

    </div>
</div>

<link rel="stylesheet" href="js/owl.carousel.css" type="text/css" media="screen" /></div>
<script src="js/owl.carousel.js"></script>

<?php
$msg = $_SESSION["msg"];
$_SESSION["msg"]='';
?>
<script type="text/javascript">
    $(document).ready(function() {
       msg='';
        msg='<?php echo $msg; ?>';
        if(msg!='') {
            alertify.success(msg);
            //var notification = alertify.notify(error_re, 'success', 5, function(){  console.log('dismissed'); });
        }
    });

    $(document).on('mouseover','.gr_hover_this .admin_memebr_t_l_in', function(){
        $('.gr_hover_this .admin_memebr_t_l_in span').show();
    });

    $(document).on('mouseout','.gr_hover_this .admin_memebr_t_l_in', function(){
        $('.gr_hover_this .admin_memebr_t_l_in span').hide();
    });

    $(document).on('click','.video_show_slide, .close_video',function(){
        if($('.videoslider_main').is(':visible')) {
            $('.videoslider_main').stop().fadeOut();
            $('.detail_slider').stop().slideDown();
        } else {
            $('.detail_slider').stop().slideUp();
            $('.videoslider_main').stop().slideDown();
        }
    });

    function search_mem() {
        search_key = $.trim($('#search_text').val());
        $("#updatediv").html('');
        // $("#updatediv").infinitescroll("destroy");
        $("#updatediv").infinitescroll("unbind");
        getpagelistingNew('<?php echo $SECTION; ?>','<?php echo $SECTION_TABLE?>','<?php echo $SECTION_FIELD_PREFIX?>','<?php echo $SECTION_MANAGE_PAGE; ?>','',search_key);
    }

    function infinite_f() {
        $("#updatediv").infinitescroll("bind");
        total = $('#total_page').val();
        search_key = $.trim($('#search_text').val());

        url = ajax_folder+"getBusinessList.php"+'?fieldPrefix=bus_&managePage=manage_member&search_text='+search_key+'&tableName=tbl_member_business&xtraCondition=&orderby=id&order=&page=';

        $("#updatediv").infinitescroll({
            navSelector  	: "#next:last",
            nextSelector 	: "a#next:last",
            itemSelector 	: "#updatediv div.gr_on_view_card123",
            msgText         : "Loading group list...",
            debug		 	: true,
            dataType	 	: 'html',
            maxPage         : total,//$('#total_page').val(),

            path: function(index) {
                current = $('#current_page').val();
                current_next = parseInt(current)+ parseInt('1');
                //url = url;
                return url+current_next;
            },
            state: {
                isDestroyed: false,
                isDone: false,
                isDuringAjax : false
            }
            // behavior		: 'twitter',
            // appendCallback	: false, // USE FOR PREPENDING
            // pathParse     	: function( pathStr, nextPage ){ return pathStr.replace('2', nextPage ); }
        }, function(newElements, data, url){
            $('#current_page').val(current_next);
            $("#updatediv").append(newElements);
            $('.tooltip').tooltipster();
            $.fancybox.close();

            // $("#updatediv").prepend(newElements);
        });
    }
</script>