<div class="container group-setion">
<div class="row">
	<div class="max_width">
	<div class="second_menu">
	  	<ul>	    
	    	<li><a  href="#"><i class="icon_style no_media_icon_blue"></i>Media</a></li>
	  	</ul></div>

	<div class="user_slider" id="owl-demo">
		<div class="item">
			<div class="user_slide">
         		<a href="http://development.b2bms.net/upload_files/Business/Business_Sub/63/cdv_photo_001.jpg.jpeg" class="fancybox"><img src="http://development.b2bms.net/upload_files/Business/Business_Sub/63/cropped_details/cdv_photo_001.jpg.jpeg" class="popphoto"></a>
                <div class="user_name">
					<strong>Carl Davis Jr</strong> - President
					Silicon Valley Black Chamber of Commerce
				</div>
			</div>
		</div>
		<div class="item">
			<div class="user_slide cmpny_info_slide">
				<a href="http://development.b2bms.net/upload_files/Business/Business_Sub/63/cdv_photo_002.jpg.jpeg" class="fancybox"><img src="http://development.b2bms.net/upload_files/Business/Business_Sub/63/cdv_photo_002.jpg.jpeg" class="popphoto"></a>
				<div class="company_detail">
					<p>25 N. 14th Street #505, San Jose<br>
					California, 95112<br>
					<a target="_new" href="www.blackchamber.com">www.blackchamber.com</a><br>
					P. 4082888806<br>
					F. 4085092886<br>
					<a href="mailto:Pres@blackchamber.com">Pres@blackchamber.com</a>
					</p>
				</div>                                    
			</div>
		</div>
	</div>
		</div>
	</div>

 <link rel="stylesheet" href="js/owl.carousel.css" type="text/css" media="screen" /></div>
  <script src="js/owl.carousel.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
 
  $("#owl-demo").owlCarousel({
 
      navigation : true, // Show next and prev buttons
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem:true
 
      // "singleItem:true" is a shortcut for:
      // items : 1, 
      // itemsDesktop : false,
      // itemsDesktopSmall : false,
      // itemsTablet: false,
      // itemsMobile : false
 
  });
 
});
</script>



