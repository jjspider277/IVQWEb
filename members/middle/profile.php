<?php
if(isset($_POST)){	
	$data = $_POST;	
}
if ($data['change_password'] == 'Change') {
	$uname = $data['change_username'];
	$upass = md5($data['change_old_pass']);

	$mebType = getMemberType();
	$sql="select meb_id from ".TBL_MEMBER." where meb_id=".getMemberSessionId();

	$change_data = $db->select($sql);
	if(!empty($change_data)) {
		unset($add_values);

        $add_values['meb_name'] 		    = $data["meb_firstname"];
        $add_values['meb_lastname'] 		= $data["meb_lastname"];
        //$add_values['meb_phone'] 		    = $data["meb_phone"];

        if(isset($data["change_new_pass"]) && $data["change_new_pass"]!='')
            $add_values['meb_password'] 		= md5($data["change_new_pass"]);

        $add_values['meb_updated_id'] 		= $mebId;
        $add_values['meb_updated_date'] 	= date("Y-m-d H:i:s");
        $SECTION_WHERE = "meb_id=".$change_data[0]["meb_id"];
        $id = $db->updateData(TBL_MEMBER, $add_values,$SECTION_WHERE);

		$_SESSION["msg"] = "Successfully updated profile.";
		$URL = getMemberURL(MEB_PROFILE);
		redirect($URL);
		exit;
	} else {
		$_SESSION["err"] = "Wrong username or password...";
		$_SESSION['add_detail_error'][]=$_POST;
		$URL = getMemberURL(MEB_PROFILE);
		redirect($URL);
		exit;
	}
}

$mebId = getMemberSessionId();
if($mebId) {
    $sql="select * from ".TBL_MEMBER." where meb_id=".getMemberSessionId();
    $result_query = $db->select($sql);

    //var_dump($result_query);

	$change_username = $result_query[0]["meb_username"];
    $first_name = $result_query[0]["meb_name"];
    $last_name = $result_query[0]["meb_lastname"];
    $phone = $c_phone = $result_query[0]["meb_phone"];

	unset($_SESSION['add_detail_error']);
}
?>
<div class="container step-page-section">
    <div class="row">
        <?php if($_SESSION["err"]!="") { ?>
            <script>
                alertify.error('<?php echo $_SESSION["err"]; ?>');
            </script>
        <?php $_SESSION["err"]=""; } ?>

        <?php if($_SESSION["msg"]!="") { ?>
            <script>
                alertify.success('<?php echo $_SESSION["msg"]; ?>');
            </script>
        <?php $_SESSION["msg"]=""; } ?>

        <form name="change_pass_frm" id="change_pass_frm" action="" method="post" autocomplete="off" >
            <input type="hidden" name="change_password" value="Change">

            <div class="form-main">

                <label>First Name <?php echo getRequiredIcon()?></label>
                <input type="text" name="meb_firstname" id="meb_firstname" tabindex="1" value="<?php echo $first_name; ?>" />

                <label>Last Name <?php echo getRequiredIcon()?></label>
                <input type="text" name="meb_lastname" id="meb_lastname" tabindex="2" value="<?php echo $last_name; ?>" />

                <label>E-mail address <?php echo getRequiredIcon()?></label>
                <input disabled="disabled" type="text" name="change_username" id="change_username" tabindex="3" value="<?php echo $change_username; ?>" />

                <label>Password</label>
                <input type="password" name="change_new_pass" id="change_new_pass" tabindex="4" value="" />

                <label>Confirm Password</label>
                <input type="password" name="change_conf_pass" id="change_conf_pass" tabindex="5" value="" />

                <div class="button-section" style="float: right;">
                    <div class="login"><a href="javascript:void(0);" onclick="return checkpass()">Submit<span class="login-arrow"></span></a></div>
                </div>

            </div>
        </form>

    </div>
</div>

<script type="text/javascript">
    function checkpass() {
        $('#change_conf_pass').removeClass('error');
        err='';
        if($('#change_new_pass').val()!='') {
            if($('#change_conf_pass').val()=='') {
                $('#change_conf_pass').addClass('error');
                err=='yes'
                return false;
            } else if($('#change_new_pass').val()!=$('#change_conf_pass').val()){
                $('#change_conf_pass').addClass('error');
                err=='yes'
                return false;
            }
        }

        if(err=='') {
            $('#change_pass_frm').submit();
        }
    }
$(document).ready(function(){
    $("#change_pass_frm").validate({
		rules: {
            meb_firstname:"required",
            meb_lastname:"required"
			/*change_new_pass : {
				required : true
			},
			change_conf_pass : {
				required : true,
				equalTo : "#change_new_pass"
			},*/
		},
		messages: {
			change_username : {
				required : "Please enter username",
				email : "Please enter valid email id"
			},
			change_old_pass : {
				required : "Please enter old password"
			},
			change_new_pass : {
				required : "Please enter new password"
			},
			change_conf_pass : {
				required : "Please enter confirm password",
				equalTo : "Please enter same confirm password"
			}
		}
	});
});
</script>