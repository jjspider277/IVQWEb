<?php
//Category Listing...
$cat_fields = array("dic_id","dic_name");
$cat_where  = "dic_status = 'Active'";
$catRes 	= $db->selectData(TBL_DIRECTORY_CATEGORY,$cat_fields,$cat_where,$extra="",2);

//State Listing...
$state_fields = array("sta_id","sta_name");
$state_where  = "sta_status = 'Active'";
$staRes 	= $db->selectData(TBL_STATE,$state_fields,$state_where,$extra="",2);

//Country Listing...
$con_fields = array("con_id","con_name");
$con_where  = "con_status = 'Active'";
$conRes 	= $db->selectData(TBL_COUNTRY,$con_fields,$con_where,$extra="",2);

#################################################################
$SECTION_FIELD_PREFIX='dir_';
$SECTION_AUTO_ID=$_REQUEST[$SECTION_FIELD_PREFIX.'id'];
$SECTION_WHERE=$SECTION_FIELD_PREFIX."id=".$SECTION_AUTO_ID."";
$SECTION_VIEW_PAGE = ADM_VIEW_DIRECTORY;
$SECTION_MANAGE_PAGE=ADM_MANAGE_DIRECTORY;
$SECTION_TABLE=TBL_DIRECTORY;
$SECTION_NAME='Directory';

$uploadFILEURL = UPLOAD_DIR_DIRECTORY_FILE;
$uploadFILEWWW = UPLOAD_WWW_DIRECTORY_FILE;

#################################################################
if($_REQUEST['method'] != "")
    $action =  $_REQUEST['method'];
else if($_REQUEST['method'] != "")
    $action =  $_REQUEST['method'];

if(isset($_POST)){
    $data = $_POST;
}

if ($_POST['dir_submit'] == 'Save Directory' || $data['dir_addNew'] == 'Add New') {

    $dir_logo_file_name= $_FILES['dir_logo_file']['name'];

    $status 		                                    =	"Active";
    $add_values[$SECTION_FIELD_PREFIX.'dic_id'] 		= trim($_POST["dir_cat"]);
    $add_values[$SECTION_FIELD_PREFIX.'name'] 			= trim($_POST["dir_name"]);
    $add_values[$SECTION_FIELD_PREFIX.'address'] 		= trim($_POST["dir_address"]);
    $add_values[$SECTION_FIELD_PREFIX.'city'] 			= trim($_POST["dir_city"]);
    $add_values[$SECTION_FIELD_PREFIX.'sta_id'] 		= trim($_POST["dir_state"]);
    $add_values[$SECTION_FIELD_PREFIX.'zip'] 			= trim($_POST["dir_zip"]);
    $add_values[$SECTION_FIELD_PREFIX.'con_id'] 		= $_POST["dir_country"];
    $add_values[$SECTION_FIELD_PREFIX.'office_phone'] 	= trim($_POST["dir_office_phone"]);
    //$add_values[$SECTION_FIELD_PREFIX.'email'] 			= trim($_POST["dir_email"]);
    $add_values[$SECTION_FIELD_PREFIX.'website'] 		= trim($_POST["dir_website"]);

    $add_values[$SECTION_FIELD_PREFIX.'featured'] 		= trim($_POST["dir_featured"]);
    $add_values[$SECTION_FIELD_PREFIX.'private'] 		= trim($_POST["dir_private"]);
    $add_values[$SECTION_FIELD_PREFIX.'t_c'] 		    = trim($_POST["dir_t_c"]);
    $add_values[$SECTION_FIELD_PREFIX.'t_c_description'] = trim($_POST["dir_t_c_description"]);


    if($action != 'Update'){
        $add_values[$SECTION_FIELD_PREFIX.'logo_file'] 		= trim($_POST["dir_logo_file"]);

        $add_values[$SECTION_FIELD_PREFIX.'status'] 		= $status;
        $add_values[$SECTION_FIELD_PREFIX . 'created_id'] 	= getMemberSessionId();
        $add_values[$SECTION_FIELD_PREFIX . 'created_date'] = $today_date;
        $GPDetail_result = $db->insertData($SECTION_TABLE, $add_values);

        $Dir_Id=$GPDetail_result;
        if($dir_logo_file_name!="") {
            if(is_dir($uploadFILEURL.$Dir_Id) == false) {
                mkdir($uploadFILEURL.$Dir_Id);
            }
            $bus_file = uploadIVA($uploadFILEURL.$Dir_Id."/",$_FILES['dir_logo_file']);
            if($bus_file=="type_err") {
                $_SESSION["err_file"]="Please select only image videos and audio file";
                $db->delete("delete from ".$SECTION_TABLE." where dir_id=".$Dir_Id);

                $_SESSION['add_detail_error'][]=$_POST;
                $URL = getMemberURL(MEB_CREATE_GROUP_1,$action);
                redirect($URL);
                exit;
            } else if($bus_file=="size_err") {
                $_SESSION["err_file"]="Please upload file less than 32MB file size!";
                $db->delete("delete from ".$SECTION_TABLE." where dir_id=".$Dir_Id);

                $_SESSION['add_detail_error'][]=$_POST;
                $URL = getMemberURL(MEB_CREATE_GROUP_1,$action);
                redirect($URL);
                exit;
            } else {
                $bus_logo_file = $bus_file;
                $updateData[$SECTION_FIELD_PREFIX.'logo_file'] 	= $bus_logo_file;
                $SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$Dir_Id;
                $db->updateData($SECTION_TABLE, $updateData, $SECTION_WHERE);
            }
        }
        $_SESSION['msg']  =  "Group has been inserted successfully.";
    } else {
        //edit_bus_logo_file
        if($_POST["dir_logo_file"]!='')
            $add_values[$SECTION_FIELD_PREFIX.'logo_file'] 		= trim($_POST["dir_logo_file"]);
        else if($_REQUEST['edit_bus_logo_file']=='')
            $add_values[$SECTION_FIELD_PREFIX.'logo_file'] 		= '';

        $add_values[$SECTION_FIELD_PREFIX . 'updated_id'] 			= getMemberSessionId();
        $add_values[$SECTION_FIELD_PREFIX . 'updated_date'] 		= $today_date;
        $SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$SECTION_AUTO_ID;
        $GPDetail_result = $db->updateData($SECTION_TABLE, $add_values, $SECTION_WHERE);

        $Dir_Id=$SECTION_AUTO_ID;
        if($dir_logo_file_name!="")
        {
            if(is_dir($uploadFILEURL.$Dir_Id) == false)
            {
                mkdir($uploadFILEURL.$Dir_Id);
            }
            $bus_file = uploadIVA($uploadFILEURL.$Dir_Id."/",$_FILES['dir_logo_file']);
            if($bus_file=="type_err")
            {
                $_SESSION["err_file"]="Please select only image videos and audio file";
                $db->delete("delete from ".$SECTION_TABLE." where dir_id=".$Dir_Id);
                $_SESSION['add_detail_error'][]=$_POST;
                $URL = getMemberURL(MEB_CREATE_GROUP_1,$action);
                redirect($URL);
                exit;
            }
            else if($bus_file=="size_err")
            {
                $_SESSION["err_file"]="Please upload file less than 32MB file size!";
                $db->delete("delete from ".$SECTION_TABLE." where dir_id=".$Dir_Id);
                $_SESSION['add_detail_error'][]=$_POST;
                $URL = getMemberURL(MEB_CREATE_GROUP_1,$action);
                redirect($URL);
                exit;
            }
            else
            {
                $bus_logo_file = $bus_file;
                $updateData[$SECTION_FIELD_PREFIX.'logo_file'] 	= $bus_logo_file;
                $SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$Dir_Id;
                $db->updateData($SECTION_TABLE, $updateData, $SECTION_WHERE);
            }
        }
        $_SESSION['msg']  =   "Group has been updated successfully.";
    }

    if($data['dir_addNew'] == 'Add New') {
        $URL = getMemberURL(MEB_HOME);
        redirect($URL);
        exit;
    } else {
        $URL = getMemberURL(MEB_HOME);
        redirect($URL);
        exit;
    }
}
if($action=='Edit') {
    $list_query = getSelectList($SECTION_TABLE,$SECTION_FIELD_PREFIX);
    $list_query .= "AND ".$SECTION_FIELD_PREFIX."id = '".$SECTION_AUTO_ID."'";
    $result_query = $db->select($list_query);
    $dir_cat		=	$result_query[0][$SECTION_FIELD_PREFIX."dic_id"];
    $dir_name		=	$result_query[0][$SECTION_FIELD_PREFIX."name"];
    $dir_address	=	$result_query[0][$SECTION_FIELD_PREFIX."address"];
    $dir_city		=	$result_query[0][$SECTION_FIELD_PREFIX."city"];
    $dir_state		=	$result_query[0][$SECTION_FIELD_PREFIX."sta_id"];
    $dir_zip		=	$result_query[0][$SECTION_FIELD_PREFIX."zip"];
    $dir_country	=	$result_query[0][$SECTION_FIELD_PREFIX."con_id"];
    $dir_office_phone	=	$result_query[0][$SECTION_FIELD_PREFIX."office_phone"];
    $dir_email		=	$result_query[0][$SECTION_FIELD_PREFIX."email"];
    $dir_website	=	$result_query[0][$SECTION_FIELD_PREFIX."website"];
    $dir_logo_file	=	$result_query[0][$SECTION_FIELD_PREFIX."logo_file"];

    $dir_featured	=	$result_query[0][$SECTION_FIELD_PREFIX."featured"];
    $dir_private	=	$result_query[0][$SECTION_FIELD_PREFIX."private"];
    $dir_t_c	    =	$result_query[0][$SECTION_FIELD_PREFIX."t_c"];
    $dir_t_c_description =	$result_query[0][$SECTION_FIELD_PREFIX."t_c_description"];


    //$action = "Update";
    $action_url = getAdminURL($SECTION_MANAGE_PAGE,$action,$SECTION_WHERE);
}

if($action=='')
{
    $status = "Active";
    //$action = "Add";
    $action_url = getAdminURL($SECTION_MANAGE_PAGE,$action);
    if($_SESSION['add_detail_error'][0]!="")
    {
        foreach($_SESSION['add_detail_error'][0] as $key => $value)
        {
            $result_query[0][$key]=stripslashes($value);
        }
        $dir_cat		=	$result_query[0]["dir_cat"];
        $dir_name		=	$result_query[0]["dir_name"];
        $dir_address	=	$result_query[0]["dir_address"];
        $dir_city		=	$result_query[0]["dir_city"];
        $dir_state		=	$result_query[0]["dir_state"];
        $dir_zip		=	$result_query[0]["dir_zip"];
        $dir_country	=	$result_query[0]["dir_country"];
        $dir_office_phone	=	$result_query[0]["dir_office_phone"];
        $dir_email		=	$result_query[0]["dir_email"];
        $dir_website	=	$result_query[0]["dir_website"];
        $dir_logo_file	=	$result_query[0]["dir_logo_file"];
        $dir_featured	=	$result_query[0]["dir_featured"];
        $dir_private	=	$result_query[0]["dir_private"];
        $dir_t_c	    =	$result_query[0]["dir_t_c"];
        $dir_t_c_description = $result_query[0]["dir_t_c_description"];

        unset($_SESSION['add_detail_error']);
    }
}

?>
<div class="container search-group-section">
    <div class="row">
        <div class="step-list">
            <ul>
                <li><a href="javascript:void(0);" class="top_step_1 active">Step <span>1</span></a></li>
                <li><a href="javascript:void(0);" class="top_step_2">Step <span>2</span></a></li>
            </ul>
        </div>
    </div>
</div>

<div class="container step-page-section">
    <div class="row">
        <form name="manage_directory" enctype="multipart/form-data" method="post" action="" id="manage_directory">

            <?php if($action=='Edit') { ?>
                <input type="hidden" name="dir_submit" value="Save Directory">
                <input type="hidden" name="method" value="Update">
                <input type="hidden" name="dir_id" value="<?php echo $SECTION_AUTO_ID; ?>">

            <?php } else { ?>
                <input type="hidden" name="dir_submit" value="Save Directory">
            <?php } ?>

            <section data-step="0">
                <div class="form-main next_step_main_1">

                    <label>Category *</label>
                    <select class="custom-select" name="dir_cat" id="dir_cat" alt="Type to search directory category">
                        <option value="">Select Category</option>
                        <?php
                        for($i=0;$i<count($catRes);$i++)
                        {
                            if($catRes[$i]['dic_id']==$dir_cat) { $select="selected='selected'"; } else { $select=""; }
                            ?>
                            <option value="<?php echo $catRes[$i]['dic_id']; ?>" <?php echo $select; ?> ><?php echo $catRes[$i]['dic_name']; ?></option>
                            <?php
                        }
                        ?>
                    </select>

                    <label>Group Name *</label>
                    <input type="text" name="dir_name" id="dir_name" value="<?php echo $dir_name;?>"/>

                    <label>Logo</label>
                    <input tabindex="20" type="file" name="dir_logo_file" id="dir_logo_file" />
                    <?php
                        if($_SESSION["err_file"]!="") {
                            echo '<label class="error">'.$_SESSION["err_file"].'</label>';
                            $_SESSION["err_file"]="";
                        }
                        if($action=="Edit" && $dir_logo_file!="") {
                            echo "<div class='msg_filesddd'>";
                            echo "<a id='vediosPlay' href='view_file_dir.php?filename=".$dir_logo_file."&dir_id=".$SECTION_AUTO_ID."' style='margin-top:5px;' class='fancybox fancybox.ajax' title='View' data-fancybox-type='ajax' >".$dir_logo_file."</a>";
                            echo "&nbsp;&nbsp;&nbsp;<a style='margin-top:5px;' id='removeLink' onclick='javascript:removeFile(edit_bus_logo_file);'>Remove</a>";
                            echo "<input type='hidden' id='edit_bus_logo_file' name='edit_bus_logo_file' value='".$dir_logo_file."' />";
                            echo "</div>";
                        }
                    ?>

                    <?php
                        $checked='';
                        if($dir_featured=='yes')
                            $checked='checked="checked"';

                        $checked_p='';
                        if($dir_private=='yes')
                            $checked_p='checked="checked"';

                        $checked_t_c='';
                        $dcr_show='none';
                        if($dir_t_c=='yes') {
                            $checked_t_c = 'checked="checked"';
                            $dcr_show='block';
                        }
                    ?>

                    <div class="checkbox">
                        <input type="checkbox" <?php echo $checked; ?> value="yes" name="dir_featured" id="dir_featured" data-label="Featured groups" />
                        <label class="tooltip" for="dir_featured" title="Select this option to have you group featured on the App main page."><span class="check-box-lable-text">Featured</span></label>

                        <input type="checkbox" <?php echo $checked_p; ?> value="yes" name="dir_private" id="dir_private" data-label="Private Group" />
                        <label class="tooltip" title="This option allows you to approve all group membership." for="dir_private"><span class="check-box-lable-text">Private Group</span></label>

                        <input type="checkbox" <?php echo $checked_t_c; ?> value="yes" name="dir_t_c" id="dir_t_c" data-label="T & C" />
                        <label class="tooltip" title="Display T & C’s" for="dir_t_c"><span class="check-box-lable-text" style="margin-right: 0;">Display T & C’s</span></label>
                    </div>

                    <div id="t_c_notes_div" style="display: <?php echo $dcr_show; ?>;">
                        <label>T & C’s Notes *</label>
                        <textarea name="dir_t_c_description" id="dir_t_c_description"><?php echo trim($dir_t_c_description);?></textarea>
                    </div>

                    <div class="step-button-row">
                        <div class="step-button"><a href="javascript:void(0);" class="next_step_1">Next</a></div>
                    </div>
                </div>
            </section>

            <section data-step="0">
                <div class="form-main next_step_main_2" style="display: none;">

                    <label>Address *</label>
                    <input type="text" name="dir_address" id="dir_address" value="<?php echo $dir_address;?>" />

                    <label>City *</label>
                    <input type="text" name="dir_city" id="dir_city" value="<?php echo $dir_city;?>" />

                    <label>State *</label>
                    <select class="custom-select" name="dir_state" id="dir_state" alt="Type to search state">
                        <option value="">Select State</option>
                        <?php for($i=0;$i<count($staRes);$i++) { if($staRes[$i]['sta_id']==$dir_state) { $select="selected='selected'"; } else { $select=""; } ?>
                            <option value="<?php echo $staRes[$i]['sta_id']; ?>" <?php echo $select; ?> ><?php echo $staRes[$i]['sta_name']; ?></option>
                        <?php } ?>
                    </select>

                    <label>Postal Code *</label>
                    <input type="text" name="dir_zip" id="dir_zip" value="<?php echo $dir_zip;?>" />

                    <label>Country *</label>
                    <select class="custom-select" name="dir_country" id="dir_country" alt="Type to search country">
                        <?php for($i=0;$i<count($conRes);$i++) { if($conRes[$i]['con_id']==$dir_country) { $select="selected='selected'"; } else { $select=""; } ?>
                            <option value="<?php echo $conRes[$i]['con_id']; ?>" <?php echo $select; ?> ><?php echo $conRes[$i]['con_name']; ?></option>
                        <?php } ?>
                    </select>

                    <label>Phone *</label>
                    <input type="text" name="dir_office_phone" id="dir_office_phone" value="<?php echo $dir_office_phone;?>"/>

                    <label>Website</label>
                    <input type="text" name="dir_website" id="dir_website" value="<?php echo $dir_website;?>"/>

                    <div class="step-button-row">
                        <div class="step-button-previous"><a class="pre_step" href="javascript:void(0);">Previous</a></div>
                        <div class="step-button-1"><a href="javascript:void(0);" onclick="$('#manage_directory').submit();">Save</a></div>
                    </div>
                </div>
            </section>

        </form>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $(".custom-select").each(function(){
            $(this).wrap("<span class='select-wrapper'></span>");
            $(this).after("<span class='holder'></span>");
        });
        $(".custom-select").change(function(){
            var selectedOption = $(this).find(":selected").text();
            $(this).next(".holder").text(selectedOption);
        }).trigger('change');

        $.validator.addMethod("phoneValidate", function(value, element) {
            return this.optional(element) || /^[0-9\-\+\,\.\)\(\s]+$/i.test(value);
        }, "Only numbers,-,+ and , allowed");

        $("#manage_directory").validate({
            rules: {
                dir_cat : {
                    required : true
                },
                dir_name : {
                    required : true
                },
                dir_logo_file: {
                    accept: "image/*"
                },
                dir_address : {
                    required : true
                },
                dir_city : {
                    required : true
                },
                dir_state : {
                    required : true
                },
                dir_zip : {
                    required : true,
                    digits : true
                },
                dir_office_phone : {
                    required : true,
                    phoneValidate : true
                },
                /*dir_country : {
                    required : true
                },
                dir_email : {
                    required : true,
                    email : true
                },
                 dir_website : {
                 required : true,
                 url : true
                 }*/
            },
            messages: {
                dir_cat : {
                    required : "Please select category"
                },
                dir_name : {
                    required : "Please enter name"
                },
                dir_address : {
                    required : "Please enter address"
                },
                dir_city : {
                    required : "Please enter city"
                },
                dir_state : {
                    required : "Please select state"
                },
                dir_zip : {
                    required : "Please enter zip code",
                    digits: "Please enter only numbers"
                },
                dir_country : {
                    required : "Please select country"
                },
                dir_office_phone : {
                    required : "Please enter phone no.",
                    phoneValidate : "Please enter valid phone no."
                },
                /*
                dir_email : {
                    required : "Please enter email address",
                    email : "Please enter valid email address"
                },
                 dir_website : {
                 required : "Please enter website URL",
                 url : "Please enter valid website URL"
                 }*/
            }
        });
    });
    
    $("#dir_office_phone").mask("999-999-9999");

    $(document).on('click', '.next_step_1, .top_step_2', function() {
        erro='';

        //$('.next_step_1').removeClass('error');

        $('html,body').animate({scrollTop: $('.step-list').offset().top-10},'slow');
        if($('#dir_cat').val()=='') {
            $('#dir_cat').parent().addClass('error_bor');
            erro='yes';
        }
        if($('#dir_name').val()=='') {
            $('#dir_name').addClass('error');
            erro='yes';
        }

        if($('#dir_logo_file').hasClass('error')) {
            erro='yes';
        }

        if ($('#dir_t_c').prop('checked')) {
            if($('#dir_t_c_description').val()==''){
                $('#dir_t_c_description').addClass('error');
                erro='yes';
            }
        }

        if(erro=='yes') {
            return false;
        } else {

            $('.top_step_1').removeClass('active');
            $('.top_step_2').addClass('active');

            $('.next_step_main_1').hide();
            $('.next_step_main_2').show();
        }
    });

    $(document).on('change', '#dir_t_c', function(){
        if ($('#dir_t_c').prop('checked')) {
            $('#dir_t_c_description').removeClass('error');
            $('#t_c_notes_div').slideDown();
        } else {
            $('#t_c_notes_div').slideUp();
        }
    });

    $(document).on('click', '.pre_step, .top_step_1', function(){
        $('html,body').animate({scrollTop: $('.step-list').offset().top-10},'slow');

        $('.top_step_1').addClass('active');
        $('.top_step_2').removeClass('active');

        $('.next_step_main_1').show();
        $('.next_step_main_2').hide();
    });

    function removeFile($fileName) {
        $hiddenId = $fileName.id
        $("#vediosPlay").remove();
        $("#removeLink").remove();
        $('#'+$hiddenId).val('');
        $('#remove_image').val('true');
        $( "#bus_submit" ).trigger( "click" );
    }
</script>