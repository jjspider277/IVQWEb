

<div class="container group-setion">
    <div class="row">
        <div class="subRow default_content">
            <h1>Privacy Policy</h1>

            <p>Thanks for using IVQ Mobile! Here we describe how we collect, use and handle your information when you use our websites, software and services ("Services").<p>

            <h3>What & Why</h3>
            <p>We collect and use the following information to provide, improve and protect our Services:</p>

            <ul>
                <li>
                    <b>Account</b>
                    <p>We collect, and associate with your account, information like your name, email address, phone number, payment info, and physical address. Some of our services let you access your accounts and your information with other service providers.</p>
                </li>

                <li>
                    <b>Services</b>
                    <p>When you use our Services, we store, process and transmit your files (including stuff like your photos, data, and emails) and information related to them (for example, location tags in photos). If you give us access to your contacts, we'll store those contacts on our servers for you to use. This will make it easy for you to do things like share your stuff, send emails, and invite others to use the Services.</p>
                </li>

                <li>
                    <b>Usage</b>
                    <p>We collect information from and about the devices you use to access the Services. This includes things like IP addresses, the type of browser and device you use, the web page you visited before coming to our sites, and identifiers associated with your devices. Your devices (depending on their settings) may also transmit location information to the Services.</p>
                </li>

                <li>
                    <b>Cookies and other technologies</b>
                    <p>We use technologies like cookies and pixel tags to provide, improve, protect and promote our Services. For example, cookies help us with things like remembering your username for your next visit, understanding how you are interacting with our Services, and improving them based on that information. You can set your browser to not accept cookies, but this may limit your ability to use the Services. If our systems receive a DNT:1 signal from your browser, we'll respond to that signal as outlined here.</p>
                </li>

            </ul>

            <h3>With whom</h3>
            <p>We may share information as discussed below.</p>

            <ul>
                <li>
                    <b>Other users</b>
                    <p>Our Services display information like your name and email address to other users in places like your user profile and sharing notifications. Certain features let you make additional information available to other users.</p>
                </li>

                <li>
                    <b>Other applications</b>
                    <p>You can also give third parties access to your information. Just remember that their use of your information will be governed by their privacy policies and terms.</p>
                </li>

                <li>
                    <b>Law & Order</b>
                    <p>We may disclose your information to third parties if we determine that such disclosure is reasonably necessary to (a) comply with the law; (b) protect any person from death or serious bodily injury; (c) prevent fraud or abuse of Dropbox or our users; or (d) protect Dropbox's property rights.</p>
                </li>
            </ul>
            <p>Stewardship of your data is critical to us and a responsibility that we embrace. We believe that our users' data should receive the same legal protections regardless of whether it's stored on our services or on their home computer's hard drive. </p>

            <h3>Changes</h3>
            <p>If we are involved in a reorganization, merger, acquisition or sale of our assets, your information may be transferred as part of that deal. We will notify you (for example, via a message to the email address associated with your account) of any such deal and outline your choices in that event.</p>
            <p>We may revise this Privacy Policy from time to time, and will post the most current version on our website. If a revision meaningfully reduces your rights, we will notify you.</p>

            <h3>Contact</h3>
            <p>Have questions or concerns about IVQ Mobile, our Services and privacy? Contact us at <a href="http://www.intelli-touch.com" target="_blank">www.intelli-touch.com.</a></p>
        </div>
	</div>
</div>


