<?php
$app_apk = '';
$app_ios = '';

$dir_fields = array("*");
$dir_where  = "";
$dirRes 	= $db->selectData(TBL_APP_SETTING,$dir_fields,$dir_where,$extra="",2);

if(count($dirRes)>0) {
	foreach($dirRes as $dirdata) {

		if($dirdata['meta'] == 'apk') {
			$app_apk = $dirdata['value'];
		}

		if($dirdata['meta'] == 'ipa') {
			$app_ios = $dirdata['value'];
		}

        if($dirdata['meta'] == 'app_title') {
            $app_title = $dirdata['value'];
        }

        if($dirdata['meta'] == 'app_description') {
            $app_description = $dirdata['value'];
        }
	}
}

?>
<style>
	.content-section{
		background: #ffffff none repeat scroll 0 0;
		border-bottom: 1px solid #efefef;
	}
</style>
<div class="page-title" style="display: none;">
	<div class="row">
		<?php echo $app_title; ?>
	</div>
</div>

<div class="content-section">
	<div class="row">
		<div class="left-image-section-1">
			<img src="<?php echo ROOT_WWW; ?>/members/images/mobille-img.png" alt="">
		</div>
		<div class="right-form-section-1">
			<?php echo $app_description; ?>

			<div class="call-section">
				<div class="main-call main-call-fist">
					<div class="call-image"><img src="<?php echo ROOT_WWW; ?>/members/images/advetise.jpg"></div>
					<h3>Advertise</h3>
					<p>your services</p>
				</div>
				<div class="main-call">
					<div class="call-image"><img src="<?php echo ROOT_WWW; ?>/members/images/reach.jpg"></div>
					<h3>Reach</h3>
					<p>new members</p>
				</div>
				<div class="main-call main-call-last">
					<div class="call-image"><img src="<?php echo ROOT_WWW; ?>/members/images/broadcast.jpg"></div>
					<h3>Broadcast</h3>
					<p>messages and coupons</p>
				</div>

				<div class="google-play-section-1">
					<div class="apple-button"><a target="_blank" href="<?php echo $app_ios; ?>"><img src="<?php echo ROOT_WWW; ?>/images/app-store.jpg"></a></div>
					<div class="google-play-button"><a target="_blank" href="<?php echo $app_apk; ?>"><img src="<?php echo ROOT_WWW; ?>/images/google-play.jpg"></a></div>
				</div>

			</div>

		</div>
	</div>
</div>


<div class="app_screnshot">
    <div class="row">
        <div class="minous_col">
            <?php
                //ROOT_WWW
                $root = 'http://b2bms.net/';
                $url_video_1 = 'file='.$root.'app_new/video/Version1_e_BusinessCards.mp4&volume=80&amp;bufferlength=5&amp;logo.hide=false&amp;&;title=sleeve_length&amp;linktarget=_self';
                $url_video_2 = 'file='.$root.'app_new/video/Version1SEARCHGROUPS.mp4&volume=80&amp;bufferlength=5&amp;logo.hide=false&amp;&;title=sleeve_length&amp;linktarget=_self';
                $url_video_3 = 'file='.$root.'app_new/video/Version1MobileCoupond_Group_Messages.mp4&volume=80&amp;bufferlength=5&amp;logo.hide=false&amp;&;title=sleeve_length&amp;linktarget=_self';
                $url_video_4 = 'file='.$root.'app_new/video/Version1Analytics_Survey.mp4&volume=80&amp;bufferlength=5&amp;logo.hide=false&amp;&;title=sleeve_length&amp;linktarget=_self';
                $url_video_5 = 'file='.$root.'app_new/video/Version1MobileCoupond_Group_Messages.mp4&volume=80&amp;bufferlength=5&amp;logo.hide=false&amp;&;title=sleeve_length&amp;linktarget=_self';
                $url_video_6 = 'file='.$root.'app_new/video/Version1Analytics_Survey.mp4&volume=80&amp;bufferlength=5&amp;logo.hide=false&amp;&;title=sleeve_length&amp;linktarget=_self';
            ?>

            <div class="three_col1">
                <a href="javascript:void(0);" class="open_box" data_url="<?php echo $url_video_1; ?>">
                    <div class="app_sc ">
                        <img src="images/ExchangeCards.png" />
                        <div class="title_box">
                            <div class="ds_table">
                                <div class="ds_cell">
                                    <i class="plus_icon"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="three_col1">
                <a href="javascript:void(0);" class="open_box" href="#popup_litebox" data_url="<?php echo $url_video_2; ?>">
                    <div class="app_sc">
                        <img src="images/JoinGroups.png" />
                        <div class="title_box">
                            <div class="ds_table">
                                <div class="ds_cell">
                                    <i class="plus_icon"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="three_col1">
                <a href="javascript:void(0);" class="open_box" href="#popup_litebox" data_url="<?php echo $url_video_3; ?>">
                    <div class="app_sc">
                        <img src="images/PromoteBusiness.png" />
                        <div class="title_box">
                            <div class="ds_table">
                                <div class="ds_cell">
                                    <i class="plus_icon"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="three_col1">
                <a href="javascript:void(0);" class="open_box" href="#popup_litebox" data_url="<?php echo $url_video_4; ?>">
                    <div class="app_sc">
                        <img src="images/AnalyseResults.png" />
                        <div class="title_box">
                            <div class="ds_table">
                                <div class="ds_cell">

                                    <i class="plus_icon"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="three_col1">
                <a href="javascript:void(0);" class="open_box" href="#popup_litebox" data_url="<?php echo $url_video_5; ?>">
                    <div class="app_sc">
                        <img src="images/BrosdcastNews.png" />
                        <div class="title_box">
                            <div class="ds_table">
                                <div class="ds_cell">

                                    <i class="plus_icon"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <div class="three_col1">
                <a href="javascript:void(0);" class="open_box" href="#popup_litebox" data_url="<?php echo $url_video_6; ?>">
                    <div class="app_sc">
                        <img src="images/Surveylients.png" />
                        <div class="title_box">
                            <div class="ds_table">
                                <div class="ds_cell">

                                    <i class="plus_icon"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <a href="#popup_litebox" class="group" style="display: none;"></a>
            <div id="popup_litebox" class="popup_litebox" style="display: none;">
                <div class="close_box" style="display: none;">
                    <span class="row_l"></span>
                    <span class="row_line2"></span>
                </div>

                <div class="ds_table ">

                    <object width="100%" height="500px" data="./player.swf" type="application/x-shockwave-flash">
                        <param value="opaque" name="wmode">
                        <param value="always" name="allowscriptaccess">
                        <param value="all" name="allownetworking">
                        <param value="" id="flashvars" name="flashvars">
                        <param name="allowFullScreen" value="true" />
                    </object>
                    
                    <div style="display: none;">
                        <div class="view_iamge ds_cell">
                            <img src="images/mobille-img.png" />
                        </div>
                        <div class="cont_detail ds_cell">
                            <h1>Join the IVQ community today</h1>
                            <p>Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod<br>tempor incididunt ut labore et dolore magna aliqua.</p>
                            <p>Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod<br>tempor incididunt ut labore et dolore magna aliqua.</p>
                            <p>Lorem ipsum dolor sit amet consectetur adipiscing elit.</p>
                        </div>
                    </div>
                    
                </div>

            </div>

            <div class="clr"></div>
        </div>

    </div>
</div>

<script>
    $(document).on('click','.open_box',function(){
        data_url = $(this).attr('data_url');
        if(data_url!='') {
            $('#flashvars').val(data_url);
            $('.group').trigger('click');
        }
    });

    $(document).ready(function() {
        /* Apply fancybox to multiple items */

        $("a.group").fancybox({
            'transitionIn'	:	'elastic',
            'transitionOut'	:	'elastic',
            'speedIn'		:	600,
            'speedOut'		:	200,
            'overlayShow'	:	false,
            'autoSize'      :   false,
             beforeShow     :   function(){
             },
             helpers: {
                overlay: {
                    locked: true
                }
             }
        });

    });
</script>