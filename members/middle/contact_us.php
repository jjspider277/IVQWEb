<?php
if(isset($_POST['contact_sub']) && $_POST['contact_sub']=='yes') {
    $name = trim($_POST['name']);
    $email = trim($_POST['email']);
    $phone = trim($_POST['phone']);
    $comment = trim($_POST['comment']);

    if($email!='' && $name!='') {

        $to = CONTACT_EMAIL;
        $subject = "Contact";


        $message = '<html>
			<head>
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <meta content="width=1000" name="viewport">
			  <title>Conatct</title>
			</head>
			<body style="margin:0px;">
    			<table style="background-color:#151515" bgcolor="#151515" cellpadding="0" cellspacing="0" width="100%">
      			<tbody>
        		<tr>
          			<td style="text-align:left" align="center">
            			<table id="topMessageWrapper" style="width:600px;background-color:#151515;color:#999999;font-family:arial;font-size:13px;padding-bottom:0px;padding-top:0px;margin:0 auto;" align="center" bgcolor="#ffffff" cellpadding="0" cellspacing="0" width="600">
              			<tbody>
                		<tr>
                  			<td>
                    			<table border="0" cellpadding="0" cellspacing="0" height="100%" width="600">
                      			<tbody>
                        		<tr>
                          			<td style="color:#999999;font-family:arial;font-size:13px;">
                            			<table border="0" cellpadding="0" cellspacing="0" width="100%">
                              			<tbody>
                                		<tr>
                                  			<td style="color:#999999;font-family:arial;font-size:13px;" valign="top">
		                                    <div style="">
												<table style="word-wrap:break-word;table-layout:fixed;" border="0" cellpadding="0" cellspacing="0" width="100%">
		                                        <tbody>
		                                        <tr>
													<td style="color:#999999;font-family:arial;font-size:13px;">
														<p style="margin-bottom:0;margin-left:0;margin-right:0;margin-top:0;padding:0;">
														<img alt="" src="'.MEMBER_WWW.'/images/jpeg_002.png" style="display:block;" usemap="#Map" border="0">
														</p>
		                                            </td>
		                                        </tr>
		                                        </tbody>
		                                      	</table>
		                                    </div>
                                  			</td>
                                		</tr>
                              			</tbody>
                            			</table>
                          			</td>
                        		</tr>
                        		<tr>
                          			<td style="background-color:#ffffff;color:#999999;font-family:arial;font-size:13px;" bgcolor="#ffffff">
                            		<table border="0" cellpadding="0" cellspacing="0" width="100%">
                              		<tbody>
                                	<tr>
                                  		<td style="background-color:#ffffff;padding-bottom:0px;padding-left:0px;padding-right:0px;color:#999999;font-family:arial;font-size:13px;" bgcolor="#ffffff" valign="top">
                                  		<div style="">
                                      	<table style="word-wrap:break-word;table-layout:fixed;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                        <tr>
											<td style="color:#2f2d2e;font-family:arial;font-size:13px; padding:33px;">
                                            <p style="margin-bottom:0;margin-left:0;margin-right:0;margin-top:0;padding:0;"><span style="color:#0e70b9; font-size:21px;"><strong>Contact</strong></span></p>
                                            </td>
                                        </tr>
                                        <tr>
											<td style="color:#2f2d2e;font-family:arial;font-size:13px; padding:0px 33px 33px;">
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td align="left" valign="top" style="color:#2f2d2e;font-family:arial;font-size:13px; line-height:24px; padding:0px 0px;"><strong>Name:</strong></td>
												<td align="left" valign="top" style="color:#2f2d2e;font-family:arial;font-size:13px; line-height:24px; padding:0px 0px;">'.$name.'</td>
											</tr>
											<tr>
												<td align="left" valign="top" style="color:#2f2d2e;font-family:arial;font-size:13px; line-height:24px; padding:0px 0px;"><strong>Email:</strong></td>
                                              	<td align="left" valign="top" style="color:#2f2d2e;font-family:arial;font-size:13px; line-height:24px; padding:0px 0px;">'.$email.'</td>
                                            </tr>
                                            <tr>
												<td align="left" valign="top" style="color:#2f2d2e;font-family:arial;font-size:13px; line-height:24px; padding:0px 0px;"><strong>Phone:</strong></td>
                                              	<td align="left" valign="top" style="color:#2f2d2e;font-family:arial;font-size:13px; line-height:24px; padding:0px 0px;">'.$phone.'</td>
                                            </tr>
                                            <tr>
												<td align="left" valign="top" style="color:#2f2d2e;font-family:arial;font-size:13px; line-height:24px; padding:0px 0px;"><strong>Comment:</strong></td>
                                              	<td align="left" valign="top" style="color:#2f2d2e;font-family:arial;font-size:13px; line-height:24px; padding:0px 0px;">'.$comment.'</td>
                                            </tr>
                                            </table>
                                            </td>
                                        </tr>
										<tr>
                      						<td style="padding:0px 33px 33px;">
                      							<p style="color:#333; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:17px; margin:0px;"><strong>Thanks,</strong></p>
                      							<p style="color:#666; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:17px; margin:0px;">IVQMobile,</p>
                      							<p style="color:#666; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:17px; margin:0px;"><a target="_blank" style="color:#0E70B9; text-decoration:underline;" href="'.MEMBER_WWW.'">'.MEMBER_WWW.'</a></p>
                      						</td>
                    					</tr>
                                        </tbody>
                                      	</table>
                                    	</div>
                                  		</td>
                               		</tr>
                              		</tbody>
                            		</table>
                          			</td>
                        		</tr>
                        		<tr>
                          			<td style="color:#999999;font-family:arial;font-size:13px;">
                            		<table border="0" cellpadding="0" cellspacing="0" width="100%">
                              		<tbody>
                                	<tr>
                                  		<td style="color:#999999;font-family:arial;font-size:13px;" valign="top">
                                    	<div>

                                    	</div>
                                  		</td>
                                	</tr>
                              		</tbody>
                            		</table>
                          			</td>
                        		</tr>
                        		<tr>
                          			<td style="color:#999999;font-family:arial;font-size:13px;">
                            		<table border="0" cellpadding="0" cellspacing="0" width="100%">
                              		<tbody>
                                	<tr>
                                  		<td style="color:#999999;font-family:arial;font-size:13px;" valign="top">
                                    	<div>
                                      		<table style="word-wrap:break-word;table-layout:fixed;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        	<tbody>
                                          	<tr>
                                            	<td style="color:#999999;font-family:arial;font-size:13px;" align="center">
                                              		<table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                	<tbody>
                                                  	<tr>
                                                    	<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#FFF; padding-top:18px; padding-bottom:18px;" align="center">
                                                      	<span style="color:#0e70b9">© '.date("Y").'</span> IVQMobile. All right reserved Intelli-Touch.</td>
                                                  	</tr>
                                                	</tbody>
                                              		</table>
                                            	</td>
                                          	</tr>
                                        	</tbody>
                                      		</table>
                                    	</div>
                                  	</td>
                                </tr>
                              	</tbody>
                            	</table>
                          	</td>
                        </tr>
                    	</tbody>
                    	</table>
                	</td>
                	</tr>
              		</tbody>
            		</table>
          		</td>
        		</tr>
      			</tbody>
    			</table>
			</body>
			</html>';

        ################## Mail To Admin in mail function########################
        $mail_flag=mailfunction($to,'IVQMobile <mail@ivqmobile.com>',$subject,$message);
        //echo $mail_flag=mailfunctionnew(ADMIN_EMAIL,$to,$subject,$message,$custom_host_name='',$custom_user_name='',$custom_password='',$custom_port_name='',$filepath='');

        ########################################################################
        if($mail_flag == 1) {
            $_SESSION['msg']="Your message has been sent!";
        } else {
            $_SESSION['msg']="Your message has been not sent!";
        }

        $URL=MEB_INDEX_PARAMETER.MEB_CONTACT_US; //."&pass=".$redndomPassword;
        redirect($URL);
        exit;
    }
}
?>

<div class="container group-setion">
    <div class="row">
        <div class="subRow default_content">
            <h1>Contact Us</h1>
            <div class="new-structure1">
            <img src="<?php echo MEMBER_WWW ?>/images/contact.jpg">
            <div class="map_div" style="">
                <div id="gmap-canvas" style="height:100%; width:100%;max-width:100%;">
                  <script src='https://maps.googleapis.com/maps/api/js?v=3.exp'></script><div style='overflow:hidden;height:100%; width:100%;max-width:100%;'><div id='gmap_canvas' style='height:400px;width:520px;'></div><style>#gmap_canvas img{max-width:none!important;background:none!important}</style></div> <a href='https://embedmap.org/'>google maps widget dreamweaver</a> <script type='text/javascript' src='https://embedmaps.com/google-maps-authorization/script.js?id=8aecfd7ec0fbc7e51e7ba291f96f1e7cfef0c8b4'></script><script type='text/javascript'>function init_map(){var myOptions = {zoom:14,center:new google.maps.LatLng(34.270014983768576,-77.83587098784409),mapTypeId: google.maps.MapTypeId.ROADMAP};map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(34.270014983768576,-77.83587098784409)});infowindow = new google.maps.InfoWindow({content:'<strong>Intelli-Touch Apps Inc.</strong><br>Bishop Ranch6  Business Park 2064 Camino Ramon<br>94583 Camino Ramon<br>'});google.maps.event.addListener(marker, 'click', function(){infowindow.open(map,marker);});infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);</script>  
                  <!--<iframe style="height:100%;width:100%;border:0;" frameborder="0" src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJvypWkWV2wYgR0E7HW9MTLvc&key=AIzaSyAYtLSTsIWMdpnMg-gSKH31ZDw96j6gQdk"></iframe>-->
                </div>
                <style>#gmap-canvas img{max-width:none!important;background:none!important;}</style>
            </div>
           

            
        </div>
            <div class="container step-page-section">
                <div class="row">
                <div class="new-address1">
                  <p><b>Intelli-Touch Apps, Inc.</b><br/>
                    Bishop Ranch (6) Business Park<br/>
                    2064 Camino Ramon<br/>
                    Suite # 155<br/>
                    San Ramon, CA 94583<br/>
                    Office Phone (925) 884-1800<br/>
                    Fax Number (925) 884-1804<br/>
                    <br/>
                </div>
                 <!-- <img src="<?php echo MEMBER_WWW ?>/images/contact.jpg"> -->
                    <form name="contact_us" method="post" id="contact_us">
                        <input type="hidden" name="contact_sub" value="yes">
                        <div class="form-main">
                            <div class="main-row">
                                <div>
                                    <label>Name <?php echo getRequiredIcon()?></label>
                                    <input type="text" name="name" id="name" tabindex="1" value="" />
                                </div>

                                <div>
                                    <label>Email <?php echo getRequiredIcon()?></label>
                                    <input type="text" name="email" id="email" tabindex="2" value="" />
                                </div>

                                <div>
                                    <label>Phone <?php echo getRequiredIcon()?></label>
                                    <input type="text" name="phone" id="phone" tabindex="3" value="" />
                                </div>

                                <div>
                                    <label>Comments</label>
                                    <textarea name="comment" id="comment" tabindex="4"></textarea>
                                </div>

                            </div>

                            <div class="main-row">
                                <div class="step-button-1"><a href="javascript:void(0);" onclick="$('#contact_us').submit();">Submit</a></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
	</div>
</div>
<script>
    $(document).ready(function(){
        $('.tooltip').tooltipster();
        $("#contact_us").validate({
            rules: {
                name : {
                    required : true
                },
                email : {
                    required : true,
                    email : true
                },
                phone : {
                    required : true
                }
            },
            messages: {
                name : {
                    required : "Please enter name"
                },
                email : {
                    required : "Please enter email address",
                    email : "Please enter valid email address"
                },
                phone : {
                    required : "Please enter phone no.",
                    phoneValidate : "Please enter valid phone no."
                }
            }
        });
    });

</script>

