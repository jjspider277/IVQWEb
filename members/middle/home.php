<script type="text/javascript" src="<?php echo AJAX_FOLDER_WWW?>getAjaxMembers.js"></script>
<?php
#################Directory###################

$SECTION_FIELD_PREFIX_DIRECTORY = 'dir_';
$SECTION_AUTO_ID_DIRECTORY = $SECTION_FIELD_PREFIX_DIRECTORY.'id';
$SECTION_VIEW_PAGE_DIRECTORY = ADM_VIEW_DIRECTORY;
$SECTION_MANAGE_PAGE_DIRECTORY = ADM_MANAGE_DIRECTORY;
$SECTION_TABLE_DIRECTORY = TBL_DIRECTORY;

#################Directory###################

//Category Listing...
$cat_fields = array("dic_id","dic_name");
$cat_where  = "dic_status = 'Active'";
$catRes 	= $db->selectData("tbl_dir_category",$cat_fields,$cat_where,$extra="",2);

//State Listing...
$sta_fields = array("sta_id","sta_name");
$sta_where  = "sta_status = 'Active'";
$staRes 	= $db->selectData("tbl_state",$sta_fields,$sta_where,$extra="",2);

//Directory Listing...
$dir_fields = array("dir_id","dir_name");
$dir_where  = "dir_status = 'Active'";
$dirRes 	= $db->selectData(TBL_DIRECTORY,$dir_fields,$dir_where,$extra="",2);
?>
<script type="text/javascript" language="javascript">
    $(document).ready(function(){
        getpagelistingNew('Directory','<?php echo $SECTION_TABLE_DIRECTORY?>','<?php echo $SECTION_FIELD_PREFIX_DIRECTORY?>','<?php echo $SECTION_MANAGE_PAGE_DIRECTORY;?>','','');
    });
</script>

<?php //include_once(MEB_SECTION_DIR.'headerinner.php');?>

<div class="container search-group-section">
    <div class="row">

        <form onsubmit="return false;">
            <div class="search-section-left">
                <input type="search" name="search" id="dir_search" placeholder="Search" class="">
                <div class="buttons_right">
                    <input type="submit" onclick="search_dir()" name="btn" value="" class="">
<!--                    <a href="#"><img src="images/filter.png" /></a>-->
                </div>

            </div>
        </form>
        <div class="search-section-right">
            <div class="group-add-text"><a href="javascript:void(0);">Groups</a></div>
        </div>
    </div>
</div>

<div class="container group-setion">
    <div class="row">
        <input type="hidden" name="total_nage" id="total_page" value="">
        <input type="hidden" name="current_page" id="current_page" value="">


        <div class="result-section">
            <div class="result-text">Result</div>

            <div class="result-add-remove-icon">
                <a href="<?php echo "export_excel.php?table=tbl_directory&prifix=dir_";?>" class="file-icon tooltip" title="Export"></a>
                <a href="<?php echo MEMBER_WWW.ADM_INDEX_PARAMETER.MEB_CREATE_GROUP_1; ?>" class="pluse-icon tooltip" title="Add"></a>
            </div>
        </div>

        <div class="group-setion-row" id="updatediv">
            <div class="pre_loader_main" >
                <img src="images/spinner.gif">
            </div>
        </div>

        <a style="display: none;" id="next" href="index2.html">next page?</a>

    </div>
</div>

<?php
$error_re='';
if($_SESSION['msg']!='') {
    $error_re=$_SESSION['msg'];
    $_SESSION['msg']='';
}

$error_re_gr='';
if($_SESSION['msg_gr']!='') {
    $error_re_gr=$_SESSION['msg_gr'];
    $_SESSION['msg_gr']='';
}
?>

<script>
    error_re='';
    error_re='<?php echo $error_re; ?>';
    if(error_re!='') {
        alertify.success(error_re);
        //var notification = alertify.notify(error_re, 'success', 5, function(){  console.log('dismissed'); });
    }

    error_re_gr='';
    error_re_gr='<?php echo $error_re_gr; ?>';
    if(error_re_gr!='') {
        alertify.error(error_re_gr);
        //var notification = alertify.notify(error_re, 'success', 5, function(){  console.log('dismissed'); });
    }

    function search_dir() {
        search_key = $.trim($('#dir_search').val());
        $("#updatediv").html('');
       // $("#updatediv").infinitescroll("destroy");
        $("#updatediv").infinitescroll("unbind");
        getpagelisting('Directory','<?php echo $SECTION_TABLE_DIRECTORY?>','<?php echo $SECTION_FIELD_PREFIX_DIRECTORY?>','<?php echo $SECTION_MANAGE_PAGE_DIRECTORY;?>','',search_key);
    }

    $(document).on('click', '.gr_on_view_card', function(event) {
        data_url = $(this).attr('data_url');
        if(data_url!='') {
            window.location=$(this).attr('data_url');
        }
    });

    $(document).on('click', '.edit_directory', function(){
        window.location=$(this).attr('href');
        return false;
    });

    $(document).on('click', '.delete_directory', function(){
        //window.location=$(this).attr('href');
        return false;
    });

    $(document).on('click', '.view_group', function(){
        //window.location=$(this).attr('href');
        return false;
    });

    /*$(window).scroll(function() {
        //if($(window).scrollTop() + $(window).height() == $(document).height()) {
        if($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
            $(window).unbind('scroll');

            naxt = $('.pagination li.active').next().children().trigger('click');

            //alert("bottom!");
        }
    });*/

    function infinite_f() {
        $("#updatediv").infinitescroll("bind");
        total = $('#total_page').val();
        url = ajax_folder+"getDirectorylist_New.php";

        search_key = $.trim($('#dir_search').val());
        url = url+'?fieldPrefix=dir_&managePage=manage_directory&search_text='+search_key+'&tableName=tbl_directory&xtraCondition=&orderby=id&order=&page=';

        $("#updatediv").infinitescroll({
            navSelector  	: "#next:last",
            nextSelector 	: "a#next:last",
            itemSelector 	: "#updatediv div.gr_on_view_card123",
            msgText         : "Loading group list...",
            debug		 	: true,
            dataType	 	: 'html',
            maxPage         : total,//$('#total_page').val(),

            path: function(index) {
                current = $('#current_page').val();
                current_next = parseInt(current)+ parseInt('1');
                //url = url+'?fieldPrefix=dir_&managePage=manage_directory&search_text=&tableName=tbl_directory&xtraCondition=&orderby=id&order=&page='+current_next;
                return url+current_next;
            },
            state: {
                isDestroyed: false,
                isDone: false,
                isDuringAjax : false
            }
            // behavior		: 'twitter',
            // appendCallback	: false, // USE FOR PREPENDING
            // pathParse     	: function( pathStr, nextPage ){ return pathStr.replace('2', nextPage ); }
        }, function(newElements, data, url){
            $('#current_page').val(current_next);
            $("#updatediv").append(newElements);
            $('.tooltip').tooltipster();
            $.fancybox.close();

            // $("#updatediv").prepend(newElements);
        });
    }

</script>