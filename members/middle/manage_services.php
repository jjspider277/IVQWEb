<?php
	#################################################################
	$SECTION_FIELD_PREFIX='ser_';
	$SECTION_AUTO_ID=$_REQUEST[$SECTION_FIELD_PREFIX.'id'];
	$SECTION_WHERE=$SECTION_FIELD_PREFIX."id=".$SECTION_AUTO_ID."";
	$SECTION_VIEW_PAGE=MEB_VIEW_SERVICES;
	$SECTION_MANAGE_PAGE=MEB_MANAGE_SERVICES;
	$SECTION_TABLE=TBL_MEMBER_SERVICES;
	$SECTION_NAME='Services';

	$uploadFILEURL = UPLOAD_DIR_SERVICE_FILE;	
	$uploadFILEWWW = UPLOAD_WWW_SERVICE_FILE;

	#################################################################

	if(isset($_POST)){	
		$data = $_POST;	
	}
	if ($data['ser_submit'] == 'Save' || $data['ser_addNew'] == 'Add New') {
		$ser_logo_file_name= $_FILES['ser_logo_file']['name'];

		$add_values[$SECTION_FIELD_PREFIX.'meb_id'] 		= trim($data["ser_meb_id"]);
  		$add_values[$SECTION_FIELD_PREFIX.'dir_id'] 		= trim($dirId);
		$add_values[$SECTION_FIELD_PREFIX.'title'] 			= trim($data["ser_title"]);
		//$add_values[$SECTION_FIELD_PREFIX.'description'] 	= trim($data["ser_description"]);
        $add_values[$SECTION_FIELD_PREFIX.'link'] 	= trim($data["ser_link"]);
		$status = "Active";
		
		if($action != 'Update'){	
			$add_values[$SECTION_FIELD_PREFIX.'status'] 		= $status;									
		    $add_values[$SECTION_FIELD_PREFIX.'created_id'] 	= $mebId;
    	    $add_values[$SECTION_FIELD_PREFIX.'created_date'] = date("Y-m-d H:i:s");	
    		$GPDetail_result = $db->insertData($SECTION_TABLE, $add_values);
    	    //$_SESSION['msg']  =   "Services has been inserted successfully."; 

    	    $Ser_Id=$GPDetail_result;
    	    if($ser_logo_file_name!="")
			{
				if(is_dir($uploadFILEURL.$Ser_Id) == false)
				{ 			
					mkdir($uploadFILEURL.$Ser_Id);
				}
				$bus_file = uploadIVA($uploadFILEURL.$Ser_Id."/",$_FILES['ser_logo_file']);
				if($bus_file=="type_err")
				{
					$_SESSION["err_file"]="Please select only image file"; 
					$db->delete("delete from ".$SECTION_TABLE." where ser_id=".$Ser_Id);
					$_SESSION['add_detail_error'][]=$_POST;
					$URL = getMemberURL($SECTION_MANAGE_PAGE,$action);
					redirect($URL);
					exit; 
				}
				else if($bus_file=="size_err")
				{
					$_SESSION["err_file"]="Please upload file less than 32MB file size!";
					$db->delete("delete from ".$SECTION_TABLE." where ser_id=".$Ser_Id);
					$_SESSION['add_detail_error'][]=$_POST;
					$URL = getMemberURL($SECTION_MANAGE_PAGE,$action);
					redirect($URL);
					exit; 
				}
				else
				{
					$ser_logo_file = $bus_file;
					$updateData[$SECTION_FIELD_PREFIX.'logo_file'] 	= $ser_logo_file;
					$SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$Ser_Id;
					$db->updateData($SECTION_TABLE, $updateData, $SECTION_WHERE);
				}
			} 
    	}else{
    	   	$add_values[$SECTION_FIELD_PREFIX . 'updated_id'] 			= $mebId;
        	$add_values[$SECTION_FIELD_PREFIX . 'updated_date'] 		= date("Y-m-d H:i:s");	
        	$GPDetail_result = $db->updateData($SECTION_TABLE, $add_values, $SECTION_WHERE);
    		//$_SESSION['msg']  =   "Services has been updated successfully.";  

    		$Ser_Id=$SECTION_AUTO_ID;
        	
        	if($_POST['edit_ser_logo_file']=='') {
				$updateData[$SECTION_FIELD_PREFIX.'logo_file'] 	= '';
				$SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$Ser_Id;
				$db->updateData($SECTION_TABLE, $updateData, $SECTION_WHERE);
			}

    	    if($ser_logo_file_name!="")
			{
				if(is_dir($uploadFILEURL.$Ser_Id) == false) { 			
					mkdir($uploadFILEURL.$Ser_Id);
				}
				$bus_file = uploadIVA($uploadFILEURL.$Ser_Id."/",$_FILES['ser_logo_file']);
				if($bus_file=="type_err")
				{
					$_SESSION["err_file"]="Please select only image file"; 
					$db->delete("delete from ".$SECTION_TABLE." where ser_id=".$Ser_Id);
					$_SESSION['add_detail_error'][]=$_POST;
					$URL = getMemberURL($SECTION_MANAGE_PAGE,$action);
					redirect($URL);
					exit; 
				}
				else if($bus_file=="size_err")
				{
					$_SESSION["err_file"]="Please upload file less than 32MB file size!";
					$db->delete("delete from ".$SECTION_TABLE." where ser_id=".$Ser_Id);
					$_SESSION['add_detail_error'][]=$_POST;
					$URL = getMemberURL($SECTION_MANAGE_PAGE,$action);
					redirect($URL);
					exit; 
				}
				else
				{
					$ser_logo_file = $bus_file;
					$updateData[$SECTION_FIELD_PREFIX.'logo_file'] 	= $ser_logo_file;
					$SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$Ser_Id;
					$db->updateData($SECTION_TABLE, $updateData, $SECTION_WHERE);
				}
			}
    	}
    	if($data['ser_addNew'] == 'Add New') {
			$URL = getMemberURL($SECTION_MANAGE_PAGE); 
			redirect($URL);
			exit;
		} else {
			$URL = getMemberURL($SECTION_VIEW_PAGE);
            if($_POST['method']=='Update')
                $_SESSION["msg"] = 'Successfully updated service.';
            else
                $_SESSION["msg"] = 'Successfully created service.';
			redirect($URL);
			exit;
		}
	}
	if($action=='Edit')
	{		
		$list_query = getSelectList($SECTION_TABLE,$SECTION_FIELD_PREFIX);
		$list_query .= "AND ".$SECTION_FIELD_PREFIX."id = '".$SECTION_AUTO_ID."'";
		$result_query = $db->select($list_query);
		$ser_title		=	$result_query[0][$SECTION_FIELD_PREFIX."title"];
		$ser_logo_file	=	$result_query[0][$SECTION_FIELD_PREFIX."logo_file"];
		//$ser_description=	$result_query[0][$SECTION_FIELD_PREFIX."description"];
        $ser_link=	$result_query[0][$SECTION_FIELD_PREFIX."link"];
		$action = "Update";
		$action_url = getMemberURL($SECTION_MANAGE_PAGE,$action,$SECTION_WHERE);
	}	
	if($action=='')
	{
		$action = "Add";
		$action_url = getMemberURL($SECTION_MANAGE_PAGE,$action);
		
		if($_SESSION['add_detail_error'][0]!="")
		{
			foreach($_SESSION['add_detail_error'][0] as $key => $value)
			{
				$result_query[0][$key]=stripslashes($value);
			}
			$ser_title		=	$result_query[0][$SECTION_FIELD_PREFIX."title"];
			$ser_logo_file	=	$result_query[0]["logo_file"];
			//$ser_description=	$result_query[0][$SECTION_FIELD_PREFIX."description"];
            $ser_link       =	$result_query[0][$SECTION_FIELD_PREFIX."link"];
			unset($_SESSION['add_detail_error']);
		}
	}
?>

<div class="container search-group-section">
    <div class="row">
        <div class="step-list">
            <ul>
                <li><a href="javascript:void(0);" class="active"><?php if($action=="Update"){echo 'Edit service';}else{echo 'Create service';} ?></a></li>
            </ul>
        </div>
    </div>
</div>

<div class="container step-page-section">
    <div class="row">
        <form name="manage_services" enctype="multipart/form-data" method="post" action="<?php echo $action_url; ?>" id="manage_services">
            <div class="form-main">
                <div class="main-row">
                    <div>
                        <label>Services Title <?php echo getRequiredIcon()?></label>
                        <input type="text" name="ser_title" id="ser_title"tabindex="1" value="<?php echo $ser_title;?>" />
                    </div>

                    <div class="image_field" style="margin-top:20px;">
                        <label>Logo file</label>
                        <input type="file" name="ser_logo_file" id="ser_logo_file" />
                        <?php if($_SESSION["err_file"]!="") {
                            echo '<label class="error">'.$_SESSION["err_file"].'</label>';
                            $_SESSION["err_file"]="";
                        }
                        if($action=="Update" && $ser_logo_file!="")
                        {
                            echo "<div class='msg_filesddd'>";
                            echo "<a id='vediosPlay' href='view_file_ser.php?filename=".$ser_logo_file."&ser_id=".$SECTION_AUTO_ID."' style='margin-top:5px;' class='fancybox fancybox.ajax' title='View' data-fancybox-type='ajax' >".$ser_logo_file."</a>";
                            echo "&nbsp;&nbsp;&nbsp;<a style='margin-top:5px;' id='removeLink' onclick='javascript:removeFile(edit_ser_logo_file);'>Remove</a>";
                            echo "<input type='hidden' id='edit_ser_logo_file' name='edit_ser_logo_file' value='".$ser_logo_file."' />";
                            echo "</div>";
                        }
                        ?>
                    </div>
                </div>
                <div class="main-row">
                    <label>Services Link <?php echo getRequiredIcon()?></label>
                    <!-- <input type="text" name="ser_description" id="ser_description" tabindex="2" style="height:100px;" value="<?php echo $ser_description;?>" /> -->
                    <!--<textarea name="ser_description" tabindex="2" id="ser_description" style="height:100px;" ><?php /*echo $ser_description;*/?></textarea>-->
                    <input type="text" name="ser_link" id="ser_link" value="<?php echo $ser_link;?>" />
                </div>
                <div class="main-row">
                    <label>&nbsp;</label>
                    <input name="ser_id" id="ser_id" value="<?php echo $SECTION_AUTO_ID; ?>" type="hidden">
                    <input name="ser_meb_id" id="ser_meb_id" value="<?php echo $mebId; ?>" type="hidden">
                    <input type="hidden" name="method" id="method" value="<?php echo $action; ?>" />

                    <input type="hidden" name="ser_submit" id="ser_submit" value="Save" />

                    <div class="step-button-1"><a href="javascript:void(0);" onclick="$('#manage_services').submit();">Save</a></div>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
$(document).ready(function(){
    edd = '<?php echo $action; ?>';
    if(edd=='Update'){
        $(document).prop('title', '<?php echo MEB_FILE_TITLE_EDIT_SERVICES; ?>');
    }

	$('.tooltip').tooltipster();
	$("#manage_services").validate({
		rules: {
			ser_title : {
				required : true
			},
			ser_link : {
				required : true,
                url: true
			}
		},
		messages: {
			ser_title : {
				required : "Please enter Services title"
			},
			ser_link : {
				required : "Please enter Services link"
			}
		}
	});
});
function removeFile($fileName)
{
     $hiddenId = $fileName.id     
     $("#vediosPlay").remove();
     $("#removeLink").remove();
     $('#'+$hiddenId).val('');
     $('#remove_image').val('true');
     $( "#bus_submit" ).trigger( "click" );
}
</script>