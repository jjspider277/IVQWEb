<?php
session_start();
$action=$_REQUEST['mod'];
if (!empty($action))
{
	$error="";
	if(empty($error))
	{
		$sql="select * from ".TBL_MEMBER." where meb_username ='".trim(addslashes($_POST['meb_username']))."' AND meb_password='".md5($_POST['meb_password'])."'"; // AND meb_status='Active'
		$data = $db->select($sql);
		if(!empty($data))
		{
			if($data[0]['meb_status'] == 'Active')
			{
				if($_POST['remember']==1)
				{
					setcookie('meb_username', $_POST['meb_username'],time() + 60*60*24*30);
					setcookie('meb_password', $_POST['meb_password'],time() + 60*60*24*30);
				}

				$_SESSION['meb_id']	        =	$data[0]['meb_id'];
				$_SESSION['meb_admin_id']	=	$data[0]['meb_id'];
				$_SESSION['mebDirId']	    =	$data[0]['meb_dir_id'];
				$_SESSION['member_name'] 	=	$data[0]['meb_name'];
				$_SESSION['meb_type']	    =	"Members";
			//	print_r($_SESSION);
				$sql = "INSERT INTO ".TBL_MEMBER_LOGIN_DETAIL." SET
						mld_meb_id = '".$data[0]['meb_id']."',
						mld_login_datetime = '".date('Y-m-d H:i:s')."',
						mld_ip_address = '".$_SERVER['REMOTE_ADDR']."'";

				$GPDetails = $db->insert($sql);
				$log_id = mysql_insert_id();
				$_SESSION['meb_log_id']=$log_id;
				$URL=MEB_INDEX_PARAMETER.MEB_HOME;
				redirect($URL);
				exit;
			}
			else
			{
				$error = "Your account has been deactivated";
			}
		}
		else
		{
			$error = "Invalid E-mail and Password.";
		}
	}
}

$app_apk = '';
$app_ios = '';

$dir_fields = array("*");
$dir_where  = "";
$dirRes 	= $db->selectData(TBL_APP_SETTING,$dir_fields,$dir_where,$extra="",2);

if(count($dirRes)>0) {
	foreach($dirRes as $dirdata) {

		if($dirdata['meta'] == 'apk') {
			$app_apk = $dirdata['value'];
		}

		if($dirdata['meta'] == 'ipa') {
			$app_ios = $dirdata['value'];
		}
	}
}
?>

<div class="content-section-01">
	<div class="row">

		<div class="left-image-section">
			<img src="<?php echo ROOT_WWW; ?>/members/images/mobille-img.png" alt="">
		</div>

		<div class="right-form-section">
			<h1>User login</h1>

			<form name='frmMember' method='post' action="<?php echo MEB_INDEX_PARAMETER.MEB_LOGIN."&mod=add";?>" id='frmMember'>

				<div class="right-form-section-form">
					<label class="error_msg"><?php echo $error; ?></label>

					<div class="error1">
						<div class="inputText username"><input type="text" name="meb_username" id="meb_username" placeholder="Enter e-mail address" value="<?php echo $_COOKIE['meb_username']; ?>" /></div>
						<span class="email-icon"></span>
					</div>

					<div class="error1">
						<div class="inputText password"><input type="password" name="meb_password" id="meb_password" placeholder="Enter password" value="<?php echo $_COOKIE['meb_password']; ?>" /></div>
						<span class="closed-icon"></span>
					</div>
					<span class="forgot_pass"><a href="<?php echo MEB_INDEX_PARAMETER.MEB_FORGOT;?>">Forgot password?</a></span>

				</div>

				<div class="button-section">
					<div style="display: none" class="remember"><input name="remember" id="remember" type="checkbox" value="1" data-label="Remember Me"  data-labelPosition="right"></div>
					<div class="create-accoumt"><a href="<?php echo MEB_INDEX_PARAMETER.MEB_REGISTER?>">Create account</a>
						</div>
					<div class="login"><a href="javascript:void(0);" onclick="$('#frmMember').submit();">Login<span class="login-arrow"></span></a></div>
				</div>

				<div class="google-play-section">
					<div class="apple-button"><a target="_blank" href="<?php echo $app_ios; ?>"><img src="images/app-store-1.jpg"></a></div>
					<div class="google-play-button"><a target="_blank" href="<?php echo $app_apk; ?>"><img src="images/google-play-1.jpg"></a></div>
				</div>

			</form>

		</div>
	</div>
</div>

<?php
$error_re='';
if($_SESSION['msg']!='') {
    $error_re=$_SESSION['msg'];
    $_SESSION['msg']='';
}

$error_msg_l_e='';
if($_SESSION['msg_l_e']!='') {
    $error_msg_l_e=$_SESSION['msg_l_e'];
    $_SESSION['msg_l_e']='';
}
?>

<script>
$(document).ready(function() {
	$('input[type="checkbox"]').prettyCheckable();	
	$("#frmMember").validate({
		rules: {
			meb_username:{
                required: true,
                email: true
            },
			meb_password:"required"
		},
		messages: {
			meb_username:"Please enter username",
			meb_password:"Please enter password"
		}
	});

    error_re='';
    error_re='<?php echo $error_re; ?>';
    if(error_re!='') {
        alertify.success(error_re);
        //var notification = alertify.notify(error_re, 'success', 5, function(){  console.log('dismissed'); });
    }

    error_msg_l_e='';
    error_msg_l_e='<?php echo $error_msg_l_e; ?>';
    if(error_msg_l_e!='') {
        alertify.error(error_msg_l_e);
        //var notification = alertify.notify(error_re, 'success', 5, function(){  console.log('dismissed'); });
    }
});
</script>
