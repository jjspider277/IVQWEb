<script type="text/javascript" src="<?php echo AJAX_FOLDER_WWW; ?>getAjaxAdmin.js"></script>
<?php 
#################################################################
	$SECTION_FIELD_PREFIX='bus_';
	$SECTION_AUTO_ID=$_REQUEST[$SECTION_FIELD_PREFIX.'id'];
	$SECTION_WHERE=$SECTION_FIELD_PREFIX."id=".$SECTION_AUTO_ID."";
	$SECTION_VIEW_PAGE=MEB_VIEW_BUSINESS;
	$SECTION_MANAGE_PAGE=MEB_MANAGE_BUSINESS;
	
	$SECTION='Business';

	$type = getMemberType();
	if($type!="Business")
	{
		$SECTION_TABLE= TBL_MEMBER_BUSINESS;
		$padding = "style='padding: 22px;'";
	}
	else
	{
		$SECTION_TABLE= TBL_MEMBER_BUSINESS_SUB;
		$padding = "style='padding: 1.5px;'";
	}
	
	//State Listing...
	$state_fields = array("sta_id","sta_name");
	$state_where  = "sta_status = 'Active'";
	$staRes 	= $db->selectData(TBL_STATE,$state_fields,$state_where,$extra="",2);
#################################################################
if($_POST["bus_import"]=="Upload Business" && $_REQUEST["method"]=="import")
{
	if((!empty($_FILES["import_bus"])) && ($_FILES['import_bus']['error'] == 0))
	{
		$filename = basename($_FILES['import_bus']['name']);
		$newname = UPLOAD_DIR_BUSINESS_FILE.$filename;
		if ((move_uploaded_file($_FILES['import_bus']['tmp_name'],$newname)))
		{
			include(ROOT_DIR.'include/Classes/PHPExcel.php');
			$objReader = new PHPExcel_Reader_Excel5();
			$objReader->setReadDataOnly(true);
			$objPHPExcel = $objReader->load($newname);
			$rowIterator = $objPHPExcel->getActiveSheet()->getRowIterator();
			$array_data = array();
			$flag = false;
			foreach($rowIterator as $row)
			{
				$cellIterator = $row->getCellIterator();
				$cellIterator->setIterateOnlyExistingCells(false); // Loop all cells, even if it is not set
				$rowIndex = $row->getRowIndex();
				$array_data[$rowIndex] = array('A'=>'', 'B'=>'','C'=>'','D'=>'','E'=>'','F'=>'','G'=>'','H'=>'','I'=>'','J'=>'','K'=>'','L'=>'');
				foreach ($cellIterator as $cell) {
			   if('A' == $cell->getColumn()){
			   $array_data[$rowIndex][$cell->getColumn()] = $cell->getValue();
			   } else if('B' == $cell->getColumn()){
			   $array_data[$rowIndex][$cell->getColumn()] = $cell->getValue();
			   } else if('C' == $cell->getColumn()){
						$array_data[$rowIndex][$cell->getColumn()] = $cell->getValue();
      } else if('D' == $cell->getColumn()){
						$array_data[$rowIndex][$cell->getColumn()] = $cell->getValue();
			   } else if('E' == $cell->getColumn()){
						$array_data[$rowIndex][$cell->getColumn()] = $cell->getValue();
			   } else if('F' == $cell->getColumn()){
						$array_data[$rowIndex][$cell->getColumn()] = $cell->getValue();
			   } else if('G' == $cell->getColumn()){
						$array_data[$rowIndex][$cell->getColumn()] = $cell->getValue();
			   } else if('H' == $cell->getColumn()){
						$array_data[$rowIndex][$cell->getColumn()] = $cell->getValue();
			   } else if('I' == $cell->getColumn()){
						$array_data[$rowIndex][$cell->getColumn()] = $cell->getValue();
			   } else if('J' == $cell->getColumn()){
						$array_data[$rowIndex][$cell->getColumn()] = $cell->getValue();
			   } else if('K' == $cell->getColumn()){
						$array_data[$rowIndex][$cell->getColumn()] = $cell->getValue();
			   } else if('L' == $cell->getColumn()){
						$array_data[$rowIndex][$cell->getColumn()] = $cell->getValue();
			   }
			 }
			}
   
			/*echo "<pre>";
			print_r($array_data);
			echo "</pre>";
			exit;*/
			if($array_data[1]["A"]=="Business Title" && $array_data[1]["B"]=="Company" && $array_data[1]["C"]=="First Name" && $array_data[1]["D"]=="Last Name" && $array_data[1]["E"]=="Business Address" && $array_data[1]["F"]=="Business City" && $array_data[1]["G"]=="Business State" && $array_data[1]["H"]=="Business Phone" && $array_data[1]["I"]=="Business Zipcode" && $array_data[1]["J"]=="Business Fax" && $array_data[1]["K"]=="Business Email" && $array_data[1]["L"]=="Business About")
			{
				$i=2;
				$ins = 1;
				$not_ins = 0;
				$total_rec = count($array_data);
				if($total_rec==1)
				{
					$total_rec=2;
				}
				while($i<=$total_rec)
				{
					if($array_data[$i]["A"]!="" && $array_data[$i]["C"]!="" && $array_data[$i]["D"]!="" && $array_data[$i]["E"]!="" && $array_data[$i]["F"]!="" && $array_data[$i]["G"]!="" && $array_data[$i]["H"]!="" && $array_data[$i]["I"]!="" && $array_data[$i]["J"]!="" && $array_data[$i]["K"]!="" && $array_data[$i]["L"]!="")
					{
						$state_fields = array("sta_id");
						$state_where  = "sta_sort_name = '".strtoupper($array_data[$i]["G"])."'";
						$staRes 	= $db->selectData(TBL_STATE,$state_fields,$state_where,$extra="",2);
						$sql="insert into ".TBL_MEMBER_BUSINESS."(bus_meb_id,bus_dir_id,bus_title,bus_company,bus_name,bus_lname,bus_address1,bus_city,bus_sta_id,bus_phone,bus_zip,bus_fax,bus_email,bus_about,bus_status,bus_created_id,bus_created_date) values('".$mebId."','".$dirId."','".mysql_real_escape_string($array_data[$i]["A"])."','".mysql_real_escape_string($array_data[$i]["B"])."','".mysql_real_escape_string($array_data[$i]["C"])."','".mysql_real_escape_string($array_data[$i]["D"])."','".mysql_real_escape_string($array_data[$i]["E"])."','".mysql_real_escape_string($array_data[$i]["F"])."','".$staRes[0]['sta_id']."','".mysql_real_escape_string($array_data[$i]["H"])."','".mysql_real_escape_string($array_data[$i]["I"])."','".mysql_real_escape_string($array_data[$i]["J"])."','".mysql_real_escape_string($array_data[$i]["K"])."','".mysql_real_escape_string($array_data[$i]["L"])."','Active','".$mebId."','".date("Y-m-d H:i:s")."')";
						//echo $sql;exit;
						$insert_res = $db->Query($sql);
						$ins = $ins+1;
					}
					else
					{
						$not_ins = $not_ins+1;
					}
					$i++;
				}
				//$_SESSION["msg"]="Subscribers has been imported successfully. Total : ".$total_rec.". Inserted : ".$ins.". Not Inserted : $not_ins";
			}
			else
			{
				//$_SESSION["error"]="Subscribers has been not imported successfully.";
			}
			unlink($newname);
		}
		else
		{
			//$_SESSION["error"] = "A problem occurred during file upload!";
		}
	}
	else
	{
		//$_SESSION["error"] = "Data has been not inserted";
	}
	$URL = getMemberURL($SECTION_VIEW_PAGE); 
	redirect($URL);
}
?>
<script type="text/javascript" language="javascript">
$(document).ready(function(){
	getpagelisting('<?php echo $SECTION; ?>','<?php echo $SECTION_TABLE?>','<?php echo $SECTION_FIELD_PREFIX?>','<?php echo $SECTION_MANAGE_PAGE; ?>');

	$.validator.addMethod("validExcel", function(value, element) {
    	return this.optional(element) || /\.(xls|xlsx|XLS|XLSX)$/i.test(value);
	}, "Please select only image");
	$("#import_business").validate({
		rules: {
			import_bus : {
				required : true,
				validExcel : true
			}
		},
		messages: {
			import_bus : {
				required : "Please select business file",
				validExcel : "Please select only xls or xlxs file"
			}
		}
	});

	/*$("#bus_from_date").datepicker({
		dateFormat: "mm/dd/yy",
		changeMonth: true,
		changeYear: true,
		buttonImageOnly: false,
		onSelect: function(selected) 
		{						
			$('#bus_to_date').datepicker('option', 'minDate', new Date(selected));
		}
	});
	$("#bus_to_date").datepicker({
		dateFormat: "mm/dd/yy",
		changeMonth: true,
		changeYear: true,
		buttonImageOnly: false,
		onSelect: function(selected) 
		{						
			$('#bus_from_date').datepicker('option', 'maxDate', new Date(selected));
		}
	});
	$('#bus_from_date').datepicker('option', 'maxDate', new Date($('#bus_to_date').val()));
	$('#bus_to_date').datepicker('option', 'minDate', new Date($('#bus_from_date').val()));*/
});
</script>
<section>
	<article id="page" >
	<header>
		<?php include_once(MEB_SECTION_DIR.'headerinner.php');
		
		?>
		<div class="tab_content_holder">
			<div <?php echo $padding; ?> > <!-- class="tab_content_holder_inner" -->
				<?php 
				if($type!="Business")
				{
				?>
				<div class="border-txt">
				<div class="serviceTxt">Search<span></span></div>
				<div class="block-part">
				<form name="search_bus" id="search_bus" action="" method="post">
					<div class="main-row">
						<label>Title</label>
						<input type="text" name="bus_title" id="bus_title" tabindex="1" value="" />
					</div>
					<div class="main-row">
						<label>Company</label>
						<input type="text" name="bus_company" id="bus_company" tabindex="2" value="" />
					</div>
					<div class="main-row">
						<label>Name</label>
						<input type="text" name="bus_name" id="bus_name" tabindex="3" value="" />
					</div>
					<div class="main-row">                            	
						<label>City</label>
						<input type="text" name="bus_city" id="bus_city" tabindex="4" value="" />
					</div>
					<div class="main-row">
						<label>State</label>
						<select name="bus_sta_id" id="bus_sta_id" class="select" tabindex="5" alt="Type to search state">
							<option value="">Select State</option>
							<?php						
							for($i=0;$i<count($staRes);$i++)
							{
								if($staRes[$i]['sta_id']==$bus_sta_id) { $select="selected='selected'"; } else { $select=""; }
							?>
								<option value="<?php echo $staRes[$i]['sta_id']; ?>" <?php echo $select; ?> ><?php echo $staRes[$i]['sta_name']; ?></option>
							<?php
							}
							?>
						</select>
					</div>
					<div class="main-row">
						<label>Phone Number</label>
						<input type="text" name="bus_phone" id="bus_phone" tabindex="6" value="" />
					</div>
					<div class="main-row">
						<label>Zip Code</label>
						<input type="text" name="bus_zip" id="bus_zip" tabindex="7" value="" />
					</div>
					<div class="main-row">                            	
						<label>Fax</label>
						<input type="text" name="bus_fax" id="bus_fax" tabindex="8" value="" />
					</div>
					<div class="main-row">
						<label>Email</label>
						<input type="text" name="bus_email" id="bus_email" tabindex="9" value="" />
					</div>
					
					<!-- <div class="main-row">
						<div class="fromdate">
						<label>From</label>
						<input type="text" readonly="readonly" tabindex="8" name="bus_from_date" id="bus_from_date"></input>
						</div>
						<div class="todate">
						<label>To</label>
						<input type="text" readonly="readonly" tabindex="9" name="bus_to_date" id="bus_to_date"></input>
						</div>
					</div> -->				
					<div class="main-row">  <!-- search-add -->
						<label>&nbsp;</label>
						<input type="button" tabindex="10" onclick="getLiveSearch('<?php echo $SECTION; ?>','<?php echo $SECTION_TABLE?>','<?php echo $SECTION_FIELD_PREFIX?>','','<?php echo $SECTION_MANAGE_PAGE; ?>');" name="bus_search" id="bus_search" value="Search" />
						<input type="button" id="bus_reset" onclick="resetForm('search_bus','<?php echo $SECTION; ?>','<?php echo $SECTION_TABLE?>','<?php echo $SECTION_FIELD_PREFIX?>','<?php echo $SECTION_MANAGE_PAGE; ?>');" tabindex="11" value="Clear" />
					</div>
				</form>
				</div>
				<div class="clr"></div>
				</div>
				<div class="bottomDiv">
					<form name="import_business" enctype="multipart/form-data" method="post" action="<?php echo getMemberURL($SECTION_VIEW_PAGE,"import"); ?>" id="import_business">
						<div class="main-row attach">                            	
							<label>Attach File <?php echo getRequiredIcon()?></label>
							<input type="file" tabindex="11" name="import_bus" id="import_bus" />
							<div class="exel-demo"><a href="<?php echo UPLOAD_WWW.'Member-Business-demo.xls'; ?>">Download Template</a></div> 
						</div>
						<div class="main-row phone">
							<label>&nbsp;</label>
							<input type="submit" tabindex="12" name="bus_import" id="bus_import" value="Upload Business" />
						</div>
					</form>
				</div>
				<div class="clr"></div>
				<?php } ?>
			</div>
		</div>
	</header>
	<div id="updatediv"></div>
	</article>
</section>