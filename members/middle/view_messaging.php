<script type="text/javascript" src="<?php echo AJAX_FOLDER_WWW; ?>getAjaxMembers.js"></script>
<?php
	#################################################################
	$SECTION_FIELD_PREFIX='msg_';
	$SECTION_AUTO_ID=$_REQUEST[$SECTION_FIELD_PREFIX.'id'];
	$SECTION_WHERE=$SECTION_FIELD_PREFIX."id=".$SECTION_AUTO_ID."";
	$SECTION_VIEW_PAGE=MEB_VIEW_MESSAGING;
	$SECTION_MANAGE_PAGE=MEB_MANAGE_MESSAGING;
	$SECTION_TABLE=TBL_MEMBER_MESSAGING;
	$SECTION_NAME='Messaging';
?>
<script type="text/javascript" language="javascript">
$(document).ready(function(){
	getpagelistingNew('<?php echo $SECTION_NAME; ?>','<?php echo $SECTION_TABLE?>','<?php echo $SECTION_FIELD_PREFIX?>','<?php echo $SECTION_MANAGE_PAGE; ?>','','');
});
</script>

<div class="container search-group-section">
    <div class="row">
        <form onsubmit="return false;">
            <div class="search-section-left">
                <input type="search" name="search" id="cop_title" placeholder="Search" class="">
                <div class="buttons_right"><input type="submit" onclick="search_dir();" name="btn" value="" class=""></div>
            </div>
        </form>
        <div class="search-section-right">
            <div class="messages-title-text"><a href="javascript:void(0);">Broadcast</a></div>
        </div>
    </div>
</div>

<div class="container member-page-setion-1">
    <div class="row">
        <input type="hidden" name="total_nage" id="total_page" value="">
        <input type="hidden" name="current_page" id="current_page" value="">
        <div class="result-section">
            <div class="result-text">Result</div>

            <div class="result-add-remove-icon">
                <?php $href_export = "export_excel.php?table=tbl_member_messaging&prifix=msg_";?>
                <a href="<?php echo $href_export; ?>" class="file-icon tooltip" title="Export"></a>
                <a href="<?php echo MEB_INDEX_PARAMETER.$SECTION_MANAGE_PAGE; ?>" title="Add" class="pluse-icon tooltip"></a>
            </div>
        </div>

        <div class="group-setion-row" id="updatediv">

        </div>
        <a style="display: none;" id="next" href="index2.html">next page?</a>
    </div>
</div>

<?php
$error_re='';
if($_SESSION['msg']!='') {
    $error_re=$_SESSION['msg'];
    $_SESSION['msg']='';
}
?>

<script>
$(document).ready(function() {
    error_re='';
    error_re='<?php echo $error_re; ?>';
    if(error_re!='') {
        alertify.success(error_re);
        //var notification = alertify.notify(error_re, 'success', 5, function(){  console.log('dismissed'); })    ;
    }

	$('.tooltip').tooltipster();
	
	$("#manage_messaging").validate({
		rules: {
			msg_message : {
				required : true
			},
			msg_file : {
				filesize: 1048576   
			}
		},
		messages: {
			msg_message : {
				required : "Please enter publish message"
			},msg_file : {
				filesize : "File must be less than 1MB"
			}
		}
	});
});

function search_dir() {
    search_key = $.trim($('#cop_title').val());
    $("#updatediv").html('');
    // $("#updatediv").infinitescroll("destroy");
    $("#updatediv").infinitescroll("unbind");
    getpagelistingNew('<?php echo $SECTION_NAME; ?>','<?php echo $SECTION_TABLE?>','<?php echo $SECTION_FIELD_PREFIX?>','<?php echo $SECTION_MANAGE_PAGE; ?>','',search_key);
}

function infinite_f() {
    $("#updatediv").infinitescroll("bind");
    total = $('#total_page').val();
    url = ajax_folder+"getMessagingList.php";

    search_key = $.trim($('#cop_title').val());
    url = url+'?fieldPrefix=msg_&managePage=manage_messaging&search_text='+search_key+'&tableName=tbl_member_messaging&xtraCondition=&order=&page=';

    $("#updatediv").infinitescroll({
        navSelector  	: "#next:last",
        nextSelector 	: "a#next:last",
        itemSelector 	: "#updatediv div.gr_on_view_card123",
        msgText         : "Loading group list...",
        debug		 	: true,
        dataType	 	: 'html',
        maxPage         : total,//$('#total_page').val(),

        path: function(index) {
            current = $('#current_page').val();
            current_next = parseInt(current)+ parseInt('1');
            //url = url+'?fieldPrefix=dir_&managePage=manage_directory&search_text=&tableName=tbl_directory&xtraCondition=&orderby=id&order=&page='+current_next;
            return url+current_next;
        },
        state: {
            isDestroyed: false,
            isDone: false,
            isDuringAjax : false
        }
        // behavior		: 'twitter',
        // appendCallback	: false, // USE FOR PREPENDING
        // pathParse     	: function( pathStr, nextPage ){ return pathStr.replace('2', nextPage ); }
    }, function(newElements, data, url){
        $('#current_page').val(current_next);
        $("#updatediv").append(newElements);
        $('.tooltip').tooltipster();
        $.fancybox.close();

        // $("#updatediv").prepend(newElements);
    });
}
</script>	