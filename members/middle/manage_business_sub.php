<script type="text/javascript" src="<?php echo AJAX_FOLDER_WWW; ?>getAjaxAdmin.js"></script>
<?php
#################################################################
$SECTION_FIELD_PREFIX = 'bsb_';
$SECTION_AUTO_ID = $SECTION_FIELD_PREFIX.'id';
$SECTION_VIEW_PAGE = MEB_VIEW_SUBSCRIBER;
$SECTION_MANAGE_PAGE = MEB_MANAGE_BUSINESS_SUB;
$SECTION_TABLE = TBL_BUSINESS_SUB;
#################################################################
//Directory Listing...
$dir_fields = array("dir_id","dir_name");
$dir_where  = "dir_status = 'Active'";
$dirRes 	= $db->selectData(TBL_DIRECTORY,$dir_fields,$dir_where,$extra="",2);
	
if(!empty($_REQUEST[$SECTION_FIELD_PREFIX.'id']))
	$SECTION_AUTO_ID = $_REQUEST[$SECTION_FIELD_PREFIX.'id'];
else	
	$SECTION_AUTO_ID  = $SECTION_FIELD_PREFIX.'id';		

if(isset($_POST)){	
	$data = $_POST;	
}
if ($data['bsb_submit'] == 'Save')
{
	unset($add_values);		
	$add_values[$SECTION_FIELD_PREFIX . 'dir_id'] 		= $data[$SECTION_FIELD_PREFIX . 'dir'];
	$add_values[$SECTION_FIELD_PREFIX . 'name'] 		= $data[$SECTION_FIELD_PREFIX . 'name'];
	$add_values[$SECTION_FIELD_PREFIX . 'username'] = $data[$SECTION_FIELD_PREFIX . 'username'];
	if($data[$SECTION_FIELD_PREFIX . 'password']!="")
	{
		$add_values[$SECTION_FIELD_PREFIX . 'password'] = md5($data[$SECTION_FIELD_PREFIX . 'password']);
	}
	$add_values[$SECTION_FIELD_PREFIX . 'question'] = $data[$SECTION_FIELD_PREFIX . 'question'];
	$add_values[$SECTION_FIELD_PREFIX . 'answer'] 	= $data[$SECTION_FIELD_PREFIX . 'answer'];
	$add_values[$SECTION_FIELD_PREFIX . 'phone'] 	= $data[$SECTION_FIELD_PREFIX . 'phone'];

		
	if($action != 'Update'){
		$add_values[$SECTION_FIELD_PREFIX . 'status'] 	= "Inactive";
		$add_values[$SECTION_FIELD_PREFIX . 'created_id'] 			= $mebId;
		$add_values[$SECTION_FIELD_PREFIX . 'created_date'] 		= $today_date;	
		$GPDetail_result = $db->insertData($SECTION_TABLE, $add_values); 
    }else{
    	$pass = $data[$SECTION_FIELD_PREFIX.'password'];
    	$uname = $data[$SECTION_FIELD_PREFIX.'username'];
    	if($pass!="" && $uname!="")
		{
			$subject = "Account Details";
			$message = businessSubscriberInfo($uname,$pass);
			$mail_flag=mailfunction($uname,ADMIN_EMAIL,$subject,$message);
		}
		$add_values[$SECTION_FIELD_PREFIX . 'updated_id'] 			= $mebId;
		$add_values[$SECTION_FIELD_PREFIX . 'updated_date'] 		= $today_date;		
		$SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$SECTION_AUTO_ID;
		$GPDetail_result = $db->updateData($SECTION_TABLE, $add_values, $SECTION_WHERE); 
	}
   	$URL = getMemberURL($SECTION_VIEW_PAGE); 
	redirect($URL);
	exit;
}

if ($action == 'Edit' && $SECTION_AUTO_ID != $SECTION_FIELD_PREFIX."id")
{
    $list_query = getSelectList($SECTION_TABLE, $SECTION_FIELD_PREFIX);
    $list_query .= " AND " . $SECTION_FIELD_PREFIX . "id = " . $SECTION_AUTO_ID; 
    $result_query = $db->select($list_query); 
    $bsb_dir_id = $result_query[0][$SECTION_FIELD_PREFIX.'dir_id'];
	$bsb_name = $result_query[0][$SECTION_FIELD_PREFIX.'name'];
    $bsb_username = $result_query[0][$SECTION_FIELD_PREFIX.'username'];
    $bsb_question = $result_query[0][$SECTION_FIELD_PREFIX.'question']; 
    $bsb_answer = $result_query[0][$SECTION_FIELD_PREFIX.'answer']; 
    $bsb_phone = $result_query[0][$SECTION_FIELD_PREFIX.'phone']; 
    
    $action = "Update";
    $action_url = getMemberURL($SECTION_MANAGE_PAGE,$action);

}
if ($action == '')
{
	$action = "Add";
    $action_url = getMemberURL($SECTION_MANAGE_PAGE,$action);
}
?>
<section>
<article id="page" >
	<header>
		<ul class="tab_links ">
    		<li><div class="tab_link_active inner">
    			<span><img src="<?php echo IMG_WWW; ?>directory.png"></span><h1>Business Subscriber</h1>
				<div class="view">
					<a href="<?php echo getMemberURL($SECTION_VIEW_PAGE); ?>" class="tooltip" title="back">
					<img src="<?php echo IMG_WWW; ?>back-orange.png"></a>
				</div>
			</div></li>    					
    	</ul>
	</header>
	<aside>		
		<div class="tab_content_holder directory">
			<div class="tab_content_holder_inner">
				<div class="block-part">
				<form name="manage_bussub" method="post" action="<?php echo $action_url; ?>" id="manage_bussub">
					<div class="tableRow">
						<div class="main-row">
							<label>Directory Name <?php echo getRequiredIcon()?></label>
							<select name="bsb_dir" id="bsb_dir" tabindex="1" alt="Type to search directory name">
								<option value="">Select Directory</option>
								<?php						
									for($i=0;$i<count($dirRes);$i++)
									{
										if($dirRes[$i]['dir_id']==$bsb_dir_id) { $select="selected='selected'"; } else { $select=""; }
								?>
								<option value="<?php echo $dirRes[$i]['dir_id']; ?>" <?php echo $select; ?> ><?php echo $dirRes[$i]['dir_name']; ?></option>
								<?php
								}
								?>
							</select>
						</div>
						<div class="main-row">
							<label>Business Name <?php echo getRequiredIcon()?></label>
							<input type="text" name="bsb_name" id="bsb_name" tabindex="2" value="<?php echo $bsb_name;?>" />
						</div>
						<div class="main-row">
							<label>User Name (Valid Email) <?php echo getRequiredIcon();?></label>
							<input type="text" name="bsb_username" id="bsb_username" tabindex="3" value="<?php echo $bsb_username;?>" onblur="javascript:checkUserEmail(this.value,'<?php echo $SECTION_AUTO_ID; ?>','BusinessSub');"/>
							<div id="userEmailmsg"></div>
						</div>
						<div class="main-row">
							<label>Confirm Email <?php echo getRequiredIcon()?></label>
							<input type="text" name="bsb_confirm" id="bsb_confirm" tabindex="4" value="<?php echo $bsb_username;?>" autocomplete="off" />
						</div>
						<div class="main-row">
							<label>Password <?php echo getRequiredIcon()?></label>
							<input type="password" name="bsb_password" id="bsb_password" tabindex="5" value="<?php echo $bsb_password;?>"  autocomplete="off"/>
						</div>
						<div class="main-row">
							<label>Confirm Password <?php echo getRequiredIcon()?></label>
							<input name="bsb_confirm_password" id="bsb_confirm_password" tabindex="6" value="<?php echo $bsb_password;?>" type="password">
						</div>
						<div class="main-row">
							<label>Secret Question <?php echo getRequiredIcon()?></label>
							<input type="text" name="bsb_question" id="bsb_question"  tabindex="7" value="<?php echo $bsb_question;?>"/>
						</div>
						<div class="main-row">
							<label>Answer <?php echo getRequiredIcon()?></label>
							<input type="text" name="bsb_answer" id="bsb_answer" tabindex="8" value="<?php echo $bsb_answer;?>" />
						</div>
						<div class="main-row">
							<label>Phone Number <?php echo getRequiredIcon()?></label>
							<input type="text" name="bsb_phone" id="bsb_phone" tabindex="9" value="<?php echo $bsb_phone;?>"/>
						</div>
						<div class="main-row">
							<label>&nbsp;</label>
							<input type="hidden" name="bsb_id" id="bsb_id" value="<?php echo $SECTION_AUTO_ID; ?>"/>
							<input type="submit" name="bsb_submit" id="bsb_submit" tabindex="9" value="Save"/>
						</div>
					</div>
				</form>
				</div>
				<div class="clr"></div>
			</div>
		</div>
	</aside>
</article>
</section>
<script>
$(document.body).on('click', "#bsb_submit", function(e){
	if($("#userEmailmsg label").text().length > 5)
	{
		return false;		
	}
	else
	{
		return true;
	}
});
$(document).ready(function(){
	$('.tooltip').tooltipster();
	$.validator.addMethod("phoneValidate", function(value, element) {
    	return this.optional(element) || /^[0-9\-\+\,\.\)\(\s]+$/i.test(value);
	}, "Only numbers,-,+ and , allowed");
	$("#manage_bussub").validate({
		rules: {
			bsb_dir : {
				required : true
			},
			bsb_name : {
				required : true
			},
			bsb_username : {
				required : true,
				email : true
			},
			bsb_confirm :{
				required: true,
				email : true,
				equalTo: "#bsb_username"
			},
			bsb_password : {
				<?php if($action != "Update"){ ?> required : true <?php } ?>
			},
			bsb_confirm_password : {
				<?php if($action != "Update"){ ?> required : true, <?php } ?>
				equalTo: "#bsb_password"
			},
			bsb_question : {
				required : true
			},
			bsb_answer : {
				required : true
			},
			bsb_phone : {
				required : true,
				phoneValidate : true
			}
		},
		messages: {
			bsb_dir : {
				required : "Please select directory name"
			},
			bsb_name : {
				required : "Please enter name"
			},
			bsb_username : {
				required : "Please enter email address",
				email : "Please enter valid email address"
			},
			bsb_confirm: {
				required: "Please enter email address",				
				email : "Please enter valid email address",
				equalTo: "Please enter the same email address"
			},
			bsb_password : {
				required : "Please enter password"
			},
			bsb_confirm_password: {
				required: "Please enter password",				
				equalTo: "Please enter the same password"
			},
			bsb_question : {
				required : "Please enter question"
			},
			bsb_answer : {
				required : "Please enter answer"
			},
			bsb_phone : {
				required : "Please enter phone no.",
				phoneValidate : "Please enter valid phone no."
			}
		}
	});
<?php if($action=="Update"){?>
	$("span.custom-combobox input").attr({"disabled": true});
	$("span.custom-combobox a.custom-combobox-toggle").hide();
	$("span.custom-combobox input").attr({"value": $("span.custom-combobox input").attr("placeholder")});
	$("span.custom-combobox a.custom-combobox-toggle").hide();
<?php } ?>
});
</script>
