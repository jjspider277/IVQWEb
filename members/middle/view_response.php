<script type="text/javascript" src="<?php echo ROOT_WWW; ?>js/canvasjs.min.js"></script>
<?php
	#################################################################
	$SECTION_FIELD_PREFIX='msg_';
	$SECTION_AUTO_ID=$_REQUEST[$SECTION_FIELD_PREFIX.'id'];
	$SECTION_WHERE=$SECTION_FIELD_PREFIX."id=".$SECTION_AUTO_ID."";
	$SECTION_VIEW_PAGE=MEB_VIEW_MESSAGING;
	$SECTION_MANAGE_PAGE=MEB_MANAGE_MESSAGING;
	$SECTION_TABLE=TBL_MEMBER_MESSAGING;
	$SECTION_NAME='Messaging';

	#################################################################

    $list_query     = getSelectList($SECTION_TABLE,$SECTION_FIELD_PREFIX);
    $list_query     .= "AND ".$SECTION_FIELD_PREFIX."id = '".$SECTION_AUTO_ID."'";
    $result_query   = $db->select($list_query);
    $cat_id         = $result_query[0]["buc_id"];
    $msg_message 	= trim($result_query[0][$SECTION_FIELD_PREFIX."message"]);
    $msg_file 		=	$result_query[0][$SECTION_FIELD_PREFIX."file"];
    $msg_company_title 	=	$result_query[0][$SECTION_FIELD_PREFIX."company_title"];
    $dir_quetion        =   $result_query[0][$SECTION_FIELD_PREFIX."has_question"];

    $que_query = 'SELECT * FROM '.TBL_QUESTION.' WHERE msg_id="'.$SECTION_AUTO_ID.'" AND user_id="'.getMemberSessionId().'"';
    $queResult  = $db->select($que_query);

    $msg_quetion_title='';
    $msg_quetion_id='';
    $queOptResult=array();
    $RespoResult=array();

    if(count($queResult)>0) {
        $msg_quetion_title = $queResult[0]['question'];
        $msg_quetion_id = $queResult[0]['que_id'];

        $que_opt_query = 'SELECT * FROM '.TBL_QUESTION_OPTION.' WHERE que_id="'.$msg_quetion_id.'"';
        $queOptResult  = $db->select($que_opt_query);

        $Respo_query = 'SELECT rep.*, opt.que_id, opt.que_id, opt_title, sub.sub_id, sub.sub_name FROM '.TBL_RESPONSE.' rep, '.TBL_QUESTION_OPTION.' opt, '.TBL_MEMBER_SUBSCRIBERS.' sub WHERE rep.que_id="'.$msg_quetion_id.'" AND rep.user_id=sub.sub_id AND rep.opt_id=opt.opt_id ORDER BY rep.res_id DESC';
        $RespoResult  = $db->select($Respo_query);

        $chart_array='';
        if(count($queOptResult)>0) {
            foreach($queOptResult as $queOpt) {
                $opt_id = $queOpt['opt_id'];
                $opt_title = $queOpt['opt_title'];

                $Respo_query_1 = 'SELECT COUNT(res_id) as total FROM '.TBL_RESPONSE.' rep, '.TBL_MEMBER_SUBSCRIBERS.' sub WHERE rep.que_id="'.$msg_quetion_id.'" AND rep.opt_id="'.$opt_id.'" AND rep.user_id=sub.sub_id';
                $RespoResult_1  = $db->select($Respo_query_1);

                $total = $RespoResult_1[0]['total'];
                $chart_array .= '{ label: "'.$opt_title.'", y: '.$total.' },';
            }
            $chart_array = substr($chart_array,0,-1);
        }
    }

?>

<div class="container search-group-section">
    <div class="row">
        <div class="subRow default_content responce_title">
            <h1><?php echo $msg_quetion_title; ?></h1>
        </div>
    </div>
</div>

<div class="container step-page-section">
    <div class="row">
        <div id="chartContainer" style="height: 300px; width: 100%;"></div>
         <div class="teamLists">
             <ul>
             <?php
                if(count($RespoResult)>0){
                    foreach($RespoResult as $Respo) {
                        $user_image='images/man421.png';
                        $user_name=$Respo['sub_name'];

                        $option_title=$Respo['opt_title'];
                        $datee = new DateTime($Respo['created_date']);
                        $repso_date= $datee->format('d/m/Y');
                ?>
                        <li>
                            <img src="<?php echo $user_image; ?>" />
                            <h2><?php echo $user_name; ?></h2>
                            <div class="user_cont">
                                <div class="name_user"><?php echo $option_title; ?></div>
                                <p><?php echo $repso_date; ?></p>
                            </div>
                        </li>
                <?php
                    }
                } else {
                    echo '<li>No Response</li>';
                }
             ?>
            <!--<li>
                    <img src="images/memeber-profile-icon.png" />
                    <div class="user_cont">
                        <h2>Lorem Ipsum is simply dummy text of the ?</h2>
                        <div class="name_user">Hello dev user</div>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500...</p>
                    </div>
                </li>-->
             </ul>
        </div>
</div>

<script>
$(document).ready(function(){
    var chart = new CanvasJS.Chart("chartContainer", {
        backgroundColor: "transparent",
        animationEnabled: true,  //change to false
        title:{
            text: "Survey Results",
            fontFamily: "Open sans",
            fontSize: 24  
        },
        data: [//array of dataSeries
            { //dataSeries object
                /*** Change type "column" to "bar", "area", "line" or "pie"***/
                type: "column",
                dataPoints: [<?php echo $chart_array; ?>]
            }
        ]
    });

    chart.render();
});
</script>