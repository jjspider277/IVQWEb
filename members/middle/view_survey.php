<section>
		<article id="page" >
			<header>
				<?php include_once(FRT_SECTION_DIR.'headerinner.php');?>
				<div class="tab_content_holder">
					<div class="tab_content_holder_inner">						
							<div class="first-col">
								 <div class="main-row">
    								 <label>Subscriber Name</label>
    								 <input type="text"></input>
								 </div>								 
							</div>
							<div class="middle-col">
								 <div class="main-row">
    								 <label>Address</label>
    								 <input type="text"></input>
								 </div>								 
							</div>
							<div class="last-col">
								 <div class="main-row city">
								 	  <label>City</label>
								 	  <input type="text"></input>
								 </div>
								 <div class="main-row zip">
								 	  <label>State</label>
								 	  <input type="text"></input>
								 </div>
								 <div class="clr"></div>																 
							</div>
							<div class="clr"></div>
                            <div class="bottomDiv">
                                <div class="main-row state">                            	
                                      <label>Zip</label>
                                      <input type="text">
                                </div>
                                <div class="main-row phone">                            	
                                      <label>Phone Number</label>
                                      <input type="text">
                                </div>
                                <div class="main-row phone">                            	
                                      <label>Email</label>
                                      <input type="text">
                                </div>
                                <div class="main-row search">
								 	  <label>&nbsp;</label>
								 	  <input type="submit" value="Search">
								 </div>
                            </div>
                            <div class="clr"></div>
                            <div class="bottomDiv">
                                <div class="main-row attach">                            	
                                      <label>Attach File</label>
                                      <input type="text" placeholder="Select file path">
                                      <input type="button" value="Attach File">
                                </div>
                                <div class="main-row phone">                            	
                                      <label>&nbsp;</label>
                                      <input type="button" value="Upload subscribers">
                                </div>                                
                            </div>
                            <div class="clr"></div>                          
							
					</div>
				</div>
			</header>
			
			<nav>
				 <div class="shorting">
				 	  <ul>
					  	  <li><a href="#">a</a></li>
						  <li><a href="#">b</a></li>
						  <li><a href="#">c</a></li>
						  <li><a class="active" href="#">d</a></li>
						  <li><a href="#">e</a></li>
						  <li><a href="#">f</a></li>
						  <li><a href="#">g</a></li>
						  <li><a href="#">h</a></li>
						  <li><a href="#">i</a></li>
						  <li><a href="#">j</a></li>
						  <li><a href="#">k</a></li>
						  <li><a href="#">l</a></li>
						  <li><a href="#">all</a></li>
					  </ul>
                      <div class="export add"><a class="tooltip" href="#" title="Add"><img src="../images/add-member.png"></a></div>
					  <div class="export"><a href="#" class="tooltip" title="Export"><img src="../images/export.png" /></a></div>
				 </div>
			</nav>
			<div class="clr"></div>
			<aside>
				 <div class="tab_content_holder second">
				 	  <div class="tab_content_holder_inner">
					  	   <table width="100%" cellspacing="0" cellpadding="0">
						   		<tr>						
									<th width="16%">Directory Name</th>
									<th width="16%">Address</th>
									<th width="9%">State</th>
									<th width="8%">Zip</th>
									<th width="10%">Phone</th>
									<th width="15%">Email</th>
									<th width="15%">Website</th>
									<th width="11%">Action</th>
								</tr>
								<tr>
									<td>Directory Name</td>
									<td>Address</td>
									<td>NY</td>
									<td>10021</td>
									<td>00-000-00</td>
									<td><a href="mailto:info@b2bms.com">info@b2bms.com</a></td>
									<td><a href="#">www.google.com</a></td>
									<td>
										<a href="#" class="tooltip" title="Edit"><img src="../images/right.png" /></a>
										<a href="#" class="tooltip" title="Delete"><img src="../images/delete.png" /></a>
										<a href="#" class="tooltip" title="View"><img src="../images/equale.png" /></a>
									</td>
								</tr>
								<tr class="light">
									<td>Directory Name</td>
									<td>Address</td>
									<td>NY</td>
									<td>10021</td>
									<td>00-000-00</td>
									<td><a href="mailto:info@b2bms.com">info@b2bms.com</a></td>
									<td><a href="#">www.google.com</a></td>
									<td>
										<a href="#" class="tooltip" title="Edit"><img src="../images/right.png" /></a>
										<a href="#" class="tooltip" title="Delete"><img src="../images/delete.png" /></a>
										<a href="#" class="tooltip" title="View"><img src="../images/equale.png" /></a>
									</td>
								</tr>
								<tr>
									<td>Directory Name</td>
									<td>Address</td>
									<td>NY</td>
									<td>10021</td>
									<td>00-000-00</td>
									<td><a href="mailto:info@b2bms.com">info@b2bms.com</a></td>
									<td><a href="#">www.google.com</a></td>
									<td>
										<a href="#" class="tooltip" title="Edit"><img src="../images/right.png" /></a>
										<a href="#" class="tooltip" title="Delete"><img src="../images/delete.png" /></a>
										<a href="#" class="tooltip" title="View"><img src="../images/equale.png" /></a>
									</td>
								</tr>
								<tr class="light">
									<td>Directory Name</td>
									<td>Address</td>
									<td>NY</td>
									<td>10021</td>
									<td>00-000-00</td>
									<td><a href="mailto:info@b2bms.com">info@b2bms.com</a></td>
									<td><a href="#">www.google.com</a></td>
									<td>
										<a href="#" class="tooltip" title="Edit"><img src="../images/right.png" /></a>
										<a href="#" class="tooltip" title="Delete"><img src="../images/delete.png" /></a>
										<a href="#" class="tooltip" title="View"><img src="../images/equale.png" /></a>
									</td>
								</tr>
								<tr>
									<td>Directory Name</td>
									<td>Address</td>
									<td>NY</td>
									<td>10021</td>
									<td>00-000-00</td>
									<td><a href="mailto:info@b2bms.com">info@b2bms.com</a></td>
									<td><a href="#">www.google.com</a></td>
									<td>
										<a href="#" class="tooltip" title="Edit"><img src="../images/right.png" /></a>
										<a href="#" class="tooltip" title="Delete"><img src="../images/delete.png" /></a>
										<a href="#" class="tooltip" title="View"><img src="../images/equale.png" /></a>
									</td>
								</tr>
								<tr class="light">
									<td>Directory Name</td>
									<td>Address</td>
									<td>NY</td>
									<td>10021</td>
									<td>00-000-00</td>
									<td><a href="mailto:info@b2bms.com">info@b2bms.com</a></td>
									<td><a href="#">www.google.com</a></td>
									<td>
										<a href="#" class="tooltip" title="Edit"><img src="../images/right.png" /></a>
										<a href="#" class="tooltip" title="Delete"><img src="../images/delete.png" /></a>
										<a href="#" class="tooltip" title="View"><img src="../images/equale.png" /></a>
									</td>
								</tr>
								<tr>
									<td>Directory Name</td>
									<td>Address</td>
									<td>NY</td>
									<td>10021</td>
									<td>00-000-00</td>
									<td><a href="mailto:info@b2bms.com">info@b2bms.com</a></td>
									<td><a href="#">www.google.com</a></td>
									<td>
										<a href="#" class="tooltip" title="Edit"><img src="../images/right.png" /></a>
										<a href="#" class="tooltip" title="Delete"><img src="../images/delete.png" /></a>
										<a href="#" class="tooltip" title="View"><img src="../images/equale.png" /></a>
									</td>
								</tr>
								<tr class="light">
									<td>Directory Name</td>
									<td>Address</td>
									<td>NY</td>
									<td>10021</td>
									<td>00-000-00</td>
									<td><a href="mailto:info@b2bms.com">info@b2bms.com</a></td>
									<td><a href="#">www.google.com</a></td>
									<td>
										<a href="#" class="tooltip" title="Edit"><img src="../images/right.png" /></a>
										<a href="#" class="tooltip" title="Delete"><img src="../images/delete.png" /></a>
										<a href="#" class="tooltip" title="View"><img src="../images/equale.png" /></a>
									</td>
								</tr>  		  
						   </table>
						   <div class="clr"></div>
						   <nav>
            				 <div class="paging">
            				 	  <ul>
            					  	  <li><a class="active" href="#">1</a></li>
            						  <li><a href="#">2</a></li>
            						  <li><a href="#">3</a></li>
            						  <li><a href="#">4</a></li>
            						  <li><a href="#">5</a></li>
            						  <li><a href="#">6</a></li>
            						  <li><a href="#">7</a></li>
            						  <li><a href="#">8</a></li>
            						  <li><a href="#">9</a></li>
            						  <li><a href="#">10</a></li>            						  
            					  </ul>            					  
            				 </div>
               			   </nav>
					  </div>
				 </div>               				   
			</aside>
			
		</article>
	</section>