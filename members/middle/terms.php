<div class="container group-setion">
    <div class="row">
        <div class="subRow default_content">
            <h1>Terms of Use</h1>
            <p>Please read these Terms of Use ("Terms", "Terms of Use") carefully before using the <b>IVQ Mobile</b> operated by Intelli-Touch Apps.</p>
            <p>Your access to, and you use of, the Service is conditioned on your acceptance of, and your compliance with, these Terms. These Terms apply to all visitors, users and others who access and/or use the Service. </p>
            <p>By accessing, or using, the Service you agree to be bound by these Terms. If you disagree with any part of these terms then you may not access the Service.</p>

            <h3>Termination</h3>
            <p>We may terminate, or suspend, access to our Service immediately, without prior notice or liability, for any reason whatsoever, including, without limitation, if you breach the Terms.</p>
            <p>All provisions of the Terms, which by their nature should survive termination, shall survive termination, including, without limitation, ownership provisions, warranty disclaimers, indemnity and limitations of liability.</p>

            <h3>Links To Other Web Sites</h3>
            <p>Our Services may contain links to third party websites, or services that are not owned or controlled by Intelli-Touch Apps.</p>
            <p>Intelli-Touch Apps has no control over, and assumes no responsibility for, the content, privacy policies, or practices of any third party website or services. You further acknowledge and agree that Intelli-Touch Apps shall not be responsible, or liable, directly, or indirectly, for any damage or loss caused, or alleged to be caused, by, or in connection with, use of our reliance on any such content, goods or services available on, or through, any such websites or services.</p>
            <p>We strongly advise you to read the terms and conditions, and privacy policies, of any third-party website or services that you visit.</p>

            <h3>Governing Law</h3>
            <p>These Terms shall be governed and construed in accordance with the laws of the United States.</p>
            <p>Our failure to enforce any right, or provision, of these Terms will not be considered a waiver of those rights. If any provision of these Terms is held to be invalid, or unenforceable by a court, the remaining provisions of these Terms will remain in effect. These Terms constitute the entire agreement between us regarding our Service, and supersede, and replace, any prior agreements we might have between us regarding the Service.</p>

            <h3>Changes</h3>
            <p>We reserve the right, at our sole discretion, to modify or replace these Terms at any time. If a revision is material we will try to provide at least 30 day notice prior to any new terms taking effect. What constitutes a material  change will be determined by our sole discretion. By continuing to access, or use our Service after those revisions become effective, you agree to be bound by the revised terms. If you do not agree with the new terms, please stop using the Service.</p>

            <h3>Contact Us</h3>
            <p>If you have any questions about these Terms, please contact us at <a href="http://www.intelli-services.com" target="_blank">www.intelli-services.com.</a></p>

        </div>
	</div>
</div>