<?php
if(isset($_POST)){	
	$data = $_POST;	
}
if ($data['change_password'] == 'Change')
{
	$uname = $data['change_username'];
	$upass = md5($data['change_old_pass']);

	$mebType = getMemberType();
	if($mebType!="Business")
	{
		$sql="select meb_id from ".TBL_MEMBER." where meb_username ='".trim(addslashes($uname))."' AND meb_password='".$upass."' AND meb_id=".getMemberSessionId();
	}
	else
	{
		$sql="select sub_id from ".TBL_MEMBER_SUBSCRIBERS." where sub_email ='".trim(addslashes($uname))."' AND sub_password='".$upass."' AND sub_type='Business'  AND sub_id=".getMemberSessionId();
	}
	$change_data = $db->select($sql);
	if(!empty($change_data))
	{
		unset($add_values);
		if($mebType!="Business")
		{
			$add_values['meb_password'] 		= md5($data["change_new_pass"]);
			$add_values['meb_updated_id'] 		= $mebId;
			$add_values['meb_updated_date'] 	= date("Y-m-d H:i:s");
			$SECTION_WHERE = "meb_id=".$change_data[0]["meb_id"];
			$id = $db->updateData(TBL_MEMBER, $add_values,$SECTION_WHERE);
		}
		else
		{
			$add_values['sub_password'] 		= md5($data["change_new_pass"]);
			$add_values['sub_updated_id'] 		= $mebId;
			$add_values['sub_updated_date'] 	= date("Y-m-d H:i:s");
			$SECTION_WHERE = "sub_id=".$change_data[0]["sub_id"];
			$id = $db->updateData(TBL_MEMBER_SUBSCRIBERS, $add_values,$SECTION_WHERE);
		}
		$_SESSION["msg"] = "Successfully change your password";
		$URL = getMemberURL(MEB_CHANGE_PASSWORD);
		redirect($URL);
		exit;
	}
	else
	{
		$_SESSION["err"] = "Wrong username or password...";
		$_SESSION['add_detail_error'][]=$_POST;
		$URL = getMemberURL(MEB_CHANGE_PASSWORD);
		redirect($URL);
		exit;
	}
}	
if($_SESSION['add_detail_error'][0]!="")
{
	$result_query[]=$_SESSION['add_detail_error'][0];
	$change_username = $result_query[0]["change_username"];
	$change_old_pass = $result_query[0]["change_old_pass"];
	$change_new_pass = $result_query[0]["change_new_pass"];
	$change_conf_pass= $result_query[0]["change_conf_pass"];
	unset($_SESSION['add_detail_error']);
}
?>
<section>
	<article id="page">
	<header>
		<?php include_once(MEB_SECTION_DIR.'headerinner.php');?>
		<div class="tab_content_holder">
			<div class="tab_content_holder_inner">
				<div class="block-part">
					<?php if($_SESSION["err"]!="") { ?>
						<div id="mesg" style="color:#FF0000;padding: 0px 0px 15px 30px;font-size: 14px;">
						<?php
							echo $_SESSION["err"];
							$_SESSION["err"]="";
						?>
						</div>
					<script type="text/javascript">$("#mesg").delay(5000).fadeOut();</script>
					<?php } ?>
					<?php if($_SESSION["msg"]!="") { ?>
						<div id="mesgSuc" style="color: #697F08;padding: 0px 0px 15px 30px;font-size: 14px;">
							<?php
							echo $_SESSION["msg"];
							$_SESSION["msg"]="";
							?>
						</div>
					<script type="text/javascript">$("#mesgSuc").delay(5000).fadeOut();</script>
					<?php } ?>
				<form name="change_pass_frm" id="change_pass_frm" action="" method="post" autocomplete="off" >
					<div class="main-row">
						<label>User Name (Valid Email) <?php echo getRequiredIcon()?></label>
						<input type="text" name="change_username" id="change_username" tabindex="1" value="<?php echo $change_username; ?>" />
					</div>
					<div class="main-row">
						<label>Old Password <?php echo getRequiredIcon()?></label>
						<input type="password" name="change_old_pass" id="change_old_pass" tabindex="2" value="<?php echo $change_old_pass; ?>" />
					</div>
					<br>
					<div class="main-row">
						<label>New Password <?php echo getRequiredIcon()?></label>
						<input type="password" name="change_new_pass" id="change_new_pass" tabindex="3" value="<?php echo $change_new_pass; ?>" />
					</div>
					<div class="main-row">
						<label>Confirm New Password <?php echo getRequiredIcon()?></label>
						<input type="password" name="change_conf_pass" id="change_conf_pass" tabindex="4" value="<?php echo $change_conf_pass; ?>" />
					</div>
					<div class="main-row">
						<label>&nbsp;</label>
						<input type="submit" id="change_password" tabindex="5" name="change_password" value="Change" />
					</div>
				</form>
				</div>
			</div>
			<div class="clr"></div>
		</div>
	</header>
	</article>
</section>
<script type="text/javascript">
$(document).ready(function(){
	$("#change_pass_frm").validate({
		rules: {
			change_username : {
				required : true,
				email : true
			},
			change_old_pass : {
				required : true
			},
			change_new_pass : {
				required : true
			},
			change_conf_pass : {
				required : true,
				equalTo : "#change_new_pass"
			}
		},
		messages: {
			change_username : {
				required : "Please enter username",
				email : "Please enter valid email id"
			},
			change_old_pass : {
				required : "Please enter old password"
			},
			change_new_pass : {
				required : "Please enter new password"
			},
			change_conf_pass : {
				required : "Please enter confirm password",
				equalTo : "Please enter same confirm password"
			}
		}
	});
});
</script>