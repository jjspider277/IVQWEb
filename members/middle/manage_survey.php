<?php
	error_reporting(0);
	#################################################################
	$SECTION_FIELD_PREFIX='sub_';
	$SECTION_AUTO_ID=$_REQUEST[$SECTION_FIELD_PREFIX.'id'];
	$SECTION_WHERE=$SECTION_FIELD_PREFIX."id=".$SECTION_AUTO_ID."";
	$SECTION_VIEW_PAGE=VIEW_SUBSCRIBERS;
	$SECTION_MANAGE_PAGE=MANAGE_SUBSCRIBERS;
	$SECTION_TABLE=TBL_MEMBER_SUBSCRIBERS;
	$SECTION_NAME='Subscribers';

	#################################################################
	if($_POST['method'] != "")
		$action =  $_POST['method'];
	else if($_REQUEST['method'] != "")
		$action =  $_REQUEST['method'];

	if(isset($_POST)){	
		$data = $_POST;	
	}
	if ($data['sub_submit'] == 'Save Subscribers')
	{
		$add_values[$SECTION_FIELD_PREFIX.'meb_id'] 		= trim($data["sub_meb_id"]);
		$add_values[$SECTION_FIELD_PREFIX.'name'] 			= trim($data["sub_name"]);
		$add_values[$SECTION_FIELD_PREFIX.'city'] 			= trim($data["sub_city"]);
		$add_values[$SECTION_FIELD_PREFIX.'email'] 			= trim($data["sub_email"]);
		$add_values[$SECTION_FIELD_PREFIX.'phone'] 			= trim($data["sub_phone"]);
		$add_values[$SECTION_FIELD_PREFIX.'state'] 			= trim($data["sub_state"]);
		$add_values[$SECTION_FIELD_PREFIX.'filename'] 		= $sub_filename="";
		$status = "Active";
		
		if($action != 'Update'){	
			$add_values[$SECTION_FIELD_PREFIX.'status'] 		= $status;									
		    $add_values[$SECTION_FIELD_PREFIX . 'created_id'] 	= $_SESSION['meb_id'];
    	    $add_values[$SECTION_FIELD_PREFIX . 'created_date'] = date("Y-m-d H:i:s");	
    	    $GPDetail_result = $db->insertData($SECTION_TABLE, $add_values);
			$_SESSION['msg']  =   "Subscribers has been inserted successfully.";  
    	}else{
    	   	$add_values[$SECTION_FIELD_PREFIX . 'updated_id'] 			= $_SESSION['meb_id'];
        	$add_values[$SECTION_FIELD_PREFIX . 'updated_date'] 		= date("Y-m-d H:i:s");	
        	$GPDetail_result = $db->updateData($SECTION_TABLE, $add_values, $SECTION_WHERE);
    		$_SESSION['msg']  =   "Subscribers has been updated successfully.";  
    	}
        $URL = getMemberURL($SECTION_VIEW_PAGE); 
		redirect($URL);
    	exit;
	}
	if($action=='Edit')
	{
		$list_query = getSelectList($SECTION_TABLE,$SECTION_FIELD_PREFIX);
		$list_query .= "AND ".$SECTION_FIELD_PREFIX."id = '".$SECTION_AUTO_ID."'";
		$result_query = $db->select($list_query);
		$sub_name		=	$result_query[0][$SECTION_FIELD_PREFIX."name"];
		$sub_city		=	$result_query[0][$SECTION_FIELD_PREFIX."city"];
		$sub_email		=	$result_query[0][$SECTION_FIELD_PREFIX."email"];
		$sub_phone		=	$result_query[0][$SECTION_FIELD_PREFIX."phone"];
		$sub_state		=	$result_query[0][$SECTION_FIELD_PREFIX."state"];
		$sub_filename	=	$result_query[0][$SECTION_FIELD_PREFIX."filename"];
		$action = "Update";
		$action_url = getMemberURL($SECTION_MANAGE_PAGE,$action,$SECTION_WHERE);
	}	
	if($action=='')
	{
		$status = "Active";
		$action = "Add";
		$action_url = getMemberURL($SECTION_MANAGE_PAGE,$action);
		
		if($_SESSION['add_detail_error'][0]!="")
		{
			foreach($_SESSION['add_detail_error'][0] as $key => $value)
			{
				$result_query[0][$key]=stripslashes($value);
			}
			$sub_name		=	$result_query[0][$SECTION_FIELD_PREFIX."name"];
			$sub_city		=	$result_query[0][$SECTION_FIELD_PREFIX."city"];
			$sub_email		=	$result_query[0][$SECTION_FIELD_PREFIX."email"];
			$sub_phone		=	$result_query[0][$SECTION_FIELD_PREFIX."phone"];
			$sub_state		=	$result_query[0][$SECTION_FIELD_PREFIX."state"];
			$sub_filename	=	$result_query[0][$SECTION_FIELD_PREFIX."filename"];
			unset($_SESSION['add_detail_error']);
		}
	}
	if($_POST["sub_import"]=="Import" && $_REQUEST["method"]=="import")
	{
		if((!empty($_FILES["import_sub"])) && ($_FILES['import_sub']['error'] == 0))
		{
			$filename = basename($_FILES['import_sub']['name']);
			$newname = SUBSCRIBERS_UPLOAD.$filename;
			if ((move_uploaded_file($_FILES['import_sub']['tmp_name'],$newname)))
			{
				include(ROOT_DIR.'include/Classes/PHPExcel.php');
				$objReader = new PHPExcel_Reader_Excel5();
				$objReader->setReadDataOnly(true);
				$objPHPExcel = $objReader->load($newname);
				$rowIterator = $objPHPExcel->getActiveSheet()->getRowIterator();
				$array_data = array();
				foreach($rowIterator as $row){
					$cellIterator = $row->getCellIterator();
					$cellIterator->setIterateOnlyExistingCells(false); // Loop all cells, even if it is not set
					if(1 == $row->getRowIndex ()) continue;//skip first row
					$rowIndex = $row->getRowIndex ();
					$array_data[$rowIndex] = array('A'=>'', 'B'=>'','C'=>'','D'=>'','E'=>'','F'=>'');
					foreach ($cellIterator as $cell) {
				    	if('A' == $cell->getColumn()){
				    		$array_data[$rowIndex][$cell->getColumn()] = $cell->getCalculatedValue();
				    	} else if('B' == $cell->getColumn()){
				          $array_data[$rowIndex][$cell->getColumn()] = $cell->getCalculatedValue();
				    	} else if('C' == $cell->getColumn()){
							$array_data[$rowIndex][$cell->getColumn()] = $cell->getCalculatedValue();
						} else if('D' == $cell->getColumn()){
							$array_data[$rowIndex][$cell->getColumn()] = $cell->getCalculatedValue();
				        } else if('E' == $cell->getColumn()){
							$array_data[$rowIndex][$cell->getColumn()] = $cell->getCalculatedValue();
				        } else if('F' == $cell->getColumn()){
							$array_data[$rowIndex][$cell->getColumn()] = $cell->getCalculatedValue();
				        }
				    }
				}
				/*echo "<pre>";
				print_r($array_data);
				echo "</pre>";*/
				$i=2;
				while($i<=count($array_data))
				{
					$sql="insert into ".TBL_MEMBER_SUBSCRIBERS."(sub_meb_id,sub_name,sub_city,sub_email,sub_phone,sub_state,sub_created_id,sub_created_date) values('".$array_data[$i]["A"]."','".$array_data[$i]["B"]."','".$array_data[$i]["C"]."','".$array_data[$i]["D"]."','".$array_data[$i]["E"]."','".$array_data[$i]["F"]."','".$_SESSION['meb_id']."','".date("Y-m-d H:i:s")."')";
					$insertedid = $db->Query($sql);
					$i++;
				}
				unlink($newname);
				$_SESSION["msg"]="Subscribers has been imported successfully.";
			}
			else
			{
				$_SESSION["error"] = "Error: A problem occurred during file upload!";
			}
		}
		else
		{
			$_SESSION["error"] = "Data has not b";
		}
		$URL = getMemberURL($SECTION_VIEW_PAGE); 
		redirect($URL);
	}
?>
<div class="contentSection">
	<div class="contentSection-inner">
		<div class="subRow">
			<a href="<?php echo MEB_INDEX_PARAMETER.MEB_HOME?>"><div class="fr vinixLogo"></div></a>
			<div class="fl pageTitle edit"> Manage Directory </div>
			<div style="float:right" class="page_title_text" id="AddIcon"> 
				<a href=<?php echo MEB_INDEX_PARAMETER.$SECTION_VIEW_PAGE?>><img src="<?php echo IMG_WWW; ?>edit.png" border="0" title="View Directory"></a>
			</div>
		</div>
		<div class="subRow">
			<input type="radio" name="import" value="textImport" onchange="return importData(this.value);" checked="checked" > Insert Data
			<input type="radio" name="import" value="excelImport"onchange="return importData(this.value);" > Import Excel
		</div>
		<div id="textImport">
		<form name="manage_subscribers" enctype="multipart/form-data" method="post" action="<?php echo $action_url; ?>" id="manage_subscribers">
			<div class="subRow">
				<div class="enterTitle">
					Name <?php echo getRequiredIcon()?> : 
					<input type="text" name="sub_name" id="sub_name" value="<?php echo $sub_name;?>" size="70"/>
				</div>
			</div>
			<div class="subRow">
				<div class="enterTitle">
					City <?php echo getRequiredIcon()?> : 
					<input type="text" name="sub_city" id="sub_city" value="<?php echo $sub_city;?>" size="70"/>
				</div>
			</div>
			<div class="subRow">
				<div class="enterTitle">
					Email <?php echo getRequiredIcon()?> : 
					<input type="text" name="sub_email" id="sub_email" value="<?php echo $sub_email;?>" size="70"/>
				</div>
			</div>
			<div class="subRow">
				<div class="enterTitle">
					Phone No <?php echo getRequiredIcon()?> : 
					<input type="text" name="sub_phone" id="sub_phone" value="<?php echo $sub_phone;?>" size="70"/>
				</div>
			</div>
			<div class="subRow">
				<div class="enterTitle">
					State <?php echo getRequiredIcon()?> : 
					<input type="text" name="sub_state" id="sub_state" value="<?php echo $sub_state;?>" size="70"/>
				</div>
			</div>
			<div class="fl updateBut">
				<input name="sub_id" id="sub_id" value="<?php echo $SECTION_AUTO_ID; ?>" type="hidden">
				<input name="sub_meb_id" id="sub_meb_id" value="<?php echo $_SESSION['meb_id']; ?>" type="hidden">
				<input type="hidden" name="method" id="method" value="<?php echo $action; ?>" />
				<input type="submit" name="sub_submit" id="sub_submit" value="Save Subscribers" />
			</div>
		</form>
		</div>
		<div id="excelImport" style="display:none;">
		<form name="import_subscribers" enctype="multipart/form-data" method="post" action="<?php echo getMemberURL($SECTION_MANAGE_PAGE,"import"); ?>" id="import_subscribers">
			<div class="subRow">
				<div class="enterTitle">
					Imports Files <?php echo getRequiredIcon(); ?> : 
					<input type="file" name="import_sub" id="import_sub" />					
				</div>
			</div>
			<div class="fl updateBut">
				<input type="submit" name="sub_import" id="sub_import" value="Import" />
			</div>
		</form>
		</div>
	</div>
    <div class="cl"></div>
</div>
<script>
function importData(dataval)
{
	if(dataval=="excelImport")
	{
		$("#excelImport").css({"display":"block"});
		$("#textImport").css({"display":"none"});
	}
	else
	{
		$("#excelImport").css({"display":"none"});
		$("#textImport").css({"display":"block"});
	}
}
$(document).ready(function(){
	$.validator.addMethod("phoneValidate", function(value, element) {
    	return this.optional(element) || /^[0-9\-\+\,\.\)\(\s]+$/i.test(value);
	}, "Only numbers,-,+ and , allowed");
	/*$.validator.addMethod("validImage", function(value, element) {
    	return this.optional(element) || /\.(gif|jpg|jpeg|png|GIF|JPG|JPEG|PNG)$/i.test(value);
	}, "Please select only image");*/
	$.validator.addMethod("validExcel", function(value, element) {
    	return this.optional(element) || /\.(xls|xlsx|XLS|XLSX)$/i.test(value);
	}, "Please select only image");
	$("#import_subscribers").validate({
		rules: {
			import_sub : {
				required : true,
				validExcel : true
			}
		},
		messages: {
			import_sub : {
				required : "Please select subscribers file",
				validExcel : "Please select only xls or xlxs file"
			}
		}
	});
	$("#manage_subscribers").validate({
		rules: {
			sub_name : {
				required : true
			},
			sub_city : {
				required : true
			},
			sub_email : {
				required : true,
				email : true
			},
			sub_phone : {
				required : true,
				phoneValidate : true
			},
			sub_state : {
				required : true
			}
		},
		messages: {
			sub_name : {
				required : "Please enter subscribers name"
			},
			sub_city : {
				required : "Please enter subscribers city"
			},
			sub_email : {
				required : "Please enter subscribers email",
				email : "Please enter valid subscribers email"
			},
			sub_phone : {
				required : "Please enter subscribers phone no.",
				phoneValidate : "Please enter valid subscribers phone no."
			},
			sub_state : {
				required : "Please enter subscribers state"
			}
		}
	});
});
</script>