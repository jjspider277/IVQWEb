<script type="text/javascript" src="<?php echo AJAX_FOLDER_WWW; ?>getAjaxMembers.js"></script>
<?php 
#################################################################
    $SECTION_FIELD_PREFIX='bus_';
	$SECTION_AUTO_ID=$_REQUEST[$SECTION_FIELD_PREFIX.'id'];
	$SECTION_WHERE=$SECTION_FIELD_PREFIX."id=".$SECTION_AUTO_ID."";
	$SECTION_VIEW_PAGE=MEB_VIEW_BUSINESS;
	$SECTION_MANAGE_PAGE=MEB_MANAGE_BUSINESS;
	
	$SECTION='Business';

	$type = getMemberType();
	if($type!="Business")
	{
		$SECTION_TABLE= TBL_MEMBER_BUSINESS;
		$padding = "style='padding: 22px;'";
	}
	else
	{
		$SECTION_TABLE= TBL_MEMBER_BUSINESS_SUB;
		$padding = "style='padding: 1.5px;'";
	}
	
	//State Listing...
	$state_fields = array("sta_id","sta_name");
	$state_where  = "sta_status = 'Active'";
	$staRes 	= $db->selectData(TBL_STATE,$state_fields,$state_where,$extra="",2);
#################################################################
if($_POST["bus_import"]=="Upload Business" && $_REQUEST["method"]=="import")
{
	if((!empty($_FILES["import_bus"])) && ($_FILES['import_bus']['error'] == 0))
	{
		$filename = basename($_FILES['import_bus']['name']);
		$newname = UPLOAD_DIR_BUSINESS_FILE.$filename;
		if ((move_uploaded_file($_FILES['import_bus']['tmp_name'],$newname)))
		{
			include(ROOT_DIR.'include/Classes/PHPExcel.php');
			$objReader = new PHPExcel_Reader_Excel5();
			$objReader->setReadDataOnly(true);
			$objPHPExcel = $objReader->load($newname);
			$rowIterator = $objPHPExcel->getActiveSheet()->getRowIterator();
			$array_data = array();
			$flag = false;
			foreach($rowIterator as $row)
			{
				$cellIterator = $row->getCellIterator();
				$cellIterator->setIterateOnlyExistingCells(false); // Loop all cells, even if it is not set
				$rowIndex = $row->getRowIndex();
				$array_data[$rowIndex] = array('A'=>'', 'B'=>'','C'=>'','D'=>'','E'=>'','F'=>'','G'=>'','H'=>'','I'=>'','J'=>'','K'=>'','L'=>'');
				foreach ($cellIterator as $cell) {
                    if('A' == $cell->getColumn()){
                        $array_data[$rowIndex][$cell->getColumn()] = $cell->getValue();
                    } else if('B' == $cell->getColumn()){
                        $array_data[$rowIndex][$cell->getColumn()] = $cell->getValue();
                    } else if('C' == $cell->getColumn()){
                        $array_data[$rowIndex][$cell->getColumn()] = $cell->getValue();
                    } else if('D' == $cell->getColumn()){
                        $array_data[$rowIndex][$cell->getColumn()] = $cell->getValue();
                    } else if('E' == $cell->getColumn()){
                        $array_data[$rowIndex][$cell->getColumn()] = $cell->getValue();
                    } else if('F' == $cell->getColumn()){
                        $array_data[$rowIndex][$cell->getColumn()] = $cell->getValue();
                    } else if('G' == $cell->getColumn()){
                        $array_data[$rowIndex][$cell->getColumn()] = $cell->getValue();
                    } else if('H' == $cell->getColumn()){
                        $array_data[$rowIndex][$cell->getColumn()] = $cell->getValue();
                    } else if('I' == $cell->getColumn()){
                        $array_data[$rowIndex][$cell->getColumn()] = $cell->getValue();
                    } else if('J' == $cell->getColumn()){
                        $array_data[$rowIndex][$cell->getColumn()] = $cell->getValue();
                    } else if('K' == $cell->getColumn()){
                        $array_data[$rowIndex][$cell->getColumn()] = $cell->getValue();
                    } else if('L' == $cell->getColumn()){
                        $array_data[$rowIndex][$cell->getColumn()] = $cell->getValue();
                    }
			    }
			}
   
			/*echo "<pre>";
			print_r($array_data);
			echo "</pre>";
			exit;*/
			if($array_data[1]["A"]=="Business Title" && $array_data[1]["B"]=="Company" && $array_data[1]["C"]=="First Name" && $array_data[1]["D"]=="Last Name" && $array_data[1]["E"]=="Business Address" && $array_data[1]["F"]=="Business City" && $array_data[1]["G"]=="Business State" && $array_data[1]["H"]=="Business Phone" && $array_data[1]["I"]=="Business Zipcode" && $array_data[1]["J"]=="Business Fax" && $array_data[1]["K"]=="Business Email" && $array_data[1]["L"]=="Business About")
			{
				$i=2;
				$ins = 1;
				$not_ins = 0;
				$total_rec = count($array_data);
				if($total_rec==1)
				{
					$total_rec=2;
				}
				while($i<=$total_rec)
				{
					if($array_data[$i]["A"]!="" && $array_data[$i]["C"]!="" && $array_data[$i]["D"]!="" && $array_data[$i]["E"]!="" && $array_data[$i]["F"]!="" && $array_data[$i]["G"]!="" && $array_data[$i]["H"]!="" && $array_data[$i]["I"]!="" && $array_data[$i]["J"]!="" && $array_data[$i]["K"]!="" && $array_data[$i]["L"]!="")
					{
						$state_fields = array("sta_id");
						$state_where  = "sta_sort_name = '".strtoupper($array_data[$i]["G"])."'";
						$staRes 	= $db->selectData(TBL_STATE,$state_fields,$state_where,$extra="",2);
						$sql="insert into ".TBL_MEMBER_BUSINESS."(bus_meb_id,bus_dir_id,bus_title,bus_company,bus_name,bus_lname,bus_address1,bus_city,bus_sta_id,bus_phone,bus_zip,bus_fax,bus_email,bus_about,bus_status,bus_created_id,bus_created_date) values('".$mebId."','".$dirId."','".mysql_real_escape_string($array_data[$i]["A"])."','".mysql_real_escape_string($array_data[$i]["B"])."','".mysql_real_escape_string($array_data[$i]["C"])."','".mysql_real_escape_string($array_data[$i]["D"])."','".mysql_real_escape_string($array_data[$i]["E"])."','".mysql_real_escape_string($array_data[$i]["F"])."','".$staRes[0]['sta_id']."','".mysql_real_escape_string($array_data[$i]["H"])."','".mysql_real_escape_string($array_data[$i]["I"])."','".mysql_real_escape_string($array_data[$i]["J"])."','".mysql_real_escape_string($array_data[$i]["K"])."','".mysql_real_escape_string($array_data[$i]["L"])."','Active','".$mebId."','".date("Y-m-d H:i:s")."')";
						//echo $sql;exit;
						$insert_res = $db->Query($sql);
						$ins = $ins+1;
					}
					else
					{
						$not_ins = $not_ins+1;
					}
					$i++;
				}
				//$_SESSION["msg"]="Subscribers has been imported successfully. Total : ".$total_rec.". Inserted : ".$ins.". Not Inserted : $not_ins";
			}
			else
			{
				//$_SESSION["error"]="Subscribers has been not imported successfully.";
			}
			unlink($newname);
		}
		else
		{
			//$_SESSION["error"] = "A problem occurred during file upload!";
		}
	}
	else
	{
		//$_SESSION["error"] = "Data has been not inserted";
	}
	$URL = getMemberURL($SECTION_VIEW_PAGE); 
	redirect($URL);
}
?>
<script type="text/javascript" language="javascript">
$(document).ready(function(){
    getpagelistingNew('<?php echo $SECTION; ?>','<?php echo $SECTION_TABLE?>','<?php echo $SECTION_FIELD_PREFIX?>','<?php echo $SECTION_MANAGE_PAGE; ?>','','');

	$.validator.addMethod("validExcel", function(value, element) {
    	return this.optional(element) || /\.(xls|xlsx|XLS|XLSX)$/i.test(value);
	}, "Please select only image");
	$("#import_business").validate({
		rules: {
			import_bus : {
				required : true,
				validExcel : true
			}
		},
		messages: {
			import_bus : {
				required : "Please select business file",
				validExcel : "Please select only xls or xlxs file"
			}
		}
	});
});
</script>
<?php //include_once(MEB_SECTION_DIR.'headerinner.php');?>
<div class="container search-group-section">
    <div class="row" <?php //echo $padding; ?> >
    <?php if($type!="Business") { ?>

        <form name="search_bus" id="search_bus" action="" method="post" onsubmit="return false;">
            <div class="search-section-left">
                <input type="search" name="search_text" id="search_text" placeholder="Search" class="">
                <div class="buttons_right">
                    <input type="submit" name="bus_search" id="bus_search" value="" onclick="search_mem()">
                </div>
            </div>
        </form>

        <div class="search-section-right">
            <div class="group-add-text"><a href="javascript:void(0);">Members</a></div>
        </div>

        <?php /* ?>
        <div class="bottomDiv" style="display: none;">
            <form name="import_business" enctype="multipart/form-data" method="post" action="<?php echo getMemberURL($SECTION_VIEW_PAGE,"import"); ?>" id="import_business">
                <div class="main-row attach">
                    <label>Attach File <?php echo getRequiredIcon()?></label>
                    <input type="file" tabindex="11" name="import_bus" id="import_bus" />
                    <div class="exel-demo"><a href="<?php echo UPLOAD_WWW.'Member-Business-demo.xls'; ?>">Download Template</a></div>
                </div>
                <div class="main-row phone">
                    <label>&nbsp;</label>
                    <input type="submit" tabindex="12" name="bus_import" id="bus_import" value="Upload Business" />
                </div>
            </form>
        </div>
        <?php */ ?>

    <?php } ?>
    </div>
</div>

<div class="container member-page-setion-1">
    <div class="row">
        <input type="hidden" name="total_nage" id="total_page" value="">
        <input type="hidden" name="current_page" id="current_page" value="">

        <div class="result-section">
            <div class="result-text">Result</div>

            <div class="result-add-remove-icon">
                <?php
                    $href_export = "export_excel.php?table=tbl_member_business&prifix=bus_";
                ?>
                <a href="<?php echo $href_export; ?>" class="file-icon tooltip" title="Export"></a>
                <a href="<?php echo MEB_INDEX_PARAMETER.$SECTION_MANAGE_PAGE; ?>" class="pluse-icon tooltip" title="Add"></a>
            </div>
        </div>

        <div class="group-setion-row">
            <div class="group-setion-row" id="updatediv">
                <div class="pre_loader_main" >
                    <img src="images/spinner.gif">
                </div>
            </div>
        </div>
        <a style="display: none;" id="next" href="index2.html">next page?</a>

    </div>
</div>

<link rel="stylesheet" href="js/owl.carousel.css" type="text/css" media="screen" /></div>
<script src="js/owl.carousel.js"></script>

<?php
$msg = $_SESSION["msg"];
$_SESSION["msg"]='';
?>
<script type="text/javascript">
    $(document).ready(function() {
       msg='';
        msg='<?php echo $msg; ?>';
        if(msg!='') {
            alertify.success(msg);
            //var notification = alertify.notify(error_re, 'success', 5, function(){  console.log('dismissed'); });
        }
    });

    $(document).on('mouseover','.gr_hover_this .admin_memebr_t_l_in', function(){
        $('.gr_hover_this .admin_memebr_t_l_in span').show();
    });

    $(document).on('mouseout','.gr_hover_this .admin_memebr_t_l_in', function(){
        $('.gr_hover_this .admin_memebr_t_l_in span').hide();
    });

    $(document).on('click','.video_show_slide, .close_video',function(){
        if($('.videoslider_main').is(':visible')) {
            $('.videoslider_main').stop().fadeOut();
            $('.detail_slider').stop().slideDown();
        } else {
            $('.detail_slider').stop().slideUp();
            $('.videoslider_main').stop().slideDown();
        }
    });

    function search_mem() {
        search_key = $.trim($('#search_text').val());
        $("#updatediv").html('');
        // $("#updatediv").infinitescroll("destroy");
        $("#updatediv").infinitescroll("unbind");
        getpagelistingNew('<?php echo $SECTION; ?>','<?php echo $SECTION_TABLE?>','<?php echo $SECTION_FIELD_PREFIX?>','<?php echo $SECTION_MANAGE_PAGE; ?>','',search_key);
    }

    function infinite_f() {
        $("#updatediv").infinitescroll("bind");
        total = $('#total_page').val();
        search_key = $.trim($('#search_text').val());

        url = ajax_folder+"getBusinessList.php"+'?fieldPrefix=bus_&managePage=manage_member&search_text='+search_key+'&tableName=tbl_member_business&xtraCondition=&orderby=id&order=&page=';

        $("#updatediv").infinitescroll({
            navSelector  	: "#next:last",
            nextSelector 	: "a#next:last",
            itemSelector 	: "#updatediv div.gr_on_view_card123",
            msgText         : "Loading group list...",
            debug		 	: true,
            dataType	 	: 'html',
            maxPage         : total,//$('#total_page').val(),

            path: function(index) {
                current = $('#current_page').val();
                current_next = parseInt(current)+ parseInt('1');
                //url = url;
                return url+current_next;
            },
            state: {
                isDestroyed: false,
                isDone: false,
                isDuringAjax : false
            }
            // behavior		: 'twitter',
            // appendCallback	: false, // USE FOR PREPENDING
            // pathParse     	: function( pathStr, nextPage ){ return pathStr.replace('2', nextPage ); }
        }, function(newElements, data, url){
            $('#current_page').val(current_next);
            $("#updatediv").append(newElements);
            $('.tooltip').tooltipster();
            $.fancybox.close();

            // $("#updatediv").prepend(newElements);
        });
    }
</script>