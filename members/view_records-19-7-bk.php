<?php 

include_once('../include/includeclass.php');

if ($_REQUEST['sectionName'])
	$sectionName = $_REQUEST['sectionName'];

if($sectionName == "Messaging")
{ 
	$msg_id = $_REQUEST['msg_id'];
	//subscriber Listing...
	$msg_fields = array("*");
	$msg_where  = "msg_id = ".$msg_id." AND msg_status = 'Active'";
	$msgRes 	= $db->selectData(TBL_MEMBER_MESSAGING,$msg_fields,$msg_where,$extra="",2);
?>
<div class="fullview_clients">
	<div class="subRow">
		 <div class="enterTitle main">
		 	  <h1>Massage Information</h1><a href="javascript:void(0);" class="fancybox-item fancybox-close" title="Close"></a>
		</div>
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>Message</b><span>:</span> <p><?php echo $msgRes[0]['msg_message']; ?></p></div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>Sent On</b><span>:</span> <p><?php echo date('m/d/Y',strtotime($msgRes[0]['msg_updated_date'])); ?></p></div>
	</div>
</div>
<?php
}
else if($sectionName == "Services")
{ 
	$ser_id = $_REQUEST['ser_id'];
	//subscriber Listing...
	$ser_fields = array("*");
	$ser_where  = "ser_id = ".$ser_id." AND ser_status = 'Active'";
	$serRes 	= $db->selectData(TBL_MEMBER_SERVICES,$ser_fields,$ser_where,$extra="",2);
?>
<div class="fullview_clients">
	<div class="subRow">
		 <div class="enterTitle main">
		 	  <h1>Services Information</h1><a href="javascript:void(0);" class="fancybox-item fancybox-close" title="Close"></a>
		</div>
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>Services Title</b><span>:</span> <p><?php echo $serRes[0]['ser_title']; ?></p></div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>Services Description</b><span>:</span> <p><?php echo $serRes[0]['ser_description']; ?></p></div>
	</div>
</div>
<?php
}
else if($sectionName == "BusinessSub")
{ 
	$bsb_id = $_REQUEST['bsb_id'];
	//subscriber Listing...
	$bsb_fields = array("*");
	$bsb_where  = "bsb_id = ".$bsb_id." AND bsb_status != 'Deleted'";
	$bussubRes 	= $db->selectData(TBL_BUSINESS_SUB,$bsb_fields,$bsb_where,$extra="",2);
?>
<div class="fullview_clients">
	<div class="subRow">
		 <div class="enterTitle main">
		 	  <h1>Business Subscriber Information</h1><a href="javascript:void(0);" class="fancybox-item fancybox-close" title="Close"></a>
		</div>
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>Directory Name</b><span>:</span> <p><?php
		$dir_fields = array("dir_name");
		$dir_where  = "dir_id = '".$bussubRes[0]['bsb_dir_id']."'";
		$dirRes = $db->selectData(TBL_DIRECTORY,$dir_fields,$dir_where,$extra="",1);
		echo $dirRes[0]["dir_name"]; 
		?></p></div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>Business Name</b><span>:</span> <p><?php echo $bussubRes[0]['bsb_name']; ?></p></div>
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>Email</b><span>:</span> <p><?php echo $bussubRes[0]['bsb_username']; ?></p></div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>Question</b><span>:</span> <p><?php echo $bussubRes[0]['bsb_question']; ?></p></div>
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>Answer</b><span>:</span> <p><?php echo $bussubRes[0]['bsb_answer']; ?></p></div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>Phone Number</b><span>:</span> <p><?php echo $bussubRes[0]['bsb_phone']; ?></p></div>
	</div>
</div>
<?php
}
else if($sectionName == "Subscribers")
{ 
	$sub_id = $_REQUEST['sub_id'];
	//subscriber Listing...
	$sub_fields = array("*");
	$sub_where  = "sub_id = ".$sub_id;
	$subRes 	= $db->selectData(TBL_MEMBER_SUBSCRIBERS,$sub_fields,$sub_where,$extra="",2);
	
?>
<div class="fullview_clients">
	<div class="subRow">
		 <div class="enterTitle main">
		 	  <h1>Subscriber Information</h1><a href="javascript:void(0);" class="fancybox-item fancybox-close" title="Close"></a>
		</div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>Directory Name</b><span>:</span>
		<p><?php
			$dir_fields = array("dir_name");
			$dir_where  = "dir_id = '".$subRes[0]['sub_dir_id']."'";
			$dirRes = $db->selectData(TBL_DIRECTORY,$dir_fields,$dir_where,$extra="",1);
			echo $dirRes[0]["dir_name"]; 
		?></p>
		</div>
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>Title</b><span>:</span> <p><?php echo $subRes[0]['sub_title']; ?></p></div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>Company Name</b><span>:</span> <p><?php echo $subRes[0]['sub_company']; ?></p></div>
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>Name</b><span>:</span> <p><?php echo $subRes[0]['sub_name']; ?></p></div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>Phone</b><span>:</span> <p><?php echo $subRes[0]['sub_phone']; ?></p></div>
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>Zip Code</b><span>:</span> <p><?php echo $subRes[0]['sub_zip']; ?></p></div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>City</b><span>:</span><p><?php echo $subRes[0]['sub_city']; ?></p> </div>
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>State</b><span>:</span> <p><?php //echo $subRes[0]['sub_state'];
			$state_fields = array("sta_name");
			$state_where  = "sta_id = '".$subRes[0]['sub_sta_id']."'";
			$staRes = $db->selectData(TBL_STATE,$state_fields,$state_where,$extra="",1);
			echo $staRes[0]["sta_name"]; 
		 ?> </p></div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>Email</b><span>:</span><p><a href="mailto:<?php echo $subRes[0]['sub_email'] ?>"><?php echo $subRes[0]['sub_email']; ?></a></p> </div>
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>Question</b><span>:</span><p><?php echo $subRes[0]['sub_question'] ?></a></p> </div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>Answer</b><span>:</span><p><?php echo $subRes[0]['sub_answer'] ?></a></p> </div>
	</div>
</div>
<?php
}
else if($sectionName == "Business")
{
	$type = getMemberType();
	if($type!="Business")
	{
		$SECTION_TABLE= TBL_MEMBER_BUSINESS;
		$uploadFILEWWW = UPLOAD_WWW_BUSINESS_FILE;
	}
	else
	{
		$SECTION_TABLE= TBL_MEMBER_BUSINESS_SUB;
		$uploadFILEWWW = UPLOAD_WWW_BUSINESS_FILE."Business_Sub/";
	}
	$bus_id = $_REQUEST['bus_id'];
	//subscriber Listing...
	$bus_fields = array("*");
	$bus_where  = "bus_id = ".$bus_id." AND bus_status = 'Active'";
	$busRes 	= $db->selectData($SECTION_TABLE,$bus_fields,$bus_where,$extra="",2);	
?>
<div class="fullview_clients" id="business">
	<div class="subRow">
		 <div class="enterTitle main">
		 	  <h1>Business Information</h1><a href="javascript:void(0);" class="fancybox-item fancybox-close" title="Close"></a>
		</div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>Title</b><span>:</span> <p><?php echo $busRes[0]['bus_title']; ?></p></div>
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>Name</b><span>:</span> <p><?php echo $busRes[0]['bus_name']; ?></p></div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>Address1</b><span>:</span> <p><?php echo $busRes[0]['bus_address1']; ?></p></div>
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>Address2</b><span>:</span><p><?php echo $busRes[0]['bus_address2']; ?></p> </div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>City</b><span>:</span> <p><?php echo $busRes[0]['bus_city']; ?></p></div>
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>State</b><span>:</span> <p><?php //echo $subRes[0]['sub_state'];
			$state_fields = array("sta_name");
			$state_where  = "sta_id = '".$busRes[0]['bus_sta_id']."'";
			$staRes = $db->selectData(TBL_STATE,$state_fields,$state_where,$extra="",1);
			echo $staRes[0]["sta_name"]; 
		 ?> </p></div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>Phone No</b><span>:</span> <p><?php echo $busRes[0]['bus_phone']; ?></p></div>
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>Zip Code</b><span>:</span> <p><?php echo $busRes[0]['bus_zip']; ?></p></div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>Fax</b><span>:</span><p><?php echo $busRes[0]['bus_fax']; ?></p> </div>
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>Email</b><span>:</span><p><a href="mailto:<?php echo $busRes[0]['bus_email'] ?>"><?php echo $busRes[0]['bus_email']; ?></a></p> </div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>Website</b><span>:</span><p><?php echo $busRes[0]['bus_website']; ?></p> </div>
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>About</b><span>:</span><p><?php echo $busRes[0]['bus_about']; ?></p> </div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>Notes</b><span>:</span><p><?php echo $busRes[0]['bus_notes']; ?></p> </div>
	</div>
	<div class="subRow">
		<div class="enterTitle">
			<b>Logo File</b><span>:</span><p>
			<?php
			if($busRes[0]['bus_logo_file']!="")
			{
				$fileNameExt = explode(".", $busRes[0]['bus_logo_file']);
				$fileExt 	 = $fileNameExt[count($fileNameExt)-1];
				$allowedImgExts = array("gif","jpg","jpeg","png","GIF","JPG","JPEG","PNG");
				if(in_array($fileExt,$allowedImgExts))
				{
				echo "<img src='".$uploadFILEWWW.$bus_id."/".$busRes[0]['bus_logo_file']."' width='150' height='150' />";
				}
				else
				{
				?>
					<object width="300" height="150" data="player.swf" type="application/x-shockwave-flash">
						<param value="opaque" name="wmode">
						<param value="always" name="allowscriptaccess">
						<param value="all" name="allownetworking">
						<param value="file=<?php echo $uploadFILEWWW.$bus_id."/".$busRes[0]['bus_logo_file']; ?>&;volume=80&amp;bufferlength=5&amp;logo.hide=false&amp;&;title=sleeve_length&amp;linktarget=_self" name="flashvars">
						<param name="allowFullScreen" value="true" />
					</object>
				<?php 
				} 
			}
			?>
			</p>
		</div>
	</div>
</div>
<?php 
}
if($sectionName == "Coupons")
{ 
	$cop_id = $_REQUEST['cop_id'];
	//Coupons Listing...
	$cop_fields = array("*");
	$cop_where  = "cop_id = ".$cop_id." AND cop_status = 'Active'";
	$copRes 	= $db->selectData(TBL_MEMBER_COUPONS,$cop_fields,$cop_where,$extra="",2);
?>
<div class="fullview_clients">	
	<div class="subRow">
		 <div class="enterTitle main">
		 	  <h1>Coupons Information</h1><a href="javascript:void(0);" class="fancybox-item fancybox-close" title="Close"></a>
		</div>
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>Coupons Title</b><span>:</span> <p><?php echo $copRes[0]['cop_title']; ?></p></div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>Percentage</b><span>:</span> <p><?php echo $copRes[0]['cop_percentage']; ?>%</p></div>
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>Description</b><span>:</span> <p><?php echo $copRes[0]['cop_description']; ?></p></div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>Coupon Code</b><span>:</span><p><?php echo $copRes[0]['cop_coupon_code']; ?></p> </div>
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>Coupon Valid From</b><span>:</span><p><?php echo date('m/d/Y',strtotime($copRes[0]['cop_valid_from'])); ?></p> </div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>Coupon Valid To</b><span>:</span><p><?php echo date('m/d/Y',strtotime($copRes[0]['cop_valid_thru'])); ?></p> </div>
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>BarCode</b><span>:</span> <p><img src="<?php echo UPLOAD_WWW_COUPONS_FILE.$cop_id."/".$copRes[0]['cop_barcode']; ?>" alt="Barcode" title="Barcode"></p></div>
	</div>
	
</div>
<?php
} ?>

<?php if($sectionName == "Directory"){ 

if ($_REQUEST['sectionName'])
	$dirId = $_REQUEST['dir_id'];

	//Directory Listing...
$dir_fields = array("*");
$dir_where  = "dir_id = ".$dirId." AND dir_status = 'Active'";
$dirRes 	= $db->selectData(TBL_DIRECTORY ." LEFT JOIN ".TBL_DIRECTORY_CATEGORY." ON dir_dic_id = dic_id"." LEFT JOIN ".TBL_STATE." ON dir_sta_id = sta_id"." LEFT JOIN ".TBL_COUNTRY." ON dir_con_id = con_id",$dir_fields,$dir_where,$extra="",2);
?>

<div class="fullview_clients">
	<div class="subRow">
		 <div class="enterTitle main">
		 	  <h1>Directory Information</h1><a href="javascript:;" class="fancybox-item fancybox-close" title="Close"></a>
		</div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>Directory Category</b><span>:</span> <p><?php echo $dirRes[0]['dic_name']; ?></p></div>
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>Directory Name</b><span>:</span> <p><?php echo $dirRes[0]['dir_name']; ?></p></div>
	</div>
	<div class="subRow light">
		<div class="enterTitle address-title"><b>Address </b><span>:</span> <p><?php echo $dirRes[0]['dir_address']; ?> </p></div>
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>City </b><span>:</span> <p><?php echo $dirRes[0]['dir_city']; ?></p></div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>State </b><span>:</span> <p><?php echo $dirRes[0]['sta_name']; ?></p></div>
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>Zip</b><span>:</span><p><?php echo $dirRes[0]['dir_zip']; ?></p> </div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>Country</b><span>:</span><p><?php echo $dirRes[0]['con_name']; ?></p> </div>
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>Phone </b><span>:</span> <p><?php echo $dirRes[0]['dir_office_phone']; ?> </p></div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>E-mail </b><span>:</span><p><a href="mailto:<?php echo $dirRes[0]['dir_email']; ?>"><?php echo $dirRes[0]['dir_email']; ?></a></p></div>
	</div>
	<div class="subRow">
		<div class="enterTitle courses-details-title"><b>Website </b><span>:</span><p><?php echo $dirRes[0]['dir_website']; ?></p></div>
	</div>  
</div>
<?php }?>