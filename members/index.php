<?php 
$today_date = date('Y-m-d H:i:s');
if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start("ob_gzhandler"); else ob_start();
	include_once('../include/includeclass.php');
	$filename=$_REQUEST['files'];
	$action=$_REQUEST['method'];
	$str=$_SERVER['PHP_SELF'];
	$exp=explode("/",$str);

    if($filename == MEB_FRONT || $filename == MEB_LOGIN || $filename == MEB_REGISTER ||
       $filename == MEB_FORGOT || $filename == MEB_TERMS || $filename == MEB_OPPORTUNITIES ||
       $filename == MEB_INVESTOR_NEWS || $filename == MEB_ABOUT_US || $filename == MEB_CONTACT_US || $filename == MEB_PRIVACY_POLICY) {

        if($filename == MEB_TERMS || $filename == MEB_OPPORTUNITIES || $filename == MEB_INVESTOR_NEWS ||
           $filename == MEB_ABOUT_US || $filename == MEB_CONTACT_US || $filename == MEB_PRIVACY_POLICY) {

        } else if(! empty($_SESSION['meb_admin_id'])) {
            $URL = MEMBER_WWW.ADM_INDEX_PARAMETER.MEB_HOME;
            redirect($URL);
            exit;
        }

    } else {
        if(empty($_SESSION['meb_admin_id'])) {
            $_SESSION['msg_l_e'] = "Please login first.";

            $URL = MEMBER_WWW.ADM_INDEX_PARAMETER.MEB_LOGIN;
            redirect($URL);
            exit;
        }
    }

	if(!empty($filename)) {
		$middlefile=MEB_MIDD_DIR.$filename;
		if(!file_exists($middlefile.".php"))
		{
			$middlefile=MEB_MIDD_DIR.MEB_HOME;
		}
	} else {
        if(empty($_SESSION['meb_admin_id'])) {
            $URL = MEMBER_WWW.ADM_INDEX_PARAMETER.MEB_FRONT;
            redirect($URL);
            exit;
        } else {
            $URL = MEMBER_WWW.ADM_INDEX_PARAMETER.MEB_HOME;
            redirect($URL);
            exit;
        }
	}
	
	########################################################################################
	$display_title = getMemberFileTitle($filename);	//For Display File Title
	#########################################################################################
	
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />

    <title><?php echo $display_title;?></title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="format-detection" content="telephone=no">

    <link rel="shortcut icon" href="<?php echo IMG_WWW; ?>favicon.ico" type="image/x-icon" />
    <!--[if lt IE 9]>
    <script src="<?php echo JS_FOLDER_WWW?>html5.js" type="text/javascript"></script>
    <![endif]-->

    <script type="text/javascript">
    var ajax_folder='<?php echo AJAX_FOLDER_WWW; ?>';
    var middle_folder='<?php echo FRT_MIDD_WWW; ?>';
    </script>

    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="css/styles.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="css/responsive.css" type="text/css" media="screen" />

    <link rel="stylesheet" href="css/tooltipster.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="<?php echo MEB_CSS_WWW;?>prettyCheckable.css" type="text/css" media="screen" />
    <!--[if IE]>
        <link rel="stylesheet" href="<?php echo FRT_CSS;?>ie.css" type="text/css" media="screen" />
    <![endif]-->
    <link rel="stylesheet" href="<?php echo JS_FOLDER_WWW?>jquery-ui.css" type="text/css" media="screen">
    <link rel="stylesheet" type="text/css" href="<?php echo JS_FOLDER_WWW?>Fancybox/jquery.fancybox.css?v=2.0.6" media="screen" />
    <link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="css/mystyle.css" type="text/css" media="screen" />

    <!-- include the RTL css files-->
    <link rel="stylesheet" href="css/alert/alertify.css">
    <link rel="stylesheet" href="css/alert/themes/default.rtl.css">

    <script type="text/javascript" src="<?php echo JS_FOLDER_WWW?>jquery-1.11.0.min.js"></script>

    <script src="<?php echo JS_FOLDER_WWW?>jquery-ui.js" type="text/javascript"></script>

    <!--<script type="text/javascript" src="<?php //echo JS_FOLDER_WWW?>fremworks.js"></script>-->

    <script type="text/javascript" src="<?php echo JS_FOLDER_WWW?>placeholders.min.js"></script>
    <script type="text/javascript" src="<?php echo JS_FOLDER_WWW?>jquery.tooltipster.js"></script>
    <script type="text/javascript" src="<?php echo JS_FOLDER_WWW?>Fancybox/jquery.fancybox.js?v=2.0.6"></script>

    <script type="text/javascript" src="<?php echo JS_FOLDER_WWW?>prettyCheckable.js"></script>
    <script type="text/javascript" src="<?php echo JS_FOLDER_WWW?>jquery-validation.js"></script>
    <script src="<?php echo ROOT_WWW?>/js/additional-methods.min.js"></script>

    <script defer src="<?php echo JS_FOLDER_WWW?>jquery.flexslider.js"></script>
    <script src="<?php echo JS_FOLDER_WWW?>modernizr.js"></script>
    <script type="text/javascript" src="<?php echo AJAX_FOLDER_WWW?>getAjaxAdmin.js"></script>

    <script type="text/javascript" src="<?php echo ROOT_WWW; ?>js/custom.js"></script>
    <script type="text/javascript" src="<?php echo ROOT_WWW; ?>js/placeholders.min.js"></script>

    <!-- include alertify script -->
    <script src="<?php echo ROOT_WWW?>js/alertify.js"></script>

    <!-- include mask phone mumber script -->
    <script src="<?php echo ROOT_WWW?>/js/jquery.maskedinput.min.js"></script>

    <!-- include infinitescroll script -->
    <script src="<?php echo ROOT_WWW?>/js/jquery.infinitescroll.js"></script>
</head>

<body>

    <?php include_once('header.php');?>

	<?php include_once($middlefile.'.php');?>

    <?php include_once('footer.php');?>

</body>
</html>
