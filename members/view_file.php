<?php

include_once('../include/includeclass.php');

    $bus_id = $_REQUEST['bus_id'];
    $filename = $_REQUEST['filename'];
	$type = getMemberType();
	if($type!="Business")
	{
		$SECTION_TABLE= TBL_MEMBER_BUSINESS;
		$uploadFILEURL = UPLOAD_DIR_BUSINESS_FILE;
		$uploadFILEWWW = UPLOAD_WWW_BUSINESS_FILE;
	}
	else
	{
		$SECTION_TABLE= TBL_MEMBER_BUSINESS_SUB;
		$uploadFILEURL = UPLOAD_DIR_BUSINESS_FILE."Business_Sub/";
		$uploadFILEWWW = UPLOAD_WWW_BUSINESS_FILE."Business_Sub/";
	}
?>
<div id="defaultPlay">
	<div class="fullview_clients">
		<div class="subRow">
			<div class="enterTitle main">
				<h1>Member Information File</h1><a href="javascript:void(0);" class="fancybox-item fancybox-close" title="Close"></a>
			</div>
		</div>
		<div class="subRow">
			<?php
			$fileNameExt = explode(".", $filename);
			$fileExt 	 = $fileNameExt[count($fileNameExt)-1];
			$allowedImgExts = array("gif","jpg","jpeg","png","GIF","JPG","JPEG","PNG");
			if(in_array($fileExt,$allowedImgExts))
			{
			echo "<img src='".$uploadFILEWWW.$bus_id."/".$filename."' width='600' height='300' />";
			}
			else
			{
			?>
			<object width="600" height="300" data="./player.swf" type="application/x-shockwave-flash">
				<param value="opaque" name="wmode">
				<param value="always" name="allowscriptaccess">
				<param value="all" name="allownetworking">
				<param value="file=<?php echo $uploadFILEWWW.$bus_id."/".$filename; ?>&;volume=80&amp;bufferlength=5&amp;logo.hide=false&amp;&;title=sleeve_length&amp;linktarget=_self" name="flashvars">
				<param name="allowFullScreen" value="true" />
			</object>
			<?php } ?>
		</div>
	</div>
</div>
