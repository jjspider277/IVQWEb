<?php 
	ob_start();
	include_once('../include/includeclass.php');
	//print_r($_SESSION);
	$action=$_REQUEST['mod'];
	if (!empty($action))	
	{
		$error="";			
		if(empty($error))
		{
		 	$sql="select * from ".TBL_MEMBER." where meb_username ='".trim(addslashes($_POST['meb_username']))."' AND meb_password='".md5($_POST['meb_password'])."'"; // AND meb_status='Active'
		 	$data = $db->select($sql);
		 	if(!empty($data))
    		{
				if($data[0]['meb_status'] == 'Active') 
				{
					session_start();
					if($_POST['remember']==1)
					{
						setcookie('meb_username', $_POST['meb_username'],time() + 60*60*24*30); 
						setcookie('meb_password', $_POST['meb_password'],time() + 60*60*24*30);
					}
     
					$_SESSION['meb_id']	=	$data[0]['meb_id'];
					$_SESSION['meb_admin_id']	=	$data[0]['meb_id'];
					$_SESSION['mebDirId']	=	$data[0]['meb_dir_id'];
					$_SESSION['member_name'] 	=	$data[0]['meb_name'];
					$_SESSION['meb_type']	=	"Members";
				//	print_r($_SESSION);
					$sql = "INSERT INTO ".TBL_MEMBER_LOGIN_DETAIL." SET 
							mld_meb_id = '".$data[0]['meb_id']."',
							mld_login_datetime = '".date('Y-m-d H:i:s')."',
							mld_ip_address = '".$_SERVER['REMOTE_ADDR']."'"; 
							
					$GPDetails = $db->insert($sql);
					$log_id = mysql_insert_id();
					$_SESSION['meb_log_id']=$log_id;			
					$URL=MEB_INDEX_PARAMETER.MEB_HOME; 			
					redirect($URL);
					exit;
				} 
				else 
				{
					$error = "Your account has been deactivated";
				}	
			}
			else
			{
				$error = "Invalid User name and Password.";		
			}
		}
	}
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>B2B Mobile Services - Members Login</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="format-detection" content="telephone=no">
	<link rel="shortcut icon" href="<?php echo IMG_WWW; ?>favicon.ico" type="image/x-icon" />

	<link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="css/styles.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="css/responsive.css" type="text/css" media="screen" />

	<link rel="stylesheet" href="<?php echo MEB_CSS_WWW;?>prettyCheckable.css" type="text/css" media="screen" />
	<script type="text/javascript" src="<?php echo JS_FOLDER_WWW?>jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="<?php echo JS_FOLDER_WWW?>jquery-validation.js"></script>
	<script type="text/javascript" src="<?php echo JS_FOLDER_WWW?>placeholders.min.js"></script>
	<script type="text/javascript" src="<?php echo JS_FOLDER_WWW?>prettyCheckable.js"></script>
	<script type="text/javascript" src="<?php echo ROOT_WWW; ?>js/custom.js"></script>

</head>
<body>

	<?php include_once('header.php');?>

	<div class="content-section-01">
		<div class="row">

			<div class="left-image-section">
				<img src="<?php echo ROOT_WWW; ?>/members/images/mobille-img.png" alt="">
			</div>

			<div class="right-form-section">
				<h1>User login</h1>

				<form name='frmMember' method='post' action="<?php echo MEB_FOLDER_WWW.MEB_LOGIN."?mod=add";?>" id='frmMember'>

					<div class="right-form-section-form">
						<label class="error"><?php echo $error; ?></label>

						<div class="error1">
							<div class="inputText username"><input type="text" name="meb_username" id="meb_username" placeholder="User name (Valid Email)" value="<?php echo $_COOKIE['meb_username']; ?>" /></div>
							<span class="email-icon"></span>
						</div>

						<div class="error1">
							<div class="inputText password"><input type="password" name="meb_password" id="meb_password" placeholder="Password" value="<?php echo $_COOKIE['meb_password']; ?>" /></div>
							<span class="closed-icon"></span>
						</div>

						<div class="button-section">
							<div style="display: none" class="remember"><input name="remember" id="remember" type="checkbox" value="1" data-label="Remember Me"  data-labelPosition="right"></div>
							<div style="display: none" class="forgot"><a href="<?php echo MEB_FOLDER_WWW.MEB_FORGOT;?>">Reset Password?</a></div>

							<div class="create-accoumt"><a href="#">Create account</a></div>
							<div class="login"><a href="#">Login<span class="login-arrow"></span></a></div>
						</div>

						<div class="google-play-section">
							<div class="apple-button"><a href="#"><img src="images/app-store-1.jpg"></a></div>
							<div class="google-play-button"><a href="#"><img src="images/google-play-1.jpg"></a></div>
						</div>

					</div>

				</form>

			</div>
		</div>
	</div>

	<?php include_once('footer.php');?>

</body>
</html>
<script>
$(document).ready(function() {
	$('input[type="checkbox"]').prettyCheckable();	
	$("#frmMember").validate({
		rules: {
			meb_username:"required",
			meb_password:"required"
		},
		messages: {
			meb_username:"Please enter username",
			meb_password:"Please enter password"
		}
	});
});
</script>
<?php 
flush();
?>
