<?php
	/*ini_set('max_execution_time',300);
	ini_set('max_input_time',300); 
	ini_set('memory_limit', '300M');
	ini_set('post_max_size', '32M');
	ini_set('upload_max_filesize', '32M');*/
	#################################################################
	$SECTION_FIELD_PREFIX='bus_';
	$SECTION_AUTO_ID=$_REQUEST[$SECTION_FIELD_PREFIX.'id'];
	$SECTION_WHERE=$SECTION_FIELD_PREFIX."id=".$SECTION_AUTO_ID."";
	$SECTION_VIEW_PAGE=MEB_VIEW_BUSINESS;
	$SECTION_MANAGE_PAGE=MEB_MANAGE_BUSINESS;
	
	$SECTION_NAME='Business';

	$type = getMemberType();
	if($type!="Business")
	{
		$SECTION_TABLE= TBL_MEMBER_BUSINESS;
		$uploadFILEURL = UPLOAD_DIR_BUSINESS_FILE;
		$uploadFILEWWW = UPLOAD_WWW_BUSINESS_FILE;
	}
	else
	{
		$SECTION_TABLE= TBL_MEMBER_BUSINESS_SUB;
		$uploadFILEURL = UPLOAD_DIR_BUSINESS_FILE."Business_Sub/";
		$uploadFILEWWW = UPLOAD_WWW_BUSINESS_FILE."Business_Sub/";
	}

	//State Listing...
	$state_fields = array("sta_id","sta_name");
	$state_where  = "sta_status = 'Active'";
	$staRes 	= $db->selectData(TBL_STATE,$state_fields,$state_where,$extra="",2);
	#################################################################
	if(isset($_POST)){	
		$data = $_POST;	
	}

	if ($data['bus_submit'] == 'Save' || $data['bus_addNew'] == 'Add New')
	{
		//print_r($_FILES['bus_logo_file']);exit;	
		$bus_logo_file_name= $_FILES['bus_logo_file']['name'];
		$type = getMemberType();
		if($type!="Business")
		{
  		if($action != 'Update'){	
			$add_values[$SECTION_FIELD_PREFIX.'meb_id'] 		= trim($data["bus_meb_id"]);
   }
		}
		else
		{
  if($action != 'Update'){	
			$add_values[$SECTION_FIELD_PREFIX.'sub_id'] 		= trim($data["bus_meb_id"]);
   }
		}
		
		$add_values[$SECTION_FIELD_PREFIX.'title'] 			= trim($data["bus_title"]);
		$add_values[$SECTION_FIELD_PREFIX.'name'] 			= trim($data["bus_name"]);
		$add_values[$SECTION_FIELD_PREFIX.'company']		= trim($data["bus_company"]);
		$add_values[$SECTION_FIELD_PREFIX.'phone'] 			= trim($data["bus_phone"]);
		$add_values[$SECTION_FIELD_PREFIX.'zip'] 			= trim($data["bus_zip"]);
		$add_values[$SECTION_FIELD_PREFIX.'address1'] 		= trim($data["bus_address1"]);
		$add_values[$SECTION_FIELD_PREFIX.'city'] 			= trim($data["bus_city"]);
		$add_values[$SECTION_FIELD_PREFIX.'email'] 			= trim($data["bus_email"]);
		$add_values[$SECTION_FIELD_PREFIX.'about'] 			= trim($data["bus_about"]);
		$add_values[$SECTION_FIELD_PREFIX.'website'] 		= trim($data["bus_website"]);
		$add_values[$SECTION_FIELD_PREFIX.'fax'] 			= trim($data["bus_fax"]);
		$add_values[$SECTION_FIELD_PREFIX.'address2'] 		= trim($data["bus_address2"]);
		$add_values[$SECTION_FIELD_PREFIX.'sta_id'] 		= trim($data["bus_sta_id"]);
		$add_values[$SECTION_FIELD_PREFIX.'notes'] 			= trim($data["bus_notes"]);
		$status = "Active";

		if($action != 'Update'){	
			$add_values[$SECTION_FIELD_PREFIX.'status'] 		= $status;									
			$add_values[$SECTION_FIELD_PREFIX . 'created_id'] 	= $mebId;
			$add_values[$SECTION_FIELD_PREFIX . 'created_date'] = date("Y-m-d H:i:s");
			$BusId = $db->insertData($SECTION_TABLE, $add_values);
			if($bus_logo_file_name!="")
			{
				if(is_dir($uploadFILEURL.$BusId) == false)
				{ 			
					mkdir($uploadFILEURL.$BusId);
				}
				$bus_file = uploadIVA($uploadFILEURL.$BusId."/",$_FILES['bus_logo_file']);
				if($bus_file=="type_err")
				{
					$_SESSION["err_file"]="Please select only image videos and audio file"; 
					$db->delete("delete from ".$SECTION_TABLE." where bus_id=".$BusId);
					$_SESSION['add_detail_error'][]=$_POST;
					$URL = getMemberURL($SECTION_MANAGE_PAGE,$action);
					redirect($URL);
					exit; 
				}
				else if($bus_file=="size_err")
				{
					$_SESSION["err_file"]="Please upload file less than 32MB file size!";
					$db->delete("delete from ".$SECTION_TABLE." where bus_id=".$BusId);
					$_SESSION['add_detail_error'][]=$_POST;
					$URL = getMemberURL($SECTION_MANAGE_PAGE,$action);
					redirect($URL);
					exit; 
				}
				else
				{
					$bus_logo_file = $bus_file;
					$updateData[$SECTION_FIELD_PREFIX.'logo_file'] 	= $bus_logo_file;
					$SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$BusId;
					$db->updateData($SECTION_TABLE, $updateData, $SECTION_WHERE);
				}
			}
			//$_SESSION['msg']  =   "Subscribers has been inserted successfully.";  
		}
		else
		{
	 		$BusId = $SECTION_AUTO_ID;
			if($bus_logo_file_name!="")
			{
				if(is_dir($uploadFILEURL.$SECTION_AUTO_ID) == false)
				{ 			
					mkdir($uploadFILEURL.$SECTION_AUTO_ID);
				}
	
				if($data["edit_bus_logo_file"]!="")
				{
					unlink($uploadFILEURL.$SECTION_AUTO_ID."/".$data["edit_bus_logo_file"]);
				}
				$bus_file = uploadIVA($uploadFILEURL.$SECTION_AUTO_ID."/",$_FILES['bus_logo_file']);
				if($bus_file=="type_err")
				{
					$_SESSION["err_file"]="Please select only image videos and audio file"; 
					$_SESSION['add_detail_error'][]=$_POST;
					$URL = getMemberURL($SECTION_MANAGE_PAGE,"Edit","bus_id".$SECTION_AUTO_ID);
					redirect($URL);
					exit; 
				}
				else if($bus_file=="size_err")
				{
					$_SESSION["err_file"]="Please upload file less then 32MB file size!"; 
					$_SESSION['add_detail_error'][]=$_POST;
					$URL = getMemberURL($SECTION_MANAGE_PAGE,"Edit","bus_id".$SECTION_AUTO_ID);
					redirect($URL);
					exit; 
				}
				else
				{
					$bus_logo_file = $bus_file;
				}
			}
			else
			{
				$bus_logo_file = $data["edit_bus_logo_file"];
			}
			$add_values[$SECTION_FIELD_PREFIX.'logo_file'] 	= $bus_logo_file;
			$add_values[$SECTION_FIELD_PREFIX . 'updated_id'] 			= $mebId;
			$add_values[$SECTION_FIELD_PREFIX . 'updated_date'] 		= date("Y-m-d H:i:s");	
			$SECTION_WHERE=$SECTION_FIELD_PREFIX."id=".$SECTION_AUTO_ID."";
			$GPDetail_result = $db->updateData($SECTION_TABLE, $add_values, $SECTION_WHERE);
			//$_SESSION['msg']  =   "Subscribers has been updated successfully."; 
		}
		$videoFile1 = $_FILES["bus_video_file1"]["name"];
		$videoFile2 = $_FILES["bus_video_file2"]["name"];
		$videoFile3 = $_FILES["bus_video_file3"]["name"];
		$videoFile4 = $_FILES["bus_video_file4"]["name"];
		$videoFile5 = $_FILES["bus_video_file5"]["name"];
		if(is_dir($uploadFILEURL.$SECTION_AUTO_ID) == false)
		{ 			
			mkdir($uploadFILEURL.$SECTION_AUTO_ID);
		}
	 
		$ext = pathinfo($videoFile1, PATHINFO_EXTENSION);  
		if($videoFile1 != "")
		{
			$videoFile_1 = uploadVideo($uploadFILEURL.$BusId."/",$_FILES["bus_video_file1"]);
			if($videoFile_1 == $videoFile1)
			{           
				unset($update_values);
		   		$update_values[$SECTION_FIELD_PREFIX.'video_file_1'] = trim($videoFile1);
				$SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$BusId;
				$db->updateData($SECTION_TABLE, $update_values, $SECTION_WHERE);
			}
			else
			{
				$_SESSION["video_file_1"] = $videoFile_1;            
				$_SESSION['add_detail_error'][]=$data;
				$SECTION_WHERE=$SECTION_FIELD_PREFIX."id=".$BusId."";
				$action = "Edit";           
				$URL = getMemberURL($SECTION_MANAGE_PAGE,$action,$SECTION_WHERE);
				redirect($URL);
				exit;
			}
		}

		$ext = pathinfo($videoFile2, PATHINFO_EXTENSION);  
		if($videoFile2 != "")
		{
			$videoFile_2 = uploadVideo($uploadFILEURL.$BusId."/",$_FILES["bus_video_file2"]);
			if($videoFile_2 == $videoFile2)
			{
				unset($update_values);
				$update_values[$SECTION_FIELD_PREFIX.'video_file_2'] 			= trim($videoFile2);
				$SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$BusId;
				$db->updateData($SECTION_TABLE, $update_values, $SECTION_WHERE);
			}
			else
			{
				$_SESSION["video_file_2"] = $videoFile_2; 
				//$db->delete("delete from ".$SECTION_TABLE." where bus_id=".$BusId);
				$_SESSION['add_detail_error'][]=$data;
				$SECTION_WHERE=$SECTION_FIELD_PREFIX."id=".$BusId."";
				$action = "Edit";           
				$URL = getMemberURL($SECTION_MANAGE_PAGE,$action,$SECTION_WHERE);
				redirect($URL);
				exit;
			}
		}

		$ext = pathinfo($videoFile3, PATHINFO_EXTENSION);  
		if($videoFile3 != "")
		{
			$videoFile_3 = uploadVideo($uploadFILEURL.$BusId."/",$_FILES["bus_video_file3"]);
			if($videoFile_3 == $videoFile3)
			{
				unset($update_values);
				$update_values[$SECTION_FIELD_PREFIX.'video_file_3'] 			= trim($videoFile3);
				$SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$BusId;
				$db->updateData($SECTION_TABLE, $update_values, $SECTION_WHERE);
			}
			else
			{
				$_SESSION["video_file_2"] = $videoFile_3; 
				//$db->delete("delete from ".$SECTION_TABLE." where bus_id=".$BusId);
				$_SESSION['add_detail_error'][]=$data;
				$SECTION_WHERE=$SECTION_FIELD_PREFIX."id=".$BusId."";
				$action = "Edit";           
				$URL = getMemberURL($SECTION_MANAGE_PAGE,$action,$SECTION_WHERE);
				redirect($URL);
				exit;
			}
		}

		$ext = pathinfo($videoFile4, PATHINFO_EXTENSION);  
		if($videoFile4 != "")
		{
			$videoFile_4 = uploadVideo($uploadFILEURL.$BusId."/",$_FILES["bus_video_file4"]);
			if($videoFile_4 == $videoFile4)
			{
				unset($update_values);
				$update_values[$SECTION_FIELD_PREFIX.'video_file_4'] 			= trim($videoFile4);
				$SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$BusId;
				$db->updateData($SECTION_TABLE, $update_values, $SECTION_WHERE);
			}
			else
			{
				$_SESSION["video_file_2"] = $videoFile_4; 
				//$db->delete("delete from ".$SECTION_TABLE." where bus_id=".$BusId);
				$_SESSION['add_detail_error'][]=$data;
				$SECTION_WHERE=$SECTION_FIELD_PREFIX."id=".$BusId."";
				$action = "Edit";           
				$URL = getMemberURL($SECTION_MANAGE_PAGE,$action,$SECTION_WHERE);
				redirect($URL);
				exit;
			}
		}

		$ext = pathinfo($videoFile5, PATHINFO_EXTENSION);  
		if($videoFile5 != "")
		{
			$videoFile_5 = uploadVideo($uploadFILEURL.$BusId."/",$_FILES["bus_video_file5"]);
			if($videoFile_5 == $videoFile5)
			{
				unset($update_values);
				$update_values[$SECTION_FIELD_PREFIX.'video_file_5'] 			= trim($videoFile5);
				$SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$BusId;
				$db->updateData($SECTION_TABLE, $update_values, $SECTION_WHERE);
			}
			else
			{
				$_SESSION["video_file_2"] = $videoFile_5;            
				$_SESSION['add_detail_error'][]=$data;
				$SECTION_WHERE=$SECTION_FIELD_PREFIX."id=".$BusId."";
				$action = "Edit";           
				$URL = getMemberURL($SECTION_MANAGE_PAGE,$action,$SECTION_WHERE);
				redirect($URL);
				exit;
			}
		}
		if($data['bus_addNew'] == 'Add New')
		{
			$URL = getMemberURL($SECTION_MANAGE_PAGE); 
			redirect($URL);
			exit;
		}
		else
		{
			$URL = getMemberURL($SECTION_VIEW_PAGE); 
			redirect($URL);
			exit;
		}
	}
	if($action=='Edit')
	{
		$list_query = getSelectList($SECTION_TABLE,$SECTION_FIELD_PREFIX);
		$list_query .= "AND ".$SECTION_FIELD_PREFIX."id = '".$SECTION_AUTO_ID."'";
		$result_query = $db->select($list_query);

		$bus_title		=	$result_query[0][$SECTION_FIELD_PREFIX."title"];
		$bus_name		=	$result_query[0][$SECTION_FIELD_PREFIX."name"];
		$bus_company	=	$result_query[0][$SECTION_FIELD_PREFIX."company"];
		$bus_phone		=	$result_query[0][$SECTION_FIELD_PREFIX."phone"];
		$bus_zip		=	$result_query[0][$SECTION_FIELD_PREFIX."zip"];
		$bus_address1	=	$result_query[0][$SECTION_FIELD_PREFIX."address1"];
		$bus_city		=	$result_query[0][$SECTION_FIELD_PREFIX."city"];
		$bus_email		=	$result_query[0][$SECTION_FIELD_PREFIX."email"];
		$bus_about		=	$result_query[0][$SECTION_FIELD_PREFIX."about"];
		$bus_website	=	$result_query[0][$SECTION_FIELD_PREFIX."website"];
		$bus_fax		=	$result_query[0][$SECTION_FIELD_PREFIX."fax"];
		$bus_address2	=	$result_query[0][$SECTION_FIELD_PREFIX."address2"];
		$bus_sta_id		=	$result_query[0][$SECTION_FIELD_PREFIX."sta_id"];
		$bus_logo_file	=	$result_query[0][$SECTION_FIELD_PREFIX."logo_file"];
		$video_file_1	=	$result_query[0][$SECTION_FIELD_PREFIX."video_file_1"];
		$video_file_2	=	$result_query[0][$SECTION_FIELD_PREFIX."video_file_2"];
		$video_file_3	=	$result_query[0][$SECTION_FIELD_PREFIX."video_file_3"];
		$video_file_4	=	$result_query[0][$SECTION_FIELD_PREFIX."video_file_4"];
		$video_file_5	=	$result_query[0][$SECTION_FIELD_PREFIX."video_file_5"];
		$bus_notes		=	$result_query[0][$SECTION_FIELD_PREFIX."notes"];
		$action = "Update";
		$action_url = getMemberURL($SECTION_MANAGE_PAGE,$action,$SECTION_WHERE);
	}	
	if($action=='')
	{
		$status = "Active";
		$action = "Add";
		$action_url = getMemberURL($SECTION_MANAGE_PAGE,$action);
		
		if($_SESSION['add_detail_error'][0]!="")
		{
			foreach($_SESSION['add_detail_error'][0] as $key => $value)
			{
				$result_query[0][$key]=stripslashes($value);
			}
			$bus_title		=	$result_query[0][$SECTION_FIELD_PREFIX."title"];
			$bus_name		=	$result_query[0][$SECTION_FIELD_PREFIX."name"];
			$bus_company	=	$result_query[0][$SECTION_FIELD_PREFIX."company"];
			$bus_phone		=	$result_query[0][$SECTION_FIELD_PREFIX."phone"];
			$bus_zip		=	$result_query[0][$SECTION_FIELD_PREFIX."zip"];
			$bus_address1	=	$result_query[0][$SECTION_FIELD_PREFIX."address1"];
			$bus_city		=	$result_query[0][$SECTION_FIELD_PREFIX."city"];
			$bus_email		=	$result_query[0][$SECTION_FIELD_PREFIX."email"];
			$bus_about		=	$result_query[0][$SECTION_FIELD_PREFIX."about"];
			$bus_website	=	$result_query[0][$SECTION_FIELD_PREFIX."website"];
			$bus_fax		=	$result_query[0][$SECTION_FIELD_PREFIX."fax"];
			$bus_address2	=	$result_query[0][$SECTION_FIELD_PREFIX."address2"];
			$bus_sta_id		=	$result_query[0][$SECTION_FIELD_PREFIX."sta_id"];
			$bus_logo_file	=	$result_query[0][$SECTION_FIELD_PREFIX."logo_file"];
			$bus_notes		=	$result_query[0][$SECTION_FIELD_PREFIX."notes"];
			unset($_SESSION['add_detail_error']);
		}
	}
?>
<section>
<article id="page" >
	<header>
		<ul class="tab_links ">
			<li><div class="tab_link_active inner">
				<span><img src="<?php echo IMG_WWW; ?>directory.png"></span><h1>Business Cards</h1>
				<div class="view">
					<a href="<?php echo getMemberURL($SECTION_VIEW_PAGE); ?>" class="tooltip" title="back">
					<img src="<?php echo IMG_WWW; ?>back-orange.png"></a>
				</div>
			</div></li>    					
		</ul>
	</header>
	<aside>		
		<div class="tab_content_holder directory">
			<div class="tab_content_holder_inner">
				<div class="block-part">
				<form name="manage_business" enctype="multipart/form-data" method="post" action="<?php echo $action_url; ?>" id="manage_business">
					<div class="main-row">
						<label>Title <?php echo getRequiredIcon()?></label>
						<input type="text" name="bus_title" id="bus_title" tabindex="1" value="<?php echo $bus_title;?>" />
					</div>
					<div class="main-row">
						<label>Company</label>
						<input type="text" name="bus_company" id="bus_company" tabindex="2" value="<?php echo $bus_company;?>" />
					</div>
					<div class="main-row">
						<label>Name <?php echo getRequiredIcon()?></label>
						<input type="text" name="bus_name" id="bus_name" tabindex="3" value="<?php echo $bus_name;?>" />
					</div>
					<div class="main-row">
						<label>Address1 <?php echo getRequiredIcon()?></label>
						<input type="text" name="bus_address1" id="bus_address1" tabindex="4" value="<?php echo $bus_address1;?>" />
					</div>
					<div class="main-row">
						<label>Address2 <?php //echo getRequiredIcon()?></label>
						<input type="text" name="bus_address2" id="bus_address2" tabindex="5" value="<?php echo $bus_address2;?>" />
					</div>
					<div class="main-row">
						<label>City <?php echo getRequiredIcon()?></label>
						<input type="text" name="bus_city" id="bus_city" tabindex="6" value="<?php echo $bus_city;?>" />
					</div>
					<div class="main-row">
						<label>State <?php echo getRequiredIcon()?></label>
						<select name="bus_sta_id" id="bus_sta_id" tabindex="7"  alt="Type to search state">
							<option value="">Select State</option>
							<?php
							for($i=0;$i<count($staRes);$i++)
							{
								if($staRes[$i]['sta_id']==$bus_sta_id) { $select="selected='selected'"; } else { $select=""; }
							?>
							<option value="<?php echo $staRes[$i]['sta_id']; ?>" <?php echo $select; ?> ><?php echo $staRes[$i]['sta_name']; ?></option>
							<?php
							}
							?>
						</select>
					</div>
					<div class="main-row">
						<label>Phone Number <?php echo getRequiredIcon()?></label>
						<input type="text" name="bus_phone" id="bus_phone" tabindex="8" value="<?php echo $bus_phone;?>" />
					</div>
					<div class="main-row">
						<label>Zip Code <?php echo getRequiredIcon()?></label>
						<input type="text" name="bus_zip" id="bus_zip" tabindex="9" value="<?php echo $bus_zip;?>" />
					</div>
					<div class="main-row">
						<label>Fax <?php echo getRequiredIcon()?></label>
						<input type="text" name="bus_fax" id="bus_fax" tabindex="10" value="<?php echo $bus_fax;?>" />
					</div>
					<div class="main-row">
						<label>Email <?php echo getRequiredIcon()?></label>
						<input type="text" name="bus_email" id="bus_email" tabindex="11" value="<?php echo $bus_email;?>" />
					</div>
					<div class="main-row">
						<label>Website</label>
						<input type="text" name="bus_website" id="bus_website" tabindex="12" value="<?php echo $bus_website;?>" />
					</div>
					<div class="main-row" style="height:auto;">
						<label>Video Playlist</label>
						<input tabindex="13" type="file" name="bus_video_file1" id="bus_video_file1" />
						<?php
							if($_SESSION["video_file_1"]!="")
							{
								echo '<label class="error">'.$_SESSION["video_file_1"].'</label>';
								$_SESSION["video_file_1"]="";
							}
							if($action=="Update" && $video_file_1!="")
							{
								echo "<a id='vediosPlay' href='view_file.php?filename=".$video_file_1."&bus_id=".$SECTION_AUTO_ID."' style='margin-top:5px;' class='fancybox fancybox.ajax' title='View' data-fancybox-type='ajax' >".$video_file_1."</a>";
								echo "<input type='hidden' name='edit_bus_video_file_1' value='".$video_file_1."' />";
							}
						?>
	  					<input tabindex="14" type="file" name="bus_video_file2" id="bus_video_file2" />
	  					<?php
							if($_SESSION["video_file_2"]!="")
							{
								echo '<label class="error">'.$_SESSION["video_file_2"].'</label>';
								$_SESSION["video_file_2"]="";
							}
							if($action=="Update" && $video_file_2!="")
							{
								echo "<a id='vediosPlay' href='view_file.php?filename=".$video_file_2."&bus_id=".$SECTION_AUTO_ID."' style='margin-top:5px;' class='fancybox fancybox.ajax' title='View' data-fancybox-type='ajax' >".$video_file_2."</a>";
								echo "<input type='hidden' name='edit_bus_video_file_2' value='".$video_file_2."' />";
							}
						?>
						<input tabindex="15" type="file" name="bus_video_file3" id="bus_video_file3" />
						<?php
							if($_SESSION["video_file_3"]!="")
							{
								echo '<label class="error">'.$_SESSION["video_file_3"].'</label>';
								$_SESSION["video_file_3"]="";
							}
							if($action=="Update" && $video_file_3!="")
							{
								echo "<a id='vediosPlay' href='view_file.php?filename=".$video_file_3."&bus_id=".$SECTION_AUTO_ID."' style='margin-top:5px;' class='fancybox fancybox.ajax' title='View' data-fancybox-type='ajax' >".$video_file_3."</a>";
								echo "<input type='hidden' name='edit_bus_video_file_3' value='".$video_file_3."' />";
							}
						?>
						<input tabindex="16" type="file" name="bus_video_file4" id="bus_video_file4" />
						<?php
							if($_SESSION["video_file_4"]!="")
							{
								echo '<label class="error">'.$_SESSION["video_file_4"].'</label>';
								$_SESSION["video_file_4"]="";
							}
							if($action=="Update" && $video_file_4!="")
							{
								echo "<a id='vediosPlay' href='view_file.php?filename=".$video_file_4."&bus_id=".$SECTION_AUTO_ID."' style='margin-top:5px;' class='fancybox fancybox.ajax' title='View' data-fancybox-type='ajax' >".$video_file_4."</a>";
								echo "<input type='hidden' name='edit_bus_video_file_4' value='".$video_file_4."' />";
							}
						?>
						<input tabindex="17" type="file" name="bus_video_file5" id="bus_video_file5" />
						<?php
							if($_SESSION["video_file_5"]!="")
							{
								echo '<label class="error">'.$_SESSION["video_file_5"].'</label>';
								$_SESSION["video_file_5"]="";
							}
							if($action=="Update" && $video_file_5!="")
							{
								echo "<a id='vediosPlay' href='view_file.php?filename=".$video_file_5."&bus_id=".$SECTION_AUTO_ID."' style='margin-top:5px;' class='fancybox fancybox.ajax' title='View' data-fancybox-type='ajax' >".$video_file_5."</a>";
								echo "<input type='hidden' name='edit_bus_video_file_5' value='".$video_file_5."' />";
							}
						?>						
					</div>
					<div class="main-row" style="height:auto;">
						<label>About <?php echo getRequiredIcon()?></label>
						<textarea name="bus_about" tabindex="18" id="bus_about" style="height:150px;"><?php echo $bus_about; ?></textarea>
					</div>
					<div class="main-row" style="height:auto;">
						<label>Notes</label>
						<textarea name="bus_notes" tabindex="19" id="bus_notes" style="height:150px;"><?php echo $bus_notes; ?></textarea>
					</div>
					<div class="main-row" style="margin-top:20px;">
						<label>Logo File</label>
						<input tabindex="20" type="file" name="bus_logo_file" id="bus_logo_file" />
						<?php
							if($_SESSION["err_file"]!="")
							{
								echo '<label class="error">'.$_SESSION["err_file"].'</label>';
								$_SESSION["err_file"]="";
							}
							if($action=="Update" && $bus_logo_file!="")
							{
								echo "<a id='vediosPlay' href='view_file.php?filename=".$bus_logo_file."&bus_id=".$SECTION_AUTO_ID."' style='margin-top:5px;' class='fancybox fancybox.ajax' title='View' data-fancybox-type='ajax' >".$bus_logo_file."</a>";
								echo "<input type='hidden' name='edit_bus_logo_file' value='".$bus_logo_file."' />";
							}
						?>
					</div>
					<div class="main-row" style="margin-top:20px;">
						<label>&nbsp;</label>
						<input name="bus_id" id="bus_id" value="<?php echo $SECTION_AUTO_ID; ?>" type="hidden">
						<input name="bus_meb_id" id="bus_meb_id" value="<?php echo $mebId; ?>" type="hidden">
						<input type="hidden" name="method" id="method" value="<?php echo $action; ?>" />
						<input type="submit" tabindex="21" name="bus_submit" id="bus_submit" value="Save" />
						<?php if($action!="Update" && $type!="Business")
						{?>
						<input type="submit" tabindex="22" name="bus_addNew" id="bus_addNew" value="Add New" />
						<?php } ?>
					</div>
				</form>
				<div class="clr"></div>
				</div>
				<div class="clr"></div>
			</div>
		</div>
	</aside>
</article>
</section>
<div id="defaultPlay" style="display:none;">
	<div class="fullview_clients">
		<div class="subRow">
			<div class="enterTitle main">
				<h1>Business Information File</h1><a href="javascript:void(0);" class="fancybox-item fancybox-close" title="Close"></a>
			</div>
		</div>
		<div class="subRow">
			<?php
			$fileNameExt = explode(".", $bus_logo_file);
			$fileExt 	 = $fileNameExt[count($fileNameExt)-1];
			$allowedImgExts = array("gif","jpg","jpeg","png","GIF","JPG","JPEG","PNG");
			if(in_array($fileExt,$allowedImgExts))
			{
			echo "<img src='".$uploadFILEWWW.$SECTION_AUTO_ID."/".$bus_logo_file."' width='600' height='300' />";
			}
			else
			{
			?>
			<object width="600" height="300" data="./player.swf" type="application/x-shockwave-flash">
				<param value="opaque" name="wmode">
				<param value="always" name="allowscriptaccess">
				<param value="all" name="allownetworking">
				<param value="file=<?php echo $uploadFILEWWW.$SECTION_AUTO_ID."/".$bus_logo_file; ?>&;volume=80&amp;bufferlength=5&amp;logo.hide=false&amp;&;title=sleeve_length&amp;linktarget=_self" name="flashvars">
				<param name="allowFullScreen" value="true" />
			</object>
			<?php } ?>
		</div>
	</div>
</div>
<script>
$(document).ready(function(){
	$('.tooltip').tooltipster();
	/*$("#vediosPlay").click(function(){
		$("#defaultPlay").dialog({modal: true, height: 320, width: 500 });
	});*/
	$.validator.addMethod("phoneValidate", function(value, element) {
		return this.optional(element) || /^[0-9\-\+\,\.\)\(\s]+$/i.test(value);
	}, "Only numbers,-,+ and , allowed");
	$.validator.addMethod("validImage", function(value, element) {
		return this.optional(element) || /\.(gif|jpg|jpeg|png|GIF|JPG|JPEG|PNG|mp3|MP3|MP4|mp4|M4V|m4v|M4A|m4a|f4v|F4V|f4a|F4A|flv|FLV)$/i.test(value);
		//|mov|MOV|MPG|mpg|VOB|vob|wmv|WMV
	}, "Please select only image");
	$.validator.addMethod("validVideo", function(value, element) {
		return this.optional(element) || /\.(MP4|mp4|M4V|m4v|M4A|m4a|f4v|F4V|f4a|F4A|flv|FLV)$/i.test(value);
		//|mov|MOV|MPG|mpg|VOB|vob|wmv|WMV
	}, "Please select only image");
	$.validator.addMethod('filesize', function(value, element, param) {
		return this.optional(element) || (element.files[0].size <= param) 
	});
	// custom method for url validation with or without http://
	/*$.validator.addMethod("cus_url", function(value, element) { 		
		if(value.substr(0,7) != 'http://'){
			value = 'http://' + value;
		}
		if(value.substr(value.length-1, 1) != '/'){
			value = value + '/';
		}
		return this.optional(element) || /^(http|https|ftp):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(value); 
	}, "Not valid url.");*/
	
	$("#manage_business").validate({
		rules: {
			bus_title : {
				required : true
			},
			bus_name : {
				required : true
			},
			bus_phone : {
				required : true,
				phoneValidate : true
			},
			bus_zip : {
				required : true,
				digits : true
			},
			bus_address1 : {
				required : true
			},
			bus_city : {
				required : true
			},
			bus_email : {
				required : true,
				email : true
			},
			bus_fax : {
				required : true,
				phoneValidate : true
			},
			bus_sta_id : {
				required : true
			},
			bus_about : {
				required : true
			},
			bus_logo_file : {
				validImage : true,
				 filesize: 33554432
			},
			bus_video_file1:{
				validVideo : true
			},
			bus_video_file2:{
				validVideo : true
			},
			bus_video_file3:{
				validVideo : true
			},
			bus_video_file4:{
				validVideo : true
			},
			bus_video_file5:{
				validVideo : true
			}  
			
		},
		messages: {
			bus_title : {
				required : "Please enter business title"
			},
			bus_name : {
				required : "Please enter business name"
			},
			bus_phone : {
				required : "Please enter business phone No.",
				phoneValidate : "Please enter valid phone No."
			},
			bus_zip: {
				required : "Please enter business zip code",
				digits : "Please enter valid zip code"
			},
			bus_address1 : {
				required : "Please enter business address1"
			},
			bus_city : {
				required : "Please enter business city"
			},
			bus_email : {
				required : "Please enter business email",
				email : "Please enter valid email"
			},
			bus_fax : {
				required : "Please enter business fax No.",
				phoneValidate : "Please enter valid fax No."
			},
			bus_sta_id : {
				required : "Please select business state"
			},
			bus_about : {
				required : "Please enter business about"
			},
			bus_logo_file : {
				validImage : "Please select only image, videos and audio file",
				filesize : "Please upload file less then 32MB"
			},
			bus_video_file1:{
				validVideo : "Please select only video file"
			},
			bus_video_file2:{
				validVideo : "Please select only video file"
			},
			bus_video_file3:{
				validVideo : "Please select only video file"
			},
			bus_video_file4:{
				validVideo : "Please select only video file",
			},
			bus_video_file5:{
				validVideo : "Please select only video file",
			}
		}
	});
});
</script>
