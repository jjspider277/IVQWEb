<?php 

include_once('../include/includeclass.php');

if ($_REQUEST['sectionName'])
	$sectionName = $_REQUEST['sectionName'];

if($sectionName == "Messaging")
{ 
	$msg_id = $_REQUEST['msg_id'];
	//subscriber Listing...
	$msg_fields = array("*");
	$msg_where  = "msg_id = ".$msg_id." AND msg_status = 'Active'";
	$msgRes 	= $db->selectData(TBL_MEMBER_MESSAGING,$msg_fields,$msg_where,$extra="",2);

    $msg_file = '';
    if($msgRes[0]['msg_file']!='') {
        $filename = $msgRes[0]['msg_file'];
        $fileNameExt = explode(".", $filename);
        $fileExt 	 = $fileNameExt[count($fileNameExt)-1];
        $allowedImgExts = array("gif","jpg","jpeg","png","GIF","JPG","JPEG","PNG");
        if(in_array($fileExt,$allowedImgExts)) {
            $msg_file = '<div class="enterTitle"><b>Logo image</b><span>:</span> <img style="width: 40%;" src="'.UPLOAD_WWW_MESSAGING_FILE.$msgRes[0]['msg_id'].'/'.$msgRes[0]['msg_file'].'"></div>';
        } else {
            $filename = '';
            $filename = UPLOAD_WWW_MESSAGING_FILE.$msgRes[0]['msg_id'].'/'.$msgRes[0]['msg_file'];

            $msg_file = '<div class="enterTitle"><b>Video</b><span>:</span> <p><object width="400" height="200" data="./player.swf" type="application/x-shockwave-flash">
				<param value="opaque" name="wmode">
				<param value="always" name="allowscriptaccess">
				<param value="all" name="allownetworking">
				<param value="file='.$filename.'&;volume=80&amp;bufferlength=5&amp;logo.hide=false&amp;&;title=sleeve_length&amp;linktarget=_self" name="flashvars">
				<param name="allowFullScreen" value="true" />
			</object></p></div>';
        }
    }
?>
<div class="fullview_clients">
	<div class="subRow">
		 <div class="enterTitle main">
		 	  <h1>Broadcast Information</h1><a href="javascript:void(0);" class="fancybox-item fancybox-close" title="Close"></a>
		</div>
	</div>
    <div class="subRow">
        <div class="enterTitle"><b>Directory Name</b><span>:</span>
            <p>
            <?php
                $dir_fields = array("dir_name");
                $dir_where  = "dir_id = '".$msgRes[0]['msg_dir_id']."'";
                $dirRes = $db->selectData(TBL_DIRECTORY,$dir_fields,$dir_where,$extra="",1);
                echo $dirRes[0]["dir_name"];
                ?>
            </p>
        </div>
    </div>
    <div class="subRow">
        <div class="enterTitle"><b>Company name</b><span>:</span> <p><?php echo $msgRes[0]['msg_company_title']; ?></p></div>
    </div>
	<div class="subRow">
		<div class="enterTitle"><b>Message</b><span>:</span> <p><?php echo $msgRes[0]['msg_message']; ?></p></div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>Sent On</b><span>:</span> <p><?php echo date('m/d/Y',strtotime($msgRes[0]['msg_updated_date'])); ?></p></div>
	</div>
    <div class="subRow">
        <?php echo $msg_file; ?>
    </div>
</div>
<?php
}
else if($sectionName == "Services")
{ 
	$ser_id = $_REQUEST['ser_id'];
	//subscriber Listing...
	$ser_fields = array("*");
	$ser_where  = "ser_id = ".$ser_id." AND ser_status = 'Active'";
	$serRes 	= $db->selectData(TBL_MEMBER_SERVICES,$ser_fields,$ser_where,$extra="",2);
?>
<div class="fullview_clients">
	<div class="subRow">
		 <div class="enterTitle main">
		 	  <h1>Services Information</h1><a href="javascript:void(0);" class="fancybox-item fancybox-close" title="Close"></a>
		</div>
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>Services Title</b><span>:</span> <p><?php echo $serRes[0]['ser_title']; ?></p></div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>Services Description</b><span>:</span> <p><?php echo $serRes[0]['ser_description']; ?></p></div>
	</div>
</div>
<?php
}
else if($sectionName == "BusinessSub")
{ 
	$bsb_id = $_REQUEST['bsb_id'];
	//subscriber Listing...
	$bsb_fields = array("*");
	$bsb_where  = "bsb_id = ".$bsb_id." AND bsb_status != 'Deleted'";
	$bussubRes 	= $db->selectData(TBL_BUSINESS_SUB,$bsb_fields,$bsb_where,$extra="",2);
?>
<div class="fullview_clients">
	<div class="subRow">
		 <div class="enterTitle main">
		 	  <h1>Business Subscriber Information</h1><a href="javascript:void(0);" class="fancybox-item fancybox-close" title="Close"></a>
		</div>
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>Directory Name</b><span>:</span> <p><?php
		$dir_fields = array("dir_name");
		$dir_where  = "dir_id = '".$bussubRes[0]['bsb_dir_id']."'";
		$dirRes = $db->selectData(TBL_DIRECTORY,$dir_fields,$dir_where,$extra="",1);
		echo $dirRes[0]["dir_name"]; 
		?></p></div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>Business Name</b><span>:</span> <p><?php echo $bussubRes[0]['bsb_name']; ?></p></div>
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>Email</b><span>:</span> <p><?php echo $bussubRes[0]['bsb_username']; ?></p></div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>Question</b><span>:</span> <p><?php echo $bussubRes[0]['bsb_question']; ?></p></div>
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>Answer</b><span>:</span> <p><?php echo $bussubRes[0]['bsb_answer']; ?></p></div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>Phone Number</b><span>:</span> <p><?php echo $bussubRes[0]['bsb_phone']; ?></p></div>
	</div>
</div>
<?php
}
else if($sectionName == "Subscribers")
{ 
	$sub_id = $_REQUEST['sub_id'];
	//subscriber Listing...
	$sub_fields = array("*");
	$sub_where  = "sub_id = ".$sub_id;
	$subRes 	= $db->selectData(TBL_MEMBER_SUBSCRIBERS,$sub_fields,$sub_where,$extra="",2);
	
?>
<div class="fullview_clients">
	<div class="subRow">
		 <div class="enterTitle main">
		 	  <h1>Subscriber Information</h1><a href="javascript:void(0);" class="fancybox-item fancybox-close" title="Close"></a>
		</div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>Directory Name</b><span>:</span>
		<p><?php
			$dir_fields = array("dir_name");
			$dir_where  = "dir_id = '".$subRes[0]['sub_dir_id']."'";
			$dirRes = $db->selectData(TBL_DIRECTORY,$dir_fields,$dir_where,$extra="",1);
			echo $dirRes[0]["dir_name"]; 
		?></p>
		</div>
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>Title</b><span>:</span> <p><?php echo $subRes[0]['sub_title']; ?></p></div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>Company Name</b><span>:</span> <p><?php echo $subRes[0]['sub_company']; ?></p></div>
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>Name</b><span>:</span> <p><?php echo $subRes[0]['sub_name']; ?></p></div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>Phone</b><span>:</span> <p><?php echo $subRes[0]['sub_phone']; ?></p></div>
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>Zip Code</b><span>:</span> <p><?php echo $subRes[0]['sub_zip']; ?></p></div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>City</b><span>:</span><p><?php echo $subRes[0]['sub_city']; ?></p> </div>
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>State</b><span>:</span> <p><?php //echo $subRes[0]['sub_state'];
			$state_fields = array("sta_name");
			$state_where  = "sta_id = '".$subRes[0]['sub_sta_id']."'";
			$staRes = $db->selectData(TBL_STATE,$state_fields,$state_where,$extra="",1);
			echo $staRes[0]["sta_name"]; 
		 ?> </p></div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>Email</b><span>:</span><p><a href="mailto:<?php echo $subRes[0]['sub_email'] ?>"><?php echo $subRes[0]['sub_email']; ?></a></p> </div>
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>Question</b><span>:</span><p><?php echo $subRes[0]['sub_question'] ?></a></p> </div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>Answer</b><span>:</span><p><?php echo $subRes[0]['sub_answer'] ?></a></p> </div>
	</div>
</div>
<?php
} else if($sectionName == "Business") {
	$type = $_REQUEST["type"];
	if($type=="Member") {
		$SECTION_TABLE= TBL_MEMBER_BUSINESS;
		$uploadFILEWWW = UPLOAD_WWW_BUSINESS_FILE;
	} else {
		$SECTION_TABLE= TBL_MEMBER_BUSINESS_SUB;
		$uploadFILEWWW = UPLOAD_WWW_BUSINESS_FILE."Business_Sub/";
	}
	$bus_id = $_REQUEST['bus_id'];
	//subscriber Listing...
	$bus_fields = array("*");
	$bus_where  = "bus_id = ".$bus_id." AND bus_status = 'Active'";
	$busRes 	= $db->selectData($SECTION_TABLE,$bus_fields,$bus_where,$extra="",2);

    $logo_image = 'images/no_logo.png';
    if($busRes[0]['bus_logo_file']!="") {
        $fileNameExt = explode(".", $busRes[0]['bus_logo_file']);
        $fileExt 	 = $fileNameExt[count($fileNameExt)-1];
        $allowedImgExts = array("gif","jpg","jpeg","png","GIF","JPG","JPEG","PNG");
        if(in_array($fileExt,$allowedImgExts)) {
            $logo_image = $uploadFILEWWW.$bus_id."/".$busRes[0]['bus_logo_file'];
        } else {
            $logo_image = 'images/no_logo.png';
        }
    }

    $pro_image = 'images/group-user-list-icon.png';
    if($busRes[0]['bus_picture_file']!="") {
        $fileNameExt = explode(".", $busRes[0]['bus_picture_file']);
        $fileExt 	 = $fileNameExt[count($fileNameExt)-1];
        $allowedImgExts = array("gif","jpg","jpeg","png","GIF","JPG","JPEG","PNG");
        if(in_array($fileExt,$allowedImgExts)) {
            $pro_image = $uploadFILEWWW.$bus_id."/".$busRes[0]['bus_picture_file'];
        } else {
            $pro_image = 'images/group-user-list-icon.png';
        }
    }

    $is_video = '';
    $video_list = '';
    if($busRes[0]['bus_video_file_1']!="" || $busRes[0]['bus_video_file_2']!="" || $busRes[0]['bus_video_file_3']!="" || $busRes[0]['bus_video_file_4']!="" || $busRes[0]['bus_video_file_5']!="") {
        $is_video = '<a  href="javascript:void(0);" class="video_show_slide"><i class="icon_style no_media_icon_blue"></i>Media</a>';

        $video_list = '<div class="videoslider_main" style="display: none;"><a href="javascript:void(0);" class="close_video">Close</a><div class="user_slider videoslider extranal_arrow">';

        if($busRes[0]['bus_video_file_1']!="") {
            $filename = '';
            $filename = $uploadFILEWWW.$bus_id.'/'.$busRes[0]['bus_video_file_1'];

            $video_list .= '<div class="videoSlide">';
            if($busRes[0]['bus_video_title_1']!='')
                $video_list .= '<span class="video_title">'.$busRes[0]['bus_video_title_1'].'</span>';

            $video_list .= '<object width="600" height="300" data="./player.swf" type="application/x-shockwave-flash">
				<param value="opaque" name="wmode">
				<param value="always" name="allowscriptaccess">
				<param value="all" name="allownetworking">
				<param value="file='.$filename.'&;volume=80&amp;bufferlength=5&amp;logo.hide=false&amp;&;title=sleeve_length&amp;linktarget=_self" name="flashvars">
				<param name="allowFullScreen" value="true" />
			</object></div>';
        }
        if($busRes[0]['bus_video_file_2']!="") {
            $filename = '';
            $filename = $uploadFILEWWW.$bus_id.'/'.$busRes[0]['bus_video_file_2'];
            $video_list .= '<div class="videoSlide">';
            if($busRes[0]['bus_video_title_2']!='')
                $video_list .= '<span class="video_title">'.$busRes[0]['bus_video_title_2'].'</span>';

            $video_list .= '<object width="600" height="300" data="./player.swf" type="application/x-shockwave-flash">
				<param value="opaque" name="wmode">
				<param value="always" name="allowscriptaccess">
				<param value="all" name="allownetworking">
				<param value="file='.$filename.'&;volume=80&amp;bufferlength=5&amp;logo.hide=false&amp;&;title=sleeve_length&amp;linktarget=_self" name="flashvars">
				<param name="allowFullScreen" value="true" />
			</object></div>';
        }

        if($busRes[0]['bus_video_file_3']!=""){
            $filename = '';
            $filename = $uploadFILEWWW.$bus_id.'/'.$busRes[0]['bus_video_file_3'];
            $video_list .= '<div class="videoSlide">';
            if($busRes[0]['bus_video_title_3']!='')
                $video_list .= '<span class="video_title">'.$busRes[0]['bus_video_title_3'].'</span>';
            $video_list .= '<object width="600" height="300" data="./player.swf" type="application/x-shockwave-flash">
				<param value="opaque" name="wmode">
				<param value="always" name="allowscriptaccess">
				<param value="all" name="allownetworking">
				<param value="file='.$filename.'&;volume=80&amp;bufferlength=5&amp;logo.hide=false&amp;&;title=sleeve_length&amp;linktarget=_self" name="flashvars">
				<param name="allowFullScreen" value="true" />
			</object></div>';
        }

        if($busRes[0]['bus_video_file_4']!="") {
            $filename = '';
            $filename = $uploadFILEWWW.$bus_id.'/'.$busRes[0]['bus_video_file_4'];
            $video_list .= '<div class="videoSlide">';
            if($busRes[0]['bus_video_title_4']!='')
                $video_list .= '<span class="video_title">'.$busRes[0]['bus_video_title_4'].'</span>';
            $video_list .= '<object width="600" height="300" data="./player.swf" type="application/x-shockwave-flash">
				<param value="opaque" name="wmode">
				<param value="always" name="allowscriptaccess">
				<param value="all" name="allownetworking">
				<param value="file='.$filename.'&;volume=80&amp;bufferlength=5&amp;logo.hide=false&amp;&;title=sleeve_length&amp;linktarget=_self" name="flashvars">
				<param name="allowFullScreen" value="true" />
			</object></div>';
        }

        if($busRes[0]['bus_video_file_5']!="") {
            $filename = '';
            $filename = $uploadFILEWWW.$bus_id.'/'.$busRes[0]['bus_video_file_5'];
            $video_list .= '<div class="videoSlide">';
            if($busRes[0]['bus_video_title_4']!='')
                $video_list .= '<span class="video_title">'.$busRes[0]['bus_video_title_4'].'</span>';
            $video_list .= '<object width="600" height="300" data="./player.swf" type="application/x-shockwave-flash">
				<param value="opaque" name="wmode">
				<param value="always" name="allowscriptaccess">
				<param value="all" name="allownetworking">
				<param value="file='.$filename.'&;volume=80&amp;bufferlength=5&amp;logo.hide=false&amp;&;title=sleeve_length&amp;linktarget=_self" name="flashvars">
				<param name="allowFullScreen" value="true" />
			</object></div>';
        }

        $video_list .= '</div></div>';

    } else {
        $is_video = '<a  href="javascript:void(0);"><i class="icon_style no_media_icon_new"></i>No Media</a>';
    }


    //$busRes[0]['bus_about']; $busRes[0]['bus_notes'];
?>
<div class="fullview_clients" id="business">
    <div class="container group-setion">
        <div class="rowss">
            <div class="max_width">

                <div class="second_menu">
                    <ul>
                        <li><?php echo $is_video; ?></li>
                    </ul>
                </div>

                <?php echo $video_list; ?>

                <div class="user_slider detail_slider" id="owl-demo">
                    <div class="item">
                        <div class="user_slide">
                            <a href="javascript:void(0)"><img src="<?php echo $pro_image; ?>" class="popphoto"></a>
                            <div class="user_name">
                                <strong><?php echo $busRes[0]['bus_name']." ".$busRes[0]['bus_lname']; ?></strong>&#8232;<?php echo $busRes[0]['bus_title']; ?>
                                <br/><?php echo $busRes[0]['bus_company']; ?>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="user_slide cmpny_info_slide">
                            <a href="javascript:void(0)"><img src="<?php echo $logo_image; ?>" style="max-height: 120px;" class="popphoto"></a>
                            <div class="company_detail">
                                <p>
                                    <?php
                                        if($busRes[0]['bus_address1']!='')
                                            echo $busRes[0]['bus_address1'].', ';
                                        if($busRes[0]['bus_address2']!='')
                                            echo $busRes[0]['bus_address2'].', ';

                                        if($busRes[0]['bus_city']!='')
                                            echo $busRes[0]['bus_city'];


                                        $state_fields = array("sta_name");
                                        $state_where  = "sta_id = '".$busRes[0]['bus_sta_id']."'";
                                        $staRes = $db->selectData(TBL_STATE,$state_fields,$state_where,$extra="",1);

                                        echo '<br>';
                                        if($staRes[0]["sta_name"]!='')
                                            echo $staRes[0]["sta_name"];
                                        if($busRes[0]['bus_zip'])
                                            echo ', '.$busRes[0]['bus_zip'];

                                        echo '<br>';

                                        if($busRes[0]['bus_website']!='')
                                            echo '<a target="_new" href="'.$busRes[0]['bus_website'].'">'.$busRes[0]['bus_website'].'</a><br>';

                                        if($busRes[0]['bus_phone']!='')
                                            echo 'P. '.$busRes[0]['bus_phone'].'<br>';
                                        if($busRes[0]['bus_fax']!='')
                                            echo 'P. '.$busRes[0]['bus_fax'].'<br>';
                                    ?>
                                    <a href="mailto:<?php echo $busRes[0]['bus_email'] ?>"><?php echo $busRes[0]['bus_email'] ?></a>
                                </p>
                            </div>
                        </div>
                    </div>

                </div>


            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $("#owl-demo").owlCarousel({
            navigation : true, // Show next and prev buttons
            slideSpeed : 300,
            paginationSpeed : 400,
            singleItem:true
            // "singleItem:true" is a shortcut for:
            // items : 1,
            // itemsDesktop : false,
            // itemsDesktopSmall : false,
            // itemsTablet: false,
            // itemsMobile : false
        });

        $(".videoslider").owlCarousel({
            navigation : true, // Show next and prev buttons
            slideSpeed : 300,
            paginationSpeed : 400,
            singleItem:true
            // "singleItem:true" is a shortcut for:
            // items : 1,
            // itemsDesktop : false,
            // itemsDesktopSmall : false,
            // itemsTablet: false,
            // itemsMobile : false
        });
    });

</script>
<?php 
}
if($sectionName == "Coupons")
{ 
	$cop_id = $_REQUEST['cop_id'];
	//Coupons Listing...
	$cop_fields = array("*");
	$cop_where  = "cop_id = ".$cop_id." AND cop_status = 'Active'";
	$copRes 	= $db->selectData(TBL_MEMBER_COUPONS,$cop_fields,$cop_where,$extra="",2);

    $col_im = '';
    if($copRes[0]['cop_logo_file']!='') {
        $filename = $copRes[0]['cop_logo_file'];
        $fileNameExt = explode(".", $filename);
        $fileExt 	 = $fileNameExt[count($fileNameExt)-1];
        $allowedImgExts = array("gif","jpg","jpeg","png","GIF","JPG","JPEG","PNG");
        if(in_array($fileExt,$allowedImgExts)) {
            $col_im = '<div class="enterTitle"><b>Logo image</b><span>:</span> <p><img style="width: 40%;" src="'.UPLOAD_WWW_COUPONS_FILE . $copRes[0]['cop_id'] . '/' . $copRes[0]['cop_logo_file'].'"></p></div>';
        } else {
            $filename = '';
            $filename = UPLOAD_WWW_COUPONS_FILE . $copRes[0]['cop_id'] . '/' . $copRes[0]['cop_logo_file'];

            $col_im = '<div class="enterTitle"><b>Video</b><span>:</span> <p><object width="400" height="200" data="./player.swf" type="application/x-shockwave-flash">
				<param value="opaque" name="wmode">
				<param value="always" name="allowscriptaccess">
				<param value="all" name="allownetworking">
				<param value="file='.$filename.'&;volume=80&amp;bufferlength=5&amp;logo.hide=false&amp;&;title=sleeve_length&amp;linktarget=_self" name="flashvars">
				<param name="allowFullScreen" value="true" />
			</object></p></div>';
        }
    }
?>
<div class="fullview_clients">	
	<div class="subRow">
		 <div class="enterTitle main">
		 	  <h1>Coupons Information</h1><a href="javascript:void(0);" class="fancybox-item fancybox-close" title="Close"></a>
		</div>
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>Coupons Title</b><span>:</span> <p><?php echo $copRes[0]['cop_title']; ?></p></div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>Percentage</b><span>:</span> <p><?php echo $copRes[0]['cop_percentage']; ?>%</p></div>
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>Description</b><span>:</span> <p><?php echo $copRes[0]['cop_description']; ?></p></div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>Coupon Code</b><span>:</span><p><?php echo $copRes[0]['cop_coupon_code']; ?></p> </div>
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>Coupon Valid From</b><span>:</span><p><?php echo date('m/d/Y',strtotime($copRes[0]['cop_valid_from'])); ?></p> </div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>Coupon Valid To</b><span>:</span><p><?php echo date('m/d/Y',strtotime($copRes[0]['cop_valid_thru'])); ?></p> </div>
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>Bar code</b><span>:</span> <p><img src="<?php echo UPLOAD_WWW_COUPONS_FILE.$cop_id."/".$copRes[0]['cop_barcode']; ?>" alt="Barcode" title="Barcode"></p></div>
	</div>
	<?php
        if($copRes[0]['cop_logo_file']!='') {
            echo $col_im;
        }
    ?>
</div>
<?php
} ?>

<?php if($sectionName == "Directory"){ 

if ($_REQUEST['sectionName'])
	$dirId = $_REQUEST['dir_id'];

	//Directory Listing...
$dir_fields = array("*");
$dir_where  = "dir_id = ".$dirId." AND dir_status = 'Active'";
$dirRes 	= $db->selectData(TBL_DIRECTORY ." LEFT JOIN ".TBL_DIRECTORY_CATEGORY." ON dir_dic_id = dic_id"." LEFT JOIN ".TBL_STATE." ON dir_sta_id = sta_id"." LEFT JOIN ".TBL_COUNTRY." ON dir_con_id = con_id",$dir_fields,$dir_where,$extra="",2);
?>

<div class="fullview_clients">
	<div class="subRow">
		 <div class="enterTitle main">
		 	  <h1>Group Information</h1><a href="javascript:;" class="fancybox-item fancybox-close" title="Close"></a>
		</div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>Group Category</b><span>:</span> <p><?php echo $dirRes[0]['dic_name']; ?></p></div>
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>Group Name</b><span>:</span> <p><?php echo $dirRes[0]['dir_name']; ?></p></div>
	</div>
	<div class="subRow light">
		<div class="enterTitle address-title"><b>Address </b><span>:</span> <p><?php echo $dirRes[0]['dir_address']; ?> </p></div>
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>City </b><span>:</span> <p><?php echo $dirRes[0]['dir_city']; ?></p></div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>State </b><span>:</span> <p><?php echo $dirRes[0]['sta_name']; ?></p></div>
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>Zip</b><span>:</span><p><?php echo $dirRes[0]['dir_zip']; ?></p> </div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>Country</b><span>:</span><p><?php echo $dirRes[0]['con_name']; ?></p> </div>
	</div>
	<div class="subRow">
		<div class="enterTitle courses-details-title"><b>Website </b><span>:</span><p><?php echo $dirRes[0]['dir_website']; ?></p></div>
	</div>  
</div>
<?php }?>