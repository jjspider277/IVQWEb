<?php
$mebId = getMemberSessionId();
$mebType = getMemberType();

if($filename == MEB_HOME){
    $_SESSION['mebDirId'] = "";
}

if(isset($_REQUEST['dir_id']) && $_REQUEST['dir_id']!='') {
    $_SESSION['mebDirId'] = $_REQUEST['dir_id'];
    if($filename!=MEB_CREATE_GROUP_1) {
        ?>
        <script>
            var uri = window.location.toString();
            if (uri.indexOf("?") > 0) {
                var clean_uri = uri.substring(0, uri.indexOf("&dir_id"));
                window.history.replaceState({}, document.title, clean_uri);
            }
        </script>
        <?php
    }
}

$dirId = getMemberSessionDirId();
if($dirId!='') {
    $dir_files  = array("dir_id","dir_name","dir_private");
    $dir_where  = "dir_status = 'Active' and dir_id='".$dirId."'";
    $dirRes 	= $db->selectData(TBL_DIRECTORY,$dir_files,$dir_where,$extra="",2);
}

$header_link=MEMBER_WWW.ADM_INDEX_PARAMETER.MEB_FRONT;

$header_class='';
$logo_image=ROOT_WWW.'/members/images/logo.png';
$logo_class='';
if($filename == MEB_LOGIN || $filename == MEB_REGISTER || $filename == MEB_FRONT|| $filename == MEB_FORGOT ) {

} else {
    $header_class='logo-section-group';
    $logo_image=ROOT_WWW.'/members/images/logo-2.png';
    $logo_class='logo-group1';
}

?>

<div class="container header">
    <div class="row">
        <?php if($filename == MEB_MANAGE_BUSINESS || $filename == MEB_VIEW_BUSINESS ||
                 $filename == MEB_MANAGE_COUPONS || $filename == MEB_VIEW_COUPONS ||
                 $filename == MEB_MANAGE_MESSAGING || $filename == MEB_VIEW_MESSAGING ||
                 $filename == MEB_MANAGE_SERVICES || $filename == MEB_VIEW_SERVICES ||
                 $filename == MEB_PROFILE || $filename == MEB_VIEW_REQUESTS) { //onclick="window.history.back();"
            if($filename != MEB_PROFILE){
                if($_SESSION['mebDirId']=='') {
                    $URL = MEMBER_WWW.ADM_INDEX_PARAMETER.MEB_HOME;
                    $_SESSION['msg_gr']='Please first select any group.';
                    redirect($URL);
                    exit;
                }
            }

            ?>
            <div class="cre-bottons group-cre-botton head_bak"><a href="<?php echo MEMBER_WWW.MEB_INDEX_PARAMETER.MEB_HOME;?>" >Manage group</a></div>
        <?php } ?>

        <?php
            if($filename == MEB_TERMS || $filename == MEB_OPPORTUNITIES || $filename == MEB_INVESTOR_NEWS ||
               $filename == MEB_ABOUT_US || $filename == MEB_CONTACT_US || $filename == MEB_PRIVACY_POLICY) {
                if(getMemberSessionId()!=''){
        ?>
                    <div class="cre-bottons group-cre-botton head_bak"><a href="<?php echo MEMBER_WWW.MEB_INDEX_PARAMETER.MEB_HOME;?>" >Manage group</a></div>
        <?php
                }
            }
        ?>

        <input type="hidden" name="path_url" id="path_url" value="<?php echo MEMBER_WWW;?>">
        <?php
            $welcome = '';
            if($mebId!='') {
                $user_files = array("meb_dir_id","meb_name","meb_lastname");
                $user_where  = "meb_status = 'Active' and meb_id='".$mebId."'";
                $userRes 	= $db->selectData(TBL_MEMBER,$user_files,$user_where,$extra="",2);

                $dirId = getMemberSessionDirId();
                $mebName = $userRes[0]['meb_name'].' '.$userRes[0]['meb_lastname'];

                $welcome = '<a class="profile_link" href="'.MEB_INDEX_PARAMETER.MEB_PROFILE.'">Welcome, '.$mebName.'</a>';
            } else {
                $welcome = 'Join the IVQ community today';
            }
        ?>
        <div class="header-left"><?php echo $welcome; ?></div>

        <?php if($filename == MEB_LOGIN ) { ?>
            <div class="cre-botton"><a href="<?php echo MEB_INDEX_PARAMETER.MEB_REGISTER?>">Create account</a></div>
        <?php } else if($filename == MEB_REGISTER || $filename == MEB_FRONT || $filename == MEB_FORGOT ) { ?>
            <div class="cre-botton"><a href="<?php echo MEB_INDEX_PARAMETER.MEB_LOGIN?>">Member Login</a></div>
        <?php
            } else {
            $header_link=MEMBER_WWW.ADM_INDEX_PARAMETER.MEB_HOME;

            $dir_name='';
            $dir_class='cre-bottons group-cre-botton';
            if(count($dirRes)>0 && $dirId!='') {
                $dir_name='<span class="header-top-right-text">'.$dirRes[0]['dir_name'].'</span>';
                $dir_class='group-cre-botton';
            }
        ?>
            <a style="display: none;" href="<?php echo MEB_INDEX_PARAMETER.MEB_CHANGE_PASSWORD; ?>">Change Password</a>
            <?php if($_SESSION['meb_id']=='') { ?>
                <div class="cre-botton"><a href="<?php echo MEB_INDEX_PARAMETER.MEB_LOGIN?>">Member Login</a></div>
            <?php } else { ?>
                <div class="<?php echo $dir_class; ?>"><?php echo $dir_name; ?><a href="<?php echo MEB_INDEX_PARAMETER.ADM_LOGOUT; ?>">Log out</a></div>
            <?php } ?>
        <?php } ?>


    </div>
</div>

<div class="logo-section <?php echo $header_class; ?>">
    <div class="logo <?php echo $logo_class; ?>"><a href="<?php echo $header_link; ?>"><img src="<?php echo $logo_image; ?>" title="IVQ" alt=""></a></div>
</div>

<?php
    if($filename == MEB_VIEW_BUSINESS || $filename == MEB_MANAGE_BUSINESS ||
        $filename == MEB_VIEW_COUPONS || $filename == MEB_MANAGE_COUPONS ||
        $filename == MEB_VIEW_MESSAGING || $filename == MEB_MANAGE_MESSAGING ||
        $filename == MEB_VIEW_SERVICES || $filename == MEB_MANAGE_SERVICES ||
        $filename == MEB_VIEW_REQUESTS || $filename == MEB_MANAGE_REQUESTS || $filename == MEB_VIEW_RESPONSE) {

        $style='';
        $nav_show='';
        if($filename == MEB_MANAGE_BUSINESS || $filename == MEB_MANAGE_COUPONS ||$filename == MEB_MANAGE_MESSAGING || $filename == MEB_MANAGE_SERVICES || $filename == MEB_VIEW_RESPONSE) {
            $style='display: none;';
            $nav_show='<div class="nav_in_manage row"><a class="show_manage_menu" href="javascript:void(0);"><i class="line"></i><i class="line line2"></i><i class="line line3"></i></a></div>';
        }
?>

<?php
if(count($dirRes)>0 && $dirId!='') {
    $is_p = $dirRes[0]['dir_private'];
    if ($dirRes[0]['dir_private'] == 'yes') {
?>
        <style>
            .member-ul-tab ul {
                width: 750px;
            }
            .member-ul-tab li {
                width: 20%;
            }
        </style>
<?php
    }
}
?>

<div class="container member-tab-section header_main_menu_class" style="<?php echo $style; ?>">
    <div class="row">
        <div class="member-ul-tab">
            <ul>
                <li><a href="<?php echo MEB_INDEX_PARAMETER.MEB_VIEW_SERVICES; ?>"><em class="services-img <?php if($filename == MEB_VIEW_SERVICES || $filename == MEB_MANAGE_SERVICES){ ?>services-active-img<?php } ?>"></em><span class="list-name <?php if($filename == MEB_VIEW_SERVICES || $filename == MEB_MANAGE_SERVICES){ ?>list-active-name<?php } ?>">Services</span></a></li>
                <li><a href="<?php echo MEB_INDEX_PARAMETER.MEB_VIEW_BUSINESS; ?>"><em class="members-img <?php if($filename == MEB_VIEW_BUSINESS || $filename == MEB_MANAGE_BUSINESS){ ?>members-active-img<?php } ?>"></em><span class="list-name <?php if($filename == MEB_VIEW_BUSINESS || $filename == MEB_MANAGE_BUSINESS){ ?>list-active-name<?php } ?>">Members</span></a></li>
                <li><a href="<?php echo MEB_INDEX_PARAMETER.MEB_VIEW_COUPONS; ?>"><em class="coupons-img <?php if($filename == MEB_VIEW_COUPONS || $filename == MEB_MANAGE_COUPONS){ ?>coupons-active-img<?php } ?>"></em><span class="list-name <?php if($filename == MEB_VIEW_COUPONS || $filename == MEB_MANAGE_COUPONS){ ?>list-active-name<?php } ?>">Coupons</span></a></li>
                <li><a href="<?php echo MEB_INDEX_PARAMETER.MEB_VIEW_MESSAGING; ?>"><em class="messages-img <?php if($filename == MEB_VIEW_MESSAGING || $filename == MEB_MANAGE_MESSAGING || $filename == MEB_VIEW_RESPONSE){ ?>messages-active-img<?php } ?>"></em><span class="list-name <?php if($filename == MEB_VIEW_MESSAGING || $filename == MEB_MANAGE_MESSAGING || $filename == MEB_VIEW_RESPONSE){ ?>list-active-name<?php } ?>">Broadcast</span></a></li>
                <?php
                    if(count($dirRes)>0 && $dirId!='') {
                        if($dirRes[0]['dir_private']=='yes') {
                ?>
                            <li><a href="<?php echo MEB_INDEX_PARAMETER.MEB_VIEW_REQUESTS; ?>"><em class="request-img <?php if($filename == MEB_VIEW_REQUESTS || $filename == MEB_MANAGE_REQUESTS){ ?>request-active-img<?php } ?>"></em><span class="list-name <?php if($filename == MEB_VIEW_REQUESTS || $filename == MEB_MANAGE_REQUESTS){ ?>list-active-name<?php } ?>">Requests</span></a></li>
                <?php
                        }
                    }
                ?>
            </ul>
        </div>
    </div>
</div>
<?php echo $nav_show; ?>
<?php } ?>
<script>
    $(document).on('click','.show_manage_menu',function() {
        is_p = '<?php echo $is_p; ?>';
        if($('.header_main_menu_class').is(':visible')){
            $('.header_main_menu_class').stop().slideUp("slow", function(){
                if(is_p=='yes')
                    $('.header_main_menu_class .row div').css('padding-top','');
            });
            $(this).removeClass("open_item");
        } else {
            if(is_p=='yes')
                $('.header_main_menu_class .row div').css('padding-top','13px');
            $('.header_main_menu_class').stop().slideDown("slow", function(){

            });
            $(this).addClass("open_item");
        }
    });
</script>
