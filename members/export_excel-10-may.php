<?php
ob_start();
include_once('../include/includeclass.php');
ini_set('memory_limit', '-1');
/** Error reporting */
	@session_start();
   
  if(empty($_SESSION['meb_id'])){
	header('Location:login.php');
  }
  $table = $_GET["table"];
  $prifix = $_GET["prifix"];
/*exit;*/
error_reporting(0);

date_default_timezone_set('Europe/London');
/** PHPExcel */
require_once '../include/Classes/PHPExcel.php';

//$con = mysql_connect("localhost", "root", "");
//$db_selected = mysql_select_db("nccsolutions", $con);

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set properties
$objPHPExcel->getProperties()->setCreator("Chintan")
        ->setLastModifiedBy("Chintan")
        ->setTitle("Office 2007 XLSX Client Document")
        ->setSubject("Office 2007 XLSX TestClient Document")
        ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
        ->setKeywords("office 2007 openxml php")
        ->setCategory("Clients Report");


// Query

if($table == "tbl_member_subscriber")
{
	$select_data_query = "SELECT *,(select sta_name from tbl_state where sta_id = sub_sta_id ) as Statename FROM ".$table." WHERE  `".$prifix."status`!='Deleted' AND `".$prifix."id`!=0 AND sub_meb_id='".$_SESSION["meb_id"]."' order by ".$prifix."id desc";	
}
else if($table == "tbl_member_services")
{
    $select_data_query = "SELECT * FROM ".$table." WHERE  `".$prifix."status`!='Deleted' AND `".$prifix."id`!=0 AND ser_meb_id='".$_SESSION["meb_id"]."' order by ".$prifix."id desc"; 
}
else if($table == "tbl_member_business")
{
    $select_data_query = "SELECT *,(select sta_name from tbl_state where sta_id = bus_sta_id ) as Statename FROM ".$table." WHERE  `".$prifix."status`!='Deleted' AND `".$prifix."id`!=0 AND bus_meb_id='".$_SESSION["meb_id"]."' order by ".$prifix."id desc"; 
}
else if($table == "tbl_member_coupons")
{
    $select_data_query = "SELECT * FROM ".$table." WHERE  `".$prifix."status`!='Deleted' AND `".$prifix."id`!=0 AND cop_meb_id='".$_SESSION["meb_id"]."' order by ".$prifix."id desc"; 
}
else if($table == "tbl_member_messaging")
{
    $select_data_query = "SELECT * FROM ".$table." WHERE  `".$prifix."status`!='Deleted' AND `".$prifix."id`!=0 AND msg_meb_id='".$_SESSION["meb_id"]."' order by ".$prifix."id desc"; 
}
$geted_data = $db->select($select_data_query);

/*echo "<pre>";
print_r($geted_data);
echo "</pre>";
exit;*/

$objPHPExcel->getActiveSheet()->getStyle('A3:IK3')->getFont()->setBold(true);
if($table=="tbl_member_subscriber")
{    
    $objPHPExcel->getActiveSheet()->getStyle('A3:H3')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);  
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20); 
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20); 
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20); 
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20); 
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20); 
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20); 
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15); 
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20); 
    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20); 
     
    
    $title_id = 3;
    $continue_val=-1;
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Sr No');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Title');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Company Name');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Subscriber Name');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Subscriber Phone');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Subscriber Zip Code');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Subscriber City');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Subscriber State');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Subscriber Email');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Subscriber Status');
    $sr = 1;
    $i = 5;
    $static_sr_no=1;
    $start_val=-1;
    foreach ($geted_data as $geted_data) {
        //Get Communication records.
        $start_val=-1;
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val ,$i, $sr);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['sub_title']);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['sub_company']);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['sub_name']);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['sub_phone']);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['sub_zip']);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['sub_city']);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['Statename']);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['sub_email']);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['sub_status']);
        $i++;
        $sr++;
        $var_loop = 0;
    }
}
else if($table == "tbl_member_services")
{
    $objPHPExcel->getActiveSheet()->getStyle('A3:C3')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);  
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20); 
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30); 
    
    $title_id = 3;
    $continue_val=-1;
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Sr No');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Services Title');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Services Description');
    
    $sr = 1;
    $i = 5;
    $static_sr_no=1;
    $start_val=-1;
    foreach ($geted_data as $geted_data) {
        //Get Communication records.
        $start_val=-1;
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val ,$i, $sr);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['ser_title']);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['ser_description']);
        $i++;
        $sr++;
        $var_loop = 0;
    }
}
else if($table == "tbl_member_business")
{
    $objPHPExcel->getActiveSheet()->getStyle('A3:N3')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);  
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20); 
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20); 
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20); 
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20); 
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15); 
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15); 
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15); 
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15); 
    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15); 
    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(25); 
    $objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(25); 
    $objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(25); 
    $objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(25); 

    $title_id = 3;
    $continue_val=-1;
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Sr No');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Business Title');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Business Name');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Business Address1');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Business Address2');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Business city');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Business State');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Business Phone');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Business Zipcode');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Business Fax');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Business email');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Business Website');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Business About');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Business Notes');
 
    $sr = 1;
    $i = 5;
    $static_sr_no=1;
    $start_val=-1;
    foreach ($geted_data as $geted_data) {
        //Get Communication records.
        $start_val=-1;
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val ,$i, $sr);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['bus_title']);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['bus_name']);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['bus_address1']);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['bus_address2']);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['bus_city']);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['Statename']);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['bus_phone']);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['bus_zip']);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['bus_fax']);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['bus_email']);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['bus_website']);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['bus_about']);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['bus_notes']);

        $i++;
        $sr++;
        $var_loop = 0;
    }
}
else if($table=="tbl_member_coupons")
{    
    $objPHPExcel->getActiveSheet()->getStyle('A3:E3')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);  
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20); 
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15); 
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20); 
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15); 

    $title_id = 3;
    $continue_val=-1;
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Sr No');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Coupons Title');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Coupons Percentage');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Coupons Description');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Coupons Code');
    
    $sr = 1;
    $i = 5;
    $static_sr_no=1;
    $start_val=-1;
    foreach ($geted_data as $geted_data) {
        //Get Communication records.
        $start_val=-1;
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val ,$i, $sr);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['cop_title']);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['cop_percentage']);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['cop_description']);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['cop_coupon_code']);
        
        $i++;
        $sr++;
        $var_loop = 0;
    }
}
else if($table=="tbl_member_messaging")
{    
    $objPHPExcel->getActiveSheet()->getStyle('A3:C3')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);  
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20); 
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);

    $title_id = 3;
    $continue_val=-1;
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Sr No');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Message');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Sent On');
    $sr = 1;
    $i = 5;
    $static_sr_no=1;
    $start_val=-1;
    foreach ($geted_data as $geted_data) {
        //Get Communication records.
        $start_val=-1;
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val ,$i, $sr);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['msg_message']);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, date("m/d/Y",strtotime($geted_data['msg_updated_date'])));
        $i++;
        $sr++;
        $var_loop = 0;
    }
}
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
// Redirect output to a client’s web browser (Excel5)
$nameexp = explode("_", $table);
if(isset($nameexp[2]))
{
    $last = "-".ucfirst($nameexp[2]);
}
else
{
    $last = "";
}
$name = ucfirst($nameexp[1]).$last;
//header('Content-Type: application/vnd.ms-excel');
//header('Content-Disposition: attachment;filename="'.$name.'_list.xls"');
//header('Cache-Control: max-age=0');
// header("Pragma: no-cache"); 
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save($name.'_list.xls');

//Download header

header('Content-Description: File Transfer');
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment; filename="'.$name.'_list.xls"');
header('Content-Transfer-Encoding: binary');
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Content-Length: ' . filesize($name.'_list.xls'));
ob_clean();
flush();
readfile($name.'_list.xls');
exit;
?>
