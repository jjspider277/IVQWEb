<?php 
$display_title = getMemberFileTitle($filename);	//For Display File Title
?>
<ul class="tab_links">
	<li ><div class="tab_link_active"><span><a href="<?php echo MEB_INDEX_PARAMETER.MEB_HOME;?>"><img src="<?php echo IMG_WWW; ?>directory-mem.png" /></a></span><h1><?php echo $display_title; ?></h1></div></li>
	
	<?php if($mebType!="Business") { ?>
	<li class="tab_link" ><a href="<?php echo MEB_INDEX_PARAMETER.MEB_VIEW_SUBSCRIBER?>" class="<?php if($filename == MEB_VIEW_SUBSCRIBER)echo "active";?>"><p class="sub"></p>Subscribers</a></li>
	<?php } ?>
	<li class="tab_link" ><a href="<?php echo MEB_INDEX_PARAMETER.MEB_VIEW_BUSINESS?>" class="<?php if($filename == MEB_VIEW_BUSINESS)echo "active";?>"><p class="bcard"></p>Business&nbsp;Cards</a></li>
	<?php if($mebType!="Business") { ?>
	<li class="tab_link" ><a href="<?php echo MEB_INDEX_PARAMETER.MEB_VIEW_SERVICES?>" class="<?php if($filename == MEB_VIEW_SERVICES)echo "active";?>"><p class="mservice"></p>Member&nbsp;Services</a></li>
	<?php } ?>
	<li class="tab_link" ><a href="<?php echo MEB_INDEX_PARAMETER.MEB_VIEW_MESSAGING?>" class="<?php if($filename == MEB_VIEW_MESSAGING)echo "active";?>"><p class="msg"></p>Messaging</a></li>
	<li class="tab_link" ><a href="<?php echo MEB_INDEX_PARAMETER.MEB_VIEW_COUPONS?>" class="<?php if($filename == MEB_VIEW_COUPONS)echo "active";?>"><p class="coupon"></p>Coupons</a></li>
	<!-- <li class="tab_link last" ><a href="<?php //echo MEB_INDEX_PARAMETER.MEB_VIEW_SURVEY?>#" class="<?php if($filename == MEB_VIEW_SURVEY)echo "active";?>"><p class="survey"></p>Survey</a></li> -->
</ul>