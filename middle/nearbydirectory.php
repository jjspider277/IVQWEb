<script type="text/javascript" src="<?php echo JS_FOLDER_WWW?>functions.js"></script>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places&sensor=false"></script>
<script type="text/javascript">
	$(document).ready(function() {
		//member_searck_key();
		up206b.initialize();
	});

	function onLoad() {
		document.addEventListener("deviceready", onDeviceReady, false);
	}
	// device APIs are available
	//
	function onDeviceReady() {
		// Now safe to use device APIs
		//getdirectorys('yes');
		//return false;
		//member_searck_key();
		up206b.initialize();
		//navigator.geolocation.getCurrentPosition(onSuccess, onError);
	}
	
	function onSuccess(position) {
		var element = document.getElementById('geolocation');
        element.innerHTML = 'Latitude: '           + position.coords.latitude              + '<br />' +
                            'Longitude: '          + position.coords.longitude             + '<br />' +
                            'Altitude: '           + position.coords.altitude              + '<br />' +
                            'Accuracy: '           + position.coords.accuracy              + '<br />' +
                            'Altitude Accuracy: '  + position.coords.altitudeAccuracy      + '<br />' +
                            'Heading: '            + position.coords.heading               + '<br />' +
                            'Speed: '              + position.coords.speed                 + '<br />' +
                            'Timestamp: '          + new Date(position.timestamp)          + '<br />';
    }
	
	function onError(error) {
        alert('code: '    + error.code    + '\n' +
              'message: ' + error.message + '\n');
    }

	function member_searck_key() {
		//getdirectorys('yes');
		$('#search').keyup(function(event) {
			$('#page_val').val('');
			getdirectorys('no');
		});
		$(document).on('click', '.ui-input-clear', function() {
			$('#page_val').val('');
			getdirectorys('yes');
		});
	}
</script>

<script type="text/javascript">
	//declare namespace
	var up206b = {};

	//declare globals
	var map;

	//trace function for debugging
	function trace(message) {
		if (typeof console != 'undefined') {
			console.log(message);
		}
	}

	//Function that gets run when the document loads
	up206b.initialize = function() {
 
		$('.ui-loader').show();
		if (navigator.geolocation) {
			geocoder = new google.maps.Geocoder();
			navigator.geolocation.getCurrentPosition(function (position) {
				var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
				//var latlng = new google.maps.LatLng('40.7141667', '-74.0063889');
				var myOptions = {
					zoom : 14,
					center : latlng,
					mapTypeId : google.maps.MapTypeId.ROADMAP
				};
				geocoder.geocode({'latLng': latlng}, function(results, status) {
					if (status == google.maps.GeocoderStatus.OK) {
				    	//console.log(results)
				      	if (results[1]) {
				        	//formatted address
				        	//alert(results[0].formatted_address)
				        	//find country name
				        	for (var i=0; i<results[0].address_components.length; i++) {
				            	for (var b=0;b<results[0].address_components[i].types.length;b++) {
				            		//alert(results[0].address_components[i].types[b]);
				            		//alert(results[0].address_components[i].long_name);
				            		if (results[0].address_components[i].types[b] == "administrative_area_level_2") {
				                    	//this is the object you are looking for
				                    	city= results[0].address_components[i].long_name;
				                    	break;
				                	}
				            	}
				        	}
				        }
				    }
        
					if(city != '') {
						$('.near_me_div').html(city);
					}
				});
				map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
				up206b.placesRequest('police', latlng, 1500, [ 'police' ], '');
			});
		}	
	}
	//Request places from Google
	up206b.placesRequest = function(title, latlng, radius, types, icon) {
		//Parameters for our places request
		var request = {
			location : latlng,
			radius : radius,
			types : types
		};

		//Make the service call to google
		var locations = new Array();
		var i=0;
		var callPlaces = new google.maps.places.PlacesService(map);
		callPlaces.search(request, function(results, status) {
			//trace(results);
			$.each(results, function(i, place) {
				var placeLoc = place.geometry.location;
				var vicinity = place.vicinity;
				vicinity = vicinity.split(', ');
				var j;
				for (j = 0; j < vicinity.length; ++j) {
					if(locations.indexOf('"'+vicinity[j]+'"') < 0) {
						locations.push(''+vicinity[j]+'');
					}
				}
				i++;
			})
			locationss = $.unique( locations );
   //alert(locationss);
			//console.log(locationss);
			var myVar2 = locationss.join(",");
   //alert(myVar2);
			//console.log(myVar2);
			//alert(locations);
			getNearby(myVar2);
		});
	}
</script>

<div class="directory-page sub-service">
<div class="buy-sal">Subscriber Services</div>
<div class="dir-pic-b">
<div class="s-d-title">Nearby Directories</div>
<div id="nearByArea"></div>
</div>
</div>

<div data-role="page" id="seaarch" class="jqm-demos" id="myPage" style="display:none;">	
		<div id="map_canvas" style="height: 0; margin-left: 0;"></div>
		<section class="home_c">
			<article id="content">
				<header class="details-page near_me_div" >Near me</header>
			</article>
		</section>
</div>