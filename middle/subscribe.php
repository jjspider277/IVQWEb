<!doctype html>
<?php 
include_once('include/includeclass.php');
//Directory Listing...
$dir_fields = array("dir_id","dir_name");
$dir_where  = "dir_status = 'Active'";
$dirRes 	= $db->selectData(TBL_DIRECTORY,$dir_fields,$dir_where,$extra="",2);

//State Listing...
$state_fields = array("sta_id","sta_name");
$state_where  = "sta_status = 'Active'";
$staRes 	= $db->selectData(TBL_STATE,$state_fields,$state_where,$extra="",2);

//print_r($mysubRes);
//Free Member Data
$sbfId = getFreeMemberSessionId();
// $mysub_fields = array("*");
// $mysub_where  = "sbf_status = 'Active' AND sbf_id='".$sbfId."'";
// $mysubRes 	= $db->selectData(TBL_SUBSCRIBER_FREE,$mysub_fields,$mysub_where,$extra="",2);
// $totmysubRes = count($mysubRes);
if($mysubRes[0]['sub_upgrade'] == "True")
{
 $URL = ROOT_WWW.FRT_INDEX_PARAMETER."directorysearch"; 
	redirect($URL);
	exit;
}
//Free Member Data

if(isset($_POST)){	
	$data = $_POST;	
}
$invDirId = $_REQUEST['dirId'];
if ($data['btn_subscribe'] == 'Subscribe')
{
	unset($add_values);
	$add_values['sub_dir_id'] 			= $invDirId;
	//$add_values['sub_type'] 			  = "Business";
 $add_values['sub_upgrade'] 		= "True";
	$add_values['sub_name'] 			  = $data['sub_name'];
	$add_values['sub_title'] 			 = $data['sub_title'];
	$add_values['sub_company'] 		= $data['sub_company'];	
	$add_values['sub_city'] 			  = $data['sub_city'];		
	$add_values['sub_phone'] 			 = $data['sub_phone'];
	$add_values['sub_zip'] 				  = $data['sub_zip'];
	$add_values['sub_sta_id'] 			= $data['sub_state'];	
	$add_values['sub_question'] 	= $data['sub_question'];
	$add_values['sub_answer'] 			= $data['sub_answer'];
 $SECTION_WHERE = "sub_id=".$sbfId;
	$insSubId = $db->updateData(TBL_MEMBER_SUBSCRIBERS, $add_values,$SECTION_WHERE); 
 
 if($insSubId > 0)
 {  
  unset($add_sbm_values);
		$add_sbm_values['sbm_sub_id'] 		    = $sbfId;
  $add_sbm_values['sbm_dir_id'] 		    = $data['sub_dir'];
  //$add_sbm_values['sbm_type']   		    = "Business";		
		$add_sbm_values['sbm_status'] 		    = "Active";
		$add_sbm_values['sbm_created_id'] 	 = 0;
		$add_sbm_values['sbm_created_date'] = date('Y-m-d H:i:s');  
		$inssbmId = $db->insertData(TBL_MEMBER_SUBSCRIBE_DIRECTORY, $add_sbm_values); 
 }

	// $subject = "B2b Mobile Message";
	// $message = subscriberHome();
	
	// $mail_flag=mailfunction($data['sub_email'],ADMIN_EMAIL,$subject,$message);
	// if($data['sub_type']=="Individual")
	// {
		// $_SESSION['msg'] = "Your subscription is successfully processed, you will start receiving coupons and messages in your e-mail inbox.";	
	// }
	// else
	// {
		// $_SESSION['msg'] = "Your subscription is successfully processed, ​ you can login and set up your business card and start sending messages and coupons.";
	// }	
    $URL = ROOT_WWW.FRT_INDEX_PARAMETER."subscriberservice&dirId=".$data['sub_dir']; 
    redirect($URL);
    exit;
}
?>

<script type="text/javascript" src="<?php echo AJAX_FOLDER_WWW; ?>getAjaxAdmin.js"></script>
<div class="m-m">
<div class="buy-sal">Subscription</div>
</div>
		<header>
			<ul class="tab_links">
				<li><div class="tab_link_active"><span><a href="#"><img src="images/directory-mem.png"></a></span><h1>Fill in the form below to Subscribe</h1></div></li>
			</ul>
			<div class="tab_content_holder">
				<div class="tab_content_holder_inner">
					<div class="border-txt">
					<div class="serviceTxt">Subscribe</div>
					<?php if(!empty($_SESSION['msg'])) {?>
						<div id="mesg" style="color: green;font-size: 20px;margin-top: -20px;padding: 10px 15px; max-width:900px;float:left;">
							<?php
							echo $_SESSION['msg'];
							$_SESSION['msg']="";
							?>
						</div>
						<script type="text/javascript">$("#mesg").delay(10000).fadeOut();</script>
					<?php } ?>
						<div class="subscriber_form_radio" style="display:none;">
						<div class="main-row">
							<div class="fl subscriber-type" >
								<label class="radio-label"  for="rad_Individual">
									<input type="radio" name="subscriber_flag" id="rad_Individual"  value="Individual" onclick="changeStatus(this.value);">Individual
								</label>
								<img src="images/questuin-mark.png" class="tooltip_ind" title="" >
                            </div>
                            <div class="fl subscriber-type" >
                                <label class="radio-label" for="rad_Directory">
									<input type="radio" name="subscriber_flag" id="rad_Directory" checked="checked" onclick="changeStatus(this.value);" value="Business">Business
								</label>
								<img src="images/questuin-mark.png" class="tooltip_bus" title="" >
								
                            </div>
						</div>
						</div>
						<div class="clr"></div>
						<div class="subscriber_form" >
                            <form name="business_subscriber" id="business_subscriber" method="post" action="<?php echo ROOT_WWW.FRT_INDEX_PARAMETER."subscribe"; ?>" autocomplete="off">
								<!--<div class="main-row">
									<label>Directory Name <?php echo getRequiredIcon()?></label>
									<select name="sub_dir" id="sub_dir" tabindex="1" alt="Type to search directory name">
										<option value="">Select Directory</option>
										<?php						
										for($i=0;$i<count($dirRes);$i++)
										{
											if($dirRes[$i]['dir_id']==$invDirId) { $select="selected='selected'"; } else { $select=""; }
										?>
											<option value="<?php echo $dirRes[$i]['dir_id']; ?>" <?php echo $select; ?> ><?php echo $dirRes[$i]['dir_name']; ?></option>
										<?php
										}
										?>
									</select>
								</div>-->
								<div class="main-row">
									<label>Subscriber Name <?php echo getRequiredIcon()?></label>
									<input type="text" name="sub_name" id="sub_name" tabindex="2" />
								</div>
								<div class="main-row dynamic">
									<label>Company Name <?php echo getRequiredIcon()?></label>
									<input class="disable" type="text" name="sub_company" id="sub_company" tabindex="3" />
								</div>
								<div class="main-row dynamic">
									<label>Title <?php echo getRequiredIcon()?></label>
									<input  class="disable" type="text" name="sub_title" id="sub_title" tabindex="4" />
								</div>								
								<div class="main-row">
									<label>City <?php echo getRequiredIcon()?></label>
									<input type="text" name="sub_city" id="sub_city" tabindex="9" />
								</div>
								<div class="main-row">
									<label>State <?php echo getRequiredIcon()?></label>
									<select name="sub_state" id="sub_state" tabindex="10" alt="Type to search state">
										<option value="">Select State</option>
										<?php
										for($i=0;$i<count($staRes);$i++)
										{
											if($staRes[$i]['sta_id']==$sub_state) { $select="selected='selected'"; } else { $select=""; }
										?>
											<option value="<?php echo $staRes[$i]['sta_id']; ?>" <?php echo $select; ?> ><?php echo $staRes[$i]['sta_name']; ?></option>
										<?php
										}
										?>
									</select>
								</div>
								<div class="main-row">
									<label>Zip Code <?php echo getRequiredIcon()?></label>
									<input type="text" name="sub_zip" id="sub_zip" tabindex="11" />
								</div>
								<div class="main-row">
									<label>Phone Number <?php echo getRequiredIcon()?></label>
									<input type="text" name="sub_phone" id="sub_phone" value="<?php echo $mysubRes[0]['sbf_mobile'];?>" tabindex="12" />
								</div>
								<div class="main-row dynamic">
									<label>Secret Question <?php echo getRequiredIcon()?></label>
									<input class="disable" type="text" name="sub_question" id="sub_question" tabindex="13"/>
								</div>
								<div class="main-row dynamic">
									<label>Answer <?php echo getRequiredIcon()?></label>
									<input class="disable" type="text" name="sub_answer" id="sub_answer" tabindex="14"/>
								</div>
								
								<div class="main-row terms">
									<label>&nbsp;</label>
									<label for="agree" class="checkbox"><input type="checkbox" name="agree" id="agree" tabindex="15" value="Agree" />
									<a for="agree" href="#termsAndCondi" class="fancybox">Terms And Conditions</a></label>
									<!-- <input type="hidden" name="agree" id="agree" value="" /> -->
								</div>
								<div class="main-row btn_business_row">
									<input type="hidden" name="sub_dir" id="sub_dir" value="<?php echo $invDirId; ?>">
									<input type="hidden" value="Business" name="sub_type" id="sub_type" />
									<input type="submit" tabindex="16" value="Subscribe" name="btn_subscribe" id="btn_subscribe" />
								</div>
							</form>
							<div style="clear:both;"></div>
							</div>
                            </div>
						</div>
						<div style="clear:both;"></div>
					</div>
					
				</div>
			</div>
		</header>
		<div class="clr"></div>
<div id="termsAndCondi" style="display:none;">
	<div class="fullview_clients">
		<div class="subRow">
			 <div class="enterTitle main">
			 	  <h1>Terms And Conditions</h1><!-- <a href="javascript:void(0);" class="fancybox-item fancybox-close" title="Close"> --></a>
			</div>
		</div>
		<div class="subRow">
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
		</div>
		<div class="terms-btns"><input type="button" name="agreefancy" id="agreefancy" value="I Agree" />
		<input type="button" name="dontagreefancy" id="dontagreefancy" value="I Don't Agree" />
	</div>
	</div>
</div>
<script type="text/javascript">
$(document.body).on('click', "#btn_subscribe", function(e){
	if($("#userEmailmsg label").text().length > 5)
	{
		return false;		
	}
	else
	{
		return true;
	}
});
$(document.body).on('click', "#agreefancy", function(e){
	if(!$(".terms #agree").is(":checked"))
	{
		$('.terms #agree').prop({"checked":true});
		$('.terms .checkbox label').html('');
	}
	$.fancybox.close();
});
$(document.body).on('click', "#dontagreefancy", function(e){
	$('.terms #agree').prop({"checked":false});
	$.fancybox.close();
});
$(document).ready(function(){
	$("#sub_email").focus(function() {
		$("#userEmailmsg label").html("");
	});
	$(".dynamic").show();
	$(".disable").attr({"disabled": false});
	
	$.validator.addMethod("phoneValidate", function(value, element) {
    	return this.optional(element) || /^[0-9\-\+\,\.\)\(\s]+$/i.test(value);
	}, "Only numbers,-,+ and , allowed");
	
	$("#business_subscriber").validate({
		rules: {
			sub_dir : {
				required : true
			},
			sub_name : {
				required : true
			},
			sub_company : {
				required : true
			},
			sub_title : {
				required : true
			},			
			sub_phone : {
				required : true,
				phoneValidate : true
			},
			sub_zip : {
				required : true,
				phoneValidate : true
			},
			sub_city : {
				required : true
			},
			sub_state : {
				required : true
			},
			sub_question : {
				required : true
			},
			sub_answer : {
				required : true
			},
			agree : {
				required : true
			}
		},
		messages: {
			sub_dir : {
				required : "Please select directory name"
			},
			sub_name : {
				required : "Please enter name"
			},
			sub_company : {
				required : "Please enter company name"
			},
			sub_title : {
				required : "Please enter title"
			},		
			sub_phone : {
				required : "Please enter phone no.",
				phoneValidate : "Please enter valid phone no."
			},
			sub_zip : {
				required : "Please enter zip code",
				phoneValidate : "Please enter valid zip code"
			},
			sub_city : {
				required : "Please enter city"
			},
			sub_state : {
				required : "Please select state"
			},
			sub_question : {
				required : "Please enter question"
			},
			sub_answer : {
				required : "Please enter answer"
			},
			agree : {
				required : "Please check terms and conditions"
			}
		}
	});
	$('.tooltip_ind').tooltipster({position : "right",content: $('<strong>Individual subscribers get the following benefits:</strong><ul><li>​Search for directory and business services</li><li>Subscribe to your favorite directories</li><li>Receive special offers and coupons</li><li>Share business cards with colleagues and friends</li><li>Use location base mobile application​</li></ul>')});
	$('.tooltip_bus').tooltipster({position : "right",content: $('<strong>Business subscribers get the following benefits:</strong><ul><li>Search for directory and business services</li><li>Create and share business cards</li><li>Send special offers and coupons to subscribers</li><li>Create custom media for business card and offers</li></ul>')});
});
function changeStatus(status)
{
	if(status=="Individual")
	{
		$(".dynamic").hide();
		$(".disable").attr({"disabled": true});
		$("#sub_type").val('Individual');
		$("label.error").html("");
		$('input:text').val('');
	}
	else
	{
		$(".dynamic").show();
		$(".disable").attr({"disabled": false});
		$("#sub_type").val('Business');
		$("label.error").html("");
		$('input:text').val('');
	}
}

</script>
<style type="text/css">
.checkbox label.error{
	position: absolute;
	margin-top: 20px;
}
</style>

