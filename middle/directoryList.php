<?php 
if ($_POST['home_search'] == 'Search')
{
	$dirHomeId = $_POST["home_dir"];
	if($dirHomeId !="")
	{
		$sqlDirQuery = "SELECT *,(select sta_name from tbl_state where sta_id=dir_sta_id) as dir_state FROM ".TBL_DIRECTORY." WHERE (dir_name LIKE '%".mysql_real_escape_string($dirHomeId)."%' OR dir_city LIKE '".mysql_real_escape_string($dirHomeId)."%' OR dir_zip LIKE '%".mysql_real_escape_string($dirHomeId)."%' OR dir_sta_id IN (SELECT sta_id FROM tbl_state WHERE sta_name LIKE '%".mysql_real_escape_string($dirHomeId)."%')) AND dir_status != 'Deleted' order by dir_id desc";
		$DirResult  = $db->select($sqlDirQuery);
		$totDir = count($DirResult);
	}
}
?>
<aside style="margin-top:20px;">
	<div class="tab_content_holder radius search-page">
		<div class="tab_content_holder_inner">
			<div class="listing_banner"><img src="images/list-banner.png" /></div>
			<div class="search_listing">
				<?php
					if($DirResult > 0)
					{
				?>
				<ul>
					<?php
						$j=1;
						for($i=0;$i<$totDir;$i++)
						{
					?>
					<li <?php if($i == $j){ echo 'class="light"'; $j = $j + 2;} ?>>
						<h1>
							<a href="<?php echo ROOT_WWW.FRT_INDEX_PARAMETER."allInformation&dirId=".$DirResult[$i]['dir_id']; ?>" ><?php echo $DirResult[$i]['dir_name']; ?></a>
						</h1>
						<span class="list_url">
							<?php echo $DirResult[$i]['dir_website']; ?>
						</span>
						<p>
							<span>Address:</span> <?php echo $DirResult[$i]['dir_address']; ?> 
							<span>City:</span> <?php echo $DirResult[$i]['dir_city']; ?>
							<span>State:</span> <?php echo $DirResult[$i]['dir_state']; ?> 
							<span>Zip Code:</span> <?php echo $DirResult[$i]['dir_zip']; ?> 
							<span>Phone:</span> <?php echo $DirResult[$i]['dir_office_phone']; ?> 
							<span>Email:</span> <?php echo $DirResult[$i]['dir_email']; ?>
						</p>
					</li>
					<?php } ?>
				</ul>
				<?php
				}
				else
				{
				?>
				<div class="no-record">Directory not available</div>
				<?php
				}
				?>
			</div>
		</div>
	</div>               				   
</aside>