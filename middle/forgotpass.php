<?php
$error = "";
$id = $_REQUEST["id"];
if($id!="")
{
    $sub_fields = array("sub_id","sub_email","sub_question","sub_answer");
    $sub_where  = "sub_status = 'Active' and sub_type='Business' and sub_id=".$id;
    $subRes     = $db->selectData(TBL_MEMBER_SUBSCRIBERS,$sub_fields,$sub_where,$extra="",2);

    $subEmail    = $subRes[0]["sub_email"];
    $subQuestion = $subRes[0]["sub_question"];
}
if($_POST["frm_forgotSub"]=="Change Password")
{
    $sub_email      = $subEmail;
    $sub_question   = $subQuestion;
    $sub_answer     = $_POST["sub_answer"];
    $sub_password   = $_POST["sub_password"];
    
    $sub_que_fields = array("sub_id");
    $sub_que_where  = "sub_email='".$sub_email."' and sub_question='".$sub_question."' and sub_answer='".$sub_answer."'";
    $subQueRes     = $db->selectData(TBL_MEMBER_SUBSCRIBERS,$sub_que_fields,$sub_que_where,$extra="",2);
    if($subQueRes!="")
    {
        $sub_id = $subQueRes[0]["sub_id"];
        unset($update_values);
        $update_values["sub_password"]      = md5($sub_password);
        $update_values["sub_updated_id"]    = 0;
        $update_values["sub_updated_date"]  = date('Y-m-d H:i:s');
        $updId = $db->updateData(TBL_MEMBER_SUBSCRIBERS, $update_values,"sub_id=".$sub_id);

        $mail_flag=mailfunction($to,ADMIN_EMAIL,$subject,$message);
        $_SESSION["succMsg"] = "Your password has been changed succesufully...";
        
        redirect(ROOT_WWW.FRT_INDEX_PARAMETER.'forgotpass&id='.$sub_id);
        exit;
    }
    else
    {
        $error = "Please enter correct answer...";  
    }
}
?>
<aside class="business-box" style="margin-top:20px;">
    <header>
        <ul class="tab_links">
            <li><div class="tab_link_active"><span><a href="#"><img src="images/directory-mem.png"></a></span><h1>Forgot Password</h1></div></li>
        </ul>
        <div class="tab_content_holder">
            <div class="tab_content_holder_inner">
                <div class="border-txt">
                    <div class="serviceTxt">Forgot Password</div>
                    <?php if(!empty($error)){?>
                        <div id="mesg" style="color:#FF0000;font-size:20px;margin-top: -20px;padding:10px 15px;width:100%;float:left;">
                            <?php
                            echo $error;
                            $error="";
                            ?>
                        </div>
                        <script type="text/javascript">$("#mesg").delay(10000).fadeOut();</script>
                        <div class="clr"></div>
                    <?php } ?>
                    <?php if(!empty($_SESSION["succMsg"])){?>
                        <div id="mesgSucc" style="color:green;font-size:20px;margin-top: -20px;padding:10px 15px;width:100%;float:left;">
                            <?php
                            echo $_SESSION["succMsg"];
                            unset($_SESSION["succMsg"]);
                            ?>
                        </div>
                        <script type="text/javascript">$("#mesgSucc").delay(10000).fadeOut();</script>
                        <div class="clr"></div>
                    <?php } ?>
                    <div class="subscriber_form">     
                        <form name="frm_forgotPass" autocomplete="off" id="frm_forgotPass" action="<?php echo ROOT_WWW.FRT_INDEX_PARAMETER.'forgotpass&id='.$id; ?>" method="post">
                            <div class="main-row">
                                <label>Email</label>
                                <input type="text" disabled="disabled" name="sub_email" id="sub_email" tabindex="1" value="<?php echo $subEmail; ?>" />
                            </div>
                            <div class="main-row">
                                <label>Your Question</label>
                                <input disabled="disabled" type="text" name="sub_question" id="sub_question" tabindex="2" value="<?php echo $subQuestion; ?>" />
                            </div>
                            <div class="main-row">
                                <label>Answer <?php echo getRequiredIcon()?></label>
                                <input type="text" name="sub_answer" id="sub_answer" tabindex="3" />
                            </div>
                            <div class="main-row">
                                <label>New Password <?php echo getRequiredIcon()?></label>
                                <input type="password" name="sub_password" id="sub_password" tabindex="4" />
                            </div>
                            <div class="main-row">
                                <label>Confirm Password <?php echo getRequiredIcon()?></label>
                                <input type="password" name="sub_conf_pass" id="sub_conf_pass" tabindex="5" />
                            </div>
                            <div class="main-row">
                                <label>&nbsp;</label>
                                <input type="submit" name="frm_forgotSub" id="frm_forgotSub" value="Change Password" tabindex="6" />
                            </div>
                        </form>
                    </div>
                    <div class="clr"></div>
                </div>
            </div>
        </div>
    </header>
</aside>
<script type="text/javascript">
$(document).ready(function(){
    $("#frm_forgotPass").validate({
        rules: {
            sub_answer:{
                required :true
            },
            sub_password:{
                required :true
            },
            sub_conf_pass:{
                required :true,
                equalTo :"#sub_password"
            }
        },
        messages: {
            sub_answer:{
                required :"Please enter answer"
            },
            sub_password:{
                required :"Please enter password"
            },
            sub_conf_pass:{
                required :"Please enter password",
                equalTo: "Please enter same password"
            }
        }
    });
});
</script>