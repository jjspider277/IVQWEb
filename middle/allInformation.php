<?php 

if ($_REQUEST['dirId'] != '')
{
	$dirHomeId = $_REQUEST['dirId'];
	if($dirHomeId!="")
	{
		//select Directory records
		$sqlDirQuery = "SELECT * FROM ".TBL_DIRECTORY." WHERE dir_id=".$dirHomeId." AND dir_status != 'Deleted' order by dir_id desc";
		$DirResult  = $db->select($sqlDirQuery);
		$totDir = count($DirResult);

		//select member as directory wise
		$meb_fields = array("meb_id");
		$meb_where  = "meb_dir_id= ".$dirHomeId." AND meb_status != 'Deleted'";
		$mebRes 	= $db->selectData(TBL_MEMBER,$meb_fields,$meb_where,$extra="",2);
        
		for($i=0;$i<count($mebRes);$i++)
		{
			if($i==count($mebRes)-1)
			{
				$addComma = "";
			}
			else
			{
				$addComma = ",";
			}
			$allMeb .= $mebRes[$i]["meb_id"].$addComma;
		}
		if($mebRes > 0)
		{
			//select business Records
			/*$sqlBusQuery = "SELECT * FROM ".TBL_MEMBER_BUSINESS." WHERE bus_id!= 0 AND bus_meb_id IN (".$allMeb.") AND bus_status != 'Deleted' order by bus_title";
			$BusResult  = $db->select($sqlBusQuery);
			$totBus = count($BusResult);*/

			//select Services records
			$sqlSerQuery = "SELECT * FROM ".TBL_MEMBER_SERVICES." WHERE ser_id!= 0 AND ser_meb_id IN (".$allMeb.") AND ser_status != 'Deleted' order by ser_id desc";
			$SerResult  = $db->select($sqlSerQuery);
			$totSer = count($SerResult);
		}
	}
}
?>
<aside style="margin-top:20px;">
	<div class="tab_content_holder radius">
		<div class="tab_content_holder_inner detail_page banner-wrp">
            <div class="listing_banner"><img src="images/detail-banner-new.png">
                <div class="baner-contet">
                    <h1><?php
                        $dir_fields = array("dir_name");
                        $dir_where  = "dir_id= ".$dirHomeId." AND dir_status != 'Deleted'";
                        $dirRes     = $db->selectData(TBL_DIRECTORY,$dir_fields,$dir_where,$extra="",2);
                        echo $dirRes[0]['dir_name'];
                    ?></h1>
                </div>
            </div>
		</div>
	</div>
</aside>

<aside class="business-box"  style="margin-top:20px;">
	<div class="tab_content_holder radius">
		<div class="tab_content_holder_inner">
    		<div class="border-txt bottom">
                <div class="sub-new changedbtn">
                <a href="<?php echo ROOT_WWW.FRT_INDEX_PARAMETER.'subscriber_home&dirId='.$dirHomeId; ?>" title="Subscribe" >Subscribe</a>
                </div>
                <div class="serviceTxt">Business Card</div>
                <div id="businessCards"></div>
      </div>
  </div>
 </div>
</aside>


<script type="text/javascript">

</script>
<aside class="business-box" style="margin-top:20px;">
    <div class="tab_content_holder radius">
        <div class="tab_content_holder_inner">
            <div class="border-txt bottom">
                <div class="serviceTxt">Services</div>
                <?php
                if($totSer>0)
                {
                ?>
                <div class="subRow direct card service_box flexslider">
                    <ul class="slides">
                <?php
                    $b=1;
                    for($k=0;$k<$totSer;$k++)
                    {
                ?>
                    <li>
                        <div class="enterTitle light">
                            <b>Title</b><span>:</span>
                            <p><?php echo $SerResult[$k]['ser_title']; ?></p>
                        </div>
                        <div class="enterTitle dark">
                            <b>Description</b><span>:</span>
                            <p><?php echo $SerResult[$k]['ser_description']; ?></p>
                        </div>
                    </li>
                
                <?php
                    }
                ?>
                    </ul>
                </div>
                <?php
                }
                else
                {
                ?>
                    <div class="no-record">No Records</div>
                <?php
                }
                ?>
            </div>
        </div>
    </div>
</aside>

<script type="text/javascript" src="<?php echo JS_FOLDER_WWW?>jquery.flexslider.js"></script>
<script type="text/javascript" src="<?php echo JS_FOLDER_WWW?>modernizr.js"></script>
<script type="text/javascript">
$( document ).ready( function(){
    $('.flexslider').flexslider({
        animation: 'slide',
        easing: 'linear',  
        pauseOnAction:false,
        smoothHeight :false,
        pauseOnHover:true,
        controlNav:false,
        directionNav:true,
        useCSS: false,        
        slideshow: false,    
        start: function(slider){
            $('body').removeClass('loading'); 
        }
    });
});
</script>

<script type="text/javascript" src="<?php echo JS_FOLDER_WWW?>functions.js"></script>
<script type="text/javascript" src="<?php echo JS_FOLDER_WWW?>jquery-1.3.2.js"></script>
<script type="text/javascript" src="<?php echo JS_FOLDER_WWW?>jquery-ui-1.7.2.custom.js">
</script>
<script type="text/javascript" src="<?php echo JS_FOLDER_WWW?>jquery.jcoverflip.js"></script>
<script type="text/javascript">
var jQuery = $.noConflict(true);
$( document ).ready( function(){
    getbusinesscards('<?php echo $dirHomeId; ?>','');
});
</script>
