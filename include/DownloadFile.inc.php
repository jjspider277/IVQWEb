<?php
/*********************************************************************************************/
/*			Note :- How to use of this class file               							 */
/*********************************************************************************************/
/*	1. This code include in one blank PHP file.                 							 */
/* 	2. Don't include any PHP/Javascript etc. file in this file.								 */
/* 								(write follwoing code)										 */
/*  1. include("../../inc/DownloadFile.inc.php");//include download file class(means this class)*/
/*  2. $download = new download_file();//create object of download file class
/*	3. $path = ROOT_DIR_PATH".$_GET['filename'];//document dir. path of file and get file name from 		/*     url																					 */
/*	4. $download->downloadFile($path);//call download file function of download_file class
	exit;
*/
class download_file
{
	function downloadFile($path)
	{
		$filename = $path;

		//required for IE, otherwise Content-disposition is ignored
		if(ini_get('zlib.output_compression'))
  		ini_set('zlib.output_compression', 'Off');

		// addition by Jorg Weske
		$file_extension = strtolower(substr(strrchr($filename,"."),1));

		if( $filename == "" ) 
		{
			  echo "<html><title></title><body>ERROR: download file NOT SPECIFIED.</body></html>";
  				exit;
		} elseif ( ! file_exists( $filename ) ) 
		{
			  echo "<html><title></title><body>ERROR: File not found.</body></html>";
			  exit;
		};
		switch( $file_extension )
		{
			case "txt": 
				$ctype="application/txt"; 
				break;
	
			case "pdf": 
				$ctype="application/pdf"; 
				break;

			case "exe": 
				$ctype="application/octet-stream"; 
				break;

  			case "zip": 
				$ctype="application/zip"; 
				break;

  			case "doc": 
				$ctype="application/msword"; 
				break;

  			case "xls": 
				$ctype="application/vnd.ms-excel"; 
				break;

  			case "ppt": 
				$ctype="application/vnd.ms-powerpoint"; 
				break;

  			case "gif": 
				$ctype="image/gif"; 
				break;

  			case "png": 
				$ctype="image/png"; 
				break;

  			case "jpeg":

  			case "jpg": 
				$ctype="image/jpg"; 
				break;

  			default: 
				$ctype="application/force-download";
		}

		header("Pragma: public"); // required
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private",true); // required for certain browsers 
		header("Content-Type: ".$ctype);
		header('Content-Disposition: inline; filename="' .$filename. '"'); 
		header('Content-Type: application/octet-stream'); 
		header("Content-Disposition: attachment; filename=\"".basename($filename)."\";" );
		header("Content-Length: ".filesize($filename));
		return readfile($filename);
	}
}
?>