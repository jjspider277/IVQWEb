<?php
    ################### Admin File Varible #########################
	define('ADM_INDEX','index.php');
	define('ADM_INDEX_PARAMETER','index.php?files=');
	
	define('ADM_LOGIN','login.php');
	define('ADM_LOGOUT','logout');
	define('ADM_FORGOT','forgot.php');
	define('ADM_HEADER','header.php');
	define('ADM_LEFT','left.php');
	define('ADM_FOOTER','footer.php');
	
	define('ADM_HOME','home');
	
	define('ADM_MANAGE_FRONT_HOME','manage_front_home');
	define('ADM_VIEW_FRONT_HOME','view_front_home');
	
	define('ADM_VIEW_APP_SETTING','app_setting');
	define('ADM_VIEW_GALLERY','gallery');

	define('ADM_VIEW_DIRECTORY','view_directory');
	define('ADM_MANAGE_DIRECTORY','manage_directory');
	
	define('ADM_MANAGE_MEMBERS','manage_members');
	define('ADM_VIEW_MEMBERS','view_members');

	define('ADM_MANAGE_ADVERTISE','manage_advertise');
	define('ADM_VIEW_ADVERTISE','view_advertise');

	define('ADM_MANAGE_BUSINESS','manage_business');
	define('ADM_VIEW_BUSINESS','view_business');

	define('ADM_MANAGE_INDIVIDUAL','manage_individual');
	define('ADM_VIEW_INDIVIDUAL','view_individual');		
	
####################################################################

#################### Member File Varible ###########################

	define('MEB_INDEX','index.php');
	define('MEB_INDEX_PARAMETER','index.php?files=');
	define('MEB_LOGIN','login');
	define('MEB_REGISTER','register');
	define('MEB_FORGOT','forgot');
	define('MEB_FRONT','front');
	define('MEB_LOGOUT','logout');
	define('MEB_PROFILE','profile');
    define('MEB_CREATE_GROUP_1','create_group_1');
    define('MEB_HOME','home');
    define('MEB_TERMS','terms');

    define('MEB_MANAGE_BUSINESS','manage_member');
    define('MEB_VIEW_BUSINESS','view_member');

	
	define('MEB_MANAGE_MEMBERS','manage_members');
	define('MEB_VIEW_MEMBERS','view_members');
	
	define('MEB_MANAGE_SUBSCRIBER','manage_subscriber');
	define('MEB_VIEW_SUBSCRIBER','view_subscriber');

	define('MEB_MANAGE_SERVICES','manage_services');
	define('MEB_VIEW_SERVICES','view_services');
	
	define('MEB_MANAGE_MESSAGING','manage_messaging');
	define('MEB_VIEW_MESSAGING','view_messaging');
	
	define('MEB_MANAGE_COUPONS','manage_coupons');
	define('MEB_VIEW_COUPONS','view_coupons');

	define('MEB_MANAGE_BUSINESS_SUB','manage_business_sub');
	define('MEB_VIEW_BUSINESS_SUB','view_business_sub');
	
	define('MEB_MANAGE_SURVEY','manage_survey');
	define('MEB_VIEW_SURVEY','view_survey');

    define('MEB_MANAGE_REQUESTS','view_requests');
    define('MEB_VIEW_REQUESTS','view_requests');

    define('MEB_OPPORTUNITIES','opportunities');
    define('MEB_INVESTOR_NEWS','investor_news');
    define('MEB_ABOUT_US','about_us');
    define('MEB_CONTACT_US','contact_us');
    define('MEB_PRIVACY_POLICY','privacy_policy');

    define('MEB_VIEW_RESPONSE','view_response');
###################################################################
?>
