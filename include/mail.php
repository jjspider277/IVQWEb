<?php
require_once("class.phpmailer.php"); 
function mailfunctionnew($fromemail,$toemail,$subject,$message,$custom_host_name='',$custom_user_name='',$custom_password='',$custom_port_name='',$filepath)
{		
			$mail = new PHPMailer(false);
			$mail->MailerDebug = false;
			$mail->SMTPDebug = false;

			//If send email using SMTP...
			if ($custom_host_name!="" && $custom_port_name!="") 
			{
				$mail->IsSMTP();
				$mail->Host = $custom_host_name;
				$mail->SMTPAuth = true;
				$mail->Username = $custom_user_name;
				$mail->Password = $custom_password;
				$mail->Port = $custom_port_name;
			}
		

			$from_name = "mail@iflair.com"; // Show default name of the sender
			$mail->From = $fromemail; // Sender Email
			$mail->FromName = $fromemail; // Define name of the sender -- 
			$mail->Subject = $subject;
			$mail->CharSet = 'utf-8';
			$mail->MsgHTML(stripslashes($message));
			if($filepath != "")
			{
				$mail->AddAttachment($filepath);
			}
			$abc = 1;
			foreach($toemail as $key => $email_to)
			{	
				$mail->AddAddress($email_to);				
				$mail->Send();					
				//$abc .= $email_to;
				$mail->ClearAddresses();
			}
										
}

function mail_attachment($filename, $path, $mailto, $from_mail, $from_name, $replyto, $subject, $message) {
    $file = $path.$filename;
    $file_size = filesize($file);
    $handle = fopen($file, "r");
    $content = fread($handle, $file_size);
    fclose($handle);
    $content = chunk_split(base64_encode($content));
    $uid = md5(uniqid(time()));
    $name = basename($file);
    $header = "From: ".$from_name." <".$from_mail.">\r\n";
    $header .= "Reply-To: ".$replyto."\r\n";
    $header .= "MIME-Version: 1.0\r\n";
    $header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
    $header .= "This is a multi-part message in MIME format.\r\n";
    $header .= "--".$uid."\r\n";
    $header .= "Content-type:text/plain; charset=iso-8859-1\r\n";
    $header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
    $header .= $message."\r\n\r\n";
    $header .= "--".$uid."\r\n";
    $header .= "Content-Type: application/octet-stream; name=\"".$filename."\"\r\n"; // use different content types here
    $header .= "Content-Transfer-Encoding: base64\r\n";
    $header .= "Content-Disposition: attachment; filename=\"".$filename."\"\r\n\r\n";
    $header .= $content."\r\n\r\n";
    $header .= "--".$uid."--";
    if (mail($mailto, $subject, "", $header)) {
        echo "mail send ... OK"; // or use booleans here
    } else {
        echo "mail send ... ERROR!";
    }
}
?>