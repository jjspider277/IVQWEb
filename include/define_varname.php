<?php
################## Front Side Varible ##############################

define("FRT_FOLDER_DIR",ROOT_DIR);
//define("ROOT_WWW",ROOT_WWW);
define('FRT_HOME','home');
define('FRT_HEADER','header.php');
define('FRT_INDEX','index.php');
define('FRT_INDEX_PARAMETER','index.php?files=');

define("FRT_MIDD_DIR", ROOT_DIR."middle/");
define("FRT_MIDD_WWW", ROOT_WWW."middle/");

define("FRT_SECTION_DIR", ROOT_DIR."section/");
define("FRT_SECTION_WWW", ROOT_WWW."section/");

###################################################################

define("ADM_TITLE","Nccsolutions");
define("ADM_FAVICON","images/favicon.ico");
define("ADM_DEFAULT_PAGE_TITLE", "Administrator");

define("ADM_DEFAULT_PREFIX", "");


define("ADM_FOLDER",ROOT_DIR."admin/");
define("ADM_FOLDER_DIR",ROOT_DIR."admin/");
define("ADM_FOLDER_WWW",ROOT_WWW."admin/");


define("ADM_MIDD_DIR", ADM_FOLDER_DIR."middle/");
define("ADM_MIDD_WWW", ADM_FOLDER_WWW."middle/");


define("ADM_IMAGES_DIR", ADM_FOLDER_DIR."images/");
define("ADM_IMAGES_WWW", ADM_FOLDER_WWW."images/");

define("ADM_CSS_DIR", ADM_FOLDER_DIR."css/");
define("ADM_CSS_WWW", ADM_FOLDER_WWW."css/");
########################################################################

#################### Javascript Folder Varible ########################
define("JAVASCRIPT_FOLDER",ROOT_WWW."javascript/");
define("JAVASCRIPT_FOLDER_ROOT",ADM_FOLDER."javascript/");

define("JAVASCRIPT_FOLDER_DIR",ROOT_DIR."javascript/");
define("JAVASCRIPT_FOLDER_WWW",ROOT_WWW."javascript/");

define("JS_FOLDER_WWW",JAVASCRIPT_FOLDER_WWW."js/");

define("AJAX_FOLDER_DIR",JAVASCRIPT_FOLDER_DIR."ajax/");
define("AJAX_FOLDER_WWW",JAVASCRIPT_FOLDER_WWW."ajax/");

define("AJAX_FOLDER",JAVASCRIPT_FOLDER."ajax/");

##################################################################

define("IMG_DIR",ROOT_DIR."images/");
define("IMG_WWW",ROOT_WWW."images/");

#############################Upload FIles Folder ########################
define("UPLOAD_DIR",ROOT_DIR."upload_files/");
define("UPLOAD_WWW",ROOT_WWW."upload_files/");

#########################Upload Business Cards #########################
define("UPLOAD_DIR_BUSINESS_FILE",ROOT_DIR."upload_files/Business/");
define("UPLOAD_WWW_BUSINESS_FILE",ROOT_WWW."upload_files/Business/");

#########################Upload Directory Cards #########################
define("UPLOAD_DIR_DIRECTORY_FILE",ROOT_DIR."upload_files/Directory/");
define("UPLOAD_WWW_DIRECTORY_FILE",ROOT_WWW."upload_files/Directory/");

#########################Upload Service #########################
define("UPLOAD_DIR_SERVICE_FILE",ROOT_DIR."upload_files/Service/");
define("UPLOAD_WWW_SERVICE_FILE",ROOT_WWW."upload_files/Service/");

#########################Upload Messaging File #########################
define("UPLOAD_DIR_MESSAGING_FILE",ROOT_DIR."upload_files/Messaging/");
define("UPLOAD_WWW_MESSAGING_FILE",ROOT_WWW."upload_files/Messaging/");


#########################Upload Subscriber File #########################
define("UPLOAD_DIR_SUBSCRIBER_FILE",ROOT_DIR."upload_files/Subscriber/");
define("UPLOAD_WWW_SUBSCRIBER_FILE",ROOT_WWW."upload_files/Subscriber/");


#########################Upload Advertisememt File #########################
define("UPLOAD_DIR_ADVERTISE_FILE",ROOT_DIR."upload_files/Advertisememt/");
define("UPLOAD_WWW_ADVERTISE_FILE",ROOT_WWW."upload_files/Advertisememt/");

#########################Upload Banner File #########################
define("UPLOAD_DIR_BANNER_FILE",ROOT_DIR."upload_files/Banner/");
define("UPLOAD_WWW_BANNER_FILE",ROOT_WWW."upload_files/Banner/");

#########################Upload Coupons File #########################
define("UPLOAD_DIR_COUPONS_FILE",ROOT_DIR."upload_files/Coupons/");
define("UPLOAD_WWW_COUPONS_FILE",ROOT_WWW."upload_files/Coupons/");


###########################################################################


################  Member Folder Varible #########################

define("MEB_FOLDER",ROOT_DIR."members/");
define("MEB_FOLDER_DIR",ROOT_DIR."members/");
define("MEB_FOLDER_WWW",ROOT_WWW."members/");

define("MEB_SECTION_DIR", MEB_FOLDER_DIR."section/");
define("MEB_SECTION_WWW", MEB_FOLDER_WWW."section/");


define("MEB_MIDD_FOLD_WWW", MEB_FOLDER_DIR."middle/");
define("MEB_MIDD_DIR", MEB_FOLDER_DIR."middle/");
define("MEB_MIDD_WWW", MEB_FOLDER_WWW."middle/");
define("MEB_MIDD_FCK_DISPLAY", MEB_FOLDER_WWW."middle/");

define("MEB_CSS_DIR", MEB_FOLDER_DIR."css/");
define("MEB_CSS_WWW", MEB_FOLDER_WWW."css/");

//default group id
define("DEFAULT_GROUP_ID", "167");

##########################################################################

?>
