<?php
error_reporting(E_ALL);
ini_set('max_execution_time',300);
ini_set('max_input_time',300);
ini_set('memory_limit', '300M');
session_start();
$imagew = $_REQUEST['w'];
$imageh = $_REQUEST['h'];




$dir = $_REQUEST['dir']; 
$uploaddir = $dir."/";
$uploadedfile=$_FILES['uploadfile']['tmp_name'];
$getExt = explode ('.', $_FILES['uploadfile']['name']);
$file_type = $getExt[count($getExt)-1];

$return_name = preg_replace("/[^A-Za-z0-9.\-]/","-",$_FILES['uploadfile']['name']);

$file = $uploaddir ."web_".preg_replace("/[^A-Za-z0-9.\-]/","-",$_FILES['uploadfile']['name']); 
$file1 = $uploaddir ."this-is-myweb_".preg_replace("/[^A-Za-z0-9.\-]/","-",$_FILES['uploadfile']['name']); 
$file_name= "web_".preg_replace("/[^A-Za-z0-9.\-]/","-",$_FILES['uploadfile']['name']); 
$file_path = $dir."/".$file_name;
$file_path1 = $dir."/25-".$file_name;
$file_path2 = $dir."/50-".$file_name;
$file_path3 = $dir."/75-".$file_name;
$file_path4 = $dir."/100-".$file_name;

class SimpleImage{
     var $image;
     var $image_type;

     function load($filename) {
        $image_info = getimagesize($filename);
        $this->image_type = $image_info[2];
        if( $this->image_type == IMAGETYPE_JPEG ) {
            $this->image = imagecreatefromjpeg($filename);
        } elseif( $this->image_type == IMAGETYPE_GIF ) {
            $this->image = imagecreatefromgif($filename);
        } elseif( $this->image_type == IMAGETYPE_PNG ) {
            $this->image = imagecreatefrompng($filename);
        }
    }
function save($filename, $image_type=IMAGETYPE_JPEG, $compression=75, $permissions=null) {

   // do this or they'll all go to jpeg
   $image_type=$this->image_type;

/*  if( $image_type == IMAGETYPE_JPEG ) {
     imagejpeg($this->image,$filename,$compression);
  } elseif( $image_type == IMAGETYPE_GIF ) {
     imagegif($this->image,$filename);  
  } elseif( $image_type == IMAGETYPE_PNG ) {*/
    // need this for transparent png to work          
    imagealphablending($this->image, false);
    imagesavealpha($this->image,true);
    imagepng($this->image,$filename);
 /* } */  
  if( $permissions != null) {
     chmod($filename,$permissions);
  }
}

    function output($image_type=IMAGETYPE_JPEG) {
        if( $image_type == IMAGETYPE_JPEG ) {
            imagejpeg($this->image);
        } elseif( $image_type == IMAGETYPE_GIF ) {
            imagegif($this->image);
        } elseif( $image_type == IMAGETYPE_PNG ) {
            imageAlphaBlending($this->image, true);
            imageSaveAlpha($this->image, true);
            imagepng($this->image);
        }   
    }

    function getWidth() {
        return imagesx($this->image);
    }
    function getHeight() {
        return imagesy($this->image);
    }
    function resizeToHeight($height) {
        $ratio = $height / $this->getHeight();
        $width = $this->getWidth() * $ratio;
        $this->resize($width,$height);
    }
    function resizeToWidth($width) {
        $ratio = $width / $this->getWidth();
        $height = $this->getheight() * $ratio;
        $this->resize($width,$height);
    }
    function scale($scale) {
        $width = $this->getWidth() * $scale/100;
        $height = $this->getheight() * $scale/100;
        $this->resize($width,$height);
    }
function resize($width,$height,$forcesize='n') {

  /* optional. if file is smaller, do not resize. */
  if ($forcesize == 'n') {
      if ($width > $this->getWidth() && $height > $this->getHeight()){
          $width = $this->getWidth();
          $height = $this->getHeight();
      }
  }

  $new_image = imagecreatetruecolor($width, $height);
  /* Check if this image is PNG or GIF, then set if Transparent*/  
  if(($this->image_type == IMAGETYPE_GIF) || ($this->image_type==IMAGETYPE_PNG)){
      imagealphablending($new_image, false);
      imagesavealpha($new_image,true);
      $transparent = imagecolorallocatealpha($new_image, 255, 255, 255, 127);
      imagefilledrectangle($new_image, 0, 0, $width, $height, $transparent);
  }
  imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());

  $this->image = $new_image;   
    }

}
list($width,$height)=getimagesize($uploadedfile);

$image = new SimpleImage();

$max_width = 200; // set a max width
$max_height = 150; // set a max height

$newwidth1=$width*0.25;
$newheight1=($height/$width)*$newwidth1;
$newwidth2=$width*0.50;
$newheight2=($height/$width)*$newwidth2;
$newwidth3=$width*0.75;
$newheight3=($height/$width)*$newwidth3;
$newwidth4=$width;
$newheight4=($height/$width)*$newwidth4;

$image->load($uploadedfile);
$image->resize($newwidth1,$newheight1); 
$image->save($file_path1); 

$image->load($uploadedfile);
$image->resize($newwidth2,$newheight2); 
$image->save($file_path2); 

$image->load($uploadedfile);
$image->resize($newwidth3,$newheight3); 
$image->save($file_path3); 

$image->load($uploadedfile);
$image->resize($newwidth4,$newheight4); 
$image->save($file_path4); 

 echo $return_name;

function getExtension($str) {

         $i = strrpos($str,".");
         if (!$i) { return ""; } 

         $l = strlen($str) - $i;
         $ext = substr($str,$i+1,$l);
         return $ext;
}

?>
