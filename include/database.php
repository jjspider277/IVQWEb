<?php
################################### Database Table Name #######################################################

	define('TBL_ADMIN','tbl_admin');
	define('TBL_ADMIN_LOGIN_DETAIL','tbl_admin_login_detail');

	define('TBL_COUNTRY','tbl_country');
	define('TBL_STATE','tbl_state');
	define('TBL_FRONT_DESC','tbl_front_description');
	define('TBL_FRONT_BANNER','tbl_front_banner');

	define('TBL_SUBSCRIBER_INV','tbl_subscriber_individual');
	define('TBL_MEMBER_BUSINESS_SUB','tbl_member_business_sub');
	

	define('TBL_ADVERTISE','tbl_advertise');
	define('TBL_DIRECTORY','tbl_directory');
	define('TBL_DIRECTORY_CATEGORY','tbl_dir_category');
	define('TBL_DIRECTORY_CONTACT','tbl_directory_contact');


	define('TBL_MEMBER','tbl_member');
	define('TBL_MEMBER_DIRECTORY','tbl_member_directory');
	define('TBL_MEMBER_LOGIN_DETAIL','tbl_member_login_detail');
	
	define('TBL_MEMBER_SUBSCRIBERS','tbl_member_subscriber');
 	define('TBL_MEMBER_SUBSCRIBE_DIRECTORY','tbl_member_subscribe_directory');
	define('TBL_MEMBER_SERVICES','tbl_member_services');
	define('TBL_MEMBER_BUSINESS','tbl_member_business');
	define('TBL_MEMBER_COUPONS','tbl_member_coupons');
	define('TBL_MEMBER_MESSAGING','tbl_member_messaging');
	
	define('TBL_SHARE','tbl_share');
 
 	define('TBL_SUBSCRIBER_FREE','tbl_member_subscriber_free');
 
 	define('TBL_BUSINESS_CATEGORY','tbl_business_category');
 	define('TBL_SUBSCRIBER_DIRECTORY','tbl_subscriber_directory');

	define('TBL_APP_SETTING','tbl_app_setting');

	define('TBL_QUESTION','tbl_question');
    define('TBL_QUESTION_OPTION','tbl_question_option');
    define('TBL_RESPONSE','tbl_response');
#################################################################################################################
?>