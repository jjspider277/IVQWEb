<?php
	######################  Member Side File Title #########################
	define('MEB_FILE_TITLE_HOME','IVQ - GROUP LIST');
    define('MEB_FILE_TITLE_FRONT','IVQ - BUY-SMART-SELL');
    define('MEB_FILE_TITLE_LOGIN','IVQ - LOGIN');
	define('MEB_FILE_TITLE_REGISTER','IVQ - Register');
    define('MEB_FILE_TITLE_CREATE_GROUP_1','IVQ - STEP-1');
    define('MEB_FILE_TITLE_GROUP_LIST','IVQ - STEP-1');
    define('MEB_FILE_TITLE_FORGOT','IVQ - FORGOT PASSWORD');
    define('MEB_FILE_TITLE_TERMS','IVQ - Terms of Use');
    define('MEB_FILE_TITLE_OPPORTUNITIES','IVQ - Opportunities');
    define('MEB_FILE_TITLE_INVESTOR_NEWS','IVQ - Investor News');
    define('MEB_FILE_TITLE_ABOUT_US','IVQ - ​About Us');
    define('MEB_FILE_TITLE_CONTACT_US','IVQ - Contact Us');
    define('MEB_FILE_TITLE_PRIVACY_POLICY','IVQ - Privacy Policy');

    define('MEB_FILE_TITLE_MANAGE_MEMBER','IVQ - CREATE MEMBER');
    define('MEB_FILE_TITLE_VIEW_MEMBER','IVQ - MEMBER');
    define('MEB_FILE_TITLE_MANAGE_COUPONS','IVQ - CREATE COUPON');
    define('MEB_FILE_TITLE_EDIT_COUPONS','IVQ - EDIT COUPON');
    define('MEB_FILE_TITLE_VIEW_COUPONS','IVQ - COUPONS');
    define('MEB_FILE_TITLE_PROFILE','IVQ - PROFILE');
    define('MEB_FILE_TITLE_MANAGE_MESSAGING','IVQ - CREATE BROADCAST');
    define('MEB_FILE_TITLE_EDIT_MESSAGING','IVQ - EDIT BROADCAST');
    define('MEB_FILE_TITLE_VIEW_MESSAGING','IVQ - BROADCAST');
    define('MEB_FILE_TITLE_MANAGE_SERVICES','IVQ - CREATE SERVICE');
    define('MEB_FILE_TITLE_EDIT_SERVICES','IVQ - EDIT SERVICE');
    define('MEB_FILE_TITLE_VIEW_SERVICES','IVQ - SERVICES');
    define('MEB_FILE_TITLE_VIEW_REQUESTS','IVQ - REQUESTS');

	define('MEB_FILE_TITLE_CHANGE_PASSWORD','Change Password');
	
	define('MEB_FILE_TITLE_MANAGE_SUBSCRIBER','manage_subscriber');
	define('MEB_FILE_TITLE_VIEW_SUBSCRIBER','Subscribers');

	define('MEB_FILE_TITLE_MANAGE_BUSINESS_SUB','manage_business_sub');
	define('MEB_FILE_TITLE_VIEW_BUSINESS_SUB','Business subscriber');

	define('MEB_FILE_TITLE_MANAGE_SURVEY','manage_survey');
	define('MEB_FILE_TITLE_VIEW_SURVEY','Surveys');

	define('MEB_FILE_TITLE_VIEW_RESPONSE','IVQ - Survey Results');
	############################################################################
	
	
    ###################### Admin Side File Title#################################

    define('ADM_FILE_TITLE_MANAGE_FRONT_HOME','Consumer Banner');
	
	define('ADM_FILE_TITLE_MANAGE_DIRECTORY','Group Setup');
	define('ADM_FILE_TITLE_VIEW_DIRECTORY','Search Group');
 
	define('ADM_FILE_TITLE_MANAGE_MEMBERS','Member Setup');
	define('ADM_FILE_TITLE_VIEW_MEMBERS','Search Members');
	
	define('ADM_FILE_TITLE_MANAGE_ADVERTISE','Advertisement');
	define('ADM_FILE_TITLE_VIEW_ADVERTISE','Search Advertisement');
	
	define('ADM_FILE_TITLE_MANAGE_BUSINESS','Business');
	define('ADM_FILE_TITLE_VIEW_BUSINESS','Search Business');

	define('ADM_FILE_TITLE_MANAGE_INDIVIDUAL','Individual subscriber');
	define('ADM_FILE_TITLE_VIEW_INDIVIDUAL','Search Individual subscriber');
	
	#############################################################################
?>