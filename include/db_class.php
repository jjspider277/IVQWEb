<?php
class DbConnect
{
   var $DbName=DB_PLATFORM;
   var $ServerName=DB_SERVER;
   var $UserName=DB_USER;
   var $PassWord=DB_PASSWORD;
   var $link;
   var $_lastHandle=false;
   
   ######################## Database Connection ############################################
   function  DbConnect()
   {
 	  //global $link;
      $this->link = mysql_pconnect($this->ServerName, $this->UserName, $this->PassWord);
			
      if (!$this->link) 
	  {
       die('Could not connect: ' . mysql_error());
      }
      $db_selected = mysql_select_db($this->DbName,$this->link);
      if (!$db_selected) 
	  {
         die ('Can\'t use foo : ' . mysql_error());
      }
      
      //mysql_set_charset("utf8");
   }
   #########################################################################################
   
   ######################## Query For Anytype ##############################################
   function Query($query) 
   {
		$result = mysql_query($query,$this->link);
		if(!$result)
		{
			 echo 'MySQL Error: ' . mysql_error();
			 exit;
		}
		$this->_lastHandle = $result;
		return $result;
   }
   #########################################################################################
   
   ######################## Select General Query ###########################################
   function select($sql,$rettype=1)
   {
    //RetType will return the type of rwo return
    // 1 for array, 2 for assoc, 3 for id
    
      $sql=trim($sql);
	 	  //echo $sql;

      $result = mysql_query($sql,$this->link);
      if(!$result)
	  {
         echo 'MySQL Error: ' . mysql_error();
         exit;
      }
      switch ($rettype)
	  {
        case 1:
            //return array
            while ($row = mysql_fetch_array($result)) 
			{ 
			  foreach ($row as $key => $value)
			  {
			  	$v[$key]=stripslashes($value);
			  }
              $data[]=$v;	
		    }	
			break;
        case 2:
          //return assoc
          while ($row = mysql_fetch_assoc($result)) 
		  {
              foreach ($row as $key => $value)
			  {
			  	$v[$key]=stripslashes($value);
			  }
              $data[]=$v;
          }
          break;
        case 3:
          //return row
          while ($row = mysql_fetch_row($result)) 
		  {
              foreach ($row as $key => $value)
			  {
			  	$v[$key]=stripslashes($value);
			  }
              $data[]=$v;
          }
          break;
      }
      mysql_free_result($result);
      return $data;
   }
   #########################################################################################
   
   
   ######################## Select Data Query #############################################
   function selectData($table,$fields="",$where="",$extra="",$rettype=1)
   {
   	  if($fields=="")
	  {
		 $fields = '*';
	  }
	  else{
			$fields = implode(",",$fields);
		}
      //RetType will return the type of rwo return
      // 1 for array, 2 for assoc, 3 for id
	  $sql = "Select ".$fields." from ".$table;
	  if(!empty($where))
	  {
	 	 $sql .= " where " .$where;
	  }
	  if(!empty($extra))
	  {
	  	$sql .= $extra;
	  }
    $sql=trim($sql);
	  
	 // echo $sql;
	  //echo "<hr>";
	 //exit();
	 
      $result = mysql_query($sql,$this->link);
      if(!$result)
	  {
         echo $sql;
	     echo "<hr>";
		 echo 'MySQL Error: ' . mysql_error();
		 exit;
      }
      switch ($rettype)
	  {
        case 1:
            //return array
            while ($row = mysql_fetch_array($result)) 
			{
              foreach ($row as $key => $value)
			  {
			  	$v[$key]=stripslashes($value);
			  }
              $data[]=$v;
            }
            break;
        case 2:
          //return assoc
          while ($row = mysql_fetch_assoc($result)) 
		  {
              foreach ($row as $key => $value)
			  {
			  	$v[$key]=stripslashes($value);
			  }
              $data[]=$v;
          }
          break;
        case 3:
          //return row
          while ($row = mysql_fetch_row($result)) 
		  {
              foreach ($row as $key => $value)
			  {
			  	$v[$key]=stripslashes($value);
			  }
              $data[]=$v;
          }
          break;
      }
      mysql_free_result($result);
      return $data;
   }
   #########################################################################################
   
   ######################## Insert General Query ##########################################
   function insert($sql)
   {
      $sql=trim($sql);
      $result = mysql_query($sql,$this->link);
      if (!$result) 
	  {
        return 0;
      }
	  else
	  {
        return mysql_insert_id();
      }
   }
   #########################################################################################
   
   ######################## Insert Data Query #############################################
   function insertData($table,$values='')
   {
        $data = "";
		foreach($values as $k=>$v) 
		{
			if($data == "") 
			{
				$data = " ".trim($k)." = '".mysql_real_escape_string(trim($v))."'";
			} 
			else 
			{
				$data .= ", ".trim($k)." = '".mysql_real_escape_string(trim($v))."'";
			}
		}
		
		$sql = "Insert into ".$table." Set ".$data;
		$sql = str_replace("\\&quot;\\","",$sql);
		$sql = str_replace("\\&quot;","",$sql);
		
		$sql=trim($sql);
	  //echo $sql;
  
	  $result = mysql_query($sql,$this->link);
		
	    if (!$result)
	    {
			 /*echo $sql;
			 echo "<hr>";
			 echo 'MySQL Error: ' . mysql_error();
			 exit;*/
			 
			return 0;
	    }
	    else
	    {
			return mysql_insert_id();
		}
		
   }
   #########################################################################################
   
   ######################## Update General Query ##########################################
   function update($sql)
   {
      $sql=trim($sql);
      $result = mysql_query($sql,$this->link);
	 
      if (!$result)
      {
         return 0;
      }
	  else
	  {
        return $result;
    }
      mysql_free_result($result);
   }
   #########################################################################################
   
   ######################## Update Data Query #############################################
   function updateData($table,$values='',$where='')
   {
   	  $data = "";
	  foreach($values as $k=>$v) 
	  {
			if($data == "") 
			{
				$data = " ".trim($k)." = '".mysql_real_escape_string(trim($v))."'";
			} 
			else 
			{
				$data .= ", ".trim($k)." = '".mysql_real_escape_string(trim($v))."'";
			}
	   }
 $sql = "Update ".$table." Set ".$data; 
	   if($where) 
	   {
			$sql .= " where ".$where;
	   } 
	   $sql = str_replace("\\&quot;\\","",$sql);
	   $sql = str_replace("\\&quot;","",$sql);
       $sql=trim($sql);
    $sql; 
       
       $result = mysql_query($sql,$this->link);
       //echo $sql;
	  /*
	   echo "<hr>";
	   echo 'MySQL Error: ' . mysql_error();
	   exit;*/
	   if (!$result)
	   {
		  	/* echo $sql;
			 echo "<hr>";
			 echo 'MySQL Error: ' . mysql_error();
			 exit;*/
		 return 0;
	   }
	   else
	   {
		 return $result;
	   }
		mysql_free_result($result);
   }
   #########################################################################################

   ######################## Delete General Query ##########################################
   function delete($sql)
   {
      $sql=trim($sql);
      $result = mysql_query($sql,$this->link);
      if (!$result) 
	  {
        return 0;
      }
	  else
	  {
        return $result;
      }
   }
   function deleteData($table,$where='')
   {
   	  $sql = "Delete From " . $table;
	  if($where) 
	  {
		$sql .= " where ".$where;
	  } 
	  
	  $sql=trim($sql);
      /* echo $sql;
      exit;*/
      $result = mysql_query($sql,$this->link);
	  
	  /*echo $sql;
	   echo "<hr>";
	   echo 'MySQL Error: ' . mysql_error();
	   exit;*/
      if (!$result) 
	  {
        return 0;
      }
	  else
	  {
        return $result;
      }
   }
  
   #########################################################################################
   
   ######################## Get Any Field Query ############################################
   function getFieldName($sql)
   {
      $sql=trim($sql);
      $result = mysql_query($sql,$this->link);

      for($i=0;$i<mysql_num_fields($result);$i++)
      {
        $row[]=mysql_field_name($result, $i) ;
      }

     return $row;
   }
   #########################################################################################
   
   ######################## Database Connection Close ######################################
   function DbClose()
   {
     mysql_close($this->link);
   }
   #########################################################################################
}
?>
