<?php
function ajaxMsg() {
    if ($_SESSION['msg']) {
        $str = '<table width="100%" border="0" align="left" class="mysuc">';
        $str .= "<tr>
	<td class='msgcls'>" . $_SESSION['msg'] . "</td>
          </tr>";
        echo $str .= "</table>";
        unset($_SESSION['msg']);
    }
}
function getSlider($arrayImg)
{
	$slider ='<ul class="slides">';
	for($i=0;$i<count($arrayImg);$i++)
	{
		if($arrayImg[$i]['adv_url']!="")
		{
			$url = $arrayImg[$i]['adv_url'];
			$targetblank = "target='_blank'"; 
		}
		else
		{
			$url = "#";
			$targetblank = "";
		}
 	$slider .='<li><a href="'.$url.'" '.$targetblank.' ><img src="'.UPLOAD_WWW_ADVERTISE_FILE.$arrayImg[$i]['adv_id']."/".$arrayImg[$i]['adv_filename'].'" title="'.$arrayImg[$i]['adv_title'].'"/></a></li>';
 	//<p class="flex-caption">test</p> 
 	}
	$slider .='</ul>';
	return $slider;

}
################################################################################################################################
function isAdminMember()
{
	if(empty($_SESSION['adm_admin_id']))
	{
		$URL=ADM_FOLDER_WWW.ADM_LOGIN;
		redirect($URL);
		exit;
	}
}

function isMember()
{

	if(empty($_SESSION['meb_admin_id']))
	{
		$URL=MEB_FOLDER_WWW.MEB_LOGIN;
		redirect($URL);
		exit;
	}
 
}

function isFreeMember()
{

	if(empty($_SESSION['sbf_admin_id']))
	{
		$URL = ROOT_WWW."freememberlogin.php";
		redirect($URL);
		exit;
	}
 
}

function frontData()
{
	global $db;
	$des_fields = array("*");
	$des_where  = "des_id=1";
	return $desRes 	= $db->selectData(TBL_FRONT_DESC,$des_fields,$des_where,$extra="",2);
}
function getMemeberAsDir()
{
	$dirId = getMemberSessionDirId();

	if(empty($dirId))
	{
		$URL = ROOT_WWW.MEB_LOGIN;
		redirect($URL);
		exit;
	}
	global $db;
	$allmeb_dir_files = array("meb_id");
	$allmeb_dir_where  = "meb_status = 'Active' and meb_dir_id='".$dirId."'";
	$allmeb_dir 	= $db->selectData(TBL_MEMBER,$allmeb_dir_files,$allmeb_dir_where,$extra="",2);
	$tot_meb = count($allmeb_dir);
	for($i=0;$i<$tot_meb;$i++)
	{
		if($i==$tot_meb-1)
		{
			$addComma = "";
		}
		else
		{
			$addComma = ",";
		}
		$allMeb .= $allmeb_dir[$i]["meb_id"].$addComma;
	}
	return $allMeb;
}
function isSetProject()
{
	if(empty($_SESSION["pro_id1"]))
	{
		$URL=ROOT_WWW.FRT_PROJECTEN;
		redirect($URL);
		exit;
	}
}

function chkSessionVar($session)
{
	if(!empty($session))
	{
		return true;
	}
	else
	{
		return false;
	}
}

function valid_user()
{
	global $db;
	
	if(empty($_SESSION['adm_username']))
	{
		return 0;
	}
	elseif(empty($_SESSION['adm_password']))
	{
		return 0;
	}
	elseif(empty($_SESSION['adm_id']))
	{
		return 0;
	}
	else
	{
	   	$adm_username = $_SESSION['adm_username'];
		$adm_password = $_SESSION['adm_password'];
		$adm_id = $_SESSION['adm_id'];
				
		$data = $db->select("SELECT * FROM ".TBL_ADMIN_USER." WHERE adm_username='".$adm_username."' and adm_password='".$adm_password."' ");
		if(count($data)==0)
		{
			return 0;
		}
		else
		{
			if( !($adm_id == $data[0]['adm_id']))
			{
				return 0;
			}
		}
	}
	return 1;
}
################################################################################################################################
function randomPassword($length) {
	$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
	$pass = array(); 
	$alphaLength = strlen($alphabet) - 1;
	for ($i = 0; $i < $length; $i++) {
		$n = rand(0, $alphaLength);
		$pass[] = $alphabet[$n];
	}
	return implode($pass);
}
################################################################################################################################
function getAdminSessionId()
{
	return $_SESSION['adm_id'];
}
function getFreeMemberSessionId()
{
 // if(empty($_SESSION['sbf_admin_id']))
	// {
		// $URL = ROOT_WWW."freememberlogin.php";
		// redirect($URL);
		// exit;
	// }
 // else 
 // {
  return $_SESSION['sbf_id'];
 //}
	
}

function getMemberSessionId()
{
	return $_SESSION['meb_id'];
}
function getMemberType()
{
	return $_SESSION['meb_type'];
}
function getMemberSessionDirId()
{
	return $_SESSION['mebDirId'];
}
function setSession($url)
{
	$expl_url = explode("/",$url);
	foreach($expl_url as $key => $value)
	{
		 $lan_prefix = $expl_url[2];
	}
	return $lan_prefix;
}

################################################################################################################################


################################################################################################################################
function getFrontFileMetaKey($filename,$field_prefix)
{
	if(!empty($filename))
	{
		$file = explode(".",$filename);	
				
		$file_meta_key = FRT_META_KEY_.strtoupper($file[0]);
		
		if(defined($file_meta_key))	
		{
			$display_meta_key=constant($file_meta_key);
		}
		else 
		{
			$display_meta_key=FRT_DEFAULT_META_KEY;
		}	
	}
	else
	{
		$display_meta_key=FRT_DEFAULT_META_KEY;
	}
	
	return $display_meta_key;
}

function getFrontFileMetaDesc($filename,$field_prefix)
{
	if(!empty($filename))
	{
		$file = explode(".",$filename);	
				
		$file_meta_desc = FRT_META_DESC_.strtoupper($file[0]);
		
		if(defined($file_meta_desc))	
		{
			$display_meta_desc=constant($file_meta_desc);
		}
		else 
		{
			$display_meta_desc=FRT_DEFAULT_META_DESC;
		}	
	}
	else
	{
		$display_meta_desc=FRT_DEFAULT_META_DESC;
	}
	
	return $display_meta_desc;
}

function getFrontFileTitle($filename,$field_prefix)
{
	if(!empty($filename))
	{
		$file = explode(".",$filename);	
				
		$file_title = FRT_TITLE_.strtoupper($file[0]);
		
		if(defined($file_title))	
		{
			$display_file_title=constant($file_title);
			$display_title=FRT_DEFAULT_TITLE." - ".$display_file_title;
		}
		else 
		{
			$display_title=FRT_DEFAULT_TITLE;
		}	
	}
	else
	{
		$display_title=FRT_DEFAULT_TITLE;
	}
	
	return $display_title;
}

function getMemberFileTitle($filename)
{
	if(!empty($filename))
	{
		$file = explode(".",$filename);	
		
		$file_title = MEB_FILE_TITLE_.strtoupper($file[0]);
		
		if(defined($file_title))	
		{
			$display_file_title=constant($file_title);
			$display_title=$display_file_title;
		}
		else 
		{
			$display_title=MEB_FILE_TITLE_HOME;
		}
	}
	else
	{
		$display_title=MEB_FILE_TITLE_HOME;
	}
	
	return $display_title;
}
function getAdminFileTitle($filename)
{
	if(!empty($filename))
	{
		$file = explode(".",$filename);	
		
		$file_title = ADM_FILE_TITLE_.strtoupper($file[0]);
		
		if(defined($file_title))	
		{
			$display_file_title=constant($file_title);
			$display_title=$display_file_title;
		}
		else 
		{
			$display_title=ADM_DEFAULT_PAGE_TITLE;
		}
	}
	else
	{
		$display_title=ADM_DEFAULT_PAGE_TITLE;
	}
	
	return $display_title;
}
################################################################################################################################


################################################################################################################################
function getTextArea($field_name='',$field_id='',$field_value='',$rows='3',$cols='53')
{
	$tinymce = '<textarea name="'.$field_name.'" id="'.$field_id.'" rows="'.$rows.'" cols="'.$cols.'">'.$field_value.'</textarea>';
	
	return $tinymce;
}



function getCKeditorSource($field_id, $field_name, $field_value = "") {
    ?>
    <textarea id="<?php echo $field_id ?>" name="<?php echo $field_name ?>"><?php echo stripslashes($field_value); ?></textarea>
    <script type="text/javascript">
    $(window).load(function(){
			CKEDITOR.replace( '<?php echo $field_name ?>',{
            filebrowserBrowseUrl : 'ckeditor/ckfinder/ckfinder.html',
            filebrowserUploadUrl : 'ckeditor/ckeditor/uploader/upload.php',
            toolbar : 'MyToolbar',
            width: "100%"
        });
});
   </script>	

    <?php
}



################################################################################################################################
function getRequiredIcon()
{
	$text = '<span class="required_icon">*</span>';
	
	return $text;
}

function getSQLMessage($action,$sql_value,$display_name)
{
	if($action=="Add")
	{
		if($sql_value==0)
		{
			$text = $display_name." has not been added.";	
		}
		else
		{
			$text = $display_name." has been added successfully.";	
		}
	}
	
	if($action=="Update")
	{
		if($sql_value==0)
		{
			$text = $display_name." data has been updated without changes.";
		}
		else
		{
			$text = $display_name." data has been updated successfully.";
		}
	}
	
	if($action=="Status")
	{
		if($sql_value==0)
		{
			$text = $display_name." status has been updated without changes.";
		}
		else
		{
			$text = $display_name." status has been updated successfully.";
		}
	}
	
	if($action=="Delete")
	{
		if($sql_value==0)
		{
			$text = $display_name." has not been deleted.";	
		}
		else
		{
			$text = $display_name." has been deleted successfully.";	
		}
	}
	
	if($action=="Send")
	{
		if($sql_value==0)
		{
			$text = "E-mail is not sent.";	
		}
		else
		{
			$text = "E-mail is sent.";	
		}
	}
	
	return $text;
}
################################################################################################################################


################################################################################################################################
function getFrontActionLink($link,$class,$display_name)
{

	  $link='<a href="'.ROOT_WWW.$link.'" class="'.$class.'" title="'.$display_name.'">'.$display_name.'</a>';
     return $link;	
}


function getAdminActionLink($pagename,$language_prefix,$action,$id_name,$id,$display_name="",$xtra="")
{
	
	if(!empty($id))
	{
		$getid='&'.$id_name.'='.$id;
	}

	if($display_name=="Active")
	{
		$status='&status=Inactive';
	}
	else if($display_name=="Inactive")
	{
		$status="&status=Active";
	}
	 if($display_name=="Yes")
	{
		$status='&message_read=No';
	}
	else if($display_name=="No")
	{
		$status="&message_read=Yes";
	}
	
	if($display_name=="")
	{
		$display_name=$action;
	}
	
	if($action=="Images")
	{
		
		$link='<a href="'.ROOT_WWW.ADM_INDEX_PARAMETER.$pagename.'&method='.$action.$getid.$status.$xtra.'">'.$display_name.'</a>';
	}
	else if($action=="Delete")
	{
		$link='<a href="'.ROOT_WWW.ADM_INDEX_PARAMETER.$pagename.'&method='.$action.$getid.$status.$xtra.'" onClick="return confirm(\'Are you sure want to delete?\');"><img src="images/delete.gif" alt="Delete" title="Delete" border=0></a>';
	}
	else if($action=="Edit")
	{
		//$link='<a href="'.ROOT_WWW.ADM_INDEX_PARAMETER.$pagename.'&method='.$action.$getid.$status.$xtra.'" class="tooltip"><img src="images/right.png" alt="Edit" title="Edit" /></a>';
		$link=ROOT_WWW.ADM_INDEX_PARAMETER.$pagename.'&method='.$action.$getid.$status.$xtra;
	}
	else if($display_name=="Active")
	{
		$link='<a href="'.ADM_FOLDER_WWW.ADM_INDEX_PARAMETER.$pagename.'&method='.$action.$getid.$status.$xtra.'"><img src="images/active_status.png" alt="Active" title="Active" border=0></a>';
	}
	else if($display_name=="Inactive")
	{
		$link='<a href="'.ADM_FOLDER_WWW.ADM_INDEX_PARAMETER.$pagename.'&method='.$action.$getid.$status.$xtra.'"><img src="images/inactive.png" alt="Inactive" title="Inactive"  border=0></a>';
	}
	else if($display_name=="Yes")
	{
		$link='<a href="'.ADM_FOLDER_WWW.ADM_INDEX_PARAMETER.$pagename.'&language_prefix='.$language_prefix.'&method='.$action.$getid.$status.$xtra.'"><img src="images/green.png" alt="Yes" title="Read. Click to mark as unread." border=0></a>';
	}
	else if($display_name=="No")
	{
	
		$link='<a href="'.ADM_FOLDER_WWW.ADM_INDEX_PARAMETER.$pagename.'&language_prefix='.$language_prefix.'&method='.$action.$getid.$status.$xtra.'"><img src="images/red.png" alt="No" title="Unread. Click to mark as read."  border=0></a>';
	}
	else if($display_name=="View")
	{
		$link='<a href="'.ADM_FOLDER_WWW.ADM_INDEX_PARAMETER.$pagename.'&language_prefix='.$language_prefix.'&method='.$action.$getid.$status.$xtra.'"><img src="images/icon_fullview.png" alt="Full View" title="Full View" border=0 height="20" width="20"></a>';
	}
	else
	{
		$link='<a href="'.ADM_FOLDER_WWW.ADM_INDEX_PARAMETER.$pagename.'&method='.$action.$getid.$status.$xtra.'">'.$display_name.'</a>';	
	}
	return $link;	
}



//New AdminActionLink with Ajax code...
function getAdminActionLink2($pagename,$language_prefix,$action,$id_name,$id,$display_name="",$xtra="")
{
	if(!empty($id))
	{
		$getid='&'.$id_name.'='.$id;
	}
	
	if($action	==	"Active")
	{
	//	$link='<a href="'.ADM_FOLDER_WWW.ADM_INDEX_PARAMETER.$pagename.'&method='.$action.$getid.$status.$xtra.'"><img src="images/active_status.png" alt="Active" title="Active" border=0></a>';
	
		$link="<a href='javascript:void(0);' onclick='getAjaxStatusAction(tbl_pages,$field_prefix,$id);'><img src='images/active_status.png' alt='Active' title='Active' border=0></a>";
	}
	else if($action == "Edit")
	{
		//$link='<a href="'.ROOT_WWW.ADM_INDEX_PARAMETER.$pagename.'&method='.$action.$getid.$status.$xtra.'" class="tooltip"><img src="images/right.png" alt="Edit" title="Edit" /></a>';
		$link=ADM_FOLDER_WWW.ADM_INDEX_PARAMETER.$pagename.'&method='.$action.$getid.$status.$xtra;
	}
	return $link;	
}

function getMemberActionLink3($pagename,$language_prefix,$action,$id_name,$id,$display_name="",$xtra="")
{
    if(!empty($id))
    {
        $getid='&'.$id_name.'='.$id;
    }

    if($action	==	"Active")
    {
        //	$link='<a href="'.ADM_FOLDER_WWW.ADM_INDEX_PARAMETER.$pagename.'&method='.$action.$getid.$status.$xtra.'"><img src="images/active_status.png" alt="Active" title="Active" border=0></a>';

        $link="<a href='javascript:void(0);' onclick='getAjaxStatusAction(tbl_pages,$field_prefix,$id);'><img src='images/active_status.png' alt='Active' title='Active' border=0></a>";
    }
    else if($action == "Edit")
    {
        //$link='<a href="'.ROOT_WWW.ADM_INDEX_PARAMETER.$pagename.'&method='.$action.$getid.$status.$xtra.'" class="tooltip"><img src="images/right.png" alt="Edit" title="Edit" /></a>';
        $link=MEMBER_WWW.MEB_INDEX_PARAMETER.$pagename.'&method='.$action.$getid.$status.$xtra;
    }
    return $link;
}

function deleteDirectory($id)
{
	global $db;
	
	$mebId = $db->select("select meb_id from ".TBL_MEMBER." where meb_dir_id=".$id);	
	
	$advId = $db->select("select adv_id from ".TBL_ADVERTISE." where adv_dir_id=".$id);
	
	$conId = $db->select("select dco_id from ".TBL_DIRECTORY_CONTACT." where dco_dir_id=".$id);

	$SubId = $db->select("select sub_id from ".TBL_MEMBER_SUBSCRIBERS." where sub_dir_id=".$id);
	if($mebId!="" || $advId!=""|| $conId!="" || $SubId!="")
	{
		return "NotDeleted";
	}
	else
	{
		$update_values['dir_status']			= "Deleted";
		$update_values['dir_updated_id']		= getAdminSessionId();
		$update_values['dir_updated_date']	= date('Y-m-d H:i:s');
		$dirDelId = $db->updateData(TBL_DIRECTORY,$update_values,"dir_id=".$id);
		return "Deleted";
	}
}
function deleteMembers($id)
{
	global $db;
	
	$subId = $db->select("select sub_id from ".TBL_MEMBER_SUBSCRIBERS." where sub_meb_id=".$id);
	for($i=0;$i<count($subId);$i++)
	{
		$update_sub_values['sub_status']			= "Deleted";
		$update_sub_values['sub_updated_id']		= getAdminSessionId();
		$update_sub_values['sub_updated_date']	= date('Y-m-d H:i:s');
		$subDelId = $db->updateData(TBL_MEMBER_SUBSCRIBERS,$update_sub_values,"sub_id=".$subId[$i]["sub_id"]);
	}

	$busId = $db->select("select bus_id from ".TBL_MEMBER_BUSINESS." where bus_meb_id=".$id);
	for($a=0;$a<count($busId);$a++)
	{
		$update_bus_values['bus_status']			= "Deleted";
		$update_bus_values['bus_updated_id']		= getAdminSessionId();
		$update_bus_values['bus_updated_date']	= date('Y-m-d H:i:s');
		$busDelId = $db->updateData(TBL_MEMBER_BUSINESS,$update_bus_values,"bus_id=".$busId[$a]["bus_id"]);
	}

	$serId = $db->select("select ser_id from ".TBL_MEMBER_SERVICES." where ser_meb_id=".$id);
	for($c=0;$c<count($serId);$c++)
	{
		$update_ser_values['ser_status']			= "Deleted";
		$update_ser_values['ser_updated_id']		= getAdminSessionId();
		$update_ser_values['ser_updated_date']	= date('Y-m-d H:i:s');
		$serDelId = $db->updateData(TBL_MEMBER_SERVICES,$update_ser_values,"ser_id=".$serId[$c]["ser_id"]);
	}

	$copId = $db->select("select cop_id from ".TBL_MEMBER_COUPONS." where cop_meb_id=".$id);
	for($c=0;$c<count($copId);$c++)
	{
		$update_cop_values['cop_status']			= "Deleted";
		$update_cop_values['cop_updated_id']		= getAdminSessionId();
		$update_cop_values['cop_updated_date']	= date('Y-m-d H:i:s');
		$copDelId = $db->updateData(TBL_MEMBER_COUPONS,$update_cop_values,"cop_id=".$copId[$c]["cop_id"]);
	}

	$msgId = $db->select("select msg_id from ".TBL_MEMBER_MESSAGING." where msg_meb_id=".$id);
	for($c=0;$c<count($msgId);$c++)
	{
		$update_msg_values['msg_status']			= "Deleted";
		$update_msg_values['msg_updated_id']		= getAdminSessionId();
		$update_msg_values['msg_updated_date']	= date('Y-m-d H:i:s');
		$msgDelId = $db->updateData(TBL_MEMBER_MESSAGING,$update_msg_values,"msg_id=".$msgId[$c]["msg_id"]);
	}

	$update_values['meb_status']			= "Deleted";
	$update_values['meb_updated_id']		= getAdminSessionId();
	$update_values['meb_updated_date']	= date('Y-m-d H:i:s');
	$GPDetail_result = $db->updateData(TBL_MEMBER,$update_values,"meb_id=".$id);
}
function getAdminURL($pagename,$action="",$select_id="")
{
	$URL = ADM_FOLDER_WWW.ADM_INDEX_PARAMETER.$pagename;
		
	if(!empty($action))
	{
		$URL .= "&method=".$action;
	}
	if(!empty($select_id))
	{
		$URL .= "&".$select_id;
	}
	
	return $URL;
}

function getMemberURL($pagename,$action="",$select_id="")
{
	$URL = MEB_FOLDER_WWW.MEB_INDEX_PARAMETER.$pagename;
		
	if(!empty($action))
	{
		$URL .= "&method=".$action;
	}
	if(!empty($select_id))
	{
		$URL .= "&".$select_id;
	}
	return $URL;
}

###############################################################################################################################
function getFrontTopMainMenu()
{
	$query = "SELECT * FROM " .TBL_FRONT_MENU." WHERE fmnu_id!=0 ";
	$query .=" and fmnu_pid = '0'";
		$query .=" and fmnu_status ='Active'";
 	  $query .=" ORDER BY fmnu_sort_order ";
	return $query;
} 

function getFrontTopSubMenu($main_menu_id="")
{
	$query = "SELECT * FROM " .TBL_FRONT_MENU." WHERE fmnu_id!=0";
	$query .=" and fmnu_pid!='0'";
	$query .=" and fmnu_pid ='".$main_menu_id."'";	
	$query .=" and fmnu_display 	='1'";
	$query .=" and (fmnu_display_type 	='both' or fmnu_display_type 	='top')";
	$query .=" ORDER BY fmnu_id";
    //echo   $query;
	return $query;
}

function getFrontFooterMenu()
{
	$query = "SELECT * FROM " .TBL_FRONT_MENU." WHERE fmnu_id!=0 ";
	$query .=" and fmnu_pid = '0'";
	$query .=" and fmnu_display 	='1'";
	$query .=" and (fmnu_display_type 	='both' or fmnu_display_type 	='footer')";
	$query .=" ORDER BY fmnu_sort_order";
	
	return $query;
}
################################################################################################################################


################################################################################################################################
function getPageName($filename,$field_prefix,$language_prefix="")
{	
	$newFileName = explode(".",$filename);
	$newname = $newFileName[0];
	
	$query = "SELECT * FROM " .TBL_FRONT_MENU." WHERE ".$field_prefix."id!=0 ";
	$query .=" and ".$field_prefix."filename".$language_prefix."='".$newname."'";
	$query .=" and ".$field_prefix."status='Active'";
	
	return $query;
}


function getInnerPageName($filename,$language_prefix="")
{	
	if($language_prefix == FRT_ENGLISH_PREFIX)
	{
		$language_prefix = FRT_DUTCH_PREFIX;
	}
	else
	{
		$language_prefix = FRT_ENGLISH_PREFIX;
	}
	
	$newFileName = explode(".",$filename);
	$newname = $newFileName[0];
	$query = "SELECT * FROM " .TBL_LINK_PAGE_NAME." WHERE link_id!=0 ";
	$query .=" and link_name".$language_prefix."='".$newname."'";
	return $query;
}

function displayUploadImage($image_name,$section_name,$extra="")
{
    if(!empty($image_name))
   {
		if($section_name=='News')
		{
			$display_path=NEWS_IMG_DISPLAY;
			$org_img_prefix=NEWS_IMG_ORG_PREFIX;
			$th_img_prefix=NEWS_IMG_TH_PREFIX;
		//	$img_content='<a class="dwd" rel="lightbox" href="'.$display_path.$image_name.'">View Image</a>&nbsp;<font color="#000000">';
			 $img_content = '<a rel="lightbox" href="' . $display_path . $image_name . '"><img class="shw1" src="../images/show_img.png"  alt="Show Image" title="Show Image"/></a>&nbsp;<font color="#000000">';
			
		}
		if($section_name=='Fixed Page')
		{
			$display_path=PAGE_IMG_DISPLAY;
		    $img_content = '<a rel="lightbox" href="' . $display_path . $image_name . '"><img class="shw1" src="../images/show_img.png"  alt="Show Image" title="Show Image"/></a>&nbsp;<font color="#000000">';
			
		}
			if($section_name=='Dynamic Page')
		{
			$display_path=DYNAMIC_IMG_DISPLAY;
			  $img_content = '<a rel="lightbox" href="' . $display_path . $image_name . '"><img class="shw1" src="../images/show_img.png"  alt="Show Image" title="Show Image"/></a>&nbsp;<font color="#000000">';
			
		}
		if($section_name=='Service Image')
		{
			$display_path=SERVICE_IMG_DISPLAY;
			 $img_content = '<a rel="lightbox" href="' . $display_path . $image_name . '"><img class="shw1" src="../images/show_img.png"  alt="Show Image" title="Show Image"/></a>&nbsp;<font color="#000000">';
			
		}
			if($section_name=='Youtube')
		{
			$display_path=YOU_IMG_DISPLAY;
			 $img_content = '<a rel="lightbox" href="' . $display_path . $image_name . '"><img class="shw1" src="../images/show_img.png"  alt="Show Image" title="Show Image"/></a>&nbsp;<font color="#000000">';
			
		}
		if($section_name=='Promotion')
		{
			$display_path=PROMOTION_IMG_DISPLAY;
			$org_img_prefix=PROMOTION_IMG_ORG_PREFIX;
			$th_img_prefix=PROMOTION_IMG_TH_PREFIX;
			//$img_content='<a class="dwd" rel="lightbox" href="'.$display_path.$image_name.'">View Image</a>&nbsp;<font color="#000000">';
			 $img_content = '<a rel="lightbox" href="' . $display_path . $image_name . '"><img class="shw1" src="../images/show_img.png"  alt="Show Image" title="Show Image"/></a>&nbsp;<font color="#000000">';
			//$img_content='<a class="thickbox" href="'.$display_path.$image_name.'">View Image</a>&nbsp;<font color="#000000">';
		}
		if($section_name=='Banners')
		{
			$display_path=BANNER_IMG_DISPLAY;
			$org_img_prefix=BANNER_IMG_ORG_PREFIX;
			$th_img_prefix=BANNER_IMG_TH_PREFIX;
			//$img_content='<a class="dwd" rel="lightbox"  href="'.$display_path.$image_name.'">View Image</a>&nbsp;<font color="#000000">';
			 $img_content = '<a rel="lightbox" href="' . $display_path . $image_name . '"><img class="shw1" src="../images/show_img.png"  alt="Show Image" title="Show Image"/></a>&nbsp;<font color="#000000">';
					/*	<a rel="lightbox" href="images/examples/image-1.jpg"><img alt="" src="images/examples/thumb-1.jpg"></a>*/
			//$img_content='<a class="fancybox"  href="#inline_'.$image_name.'">View Image</a>&nbsp;<font color="#000000">';
			//$img_div='<div id="#inline_'.$image_name.'" style="width:800px;display: none;"><img src="'.$display_path.$image_name.'" /></div>';
		}
			if($section_name=='Slider')
		{
			$display_path=SLIDER_IMG_DISPLAY;
			$org_img_prefix=SLIDER_IMG_ORG_PREFIX;
		//	$img_content='<a class="dwd" rel="lightbox"  href="'.$display_path.$image_name.'">View Image</a>&nbsp;<font color="#000000">';
		 $img_content = '<a rel="lightbox" href="' . $display_path . $image_name . '"><img class="shw1" src="../images/show_img.png"  alt="Show Image" title="Show Image"/></a>&nbsp;<font color="#000000">';
		}

				if($section_name=='Home Products')
		{
			$display_path=HOME_PRODUCTS_IMG_DISPLAY;
			//$org_img_prefix=SLIDER_IMG_ORG_PREFIX;
		//	$img_content='<a class="dwd" rel="lightbox"  href="'.$display_path.$image_name.'">View Image</a>&nbsp;<font color="#000000">';
		 $img_content = '<a rel="lightbox" href="' . $display_path . $image_name . '"><img class="shw1" src="../images/show_img.png"  alt="Show Image" title="Show Image"/></a>&nbsp;<font color="#000000">';
		}
				if($section_name=='Advertisement')
		{
			if($extra == "Leaderboard" ){
			$display_path=LEADEARBOARD_IMG_DISPLAY;
			
			//$img_content='<a class="dwd" rel="lightbox"  href="'.$display_path.$image_name.'">View Image</a>&nbsp;<font color="#000000">';
			 $img_content = '<a rel="lightbox" href="' . $display_path . $image_name . '"><img class="shw1" src="../images/show_img.png"  alt="Show Image" title="Show Image"/></a>&nbsp;<font color="#000000">';
			}
			if($extra == "Skyscraper" ){
			$display_path=SKY_IMG_DISPLAY;
			
			//$img_content='<a class="dwd" rel="lightbox"  href="'.$display_path.$image_name.'">View Image</a>&nbsp;<font color="#000000">';
			 $img_content = '<a rel="lightbox" href="' . $display_path . $image_name . '"><img class="shw1" src="../images/show_img.png"  alt="Show Image" title="Show Image"/></a>&nbsp;<font color="#000000">';
			}
			if($extra == "rectangle" ){
			$display_path=RECTANGLE_IMG_DISPLAY;
		
			//$img_content='<a class="dwd" rel="lightbox"  href="'.$display_path.$image_name.'">View Image</a>&nbsp;<font color="#000000">';
			 $img_content = '<a rel="lightbox" href="' . $display_path . $image_name . '"><img class="shw1" src="../images/show_img.png"  alt="Show Image" title="Show Image"/></a>&nbsp;<font color="#000000">';
			}
			
		}	
				if($section_name=='Industries')
		{
			$display_path=INDUSTRY_IMG_DISPLAY;
			//$img_content='<a class="dwd" rel="lightbox"  href="'.$display_path.$image_name.'">View Image</a>&nbsp;<font color="#000000">';
			 $img_content = '<a rel="lightbox" href="' . $display_path . $image_name . '"><img class="shw1" class="shw1" src="../images/show_img.png"  alt="Show Image" title="Show Image"/></a>&nbsp;<font color="#000000">';
		}	
						if($section_name=='Industry Graphic')
		{
			$display_path=INDUSTRY_GRAPHIC_IMG_DISPLAY;
		
			 $img_content = '<a rel="lightbox" href="' . $display_path . $image_name . '"><img class="shw1" class="shw1" src="../images/show_img.png"  alt="Show Image" title="Show Image"/></a>&nbsp;<font color="#000000">';
		}	
						if($section_name=='Category')
		{
			$display_path=IND_CAT_IMG_DISPLAY;
			//$img_content='<a class="dwd" rel="lightbox"  href="'.$display_path.$image_name.'">View Image</a>&nbsp;<font color="#000000">';
			 $img_content = '<a rel="lightbox" href="' . $display_path . $image_name . '"><img class="shw1" class="shw1" src="../images/show_img.png"  alt="Show Image" title="Show Image"/></a>&nbsp;<font color="#000000">';
		}	
	if($section_name=='Products')
		{
			$display_path=PRODUCTS_IMG_DISPLAY;
		//	$img_content='<a class="dwd" rel="lightbox"  href="'.$display_path.$image_name.'">View Image</a>&nbsp;<font color="#000000">';
		            $img_content = '<a rel="lightbox" href="' . $display_path . $image_name . '"><img class="shw1" src="../images/show_img.png"  alt="Show Image" title="Show Image"/></a>&nbsp;<font color="#000000">';
		}
		if ($section_name == 'Client Cases') {
            $display_path = TESTIMONIALS_IMG_DISPLAY;
            
            $img_content = '<a rel="lightbox" href="' . $display_path . $image_name . '"><img class="shw1" src="../images/show_img.png"  alt="Show Image" title="Show Image"/></a>&nbsp;<font color="#000000">';
            //$img_content='<a class="thickbox" href="'.$display_path.$image_name.'">View Image</a>&nbsp;<font color="#000000">';
        }	
		
	}
	else
	{
		$img_content='';
	}
return $img_content;
}

function displayUploadFiles($pdf_name,$section_name,$extra="")
{

   if(!empty($pdf_name))
   {
	
        if($section_name=='News')   {
			$display_path=NEWS_PDF_DISPLAY;
			
		}
		if($section_name=='Fixed Page') {
				//$display_path=PAGE_PDF_DISPLAY;
			$display_path=PAGE_PDF_DISPLAY.'page_'.$extra.'/';
            $upload_path=PAGE_PDF_DISPLAY.'page_'.$extra.'/';  
		}
		if($section_name=='Dynamic Page') {
				//$display_path=DYNAMIC_PDF_DISPLAY;
			  $display_path=DYNAMIC_PDF_DISPLAY.'dynamic_'.$extra.'/';
               $upload_path=DYNAMIC_PDF_DISPLAY.'dynamic_'.$extra.'/';  
		}
				/*if($section_name=='Fixed Page') {
            $display_path=PAGE_PDF_DISPLAY.'page_'.$extra.'/';
           $upload_path=PAGE_PDF_DISPLAY.'page_'.$extra.'/';  
           
        }	*/

		//$pdf_content='<a class="dwd" href="'.$display_path.$pdf_name.'" target="_blank">View PDF</a>';
		  $pdf_content = '<a href="' . $display_path . $pdf_name . '" target="_blank"><img class="shw1" src="../images/viewpdf.png"  alt="View Pdf" title="View Pdf"/></a>';
	}
	else
	{
		$pdf_content='';
	}
	
	return $pdf_content;
}


function displayStatusSection($status,$action="")
{
	if($status=="Active")
	{ 
		$active="checked";
	}
	if($status=="Inactive")
	{ 
		$inactive="checked";
	}
	$status_content='
     <input type="radio" name="status" align="left" value="Active" '.$active.'/> Active
    
    <input type="radio" name="status" align="left" value="Inactive" '.$inactive.' /> Inactive';
					
	return $status_content;
}
function displaypdfStatusSection($status,$action="")
{if($status=="Active"){$active="checked";}if($status=="Inactive"){$inactive="checked";}$status_content='<input type="radio" name="pdfstatus0" align="left" value="Active" '.$active.'/>Active<input type="radio" name="pdfstatus0" align="left" value="Inactive" '.$inactive.' /> Inactive';return $status_content;
}
function displaypdfStatusSection1($status,$action="")
{if($status=="Active"){$active="checked";}if($status=="Inactive"){$inactive="checked";}$status_content='<input type="radio" name="pdfstatus10" align="left" value="Active" '.$active.'/>Active<input type="radio" name="pdfstatus10" align="left" value="Inactive" '.$inactive.' /> Inactive';return $status_content;
}
function displaypdfStatusSectionedit($status,$action="")
{if($status=="Active"){$active="checked";}if($status=="Inactive"){$inactive="checked";}$status_content='<input type="radio" name="pdfstatus1'.$action.'" align="left" value="Active" '.$active.'/>Active<input type="radio" name="pdfstatus1'.$action.'" align="left" value="Inactive" '.$inactive.' /> Inactive';return $status_content;
}
function displayvideoStatusSection($status,$action="")
{
	if($status=="Active")
	{ 
		$active="checked";
	}
	if($status=="Inactive")
	{ 
		$inactive="checked";
	}
	$status_content='
     <input type="radio" name="videostatus0" align="left" value="Active" '.$active.'/> Active
    
    <input type="radio" name="videostatus0" align="left" value="Inactive" '.$inactive.' /> Inactive';
					
	return $status_content;
}
function displayvideoStatusSection1($status,$action="")
{
	if($status=="Active")
	{ 
		$active="checked";
	}
	if($status=="Inactive")
	{ 
		$inactive="checked";
	}
	$status_content='
     <input type="radio" name="videostatus10" align="left" value="Active" '.$active.'/> Active
    
    <input type="radio" name="videostatus10" align="left" value="Inactive" '.$inactive.' /> Inactive';
					
	return $status_content;
}
function displayvideoStatusSectionedit($status,$action="")
{
	if($status=="Active")
	{ 
		$active="checked";
	}
	if($status=="Inactive")
	{ 
		$inactive="checked";
	}
	$status_content='
     <input type="radio" name="videostatus1'.$action.'" align="left" value="Active" '.$active.'/> Active
    
    <input type="radio" name="videostatus1'.$action.'" align="left" value="Inactive" '.$inactive.' /> Inactive';
					
	return $status_content;
}
function displayhome_bottomStatusSection($status,$action="")
{
	if($status=="Yes")
	{ 
		$active="checked";
	}
	if($status=="No")
	{ 
		$inactive="checked";
	}
	$status_content='
     <input type="radio" name="home_bottom" align="left" value="Yes" '.$active.'/> Active
    
    <input type="radio" name="home_bottom" align="left" value="No" '.$inactive.' /> Inactive';
					
	return $status_content;
}
function displayHomeDisplaySection($prm_homedisplay,$action="")
{
	if($prm_homedisplay=="Yes")
	{ 
		$active="checked";
	}
	if($prm_homedisplay=="No")
	{ 
		$inactive="checked";
	}
	$display_content=' <input type="radio" name="prm_homedisplay" align="left" value="Yes" '.$active.'/>
						  Yes
						  <input type="radio" name="prm_homedisplay" align="left" value="No" '.$inactive.' />No';
					
	return $display_content;
}
function displayTypeSection($bnr_type,$action="")
{
	if($bnr_type=="skyscraper")
	{ 
		$active="checked";
	}
	if($bnr_type=="leaderboard")
	{ 
		$inactive="checked";
	}
	$display_content='<input type="radio" name="bnr_type" align="left" value="skyscraper" '.$active.'/> Skyscraper
					   <input type="radio" name="bnr_type" align="left" value="leaderboard" '.$inactive.' /> Leaderboard';
					
	return $display_content;
}
################################################################################################################################

function getAdminLevelCombo($adm_level="")
{
	?>
<select name="adm_level" id="adm_level">
  <option value="0">--Select--</option>
  <option value="Master" <?php echo ($adm_level == "Master")?'selected="selected"':'';?>>Master</option>
  <option value="Maintenance" <?php echo ($adm_level == "Maintenance")?'selected="selected"':'';?>>Maintenance</option>
</select>
<?php
}

function getCategoryList($name="",$id="",$value="",$default_name="--Select Category--",$style="")
{
	global $db;
	$query = "SELECT * FROM ".TBL_CATEGORY." 
			  WHERE cat_status='Active' AND cat_parent_id = '0' AND cat_id != '0'";
	$data = $db->select($query);	
	
	$categoryCombo = '';
		$categoryCombo .= '<select name="'.$name.'" id="'.$id.'" class="'.$class.'" style="'.$style.'" onchange="getSubcat(this.value)">';
		$categoryCombo .= '<option value="0">'.$default_name.'</option>';
	
	for($i=0;$i<count($data);$i++)
	{		
		$categoryCombo .= '<option value="'.$data[$i]["cat_id"].'"'; 
		if($value == $data[$i]["cat_id"])
		{
			$categoryCombo .='selected="selected"';
		}
		$categoryCombo .='>'.$data[$i]["cat_name"].'</option>';

	}
	$categoryCombo .='</select>';
	
	return $categoryCombo;
}

function getSectorList($name="",$id="",$value="",$default_name="--Select Sector--",$style="",$extra="")
{
	global $db;
	$query = "SELECT * FROM ".TBL_SECTOR." 
			  WHERE sec_id != '0'";
	$data = $db->select($query);	
	
	$sectorCombo = '';
		$sectorCombo .= '<select name="'.$name.'" id="'.$id.'" class="'.$class.'" style="'.$style.'"  '.$extra.'>';
		$sectorCombo .= '<option value="0">'.$default_name.'</option>';
	
	for($i=0;$i<count($data);$i++)
	{		
		$sectorCombo .= '<option value="'.$data[$i]["sec_id"].'"'; 
		if($value == $data[$i]["sec_id"])
		{
			$sectorCombo .='selected="selected"';
		}
		$sectorCombo .='>'.$data[$i]["sec_title"].'</option>';
		
	}
	$sectorCombo .='</select>';
	
	return $sectorCombo;
}


function getProjectList($name="",$id="",$value="",$default_name="--Select Category--",$style="",$extra="")
{
	global $db;
	$query = "SELECT * FROM ".TBL_PORTFOLIO."
			  WHERE port_status='Active' AND port_id != '0' ORDER BY port_client_name";
	$data = $db->select($query);	
	
	$projCombo = '';
		$projCombo .= '<select name="'.$name.'" id="'.$id.'" class="'.$class.'" style="'.$style.'" >';
		$projCombo .= '<option value="0">'.$default_name.'</option>';
	
	for($i=0;$i<count($data);$i++)
	{		
		$projCombo .= '<option value="'.$data[$i]["port_id"].'"'; 
		if($value == $data[$i]["port_id"])
		{
			$projCombo .='selected="selected"';
		}
		$projCombo .='>'.$data[$i]["port_client_name"].'</option>';
		
	}
	$projCombo .='</select>';
	
	return $projCombo;
}

function getProjectChoices($name="",$id="",$value="",$default_name="--Select--",$class="",$style="")
{

?>
<?php /*?><select name="<?php echo $name?>" id="<?php echo $id?>" class="<?php echo $class?>" >
		<option value="0"><?php echo $default_name?></option>
		<option value="In uitvoering" <?php echo ("In uitvoering" == $value)?'selected="selected':'';?>>In uitvoering</option>
		<option value="In voorbereiding" <?php echo ("In voorbereiding" == $value)?'selected="selected':'';?>>In voorbereiding</option>
		<option value="Gerealiseerd" <?php echo ("Gerealiseerd" == $value)?'selected="selected':'';?>>Gerealiseerd</option><?php */?>
<?php	
	return; 
}


################################################################################################################################


################################################################################################################################
function getSelectList($table_name,$field_prefix,$auto_id="",$status="",$xtra_subquery="")
{
	$query = "SELECT * ";
	if(!empty($xtra_subquery))
	{
		$query .=",".$xtra_subquery;
	}
	$query .=" FROM " .$table_name; 
	$query .=" WHERE ".$field_prefix."id!=0 ";
	if(!empty($auto_id))
	{
		$query .= " AND ".$field_prefix."id ='".$auto_id."'";
	}
	if(!empty($status))
	{
		$query .= " AND ".$field_prefix."status ='".$status."'";
	}   
	return $query;
}

function getViewUserList($adm_id="")
{
	$query = "SELECT * from " .TBL_ADMIN_USER." where adm_id!=0 ";
	if(!empty($adm_id))
	{
		$query .= " and adm_id = '".$adm_id."'";
	}
	return $query;
}

function getContactListWithSupplier()
{
	$sql_query = "SELECT sup.sup_fname,con.* 
				FROM ".TBL_CONTACT_US." con 	  
				LEFT JOIN ".TBL_SUPPLIER_LEADS." as supled ON (con.con_id = supled.led_con_id)	 
				LEFT JOIN ".TBL_SUPPLIER." as sup ON (sup.sup_id = supled.led_sup_id)
				WHERE con.con_id != 0 ";
				
	return $sql_query;			
}

################################################################################################################################

function getProductForMail($product)
{
	
	global $db;
	$proDiv = '';
	for($i=0;$i<count($product);$i++)
	{
		$subpro = explode("_",$product[$i]);
		$r[$subpro[0]][] = $subpro[1];
	}
	$proDiv .='<div>';
	foreach($r as $key => $val)
	{
		$sqlCat = getSelectList(TBL_CATEGORY,"cat_",$auto_id=$key,"Active");
		$resCat = $db->select($sqlCat);
		
		$proDiv .='<div class="contact-text">'.$resCat[0]["cat_name"].'</div>';
		for($i=0;$i<count($r[$key]);$i++)
		{
			$sqlSubCat = getSelectList(TBL_CATEGORY,"cat_",$auto_id=$r[$key][$i],"Active");
			$sqlSubCat .=" AND cat_parent_id = '".$key."'";
			$resSubCat = $db->select($sqlSubCat);
			$proDiv .='<div style="margin-left:16px;">'.$resSubCat[0]["cat_name"].'</div>';
		}
	}
	$proDiv .='</div>';
	return $proDiv;
}

function checkValueExist($table,$field_prefix,$field_name,$field_value,$id="")
{
	global $db;

	$sql_query = getSelectList($table,$field_prefix);
	$sql_query .=" AND ".$field_prefix.$field_name." = '".$field_value."'";
	
	if(!empty($id))
	{
$sql_query .=" AND ".$field_prefix."id != '".$id."'";
	}
	

	$resValue = $db->select($sql_query);
	if(count($resValue) > 0)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}


function checkValueExistCat($table,$field_prefix,$field_name,$field_value,$ind_id,$id="")
{
	global $db;

	$sql_query = getSelectList($table,$field_prefix);
 	$sql_query .=" AND ".$field_prefix.$field_name." = '".$field_value."' AND icat_ind_id= '".$ind_id."' "; 
	
	if(!empty($id))
	{
 $sql_query .=" AND ".$field_prefix."id != '".$id."'";
	}
	

	$resValue = $db->select($sql_query);
	if(count($resValue) > 0)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}



//Check email exists...
function chkemailExist($email)
{
		global $db;
		$chkemailQ1			=	getSelectList("tbl_admin","adm_");
		$chkemailQ1			.=	" AND adm_email = '".$email."'";
		$chkemailRes1		=	$db->select($chkemailQ1);
		
		if(count($chkemailRes1) > 0)
			return false;
		else
			return true;
}

//Check Promotoion
function chkExist($section_table,$field_prefix,$field,$field_value)
{
		global $db;
		$chkemailQ1			=	getSelectList($section_table,$field_prefix);
		$chkemailQ1			.=	" AND ".$field_prefix.$field." = '".$field_value."'";
		$chkemailRes1		=	$db->select($chkemailQ1); 
		 
		if(count($chkemailRes1) > 0)
			return false;
		else
			return true;
}

function chkExistEdit($section_table,$field_prefix,$field,$field_value,$field_id)
{
		global $db;
		$chkemailQ1			=	getSelectList($section_table,$field_prefix);
	  $chkemailQ1			.=	" AND ".$field_prefix.$field." = '".$field_value."' AND ".$field_prefix."id!=".$field_id.""; 
		$chkemailRes1		=	$db->select($chkemailQ1); 
		 
		if(count($chkemailRes1) > 0)
		
			return false;
		else
			return true;
}




function checkedRadio($value1,$value2,$defult="")
{

	if(!empty($value1))
	{
		if($value1 == $value2)
		{
			$checked = 'checked="checked"';
		}
	}
	else if($defult == "1")
	{
		$checked = 'checked="checked"';
	}
	else
	{
		$checked = '';
	}
	return $checked;
}

function displayDefultValInlisting($value,$defultvalue="N/A",$chkVal="")
{
	if(!empty($value))
	{
		$defult = $value;
	}
	else if($chkVal == $value)
	{
		$defult = $defultvalue;
	}
	else
	{
		$defult = $defultvalue;
	}
	return $defult;
}

function implodeArry($arrVal,$explodeval)
{
	if(!empty($arrVal) && $arrVal > 1)
	{
		$implodeVal = implode($explodeval,$arrVal);
	}
	else
	{
		$implodeVal = $arrVal;
	}
	return $implodeVal;
}



function getMenu($language_prefix,$pos)
{
    global $db;
	$pageQ	 	= 	"select p.pag_menu_name,p.pag_url, l.lng_prefix from tbl_pages p , tbl_language l 
					 where p.pag_lng_id	= l.lng_id and l.lng_prefix = '".$language_prefix."' and p.pag_pgp_id = ".$pos." and p.pag_status = 'Active' and p.pag_parent_id = 0 order by p.pag_sort_order";
	$pageRes	=	$db->select($pageQ);	
	$pageresRow            = count($pageRes);
	if($pageresRow>0)
	{			
		 $menuTop .=' <a id="drop_icon" href="#" class="menu-icon">Menu</a><ul id="i_nav">';
		  for($i=0;$i<$pageresRow;$i++) 
		  {
		  	$link 			= $pageRes[$i]['lng_prefix']."/".$pageRes[$i]['pag_url'];  
		  	$display_name	= $pageRes[$i]['pag_menu_name'];
          	$menuTop .='<li>';
      		$menuTop .=	getFrontActionLink($link,$class,$display_name);
      		$menuTop .='</li>';
           }
			$menuTop .='</ul>';
		}
		return $menuTop;
}



function getHtacesslinkName($title)
{
	$hts = strtolower(str_replace(" ","-",$title));
	return $hts;
}

function getURIinProjectName()
{
	global $db;
	
	$uri = explode("/",$_SERVER["REQUEST_URI"]);
	$tt = $uri[count($uri)-2];
	$sql = getSelectList(TBL_PROJECT,"pro_");
	$sql .= " AND LOWER(REPLACE(pro_title,' ','-')) = '".$tt."'";
	$res = $db->select($sql);
	
	$idArr = array();
	$idArr["sec_id"] = $res[0]["pro_sec_id"];
	$idArr["pro_id"] = $res[0]["pro_id"];
	
	return $idArr;
}

function frtshowerr()
{
	$str = '';
	if(!empty($_SESSION['error']))
	{
		$str = '<table width="100%" border="0" align="" class="errcls">';
		for($i=0; $i<count($_SESSION['error']); $i++)
		{
			$str .= "<tr><td style='color:red'> ".$_SESSION['error'][$i]."</td></tr>";
		}
		$str .= "</table>";
		return $str;
	}
	return false;
}

function frtshowmsg()
{
	$str = '';
	if(!empty($_SESSION['msg']))
	{
		$str = '<table width="100%" border="0" align="">';
		for($i=0; $i<count($_SESSION['msg']); $i++)
		{
			$str .='<tr><td class="msgcls frt">'.$_SESSION['msg'][$i].'</td></tr>';
		}
		$str .= "</table>";
		return $str;
	}
	return false;
}





##################################################
# Function Use : Display session error
##################################################
function showerr()
{
	$str = '';
	if(!empty($_SESSION['error']))
	{
		$str = '<table width="100%" border="0" align="left" class="error_text">';
		for($i=0; $i<count($_SESSION['error']); $i++)
		{
			$str .= "<tr>
				<td class='errcls' height='20px' style='color:red'>".$_SESSION['error'][$i]."</td>
			  </tr>";
		}
		$str .= "</table>";
		return $str;
	}
	return false;
}

##################################################
# Function Use : Display session message
##################################################
function showmsg()
{
	$str = '';
	if(!empty($_SESSION['msg']))
	{
		$str = '<table width="100%" border="0" align="left" class="mysuc">';
		/*for($i=0; $i<count($_SESSION['msg']); $i++)
		{*/
			$str .= "<tr>
				<td class='msgcls'>".$_SESSION['msg']."</td>
			  </tr>";
		//}
		$str .= "</table>";
		return $str;
	}
	return false;
}



function getPrintLinkSection($titlelink,$proName)
{
	if(SITE_URL_REWRITE == "On")
	{
		$url = "projecten/".$proName."/hou-me-op-de-hoogte";
	}
	else
	{
		$url = FRT_HOU_ME_OP_DE_HOOGTE;
	}

	$printlink = '';
	$printlink .='<div class="sub-links-right">
						<ul>
							<li><a href="javascript:window.popPrint();" title="AFDRUKKEN">PROJECTFICHE AFDRUKKEN</a></li>
							<li><a href="'.getFrontURL($url).'" title="HOU ME OP DE HOOGTE">HOU ME OP DE HOOGTE</a></li>
						</ul>
					</div>';
                    //<li><a href="'.getFrontURL($url).'" title="HOU ME OP DE HOOGTE">HOU ME OP DE HOOGTE</a></li>
	return $printlink;					
}


function setSidePanelMenu()
{
	global $db;

	if(!empty($_GET["id"]))
	{
		$sql = getSelectList(TBL_PROJECT,"pro_");
		$sql .= " AND LOWER(REPLACE(pro_title,' ','-')) = '".$_GET["id"]."'";
		$res = $db->select($sql);
		$pro_id = $res[0]["pro_id"];
		$sec_id = $res[0]["pro_sec_id"];
	}
	else
	{
		$idArr = getURIinProjectName();
		$sec_id = $idArr["sec_id"];
		$pro_id = $idArr["pro_id"];
	}

	$sqllig = getSelectList(TBL_LIGGING,"lig_","","Active");
	$sqllig .= " AND lig_pro_id = '".$pro_id."'";
	$reslig = $db->select($sqllig);

	$sqlAan = getSelectList(TBL_AANBOD,"aan_","","Active");
	$sqlAan .= " AND aan_pro_id = '".$pro_id."'";
	$resAan = $db->select($sqlAan);

    $sqlPlannen = getSelectList(TBL_ARCHITECTUUR,"arc_","","Active");
	$sqlPlannen .= " AND arc_pro_id = '".$pro_id."'";
	$resPlannen = $db->select($sqlPlannen);

    $sqlDuu = getSelectList(TBL_DUURZAAM_ONTWIKKELE,"duu_","","Active");
	$sqlDuu .= " AND duu_pro_id = '".$pro_id."'";
	$resDuu = $db->select($sqlDuu);
	
	$sqlFoto = getSelectList(TBL_FOTO,"fot_","","Active");
	$sqlFoto .= " AND fot_pro_id = '".$pro_id."'";
	$resFoto = $db->select($sqlFoto);

    $sqlCon = getSelectList(TBL_CONTACT,"con_","","Active");
	$sqlCon .= " AND con_pro_id = '".$pro_id."'";
	$resCon = $db->select($sqlCon);


	if(empty($reslig))
	{
		$idsArr[] = 30;
	}
    if(empty($resAan))
	{
		$idsArr[] = 31;
	}
    if(empty($resPlannen))
	{
		$idsArr[] = 32;
	}
     if(empty($resDuu))
	{
		$idsArr[] = 33;
	}
    if(empty($resFoto))
	{
		$idsArr[] = 34;
	}
    if(empty($resCon))
	{
		$idsArr[] = 35;
	}
	return $idsArr;
}


function leftDuurzaamDisply($stringRequrireId){
  global $db;
  $sqlDuuzaamPage = getSelectList(TBL_PAGES,"pag_","","Active");
  $sqlDuuzaamPage .= " AND 	pag_fmid in ($stringRequrireId)";
  $resDuuzaamPageArr = $db->select($sqlDuuzaamPage);
  $resCount = count($resDuuzaamPageArr);
  $htmlOutput = ' <ul class="inner-menu-right">';
    $r=1;
     foreach($resDuuzaamPageArr as $resDuuzaamPage)  {
        $pagename =  $resDuuzaamPage['pag_url'];
        if($r==$resCount){$rightLastClass = 'inner-menu-right-last';}else{$rightLastClass = '';}
        if(SITE_URL_REWRITE!='On')  {
    		$linkDuuzaam = ROOT_WWW.FRT_INDEX_PARAMETER.$pagename;
	    } else {
           	$linkDuuzaam=ROOT_WWW.'nl/duurzaam-ontwikkelen/'.$pagename;
        }
       $imagePath = PAGE_IMG_DISPLAY.$resDuuzaamPage['pag_image'];
        $htmlOutput .= '<li class="'.$rightLastClass.'"><span>';
        if($resDuuzaamPage['pag_image']!="") {
            $htmlOutput .='<a href="'.$linkDuuzaam.'"><img title="'.$resDuuzaamPage['pag_title'].'" alt="'.$resDuuzaamPage['pag_title'].'" src="'.$imagePath.'" width="64px" height="61px"></a>';
        }
       $htmlOutput .= ' </span><a href="'.$linkDuuzaam.'" title="'.$resDuuzaamPage['pag_title'].'">'.ucfirst($resDuuzaamPage['pag_title']).'</a><br>
        '.$resDuuzaamPage['pag_short_desc'].'<br clear="all"></li>';
    $r++;
    }
     $htmlOutput .= '</ul>';
    return $htmlOutput;

}





#########################################################
# Function Use : redirect() Function Used to redirect url
#########################################################
function redirect($URL)
{
	echo "<script type='text/javascript'>window.location='".$URL."'</script>";
//	header('location:'.$URL);
}

function getFormAction()
{
	$editFormAction = $_SERVER['PHP_SELF'];
	if (!empty($_SERVER['QUERY_STRING']))
	{
	  $editFormAction .= "?" . htmlentities($_SERVER['QUERY_STRING']);
	}
	
	return $editFormAction;
}
##################################################
# Function Use : Display session error list
##################################################
/*function errorList($var,$message)
{
	if(empty($var))
	{
		$_SESSION['error'][]="Error: ".$message;
	}	
}*/

function errorList($var,$message)
{
	if(empty($var))
	{
		$_SESSION['err'][]= $message;
	}	
}

##############################################################
# Function Use :This Function Used to valid 
##############################################################
function isValidEmail($email)
{
	if (!@eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email))
	{
		return false; 
	}
	else
	{
	   return true;	
	}
}

function isValidCapitalAlphaChar($text)
{
	if (!eregi("^[A-Z]", $text))
	{
		return false; 
	}
	else
	{
	   return true;	
	}
}

function isValidAlphaChar($text)
{
	if (!eregi("^[A-Za-z]", $text))
	{
		return false; 
	}
	else
	{
	   return true;	
	}
}

function isValidAlphaNumericChar($text)
{
	if (!eregi("^[A-Za-z0-9]", $text))
	{
		return false; 
	}
	else
	{
	   return true;	
	}
}

function isVaildPassword($pass_text,$mini_len="6",$max_len="25")
{
   if (strlen($pass_text) < $mini_len || strlen($pass_text) > $max_len)
   { 
		$text="Password must be between ".$mini_len." to ".$max_len." character";
   }
   else
   {
	  for ($i=0 ; $i < strlen($pass_text) ; $i++) 
	  {
		   if(ord($pass_text[$i])=='32')
			   $ord="improper";
	  }
	  if($ord=="improper")
	  {
			$text="Please enter password properly.";
	  }
   }
   
   return $text;
}

function isValidHttpURL($url)
{
	if(!preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $url))
	{
		return false; 
	}
	else
	{
	   return true;	
	}
}

function isValidURL($url)
{
	/*$regex = "((https?|ftp)\:\/\/)?"; // SCHEME
	$regex .= "([a-zA-Z0-9+!*(),;?&=\$_.-]+(\:[a-zA-Z0-9+!*(),;?&=\$_.-]+)?@)?"; // User and Pass
	$regex .= "([a-zA-Z0-9-.]*)\.([a-z][A-Z]{2,3})"; // Host or IP
	$regex .= "(\:[0-9]{2,5})?"; // Port
	$regex .= "(\/([a-zA-Z0-9+\$_-]\.?)+)*\/?"; // Path
	$regex .= "(\?[a-zA-Z+&\$_.-][a-zA-Z0-9;:@&%=+\/\$_.-]*)?"; // GET Query
	$regex .= "(#[a-zA-Z_.-][a-zA-Z0-9+\$_.-]*)?"; */
	
	$regex = "((https?|ftp)\:\/\/)?"; // SCHEME
	$regex .= "([a-z0-9+!*(),;?&=\$_.-]+(\:[a-z0-9+!*(),;?&=\$_.-]+)?@)?"; // User and Pass
	$regex .= "([a-z0-9-.]*)\.([a-z]{2,3})"; // Host or IP
	$regex .= "(\:[0-9]{2,5})?"; // Port
	$regex .= "(\/([a-z0-9+\$_-]\.?)+)*\/?"; // Path
	$regex .= "(\?[a-z+&\$_.-][a-z0-9;:@&%=+\/\$_.-]*)?"; // GET Query
	$regex .= "(#[a-z_.-][a-z0-9+\$_.-]*)?"; 
	
	if(!preg_match("/^$regex$/", $url))
	{
		return false; 
	}
	else
	{
	   return true;	
	}
}

function isValidImage($file_name)
{
	$ext = strtolower(end(explode('.', $file_name)));
	if($ext=="jpg" || $ext=="jpeg" || $ext=="gif" || $ext=="bmp" ||$ext=="png"  || $ext=="tif")
	{
		return true;
	}
	else
	{
		return false;
	}
}

function isValidPDF($file_name)
{

    //echo $file_name;
    //exit;
    $ext = strtolower(end(explode('.', $file_name)));
	if($ext=="pdf")
	{
		return true;
	}
	else
	{
		return false;
	}
}

function isValidPPT($file_name)
{
	$ext = strtolower(end(explode('.', $file_name)));
	if($ext=="ppt" || $ext=="pptx" )
	{
		return true;
	}
	else
	{
		return false;
	}
}
##############################################################
# Function Use : This function used to Get Post or Requesr Value
##############################################################
function retPostOrReq($val)
{
	if(!empty($_POST[$val]))
	{
		$retval=$_POST[$val];
	}
	else 
	{
		$retval=base64_decode($_REQUEST[$val]);
	}
	return $retval;
}

####################################################################
# Function Use : This Function Used to Explode
####################################################################
function explodeFun($sep,$val)
{
	$ret=explode($sep,$val);
	return $ret;
}

####################################################################
# Function Use : This Function Used to Substring in forms of Words
####################################################################
function wordCut($text, $limit, $msg)
{
  if (strlen($text) > $limit)
  {
       $txt1 = wordwrap($text, $limit, '[cut]');
       $txt2 = explode('[cut]', $txt1);
       $ourTxt = $txt2[0];
       $finalTxt = $ourTxt.$msg;
   }
   else
   {
       $finalTxt = $text;
   }
   return $finalTxt;
}

####################################################################
# Function Use : This Function Used to Substring in forms of Words
####################################################################
function fullcharCut($mytext, $limit)
{
   if (strlen($mytext) > $limit)
   {
   		$text=chunk_split($mytext,$limit,"<br>");
   }
   else
   {
   		$text=$mytext;
   }
   return $text;
}

###########################################################################
# Function Use : This Function Used to Substring in forms of Chrarcter Cut
###########################################################################
function charCut($mytext, $limit, $msg)
{
   if (strlen($mytext) > $limit)
   {
   		$text=chunk_split($mytext,$limit," ");
   }
   else
   {
   		$text=$mytext;
   }
	
   if (strlen($text) > $limit)
   {
       $txt1 = wordwrap($text, $limit, '[cut]');
	   $txt2 = explode('[cut]', $txt1);
       $ourTxt = $txt2[0];
       $finalTxt = $ourTxt.$msg;
   }
   else
   {
       $finalTxt = $text;
   }
   return $finalTxt;
}

################################################################
# Function Use : This function used to make random code
################################################################
function makeCode($usrid) 
{
    $pars = "13579";
    $impars = "24680";
	
    for ($x=0; $x < 6; $x++) 
	{
		mt_srand ((double) microtime() * 1000000);
		$pars[$x] = substr($pars, mt_rand(0, strlen($pars)-1), 1);
		$impars[$x] = substr($impars, mt_rand(0, strlen($impars)-1), 1);
    }
	
    $coded =$usrid. $pars[0] . $impars[0] .$pars[2] . $pars[1] . $impars[1] . $pars[3] . $impars[3] . $pars[4];
    
	return($coded);
}

function dateComparison($start_date,$end_date)
{
	$start_date = strtotime(str_replace("-","/",$start_date));
	$end_date = strtotime(str_replace("-","/",$end_date));
	
	if($start_date>$end_date)
	{
		return false;
	}
	else
	{
		return true;
	}
}
	
##################################################
# Function Use : For Count of Days
##################################################	
function countDays($a,$b)
{
   // First we need to break these dates into their constituent parts:
   $gd_a = getdate( $a );
   $gd_b = getdate( $b );
   // Now recreate these timestamps, based upon noon on each day
   // The specific time doesn't matter but it must be the same each day
   $a_new = mktime( 12, 0, 0, $gd_a['mon'], $gd_a['mday'], $gd_a['year'] );
   $b_new = mktime( 12, 0, 0, $gd_b['mon'], $gd_b['mday'], $gd_b['year'] );
   // Subtract these two numbers and divide by the number of seconds in a
   // day. Round the result since crossing over a daylight savings time
  // barrier will cause this time to be off by an hour or two.
   return round( abs( $a_new - $b_new ) / 86400 );
}

################################################################
# Function Use : This Function Used to check Leap Year
################################################################
function isLeapYear($year)
{
	if ($year%4 == 0)
	{
		if ($year%100 == 0 && $year%400 <>0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	else
	{
		return false;
	}
}

##################################################
# Function Use : For Count of Total Days
##################################################
function totalDays($month,$year)
{
	$days = array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
	if ($month==2 && isLeapYear($year))
	{
		return 29;
	}
	else 
	{
		return $days[$month-1];
	}
}

function dateDiffGregorian($dformat, $endDate, $beginDate)
{
    $date_parts1=explode($dformat, $beginDate);
    $date_parts2=explode($dformat, $endDate);
    $start_date=gregoriantojd($date_parts1[0], $date_parts1[1], $date_parts1[2]);
    $end_date=gregoriantojd($date_parts2[0], $date_parts2[1], $date_parts2[2]);
    return $end_date - $start_date;
}

function dateDiff($start_date,$end_date)
{
	 $s=strtotime($end_date) - strtotime($start_date);
	 $m=intval($s/60);
	 $s=$s%60;
	 $h=intval($m/60);
	 $m=$m%60;
	 $d=intval($h/24);
	 $h=$h%24;
	 
	 return  trim($d);
}

function dateDiffTime($date1,$date2)
{
	$s=strtotime($date2) - strtotime($date1);
	$m=intval($s/60);
	$s=$s%60;
	$h=intval($m/60);
	$m=$m%60;
	$d=intval($h/24);
	$h=$h%24;
	return trim($d)." day ".trim($h)." hours and ".trim($m)." minutes ago.";
}

function getTimeDifference( $start, $end )
{
	$uts['start']      =    strtotime( $start );
    $uts['end']        =    strtotime( $end );
		  
    if( $uts['start']!==-1 && $uts['end']!==-1 )
    {
        if( $uts['end'] >= $uts['start'] )
        {
            $diff    =    $uts['end'] - $uts['start'];
            if( $days=intval((floor($diff/86400))) )
                $diff = $diff % 86400;
            if( $hours=intval((floor($diff/3600))) )
                $diff = $diff % 3600;
            if( $minutes=intval((floor($diff/60))) )
                $diff = $diff % 60;
            $diff    =    intval( $diff );            
            return( array('days'=>$days, 'hours'=>$hours, 'minutes'=>$minutes, 'seconds'=>$diff) );
        }
        else
        {
            //trigger_error( "Ending date/time is earlier than the start date/time", E_USER_WARNING );
		}
    }
    else
    {
        //trigger_error( "Invalid date/time data detected", E_USER_WARNING );
    }
    return false;
}

function dispayEndDatetime($datetime_diff,$convert)
{
	$limit0="0";
	$limit3="3";
	
	$days=$datetime_diff['days'];
	$hours=$datetime_diff['hours'];
	$minutes=$datetime_diff['minutes'];
	
	if($limit3<=$days)
	{
		$datetime=strftime("%B %d, %Y", strtotime($convert));
	}
	else if(($limit0<$days) && ($days<$limit3))
	{
		/*$datetime=$days." days, ".$hours." hours and ".$minutes." minutes";*/
		$datetime=$days." d, ".$hours." h and ".$minutes." m";
	}
	else 
	{
		if($hours!=$limit0 && $minutes!=$limit0)
		{
			/*$datetime=$hours." hours and ".$minutes." minutes";*/
			$datetime=$hours." h and ".$minutes." m";
		}
		else if($datetime_diff[hours]==$limit0 && $datetime_diff[minutes]!=$limit0)
		{
			/*$datetime=$minutes." minutes";*/
			$datetime=$minutes." m";
		}
	}
	
	return $datetime;
}

function timeConvert($time,$type)
{
  $time_hour=substr($time,0,2);
  $time_minute=substr($time,3,2);
  $time_seconds=substr($time,6,2);
  
  if($type == 1):
  	// 12 Hour Format with uppercase AM-PM
  	$time=date("g:i A", mktime($time_hour,$time_minute,$time_seconds));
  elseif($type == 2):
  	// 12 Hour Format with lowercase am-pm
  	$time=date("g:i a", mktime($time_hour,$time_minute,$time_seconds)); 
  elseif($type == 3):
  	// 24 Hour Format
  	$time=date("H:i", mktime($time_hour,$time_minute,$time_seconds)); 
  elseif($type == 4):
  	// Swatch Internet time 000 through 999
  	$time=date("B", mktime($time_hour,$time_minute,$time_seconds)); 
  elseif($type == 5):
  	// 9:30:23 PM
  	$time=date("g:i:s A", mktime($time_hour,$time_minute,$time_seconds));
  elseif($type == 6):
  	// 9:30 PM with timezone, EX: EST, MDT
  	$time=date("g:i A T", mktime($time_hour,$time_minute,$time_seconds));
  elseif($type == 7):
  	// Different to Greenwich(GMT) time in hours
  	$time=date("O", mktime($time_hour,$time_minute,$time_seconds)); 
  endif;
  return $time;
};


function humanToUnix($datestr = '')
{
    if ($datestr == '')
    {
        return FALSE;
    }
    
    $datestr = trim($datestr);
    $datestr = preg_replace("/\040+/", "\040", $datestr);

    if ( ! ereg("^[0-9]{2,4}\-[0-9]{1,2}\-[0-9]{1,2}\040[0-9]{1,2}:[0-9]{1,2}.*$", $datestr))
    {
        return FALSE;
    }

    $split = preg_split("/\040/", $datestr);

    $ex = explode("-", $split['0']);            
    
    $year  = (strlen($ex['0']) == 2) ? '20'.$ex['0'] : $ex['0'];
    $month = (strlen($ex['1']) == 1) ? '0'.$ex['1']  : $ex['1'];
    $day   = (strlen($ex['2']) == 1) ? '0'.$ex['2']  : $ex['2'];

    $ex = explode(":", $split['1']); 
    
    $hour = (strlen($ex['0']) == 1) ? '0'.$ex['0'] : $ex['0'];
    $min  = (strlen($ex['1']) == 1) ? '0'.$ex['1'] : $ex['1'];

    if (isset($ex['2']) AND ereg("[0-9]{1,2}", $ex['2']))
    {
        $sec  = (strlen($ex['2']) == 1) ? '0'.$ex['2'] : $ex['2'];
    }
    else
    {
        // Unless specified, seconds get set to zero.
        $sec = '00';
    }
    
    if (isset($split['2']))
    {
        $ampm = strtolower($split['2']);
      
        if (substr($ampm, 0, 1) == 'p' AND $hour < 12)
            $hour = $hour + 12;
            
        if (substr($ampm, 0, 1) == 'a' AND $hour == 12)
            $hour =  '00';
            
        if (strlen($hour) == 1)
            $hour = '0'.$hour;
    }
            
    return mktime($hour, $min, $sec, $month, $day, $year);
}  

####################################################################
# Function Use : This Function Used For Paging
####################################################################

function frontPaging($pageNumber,$numofrows,$rowsPerPage,$selfurl)
{
	$numrows = $numofrows;
					
	// how many pages we have when using paging?
	$maxPage = ceil($numrows/$rowsPerPage);
	
	if ($pageNumber > 1)
	{
		$page = $pageNumber - 1;
		
		$prev = " <a href=\"$selfurl&page=$page\" class=\"password\"><img src=\"".PAGING_IMAGE."/prev.png\" border=\"0\"></a> ";
		$first = " <a href=\"$selfurl&page=1\" class=\"password\"><img src=\"".PAGING_IMAGE."/first.png\" border=\"0\"></a> ";
	} 
	else
	{
		$prev  = " <img src=\"".PAGING_IMAGE."/next_gray.png\" border=\"0\"> "; // we're on page one, don't enable 'previous' link
		$first = " <img src=\"".PAGING_IMAGE."/first_gray.png\" border=\"0\"> "; // nor 'first page' link
	}
	
	// print 'next' link only if we're not
	// on the last page
	if ($pageNumber < $maxPage)
	{
		$page = $pageNumber + 1;
		
		$next = " <a href=\"$selfurl&page=$page\" class=\"password\"><img src=\"".PAGING_IMAGE."/next.png\" border=\"0\"></a> ";
		$last = " <a href=\"$selfurl&page=$maxPage\" class=\"password\"><img src=\"".PAGING_IMAGE."/last.png\" border=\"0\"></a>";
	} 
	else
	{
		$next = " <img src=\"".PAGING_IMAGE."/prev_gray.png\" border=\"0\"> "; // we're on the last page, don't enable 'next' link
		$last = " <img src=\"".PAGING_IMAGE."/last_gray.png\" border=\"0\"> " ; // nor 'last page' link
	}
	
	if($maxPage != 1 && $maxPage != 0)
	{
	   echo '<table>';
			echo '<tr><td colspan=3 height="5px"></td></tr>';
			echo '<tr>';
				echo '<td align="right" class="browsedcontent" valign="top">'. $first . "<span>" . $prev . "</span></td>" ;
				echo '<td align="right" class="browsedcontent" valign="top" style="padding-top:2px;" >Showing page <strong>'.$pageNumber.'</strong> of <strong>'.$maxPage.'</strong> pages ';
				echo '<td align="right" class="browsedcontent" valign="top"> <span>' . $next . '</span>' . $last."</td>";
			echo '</tr>';
			echo '<tr><td colspan=3 height="5px"></td></tr>';
		echo '</table>';
	}	
}


####################################################################
# Function Use : This Function Used For Paging for Admin side
####################################################################

function adminPaging($pageNumber,$numofrows,$rowsPerPage,$selfurl)
{
	// how many rows we have in database
	$numrows = $numofrows;
					
	// how many pages we have when using paging?
	$maxPage = ceil($numrows/$rowsPerPage);
	
		
	if ($pageNumber > 1)
	{
		$page = $pageNumber - 1;
		
		$prev = " <a href=\"$selfurl&page=$page\" class=\"editlink\"><img src=\"".PAGING_IMAGE."/prev.png\" border=\"0\"></a> ";
		$first = " <a href=\"$selfurl&page=1\" class=\"editlink\"><img src=\"".PAGING_IMAGE."/first.png\" border=\"0\"></a> ";
	} 
	else
	{
		$prev  = " <img src=\"".PAGING_IMAGE."/next_gray.png\" border=\"0\"> "; // we're on page one, don't enable 'previous' link
		$first = " <img src=\"".PAGING_IMAGE."/first_gray.png\" border=\"0\"> "; // nor 'first page' link
	}
	
	// print 'next' link only if we're not
	// on the last page
	if ($pageNumber < $maxPage)
	{
		$page = $pageNumber + 1;
		
		$next = " <a href=\"$selfurl&page=$page\" class=\"editlink\"><img src=\"".PAGING_IMAGE."/next.png\" border=\"0\"></a> ";
		$last = " <a href=\"$selfurl&page=$maxPage\" class=\"editlink\"><img src=\"".PAGING_IMAGE."/last.png\" border=\"0\"></a>";
	} 
	else
	{
		$next = " <img src=\"".PAGING_IMAGE."/prev_gray.png\" border=\"0\"> "; // we're on the last page, don't enable 'next' link
		$last = " <img src=\"".PAGING_IMAGE."/last_gray.png\" border=\"0\"> "; // nor 'last page' link
	}
	
	if($maxPage != 1 && $maxPage != 0)
	{
		echo '<table>';
			echo '<tr><td colspan=3 height="5px"></td></tr>';
			echo '<tr>';
				echo '<td align="right" class="browsedcontent" valign="top">'. $first . "<span>" . $prev . "</span></td>" ;
				echo '<td align="right" class="browsedcontent" valign="top" style="padding-top:2px;" >Showing page <strong>'.$pageNumber.'</strong> of <strong>'.$maxPage.'</strong> pages ';
				echo '<td align="right" class="browsedcontent" valign="top"> <span>' . $next . '</span>' . $last."</td>";
			echo '</tr>';
			echo '<tr><td colspan=3 height="5px"></td></tr>';
		echo '</table>';
	}
}

################################################################
# Function Use : This function used to send mail attachment
################################################################
function mailAttachment($filename, $path, $mailto, $from_mail, $from_name, $replyto, $subject, $message) 
{
   	$file = $path.$filename;
    $file_size = @filesize($file);
    $handle = @fopen($file, "r");
    $content = @fread($handle, $file_size);
    @fclose($handle);
    $content = chunk_split(base64_encode($content));
    $uid = md5(uniqid(time()));
    $name = basename($file);
    $header = "CC: <".$from_mail.">\r\n";
    $header .= "Reply-To: ".$replyto."\r\n";
    $header .= "MIME-Version: 1.0\r\n";
    $header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
    $header .= "This is a multi-part message in MIME format.\r\n";
    $header .= "--".$uid."\r\n";
    $header .= "Content-type:text/html; charset=iso-8859-1\r\n";
    $header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
    $header .= $message."\r\n\r\n";
    $header .= "--".$uid."\r\n";
    $header .= "Content-Type: application/octet-stream; name=\"".$filename."\"\r\n"; // use diff. tyoes here 
    $header .= "Content-Transfer-Encoding: base64\r\n";
    $header .= "Content-Disposition: attachment; filename=\"".$filename."\"\r\n\r\n";
    $header .= $content."\r\n\r\n";
    $header .= "--".$uid."--";
	
    if (@mail($mailto, $subject, "", $header)) 
    {
     return true;
    } 
    else 
    {
     return false;
    }
}


#########################################################
# Function Use : This Function Used to read File
#########################################################
function readValidFile($dirname)
{
	$retval = '';
	
	if(!is_dir($dirname))
	{
		mkdir($dirname);
	}
	if ($dir = @opendir($dirname))
	{
		while (($file = readdir($dir)) !== false)
		{
			if($file!="." && $file!="..")
			{
				if(substr_count($file,".jpg")>0)
				{
					return 	$file;	
				}
				if(substr_count($file,".jpeg")>0)
				{
					return 	$file;	
				}
			}
		}
	}
}


#########################################################
# Function Use : fileUpload() Function Used to upload files
#########################################################
function get_ext($filename)	{
$photo_ext	= explode(".", $filename);
return strtolower($photo_ext[sizeof($photo_ext) - 1]);

}

function fileUpload($tempname,$updfile,$filename)
{
  /*echo  $tempname."--".$updfile."-".$filename;
  exit*/
	if(move_uploaded_file($tempname,$updfile.$filename))
	{
		return true;
	}
	else
	{
		return false;
	}	
}
function generate_random_text($length,$method=1)
{
	$str='';
	for($x=0;$x<$length;$x++)
	{ 
		switch($method){
			case 1:{
			$str.=(chr(mt_rand(65,90)));//Upper Case Characters
			}break; 
			case 2:{
			$str.=chr(mt_rand(48,57));//Numbers 0 - 9
			}break;
		}
	}
	return $str;
}
##################################################
# Function Use : This Function Used to resize and upload images with perticuler hieght and width  Passing the array
##################################################
function ImageResizeUploadHW($updir,$image,$new_name="",$org_prefix="org_",$th_prefix="",$th_width='',$th_height='')
{
	$imgname=$image['name'];
	$imgsize=$image['size'];	
	$imgtmpname=$image['tmp_name'];
	
	list($width, $height, $type, $attr) = getimagesize($imgtmpname);
	
	if($width>=$th_width && $height>=$th_height)
	{
		$neww1= $width;			
		$newh1= $height;
		$neww2= $th_width;
		$newh2= $th_height;
		if($imgsize>0)
		{
			if($imgsize>33554432)
			{
				$stop=_MAPER;
				$ero=1;
				return 1;
			}
			else
			{
				if(($type==1) || ($type==2) || ($type==3))
				{ 
					#########################################################################
					if($type==1)
					{
						$srcImg = imagecreatefromgif($imgtmpname);
						$typeImg=".gif";
					}	
					if($type==2) 
					{
						$srcImg = imagecreatefromjpeg($imgtmpname); 
						$typeImg=".jpg";
					}
					if($type==3) 
					{
						$srcImg = imagecreatefrompng($imgtmpname);
						$typeImg=".png";
					}
					#########################################################################
					/*$dstImg1 = imagecreatetruecolor($neww1, $newh1);*/
					$dstImg2 = imagecreatetruecolor($neww2, $newh2);
					/*imagecopyresampled($dstImg1, $srcImg, 0, 0, 0, 0,$neww1, $newh1, $width,$height);*/
					imagecopyresampled($dstImg2, $srcImg, 0, 0, 0, 0,$neww2, $newh2,$width,$height);
					/*if(empty($new_name))
					{
						$new_name=makecode(1);
					}
					$_SESSION['number']['formate'] = $new_name;*/
					
					$org_name=$org_prefix.$imgname;
					$th_name=$th_prefix.$imgname;
					#########################################################################	
					if($type==1 || $type==2)
					{	
						/*imagejpeg($dstImg1,$updir.$org_name.$typeImg);*/
						imagejpeg($dstImg2,$updir.$th_name);
					}
					if($type==3) 
					{
						/*imagepng($dstImg1,$updir.$org_name.$typeImg);*/
						imagepng($dstImg2,$updir.$th_name);
					}		
					#########################################################################
					/*imagedestroy($dstImg1);*/
					imagedestroy($dstImg2);
					
					/*$numefisier=$org_name.$typeImg;*/ //
					//$numefisier=$new_name.$typeImg;
					
					#########################################################################
				}
				else
				{
					$stop="invalid file format!";
					$ero=1;
					return 1;
				}
		    }
			return $imgname;
		}
	}
	else
	{
		$stop="image height and width less then thumb height and width";
		$ero=1;
		return 1;
	}
}
##################################################
# Function Use : This Function Used to upload images,audio and vedio.
##################################################
function uploadIVA($updir,$fileinfo)
{
	$fileName=clean($fileinfo['name']);
    $fileSize=$fileinfo['size'];
	$FileTempName=$fileinfo['tmp_name'];
	$fileNameExt = explode(".", $fileName);
	$fileExt 	 = $fileNameExt[count($fileNameExt)-1];
	$allowedExts = array("gif","jpg","jpeg","png","GIF","JPG","JPEG","PNG");
	if($fileSize>0)
	{
		if($fileSize>33554432)
		{
			$stop="Please upload file less then 32MB file size!";
			return "size_err";
		}
		else
		{
			if(in_array($fileExt,$allowedExts))
			{
				//$newname= makecode(1).".".$fileExt;
				move_uploaded_file($FileTempName,$updir.$fileName);
			}
			else
			{
				$stop="Invalid file type..!";
				return "type_err";
			}
	    }
		return $fileName;
	}
}

function uploadIVAWithVideo($updir,$fileinfo)
{
    $fileName=clean($fileinfo['name']);
    $fileSize=$fileinfo['size'];
    $FileTempName=$fileinfo['tmp_name'];
    $fileNameExt = explode(".", $fileName);
    $fileExt 	 = $fileNameExt[count($fileNameExt)-1];
    $allowedExts = array("gif","jpg","jpeg","png","GIF","JPG","JPEG","PNG","wma","WMA","wav","WAV","mp3","MP3","MP4","mp4","m3u","M3U","M4A","m4a","mpa","MPA","3G2","3g2","3gp","3GP","M4V","m4v","MPG","mpg","swf","SWF","VOB","vob","wmv","WMV");
    if($fileSize>0) {
        if($fileSize>33554432) {
            $stop="Please upload file less then 32MB file size!";
            return "size_err";
        } else {
            if(in_array($fileExt,$allowedExts)) {
                //$newname= makecode(1).".".$fileExt;
                move_uploaded_file($FileTempName,$updir.$fileName);
            } else {
                $stop="Invalid file type..!";
                return "type_err";
            }
        }
        return $fileName;
    }
}

function clean($string, $force_lowercase = true, $anal = false) {
    $strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
        "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
        "â€”", "â€“", ",", "<", ">", "/", "?");
    $clean = trim(str_replace($strip, "", strip_tags($string)));
    $clean = preg_replace('/\s+/', "-", $clean);
    $clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean ;
    return ($force_lowercase) ?
        (function_exists('mb_strtolower')) ?
            mb_strtolower($clean, 'UTF-8') :
            strtolower($clean) :
        $clean;
}

function uploadVideo($updir,$fileinfo)
{
	$fileName=$fileinfo['name'];
	$fileSize=$fileinfo['size'];	
	$FileTempName=$fileinfo['tmp_name'];
	$fileNameExt = explode(".", $fileName);
	$fileExt 	 = $fileNameExt[count($fileNameExt)-1];
	$allowedExts = array("wma","WMA","wav","WAV","mp3","MP3","MP4","mp4","m3u","M3U","M4A","m4a","mpa","MPA","3G2","3g2","3gp","3GP","asf","ASF","AVI","avi","flv","FLV","M4V","m4v","mov","MOV","MPG","mpg","swf","SWF","VOB","vob","wmv","WMV");
	if($fileSize>0)
	{
		if($fileSize>33554432)
		{
			$stop="Please upload file less than 32MB file size!";
			return $stop;
		}
		else
		{
			if(in_array($fileExt,$allowedExts))
			{
				//$newname= makecode(1).".".$fileExt;
				move_uploaded_file($FileTempName,$updir.$fileName);
			}
			else
			{
				$stop="Please select only videos and audio file.";
				return $stop;
			}
	    }
		return $fileName;
	}
}
##################################################
# Function Use : This Function Used to resize and upload images Passing the array
##################################################

function oneImageResizeUpload($updir,$image,$new_name="",$org_prefix="org_",$th_prefix="",$th_width='256',$th_height='256')
{
	$imgname=$image['name'];
	$imgsize=$image['size'];	
	$imgtmpname=$image['tmp_name'];
	
	list($width, $height, $type, $attr) = getimagesize($imgtmpname);
	
	$neww1= $width;			
	$newh1= $height;
	$neww2= $th_width;
	$newh2= $th_height;
	if($imgsize>0)
	{
		if($imgsize>33554432)
		{
			$stop=_MAPER;
			$ero=1;
			return 1;
		}
		else
		{
			if(($type==1) || ($type==2) || ($type==3))
			{ 
				#########################################################################
				if($type==1)
				{
					$srcImg = imagecreatefromgif($imgtmpname);
					$typeImg=".gif";
				}	
				if($type==2) 
				{
					$srcImg = imagecreatefromjpeg($imgtmpname); 
					$typeImg=".jpg";
				}
				if($type==3) 
				{
					$srcImg = imagecreatefrompng($imgtmpname);
					$typeImg=".png";
				}
				#########################################################################
				$dstImg2 = imagecreatetruecolor($neww2, $newh2);
				
				imagecopyresampled($dstImg2, $srcImg, 0, 0, 0, 0,$neww2, $newh2,$width,$height);
				/*if(empty($new_name))
				{
					$new_name=makecode(1);
				}
				$_SESSION['number']['formate'] = $new_name;*/
				
				$org_name=$org_prefix.$imgname;
				$th_name=$th_prefix.$imgname;
				#########################################################################	
				if($type==1 || $type==2)
				{	
					imagejpeg($dstImg2,$updir.$th_name);
				}
				if($type==3) 
				{
					imagepng($dstImg2,$updir.$th_name);
				}		
				
				imagedestroy($dstImg2);
			}
			else
			{
				$stop="invalid file format!";
				$ero=1;
				return 1;
			}
	    }
		return $imgname;
	}
}

function oneImageResizeUpload2($tt="",$updir,$image,$new_name="",$org_prefix="org_",$th_prefix="",$th_size='256',$reqWidth='900',$reqHeight='450')
{
	//$reqWidth='900';
	//$reqHeight='450';
	//$th_size='120';
		
	for($i=0;$i<count($image)-1;$i++)
	{	
	$imgname=$image['name'][$tt];
	$imgsize=$image['size'][$tt];	
	$imgtmpname=$image['tmp_name'][$tt];
	
	if($imgsize>0)
	{
		list($width, $height, $type, $attr) = getimagesize($imgtmpname);
		
		$neww1=($width>$reqWidth) ? $reqWidth : $width;			
		$newh1=($height>$reqHeight) ? $reqHeight : $height;
		
		$neww2=($width>$th_size) ? $th_size : $width;
		$newh2=($height>$th_size) ? $th_size : $height;	
				
		if($imgsize>300000000)
		{
			$stop=_MAPER;
			$ero=1;
			return 1;
		}
		else
		{
			#########################################################################
			$ratio1 = max(($height / $newh1),($width / $neww1));
			$newh1 = @round($height / $ratio1);
			$neww1 = @round($width / $ratio1);
			
			if($width>$height)
			{								
				$ratio2 = max(($height / $newh2),($width / $neww2));
			}
			else if($width==$height)
			{
				$ratio2 = @($height / $newh2);
				if($ratio1==1)
				{	
					$ratio1 = @($height / $newh1);
				}
			}
			else
			{
				$ratio2 = @($height / $newh2);
				if($ratio1==1)
				{
					$ratio1 = @($width / $neww1);
				}
			} 
			
			$neww2 = @round($width / $ratio2);
			$newh2 = @round($height / $ratio2);
			#########################################################################
			
			if(($type==1) || ($type==2) || ($type==3))
			{ 
				#########################################################################
				if($type==1)
				{
					$srcImg = imagecreatefromgif($imgtmpname);
					$typeImg=".gif";
				}	
				if($type==2) 
				{
					$srcImg = imagecreatefromjpeg($imgtmpname); 
					$typeImg=".jpg";
				}
				if($type==3) 
				{
					$srcImg = imagecreatefrompng($imgtmpname);
					$typeImg=".png";
				}
				#########################################################################
				$dstImg1 = imagecreatetruecolor($neww1, $newh1);
				$dstImg2 = imagecreatetruecolor($neww2, $newh2);
				imagecopyresampled($dstImg1, $srcImg, 0, 0, 0, 0,$neww1, $newh1, $width,$height);
				imagecopyresampled($dstImg2, $srcImg, 0, 0, 0, 0,$neww2, $newh2,$width, $height);
				if(empty($new_name))
				{
					$new_name=makecode(1);
				}
				$_SESSION['number']['formate'] = $new_name;
				
				$org_name=$org_prefix.$new_name;
				$th_name=$th_prefix.$new_name;
				#########################################################################	
				if($type==1 || $type==2)
				{	
					imagejpeg($dstImg1,$updir.$org_name.$typeImg);
					imagejpeg($dstImg2,$updir.$th_name.$typeImg);
				}
				if($type==3) 
				{
					imagepng($dstImg1,$updir.$org_name.$typeImg);
					imagepng($dstImg2,$updir.$th_name.$typeImg);
				}		
				#########################################################################
				imagedestroy($srcImg);
				imagedestroy($dstImg1);
				imagedestroy($dstImg2);
				
				/*$numefisier=$org_name.$typeImg;*/
				$numefisier=$new_name.$typeImg;
				#########################################################################
			}
			else
			{
				$stop="invalid file format!";
				$ero=1;
				return 1;
			}
	    }
	}
	}//if file size>0
	return $numefisier;
}

function imgthumbpng($imgarray,$imagename,$h,$w){
   
    $imgname = explode(".",$imgarray['name']);
                    
    list($width, $height, $type, $attr) = @getimagesize($imgarray['tmp_name']);

    $w1=($width>$w) ? $w : $width;
    $h1=($height>$h) ? $h : $height;
	
    
	 
    $newh1=$h1;
    $neww1=$w1;
	
    
	
            if($imgarray['size']>300000000)
            {                                    
                 $stop=_MAPER;
                 $ero=1;
				 return 0;
            }
            else
            {
                 
				  /*if($width>=$height)
                    {
                        $ratio1 = @($width / $neww1);
                        $newh1 = @round($height / $ratio1);
                        
                        
                    }
                    else
                    {
						$ratio1 = @($height / $newh1);
                        $neww1 = @round($width / $ratio1);
                       
						
                    }*/
                    
                    if(($type==1) || ($type==2) || ($type==6) || ($type == 3))
                    { 
                            if($type==1)
                            {
							
                                    $srcImg = imagecreatefromgif($imgarray['tmp_name']);
                                    $scale1=1.2;
                                    $scale2=3;
                                    $dstImg1 = @imagecreatetruecolor($neww1, $newh1);
                                    /*$dstImg2 = imagecreatetruecolor($neww2, $newh2);    
									$dstImg3 = imagecreatetruecolor($neww3, $newh3);   
									$dstImg4 = imagecreatetruecolor($neww4, $newh4); */   
                                    imagecopyresampled($dstImg1, $srcImg, 0, 0, 0, 0,$neww1, $newh1, $width,$height);
                                    /*imagecopyresampled($dstImg2, $srcImg, 0, 0, 0, 0,$neww2, $newh2,$width, $height);
									imagecopyresampled($dstImg3, $srcImg, 0, 0, 0, 0,$neww3, $newh3,$width, $height);
									imagecopyresampled($dstImg4, $srcImg, 0, 0, 0, 0,$neww4, $newh4,$width, $height);*/
                                    $nume=$imgname[0];
                                    $nume2=$nume;

                                    
									
									imagejpeg($dstImg1,$imagename); 
                                    
                                    //  imagedestroy($srcImg);
                                    imagedestroy($dstImg1);
                                    /*imagedestroy($dstImg2);
									imagedestroy($dstImg3);
									imagedestroy($dstImg4);*/
                                     $numefisier[$i]=$nume.".gif";
                                     $path = $uploaddir.$numefisier[$i];
                            }
                            if($type==2 || ($type==6))
                            {      
                                    $srcImg = imagecreatefromjpeg($imgarray['tmp_name']);
                                   
								   							   
                                    $scale1=1.2;
                                    $scale2=3;
                                    $dstImg1 = @imagecreatetruecolor($neww1, $newh1);
                                    /*$dstImg2 = imagecreatetruecolor($neww2, $newh2); 
									 $dstImg3 = imagecreatetruecolor($neww3, $newh3);
									 $dstImg4 = imagecreatetruecolor($neww4, $newh4); */                                         
									
									imagecopyresampled($dstImg1, $srcImg, 0, 0, 0, 0,$neww1, $newh1, $width,$height);
                                    /*imagecopyresampled($dstImg2, $srcImg, 0, 0, 0, 0,$neww2, $newh2,$width, $height);
									imagecopyresampled($dstImg3, $srcImg, 0, 0, 0, 0,$neww3, $newh3,$width, $height);
									imagecopyresampled($dstImg4, $srcImg, 0, 0, 0, 0,$neww4, $newh4,$width, $height);*/
									
                                    $nume=$imgname[0];
									$nume2=$nume;
                                    //imagejpeg($dstImg1,$updir.$nume.".jpg");
									

					
					
					
                                    imagejpeg($dstImg1,$imagename); 

                                    imagedestroy($srcImg);
                                    imagedestroy($dstImg1);

                                    $numefisier[$i]=$nume.".jpg";
									
									
                                    $path = $uploaddir.$numefisier[$i];
					
									
                            }
							
							if($type==3) { $img_name = explode(".",$imagename);
							
							
                               	$srcImg = imagecreatefrompng($imgarray['tmp_name']);
								
								
                                    $scale1=1.2;
                                    $scale2=3;
                                    $dstImg1 = @imagecreatetruecolor($neww1, $newh1);
                                    /*$dstImg2 = imagecreatetruecolor($neww2, $newh2);    
									$dstImg3 = imagecreatetruecolor($neww3, $newh3);   
									$dstImg4 = imagecreatetruecolor($neww4, $newh4);    */
									
									
									imagealphablending($dstImg1, false);
								
							    imagesavealpha($dstImg1,true);
								
								
							    $transparent = imagecolorallocatealpha($dstImg1, 255, 255, 255, 127);
							
							    imagefilledrectangle($dstImg1, 0, 0, $neww1, $newh1, $transparent);
								
								//by nisha - png transparent background 
								imagecopyresampled($dstImg1, $srcImg, 0, 0, 0, 0,$neww1, $newh1, $width,$height);
							//		
                                    imagecopyresampled($dstImg1, $srcImg, 0, 0, 0, 0,$neww1, $newh1, $width,$height);
									
                                    $nume=$imgname[0];
                                    $nume2=$nume;

                                    
									
									imagepng($dstImg1,$imagename); 
									//imagepng(imagecreatefrompng("./img/".$slide_game['nameid'].".png"),"./img/".$slide_game['nameid'].".jpg");
									
									$img_name = explode(".",$imagename);
									imagejpeg(imagecreatefrompng($imagename),$img_name[0].".jpg");
                                    unlink($imagename);
                                   // imagedestroy($srcImg);
                                    imagedestroy($dstImg1);
                                    /*imagedestroy($dstImg2);
									imagedestroy($dstImg3);
									imagedestroy($dstImg4);*/
                                     $numefisier[$i]=$nume.".png";
                                     $path = $uploaddir.$numefisier[$i];
                            }
							return 1;
                    }
                    else
                    {
                    $stop="invalid file format!";
                    $ero=1;
					return 0;
                    }
            }//if file size > 300000
    } 

#################################################################
### Function Use : This function Used to create the status Combo.
#################################################################

function getDynamicCombo($name,$array,$key,$value,$selected,$style,$selectone,$value2="")
{
	$cmbstring="<SELECT name='".$name."' class = '".$style."' disabled=disabled>";
	if($selectone=='1')
	{
		$cmbstring.="<option value='' >Select One...</option> ";
	}
	for($i=0;$i<count($array);$i++)
	{
		if($selected==$array[$i][$key])
		{
			if(!empty($value2))
			{
				$cmbstring.="<OPTION value='".$array[$i][$key]."' selected>(".$array[$i][$value].") - ".$array[$i][$value2]."</OPTION>" ;	
			}
			else
			{
				$cmbstring.="<OPTION value='".$array[$i][$key]."' selected>".$array[$i][$value]."</OPTION>" ;
			}
		}
		else
		{
			if(!empty($value2))
			{
				$cmbstring.="<OPTION value='".$array[$i][$key]."' >(".$array[$i][$value].") - ".$array[$i][$value2]."</OPTION>" ;	
			}else{
				$cmbstring.="<OPTION value='".$array[$i][$key]."' >".$array[$i][$value]."</OPTION>" ;
			}

		}
	}
	echo $cmbstring.="</SELECT>";
}


function getDynamicGroupCombo($name,$array,$key,$value,$selected,$style="",$selectone="",$value2="",$selectall="")
{
	$cmbstring="<SELECT name='".$name."' ".$style.">";
	if($selectone=='1')
	{
		$cmbstring.="<option value='' >Select One...</option> ";
	}
	for($i=0;$i<count($array);$i++)
	{
		if($selected==$array[$i][$key])
		{
			if(!empty($value2))
			{
				$cmbstring.="<OPTION value='".$array[$i][$key]."' selected>(".$array[$i][$value].") - ".$array[$i][$value2]."</OPTION>" ;	
			}
			else
			{
				$cmbstring.="<OPTION value='".$array[$i][$key]."' selected>".$array[$i][$value]."</OPTION>" ;
			}
		}
		else
		{
			if(!empty($value2))
			{
				$cmbstring.="<OPTION value='".$array[$i][$key]."' >(".$array[$i][$value].") - ".$array[$i][$value2]."</OPTION>" ;	
			}
			else
			{
				$cmbstring.="<OPTION value='".$array[$i][$key]."' >".$array[$i][$value]."</OPTION>" ;
			}

		}
	}
	if($selectall=='1')
	{
		if($selected=="View all")
		{
			$cmbstring.="<option value='View all' selected >View all</option> ";
		}
		else 
		{
			$cmbstring.="<option value='View all' >View all</option> ";
		}
	}
	echo $cmbstring.="</SELECT>";
}

function getDynamicStatusCombo($name,$selected,$style="",$selectone="")
{
	$cmbstring="<SELECT name='".$name."' ".$style.">";
	if($selected=="Active")
		$cmbstring.="<option value='Active' id='Active' selected>Active</option>";
	else 
		$cmbstring.="<option value='Active' id='Active'>Active</option>";

	if($selected=="Inactive")
		$cmbstring.="<option value='Inactive' id='Inactive' selected>Inactive</option>";
	else 
		$cmbstring.="<option value='Inactive' id='Inactive'>Inactive</option>";

	echo $cmbstring.="</SELECT>";
}

##################################################
# Function Use : This function used to Create the Page size
##################################################

function pageSizeCombo($name,$selected,$style='')
{
	$cob.="	<select name='".$name."' ".$style.">";
	
	if($selected==5)
		$cob.=	"<option value='5' selected>5 / pg</option>";
	else
		$cob.=	"<option value='5'>5 / pg</option>";

	if($selected==10)
		$cob.=	"<option value='10' selected>10 / pg</option>";
	else
		$cob.=	"<option value='10'>10 / pg</option>";

	if($selected==25)
		$cob.=	"<option value='25' selected>25 / pg</option>";
	else
		$cob.=	"<option value='25'>25 / pg</option>";

	if($selected==50)
		$cob.=	"<option value='50' selected>50 / pg</option>";
	else
		$cob.=	"<option value='50'>50 / pg</option>";
		
	if($selected=='0')
		$cob.=	"<option value='0' selected>View all</option>";
	else
		$cob.=	"<option value='0'>View all</option>";

		$cob.=	"</select>";
	return $cob;
}

function getPrefixCombo($select_name,$select_id,$selected_prefix="",$style="",$extra="")
{
	$cmbstring="";
	
	$cmbstring.="<select name='".$select_name."' id='".$select_id."' ".$style." ".$extra.">";
	
	if($selected_prefix=='Ms.')
	{
		$cmbstring.="<option value='Ms.' Selected>Ms.</option>";
	}
	else
	{
		$cmbstring.="<option value='Ms.'>Ms.</option>";
	}
	
	if($selected_prefix=='Mr.')
	{
		$cmbstring.="<option value='Mr.' Selected>Mr.</option>";
	}
	else
	{
		$cmbstring.="<option value='Mr.'>Mr.</option>";
	}
	
	if($selected_prefix=='Mrs.')
	{
		$cmbstring.="<option value='Mrs.' Selected>Mrs.</option>";
	}
	else
	{
		$cmbstring.="<option value='Mrs.'>Mrs.</option>";
	}
	
	if($selected_prefix=='Miss')
	{
		$cmbstring.="<option value='Miss' Selected>Miss</option>";
	}
	else
	{
		$cmbstring.="<option value='Miss'>Miss</option>";
	}
	
	$cmbstring.="</select>";
	
	return $cmbstring;
}

function username_password($len)
{
$value='';
$chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
$length = $len;

$max_i = strlen($chars)-1;
$value = '';
	for ($i=0;$i<$length;$i++)
	{
	$value .= $chars{mt_rand(0,$max_i)};
	}
return $value;
}

//Mail Function ...

/*function mailfunction($toemail,$fromemail,$subject,$messagearea)
{						
			$message = '<html xmlns="http://www.w3.org/1999/xhtml">
						<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
						<meta http-equiv="Content-Type" content="text/html; charset=UTF-8;">
						<title>Nerta Country Site</title>
						</head>		
						<body style="margin:0; padding:0;" >
						<table width="100%" border="0" cellspacing="0" height="100%" cellpadding="0" align="center"  style="font-family:Arial, Helvetica, sans-serif;" >
  						<tr>
					    <td>
					    <table width="760" border="0"  align="center" cellpadding="20" cellspacing="0" bgcolor="#FFF">
        				<tr>
				          <td>
						  	<img src="'.ROOT_WWW.'images/logo.jpg" />
						  </td>
        				 </tr>
        				 <tr>
				          <td align="left" valign="top" bgcolor="#FFF">
						  	'.$messagearea.'
            			</td>
        				</tr>
         			<tr><td align=left>Best regards,</td></tr>
         			<tr><td align=left><b>Vinix</b></td></tr>
         			<tr><td align=left>The creative marketing and communication agency</td></tr>
         			<tr><td align=left>http://www.vinixconsulting.com</td></tr>
      				</table>
	  				</td>
  					</tr>
					</table>        
					</body>
					</html>'; 
					$message = $messagearea;
										
			if (!class_exists('PHPMailer'))
					require_once("class.phpmailer.php"); 
					
				$mail = new PHPMailer(false);
				$mail->MailerDebug = false;
				$mail->SMTPDebug = false;

				if($custom_host_name!="" && $custom_port_name!="") 
				{
					$mail->IsSMTP();
					$mail->Host = $custom_host_name;
					$mail->SMTPAuth = false;
					$mail->Username = $custom_user_name;
					$mail->Password = $custom_password;
					$mail->Port = $custom_port_name;
				}

				$from_name = $fromemail;
				$mail->From = $fromemail;
				$mail->FromName = $fromemail;
				$mail->Subject = $subject;
				$mail->CharSet = 'utf-8';
				$mail->MsgHTML(stripslashes($message));
				
			  $mail->AddAddress($toemail);
			  $mail->Send();
			  $mail->ClearAddresses();
}*/

function mailfunction($toemail,$fromemail,$subject,$messagearea)
{
	$headers = "MIME-Version: 1.0\n";
	$headers .= "Content-type: text/html; charset=UTF-8\n";
	$headers .= "Content-Transfer-Encoding: 8bit\n";
	$headers .= "From: ".$fromemail."\n";
	//$headers .= "Cc: ".$cc."\r\n";
	//$headers .= "Bcc: ".$bcc."\r\n";
	$headers .= "X-Priority: 1\n";
	$headers .= "X-MSMail-Priority: High\n";
	$headers .= "X-Mailer: PHP/" . phpversion()."\n";
	if(@mail($toemail, $subject, $messagearea, $headers))
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

function getOrderId($SECTION_TABLE,$SECTION_FIELD_PREFIX)
{
	global $db;
	$select				=	"select max(".$SECTION_FIELD_PREFIX."sort_order) as sort from ".$SECTION_TABLE;	
	$res				=	$db->select($select);
	$sortorder			=	intval($res[0]['sort']) + 1;
	return $sortorder;
}


function charConversion($string, $to = "HTML-ENTITIES", $from = 'UTF-8,ASCII,ISO-8859-9,ISO-8859-1') {
    $str = mb_convert_encoding($string, $to, $from);
    $str = stripslashes($str);
    if (empty($str)) {
        return $string;
    }
    return $str;
}

/*function mailfunction($toemail,$fromemail,$subject,$messagearea)
{						
	$message	= "<html xmlns='http://www.w3.org/1999/xhtml'>";
	$message	.= "<head><meta content='text/html; charset=iso-8859-1' http-equiv='Content-Type'>
					<title>Bjorn's Carrefour Express Parkwijk Turnhout</title>
					<link href='css/style.css' rel='stylesheet' type='text/css' />
					</head>";		
	$message 	.= "<body>
					<div style='background-image:url(".ROOT_WWW."images/header_bg.jpg); background-repeat: no-repeat;'>
						<div class='header-section'>
							<div class='logo' style='padding-left:50px;'> <a href='".ROOT_WWW."home/' style='text-decoration:none;'>
								<img width='244' height='93' border='0' alt='Logo' src='".ROOT_WWW."images/logo_2.png'> </a> 
							</div>
						</div>
						<div style='font-family:century gothic;margin-top:50px;padding-left:50px;'>
							".$messagearea."
							<div>Mvg,<br/>Björn's Express Parkwijk <br>
							</div>
					   </div>
					</div>
				 </body>
				</html>";
			if (!class_exists('PHPMailer'))
					require_once("class.phpmailer.php"); 
					
				$mail = new PHPMailer(false);
				$mail->MailerDebug = false;
				$mail->SMTPDebug = false;

				if($custom_host_name!="" && $custom_port_name!="") 
				{
					$mail->IsSMTP();
					$mail->Host = $custom_host_name;
					$mail->SMTPAuth = false;
					$mail->Username = $custom_user_name;
					$mail->Password = $custom_password;
					$mail->Port = $custom_port_name;
				}

				$from_name = $fromemail;
				$mail->From = $fromemail;
				$mail->FromName = $fromemail;
				$mail->Subject = $subject;
				$mail->CharSet = 'utf-8';
				$mail->MsgHTML(stripslashes($message));
				
			  $mail->AddAddress($toemail);
			  $mail->Send();
			  $mail->ClearAddresses();
}*/

function validateURL($URL) {
      $pattern_1 = "/^(http|https|ftp):\/\/(([A-Z0-9][A-Z0-9_-]*)(\.[A-Z0-9][A-Z0-9_-]*)+.(com|org|net|dk|at|us|tv|info|uk|co.uk|biz|se)$)(:(\d+))?\/?/i";
      $pattern_2 = "/^(www)((\.[A-Z0-9][A-Z0-9_-]*)+.(com|org|net|dk|at|us|tv|info|uk|co.uk|biz|se)$)(:(\d+))?\/?/i";       
      if(preg_match($pattern_1, $URL) || preg_match($pattern_2, $URL)){
        return true;
      } else{
        return false;
      }
    }
    

function getIndustry($id)
{
	global $db;
	 $query="select ind_name from tbl_industry where ind_id='".$id."' ";
	$data = $db->select($query);
	$industry_name = charConversion($data[0]['ind_name']);
	
if($industry_name){
	 return $industry_name;
	}else
	{
		return "---";
	}
}

function getIndustry1($id)
{
	global $db;
	 $query="select ind_name from tbl_industry where ind_id='".$id."' ";
	$data = $db->select($query);
	$industry_name = special_chars($data[0]['ind_name']);
	
		//$industry_name = preg_replace('/[^a-zA-Z0-9_. -]/s', '-',$industry_name);
	//$industry_name = preg_replace('/\s+/', '-', $industry_name);
if($industry_name){
	 return $industry_name;
	}else
	{
		return "---";
	}
}

function getCat($id)
{
	global $db;
	 $query="select icat_category_name from tbl_industry_category where icat_id='".$id."' ";
	$data = $db->select($query);
	$cat_name = charConversion($data[0]['icat_category_name']);
	
	if($cat_name){
	return $cat_name;
	}else
	{
		return "---";
	}
}

 /*To show multiple category in view products section */

function getMultiCat($id)
{
	global $db;
	$query="select icat_category_name from tbl_industry_category where icat_id IN($id) ";
	$data = $db->select($query);
	$total_rows = count($data);   
		
	for($i=0;$i<$total_rows;$i++){
	if($i>0){ $comma = ",<br/> ";}else{$comma="";}
	$cat_name .= $comma.charConversion($data[$i]['icat_category_name']);

	}
	if($cat_name){
	return $cat_name;
	}else
	{
		return "---";
	}
}


function getCat1($id)
{
	global $db;
	 $query="select icat_category_name_url from tbl_industry_category where icat_id='".$id."' ";
	$data = $db->select($query);
	$cat_name = special_chars($data[0]['icat_category_name_url']);
//$cat_name = preg_replace('/[^a-zA-Z0-9_. -]/s', '-',$cat_name);
//$cat_name = preg_replace('/\s+/', '-', $cat_name);
	
	if($cat_name){
	return $cat_name;
	}else
	{
		return "---";
	}
}

function getcatid($id)
{
	global $db;
	// $query="select icat_category_name,icat_id from tbl_industry_category where  REPLACE(icat_category_name,' ','-' )='".$id."' ";
	$query="select icat_category_name_url,icat_id from tbl_industry_category where icat_category_name_url='".$id."' ";
	$data = $db->select($query);
	$cat_name = charConversion($data[0]['icat_category_name_url']);
	$cat_id = $data[0]['icat_id'];

//$cat_name = preg_replace('/[^a-zA-Z0-9_. -]/s', '-',$cat_name);
//$cat_name = preg_replace('/\s+/', '-', $cat_name);
	
	if($cat_name){
	return $cat_id;
	}else
	{
		return '';
	}
}


function getPurpose($id)
{
	global $db;
	 $query="select pur_purpose from tbl_purpose where pur_id='".$id."'";
	$data = $db->select($query);
	$pur_name = charConversion($data[0]['pur_purpose']);
	if($pur_name){
	return $pur_name;
	}else
	{
		return "---";
	}
}

function checkdelInd($id)
{
	global $db;
	 $query="select icat_ind_id from tbl_industry_category where icat_ind_id='".$id."'";
	$data = $db->select($query);
	$count = count($data);
	if($count > 0){
	return false;
	}else
	{
		return true;
	}
}

function checkdelCat($id)
{
	global $db;
	 $query="select pur_cat_id from tbl_purpose where pur_cat_id='".$id."'";
	$data = $db->select($query);
	$count = count($data);
	if($count > 0){
	return false;
	}else
	{
		return true;
	}
}

function checkdelPur($id)
{
	global $db;
	 $query="select pro_pur_id from tbl_products where pro_pur_id='".$id."'";
	$data = $db->select($query);
$count = count($data);
	if($count > 0){
	return false;
	}else
	{
		return true;
	}
}


function productstatus($id)
{
	global $db;
	 $query="select pro_cat_id,pro_ind_id from tbl_products where pro_id='".$id."'";
	$data = $db->select($query);
	$ind_id = $data[0]['pro_ind_id'];
	$cat_id = $data[0]['pro_cat_id'];
	$ind = "select ind_status from tbl_industry where ind_id='".$ind_id."'";
	$data1 = $db->select($ind);
	 $ind_status = $data1[0]['ind_status'];
	$cat = "select icat_status from tbl_industry_category where icat_id='".$cat_id."'";
	$data2 = $db->select($cat);
	$cat_status = $data2[0]['icat_status'];
	if($ind_status =="Active" && $cat_status == "Active"){
	return true;
	}else
	{
		return false;
	}
}

function newscat($id)
{
	global $db;
	$query="select nws_nct_id from tbl_news	 where nws_id='".$id."'";
	$data = $db->select($query);
	$cat_id = $data[0]['nws_nct_id'];
	
	 $ind = "select nct_name from tbl_news_category where nct_id='".$cat_id."'";
	$data1 = $db->select($ind);
	$catname = charConversion($data1[0]['nct_name']);
	return $catname;
}

function newscat1($id)
{
	global $db;
		
	 $ind = "select nct_name from tbl_news_category where nct_id='".$id."'";
	$data1 = $db->select($ind);
	$catname = $data1[0]['nct_name'];
	return charConversion($catname);
}

//this pagination diplay in fron side same as admin side
function pagingPageNumber($filename,$pages,$page)
{
	
	
	if($pages>1)
	{
		$location=$filename;	
		
		if($page!='1')
		{
			$t=1;
			echo '<a href="'.ROOT_WWW."nl/".$location=$filename."/page/".$t.'" class="'.$class.' pg"><b class="">First</b></a>';
			$start=$page;
			$page1=$page-1;
			echo '<a href="'.ROOT_WWW."nl/".$location=$filename."/page/".$page1.'" class="'.$class.' pg"><b class="">Previous</b></a>';
		}
		
	
		if($page>=$pages)
		{
			$t1=$pages;
		}
		else
		{
			$t1=$page+4;     
		}
		if($t1>=$pages)
		{
			$t1=$pages;
		}
			
	  	for($t=$page; $t<=$t1; $t++)
		{
			if($page == $t)
				$class = 'active';
			else	
				$class = '';
      		echo '<a href="'.ROOT_WWW."fr/".$location=$filename."/page/".$t.'" class="'.$class.' pg"><b class="">'.$t.'</b></a>';
		}
		if($page!=$pages)
		{
			$next=$page+1;
			echo '<a href="'.ROOT_WWW."fr/".$location=$filename."/page/".$next.'" class="'.$class.' pg"><b class="">Next</b></a>';
			echo '<a href="'.ROOT_WWW."fr/".$location=$filename."/page/".$pages.'" class="'.$class.' pg"><b class="">Last</b></a>';
		
		}
		
	
	}
}

function showinputtext($text) {
	return $decoded_text = htmlentities(stripslashes($text));
	} 

function newscategorystatus($id)
{
	global $db;
	$query="select nws_id,nws_nct_id from tbl_news where nws_nct_id='".$id."'"; 
	$data = $db->select($query);
	$count = count($data); 
	
	if($count>0){
	return true;
	}else
	{
		return false;
	}
}

function CurrentPageURL()
{
$pageURL = $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
$pageURL .= $_SERVER['SERVER_PORT'] != '80' ? $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"] : $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
return $pageURL;
}


function special_chars($str)
    { 	$str= remove_accents($str);
        $str = htmlentities($str, ENT_COMPAT, 'iso-8859-1');
        $str = preg_replace('/&(.)(acute|cedil|circ|lig|grave|ring|tilde|uml);/', "$1", $str);
        $str = preg_replace("/&#?[a-z0-9]+;/i","",$str);
        $str = preg_replace("/[^ -\w]+/","",$str);
 		$str = preg_replace("/[\/_|+ -]+/", '-', $str);	
			
        return strtolower($str);
    } 


/**
 * Unaccent the input string string. An example string like `ÀØėÿᾜὨζὅБю`
 * will be translated to `AOeyIOzoBY`. More complete than :
 *   strtr( (string)$str,
 *          "ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ",
 *          "aaaaaaaaaaaaooooooooooooeeeeeeeecciiiiiiiiuuuuuuuuynn" );
 *
 * @param $str input string
 * @param $utf8 if null, function will detect input string encoding
 * @author http://www.evaisse.net/2008/php-translit-remove-accent-unaccent-21001
 * @return string input string without accent
 */
function remove_accents( $str, $utf8=true )
{
    $str = (string)$str;
    if( is_null($utf8) ) {
        if( !function_exists('mb_detect_encoding') ) {
            $utf8 = (strtolower( mb_detect_encoding($str) )=='utf-8');
        } else {
            $length = strlen($str);
            $utf8 = true;
            for ($i=0; $i < $length; $i++) {
                $c = ord($str[$i]);
                if ($c < 0x80) $n = 0; # 0bbbbbbb
                elseif (($c & 0xE0) == 0xC0) $n=1; # 110bbbbb
                elseif (($c & 0xF0) == 0xE0) $n=2; # 1110bbbb
                elseif (($c & 0xF8) == 0xF0) $n=3; # 11110bbb
                elseif (($c & 0xFC) == 0xF8) $n=4; # 111110bb
                elseif (($c & 0xFE) == 0xFC) $n=5; # 1111110b
                else return false; # Does not match any model
                for ($j=0; $j<$n; $j++) { # n bytes matching 10bbbbbb follow ?
                    if ((++$i == $length)
                        || ((ord($str[$i]) & 0xC0) != 0x80)) {
                        $utf8 = false;
                        break;
                    }

                }
            }
        }

    }

    if(!$utf8)
        $str = utf8_encode($str);

    $transliteration = array(
    'Ĳ' => 'I', 'Ö' => 'O','Œ' => 'O','Ü' => 'U','ä' => 'a','æ' => 'a',
    'ĳ' => 'i','ö' => 'o','œ' => 'o','ü' => 'u','ß' => 's','ſ' => 's',
    'À' => 'A','Á' => 'A','Â' => 'A','Ã' => 'A','Ä' => 'A','Å' => 'A',
    'Æ' => 'A','Ā' => 'A','Ą' => 'A','Ă' => 'A','Ç' => 'C','Ć' => 'C',
    'Č' => 'C','Ĉ' => 'C','Ċ' => 'C','Ď' => 'D','Đ' => 'D','È' => 'E',
    'É' => 'E','Ê' => 'E','Ë' => 'E','Ē' => 'E','Ę' => 'E','Ě' => 'E',
    'Ĕ' => 'E','Ė' => 'E','Ĝ' => 'G','Ğ' => 'G','Ġ' => 'G','Ģ' => 'G',
    'Ĥ' => 'H','Ħ' => 'H','Ì' => 'I','Í' => 'I','Î' => 'I','Ï' => 'I',
    'Ī' => 'I','Ĩ' => 'I','Ĭ' => 'I','Į' => 'I','İ' => 'I','Ĵ' => 'J',
    'Ķ' => 'K','Ľ' => 'K','Ĺ' => 'K','Ļ' => 'K','Ŀ' => 'K','Ł' => 'L',
    'Ñ' => 'N','Ń' => 'N','Ň' => 'N','Ņ' => 'N','Ŋ' => 'N','Ò' => 'O',
    'Ó' => 'O','Ô' => 'O','Õ' => 'O','Ø' => 'O','Ō' => 'O','Ő' => 'O',
    'Ŏ' => 'O','Ŕ' => 'R','Ř' => 'R','Ŗ' => 'R','Ś' => 'S','Ş' => 'S',
    'Ŝ' => 'S','Ș' => 'S','Š' => 'S','Ť' => 'T','Ţ' => 'T','Ŧ' => 'T',
    'Ț' => 'T','Ù' => 'U','Ú' => 'U','Û' => 'U','Ū' => 'U','Ů' => 'U',
    'Ű' => 'U','Ŭ' => 'U','Ũ' => 'U','Ų' => 'U','Ŵ' => 'W','Ŷ' => 'Y',
    'Ÿ' => 'Y','Ý' => 'Y','Ź' => 'Z','Ż' => 'Z','Ž' => 'Z','à' => 'a',
    'á' => 'a','â' => 'a','ã' => 'a','ā' => 'a','ą' => 'a','ă' => 'a',
    'å' => 'a','ç' => 'c','ć' => 'c','č' => 'c','ĉ' => 'c','ċ' => 'c',
    'ď' => 'd','đ' => 'd','è' => 'e','é' => 'e','ê' => 'e','ë' => 'e',
    'ē' => 'e','ę' => 'e','ě' => 'e','ĕ' => 'e','ė' => 'e','ƒ' => 'f',
    'ĝ' => 'g','ğ' => 'g','ġ' => 'g','ģ' => 'g','ĥ' => 'h','ħ' => 'h',
    'ì' => 'i','í' => 'i','î' => 'i','ï' => 'i','ī' => 'i','ĩ' => 'i',
    'ĭ' => 'i','į' => 'i','ı' => 'i','ĵ' => 'j','ķ' => 'k','ĸ' => 'k',
    'ł' => 'l','ľ' => 'l','ĺ' => 'l','ļ' => 'l','ŀ' => 'l','ñ' => 'n',
    'ń' => 'n','ň' => 'n','ņ' => 'n','ŉ' => 'n','ŋ' => 'n','ò' => 'o',
    'ó' => 'o','ô' => 'o','õ' => 'o','ø' => 'o','ō' => 'o','ő' => 'o',
    'ŏ' => 'o','ŕ' => 'r','ř' => 'r','ŗ' => 'r','ś' => 's','š' => 's',
    'ť' => 't','ù' => 'u','ú' => 'u','û' => 'u','ū' => 'u','ů' => 'u',
    'ű' => 'u','ŭ' => 'u','ũ' => 'u','ų' => 'u','ŵ' => 'w','ÿ' => 'y',
    'ý' => 'y','ŷ' => 'y','ż' => 'z','ź' => 'z','ž' => 'z','Α' => 'A',
    'Ά' => 'A','Ἀ' => 'A','Ἁ' => 'A','Ἂ' => 'A','Ἃ' => 'A','Ἄ' => 'A',
    'Ἅ' => 'A','Ἆ' => 'A','Ἇ' => 'A','ᾈ' => 'A','ᾉ' => 'A','ᾊ' => 'A',
    'ᾋ' => 'A','ᾌ' => 'A','ᾍ' => 'A','ᾎ' => 'A','ᾏ' => 'A','Ᾰ' => 'A',
    'Ᾱ' => 'A','Ὰ' => 'A','ᾼ' => 'A','Β' => 'B','Γ' => 'G','Δ' => 'D',
    'Ε' => 'E','Έ' => 'E','Ἐ' => 'E','Ἑ' => 'E','Ἒ' => 'E','Ἓ' => 'E',
    'Ἔ' => 'E','Ἕ' => 'E','Ὲ' => 'E','Ζ' => 'Z','Η' => 'I','Ή' => 'I',
    'Ἠ' => 'I','Ἡ' => 'I','Ἢ' => 'I','Ἣ' => 'I','Ἤ' => 'I','Ἥ' => 'I',
    'Ἦ' => 'I','Ἧ' => 'I','ᾘ' => 'I','ᾙ' => 'I','ᾚ' => 'I','ᾛ' => 'I',
    'ᾜ' => 'I','ᾝ' => 'I','ᾞ' => 'I','ᾟ' => 'I','Ὴ' => 'I','ῌ' => 'I',
    'Θ' => 'T','Ι' => 'I','Ί' => 'I','Ϊ' => 'I','Ἰ' => 'I','Ἱ' => 'I',
    'Ἲ' => 'I','Ἳ' => 'I','Ἴ' => 'I','Ἵ' => 'I','Ἶ' => 'I','Ἷ' => 'I',
    'Ῐ' => 'I','Ῑ' => 'I','Ὶ' => 'I','Κ' => 'K','Λ' => 'L','Μ' => 'M',
    'Ν' => 'N','Ξ' => 'K','Ο' => 'O','Ό' => 'O','Ὀ' => 'O','Ὁ' => 'O',
    'Ὂ' => 'O','Ὃ' => 'O','Ὄ' => 'O','Ὅ' => 'O','Ὸ' => 'O','Π' => 'P',
    'Ρ' => 'R','Ῥ' => 'R','Σ' => 'S','Τ' => 'T','Υ' => 'Y','Ύ' => 'Y',
    'Ϋ' => 'Y','Ὑ' => 'Y','Ὓ' => 'Y','Ὕ' => 'Y','Ὗ' => 'Y','Ῠ' => 'Y',
    'Ῡ' => 'Y','Ὺ' => 'Y','Φ' => 'F','Χ' => 'X','Ψ' => 'P','Ω' => 'O',
    'Ώ' => 'O','Ὠ' => 'O','Ὡ' => 'O','Ὢ' => 'O','Ὣ' => 'O','Ὤ' => 'O',
    'Ὥ' => 'O','Ὦ' => 'O','Ὧ' => 'O','ᾨ' => 'O','ᾩ' => 'O','ᾪ' => 'O',
    'ᾫ' => 'O','ᾬ' => 'O','ᾭ' => 'O','ᾮ' => 'O','ᾯ' => 'O','Ὼ' => 'O',
    'ῼ' => 'O','α' => 'a','ά' => 'a','ἀ' => 'a','ἁ' => 'a','ἂ' => 'a',
    'ἃ' => 'a','ἄ' => 'a','ἅ' => 'a','ἆ' => 'a','ἇ' => 'a','ᾀ' => 'a',
    'ᾁ' => 'a','ᾂ' => 'a','ᾃ' => 'a','ᾄ' => 'a','ᾅ' => 'a','ᾆ' => 'a',
    'ᾇ' => 'a','ὰ' => 'a','ᾰ' => 'a','ᾱ' => 'a','ᾲ' => 'a','ᾳ' => 'a',
    'ᾴ' => 'a','ᾶ' => 'a','ᾷ' => 'a','β' => 'b','γ' => 'g','δ' => 'd',
    'ε' => 'e','έ' => 'e','ἐ' => 'e','ἑ' => 'e','ἒ' => 'e','ἓ' => 'e',
    'ἔ' => 'e','ἕ' => 'e','ὲ' => 'e','ζ' => 'z','η' => 'i','ή' => 'i',
    'ἠ' => 'i','ἡ' => 'i','ἢ' => 'i','ἣ' => 'i','ἤ' => 'i','ἥ' => 'i',
    'ἦ' => 'i','ἧ' => 'i','ᾐ' => 'i','ᾑ' => 'i','ᾒ' => 'i','ᾓ' => 'i',
    'ᾔ' => 'i','ᾕ' => 'i','ᾖ' => 'i','ᾗ' => 'i','ὴ' => 'i','ῂ' => 'i',
    'ῃ' => 'i','ῄ' => 'i','ῆ' => 'i','ῇ' => 'i','θ' => 't','ι' => 'i',
    'ί' => 'i','ϊ' => 'i','ΐ' => 'i','ἰ' => 'i','ἱ' => 'i','ἲ' => 'i',
    'ἳ' => 'i','ἴ' => 'i','ἵ' => 'i','ἶ' => 'i','ἷ' => 'i','ὶ' => 'i',
    'ῐ' => 'i','ῑ' => 'i','ῒ' => 'i','ῖ' => 'i','ῗ' => 'i','κ' => 'k',
    'λ' => 'l','μ' => 'm','ν' => 'n','ξ' => 'k','ο' => 'o','ό' => 'o',
    'ὀ' => 'o','ὁ' => 'o','ὂ' => 'o','ὃ' => 'o','ὄ' => 'o','ὅ' => 'o',
    'ὸ' => 'o','π' => 'p','ρ' => 'r','ῤ' => 'r','ῥ' => 'r','σ' => 's',
    'ς' => 's','τ' => 't','υ' => 'y','ύ' => 'y','ϋ' => 'y','ΰ' => 'y',
    'ὐ' => 'y','ὑ' => 'y','ὒ' => 'y','ὓ' => 'y','ὔ' => 'y','ὕ' => 'y',
    'ὖ' => 'y','ὗ' => 'y','ὺ' => 'y','ῠ' => 'y','ῡ' => 'y','ῢ' => 'y',
    'ῦ' => 'y','ῧ' => 'y','φ' => 'f','χ' => 'x','ψ' => 'p','ω' => 'o',
    'ώ' => 'o','ὠ' => 'o','ὡ' => 'o','ὢ' => 'o','ὣ' => 'o','ὤ' => 'o',
    'ὥ' => 'o','ὦ' => 'o','ὧ' => 'o','ᾠ' => 'o','ᾡ' => 'o','ᾢ' => 'o',
    'ᾣ' => 'o','ᾤ' => 'o','ᾥ' => 'o','ᾦ' => 'o','ᾧ' => 'o','ὼ' => 'o',
    'ῲ' => 'o','ῳ' => 'o','ῴ' => 'o','ῶ' => 'o','ῷ' => 'o','А' => 'A',
    'Б' => 'B','В' => 'V','Г' => 'G','Д' => 'D','Е' => 'E','Ё' => 'E',
    'Ж' => 'Z','З' => 'Z','И' => 'I','Й' => 'I','К' => 'K','Л' => 'L',
    'М' => 'M','Н' => 'N','О' => 'O','П' => 'P','Р' => 'R','С' => 'S',
    'Т' => 'T','У' => 'U','Ф' => 'F','Х' => 'K','Ц' => 'T','Ч' => 'C',
    'Ш' => 'S','Щ' => 'S','Ы' => 'Y','Э' => 'E','Ю' => 'Y','Я' => 'Y',
    'а' => 'A','б' => 'B','в' => 'V','г' => 'G','д' => 'D','е' => 'E',
    'ё' => 'E','ж' => 'Z','з' => 'Z','и' => 'I','й' => 'I','к' => 'K',
    'л' => 'L','м' => 'M','н' => 'N','о' => 'O','п' => 'P','р' => 'R',
    'с' => 'S','т' => 'T','у' => 'U','ф' => 'F','х' => 'K','ц' => 'T',
    'ч' => 'C','ш' => 'S','щ' => 'S','ы' => 'Y','э' => 'E','ю' => 'Y',
    'я' => 'Y','ð' => 'd','Ð' => 'D','þ' => 't','Þ' => 'T','ა' => 'a',
    'ბ' => 'b','გ' => 'g','დ' => 'd','ე' => 'e','ვ' => 'v','ზ' => 'z',
    'თ' => 't','ი' => 'i','კ' => 'k','ლ' => 'l','მ' => 'm','ნ' => 'n',
    'ო' => 'o','პ' => 'p','ჟ' => 'z','რ' => 'r','ს' => 's','ტ' => 't',
    'უ' => 'u','ფ' => 'p','ქ' => 'k','ღ' => 'g','ყ' => 'q','შ' => 's',
    'ჩ' => 'c','ც' => 't','ძ' => 'd','წ' => 't','ჭ' => 'c','ხ' => 'k',
    'ჯ' => 'j','ჰ' => 'h'
    );
    $str = str_replace( array_keys( $transliteration ),
                        array_values( $transliteration ),
                        $str);
    return $str;
}





function trim_text($input, $length, $ellipses = true, $strip_html = true) {
    //strip tags, if desired
    if ($strip_html) {
        $input = strip_tags($input);
    }
  
    //no need to trim, already shorter than trim length
    if (strlen($input) <= $length) {
        return $input;
    }
  
    //find last space within length
    $last_space = strrpos(substr($input, 0, $length), ' ');
    $trimmed_text = substr($input, 0, $last_space);
  
    //add ellipses (...)
    if ($ellipses) {
        $trimmed_text .= '...';
    }
  
    return $trimmed_text;
}


function ImageHandling($imagew,$imageh,$dir,$newdir,$new_img_val="new_img_val",$new_img_val1="new_img_val",$beforvalue="",$me="me",$inline="inline1",$newimg="newimg",$img1="img1",$drag="dragg",$x="x",$y="y",$btn="btn",$submit="submit",$imgpath="imgpath",$checkimg = "check-img",$inc="")
  { 
 
  include   ROOT_DIR."include/Imagehandling/imgfun.php";
  
  }  
  
/**
* Send debug code to the Javascript console
*/
function debug_to_console($data) {
if(is_array($data) || is_object($data))
{
echo("<script>console.log('PHP: ".json_encode($data)."');</script>");
} else {
echo("<script>console.log('PHP: ".$data."');</script>");
}
}

function curPageURL() {
 $pageURL = 'http';
 if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
 $pageURL .= "://";
 if ($_SERVER["SERVER_PORT"] != "80") {
  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
 } else {
  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
 }
 return $pageURL;
}
/* To get dynamic meta title*/
  function get_dynamic_meta($id,$lng_id)
  {
  	global $db;
    $metaquery_dynamic = "SELECT pag_met_title,pag_lng_id FROM tbl_pages where  pag_id = '".$id."' and pag_lng_id = '".$lng_id."'";
    $metadata_dynamic = $db->select($metaquery_dynamic);
     return  charConversion($metadata_dynamic[0]['pag_met_title']);
 }
  /* To get dynamic meta title*/
  function get_metadata($filename,$lng_id)
  {
  	global $db;
    $metaquery = "SELECT fxp_met_title,fxp_name,fxp_lng_id FROM tbl_fixed_page where  fxp_name = '".$filename."' and fxp_lng_id = '".$lng_id."'";
    $metedata = $db->select($metaquery);
     return charConversion($metedata[0]['fxp_met_title']);
 }
   function get_metasubpage($subpage,$lng_id)
  {
  	global $db;
    $metaquery = "SELECT fxp_met_title,fxp_name,fxp_lng_id FROM tbl_fixed_page where  fxp_name = '".$subpage."' and fxp_level = 'sub' and fxp_lng_id = '".$lng_id."' ";
    $metedata = $db->select($metaquery);
     return charConversion($metedata[0]['fxp_met_title']);
 }
    function get_level2image($pag_parent_id,$lng_id)
  {
  	global $db;
    $metaquery = "SELECT pag_img  FROM tbl_pages where  pag_id = '".$pag_parent_id."' and pag_level = '2' and pag_lng_id = '".$lng_id."' ";
    $metedata = $db->select($metaquery);
     return $metedata[0]['pag_img'];
 }


function time_ago($date,$granularity=2) {
    $date = strtotime($date);
    $difference = time() - $date;
    $periods = array('decade' => 315360000,
        'year' => 31536000,
        'month' => 2628000,
        'week' => 604800, 
        'day' => 86400,
        'hour' => 3600,
        'minute' => 60,
        'second' => 1);

    foreach ($periods as $key => $value) {
        if ($difference >= $value) {
            $time = floor($difference/$value);
            $difference %= $value;
            $retval .= ($retval ? ' ' : '').$time.' ';
            $retval .= (($time > 1) ? $key.'s' : $key);
            $granularity--;
        }
        if ($granularity == '0') { break; }
    }
    return $retval.' ago';      
}


function sendMessage($mebId,$message,$filepath)
{
	global $db;
	$email_fields 		= array("sub_email");
	$email_where  		= "sub_status = 'Active' AND sub_dir_id = '".$mebId."'";
	$emailRes 			= $db->selectData(TBL_MEMBER_SUBSCRIBERS,$email_fields,$email_where,$extra="",1);	
	$countemailResRes   = count($emailRes);	
	
	$toemail[] = "";		
		
	foreach($emailRes as $key => $email_to)
	{	
		$toemail[$key] = $email_to['sub_email'];								
	}
	
	$subject 	= "B2b Mobile Message";
		
	$message = '<html xmlns="http://www.w3.org/1999/xhtml">
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<meta content="width=1000" name="viewport"> </head>
				<body><table style="background-color:#151515" bgcolor="#151515" cellpadding="0" cellspacing="0" width="100%">
      <tbody>
        <tr>
          <td style="text-align:left" align="center">
            <table id="topMessageWrapper" style="width:600px;background-color:#151515;color:#999999;font-family:arial;font-size:13px;padding-bottom:0px;padding-top:0px;margin:0 auto;" align="center" bgcolor="#ffffff" cellpadding="0" cellspacing="0" width="600">
              <tbody>
                <tr>
                  <td>
                    <!-- DESIGNER TEMPLATE: AirMailOneColumn --><!-- BASIC TEMPLATE: unknown derivation -->
                    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="600">
                      <tbody>
                        <tr>
                          <td style="color:#999999;font-family:arial;font-size:13px;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tbody>
                                <tr>
                                  <td style="color:#999999;font-family:arial;font-size:13px;" valign="top">
                                    <div style="">
                                      <table style="word-wrap:break-word;table-layout:fixed;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                          <tr>
                                            <td style="color:#999999;font-family:arial;font-size:13px;">
                                              <p style="margin-bottom:0;margin-left:0;margin-right:0;margin-top:0;padding:0;">
                                                <img alt="" src="http://ivqmobile.com/images/jpeg_002.jpg" style="display:block;" usemap="#Map" border="0"></p>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                        <tr>
                          <td style="background-color:#ffffff;color:#999999;font-family:arial;font-size:13px;" bgcolor="#ffffff">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tbody>
                                <tr>
                                  <td style="background-color:#ffffff;padding-bottom:0px;padding-left:0px;padding-right:0px;color:#999999;font-family:arial;font-size:13px;" bgcolor="#ffffff" valign="top">                                    
                                    <div style="">
                                      <table style="word-wrap:break-word;table-layout:fixed;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>		  
										  <tr>
                                            <td style="color:#2f2d2e;font-family:arial;font-size:13px; padding:0px 33px 33px;">
                                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="left" valign="top" style="color:#2f2d2e;font-family:arial;font-size:13px; line-height:24px; padding:0px 0px;"><p>'.$message.'</p></td>		
	</tr>
	
</table>

                                            </td>
                                          </tr>
										  
										  <tr>
										  <td style="padding:0px 33px 33px;">
										  	<p style="color:#333; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:17px; margin:0px;"><strong>Thanks,</strong></p>
											<p style="color:#666; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:17px; margin:0px;">B2b Mobile,</p>
											<p style="color:#666; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:17px; margin:0px;"><a target="_blank" style="color:#0E70B9; text-decoration:underline;" href="#">B2b Mobile</a></p>
										  </td>
										  </tr>
										  
                                        </tbody>
                                      </table>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                        <tr>
                          <td style="color:#999999;font-family:arial;font-size:13px;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tbody>
                                <tr>
                                  <td style="color:#999999;font-family:arial;font-size:13px;" valign="top">
                                    <div>
                                      <table style="word-wrap:break-word;table-layout:fixed;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                          <tr>
                                            <td style="color:#999999;font-family:arial;font-size:13px;">
                                              <p style="margin-bottom:0;margin-left:0;margin-right:0;margin-top:0;padding:0;">
                                                <img alt="" src="http://getnakedair.com/email-images/jpeg.jpg" style="display:block;"></p>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                        <tr>
                          <td style="color:#999999;font-family:arial;font-size:13px;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tbody>
                                <tr>
                                  <td style="color:#999999;font-family:arial;font-size:13px;" valign="top">
                                    <div>
                                      <table style="word-wrap:break-word;table-layout:fixed;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                          <tr>
                                            <td style="color:#999999;font-family:arial;font-size:13px;" align="center">
                                              <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tbody>
                                                  
                                                  <tr>
                                                    <td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#FFF; padding-top:18px; padding-bottom:18px;" align="center">
                                                      <span style="color:#0e70b9">© '.date("Y").'</span> B2B Mobile. All right reserved Intelli-Touch.</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
      </tbody>
    </table></body>
				</html>';				
	$fromemail = "B2b Mobile";
	if($countemailResRes>0)
	{
	$sendmail   = mailfunctionnew($fromemail,$toemail,$subject,$message,$custom_host_name,$custom_user_name,$custom_password,$custom_port_name,$filepath);
	}
	
}

function sendMessageSub($mebId,$message,$filepath)
{
    global $db;
    $email_fields 		= array("sub_email");
    $email_where  		= "sub_status = 'Active' AND sub_id = '".$mebId."'";
    $emailRes 			= $db->selectData(TBL_MEMBER_SUBSCRIBERS,$email_fields,$email_where,$extra="",1);
    $countemailResRes   = count($emailRes);

    $toemail[] = "";

    foreach($emailRes as $key => $email_to)
    {
        $toemail[$key] = $email_to['sub_email'];
    }

    $subject 	= "IVQMobile Message";

    $message = '<html xmlns="http://www.w3.org/1999/xhtml">
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<meta content="width=1000" name="viewport"> </head>
				<body><table style="background-color:#151515" bgcolor="#151515" cellpadding="0" cellspacing="0" width="100%">
      <tbody>
        <tr>
          <td style="text-align:left" align="center">
            <table id="topMessageWrapper" style="width:600px;background-color:#151515;color:#999999;font-family:arial;font-size:13px;padding-bottom:0px;padding-top:0px;margin:0 auto;" align="center" bgcolor="#ffffff" cellpadding="0" cellspacing="0" width="600">
              <tbody>
                <tr>
                  <td>
                    <!-- DESIGNER TEMPLATE: AirMailOneColumn --><!-- BASIC TEMPLATE: unknown derivation -->
                    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="600">
                      <tbody>
                        <tr>
                          <td style="color:#999999;font-family:arial;font-size:13px;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tbody>
                                <tr>
                                  <td style="color:#999999;font-family:arial;font-size:13px;" valign="top">
                                    <div style="">
                                      <table style="word-wrap:break-word;table-layout:fixed;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                          <tr>
                                            <td style="color:#999999;font-family:arial;font-size:13px;">
                                              <p style="margin-bottom:0;margin-left:0;margin-right:0;margin-top:0;padding:0;">
                                                <img alt="" src="'.MEMBER_WWW.'images/jpeg_002.png" style="display:block;" usemap="#Map" border="0"></p>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                        <tr>
                          <td style="background-color:#ffffff;color:#999999;font-family:arial;font-size:13px;" bgcolor="#ffffff">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tbody>
                                <tr>
                                  <td style="background-color:#ffffff;padding-bottom:0px;padding-left:0px;padding-right:0px;color:#999999;font-family:arial;font-size:13px;" bgcolor="#ffffff" valign="top">
                                    <div style="">
                                      <table style="word-wrap:break-word;table-layout:fixed;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
										  <tr>
                                            <td style="color:#2f2d2e;font-family:arial;font-size:13px; padding:0px 33px 33px;">
                                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="left" valign="top" style="color:#2f2d2e;font-family:arial;font-size:13px; line-height:24px; padding:0px 0px;"><p>'.$message.'</p></td>
	</tr>

</table>

                                            </td>
                                          </tr>

										  <tr>
										  <td style="padding:0px 33px 33px;">
										  	<p style="color:#333; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:17px; margin:0px;"><strong>Thanks,</strong></p>
											<p style="color:#666; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:17px; margin:0px;">IVQMobile,</p>
											<p style="color:#666; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:17px; margin:0px;"><a target="_blank" style="color:#0E70B9; text-decoration:underline;" href="#">IVQMobile</a></p>
										  </td>
										  </tr>

                                        </tbody>
                                      </table>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>

                        <tr>
                          <td style="color:#999999;font-family:arial;font-size:13px;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tbody>
                                <tr>
                                  <td style="color:#999999;font-family:arial;font-size:13px;" valign="top">
                                    <div>
                                      <table style="word-wrap:break-word;table-layout:fixed;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                          <tr>
                                            <td style="color:#999999;font-family:arial;font-size:13px;" align="center">
                                              <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tbody>

                                                  <tr>
                                                    <td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#FFF; padding-top:18px; padding-bottom:18px;" align="center">
                                                      <span style="color:#0e70b9">© '.date("Y").'</span> IVQMobile. All right reserved Intelli-Touch.</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
      </tbody>
    </table></body>
				</html>';
    $fromemail = "IVQMobile";
    if($countemailResRes>0) {
        $sendmail   = mailfunctionnew($fromemail,$toemail,$subject,$message,$custom_host_name,$custom_user_name,$custom_password,$custom_port_name,$filepath);
    }

}

function sendMessageAdmin($email,$message,$filepath)
{
    global $db;
    $toemail[]="";

    $toemail['0'] = $email;
    $subject 	= "IVQMobile Message";
    $message = '<html xmlns="http://www.w3.org/1999/xhtml">
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<meta content="width=1000" name="viewport"> </head>
				<body><table style="background-color:#151515" bgcolor="#151515" cellpadding="0" cellspacing="0" width="100%">
      <tbody>
        <tr>
          <td style="text-align:left" align="center">
            <table id="topMessageWrapper" style="width:600px;background-color:#151515;color:#999999;font-family:arial;font-size:13px;padding-bottom:0px;padding-top:0px;margin:0 auto;" align="center" bgcolor="#ffffff" cellpadding="0" cellspacing="0" width="600">
              <tbody>
                <tr>
                  <td>
                    <!-- DESIGNER TEMPLATE: AirMailOneColumn --><!-- BASIC TEMPLATE: unknown derivation -->
                    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="600">
                      <tbody>
                        <tr>
                          <td style="color:#999999;font-family:arial;font-size:13px;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tbody>
                                <tr>
                                  <td style="color:#999999;font-family:arial;font-size:13px;" valign="top">
                                    <div style="">
                                      <table style="word-wrap:break-word;table-layout:fixed;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                          <tr>
                                            <td style="color:#999999;font-family:arial;font-size:13px;">
                                              <p style="margin-bottom:0;margin-left:0;margin-right:0;margin-top:0;padding:0;">
                                                <img alt="" src="'.MEMBER_WWW.'images/jpeg_002.png" style="display:block;" usemap="#Map" border="0"></p>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                        <tr>
                          <td style="background-color:#ffffff;color:#999999;font-family:arial;font-size:13px;" bgcolor="#ffffff">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tbody>
                                <tr>
                                  <td style="background-color:#ffffff;padding-bottom:0px;padding-left:0px;padding-right:0px;color:#999999;font-family:arial;font-size:13px;" bgcolor="#ffffff" valign="top">
                                    <div style="">
                                      <table style="word-wrap:break-word;table-layout:fixed;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
										  <tr>
                                            <td style="color:#2f2d2e;font-family:arial;font-size:13px; padding:0px 33px 33px;">
                                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="left" valign="top" style="color:#2f2d2e;font-family:arial;font-size:13px; line-height:24px; padding:0px 0px;"><p>'.$message.'</p></td>
	</tr>

</table>

                                            </td>
                                          </tr>

										  <tr>
										  <td style="padding:0px 33px 33px;">
										  	<p style="color:#333; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:17px; margin:0px;"><strong>Thanks,</strong></p>
											<p style="color:#666; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:17px; margin:0px;">IVQMobile,</p>
											<p style="color:#666; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:17px; margin:0px;"><a target="_blank" style="color:#0E70B9; text-decoration:underline;" href="#">IVQMobile</a></p>
										  </td>
										  </tr>

                                        </tbody>
                                      </table>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>

                        <tr>
                          <td style="color:#999999;font-family:arial;font-size:13px;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tbody>
                                <tr>
                                  <td style="color:#999999;font-family:arial;font-size:13px;" valign="top">
                                    <div>
                                      <table style="word-wrap:break-word;table-layout:fixed;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                          <tr>
                                            <td style="color:#999999;font-family:arial;font-size:13px;" align="center">
                                              <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tbody>

                                                  <tr>
                                                    <td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#FFF; padding-top:18px; padding-bottom:18px;" align="center">
                                                      <span style="color:#0e70b9">© '.date("Y").'</span> IVQMobile. All right reserved Intelli-Touch.</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
      </tbody>
    </table></body>
				</html>';
    $fromemail = "IVQMobile";
    //if($countemailResRes>0) {
        $sendmail   = mailfunctionnew($fromemail,$toemail,$subject,$message,$custom_host_name,$custom_user_name,$custom_password,$custom_port_name,$filepath);
    //}

}

function subscriberHome(){
	$message = '<html xmlns="http://www.w3.org/1999/xhtml">
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<meta content="width=1000" name="viewport"> </head>
				<body><table style="background-color:#151515" bgcolor="#151515" cellpadding="0" cellspacing="0" width="100%">
      <tbody>
        <tr>
          <td style="text-align:left" align="center">
            <table id="topMessageWrapper" style="width:600px;background-color:#151515;color:#999999;font-family:arial;font-size:13px;padding-bottom:0px;padding-top:0px;margin:0 auto;" align="center" bgcolor="#ffffff" cellpadding="0" cellspacing="0" width="600">
              <tbody>
                <tr>
                  <td>
                    <!-- DESIGNER TEMPLATE: AirMailOneColumn --><!-- BASIC TEMPLATE: unknown derivation -->
                    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="600">
                      <tbody>
                        <tr>
                          <td style="color:#999999;font-family:arial;font-size:13px;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tbody>
                                <tr>
                                  <td style="color:#999999;font-family:arial;font-size:13px;" valign="top">
                                    <div style="">
                                      <table style="word-wrap:break-word;table-layout:fixed;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                          <tr>
                                            <td style="color:#999999;font-family:arial;font-size:13px;">
                                              <p style="margin-bottom:0;margin-left:0;margin-right:0;margin-top:0;padding:0;">
                                                <img alt="" src="http://ivqmobile.com/images/jpeg_002.jpg" style="display:block;" usemap="#Map" border="0"></p>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                        <tr>
                          <td style="background-color:#ffffff;color:#999999;font-family:arial;font-size:13px;" bgcolor="#ffffff">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tbody>
                                <tr>
                                  <td style="background-color:#ffffff;padding-bottom:0px;padding-left:0px;padding-right:0px;color:#999999;font-family:arial;font-size:13px;" bgcolor="#ffffff" valign="top">                                    
                                    <div style="">
                                      <table style="word-wrap:break-word;table-layout:fixed;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                        <tr>
                                        	<td style="color:#2f2d2e;font-family:arial;font-size:14px; padding:15px 0px 0px 33px;">Hello,</td>
                                        </tr>		  
										  <tr>
                                            <td style="color:#2f2d2e;font-family:arial;font-size:13px; padding:0px 45px 33px;">
                                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="left" valign="top" style="color:#2f2d2e;font-family:arial;font-size:13px; line-height:24px; padding:0px 0px;"><p>Thanks for subscribe B2B mobile Services.</p></td>		
	</tr>
	
</table>

                                            </td>
                                          </tr>
										  
										  <tr>
										  <td style="padding:0px 33px 33px;">
										  	<p style="color:#333; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:17px; margin:0px;"><strong>Thanks,</strong></p>
											<p style="color:#666; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:17px; margin:0px;">B2b Mobile,</p>
											<p style="color:#666; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:17px; margin:0px;"><a target="_blank" style="color:#0E70B9; text-decoration:underline;" href="#">B2b Mobile</a></p>
										  </td>
										  </tr>
										  
                                        </tbody>
                                      </table>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                        <tr>
                          <td style="color:#999999;font-family:arial;font-size:13px;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tbody>
                                <tr>
                                  <td style="color:#999999;font-family:arial;font-size:13px;" valign="top">
                                    <div>
                                      <table style="word-wrap:break-word;table-layout:fixed;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                          <tr>
                                            <td style="color:#999999;font-family:arial;font-size:13px;">
                                              <p style="margin-bottom:0;margin-left:0;margin-right:0;margin-top:0;padding:0;">
                                                <img alt="" src="http://getnakedair.com/email-images/jpeg.jpg" style="display:block;"></p>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                        <tr>
                          <td style="color:#999999;font-family:arial;font-size:13px;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tbody>
                                <tr>
                                  <td style="color:#999999;font-family:arial;font-size:13px;" valign="top">
                                    <div>
                                      <table style="word-wrap:break-word;table-layout:fixed;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                          <tr>
                                            <td style="color:#999999;font-family:arial;font-size:13px;" align="center">
                                              <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tbody>
                                                  
                                                  <tr>
                                                    <td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#FFF; padding-top:18px; padding-bottom:18px;" align="center">
                                                      <span style="color:#0e70b9">© '.date("Y").'</span> B2B Mobile. All right reserved Intelli-Touch.</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
      </tbody>
    </table></body>
				</html>';
	return $message;
}
function businessSubscriberInfo($uname,$pass)
{
	$message = '<html>
			<head>
			  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			  <meta content="width=1000" name="viewport">
			  <title>B2b Mobile User Information</title>
			</head>
			<body style="margin:0px;">
    			<table style="background-color:#151515" bgcolor="#151515" cellpadding="0" cellspacing="0" width="100%">
      			<tbody>
        		<tr>
          			<td style="text-align:left" align="center">
            			<table id="topMessageWrapper" style="width:600px;background-color:#151515;color:#999999;font-family:arial;font-size:13px;padding-bottom:0px;padding-top:0px;margin:0 auto;" align="center" bgcolor="#ffffff" cellpadding="0" cellspacing="0" width="600">
              			<tbody>
                		<tr>
                  			<td>
                    			<table border="0" cellpadding="0" cellspacing="0" height="100%" width="600">
                      			<tbody>
                        		<tr>
                          			<td style="color:#999999;font-family:arial;font-size:13px;">
                            			<table border="0" cellpadding="0" cellspacing="0" width="100%">
                              			<tbody>
                                		<tr>
                                  			<td style="color:#999999;font-family:arial;font-size:13px;" valign="top">
		                                    <div style="">
												<table style="word-wrap:break-word;table-layout:fixed;" border="0" cellpadding="0" cellspacing="0" width="100%">
		                                        <tbody>
		                                        <tr>
													<td style="color:#999999;font-family:arial;font-size:13px;">
														<p style="margin-bottom:0;margin-left:0;margin-right:0;margin-top:0;padding:0;">
														<img alt="" src="http://ivqmobile.com/images/jpeg_002.jpg" style="display:block;" usemap="#Map" border="0">
														</p>
		                                            </td>
		                                        </tr>
		                                        </tbody>
		                                      	</table>
		                                    </div>
                                  			</td>
                                		</tr>
                              			</tbody>
                            			</table>
                          			</td>
                        		</tr>
                        		<tr>
                          			<td style="background-color:#ffffff;color:#999999;font-family:arial;font-size:13px;" bgcolor="#ffffff">
                            		<table border="0" cellpadding="0" cellspacing="0" width="100%">
                              		<tbody>
                                	<tr>
                                  		<td style="background-color:#ffffff;padding-bottom:0px;padding-left:0px;padding-right:0px;color:#999999;font-family:arial;font-size:13px;" bgcolor="#ffffff" valign="top">
                                  		<div style="">
                                      	<table style="word-wrap:break-word;table-layout:fixed;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                        <tr>
											<td style="color:#2f2d2e;font-family:arial;font-size:13px; padding:33px;">
                                            <p style="margin-bottom:0;margin-left:0;margin-right:0;margin-top:0;padding:0;"><span style="color:#0e70b9; font-size:21px;"><strong>User Information</strong></span></p>
                                            </td>
                                        </tr>
                                        <tr>
											<td style="color:#2f2d2e;font-family:arial;font-size:13px; padding:0px 33px 33px;">
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td align="left" valign="top" style="color:#2f2d2e;font-family:arial;font-size:13px; line-height:24px; padding:0px 0px;"><strong>User Name:</strong></td>
												<td align="left" valign="top" style="color:#2f2d2e;font-family:arial;font-size:13px; line-height:24px; padding:0px 0px;">'.$uname.'</td>
											</tr>
											<tr>
												<td align="left" valign="top" style="color:#2f2d2e;font-family:arial;font-size:13px; line-height:24px; padding:0px 0px;"><strong>Password:</strong></td>
                                              	<td align="left" valign="top" style="color:#2f2d2e;font-family:arial;font-size:13px; line-height:24px; padding:0px 0px;">'.$pass.'</td>
                                            </tr>
                                            </table>
                                            </td>
                                        </tr>
										<tr>
                      						<td style="padding:0px 33px 33px;">
                      							<p style="color:#333; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:17px; margin:0px;"><strong>Thanks,</strong></p>
                      							<p style="color:#666; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:17px; margin:0px;">B2b Mobile,</p>
                      							<p style="color:#666; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:17px; margin:0px;"><a target="_blank" style="color:#0E70B9; text-decoration:underline;" href="http://ivqmobile.com">IVQMobile</a></p>
                      						</td>
                    					</tr>
                                        </tbody>
                                      	</table>
                                    	</div>
                                  		</td>
                               		</tr>
                              		</tbody>
                            		</table>
                          			</td>
                        		</tr>
                        		<tr>
                          			<td style="color:#999999;font-family:arial;font-size:13px;">
                            		<table border="0" cellpadding="0" cellspacing="0" width="100%">
                              		<tbody>
                                	<tr>
                                  		<td style="color:#999999;font-family:arial;font-size:13px;" valign="top">
                                    	<div>
                                      		<table style="word-wrap:break-word;table-layout:fixed;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        	<tbody>
                                          	<tr>
                                            	<td style="color:#999999;font-family:arial;font-size:13px;">
                                              		<p style="margin-bottom:0;margin-left:0;margin-right:0;margin-top:0;padding:0;"><img alt="" src="http://getnakedair.com/email-images/jpeg.jpg" style="display:block;"></p>
                                            	</td>
                                          	</tr>
                                        	</tbody>
                                      		</table>
                                    	</div>
                                  		</td>
                                	</tr>
                              		</tbody>
                            		</table>
                          			</td>
                        		</tr>
                        		<tr>
                          			<td style="color:#999999;font-family:arial;font-size:13px;">
                            		<table border="0" cellpadding="0" cellspacing="0" width="100%">
                              		<tbody>
                                	<tr>
                                  		<td style="color:#999999;font-family:arial;font-size:13px;" valign="top">
                                    	<div>
                                      		<table style="word-wrap:break-word;table-layout:fixed;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        	<tbody>
                                          	<tr>
                                            	<td style="color:#999999;font-family:arial;font-size:13px;" align="center">
                                              		<table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                	<tbody>
                                                  	<tr>
                                                    	<td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#FFF; padding-top:18px; padding-bottom:18px;" align="center">
                                                      	<span style="color:#0e70b9">© '.date("Y").'</span> B2B Mobile. All right reserved Intelli-Touch.</td>
                                                  	</tr>
                                                	</tbody>
                                              		</table>
                                            	</td>
                                          	</tr>
                                        	</tbody>
                                      		</table>
                                    	</div>
                                  	</td>
                                </tr>
                              	</tbody>
                            	</table>
                          	</td>
                        </tr>
                    	</tbody>
                    	</table>
                	</td>
                	</tr>
              		</tbody>
            		</table>
          		</td>
        		</tr>
      			</tbody>
    			</table>
			</body>
			</html>';
	return $message;
}

//FOR Category Name
function getCategoryNameByMebId($mebID)
{
    global $db;
    $sqlQuery = "SELECT dir_dic_id,(SELECT dic_name FROM `tbl_dir_category` WHERE dir_dic_id=dic_id) as CategoryName FROM `tbl_directory` WHERE dir_id IN(SELECT meb_dir_id FROM `tbl_member` WHERE meb_id='".$mebID."')";
    $categoryRes  = $db->select($sqlQuery);
			 $totcategoryRes = count($categoryRes);
    if($totcategoryRes > 0)
    {
         $catName = $categoryRes[0]['CategoryName'];
    }
    else
    {
         $catName = "";
    }
    return $catName;
}
//FOR Category Name

function getBusinessCategoryList($name="",$id="",$value="",$default_name="Select Category",$style="")
{
	global $db;
	$query = "SELECT * FROM ".TBL_BUSINESS_CATEGORY." 
			  WHERE buc_status='Active' AND buc_master='1'";
	$data = $db->select($query);	
	$class = "custom-select";
	$categoryCombo = '';
		$categoryCombo .= '<select name="'.$name.'" id="'.$id.'" class="'.$class.'" style="'.$style.'">';
		$categoryCombo .= '<option value="0">'.$default_name.'</option>';  	
	$jj = 0; 
 $kk = 0; 
	for($i=0;$i<count($data);$i++)
	{		
  // if($data[$i]["buc_master"] == "1")
  // {
       // $kk = $jj;
       // if($jj == $kk + 1){
           // $categoryCombo .= '</optgroup>';
       // }
       
       // $categoryCombo .= '<optgroup label="'.$data[$i]["buc_name"].'">';       
       // $jj = $jj + 1;
  // }
  //if($data[$i]["buc_master"] == "0")
  //{
		$categoryCombo .= '<option value="'.$data[$i]["buc_id"].'"'; 
		if($value == $data[$i]["buc_id"])
		{
			$categoryCombo .='selected="selected"';
		}
		$categoryCombo .='>'.$data[$i]["buc_name"].'</option>';
  //} 
	}
	$categoryCombo .='</select>';
	
	return $categoryCombo;
}

function getRatingValue($type,$id,$userId)
{
	global $db;

 if($type == "Card")
 {
      $user_query = "SELECT * from tbl_rating WHERE rat_bus_id='".$id."' AND rat_usr_id='".$userId."'";
 }
 else if($type == "Coupons")
 {
      $user_query = "SELECT * from tbl_rating WHERE rat_cop_id='".$id."' AND rat_usr_id='".$userId."'"; 
 }
 $userResult  = $db->select($user_query);
 $totuserResult = count($userResult);
 if($totuserResult > 0)
 {
      return "disabled";
 }
 else
 {
      return "";
 }
	
}
?>


