<?php
	include_once('../../include/includeclass.php');
	$action_type			=	$_POST['action_type'];
	$SECTION_TABLE			= 	$_POST['tableName'];
	$SECTION_FIELD_PREFIX	=	$_POST['fieldPrefix'];
	$SECTION_AUTO_ID 		=	$_POST['autoID'];
	$SECTION_MANAGE_PAGE	=	$_POST['managePage'];
	$SECTION_WHERE			=	$SECTION_FIELD_PREFIX."id='".$SECTION_AUTO_ID."'";
	$xtraCondition			=	stripslashes($_POST['xtraCondition']);
	$SECTION_NAME			=	$_POST['displayName'];	
	$searchchar				=	$_POST['searchchar'];
	
	$SECTION="Messaging";
	$total_language = count($result_language);
	#################################################################
	if($action_type=="sendMail")
	{
		$msgId = $_POST['msg_id'];
		$message_where  = "msg_status = 'Active' AND msg_meb_id = '".$_SESSION['meb_id']."' and msg_id=".$msgId;
		$messageRes 	= $db->selectData(TBL_MEMBER_MESSAGING,$message_fields = "",$message_where,$extra="",2);	
		$message = $messageRes[0]['msg_message'];
		$type = $messageRes[0]['msg_type'];
		if($type=="Draft")
		{
			$update_values['msg_type']	= "Published";
		}
		/*else
		{
			$update_values['msg_type']	= "Published";
		}*/
		$update_values['msg_updated_id']	= $_SESSION['meb_id'];
		$update_values['msg_updated_date']	= date('Y-m-d H:i:s');
		$GPDetail_result = $db->updateData(TBL_MEMBER_MESSAGING,$update_values,"msg_id=".$msgId);
		
		$resendfile = $messageRes[0]['msg_file'];
		if($resendfile!="")
			$msg_file = UPLOAD_DIR_MESSAGING_FILE.$msgId."/".$filename;
		else
			$msg_file = "";

		$msg=sendMessage($_SESSION['meb_id'],$message,$msg_file);
	}
	if($action_type ==  "delete")
	{
		$update_values[$SECTION_FIELD_PREFIX.'status']      = "Deleted";
		$update_values[$SECTION_FIELD_PREFIX . 'updated_id'] 	= $_SESSION['meb_id'];
        $update_values[$SECTION_FIELD_PREFIX . 'updated_date'] 	= date("Y-m-d H:i:s");
		$GPDetail_result = $db->updateData($SECTION_TABLE,$update_values,$SECTION_WHERE);
		$_SESSION['msg']  =   "Messaging has been deleted successfully.";  
	}
	
	else if($action_type  ==  "sorting")
	{
		$orderby  = $_POST['orderby'];
		$order    = $_POST['order'];
		if($order == "asc" || $order == "")
			$ORDER =  "desc";
		else
			$ORDER =  "asc";
	}
	if($orderby == "")
	{
		$orderby = "message";
	}
	##################################  General Query ###############################################  
	if ($searchchar != 'undefined' && $searchchar != "") {
		if ($searchchar == 'other') {
			$sql_query = "SELECT p.* FROM " . $SECTION_TABLE . " p WHERE p." . $SECTION_FIELD_PREFIX . "id != 0   AND  p.".$SECTION_FIELD_PREFIX."status!='Deleted' AND p.msg_meb_id=".$_SESSION['meb_id']." AND `msg_message` REGEXP '^[^a-zA-Z]'   order by p." . $SECTION_FIELD_PREFIX . $orderby . " " . $order;
		}
		else {
			if ($searchchar == 'all') {
				$sql_query = "SELECT p.* FROM " . $SECTION_TABLE . " p WHERE p." . $SECTION_FIELD_PREFIX . "id != 0  AND p.".$SECTION_FIELD_PREFIX."status!='Deleted' AND p.msg_meb_id=".$_SESSION['meb_id']." order by p." . $SECTION_FIELD_PREFIX . $orderby . " " . $order;
			} else {
				$ts = $searchchar . "%";
				$sql_query = "select * from ".$SECTION_TABLE." as p where p.msg_message LIKE '" . $ts . "' AND p.".$SECTION_FIELD_PREFIX."status!='Deleted' AND p.msg_meb_id=".$_SESSION['meb_id']." order by " . $SECTION_FIELD_PREFIX . $orderby . " " . $order;
			}
		}
	}
	else if ($xtraCondition != 'undefined' && $xtraCondition != "") {
		//$ts = $xtraCondition;
		$sql_query = "select * from ".$SECTION_TABLE." as p where (" . $xtraCondition . ") AND (p.".$SECTION_FIELD_PREFIX."status!='Deleted' AND p.msg_meb_id=".$_SESSION['meb_id'].") order by " . $SECTION_FIELD_PREFIX . $orderby . " " . $order;
	}
	else
	{
		$sql_query = "SELECT p.* FROM ".$SECTION_TABLE." as p WHERE p.".$SECTION_FIELD_PREFIX."id != 0 AND p.".$SECTION_FIELD_PREFIX."status != 'Deleted' AND p.msg_meb_id=".$_SESSION['meb_id']." order by p.".$SECTION_FIELD_PREFIX.$orderby." ".$order;

	}    
	#################################  Paging Query + Paging Code ##################################
	$paging_query = $sql_query;
	$paging_result  = $db->select($paging_query); 
	$count = count($paging_result);
	$per_page = SITE_PAGING_PER_PAGE; //rows per page
	$per_page = 10;
	$pages = ceil($count/$per_page);    
	#################################################################################################
	if($action_type == "paging")
	{
		if(!empty($_POST['page']))  
		{
			$page = $_POST['page'];
		}       
	}
	else
	{
		if(!empty($_POST['page']))  
			$page = $_POST['page'];
	  	else
		$page = 1;
	} 
	$list_query = $sql_query;
	if(!empty($per_page) && $_POST['page']!="all")
	{
		$start = ($page-1)*$per_page;
		if($start<0)
		{
			$start=0;
		}
		$list_query .= " limit $start,$per_page"; 
	} 
	$result_query  = $db->select($list_query);  
	$total_rows = count($result_query);
	//echo $ms = ajaxMsg($_SESSION['msg']);
?>
<nav>
	<div class="shorting">
		<ul>
			<?php
	        for ($i = 65; $i < 91; $i++) {
				if ($searchchar == chr($i) || $_SESSION['character_search'] == chr($i))
				{
					$selectchar = "class='active'"; 
				}
				else 
				{
					$selectchar=""; 
				}
			?>
			<li><a href="javascript:void(0);" <?php echo $selectchar; ?> onclick="getAjaxSearchByLetter('<?php echo $SECTION; ?>','<?php echo $SECTION_TABLE; ?>','<?php echo $SECTION_FIELD_PREFIX; ?>','<?php echo chr($i); ?>','<?php echo $SECTION_MANAGE_PAGE; ?>');"><?php echo chr($i); ?></a></li>
			<?php
	        }        
			if($_SESSION['character_search']=='other' || $searchchar == 'other')
			{
				$selectother = "class='active'"; 
			}
			else
			{
				$selectother=""; 
			}
			?>
			<li><a href="javascript:void(0);" <?php echo $selectother; ?> onclick="getAjaxSearchByLetter('<?php echo $SECTION; ?>','<?php echo $SECTION_TABLE; ?>', '<?php echo $SECTION_FIELD_PREFIX; ?>', 'other','<?php echo $SECTION_MANAGE_PAGE; ?>');">Other</a></li>
			<?php
			if($_SESSION['character_search']=='all' || $searchchar == 'all')
			{
				$selectall = "class='active'"; 
			}
			else
			{
				$selectall=""; 
			}
			?>
			<li><a href="javascript:void(0);" <?php echo $selectall; ?> onclick="getAjaxSearchByLetter('<?php echo $SECTION; ?>','<?php echo $SECTION_TABLE; ?>', '<?php echo $SECTION_FIELD_PREFIX; ?>', 'all','<?php echo $SECTION_MANAGE_PAGE; ?>');">All</a></li>
						
		</ul>
		<!-- <div class="export add">
			<a class="tooltip" href="#" title="Add"><img src="images/add-member.png"></a>
		</div> -->
		<div class="export">
			<?php
			if($total_rows>0)
			{
				$href_export = "export_excel.php?table=tbl_member_messaging&prifix=msg_";
			}
			else
			{
				$href_export = "#";
			}
			?>
			<a href="<?php echo $href_export; ?>" class="tooltip" title="Export"><img src="images/export-orange.png" /></a>
		</div>
	</div>
</nav>
<div class="clr"></div>
<aside>
	<div class="tab_content_holder second">
		<div class="tab_content_holder_inner">
			<?php 
			if($total_rows > 0 ) 
			{
			?>
			<table id="updateSearch" width="100%" cellspacing="0" cellpadding="0">
				<tr>						
					<th width="60%">Message</th>
					<th width="14%" onclick="getAjaxSorting('<?php echo $SECTION; ?>','<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>','<?php echo 'type'; ?>','<?php echo $ORDER; ?>','');">Status</th>
					<th width="14%" onclick="getAjaxSorting('<?php echo $SECTION; ?>','<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>','<?php echo 'updated_date'; ?>','<?php echo $ORDER; ?>','');">Sent On</th>
					<th width="12%">Action</th>									
				</tr>
				<?php
				$j=1;
				for($i=0;$i<$total_rows;$i++)
				{
				?>
				<tr <?php if($i == $j){ echo 'class="light"'; $j = $j + 2;}?>>
					<td><?php echo charCut($result_query[$i][$SECTION_FIELD_PREFIX.'message'],"120","..."); ?></td>
					<td><?php echo $result_query[$i][$SECTION_FIELD_PREFIX.'type']; ?></td>
					<td><?php echo date('m/d/Y',strtotime($result_query[$i][$SECTION_FIELD_PREFIX.'updated_date'])); ?></td>								
					<td>
						<a href="view_records.php?sectionName=Messaging&msg_id=<?php echo $result_query[$i][$SECTION_FIELD_PREFIX."id"];?>" class="fancybox fancybox.ajax tooltip" title="View" data-fancybox-type="ajax"><img src="images/equale-orange.png" /></a>
						<a href="<?php echo getMemberURL($SECTION_MANAGE_PAGE,"Edit","msg_id=".$result_query[$i][$SECTION_FIELD_PREFIX."id"]); ?>" class="tooltip" title="Edit"><img src="images/edit-orange.png" /></a>
						<?php if($result_query[$i][$SECTION_FIELD_PREFIX.'type']=="Published") { ?>
						<a href="javascript:void(0);" class="tooltip" onclick="sendmail('Published','<?php echo $result_query[$i][$SECTION_FIELD_PREFIX."id"]; ?>')" title="Resend"><img src="images/msg-send.png" /></a>	
						<?php
						}
						else
						{
						?>
						<a href="javascript:void(0);" class="tooltip" onclick="sendmail('Draft','<?php echo $result_query[$i][$SECTION_FIELD_PREFIX."id"]; ?>')" title="Publish"><img src="images/draft.png" /></a>	
						<?php
						}
						?>
						
					</td>
				</tr>
				<?php }?>				
			</table>
			<?php } else {?>
				<div class="no-record">No Records</div>
			<?php }?>   
			<div class="clr"></div>
			<nav>
            	<div class="paging">
					<ul>
						<?php
						for($t=1; $t<=$pages; $t++)
						{
							if($page==$t) { $selected = "class='active'"; } else { $selected=""; }
							//$URL = MEB_INDEX_PARAMETER.$SECTION_VIEW_PAGE."&page=".$t; 
						?>										
						<li><a <?php echo $selected; ?> href="javascript:void(0);" <?php if($pages > 1){?> onclick="getAjaxPaging('<?php echo $SECTION; ?>','<?php echo $SECTION_TABLE; ?>','<?php echo $SECTION_FIELD_PREFIX; ?>','<?php echo $pages; ?>','<?php echo $t ?>','<?php echo "&orderby=".$orderby."&order=".$order; ?>','<?php echo $SECTION_MANAGE_PAGE;?>')" <?php } ?> ><?php echo $t; ?></a></li>
						<?php
						}
						?>
					</ul>
				</div>
			</nav>
		</div>
	</div>
</aside>
<script type="text/javascript">
    $(document).ready(function() {
        $('.tooltip').tooltipster();
		$.fancybox.close();
    });
</script>
