<?php 
	include_once('../../include/includeclass.php');
	$action_type			=	$_POST['action_type'];
	$SECTION_TABLE 			= 	$_POST['tableName'];
	$SECTION_FIELD_PREFIX	=	$_POST['fieldPrefix'];
	$SECTION_AUTO_ID 		=	$_POST['autoID'];
	$SECTION_WHERE			=	$SECTION_FIELD_PREFIX."id='".$SECTION_AUTO_ID."'";
	$xtraCondition			=	stripslashes($_POST['xtraCondition']);
	$SECTION_NAME			=	$_POST['displayName'];	
	$language_prefix		=	$_POST['language_prefix'];
	$SECTION_MANAGE_PAGE 	=   ADM_MANAGE_DIRECTORY;
	$searchchar 			= 	$_POST['searchchar'];
	$SECTION_TABLE_CATEGORY	=   TBL_DIRECTORY_CATEGORY;

	$total_language = count($result_language);
	
	if($action_type	==	"delete")
	{		
		$update_values[$SECTION_FIELD_PREFIX.'status']			= "Deleted";
		$update_values[$SECTION_FIELD_PREFIX.'updated_id']		= getAdminSessionId();
		$update_values[$SECTION_FIELD_PREFIX.'updated_date']	= date('Y-m-d H:i:s');
		$GPDetail_result = $db->updateData($SECTION_TABLE,$update_values,$SECTION_WHERE);
        //$_SESSION['msg']	= 	"Directory has been deleted successfully.";	
	}
	else if($action_type	==	"active") // if $action_type == active then update values = Inactive
	{
		$update_values[$SECTION_FIELD_PREFIX.'status']			= "Inactive";
		$update_values[$SECTION_FIELD_PREFIX.'updated_id']		= getAdminSessionId();
		$update_values[$SECTION_FIELD_PREFIX.'updated_date']	= date('Y-m-d H:i:s');
		$GPDetail_result = $db->updateData($SECTION_TABLE,$update_values,$SECTION_WHERE.$xtraCondition);
		//$_SESSION['msg']	= 	"Directory has been inactive successfully.";	
	}
	else if($action_type	==	"inactive") // if $action_type == inactive then update values = active
	{
		$update_values[$SECTION_FIELD_PREFIX.'status']			= "Active";
		$update_values[$SECTION_FIELD_PREFIX.'updated_id']		= getAdminSessionId();
		$update_values[$SECTION_FIELD_PREFIX.'updated_date']	= date('Y-m-d H:i:s');
		$GPDetail_result = $db->updateData($SECTION_TABLE,$update_values,$SECTION_WHERE.$xtraCondition);
		//$_SESSION['msg'] 	= 	"Directory has been active successfully.";	
	}
	else if($action_type	==	"sorting")
	{
		$orderby	=	$_POST['orderby'];
		$order		=	$_POST['order'];
		if($order == "asc" || $order == "")
			$ORDER =	"desc";
		else
			$ORDER =	"asc";
	}

	if($orderby == "")
	{
		$orderby = "name";
	}

	$dirName = "(select dic_name from ".$SECTION_TABLE_CATEGORY." where dic_id = dir_dic_id ) as Dirname";
	##################################  General Query ############################################### 	
	if ($searchchar != 'undefined' && $searchchar != "") {
    	if ($searchchar == 'other') {
    	    $sql_query = "SELECT p.*,".$dirName." FROM " . $SECTION_TABLE . " p WHERE p." . $SECTION_FIELD_PREFIX . "id != 0   AND  p.".$SECTION_FIELD_PREFIX."status!='Deleted'  AND `dir_name` REGEXP '^[^a-zA-Z]'   order by p." . $SECTION_FIELD_PREFIX . $orderby . " " . $order;
    	} else {
        	if ($searchchar == 'all') {
            
        	    $sql_query = "SELECT p.*,".$dirName." FROM " . $SECTION_TABLE . " p WHERE p." . $SECTION_FIELD_PREFIX . "id != 0  AND p.".$SECTION_FIELD_PREFIX."status!='Deleted'  order by p." . $SECTION_FIELD_PREFIX . $orderby . " " . $order;
        	} else {
        	    $ts = $searchchar . "%";
        	    $sql_query = "select *,".$dirName." from ".$SECTION_TABLE." as p where p.dir_name LIKE '" . $ts . "' AND p.".$SECTION_FIELD_PREFIX."status!='Deleted'  order by " . $SECTION_FIELD_PREFIX . $orderby . " " . $order;
        	}
    	}
	}
	else if ($xtraCondition != 'undefined' && $xtraCondition != "") {
		$ts = "%" . $xtraCondition . "%";
		$sql_query = "select *,".$dirName." from ".$SECTION_TABLE." as p where (".$xtraCondition.") AND (p.".$SECTION_FIELD_PREFIX."status!='Deleted')  order by " . $SECTION_FIELD_PREFIX . $orderby . " " . $order;
	}
	else
	{
		$sql_query = "SELECT p.*,".$dirName." FROM ".$SECTION_TABLE." p WHERE p.".$SECTION_FIELD_PREFIX."id != 0 AND p.".$SECTION_FIELD_PREFIX."status != 'Deleted' order by p.".$SECTION_FIELD_PREFIX.$orderby." ".$order;
	}

    //echo $sql_query;
	$excel_query = "SELECT p.*,".$dirName." FROM ".$SECTION_TABLE." p WHERE p.".$SECTION_FIELD_PREFIX."id != 0 AND p.".$SECTION_FIELD_PREFIX."status != 'Deleted' order by p.".$SECTION_FIELD_PREFIX.$orderby." ".$order;
	$excel_result  = $db->select($excel_query);	
	$excelcount = count($excel_result);	
	
		#################################  Paging Query + Paging Code ##################################
		$paging_query = $sql_query;
		$paging_result  = $db->select($paging_query);	
		$count = count($paging_result);
		$per_page = SITE_PAGING_PER_PAGE; //rows per page
		$per_page = 10;
		$pages = ceil($count/$per_page);		
		#################################################################################################
		if ($action_type == "paging")
		{
			if(!empty($_POST['page']))	
			{
				$page = $_POST['page'];
			}  			
		}
		else
		{
			if(!empty($_POST['page']))	
				$page = $_POST['page'];
			else
		 		$page = 1;
		}	
		$list_query = $sql_query;
		if(!empty($per_page) && $_POST['page']!="all")
		{
			$start = ($page-1)*$per_page;
			if($start<0)
			{
				$start=0;
			}
			$list_query .= " limit $start,$per_page";	
		}	
		$result_query  = $db->select($list_query);	
		$total_rows = count($result_query);
		echo $ms = ajaxMsg($_SESSION['msg']);
	$xtraCondition = "";
?>
			<nav>
				 <div class="shorting">
				 	  <ul>
		<?php
        for ($i = 65; $i < 91; $i++) {
            if ($searchchar == chr($i) || $_SESSION['character_search'] == chr($i)) {
                ?><li><a href="javascript:void(0);" class="active" onclick="getAjaxSearchByLetter('Directory','<?php echo $SECTION_TABLE; ?>', '<?php echo $SECTION_FIELD_PREFIX; ?>', '<?php echo chr($i); ?>');"><?php echo chr($i); ?></a></li><?php
            } else {
                ?><li><a href="javascript:void(0);" onclick="getAjaxSearchByLetter('Directory','<?php echo $SECTION_TABLE; ?>', '<?php echo $SECTION_FIELD_PREFIX; ?>', '<?php echo chr($i); ?>');"><?php echo chr($i); ?></a></li><?php
            }           
        }        
        if($_SESSION['character_search']=='other' || $searchchar == 'other'){
		?>
        <li><a href="javascript:void(0);" class="active" onclick="getAjaxSearchByLetter('Directory','<?php echo $SECTION_TABLE; ?>', '<?php echo $SECTION_FIELD_PREFIX; ?>', 'other');">Other</a></li>
        <?php	}else{	?>
		<li><a href="javascript:void(0);" onclick="getAjaxSearchByLetter('Directory','<?php echo $SECTION_TABLE; ?>', '<?php echo $SECTION_FIELD_PREFIX; ?>', 'other');">Other</a></li><?php
		}?>
        <?php
        if($_SESSION['character_search']=='all' || $searchchar == 'all'){
		?><li><a href="javascript:void(0);" class="active" onclick="getAjaxSearchByLetter('Directory','<?php echo $SECTION_TABLE; ?>', '<?php echo $SECTION_FIELD_PREFIX; ?>', 'all');">All</a></li><?php	
		}else{?>
		<li><a href="javascript:void(0);" onclick="getAjaxSearchByLetter('Directory','<?php echo $SECTION_TABLE; ?>', '<?php echo $SECTION_FIELD_PREFIX; ?>', 'all');">All</a></li>
		<?php
		}
		?>		
					  </ul>
					  <div class="export"><a href="<?php echo ADM_INDEX_PARAMETER.$SECTION_MANAGE_PAGE; ?>" class="tooltip" title="Add"><img src="../images/add.png" /></a></div>
					  <div class="export"><a href="<?php if($excelcount != "0")echo "export_excel.php?table=tbl_directory&prifix=dir_";else echo ADM_INDEX_PARAMETER.ADM_HOME."#directory";?>" class="tooltip" title="Export"><img src="../images/export.png" /></a></div>					  
				 </div>
			</nav>
			<div class="clr"></div>
			<aside>
				 <div class="tab_content_holder second">
				 	  <div class="tab_content_holder_inner">
					  <?php if($total_rows > 0){ ?>
					  	   <table width="100%" cellspacing="0" cellpadding="0">
						   		<tr>			
									<th width="15%"><label onclick="getAjaxSorting('Directory','<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>','<?php echo 'website'; ?>','<?php echo $ORDER; ?>','<?php echo mysql_real_escape_string($xtraCondition); ?>','<?php echo $SECTION_MANAGE_PAGE; ?>');" >Directory Category</label></th>
									<th width="16%"><label onclick="getAjaxSorting('Directory','<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>','<?php echo 'name'; ?>','<?php echo $ORDER; ?>','<?php echo mysql_real_escape_string($xtraCondition); ?>','<?php echo $SECTION_MANAGE_PAGE; ?>');" >Directory Name</label></th>
									<th width="16%"><label onclick="getAjaxSorting('Directory','<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>','<?php echo 'address'; ?>','<?php echo $ORDER; ?>','<?php echo mysql_real_escape_string($xtraCondition); ?>','<?php echo $SECTION_MANAGE_PAGE; ?>');" >Address</label></th>
									<th width="8%"><label onclick="getAjaxSorting('Directory','<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>','<?php echo 'zip'; ?>','<?php echo $ORDER; ?>','<?php echo mysql_real_escape_string($xtraCondition); ?>','<?php echo $SECTION_MANAGE_PAGE; ?>');" >Zip</label></th>								
									<th width="9%"><label onclick="getAjaxSorting('Directory','<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>','<?php echo 'sta_id'; ?>','<?php echo $ORDER; ?>','<?php echo mysql_real_escape_string($xtraCondition); ?>','<?php echo $SECTION_MANAGE_PAGE; ?>');" >State</label></th>									
									<th width="10%"><label onclick="getAjaxSorting('Directory','<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>','<?php echo 'office_phone'; ?>','<?php echo $ORDER; ?>','<?php echo mysql_real_escape_string($xtraCondition); ?>','<?php echo $SECTION_MANAGE_PAGE; ?>');" >Phone</label></th>
									<th width="15%"><label onclick="getAjaxSorting('Directory','<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>','<?php echo 'email'; ?>','<?php echo $ORDER; ?>','<?php echo mysql_real_escape_string($xtraCondition); ?>','<?php echo $SECTION_MANAGE_PAGE; ?>');" >Email</label></th>									
									<th width="11%">Action</th>
																		
								</tr>
								<?php
								$j = 1;
								for($i=0;$i<$total_rows;$i++)
								{
								?>
								<tr <?php if($i == $j){ echo 'class="light"'; $j = $j + 2;}?>>
									<td><?php echo $result_query[$i]['Dirname']; ?></td>
									<td><?php echo $result_query[$i][$SECTION_FIELD_PREFIX.'name']; ?></td>
									<td><?php echo $result_query[$i][$SECTION_FIELD_PREFIX.'address']; ?></td>
									<td><?php echo $result_query[$i][$SECTION_FIELD_PREFIX.'zip']; ?></td>
									<td><?php
										$state_fields = array("sta_name");
										$state_where  = "sta_id = '".$result_query[$i][$SECTION_FIELD_PREFIX.'sta_id']."'";
										$staRes = $db->selectData(TBL_STATE,$state_fields,$state_where,$extra="",1);
										echo $staRes[0]["sta_name"]; 
									?></td>									
									<td><?php echo $result_query[$i][$SECTION_FIELD_PREFIX.'office_phone']; ?></td>	
									<td><a href="mailto:<?php echo $result_query[$i][$SECTION_FIELD_PREFIX.'email']; ?>"><?php echo $result_query[$i][$SECTION_FIELD_PREFIX.'email']; ?></a></td>																		
									<td>
										<a href="view_records.php?sectionName=Directory&dir_id=<?php echo $result_query[$i][$SECTION_FIELD_PREFIX."id"];?>" class="fancybox fancybox.ajax tooltip" title="View" data-fancybox-type="ajax"><img src="../images/equale.png" /></a>
										<a href="<?php echo getAdminActionLink2($SECTION_MANAGE_PAGE,$language_prefix,'Edit',$SECTION_FIELD_PREFIX."id",$result_query[$i][$SECTION_FIELD_PREFIX."id"])?>" class="tooltip" title="Edit"><img src="../images/edit.png" /></a>
										<!--<a href="javascript:void(0);" class="tooltip" title="Delete" onclick="getAjaxDeleteAction('Directory','<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>',<?php echo $result_query[$i][$SECTION_FIELD_PREFIX."id"]; ?>,'','<?php echo $SECTION_MANAGE_PAGE;?>');"><img src="../images/delete.png" /></a>-->										
										<a href="javascript:void(0);" class="tooltip" title="Delete" onclick="javascript:alertBox('Directory','<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>',<?php echo $result_query[$i][$SECTION_FIELD_PREFIX."id"]; ?>,'','<?php echo $SECTION_MANAGE_PAGE;?>');"><img src="../images/delete.png" /></a>	
									</td>
								</tr>
								<?php }?>
						   </table>
						   <?php }else{ ?><div class="no-record">No Records</div> <?php }?>
						   <div class="clr"></div>
						   <nav>
            				 <div class="paging">
            				 	  <ul>
								<?php
									for($t=1; $t<=$pages; $t++)
									{
										if($page==$t) 
										{
									?>										
										<li><a class="active" href="javascript:void(0);" <?php if($pages > 1){?> onclick="getAjaxPaging('Directory','<?php echo $SECTION_TABLE; ?>','<?php echo $SECTION_FIELD_PREFIX; ?>','<?php echo $pages; ?>','<?php echo $t ?>','<?php echo "&orderby=".$orderby."&order=".$order; ?>','<?php echo $SECTION_MANAGE_PAGE;?>');"<?php }?>><?php echo $t ?></a></li>
									<?php
										}
										else 
										{
									?>
										<li><a href="javascript:void(0);" <?php if($pages > 1){?>onclick="getAjaxPaging('Directory','<?php echo $SECTION_TABLE; ?>','<?php echo $SECTION_FIELD_PREFIX; ?>','<?php echo $pages; ?>','<?php echo $t ?>','<?php echo "&orderby=".$orderby."&order=".$order; ?>','<?php echo $SECTION_MANAGE_PAGE;?>');"<?php }?>><?php echo $t ?></a></li>
									<?php
										}
									}
								?>          					  	  
            						 					  
            					  </ul>            					  
            				 </div>
               			   </nav>
					  </div>
				 </div>               				   
			</aside>
			
	<script type="text/javascript">
    $(document).ready(function() {
        $('.tooltip').tooltipster();
		$.fancybox.close();
    });
</script>
