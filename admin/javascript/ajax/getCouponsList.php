<?php
	include_once('../../include/includeclass.php');
	$action_type			=	$_POST['action_type'];
	$SECTION_TABLE			= 	$_POST['tableName'];
	$SECTION_FIELD_PREFIX	=	$_POST['fieldPrefix'];
	$SECTION_AUTO_ID 		=	$_POST['autoID'];
	$SECTION_MANAGE_PAGE	=	$_POST['managePage'];
	$SECTION_WHERE			=	$SECTION_FIELD_PREFIX."id='".$SECTION_AUTO_ID."'";
	$xtraCondition			=	stripslashes($_POST['xtraCondition']);
	$SECTION_NAME			=	$_POST['displayName'];	
	$searchchar				=	$_POST['searchchar'];
	/*echo "<pre>";
	print_r($_POST);
	echo "</pre>";
	exit;*/
	$SECTION="Coupons";
	$total_language = count($result_language);
	#################################################################

	if($action_type ==  "delete")
	{
		$update_values[$SECTION_FIELD_PREFIX.'status']      	= "Deleted";
		$update_values[$SECTION_FIELD_PREFIX . 'updated_id'] 	= $_SESSION['meb_id'];
        $update_values[$SECTION_FIELD_PREFIX . 'updated_date'] 	= date("Y-m-d H:i:s");
		$GPDetail_result = $db->updateData($SECTION_TABLE,$update_values,$SECTION_WHERE);
		$_SESSION['msg']  =   "Subscribers has been deleted successfully.";  
	}
	
	else if($action_type  ==  "sorting")
	{
		$orderby  = $_POST['orderby'];
		$order    = $_POST['order'];
		if($order == "asc" || $order == "")
			$ORDER =  "desc";
		else
			$ORDER =  "asc";
	}
	if($orderby == "")
	{
		$orderby = "title";
	}
	##################################  General Query ###############################################  
	if ($searchchar != 'undefined' && $searchchar != "") {
		if ($searchchar == 'other') {
			$sql_query = "SELECT p.* FROM " . $SECTION_TABLE . " p WHERE p." . $SECTION_FIELD_PREFIX . "id != 0   AND  p.".$SECTION_FIELD_PREFIX."status!='Deleted' AND p.cop_meb_id=".$_SESSION['meb_id']." AND `cop_title` REGEXP '^[^a-zA-Z]'   order by p." . $SECTION_FIELD_PREFIX . $orderby . " " . $order;
		}
		else {
			if ($searchchar == 'all') {
				$sql_query = "SELECT p.* FROM " . $SECTION_TABLE . " p WHERE p." . $SECTION_FIELD_PREFIX . "id != 0  AND p.".$SECTION_FIELD_PREFIX."status!='Deleted' AND p.cop_meb_id=".$_SESSION['meb_id']." order by p." . $SECTION_FIELD_PREFIX . $orderby . " " . $order;
			} else {
				$ts = $searchchar . "%";
				$sql_query = "select * from ".$SECTION_TABLE." as p where p.cop_title LIKE '" . $ts . "' AND p.".$SECTION_FIELD_PREFIX."status!='Deleted' AND p.cop_meb_id=".$_SESSION['meb_id']." order by " . $SECTION_FIELD_PREFIX . $orderby . " " . $order;
			}
		}
	}
	else if ($xtraCondition != 'undefined' && $xtraCondition != "") {
		//$ts = $xtraCondition;
		$sql_query = "select * from ".$SECTION_TABLE." as p where (" . $xtraCondition . ") AND (p.".$SECTION_FIELD_PREFIX."status!='Deleted' AND p.cop_meb_id=".$_SESSION['meb_id'].") order by " . $SECTION_FIELD_PREFIX . $orderby . " " . $order;
	}
	else
	{
		$sql_query = "SELECT p.* FROM ".$SECTION_TABLE." as p WHERE p.".$SECTION_FIELD_PREFIX."id != 0 AND p.".$SECTION_FIELD_PREFIX."status != 'Deleted' AND p.cop_meb_id=".$_SESSION['meb_id']." order by p.".$SECTION_FIELD_PREFIX.$orderby." ".$order;

	}    
	#################################  Paging Query + Paging Code ##################################
	$paging_query = $sql_query;
	$paging_result  = $db->select($paging_query); 
	$count = count($paging_result);
	$per_page = SITE_PAGING_PER_PAGE; //rows per page
	$per_page = 10;
	$pages = ceil($count/$per_page);    
	#################################################################################################
	if($action_type == "paging")
	{
		if(!empty($_POST['page']))  
		{
			$page = $_POST['page'];
		}       
	}
	else
	{
		if(!empty($_POST['page']))  
			$page = $_POST['page'];
	  	else
		$page = 1;
	} 
	$list_query = $sql_query;
	if(!empty($per_page) && $_POST['page']!="all")
	{
		$start = ($page-1)*$per_page;
		if($start<0)
		{
			$start=0;
		}
		$list_query .= " limit $start,$per_page"; 
	} 
	$result_query  = $db->select($list_query);  
	$total_rows = count($result_query);
	//echo $ms = ajaxMsg($_SESSION['msg']);
?>
<nav>
	<div class="shorting">
		<ul>
			<?php
	        for ($i = 65; $i < 91; $i++) {
				if ($searchchar == chr($i) || $_SESSION['character_search'] == chr($i)) {
			?>
				<li><a href="javascript:void(0);" class="active" onclick="getAjaxSearchByLetter('<?php echo $SECTION; ?>','<?php echo $SECTION_TABLE; ?>','<?php echo $SECTION_FIELD_PREFIX; ?>','<?php echo chr($i); ?>','<?php echo $SECTION_MANAGE_PAGE; ?>');"><?php echo chr($i); ?></a></li>
			<?php
				} else {
			?>
				<li><a href="javascript:void(0);" onclick="getAjaxSearchByLetter('<?php echo $SECTION; ?>','<?php echo $SECTION_TABLE; ?>','<?php echo $SECTION_FIELD_PREFIX; ?>','<?php echo chr($i); ?>','<?php echo $SECTION_MANAGE_PAGE; ?>');"><?php echo chr($i); ?></a></li>
			<?php
				}           
	        }        
			if($_SESSION['character_search']=='other' || $searchchar == 'other')
			{
			?>
				<li><a href="javascript:void(0);" class="active" onclick="getAjaxSearchByLetter('<?php echo $SECTION; ?>','<?php echo $SECTION_TABLE; ?>', '<?php echo $SECTION_FIELD_PREFIX; ?>', 'other','<?php echo $SECTION_MANAGE_PAGE; ?>');">Other</a></li>
			<?php
			} else { ?>
				<li><a href="javascript:void(0);" onclick="getAjaxSearchByLetter('<?php echo $SECTION; ?>','<?php echo $SECTION_TABLE; ?>', '<?php echo $SECTION_FIELD_PREFIX; ?>', 'other','<?php echo $SECTION_MANAGE_PAGE; ?>');">Other</a></li><?php
			}
			if($_SESSION['character_search']=='all' || $searchchar == 'all')
			{
			?>
				<li><a href="javascript:void(0);" class="active" onclick="getAjaxSearchByLetter('<?php echo $SECTION; ?>','<?php echo $SECTION_TABLE; ?>', '<?php echo $SECTION_FIELD_PREFIX; ?>', 'all','<?php echo $SECTION_MANAGE_PAGE; ?>');">All</a></li>
			<?php
			} else { ?>
				<li><a href="javascript:void(0);" onclick="getAjaxSearchByLetter('<?php echo $SECTION; ?>','<?php echo $SECTION_TABLE; ?>', '<?php echo $SECTION_FIELD_PREFIX; ?>', 'all','<?php echo $SECTION_MANAGE_PAGE; ?>');">All</a></li>
			<?php
			}
			?>	
		</ul>
		<div class="export add">
			<a class="tooltip" href="<?php echo MEB_INDEX_PARAMETER.$SECTION_MANAGE_PAGE; ?>" title="Add"><img src="images/add-member.png"></a>
		</div>
		<div class="export">
			<?php
			if($total_rows>0)
			{
				$href_export = "export_excel.php?table=tbl_member_coupons&prifix=cop_";
			}
			else
			{
				$href_export = "#";
			}
			?>
			<a href="<?php echo $href_export; ?>" class="tooltip" title="Export"><img src="images/export-orange.png" /></a>
		</div>
	</div>
</nav>
<div class="clr"></div>
<aside>
	<div class="tab_content_holder second">
		<div class="tab_content_holder_inner">
			<?php
			if($total_rows>0)
			{
			?>
			<table width="100%" cellspacing="0" cellpadding="0">
				<tr>						
					<th width="20%"><label onclick="getAjaxSorting('<?php echo $SECTION ?>','<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>','<?php echo 'title'; ?>','<?php echo $ORDER; ?>','','<?php echo $SECTION_MANAGE_PAGE; ?>');">Coupons Title</label></th>
					<th width="10%"><label onclick="getAjaxSorting('<?php echo $SECTION ?>','<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>','<?php echo 'percentage'; ?>','<?php echo $ORDER; ?>','','<?php echo $SECTION_MANAGE_PAGE; ?>');">Percentage</label></th>
					<th width="25%"><label onclick="getAjaxSorting('<?php echo $SECTION ?>','<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>','<?php echo 'description'; ?>','<?php echo $ORDER; ?>','','<?php echo $SECTION_MANAGE_PAGE; ?>');">Description</label></th>
					<th width="10%"><label onclick="getAjaxSorting('<?php echo $SECTION ?>','<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>','<?php echo 'coupon_code'; ?>','<?php echo $ORDER; ?>','','<?php echo $SECTION_MANAGE_PAGE; ?>');">Coupon Code</label></th>
					<th width="15%"><label onclick="getAjaxSorting('<?php echo $SECTION ?>','<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>','<?php echo 'barcode'; ?>','<?php echo $ORDER; ?>','','<?php echo $SECTION_MANAGE_PAGE; ?>');">BarCode</label></th>
					<th width="10%">Action</th>
				</tr>
				<?php
				$j=1;
				for($i=0;$i<$total_rows;$i++)
				{
				?>
				<tr <?php if($i == $j){ echo 'class="light"'; $j = $j + 2;}?>>
					<td><?php echo $result_query[$i][$SECTION_FIELD_PREFIX.'title']; ?></td>
					<td><?php echo $result_query[$i][$SECTION_FIELD_PREFIX.'percentage']."%"; ?></td>
					<td><?php echo charCut($result_query[$i][$SECTION_FIELD_PREFIX.'description'],"100","...."); ?></td>
					<td><?php echo $result_query[$i][$SECTION_FIELD_PREFIX.'coupon_code']; ?></td>
					<td><?php echo $result_query[$i][$SECTION_FIELD_PREFIX.'barcode']; ?></td>
					<td>
						<a href="view_records.php?sectionName=Coupons&cop_id=<?php echo $result_query[$i][$SECTION_FIELD_PREFIX."id"];?>" class="fancybox fancybox.ajax tooltip" title="View" data-fancybox-type="ajax"><img src="images/equale-orange.png" /></a>
						<a href="<?php echo getMemberURL($SECTION_MANAGE_PAGE,"Edit","cop_id=".$result_query[$i][$SECTION_FIELD_PREFIX."id"]); ?>" class="tooltip" title="Edit"><img src="images/edit-orange.png" /></a>
						<a href="javascript:void(0);" class="tooltip" title="Delete" onclick="javascript:alertBoxMember('<?php echo $SECTION; ?>','<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>',<?php echo $result_query[$i][$SECTION_FIELD_PREFIX."id"]; ?>,'','<?php echo $SECTION_MANAGE_PAGE;?>');"><img src="images/delete.png" /></a>
					</td>
				</tr>
				<?php
				}
				?>  		  
			</table>
			<?php
			}
			else
			{
			?>
			<div class="no-record">No Records</div>
			<?php
			}
			?>
			<div class="clr"></div>
			<nav>
				<div class="paging">
					<ul>
					<?php
					for($t=1; $t<=$pages; $t++)
					{
						if($page==$t) 
						{
					?>										
							<li><a class="active" href="javascript:void(0);" <?php if($pages > 1){?> onclick="getAjaxPaging('<?php echo $SECTION; ?>','<?php echo $SECTION_TABLE; ?>','<?php echo $SECTION_FIELD_PREFIX; ?>','<?php echo $pages; ?>','<?php echo $t ?>','<?php echo "&orderby=".$orderby."&order=".$order; ?>','<?php echo $SECTION_MANAGE_PAGE;?>')" <?php } ?> ><?php echo $t ?></a></li>
					<?php
						}
						else 
						{
					?>
						<li><a href="javascript:void(0);" onclick="getAjaxPaging('<?php echo $SECTION; ?>','<?php echo $SECTION_TABLE; ?>','<?php echo $SECTION_FIELD_PREFIX; ?>','<?php echo $pages; ?>','<?php echo $t ?>','<?php echo "&orderby=".$orderby."&order=".$order; ?>','<?php echo $SECTION_MANAGE_PAGE;?>')"><?php echo $t ?></a></li>
					<?php
						}
					}
					?>
					</ul>            					  
				</div>
			</nav>
		</div>
	</div>               				   
</aside>
<script type="text/javascript">
    $(document).ready(function() {
        $('.tooltip').tooltipster();
		$.fancybox.close();
    });
</script>