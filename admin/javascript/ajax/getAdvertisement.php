<?php 
	include_once('../../include/includeclass.php');
	$action_type			=	$_POST['action_type'];
	$SECTION_TABLE 			= 	$_POST['tableName'];
	$SECTION_FIELD_PREFIX	=	$_POST['fieldPrefix'];
	$SECTION_AUTO_ID 		=	$_POST['autoID'];
	$SECTION_WHERE			=	$SECTION_FIELD_PREFIX."id='".$SECTION_AUTO_ID."'";
	$xtraCondition			=	stripslashes($_POST['xtraCondition']);
	$SECTION_NAME			=	$_POST['displayName'];	
	$language_prefix		=	$_POST['language_prefix'];
	$SECTION_MANAGE_PAGE 	=   ADM_MANAGE_ADVERTISE;
	$searchchar 			= 	$_POST['searchchar'];

	$total_language = count($result_language);
	
	if($action_type	==	"delete")
	{		
		$update_values[$SECTION_FIELD_PREFIX.'status']			= "Deleted";
		$update_values[$SECTION_FIELD_PREFIX.'updated_id']		= getAdminSessionId();
		$update_values[$SECTION_FIELD_PREFIX.'updated_date']	= date('Y-m-d H:i:s');
		$GPDetail_result = $db->updateData($SECTION_TABLE,$update_values,$SECTION_WHERE);
        //$_SESSION['msg']	= 	"Member has been deleted successfully.";	
	}
	else if($action_type	==	"sorting")
	{
		$orderby	=	$_POST['orderby'];
		$order		=	$_POST['order'];
		if($order == "asc" || $order == "")
			$ORDER =	"desc";
		else
			$ORDER =	"asc";
	}

	if($orderby == "")
	{
		$orderby = "title";
	}
	
	##################################  General Query ############################################### 	
	if ($searchchar != 'undefined' && $searchchar != "") {
    	if ($searchchar == 'other') {
    	    $sql_query = "SELECT p.* FROM " . $SECTION_TABLE . " p WHERE p." . $SECTION_FIELD_PREFIX . "id != 0   AND  p.".$SECTION_FIELD_PREFIX."status!='Deleted'  AND `adv_title` REGEXP '^[^a-zA-Z]'   order by p." . $SECTION_FIELD_PREFIX . $orderby . " " . $order;
    	} else {
        	if ($searchchar == 'all') {
                $sql_query = "SELECT p.* FROM " . $SECTION_TABLE . " p WHERE p." . $SECTION_FIELD_PREFIX . "id != 0  AND p.".$SECTION_FIELD_PREFIX."status!='Deleted'  order by p." . $SECTION_FIELD_PREFIX . $orderby . " " . $order;
        	} else {
        	    $ts = $searchchar . "%";
        	    $sql_query = "select p.* from ".$SECTION_TABLE." as p where p.adv_title LIKE '" . $ts . "' AND p.".$SECTION_FIELD_PREFIX."status!='Deleted'  order by " . $SECTION_FIELD_PREFIX . $orderby . " " . $order;
        	}
    	}
	}
	else if ($xtraCondition != 'undefined' && $xtraCondition != "") {
		$ts = "%" . $xtraCondition . "%";
		$sql_query = "select p.* from ".$SECTION_TABLE." as p where (".$xtraCondition.") AND (p.".$SECTION_FIELD_PREFIX."status!='Deleted')  order by " . $SECTION_FIELD_PREFIX . $orderby . " " . $order;
	}
	else
	{
		$sql_query = "SELECT p.* FROM ".$SECTION_TABLE." p WHERE p.".$SECTION_FIELD_PREFIX."id != 0 AND p.".$SECTION_FIELD_PREFIX."status != 'Deleted' order by p.".$SECTION_FIELD_PREFIX.$orderby." ".$order;
	}

    //echo $sql_query;
	$excel_query = "SELECT p.* FROM ".$SECTION_TABLE." p WHERE p.".$SECTION_FIELD_PREFIX."id != 0 AND p.".$SECTION_FIELD_PREFIX."status != 'Deleted' order by p.".$SECTION_FIELD_PREFIX.$orderby." ".$order;
	$excel_result  = $db->select($excel_query);	
	$excelcount = count($excel_result);	
		
		#################################  Paging Query + Paging Code ##################################
		$paging_query = $sql_query;
		$paging_result  = $db->select($paging_query);	
		$count = count($paging_result);
		$per_page = SITE_PAGING_PER_PAGE; //rows per page
		$per_page = 10;
		$pages = ceil($count/$per_page);		
		#################################################################################################
		if ($action_type == "paging")
		{
			if(!empty($_POST['page']))	
			{
				$page = $_POST['page'];
			}  			
		}
		else
		{
			if(!empty($_POST['page']))	
				$page = $_POST['page'];
			else
		 		$page = 1;
		}	
		$list_query = $sql_query;
		if(!empty($per_page) && $_POST['page']!="all")
		{
			$start = ($page-1)*$per_page;
			if($start<0)
			{
				$start=0;
			}
			$list_query .= " limit $start,$per_page";	
		}	
		$result_query  = $db->select($list_query);	
		$total_rows = count($result_query);
		echo $ms = ajaxMsg($_SESSION['msg']);
	
?>
<nav>
	<div class="shorting">
	<ul>
		<?php
        for ($i = 65; $i < 91; $i++) {
            if ($searchchar == chr($i) || $_SESSION['character_search'] == chr($i)) {
                ?><li><a href="javascript:void(0);" class="active" onclick="getAjaxSearchByLetter('Advertisement','<?php echo $SECTION_TABLE; ?>', '<?php echo $SECTION_FIELD_PREFIX; ?>', '<?php echo chr($i); ?>');"><?php echo chr($i); ?></a></li><?php
            } else {
                ?><li><a href="javascript:void(0);" onclick="getAjaxSearchByLetter('Advertisement','<?php echo $SECTION_TABLE; ?>', '<?php echo $SECTION_FIELD_PREFIX; ?>', '<?php echo chr($i); ?>');"><?php echo chr($i); ?></a></li><?php
            }           
        }        
        if($_SESSION['character_search']=='other' || $searchchar == 'other'){
		?>
        <li><a href="javascript:void(0);" class="active" onclick="getAjaxSearchByLetter('Advertisement','<?php echo $SECTION_TABLE; ?>', '<?php echo $SECTION_FIELD_PREFIX; ?>', 'other');">Other</a></li>
        <?php	}else{	?>
		<li><a href="javascript:void(0);" onclick="getAjaxSearchByLetter('Advertisement','<?php echo $SECTION_TABLE; ?>', '<?php echo $SECTION_FIELD_PREFIX; ?>', 'other');">Other</a></li><?php
		}?>
        <?php
        if($_SESSION['character_search']=='all' || $searchchar == 'all'){
		?><li><a href="javascript:void(0);" class="active" onclick="getAjaxSearchByLetter('Advertisement','<?php echo $SECTION_TABLE; ?>', '<?php echo $SECTION_FIELD_PREFIX; ?>', 'all');">All</a></li><?php	
		}else{?>
		<li><a href="javascript:void(0);" onclick="getAjaxSearchByLetter('Advertisement','<?php echo $SECTION_TABLE; ?>', '<?php echo $SECTION_FIELD_PREFIX; ?>', 'all');">All</a></li>
		<?php
		}
		?>		
	</ul>
		<div class="export">
			<a href="<?php echo ADM_INDEX_PARAMETER.$SECTION_MANAGE_PAGE; ?>" class="tooltip" title="Add"><img src="../images/add.png" /></a>
		</div>
		<div class="export">
			<!-- <a href="<?php if($excelcount != "0")echo "export_excel.php?table=tbl_member&prifix=meb_";else echo ADM_INDEX_PARAMETER.ADM_HOME."#Advertisement";?>" class="tooltip" title="Export"><img src="../images/export.png" /></a> -->
		</div>					  
	</div>
</nav>
<div class="clr"></div>
<aside>
	<div class="tab_content_holder second">
		<div class="tab_content_holder_inner">
			<?php if($total_rows > 0){ ?>
				<table width="100%" cellspacing="0" cellpadding="0">
					<tr>						
						<th width="25%"><label onclick="getAjaxSorting('Advertisement','<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>','<?php echo 'meb_id'; ?>','<?php echo $ORDER; ?>','');" >Members Name</label></th>									
						<th width="25%"><label onclick="getAjaxSorting('Advertisement','<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>','<?php echo 'title'; ?>','<?php echo $ORDER; ?>','');" >Advertise Title</label></th>
						<th width="35%"><label onclick="getAjaxSorting('Advertisement','<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>','<?php echo 'description'; ?>','<?php echo $ORDER; ?>','');" >Advertise Description</label></th>								
						<th width="15%">Action</th>
					</tr>
					<?php
					$j = 1;
					for($i=0;$i<$total_rows;$i++)
					{
					?>
					<tr <?php if($i == $j){ echo 'class="light"'; $j = $j + 2;}?>>
						<td><?php 
						$meb_fields = array("meb_name");
						$meb_where  = "meb_id = '".$result_query[$i][$SECTION_FIELD_PREFIX.'meb_id']."'";
						$mebRes 	= $db->selectData(TBL_MEMBER,$meb_fields,$meb_where,$extra="",2);
						echo $mebRes[0]['meb_name']; 
						?></td>
						<td><?php echo $result_query[$i][$SECTION_FIELD_PREFIX.'title']; ?></td>
						<td><?php echo charCut($result_query[$i][$SECTION_FIELD_PREFIX.'description'],"50","..."); ?></td>
						
						<td>
							<a href="view_records.php?sectionName=Advertisement&adv_id=<?php echo $result_query[$i][$SECTION_FIELD_PREFIX."id"];?>" class="fancybox fancybox.ajax tooltip" title="View" data-fancybox-type="ajax"><img src="../images/equale.png" /></a>
							<a href="<?php echo getAdminActionLink2($SECTION_MANAGE_PAGE,$language_prefix,'Edit',$SECTION_FIELD_PREFIX."id",$result_query[$i][$SECTION_FIELD_PREFIX."id"])?>" class="tooltip" title="Edit"><img src="../images/edit.png" /></a>
							<a href="javascript:void(0);" class="tooltip" title="Delete" onclick="javascript:alertBox('Advertisement','<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>',<?php echo $result_query[$i][$SECTION_FIELD_PREFIX."id"]; ?>,'','<?php echo $SECTION_MANAGE_PAGE;?>');"><img src="../images/delete.png" /></a>	
						</td>
					</tr>
					<?php }?>
				</table>
			<?php }else{ ?><div class="no-record">No Records</div> <?php }?>
			<div class="clr"></div>
			<nav>
				<div class="paging">
				<ul>
					<?php
					for($t=1; $t<=$pages; $t++)
					{
						if($page==$t) 
						{
						?>										
						<li><a class="active" href="javascript:void(0);" <?php if($pages > 1){?>onclick="getAjaxPaging('Advertisement','<?php echo $SECTION_TABLE; ?>','<?php echo $SECTION_FIELD_PREFIX; ?>','<?php echo $pages; ?>','<?php echo $t ?>','<?php echo "&orderby=".$orderby."&order=".$order; ?>','<?php echo $SECTION_MANAGE_PAGE;?>');"<?php }?>><?php echo $t ?></a></li>
						<?php
						}
						else 
						{
						?>
						<li><a href="javascript:void(0);" <?php if($pages > 1){?>onclick="getAjaxPaging('Advertisement','<?php echo $SECTION_TABLE; ?>','<?php echo $SECTION_FIELD_PREFIX; ?>','<?php echo $pages; ?>','<?php echo $t ?>','<?php echo "&orderby=".$orderby."&order=".$order; ?>','<?php echo $SECTION_MANAGE_PAGE;?>');"<?php }?>><?php echo $t ?></a></li>
					<?php
						}
					}
					?>          					  	  
            	</ul>            					  
				</div>
			</nav>
		</div>
	</div>               				   
</aside>
			
	<script type="text/javascript">
    $(document).ready(function() {
        $('.tooltip').tooltipster();
		$.fancybox.close();
    });
</script>