<?php
	$directoryActive = ($filename == ADM_VIEW_DIRECTORY || $filename=="") ? "class='active'" : "";
	$memberActive = ($filename == ADM_VIEW_MEMBERS) ? "class='active'" : "";
	$advertiseActive = ($filename == ADM_VIEW_ADVERTISE) ? "class='active'" : "";
	$businessActive = ($filename == ADM_VIEW_BUSINESS) ? "class='active'" : "";
	$individualActive = ($filename == ADM_VIEW_INDIVIDUAL) ? "class='active'" : "";
	$bannerActive = ($filename == ADM_VIEW_FRONT_HOME) ? "class='active'" : "";
	$appActive = ($filename == ADM_VIEW_APP_SETTING)? "class='active'" : "";
	$galleryActive = ($filename == ADM_VIEW_GALLERY)? "class='active'" : "";

	$display_title = getAdminFileTitle($filename);
?>
<ul class="tab_links">
	<li ><div class="tab_link_active"><span><img src="<?php echo IMG_WWW; ?>directory.png" /></span><div id="homeTitle"><h1><?php echo $display_title; ?></h1></div></div></li>
	<li class="tab_link" ><a href="<?php echo ADM_INDEX_PARAMETER.ADM_VIEW_DIRECTORY; ?>" <?php echo $directoryActive; ?> title="Directory">Group</a></li>
	<li class="tab_link" ><a href="<?php echo ADM_INDEX_PARAMETER.ADM_VIEW_MEMBERS; ?>" <?php  echo $memberActive; ?> title="Administrators">Administrators</a></li>
	<li class="tab_link" ><a href="<?php echo ADM_INDEX_PARAMETER.ADM_VIEW_ADVERTISE; ?>" <?php echo $advertiseActive; ?> title="Advertisement">Advertisement</a></li>
	<li class="tab_link" ><a href="<?php echo ADM_INDEX_PARAMETER.ADM_VIEW_FRONT_HOME; ?>" <?php echo $bannerActive; ?> title="Consumer Banner">Consumer&nbsp;Banner</a></li>
	<li class="tab_link" ><a href="<?php echo ADM_INDEX_PARAMETER.ADM_VIEW_GALLERY; ?>" <?php echo $galleryActive; ?> title="Gallery">Gallery</a></li>
	<li class="tab_link" ><a href="<?php echo ADM_INDEX_PARAMETER.ADM_VIEW_APP_SETTING; ?>" <?php echo $appActive; ?> title="App setting">Setting</a></li>
	<!--<li class="tab_link" ><a href="<?php echo ADM_INDEX_PARAMETER.ADM_VIEW_INDIVIDUAL; ?>" <?php echo $individualActive; ?> title="Individual">Individual</a></li> -->
</ul>