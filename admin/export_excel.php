<?php
ob_start();
include_once('../include/includeclass.php');
ini_set('memory_limit', '-1');
/** Error reporting */
	@session_start();
   
  if(empty($_SESSION['adm_id'])){
	header('Location:login.php');
  }
  $table = $_GET["table"];
  $prifix = $_GET["prifix"];
/*exit;*/
error_reporting(0);

date_default_timezone_set('Europe/London');
/** PHPExcel */
require_once '../include/Classes/PHPExcel.php';

//$con = mysql_connect("localhost", "root", "");
//$db_selected = mysql_select_db("nccsolutions", $con);

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set properties
$objPHPExcel->getProperties()->setCreator("Chintan")
        ->setLastModifiedBy("Chintan")
        ->setTitle("Office 2007 XLSX Client Document")
        ->setSubject("Office 2007 XLSX TestClient Document")
        ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
        ->setKeywords("office 2007 openxml php")
        ->setCategory("Clients Report");


// Query

if($table == "tbl_directory")
{
	$select_data_query = "SELECT *,(select dic_name from tbl_dir_category where dic_id = dir_dic_id ) as Dirname,(select sta_name from tbl_state where sta_id = dir_sta_id ) as Statename,(select con_name from tbl_country where con_id = dir_con_id ) as Countryname FROM ".$table." WHERE  `".$prifix."status`!='Deleted' AND `".$prifix."id`!=0 order by ".$prifix."id desc";	
}
else if($table=="tbl_individual_sub")
{
    $select_data_query = "SELECT * FROM ".$table." WHERE  `".$prifix."status`!='Deleted' AND `".$prifix."id`!=0 order by ".$prifix."id desc";
}
else if($table=="tbl_member")
{
    if($_GET['section']=='Adm_business')
    {
        $select_data_query = "SELECT *,(select dir_name from tbl_directory where dir_id = meb_dir_id ) as Dirname FROM ".$table." WHERE  `".$prifix."status`!='Deleted' AND `".$prifix."type`='Business' AND `".$prifix."id`!=0 order by ".$prifix."id desc";
    }
    else
    {
        $select_data_query = "SELECT *,(select dir_name from tbl_directory where dir_id = meb_dir_id ) as Dirname FROM ".$table." WHERE  `".$prifix."status`!='Deleted' AND `".$prifix."id`!=0 order by ".$prifix."id desc";        
    }
}

//ORDER BY cli_id DESC"
//$select_data_query="SELECT * FROM `tbl_clients`,tbl_communication where cli_status='Active' and com_status='Active' and cli_id=com_cli_id";
/*$select_data_query="SELECT * FROM `tbl_clients`
 LEFT JOIN tbl_communication ON cli_id=com_cli_id
 LEFT JOIN tbl_contact ON cli_id=con_cli_id 
LEFT JOIN  tbl_purchase_history ON cli_id=pur_cli_id
where cli_status='Active' and com_status='Active' GROUP BY con_id";
*/
$geted_data = $db->select($select_data_query);

/*echo "<pre>";
print_r($geted_data);
echo "</pre>";
exit;*/
//$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'Clients Details');
//$objPHPExcel->setActiveSheetIndex(0)->mergeCells("A1:N1");
//$objPHPExcel->getActiveSheet()->getStyle('A1:N1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


//$objPHPExcel->setActiveSheetIndex(0) ->setCellValue('O1', 'Client contacts');
//$objPHPExcel->setActiveSheetIndex(0)->mergeCells('O1:S1');
//$objPHPExcel->getActiveSheet()->getStyle('O1:S1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

//$objPHPExcel->setActiveSheetIndex(0)->setCellValue('AE1', 'Product Details');
//$objPHPExcel->setActiveSheetIndex(0)->mergeCells('T1:AI1');
//$objPHPExcel->getActiveSheet()->getStyle('T1:AI1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);





//$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AJ1:AM1');
//$objPHPExcel->getActiveSheet()->getStyle('AJ1:AM1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


//$objPHPExcel->getActiveSheet()->getStyle('A1:IK1')->getFont()->setBold(true);
//$objPHPExcel->getActiveSheet()->getStyle('A3:IK3')->getFont()->setBold(true);

/*$objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('AL1', 'Purchase History');
$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AL1:AR1');
$objPHPExcel->getActiveSheet()->getStyle('AL1:AR1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
*/
if($table=="tbl_directory")
{
    $objPHPExcel->getActiveSheet()->getStyle('A3:IK3')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);  
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20); 
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20); 
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20); 
    $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20); 
    $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20); 
    $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15); 
    $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20); 
    $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20); 
    $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20); 
    $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20); 

    $title_id = 3;
    $continue_val=-1;
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Sr No');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Directory Category');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Directory name');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Directory Address');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Directory City');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Directory State');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Directory Zip');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Directory Country');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Directory office Phone');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Directory Email');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Directory Website');
    $sr = 1;
    $i = 5;
    $static_sr_no=1;
    $start_val=-1;
    foreach ($geted_data as $geted_data) {
        //Get Communication records.
        $start_val=-1;
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val ,$i, $sr);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['Dirname']);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['dir_name']);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['dir_address']);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['dir_city']);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['Statename']);
         $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val,$i, $geted_data['dir_zip']);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['Countryname']);
         $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val,$i, $geted_data['dir_office_phone']);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['dir_email']);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['dir_website']);
        $i++;
        $sr++;
        $var_loop = 0;
    }
}
else if($table=="tbl_individual_sub")
{
    $objPHPExcel->getActiveSheet()->getStyle('A3:B3')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10);  
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);

    $title_id = 3;
    $continue_val=-1;
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Sr No');
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Email');
    $sr = 1;
    $i = 5;
    $static_sr_no=1;
    $start_val=-1;
    foreach ($geted_data as $geted_data) {
        //Get Communication records.
        $start_val=-1;
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val ,$i, $sr);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['inv_email']);
        $i++;
        $sr++;
        $var_loop = 0;
    }
}
else if($table=="tbl_member")
{    
    if($_GET['section']=='Adm_business')
    {
        $objPHPExcel->getActiveSheet()->getStyle('A3:I3')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20); 
        
        $title_id = 3;
        $continue_val=-1;
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Sr No');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Directory Name');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Business Name');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Email');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Secret Question');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Answer');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Phone');
        $sr = 1;
        $i = 5;
        $static_sr_no=1;
        $start_val=-1;
        foreach ($geted_data as $geted_data) {
            //Get Communication records.
            $start_val=-1;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val ,$i, $sr);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['Dirname']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['meb_name']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['meb_username']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['meb_question']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['meb_answer']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['meb_phone']);
            $i++;
            $sr++;
            $var_loop = 0;
        }
    }
    else
    {
        $objPHPExcel->getActiveSheet()->getStyle('A3:I3')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(10); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20); 
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20); 
        
        $title_id = 3;
        $continue_val=-1;
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Sr No');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Directory Name');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Member Name');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Username');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Secret Question');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Answer');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$continue_val,$title_id, 'Phone');
        $sr = 1;
        $i = 5;
        $static_sr_no=1;
        $start_val=-1;
        foreach ($geted_data as $geted_data) {
            //Get Communication records.
            $start_val=-1;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val ,$i, $sr);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['Dirname']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['meb_name']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['meb_username']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['meb_question']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['meb_answer']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(++$start_val, $i, $geted_data['meb_phone']);
            $i++;
            $sr++;
            $var_loop = 0;
        }
    }
}
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
// Redirect output to a client’s web browser (Excel5)
if($_GET['section']=='Adm_business')
{
    $name = "Business";
}
else {
    $nameexp = explode("_", $table);
    $name = ucfirst($nameexp[1]);
}
//header('Content-Type: application/vnd.ms-excel');
//header('Content-Disposition: attachment;filename="'.$name.'_list.xls"');
//header('Cache-Control: max-age=0');
// header("Pragma: no-cache"); 
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save($name.'_list.xls');

//Download header

header('Content-Description: File Transfer');
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment; filename="'.$name.'_list.xls"');
header('Content-Transfer-Encoding: binary');
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');
header('Content-Length: ' . filesize($name.'_list.xls'));
ob_clean();
flush();
readfile($name.'_list.xls');
exit;

?>