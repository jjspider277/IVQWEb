<?php 
$today_date = date('Y-m-d H:i:s');
if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start("ob_gzhandler"); else ob_start();
	include_once('../include/includeclass.php');
	$filename=$_REQUEST['files'];
	$action=$_REQUEST['method'];
	$str=$_SERVER['PHP_SELF']; 
	$exp=explode("/",$str);
	if($filename != ADM_FORGOT)
	{
		isAdminMember();	
	}	
	if(!empty($filename))
	{	
		$middlefile=ADM_MIDD_DIR.$filename;
		if(!file_exists($middlefile.".php"))
		{
			//$middlefile=ADM_MIDD_DIR.ADM_HOME;
			$middlefile=ADM_MIDD_DIR.ADM_VIEW_DIRECTORY;
			
		}
	}
	else
	{
		$middlefile=ADM_MIDD_DIR.ADM_VIEW_DIRECTORY;
		//$middlefile=ADM_MIDD_DIR.ADM_HOME;
	}
	######################################################################################## 
	$display_title = getAdminFileTitle($filename);	//For Display File Title
	#########################################################################################
	
	if(isset($_SESSION['sdm_id']))
	{
		$q = "select * from ".TBL_ADMIN." where adm_id = ".$_SESSION['sdm_id'];
		$data = $db->select($q);	
	}
	else
	{
		$data = array();
		$data[0] = array("adm_username"=>"Guest","adm_login_time"=>date("Y-m-d H:i:s"));
	}
?>

<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>B2B Mobile Services</title>
<meta name="viewport" content="width=1024, initial-scale=1">
<link rel="shortcut icon" href="<?php echo IMG_WWW; ?>favicon.ico" type="image/x-icon" />
<!--[if lt IE 9]>
<script src="<?php echo JS_FOLDER_WWW?>html5.js" type="text/javascript"></script>
<![endif]-->

<?php $ajax = AJAX_FOLDER_WWW; 
 $mid= FRT_MIDD_FOLD_WWW;
?>

<script type="text/javascript">
var ajax_folder='<?php echo $ajax?>';
var middle_folder="<?php echo $mid?>";
</script>

<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="<?php echo ADM_CSS_WWW;?>styles.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo ADM_CSS_WWW;?>1140.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo ADM_CSS_WWW;?>tooltipster.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo ADM_CSS_WWW;?>prettyCheckable.css" type="text/css" media="screen" />
<!--[if IE]>
	<link rel="stylesheet" href="<?php echo FRT_CSS;?>ie.css" type="text/css" media="screen" />
<![endif]-->

<script type="text/javascript" src="<?php echo JS_FOLDER_WWW?>jquery-1.11.0.min.js"></script>
<script src="<?php echo JS_FOLDER_WWW?>jquery-ui.js" type="text/javascript"></script>
<?php if($filename!='gallery') { ?>
<link rel="stylesheet" href="<?php echo JS_FOLDER_WWW?>jquery-ui.css" type="text/css">
<script type="text/javascript" src="<?php echo JS_FOLDER_WWW?>fremworks.js"></script>
<script type="text/javascript" src="<?php echo JS_FOLDER_WWW;?>placeholders.min.js"></script>
<?php } ?>
<script type="text/javascript" src="<?php echo JS_FOLDER_WWW?>jquery.tooltipster.js"></script>
<script type="text/javascript" src="<?php echo JS_FOLDER_WWW?>Fancybox/jquery.fancybox.js?v=2.0.6"></script>
<script type="text/javascript" src="<?php echo JS_FOLDER_WWW?>prettyCheckable.js"></script>
<script type="text/javascript" src="<?php echo JS_FOLDER_WWW?>jquery-validation.js"></script>
<script type="text/javascript" src="<?php echo ROOT_WWW?>js/jquery.maskedinput.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo JS_FOLDER_WWW?>Fancybox/jquery.fancybox.css?v=2.0.6" media="screen" />
</head>
<body>
	<div id="header">
		<div class="log-out"><a href="<?php echo ADM_INDEX_PARAMETER.ADM_LOGOUT?>">Log out</a></div>	
		<div class="logo"><img src="<?php echo IMG_WWW; ?>logo_new.png" width="376" height="31" alt=""></div>
		<a href="<?php echo ADM_INDEX_PARAMETER.ADM_VIEW_DIRECTORY?>"><div class="but_sty1" >IVQMobile</div></a>
	</div>
	<?php include_once($middlefile.'.php');?>
	<div id="footer" >
		<div class="copyrights">&copy; <span>Powered by</span> Intelli-Touch Apps</div>
	</div>
	<div id="test" style="display:none;">
	<div class="alert-box">
		<div class="error-title">
			<img src="<?php echo IMG_WWW; ?>error-icon.png" alt="" >
			<span>Warning</span>
			<a title="Close" class="close" href="javascript:;"></a>
		</div>
		<div class="error-txt">Error coming soon... </div>
		<div class="button-div">
			<a href="#">Ok</a>
			<a href="#">Cancel</a>
		</div>
		<div class="clr"></div>
	</div>
</div>
<?php if($filename!='gallery') { ?>
<script type="text/javascript">
    $(document).ready(function() {
    	$('input[type="checkbox"]').prettyCheckable();
        $('.tooltip').tooltipster();
		$('.fancybox').fancybox({
		'width':500,
		'height':500,
		});
		 //setTimeout("$('.myerr').fadeOut('slow')",10001); 
    });
    $(".serviceTxt").next().addClass("close");
		$(".serviceTxt").on("click",function(){
		if($(this).next().hasClass("close"))
		{
			$(this).next().removeClass("close");
			$(this).next().addClass("open");
		}
		else
		{
			$(this).next().removeClass("open");
			$(this).next().addClass("close");
		}
		});
</script>
<?php } ?>
<style type="text/css">
.open{
	display: block;
}
.close{
	display: none;
}
</style>
</body>
</html>