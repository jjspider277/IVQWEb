<?php

$SECTION_MANAGE_PAGE = ADM_VIEW_APP_SETTING;

$APP_SETTING_TABLE = TBL_APP_SETTING;

if(isset($_POST)){	
	$data = $_POST;	
}
if ($data['app_setting'] == 'Save')
{
	$query='update '.$APP_SETTING_TABLE.' set value="'.$data['app_apk'].'" where meta="apk"';
	$db->query($query);

	$query='update '.$APP_SETTING_TABLE.' set value="'.$data['app_ios'].'" where meta="ipa"';
	$db->query($query);

    $query='update '.$APP_SETTING_TABLE.' set value="'.$data['app_title'].'" where meta="app_title"';
    $db->query($query);

    $query='update '.$APP_SETTING_TABLE.' set value="'.$data['app_description'].'" where meta="app_description"';
    $db->query($query);

	$URL = getAdminURL($SECTION_MANAGE_PAGE,'',""); 
	redirect($URL);
	exit;
}

$app_apk = '';
$app_ios = '';

$dir_fields = array("*");
$dir_where  = "";
$dirRes 	= $db->selectData($APP_SETTING_TABLE,$dir_fields,$dir_where,$extra="",2);

if(count($dirRes)>0) {
	foreach($dirRes as $dirdata) {

		if($dirdata['meta'] == 'apk') {
			$app_apk = $dirdata['value'];
		}

		if($dirdata['meta'] == 'ipa') {
			$app_ios = $dirdata['value'];
		}

        if($dirdata['meta'] == 'app_title') {
            $app_title = $dirdata['value'];
        }

        if($dirdata['meta'] == 'app_description') {
            $app_description = $dirdata['value'];
        }
	}
}


?>
<section>
	<article id="page" >
		<header>
			<?php include_once(ADM_FOLDER."menu.php"); ?>
			<div class="tab_content_holder">
				<div class="tab_content_holder_inner">
					
					<div id="Description " class="new_full_roow">
						<form method="post" id="frm_front_desc" name="frm_front_desc" action="<?php echo getAdminURL($SECTION_MANAGE_PAGE); ?>">
							<div class="block-part">
								<div class="main-row">
									<label>Android .apk link</label>
									<input type="text" tabindex="1" name="app_apk" id="app_apk" value="<?php echo $app_apk; ?>" />
								</div>
								<div class="main-row">
									<label>IOS .ipa link</label>
									<input type="text" tabindex="2" name="app_ios" id="app_ios" value="<?php echo $app_ios; ?>" />
								</div>
                                <div class="main-row">
                                    <label>Landing page title</label>
                                    <textarea name="app_title" id="app_title" style="height:100px;" tabindex="3"><?php echo $app_title; ?></textarea>
                                </div>
                                <div class="main-row">
                                    <label>Landing page description</label>
                                    <textarea name="app_description" id="app_description" style="height:100px;" tabindex="4"><?php echo $app_description; ?></textarea>
                                </div>


								<div class="main-row">
									<label>&nbsp;</label>
									<input type="submit" tabindex="3" name="app_setting" id="app_setting" value="Save" />
								</div>
							</div>
							<div class="clr"></div>	
						</form>
					</div>
			
				</div>
			</div>
		</header>
		<div id="updatediv"></div>
	</article>
</section>
<script type="text/javascript" src="<?php echo AJAX_FOLDER_WWW?>getAjaxAdmin.js"></script>