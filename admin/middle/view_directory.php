<script type="text/javascript" src="<?php echo AJAX_FOLDER_WWW?>getAjaxAdmin.js"></script>
<?php 

#################Directory###################

$SECTION_FIELD_PREFIX_DIRECTORY = 'dir_';
$SECTION_AUTO_ID_DIRECTORY = $SECTION_FIELD_PREFIX_DIRECTORY.'id';
$SECTION_VIEW_PAGE_DIRECTORY = ADM_VIEW_DIRECTORY;
$SECTION_MANAGE_PAGE_DIRECTORY = ADM_MANAGE_DIRECTORY;
$SECTION_TABLE_DIRECTORY = TBL_DIRECTORY;

#################Directory###################

//Category Listing...
$cat_fields = array("dic_id","dic_name");
$cat_where  = "dic_status = 'Active'";
$catRes 	= $db->selectData("tbl_dir_category",$cat_fields,$cat_where,$extra="",2);

//State Listing...
$sta_fields = array("sta_id","sta_name");
$sta_where  = "sta_status = 'Active'";
$staRes 	= $db->selectData("tbl_state",$sta_fields,$sta_where,$extra="",2);

//Directory Listing...
$dir_fields = array("dir_id","dir_name");
$dir_where  = "dir_status = 'Active'";
$dirRes 	= $db->selectData(TBL_DIRECTORY,$dir_fields,$dir_where,$extra="",2);
?>

<script type="text/javascript" language="javascript">
$(document).ready(function(){
	getpagelisting('Directory','<?php echo $SECTION_TABLE_DIRECTORY?>','<?php echo $SECTION_FIELD_PREFIX_DIRECTORY?>','<?php echo $SECTION_MANAGE_PAGE_DIRECTORY;?>');
});

</script>
<section>
	<article id="page" >
		<header>
			<?php include_once(ADM_FOLDER."menu.php"); ?>
			<div class="tab_content_holder">
				<div class="tab_content_holder_inner">
					<!--div class="serviceTxt"><a href="#">Search</a></div-->					
					<div class="border-txt">
					<div class="serviceTxt">Search <span></span></div>
					<div id="directory">
						<form method="post" id="search_dir" name="search_dir" action="" >
							<div class="first-col dir">
								 <div class="main-row">
								 	<label>Group Category</label>
									 <select name="dir_cat" tabindex="1" id="dir_cat" alt="Type to search directory category">
										<option value="">Select Category</option>
										<?php						
										for($i=0;$i<count($catRes);$i++)
										{
											if($catRes[$i]['dic_id']==$catRes) { $select="selected='selected'"; } else { $select=""; }
										?>
											<option value="<?php echo $catRes[$i]['dic_id']; ?>" <?php echo $select; ?> ><?php echo $catRes[$i]['dic_name']; ?></option>
										<?php
										}
										?>
									</select>
								 </div>
								 <div class="main-row">
    								 <label>State</label>    								 
									 <select tabindex="5" name="dir_state" id="dir_state" alt="Type to search state">
										<option value="">Select State</option>
										<?php						
										for($i=0;$i<count($staRes);$i++)
										{
											if($staRes[$i]['sta_id']==$staRes) { $select="selected='selected'"; } else { $select=""; }
										?>
											<option value="<?php echo $staRes[$i]['sta_id']; ?>" <?php echo $select; ?> ><?php echo $staRes[$i]['sta_name']; ?></option>
										<?php
										}
										?>
									</select>
								 </div>
								<!--  <div class="main-row">
									<div class="fromdate">
									<label>From</label>
							 		<input type="text" readonly="readonly" tabindex="9" name="dir_from_date" id="dir_from_date"></input>
							 		</div>
							 		<div class="todate">
							 		<label>To</label>
							 		<input type="text" readonly="readonly" tabindex="10" name="dir_to_date" id="dir_to_date"></input>
							 		</div>
							 	</div> -->
							</div>
							<div class="middle-col dir">
								 <div class="main-row">
								 	  <label>Group Name</label>
								 	  <input tabindex="2" type="text" name="dir_name" id="dir_name"></input>
								 </div>								 
								 <div class="main-row">
    								 <label>Country</label>    								 
									 <select tabindex="6" name="dir_country" id="dir_country" alt="Type to search country">
										<option value="257">U.S.A</option>
									</select>
								 </div>
							</div>
							<div class="last-col dir">
								 <div class="main-row city">
								 	  <label>City</label>
								 	  <input tabindex="3" type="text" name="dir_city" id="dir_city"></input>
								 </div>
								 <div class="main-row zip">
								 	  <label>Zip</label>
								 	  <input tabindex="4" type="text" name="dir_zip" id="dir_zip"></input>
								 </div>
								 <div class="main-row city">
								 	  <label>Email</label>
								 	  <input tabindex="7" type="text" name="dir_email" id="dir_email"></input>
								 </div>
								 <div class="main-row zip">
								 	  <label>Phone</label>
								 	  <input type="text" tabindex="8" name="dir_phone" id="dir_phone"></input>
								 </div>								 
								 <div class="clr"></div>
							</div>
							<div class="main-row">
								<input type="button" tabindex="11" value="Search" onclick="getLiveSearch('Directory','<?php echo $SECTION_TABLE_DIRECTORY?>','<?php echo $SECTION_FIELD_PREFIX_DIRECTORY?>');"></input>
								<input type="button" id="bus_reset" onclick="resetForm('search_dir','Directory','<?php echo $SECTION_TABLE_DIRECTORY?>','<?php echo $SECTION_FIELD_PREFIX_DIRECTORY?>','<?php echo $SECTION_MANAGE_PAGE_DIRECTORY; ?>');" tabindex="12" value="Clear" />
							</div>
						</form>
						<div class="clr"></div>
					</div>
						<div class="clr"></div>
					</div>
				</div>
			</div>
		</header>
		<div id="updatediv"></div>
	</article>
</section>
