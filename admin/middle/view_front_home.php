<?php

include('../javascript/ajax/app_new_design/image_resize/helpers/common.php');
require_once '../javascript/ajax/app_new_design/image_resize/lib/WideImage.php';

$SECTION_MANAGE_PAGE = ADM_VIEW_FRONT_HOME;

$DES_PREFIX = 'des_';
$DES_TABLE = TBL_FRONT_DESC;

$BANNER_PREFIX = 'ban_';
$BANNER_TABLE = TBL_FRONT_BANNER;

if(!empty($_REQUEST['type']))
{
	$consType = $_REQUEST['type'];
}
else
{
	$consType  = "Description";
}

if(!empty($_REQUEST[$BANNER_PREFIX.'id']))
	$AUTO_ID = $_REQUEST[$BANNER_PREFIX.'id'];
else	
	$AUTO_ID  = $BANNER_PREFIX.'id';

//Description data
$desRes = frontData();
$des_title = $desRes[0]['des_title'];
$des_description = $desRes[0]['des_description'];

if(isset($_POST)){	
	$data = $_POST;	
}

if ($data['frt_desc_submit'] == 'Save')
{
	unset($updateData);
	$updateData[$DES_PREFIX.'title'] 		= $data[$DES_PREFIX.'title'];
	$updateData[$DES_PREFIX.'description']  = $data[$DES_PREFIX.'description'];
	$updateData[$DES_PREFIX.'updated_id'] 	= getAdminSessionId();
	$updateData[$DES_PREFIX.'updated_date'] = $today_date;
	$updId = $db->updateData($DES_TABLE, $updateData, $des_where);

	$URL = getAdminURL($SECTION_MANAGE_PAGE,'',"type=Description"); 
	redirect($URL);
	exit;
}
if ($data['frt_banner_submit'] == 'Add')
{
	unset($add_values);
	$ban_filename_name=$_FILES['ban_image']['name'];

	$add_values[$BANNER_PREFIX.'title'] 		= $data[$BANNER_PREFIX.'title'];
	$add_values[$BANNER_PREFIX.'url'] 		= $data[$BANNER_PREFIX.'url'];
	$add_values[$BANNER_PREFIX.'status'] 		= $data[$BANNER_PREFIX.'status'];
	$add_values[$BANNER_PREFIX.'created_id'] 	= getAdminSessionId();
	$add_values[$BANNER_PREFIX.'created_date']  = $today_date;	
	$addId = $db->insertData($BANNER_TABLE, $add_values);
	if(is_dir(UPLOAD_DIR_BANNER_FILE.$addId) == false)
	{ 			
		mkdir(UPLOAD_DIR_BANNER_FILE.$addId);
        chmod(UPLOAD_DIR_BANNER_FILE.$addId, 0777);
	}
	//$ban_image = oneImageResizeUpload(UPLOAD_DIR_BANNER_FILE.$addId."/",$_FILES['ban_image'],"","","","250","157");
    //$ban_image = uploadIVA(UPLOAD_DIR_BANNER_FILE.$addId."/",$_FILES['ban_image']);
	if($_FILES['ban_image']!='')
	{
        $ban_image_name = $_FILES['ban_image']['name'];
        $ban_image = $_FILES['ban_image']['tmp_name'];

        $img_new= WideImage::load($ban_image);

        //$resized = $img_new->crop("right", "bottom", 185, 50);
        $resized = $img_new->resizeDown("185", "75", "outside");
        $resized->saveToFile(UPLOAD_DIR_BANNER_FILE.$addId."/".$ban_image_name);
        chmod($resized, 0777);

		$updateData[$BANNER_PREFIX.'image'] = $ban_image_name;
		$SECTION_WHERE = $BANNER_PREFIX."id = ".$addId;
		$db->updateData($BANNER_TABLE, $updateData, $SECTION_WHERE);
	}

	$URL = getAdminURL($SECTION_MANAGE_PAGE,'',"type=Banner"); 
	redirect($URL);
	exit;
}

?>
<section>
	<article id="page" >
		<header>
			<?php include_once(ADM_FOLDER."menu.php"); ?>
			<div class="tab_content_holder">
				<div class="tab_content_holder_inner">
					<div class="main-row" style="margin:5px 0px 20px 5px;">
						<?php
							$rad_DescriptionCheck = ($consType=="Description") ? "checked='checked'" : "";
							$rad_BannerCheck = ($consType=="Banner") ? "checked='checked'" : "";
						?>
						<input type="radio" name="frt_flag" id="rad_Description" <?php echo $rad_DescriptionCheck; ?> value="Description" onclick="changeStatus(this.value);">
						<label class="radio-label" for="rad_Description">Consumer Description</label>
						<input type="radio" <?php echo $rad_BannerCheck; ?> name="frt_flag" id="rad_Banner" onclick="changeStatus(this.value);" value="Banner">
						<label class="radio-label" for="rad_Banner">Consumer Banner</label>
					</div>
					<div id="Description">
						<form method="post" id="frm_front_desc" name="frm_front_desc" action="<?php echo getAdminURL($SECTION_MANAGE_PAGE); ?>">
							<div class="block-part">
								<div class="main-row">
									<label>Title</label>
									<input type="text" tabindex="1" name="des_title" id="des_title" value="<?php echo $des_title; ?>" />
								</div>
								<div class="main-row">
									<label>Description</label>
									<textarea name="des_description" id="des_description" style="height:100px;" tabindex="2"><?php echo $des_description; ?></textarea>
								</div>
								<div class="main-row">
									<label>&nbsp;</label>
									<input type="submit" tabindex="4" name="frt_desc_submit" id="frt_desc_submit" value="Save" />
								</div>
							</div>
							<div class="clr"></div>	
						</form>
					</div>
					<div id="Banner">
						<form method="post"  enctype="multipart/form-data" id="frm_front_banner" name="frm_front_banner" action="<?php echo getAdminURL($SECTION_MANAGE_PAGE); ?>">
							<div class="block-part">
								<div class="main-row">
									<label>Title <?php echo getRequiredIcon()?></label>
									<input type="text" tabindex="1" name="ban_title" id="ban_title" />
								</div>
								<div class="main-row">
									<label>URL</label>
									<input type="text" tabindex="2" name="ban_url" id="ban_url" />
								</div>
								<div class="main-row">
									<label>Image <?php echo getRequiredIcon()?></label>
									<input type="file" tabindex="3" name="ban_image" id="ban_image" />
									<?php
									if($action=="Update" && $ban_image!="")
									{
										echo "<input type='hidden' name='edit_ban_image' value='".$ban_image."' />";
									}
									?>
								</div>
								<div class="main-row">
									<label>Status</label>
									<input type="radio" tabindex="4" name="ban_status" id="ban_active" <?php if($ban_status=="Active"){ echo "checked='checked'"; } else { echo "checked='checked'"; }?> value="Active" />
									<label class="radio-label" for="ban_active">Active</label>
									<input type="radio"  tabindex="4" <?php if($ban_status=="Inactive"){ echo "checked='checked'"; } ?> name="ban_status" id="ban_inactive" value="Inactive" />
									<label class="radio-label" for="ban_inactive">Inactive</label>
								</div>
								<div class="main-row">
									<label>&nbsp;</label>
									<input type="submit" tabindex="5" name="frt_banner_submit" id="frt_banner_submit" value="Add" />
								</div>
							</div>
							<div class="clr"></div>	
						</form>
					</div>
				</div>
			</div>
		</header>
		<div id="updatediv"></div>
	</article>
</section>
<script type="text/javascript" src="<?php echo AJAX_FOLDER_WWW?>getAjaxAdmin.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	<?php if($consType=="Description"){ ?>
		$("#Description").show();
		$("#Banner").hide();
	<?php }else{ ?>
		$("#Banner").show();
		$("#Description").hide();
		getpagelisting('Banner','<?php echo $BANNER_TABLE; ?>','<?php echo $BANNER_PREFIX;?>','<?php echo $SECTION_MANAGE_PAGE; ?>');
	<?php } ?>
	$.validator.addMethod("validImage", function(value, element) {
    	return this.optional(element) || /\.(gif|jpg|jpeg|png|GIF|JPG|JPEG|PNG)$/i.test(value);
    	//|mov|MOV|MPG|mpg|VOB|vob|wmv|WMV
	}, "Please select only image");
	$("#frm_front_desc").validate({
		rules: {
			des_title : {
				required : true
			},
			des_description : {
				required : true
			}
		},
		messages: {
			des_title : {
				required : "Please enter title"
			},
			des_description : {
				required : "Please enter description"
			}
		}
	});
	$("#frm_front_banner").validate({
		rules: {
			ban_title : {
				required : true
			},
			ban_url : {
				url : true
			},
			ban_image : {
				required : true,
				validImage : true
			}
		},
		messages: {
			ban_title : {
				required : "Please enter title"
			},
			ban_url : {
				url : "Please enter valid URL"
			},
			ban_image : {
				required : "Please select image",
				validImage : "Please select only image"
			}
		}
	});
});
function changeStatus(status)
{
	if(status=="Description")
	{
		$("#Banner").hide();
		$("#Description").show();
		$("label.error").html("");
		$('input:text').val('');
		$("#updatediv").html('');
	}
	else
	{
		$("#Banner").show();
		$("#Description").hide();
		$("label.error").html("");
		$('input:text').val('');
		getpagelisting('Banner','<?php echo $BANNER_TABLE; ?>','<?php echo $BANNER_PREFIX;?>','<?php echo $SECTION_MANAGE_PAGE; ?>');
	}
}
</script>