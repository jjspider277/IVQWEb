<?php
$SECTION_MANAGE_PAGE = ADM_VIEW_APP_SETTING;
?>

<!-- jQuery and jQuery UI (REQUIRED) -->
<link rel="stylesheet" type="text/css" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="javascript/js/jquery.min.js"></script>
<script src="javascript/js/jquery-ui.min.js"></script>

<!-- elFinder CSS (REQUIRED) -->
<link rel="stylesheet" type="text/css" href="elfinder/css/elfinder.min.css">
<link rel="stylesheet" type="text/css" href="elfinder/css/theme.css">

<!-- elFinder JS (REQUIRED) -->
<script src="elfinder/js/elfinder.min.js"></script>

<!-- elFinder translation (OPTIONAL) -->
<script src="elfinder/js/i18n/elfinder.ru.js"></script>

<!-- elFinder initialization (REQUIRED) -->
<script type="text/javascript" charset="utf-8">
    // Documentation for client options:
    // https://github.com/Studio-42/elFinder/wiki/Client-configuration-options
    $(document).ready(function() {
        $('#elfinder').elfinder({
            url : 'elfinder/php/connector.minimal.php',  // connector URL (REQUIRED)
            onlyMimes: ["image", "video"],
            commands : ['open', 'reload', 'home', 'up', 'back', 'forward', 'getfile', 'quicklook',
                'download', 'rm', 'duplicate', 'rename', 'mkdir', 'upload', 'copy',
                'cut', 'paste', 'edit', 'search', 'view','sort'],
            //'resize','extract', 'archive',
            //contextmenu : false,
            commandsOptions : {
                // "quicklook" command options. For additional extensions
                quicklook : {
                    autoplay : true,
                    jplayer  : 'extensions/jplayer'
                },
                // help dialog tabs
                help : { view : ['shortcuts'] }
            }
        });
    });
</script>

<style>
    .elfinder-help .ui-state-default{
        display: none !important;
    }
    .elfinder-help .ui-state-active {
        display: block !important;
    }
    .elfinder-resize-control input[type="text"] {
        width: auto;
        height: auto;
    }
</style>

<section>
	<article id="page" >
		<header>
			<?php include_once(ADM_FOLDER."menu.php"); ?>
			<div class="tab_content_holder">
                <div id="elfinder"></div>
			</div>
		</header>
		<div id="updatediv"></div>
	</article>
</section>
<script type="text/javascript" src="<?php echo AJAX_FOLDER_WWW?>getAjaxAdmin.js"></script>