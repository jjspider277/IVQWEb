<script type="text/javascript" src="<?php echo AJAX_FOLDER_WWW; ?>getAjaxAdmin.js"></script>
<?php
#################################################################
$SECTION_FIELD_PREFIX = 'meb_';
$SECTION_AUTO_ID = $SECTION_FIELD_PREFIX.'id';
$SECTION_VIEW_PAGE = ADM_VIEW_BUSINESS;
$SECTION_MANAGE_PAGE = ADM_MANAGE_BUSINESS;
$SECTION_TABLE = TBL_MEMBER;
#################################################################
//Directory Listing...
$dir_fields = array("dir_id","dir_name");
$dir_where  = "dir_status = 'Active'";
$dirRes 	= $db->selectData(TBL_DIRECTORY,$dir_fields,$dir_where,$extra="",2);
	
if(!empty($_REQUEST[$SECTION_FIELD_PREFIX.'id']))
	$SECTION_AUTO_ID = $_REQUEST[$SECTION_FIELD_PREFIX.'id'];
else	
	$SECTION_AUTO_ID  = $SECTION_FIELD_PREFIX.'id';		

if(isset($_POST)){	
	$data = $_POST;	
}
if ($data['member_submit'] == 'Save Member' || $data['meb_addNew'] == 'Add New')
{
	unset($add_values);		
	$add_values[$SECTION_FIELD_PREFIX . 'dir_id'] 		= $data[$SECTION_FIELD_PREFIX . 'dir'];
	$add_values[$SECTION_FIELD_PREFIX . 'name'] 		= $data[$SECTION_FIELD_PREFIX . 'name'];
	$add_values[$SECTION_FIELD_PREFIX . 'username'] = $data[$SECTION_FIELD_PREFIX . 'username'];
	if($data[$SECTION_FIELD_PREFIX . 'password']!="")
	{
		$add_values[$SECTION_FIELD_PREFIX . 'password'] = md5($data[$SECTION_FIELD_PREFIX . 'password']);
	}
	$add_values[$SECTION_FIELD_PREFIX . 'question'] = $data[$SECTION_FIELD_PREFIX . 'question'];
	$add_values[$SECTION_FIELD_PREFIX . 'answer'] 	= $data[$SECTION_FIELD_PREFIX . 'answer'];
	$add_values[$SECTION_FIELD_PREFIX . 'phone'] 	= $data[$SECTION_FIELD_PREFIX . 'phone'];
	$add_values[$SECTION_FIELD_PREFIX . 'status'] 	= "Inactive";
	$add_values[$SECTION_FIELD_PREFIX . 'type'] 	= "Business";
	
	if($action != 'Update'){
		$add_values[$SECTION_FIELD_PREFIX . 'created_id'] 			= getAdminSessionId();
		$add_values[$SECTION_FIELD_PREFIX . 'created_date'] 		= $today_date;	
		$GPDetail_result = $db->insertData($SECTION_TABLE, $add_values); 
    }else{
		$add_values[$SECTION_FIELD_PREFIX . 'updated_id'] 			= getAdminSessionId();
		$add_values[$SECTION_FIELD_PREFIX . 'updated_date'] 		= $today_date;		
		$SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$SECTION_AUTO_ID;
		$GPDetail_result = $db->updateData($SECTION_TABLE, $add_values, $SECTION_WHERE); 
	}
    if($data['meb_addNew'] == 'Add New')
	{
		$URL = getAdminURL($SECTION_MANAGE_PAGE); 
		redirect($URL);
		exit;
	}
	else
	{
		$URL = getAdminURL($SECTION_VIEW_PAGE); 
		redirect($URL);
		exit;
	}
}

if ($action == 'Edit' && $SECTION_AUTO_ID != $SECTION_FIELD_PREFIX."id")
{
    $list_query = getSelectList($SECTION_TABLE, $SECTION_FIELD_PREFIX);
    $list_query .= " AND " . $SECTION_FIELD_PREFIX . "id = " . $SECTION_AUTO_ID. " AND ".$SECTION_FIELD_PREFIX."type = 'Business'"; 
    $result_query = $db->select($list_query); 
    $meb_dir_id = $result_query[0][$SECTION_FIELD_PREFIX.'dir_id'];
	$meb_name = $result_query[0][$SECTION_FIELD_PREFIX.'name'];
    $meb_username = $result_query[0][$SECTION_FIELD_PREFIX.'username'];
    //$meb_password = $result_query[0][$SECTION_FIELD_PREFIX.'encripted_pass']; 
    $meb_question = $result_query[0][$SECTION_FIELD_PREFIX.'question']; 
    $meb_answer = $result_query[0][$SECTION_FIELD_PREFIX.'answer']; 
    $meb_phone = $result_query[0][$SECTION_FIELD_PREFIX.'phone']; 
    
    $action = "Update";
    $action_url = getAdminURL($SECTION_MANAGE_PAGE,$action);

}
if ($action == '')
{
	$action = "Add";
    $action_url = getAdminURL($SECTION_MANAGE_PAGE,$action);
}
?>
<section>
		<article id="page">
				<header>
    				<ul class="tab_links ">
    					<li ><div class="tab_link_active inner"><span><img src="<?php echo IMG_WWW; ?>directory.png" /></span><h1>Business Setup</h1>
							<div class="view"><a class="tooltip" title="Go Back" href="<?php echo ADM_INDEX_PARAMETER.$SECTION_VIEW_PAGE."#members"; ?>"><img src="<?php echo IMG_WWW; ?>back-blue.png"></a></div>
						</div></li>    					
    				</ul>
				</header>		   
				 <aside>	
				<form name="manage_member" method="post" action="<?php echo $action_url; ?>" id="manage_member"> 	<!-- onsubmit="javascript:userEmailCheck();"	 -->
				<input name="meb_id" id="meb_id" value="<?php echo $SECTION_AUTO_ID; ?>" type="hidden">
				<div class="tab_content_holder directory">
				 	  <div class="tab_content_holder_inner">
					  	   <div class="block-part">
						   		<h1>Business Administrator</h1>
								<div class="main-row">
									 <label>Directory Name <?php echo getRequiredIcon()?></label>
									 <select name="meb_dir" id="meb_dir" tabindex="1" alt="Type to search directory name">
										<option value="">Select Directory</option>
										<?php						
										for($i=0;$i<count($dirRes);$i++)
										{
											if($dirRes[$i]['dir_id']==$meb_dir_id) { $select="selected='selected'"; } else { $select=""; }
										?>
											<option value="<?php echo $dirRes[$i]['dir_id']; ?>" <?php echo $select; ?> ><?php echo $dirRes[$i]['dir_name']; ?></option>
										<?php
										}
										?>
									</select>
								</div>
								<div class="main-row">
									<label>Business Name <?php echo getRequiredIcon()?></label>
									<input type="text" name="meb_name" id="meb_name" tabindex="2" value="<?php echo $meb_name;?>" />
								</div>
								<div class="main-row">
									<label>User Name (Valid Email) <?php echo getRequiredIcon()?></label>
									<input type="text" name="meb_username" id="meb_username" tabindex="3" value="<?php echo $meb_username;?>" onblur="javascript:userEmailCheck();"/>
									<div id="userEmailmsg"></div>
								</div>
								<div class="main-row">
									 <label>Confirm Email <?php echo getRequiredIcon()?></label>
									 <input type="text" name="meb_confirm" id="meb_confirm" tabindex="4" value="<?php echo $meb_username;?>" autocomplete="off" />
								</div>
								<div class="main-row">
									 <label>Password <?php echo getRequiredIcon()?></label>
									 <input type="password" name="meb_password" id="meb_password" tabindex="5" value="<?php echo $meb_password;?>"  autocomplete="off"/>
								</div>
								<div class="main-row">
									 <label>Confirm Password <?php echo getRequiredIcon()?></label>
									 <input name="meb_confirm_password" id="meb_confirm_password" tabindex="6" value="<?php echo $meb_password;?>" type="password">
								</div>
								<div class="main-row">
									 <label>Secret Question <?php echo getRequiredIcon()?></label>
									 <input type="text" name="meb_question" id="meb_question"  tabindex="7" value="<?php echo $meb_question;?>"/>
								</div>
								<div class="main-row">
									 <label>Answer <?php echo getRequiredIcon()?></label>
									 <input type="text" name="meb_answer" id="meb_answer" tabindex="8" value="<?php echo $meb_answer;?>" />
								</div>
								<div class="main-row">
									 <label>Mobile Phone <?php echo getRequiredIcon()?></label>
									 <input type="text" name="meb_phone" id="meb_phone" tabindex="9" value="<?php echo $meb_phone;?>"/>
								</div>								
								<div class="main-row">
									 <label>&nbsp;</label>
									 <input name="method" id="method" value="<?php echo $action; ?>" type="hidden"> 
									 <input type="submit" value="Save Member" name="member_submit" tabindex="10" id="member_submit" >
									 <?php if($action!="Update")
									{?>
									<input type="submit" tabindex="11" name="meb_addNew" id="meb_addNew" value="Add New" />
									<?php } ?>							 
								</div>
						   </div>
						   <div class="clr"></div>
					  </div>
				 </div> 
			</form>
		</aside>
	</article>
</section>
<script>

$(document.body).on('click', "#member_submit,#meb_addNew", function(e){
	if($("#userEmailmsg label").text().length > 5)
	{
		return false;		
	}
	else
	{
		return true;
	}
});

$(document).ready(function(){

	$.validator.addMethod("phoneValidate", function(value, element) {
    	return this.optional(element) || /^[0-9\-\+\,\.\)\(\s]+$/i.test(value);
	}, "Only numbers,-,+ and , allowed");
	$("#manage_member").validate({
		rules: {
			meb_dir : {
				required : true
			},
			meb_name : {
				required : true
			},
			meb_username : {
				required : true,
				email : true
			},
			meb_confirm :{
				required: true,
				email : true,
				equalTo: "#meb_username"
			},
			meb_password : {
				<?php if($action != "Update"){ ?> required : true <?php } ?>
			},
			meb_confirm_password : {
				<?php if($action != "Update"){ ?> required : true, <?php } ?>
				equalTo: "#meb_password"
			},
			meb_question : {
				required : true
			},
			meb_answer : {
				required : true
			},
			meb_phone : {
				required : true,
				phoneValidate : true
			}
			
		},
		messages: {
			meb_dir : {
				required : "Please select Directory"
			},
			meb_name : {
				required : "Please enter member name"
			},
			meb_username : {
				required : "Please enter username",
				email : "Please enter valid Email address"
			},
			meb_confirm: {
				required: "Please enter username",				
				email : "Please enter valid Email address",
				equalTo: "Please enter the same username"
			},
			meb_password : {
				required : "Please enter password"
			},
			meb_confirm_password: {
				required: "Please enter password",				
				equalTo: "Please enter the same password"
			},
			meb_question : {
				required : "Please enter question"
			},
			meb_answer : {
				required : "Please enter answer"
			},
			meb_phone : {
				required : "Please enter phone",
				phoneValidate : "Please enter only digits"
			}
		}
	});
});
function userEmailCheck(){	
	var userName = document.getElementById('meb_username').value;	
	var usrId = document.getElementById('meb_id').value; 
	if(usrId != "")
	{
		checkUserEmail(userName,usrId);
	}
	else
	{
		checkUserEmail(userName,'');
	}
}
</script>

	
