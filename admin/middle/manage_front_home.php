<?php

include('../javascript/ajax/app_new_design/image_resize/helpers/common.php');
require_once '../javascript/ajax/app_new_design/image_resize/lib/WideImage.php';

$SECTION_MANAGE_PAGE = ADM_MANAGE_FRONT_HOME;
$SECTION_VIEW_PAGE = ADM_VIEW_FRONT_HOME;
$DES_PREFIX = 'des_';
$DES_TABLE = TBL_FRONT_DESC;

$BANNER_PREFIX = 'ban_';
$BANNER_TABLE = TBL_FRONT_BANNER;

if(!empty($_REQUEST['type']))
{
	$consType = $_REQUEST['type'];
}
else
{
	$consType  = "Description";
}

if(!empty($_REQUEST[$BANNER_PREFIX.'id']))
	$AUTO_ID = $_REQUEST[$BANNER_PREFIX.'id'];
else	
	$AUTO_ID  = $BANNER_PREFIX.'id';


if(isset($_POST)){	
	$data = $_POST;	
}

if ($data['frt_banner_submit'] == 'Save')
{
	unset($add_values);
	$ban_filename_name=$_FILES['ban_image']['name'];
    $SECTION_WHERE = $BANNER_PREFIX."id = ".$AUTO_ID;
	$add_values[$BANNER_PREFIX.'title'] 		= $data[$BANNER_PREFIX.'title'];
	$add_values[$BANNER_PREFIX.'url'] 		= $data[$BANNER_PREFIX.'url'];
	$add_values[$BANNER_PREFIX.'status'] 		= $data[$BANNER_PREFIX.'status'];
	$add_values[$BANNER_PREFIX.'created_id'] 	= getAdminSessionId();
	$add_values[$BANNER_PREFIX.'created_date']  = $today_date;	
	$addId = $db->updateData($BANNER_TABLE, $add_values,$SECTION_WHERE);
    $addId = $AUTO_ID;
	if(is_dir(UPLOAD_DIR_BANNER_FILE.$addId) == false)
	{ 			
		mkdir(UPLOAD_DIR_BANNER_FILE.$addId);
        chmod(UPLOAD_DIR_BANNER_FILE.$addId, 0777);
	}
	//$ban_image = oneImageResizeUpload(UPLOAD_DIR_BANNER_FILE.$addId."/",$_FILES['ban_image'],"","","","250","157");
    //$ban_image = uploadIVA(UPLOAD_DIR_BANNER_FILE.$addId."/",$_FILES['ban_image']);
	if($_FILES['ban_image']!='') {
        $ban_image_name = $_FILES['ban_image']['name'];
        $ban_image = $_FILES['ban_image']['tmp_name'];

        $img_new= WideImage::load($ban_image);

        //$resized = $img_new->crop("right", "bottom", 185, 50);
        $resized = $img_new->resizeDown("185", "75", "outside");
        $resized->saveToFile(UPLOAD_DIR_BANNER_FILE.$addId."/".$ban_image_name);
        chmod($resized, 0777);

		$updateData[$BANNER_PREFIX.'image'] = $ban_image_name;
		//$SECTION_WHERE = $BANNER_PREFIX."id = ".$addId;
		$db->updateData($BANNER_TABLE, $updateData, $SECTION_WHERE);
	}

	$URL = getAdminURL($SECTION_VIEW_PAGE,'',"type=Banner"); 
	redirect($URL);
	exit;
}

if($action == "Edit"){
    $list_query = getSelectList($BANNER_TABLE, $BANNER_PREFIX);
    $list_query .= " AND " . $BANNER_PREFIX . "id = " . $AUTO_ID; 
    $result_query = $db->select($list_query);    
    $ban_id = $result_query[0][$BANNER_PREFIX.'id'];
    $ban_title = $result_query[0][$BANNER_PREFIX.'title'];
    $ban_url = $result_query[0][$BANNER_PREFIX.'url'];
    $ban_status = $result_query[0][$BANNER_PREFIX.'status'];
    
    $action = "Update";
    $action_url = getAdminURL($SECTION_MANAGE_PAGE,$action);
}

?>
<section>
	<article id="page" >
		<header>		
     <ul class="tab_links ">
    					<li ><div class="tab_link_active inner"><span><img src="<?php echo IMG_WWW; ?>directory.png" /></span><h1>Edit Banner</h1>
							<div class="view"><a class="tooltip" title="Go Back" href="<?php echo ADM_INDEX_PARAMETER.$SECTION_VIEW_PAGE; ?>"><img src="<?php echo IMG_WWW; ?>back-blue.png"></a></div>
						</div></li>    					
    	</ul> 
  </header> 
     <aside>	
     <form method="post"  enctype="multipart/form-data" id="frm_front_banner" name="frm_front_banner" action="<?php echo $action_url; ?>">
     <input name="ban_id" id="ban_id" value="<?php echo $AUTO_ID; ?>" type="hidden">
			<div class="tab_content_holder directory">
				<div class="tab_content_holder_inner">					
					
					<div id="Banner">
						
							<div class="block-part">
								<div class="main-row">
									<label>Title <?php echo getRequiredIcon()?></label>
									<input type="text" tabindex="1" name="ban_title" id="ban_title" value="<?php echo $ban_title;?>" />
								</div>
								<div class="main-row">
									<label>URL</label>
									<input type="text" tabindex="2" name="ban_url" id="ban_url" value="<?php echo $ban_url;?>" />
								</div>
								<div class="main-row">
									<label>Image <?php echo getRequiredIcon()?></label>
									<input type="file" tabindex="3" name="ban_image" id="ban_image" />
									<?php
									if($action=="Update" && $ban_image!="")
									{
										echo "<input type='hidden' name='edit_ban_image' value='".$ban_image."' />";
									}
									?>
								</div>
								<div class="main-row">
									<label>Status</label>
									<input type="radio" tabindex="4" name="ban_status" id="ban_active" <?php if($ban_status=="Active"){ echo "checked='checked'"; } else { echo "checked='checked'"; }?> value="Active" />
									<label class="radio-label" for="ban_active">Active</label>
									<input type="radio"  tabindex="4" <?php if($ban_status=="Inactive"){ echo "checked='checked'"; } ?> name="ban_status" id="ban_inactive" value="Inactive" />
									<label class="radio-label" for="ban_inactive">Inactive</label>
								</div>
								<div class="main-row">
									<label>&nbsp;</label>
									<input type="submit" tabindex="5" name="frt_banner_submit" id="frt_banner_submit" value="Save" />
								</div>
							</div>
							<div class="clr"></div>	
						
					</div>
				</div>
			</div>
   </form>
		</aside>	
		
	</article>
</section>
<script type="text/javascript" src="<?php echo AJAX_FOLDER_WWW?>getAjaxAdmin.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	
 
	$.validator.addMethod("validImage", function(value, element) {
    	return this.optional(element) || /\.(gif|jpg|jpeg|png|GIF|JPG|JPEG|PNG)$/i.test(value);
    	//|mov|MOV|MPG|mpg|VOB|vob|wmv|WMV
	}, "Please select only image");

	$("#frm_front_banner").validate({
		rules: {
			ban_title : {
				required : true
			},
			ban_url : {
				url : true
			},
			ban_image : {
    <?php if($action != "Update"){ ?>
				required : true,
    <?php } ?>
				validImage : true
			}
		},
		messages: {
			ban_title : {
				required : "Please enter title"
			},
			ban_url : {
				url : "Please enter valid URL"
			},
			ban_image : {
				required : "Please select image",
				validImage : "Please select only image"
			}
		}
	});
});

</script>