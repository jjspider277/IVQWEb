<script type="text/javascript" src="<?php echo AJAX_FOLDER_WWW?>getAjaxAdmin.js"></script>
<?php
#################################################################
$SECTION_FIELD_PREFIX = 'adv_';
$SECTION_AUTO_ID = $SECTION_FIELD_PREFIX.'id';
$SECTION_VIEW_PAGE = ADM_VIEW_ADVERTISE;
$SECTION_MANAGE_PAGE = ADM_MANAGE_ADVERTISE;
$SECTION_TABLE = TBL_ADVERTISE;
#################################################################
//directory Listing...
$dir_fields = array("dir_id","dir_name");
$dir_where  = "dir_status = 'Active'";
$dirRes 	= $db->selectData(TBL_DIRECTORY,$dir_fields,$dir_where,$extra="",2);

if(!empty($_REQUEST[$SECTION_FIELD_PREFIX.'id']))
	$SECTION_AUTO_ID = $_REQUEST[$SECTION_FIELD_PREFIX.'id'];
else	
	$SECTION_AUTO_ID  = $SECTION_FIELD_PREFIX.'id';		

if(isset($_POST)){	
	$data = $_POST;	
}

if ($data['advertise_submit'] == 'Save Advertise' || $data['adv_addNew'] == 'Add New')
{
	unset($add_values);		
	$adv_filename_name=$_FILES['adv_filename']['name'];
	$adv_file_type = $data['adv_file_type'];
	if($adv_file_type=="Left" || $adv_file_type=="Right")
	{
		$height = "463";
		$width = "173";
	}
	else if($adv_file_type=="Middle")
	{
		$height = "385";
		$width = "570";
	}
	else if($adv_file_type=="Bottom")
	{
		$height = "86";
		$width = "727";
	}
	else
	{
		$height = "0";
		$width = "0";
	}
	$add_values[$SECTION_FIELD_PREFIX . 'title'] 		= $data[$SECTION_FIELD_PREFIX . 'title'];
	$add_values[$SECTION_FIELD_PREFIX . 'url'] 			= $data[$SECTION_FIELD_PREFIX . 'url'];
	$add_values[$SECTION_FIELD_PREFIX . 'description'] 	= $data[$SECTION_FIELD_PREFIX . 'description'];
	if($action!="Update")
	{
		$add_values[$SECTION_FIELD_PREFIX . 'flag'] 		=  $data[$SECTION_FIELD_PREFIX . 'flag'];
	}
	$add_values[$SECTION_FIELD_PREFIX . 'status'] 		= "Active";
	if($action!="Update")
	{
		if($data["adv_flag"]=="Directory")
		{
			$add_values[$SECTION_FIELD_PREFIX . 'dir_id'] 		= $data[$SECTION_FIELD_PREFIX . 'dir_id'];
		}
		else
		{
			$add_values[$SECTION_FIELD_PREFIX . 'dir_id'] =0;
		}
	}
	if($action != 'Update'){												
	    $add_values[$SECTION_FIELD_PREFIX . 'created_id'] 			= getAdminSessionId();
        $add_values[$SECTION_FIELD_PREFIX . 'created_date'] 		= $today_date;
        $advId = $db->insertData($SECTION_TABLE, $add_values);
        if(is_dir(UPLOAD_DIR_ADVERTISE_FILE.$advId) == false)
		{ 			
			mkdir(UPLOAD_DIR_ADVERTISE_FILE.$advId);
		}
		$adv_filename = ImageResizeUploadHW(UPLOAD_DIR_ADVERTISE_FILE.$advId."/",$_FILES['adv_filename'],"","org_","",$width,$height);
		if($adv_filename==1)
		{
			$_SESSION["err_file"]="Plese select image ".$width."X".$height; 
			$db->delete("delete from ".$SECTION_TABLE." where adv_id=".$advId);
			rmdir(UPLOAD_DIR_ADVERTISE_FILE.$advId);
			$_SESSION['add_detail_error'][]=$_POST;
			$URL = getAdminURL($SECTION_MANAGE_PAGE);
			redirect($URL);
			exit; 
		}
		else
		{
			$updateData[$SECTION_FIELD_PREFIX . 'file_type'] = $adv_file_type;
			$updateData[$SECTION_FIELD_PREFIX . 'filename'] = $adv_filename;		
			$SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$advId;
			$db->updateData($SECTION_TABLE, $updateData, $SECTION_WHERE);
		}
    }
    else
    {
    	if($data["edit_adv_filename"]!="" && $adv_filename_name!="")
		{
			$adv_filename = ImageResizeUploadHW(UPLOAD_DIR_ADVERTISE_FILE.$SECTION_AUTO_ID."/",$_FILES['adv_filename'],"","org_","",$width,$height);

			if($adv_filename==1)
			{
				$_SESSION["err_file"]="Plese select image ".$width."X".$height;
				$_SESSION['add_detail_error'][]=$_POST;
				$URL = getAdminURL($SECTION_MANAGE_PAGE,$action,"adv_id=".$SECTION_AUTO_ID);
				redirect($URL);
				exit; 
			}
			else
			{
				unlink(UPLOAD_DIR_ADVERTISE_FILE.$SECTION_AUTO_ID."/".$data["edit_adv_filename"]);
				/*unlink(UPLOAD_DIR_ADVERTISE_FILE.$SECTION_AUTO_ID."/"."org_".$data["edit_adv_filename"]);*/
			}
		}
		else
		{
			$adv_filename=$data["edit_adv_filename"];
		}
		$add_values[$SECTION_FIELD_PREFIX . 'file_type'] = $adv_file_type;
		$add_values[$SECTION_FIELD_PREFIX . 'filename'] = $adv_filename;
    	$add_values[$SECTION_FIELD_PREFIX . 'updated_id'] 			= getAdminSessionId();
       	$add_values[$SECTION_FIELD_PREFIX . 'updated_date'] 		= $today_date;		
		$SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$SECTION_AUTO_ID;
		$db->updateData($SECTION_TABLE, $add_values, $SECTION_WHERE);
	}
	if($data['adv_addNew'] == 'Add New')
	{
		$URL = getAdminURL($SECTION_MANAGE_PAGE); 
		redirect($URL);
		exit;
	}
	else
	{
		$URL = getAdminURL($SECTION_VIEW_PAGE."#advertisement"); 
		redirect($URL);
		exit;
	}
}

if ($action == 'Edit' && $SECTION_AUTO_ID != $SECTION_FIELD_PREFIX."id")
{
    $list_query = getSelectList($SECTION_TABLE, $SECTION_FIELD_PREFIX);
    $list_query .= " AND " . $SECTION_FIELD_PREFIX . "id = " . $SECTION_AUTO_ID. " AND ".$SECTION_FIELD_PREFIX."status = 'Active'"; 
    $result_query = $db->select($list_query); 
   	$adv_dir_id = $result_query[0][$SECTION_FIELD_PREFIX.'dir_id'];
   	$diredit_fields = array("dir_name");
	$dirrdit_where  = "dir_id = ".$adv_dir_id;
	$direditRes 	= $db->selectData(TBL_DIRECTORY,$diredit_fields,$dirrdit_where,$extra="",2);
	if($direditRes>0)
		$editdir=$direditRes[0]["dir_name"];
	else
		$editdir="Type to serach directory name";
	$adv_title = $result_query[0][$SECTION_FIELD_PREFIX.'title'];
	$adv_url = $result_query[0][$SECTION_FIELD_PREFIX.'url'];
    $adv_filename = $result_query[0][$SECTION_FIELD_PREFIX.'filename'];
    $adv_flag = $result_query[0][$SECTION_FIELD_PREFIX.'flag'];
    $adv_file_type = $result_query[0][$SECTION_FIELD_PREFIX.'file_type']; 
    $adv_description = $result_query[0][$SECTION_FIELD_PREFIX.'description']; 
    $action = "Update";
    $action_url = getAdminURL($SECTION_MANAGE_PAGE,$action,"adv_id=".$SECTION_AUTO_ID);
}
if($action=="")
{
	$action = "Add";
	$action_url = getAdminURL($SECTION_MANAGE_PAGE,$action);
}
if($_SESSION['add_detail_error'][0]!="")
{
	$result_query[]=$_SESSION['add_detail_error'][0];
	$adv_dir_id = $result_query[0][$SECTION_FIELD_PREFIX.'dir_id'];
	$dir_fields = array("dir_name");
	$dir_where  = "dir_id = ".$adv_dir_id;
	$direditRes 	= $db->selectData(TBL_DIRECTORY,$diredit_fields,$dirrdit_where,$extra="",2);
	if($direditRes>0)
		$editdir=$direditRes[0]["dir_name"];
	else
		$editdir="Type to serach directory name";
	$adv_title = $result_query[0][$SECTION_FIELD_PREFIX.'title'];
	$adv_url = $result_query[0][$SECTION_FIELD_PREFIX.'url'];
	$adv_filename = $result_query[0]['edit_adv_filename'];
	$adv_flag = $result_query[0][$SECTION_FIELD_PREFIX.'flag'];
	$adv_file_type = $result_query[0][$SECTION_FIELD_PREFIX.'file_type']; 
	$adv_description = $result_query[0][$SECTION_FIELD_PREFIX.'description']; 
	unset($_SESSION['add_detail_error']);
}
?>
<section>
	<article id="page">
		<header>
			<ul class="tab_links">
			<li>
				<div class="tab_link_active inner">
					<span><img src="<?php echo IMG_WWW; ?>directory.png" /></span><h1>Advertisement</h1>
					<div class="view">
						<a class="tooltip" title="Go Back" href="<?php echo ADM_INDEX_PARAMETER.$SECTION_VIEW_PAGE."#advertisement"; ?>">
						<img src="<?php echo IMG_WWW; ?>back-blue.png"></a>
					</div>
				</div>
			</li>    					
			</ul>
		</header>		   
		<aside>	
			<form name="manage_advertise" enctype="multipart/form-data" method="post" action="<?php echo $action_url; ?>" id="manage_advertise"> 		
				<div class="tab_content_holder directory">
				 	<div class="tab_content_holder_inner">
						<div class="block-part">
							<h1>Advertisement Information</h1>
							<div class="main-row" id="removeError">
								<label>Directory Name <?php echo getRequiredIcon()?></label>
    							<select name="adv_dir_id" id="adv_dir_id" tabindex="1" alt="Type to search directory name">
    								<option value="">Select Directory</option>
    								<?php						
    								for($i=0;$i<count($dirRes);$i++)
    								{
    									if($dirRes[$i]['dir_id']==$adv_dir_id) { $select="selected='selected'"; } else { $select=""; }
    								?>
    								<option value="<?php echo $dirRes[$i]["dir_id"]; ?>" <?php echo $select; ?> ><?php echo $dirRes[$i]["dir_name"]; ?></option>
    								<?php
    								}
    								?>
    							</select>
							</div>
							<div class="main-row">
								<label>Advertise Title <?php echo getRequiredIcon()?></label>
								<input type="text" tabindex="2" name="adv_title" value="<?php echo $adv_title; ?>" id="adv_title"></input>
							</div>
							<div class="main-row">
								<label>Advertise URL</label>
								<input type="text" tabindex="3" name="adv_url" value="<?php echo $adv_url; ?>" id="adv_url"></input>
							</div>
							<div class="main-row">
								<label>Advertise Category</label>
								<?php
								$Global = ($adv_flag == "Global") ? "checked='checked'" : "checked='checked'";
								$Directory = ($adv_flag == "Directory") ? "checked='checked'" : "";
								$disabled = ($action=="Update") ? "disabled='disabled'" : "";
								?>
								<input type="radio" name="adv_flag" tabindex="4" id="rad_Global" <?php echo $Global; echo $disabled; ?> value="Global">
								<label class="radio-label" for="rad_Global"> Global </label>
								<input type="radio" name="adv_flag" tabindex="4" id="rad_Directory" <?php echo $Directory; echo $disabled; ?> value="Directory">
								<label class="radio-label" for="rad_Directory"> Directory </label>
							</div>
							<div class="main-row">	
								<label>Advertise Align</label>
								<?php
								$left = ($adv_file_type == "Left") ? "checked='checked'" : "checked='checked'";
								$middle = ($adv_file_type == "Middle") ? "checked='checked'" : "";
								$right = ($adv_file_type == "Right") ? "checked='checked'" : "";
								$bottom = ($adv_file_type == "Bottom") ? "checked='checked'" : "";
								$disabled = ($action=="Update") ? "disabled='disabled'" : "";
								?>
								<input type="radio" name="adv_file_type" tabindex="5" id="left" <?php echo $left; echo $disabled; ?> value="Left">
								<label class="radio-label" for="left"> Left <br /> (173X463)</label>
								<input type="radio" name="adv_file_type" tabindex="5" id="middle" <?php echo $middle; echo $disabled; ?> value="Middle">
								<label class="radio-label" for="middle"> Middle <br /> (570X385)</label>
								<input type="radio" name="adv_file_type" tabindex="5" id="right" <?php echo $right; echo $disabled; ?> value="Right">
								<label class="radio-label" for="right"> Right <br /> (173X463)</label>
								<div class="clr"></div>
								</br>
								<input type="radio" name="adv_file_type" tabindex="5" id="bottom" <?php echo $bottom; echo $disabled; ?> value="Bottom">
								<label class="radio-label" for="bottom"> Bottom <br /> (727X86)</label>
							</div>
							<div class="main-row">
								<label>Advertise Description</label>
								<textarea name="adv_description" style="height:70px;" id="adv_description" tabindex="6"><?php echo $adv_description; ?></textarea>
							</div>
							<div class="main-row">
								<label>Advertise File <?php echo getRequiredIcon()?></label>
								<input type="file" tabindex="7" name="adv_filename" id="adv_filename"></input>
								<?php
								if($_SESSION["err_file"]!="")
								{
									echo '<label class="error">'.$_SESSION["err_file"].'</label>';
									$_SESSION["err_file"]="";
								}
								if($action=="Update" && $adv_filename!="")
								{
									echo '<input type="hidden" name="adv_file_type" value="'.$adv_file_type.'">';
									echo "<img style='width:100px; height:100px; margin-top:5px;' src='".UPLOAD_WWW_ADVERTISE_FILE.$SECTION_AUTO_ID."/".$adv_filename."' />";
									echo "<input type='hidden' name='edit_adv_filename' value='".$adv_filename."' />";
								}
                            	?>
							</div>
							<!-- <div class="clr"></div>
							<br /> -->
							<div class="main-row">
								<label>&nbsp;</label>
								<input name="method" id="method" value="<?php echo $action; ?>" type="hidden">
								<input type="submit" value="Save Advertise" name="advertise_submit" tabindex="8" id="advertise_submit" >
								<?php if($action!="Update")
								{?>
								<input type="submit" tabindex="9" name="adv_addNew" id="adv_addNew" value="Add New" />
								<?php } ?>
							</div>
						</div>
						<div class="clr"></div>
					</div>
				</div> 
			</form>
		</aside>
	</article>
</section>
<script>
$(document).ready(function(){
	<?php if($adv_flag!="Directory") { ?>
	$("span.custom-combobox input").attr({"disabled": true});
	$("span.custom-combobox a.custom-combobox-toggle").hide();
	$("#adv_dir_id").attr({"disabled": true});
	<?php 
	}
	if(isset($_GET["method"])){
	?>
	$("span.custom-combobox input").attr({"placeholder" : '<?php echo mysql_real_escape_string($editdir); ?>'});
	<?php } ?>
	$("input[name$='adv_flag']").click(function() {
        test = $(this).val();
        if(test=="Global")
        {
    	    $("span.custom-combobox input").attr({"disabled": true});
			$("span.custom-combobox a.custom-combobox-toggle").hide();
			$("span.custom-combobox input").attr({"placeholder" : 'Type to search directory name'});
			$("#adv_dir_id").attr({"disabled": true});
			$("#removeError label.error").html("");
		}
    	else
    	{
    		<?php if($action!="Update"){?>
    		$("span.custom-combobox input").attr({"disabled": false});
			$("span.custom-combobox a.custom-combobox-toggle").show();
			$("#adv_dir_id").attr({"disabled": false});
			<?php } ?>
		}
    }); 
	$.validator.addMethod("validImage", function(value, element) {
    	return this.optional(element) || /\.(gif|jpg|jpeg|png|GIF|JPG|JPEG|PNG)$/i.test(value);
	}, "Please select only image");
	$("#manage_advertise").validate({
		rules: {
			adv_dir_id : {
				required : true
			},
			adv_title : {
				required : true
			},
			adv_url : {
				url : true
			},
			adv_filename : {
				<?php if($action!="Update"){?>
				required : true,
				<?php } ?>
				validImage : true
			}	
		},
		messages: {
			adv_dir_id : {
				required : "Please select directory name"
			},
			adv_title : {
				required : "Please enter advertisement title"
			},
			adv_url : {
				url : "Please enter valid advertisement URL"
			},
			adv_filename : {
				required : "Please select advertisement file",
				validImage : "Please select only image"
			}
		}
	});
	<?php if($action=="Update"){?>
	$("span.custom-combobox input").attr({"disabled": true});
	$("span.custom-combobox a.custom-combobox-toggle").hide();
	$("span.custom-combobox input").attr({"value": $("span.custom-combobox input").attr("placeholder")});
	$("span.custom-combobox a.custom-combobox-toggle").hide();
	<?php } ?>
});
</script>