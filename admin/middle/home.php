<script type="text/javascript" src="<?php echo AJAX_FOLDER_WWW?>getAjaxAdmin.js"></script>
<?php 
#################Member######################

$SECTION_FIELD_PREFIX = 'meb_';
$SECTION_AUTO_ID = $SECTION_FIELD_PREFIX.'id';
$SECTION_ORDER_BY = $SECTION_FIELD_PREFIX.'sort_order'.','.$SECTION_FIELD_PREFIX.'id';
$SECTION_VIEW_PAGE = MEB_VIEW_MEMBERS;
$SECTION_MANAGE_PAGE = MEB_MANAGE_MEMBERS;
$SECTION_TABLE = TBL_MEMBER;

#################Member######################

#################Directory###################

$SECTION_FIELD_PREFIX_DIRECTORY = 'dir_';
$SECTION_AUTO_ID_DIRECTORY = $SECTION_FIELD_PREFIX_DIRECTORY.'id';
$SECTION_VIEW_PAGE_DIRECTORY = ADM_VIEW_DIRECTORY;
$SECTION_MANAGE_PAGE_DIRECTORY = ADM_MANAGE_DIRECTORY;
$SECTION_TABLE_DIRECTORY = TBL_DIRECTORY;

#################Directory###################

################# Advertisememt ###################

$SECTION_FIELD_PREFIX_ADVERTISE = 'adv_';
$SECTION_AUTO_ID_ADVERTISE = $SECTION_FIELD_PREFIX_ADVERTISE.'id';
$SECTION_VIEW_PAGE_ADVERTISE = ADM_VIEW_ADVERTISE;
$SECTION_MANAGE_PAGE_ADVERTISE = ADM_MANAGE_ADVERTISE;
$SECTION_TABLE_ADVERTISE = TBL_ADVERTISE;

################# Advertisememt ###################
//Category Listing...
$cat_fields = array("dic_id","dic_name");
$cat_where  = "dic_status = 'Active'";
$catRes 	= $db->selectData("tbl_dir_category",$cat_fields,$cat_where,$extra="",2);

//State Listing...
$sta_fields = array("sta_id","sta_name");
$sta_where  = "sta_status = 'Active'";
$staRes 	= $db->selectData("tbl_state",$sta_fields,$sta_where,$extra="",2);

//Directory Listing...
$dir_fields = array("dir_id","dir_name");
$dir_where  = "dir_status = 'Active'";
$dirRes 	= $db->selectData(TBL_DIRECTORY,$dir_fields,$dir_where,$extra="",2);

//directory Listing...
$dir_fields = array("dir_id","dir_name");
$dir_where  = "dir_status = 'Active'";
$dirRes 	= $db->selectData(TBL_DIRECTORY,$dir_fields,$dir_where,$extra="",2);

?>
<script type="text/javascript" language="javascript">

var sectionName = window.location.hash.substr(1);

$(document).ready(function(){

//$("#popup").fancybox().trigger('click');
if(sectionName == "" || sectionName == "directory")
{

	getpagelisting('Directory','<?php echo $SECTION_TABLE_DIRECTORY?>','<?php echo $SECTION_FIELD_PREFIX_DIRECTORY?>','<?php echo $SECTION_MANAGE_PAGE_DIRECTORY;?>');
}
else if(sectionName == "members")
{

	getpagelisting('Members','<?php echo $SECTION_TABLE?>','<?php echo $SECTION_FIELD_PREFIX?>','<?php echo $SECTION_MANAGE_PAGE;?>');
}
else if(sectionName == "advertisement")
{

	getpagelisting('Advertisement','<?php echo $SECTION_TABLE_ADVERTISE; ?>','<?php echo $SECTION_FIELD_PREFIX_ADVERTISE;?>','<?php echo $SECTION_MANAGE_PAGE_ADVERTISE; ?>');
}

});


</script>
<section>
	<article id="page" >
		<header>
			<ul class="tab_links do_tabs">
				<li ><div class="tab_link_active"><span><img src="../images/directory.png" /></span><div id="homeTitle"></div></div></li>
				<li class="tab_link" ><a href="#directory" onclick="getpagelisting('Directory','<?php echo $SECTION_TABLE_DIRECTORY?>','<?php echo $SECTION_FIELD_PREFIX_DIRECTORY?>','<?php echo $SECTION_MANAGE_PAGE_DIRECTORY;?>');">Directory</a></li>
				<li class="tab_link" ><a href="#members" onclick="getpagelisting('Members','<?php echo $SECTION_TABLE?>','<?php echo $SECTION_FIELD_PREFIX?>','<?php echo $SECTION_MANAGE_PAGE;?>');">Administrators</a></li>
				<li class="tab_link" ><a href="#advertisement" onclick="getpagelisting('Advertisement','<?php echo $SECTION_TABLE_ADVERTISE; ?>','<?php echo $SECTION_FIELD_PREFIX_ADVERTISE; ?>','<?php echo $SECTION_MANAGE_PAGE_ADVERTISE; ?>');">Advertisement</a></li>
			</ul>

			<div class="tab_content_holder">
				<div class="tab_content_holder_inner">
					<div id="directory">
						<form method="post" id="search_dir" name="search_dir" action="" >
							<div class="first-col dir">
								 <div class="main-row">
								 	<label>Directory Category</label>    								 
									 <select name="dir_cat" tabindex="1" id="dir_cat" alt="Type to search directory category">
										<option value="">Select Category</option>
										<?php						
										for($i=0;$i<count($catRes);$i++)
										{
											if($catRes[$i]['dic_id']==$catRes) { $select="selected='selected'"; } else { $select=""; }
										?>
											<option value="<?php echo $catRes[$i]['dic_id']; ?>" <?php echo $select; ?> ><?php echo $catRes[$i]['dic_name']; ?></option>
										<?php
										}
										?>
									</select>
								 </div>
								 <div class="main-row">
    								 <label>State</label>    								 
									 <select tabindex="5" name="dir_state" id="dir_state" alt="Type to search state">
										<option value="">Select State</option>
										<?php						
										for($i=0;$i<count($staRes);$i++)
										{
											if($staRes[$i]['sta_id']==$staRes) { $select="selected='selected'"; } else { $select=""; }
										?>
											<option value="<?php echo $staRes[$i]['sta_id']; ?>" <?php echo $select; ?> ><?php echo $staRes[$i]['sta_name']; ?></option>
										<?php
										}
										?>
									</select>
								 </div>
								<!--  <div class="main-row">
									<div class="fromdate">
									<label>From</label>
							 		<input type="text" readonly="readonly" tabindex="9" name="dir_from_date" id="dir_from_date"></input>
							 		</div>
							 		<div class="todate">
							 		<label>To</label>
							 		<input type="text" readonly="readonly" tabindex="10" name="dir_to_date" id="dir_to_date"></input>
							 		</div>
							 	</div> -->
							</div>
							<div class="middle-col dir">
								 <div class="main-row">
								 	  <label>Directory Name</label>
								 	  <input tabindex="2" type="text" name="dir_name" id="dir_name"></input>
								 </div>								 
								 <div class="main-row">
    								 <label>Country</label>    								 
									 <select tabindex="6" name="dir_country" id="dir_country" alt="Type to search country">
										<option value="257">U.S.A</option>
									</select>
								 </div>
							</div>
							<div class="last-col dir">
								 <div class="main-row city">
								 	  <label>City</label>
								 	  <input tabindex="3" type="text" name="dir_city" id="dir_city"></input>
								 </div>
								 <div class="main-row zip">
								 	  <label>Zip</label>
								 	  <input tabindex="4" type="text" name="dir_zip" id="dir_zip"></input>
								 </div>
								 <div class="main-row city">
								 	  <label>Email</label>
								 	  <input tabindex="7" type="text" name="dir_email" id="dir_email"></input>
								 </div>
								 <div class="main-row zip">
								 	  <label>Phone</label>
								 	  <input type="text" tabindex="8" name="dir_phone" id="dir_phone"></input>
								 </div>								 
								 <div class="clr"></div>
							</div>
							<div class="main-row">
								<input type="button" tabindex="11" value="Search" onclick="getLiveSearch('Directory','<?php echo $SECTION_TABLE_DIRECTORY?>','<?php echo $SECTION_FIELD_PREFIX_DIRECTORY?>');"></input>
								<input type="button" id="bus_reset" onclick="resetForm('search_dir','Directory','<?php echo $SECTION_TABLE_DIRECTORY?>','<?php echo $SECTION_FIELD_PREFIX_DIRECTORY?>','<?php echo $SECTION_MANAGE_PAGE_DIRECTORY; ?>');" tabindex="12" value="Clear" />
							</div>
						</form>
						<div class="clr"></div>
					</div>
					<div id="members">
						<form method="post" id="search_meb" name="search_meb" action="" >
							<div class="block-part">
								<div class="main-row">
    								<label>Directory Name</label>    								 
    								<select name="dir_list" id="dir_list" tabindex="1" alt="Type to search directory name">
    									<option value="">Select Directory</option>
    									<?php						
    									for($i=0;$i<count($dirRes);$i++)
    									{
    										if($dirRes[$i]['dir_id']==$dirRes) { $select="selected='selected'"; } else { $select=""; }
    									?>
    										<option value="<?php echo $dirRes[$i]['dir_id']; ?>" <?php echo $select; ?> ><?php echo $dirRes[$i]['dir_name']; ?></option>
    									<?php
    									}
    									?>
    								</select>
								</div>
								<div class="main-row">
							 	  <label>Member Name</label>
							 	  <input type="text" tabindex="2" name="meb_name" id="meb_name"></input>
							 	</div>
							 	<div class="main-row">
							 	  <label>User Name</label>
							 	  <input type="text" tabindex="3" name="meb_username" id="meb_username"></input>
							 	</div>
								<div class="main-row">
							 	  <label>Phone</label>
							 	  <input type="text" name="meb_phone" tabindex="4" id="meb_phone"></input>
								</div>
								<!-- <div class="main-row">
									<div class="fromdate">
									<label>From</label>
							 		<input type="text" readonly="readonly" tabindex="5" name="meb_from_date" id="meb_from_date"></input>
							 		</div>
							 		<div class="todate">
							 		<label>To</label>
							 		<input type="text" readonly="readonly" tabindex="6" name="meb_to_date" id="meb_to_date"></input>
							 		</div>
							 	</div> -->
								<div class="main-row">
							 		<label>&nbsp;</label>
							 		<input type="button" tabindex="7" value="Search" onclick="getLiveSearch('Members','<?php echo $SECTION_TABLE?>','<?php echo $SECTION_FIELD_PREFIX?>','','');"></input>
							 		<input type="button" id="bus_reset" onclick="resetForm('search_meb','Members','<?php echo $SECTION_TABLE?>','<?php echo $SECTION_FIELD_PREFIX?>','<?php echo $SECTION_MANAGE_PAGE; ?>');" tabindex="8" value="Clear" />
							 	</div>
								<div class="clr"></div>	
							</div>
						</form>
						<div class="clr"></div>
					</div>
					<div id="advertisement">
						<form method="post" id="search_adv" name="search_adv" action="" >
							<div class="block-part">
								<div class="main-row">
									<label>Directory Name</label>    								 
    								<select name="adv_dir_id" id="adv_dir_id" tabindex="1" alt="Type to search directory name">
    									<option value="">Select Directory</option>
    									<?php						
    								for($i=0;$i<count($dirRes);$i++)
    								{
    									if($dirRes[$i]['dir_id']==$adv_dir_id) { $select="selected='selected'"; } else { $select=""; }
    								?>
    								<option value="<?php echo $dirRes[$i]["dir_id"]; ?>" <?php echo $select; ?> ><?php echo $dirRes[$i]["dir_name"]; ?></option>
    								<?php
    								}
    								?>
    								</select>
								</div>
								<div class="main-row">
									<label>Advertise Title</label>
							 		<input type="text" tabindex="2" name="adv_title" id="adv_title"></input>
							 	</div>
							 	<div class="main-row">
							 	  <label>Advertise Description</label>
							 	  <input type="text" name="adv_description" id="adv_description" tabindex="3"></input>
								</div>
								<!-- <div class="main-row">
									<div class="fromdate">
									<label>From</label>
							 		<input type="text" readonly="readonly" tabindex="3" name="adv_from_date" id="adv_from_date"></input>
							 		</div>
							 		<div class="todate">
							 		<label>To</label>
							 		<input type="text" readonly="readonly" tabindex="4" name="adv_to_date" id="adv_to_date"></input>
							 		</div>
							 	</div> -->
								<div class="main-row">
									<input type="button" tabindex="5" value="Search" onclick="getLiveSearch('Advertisement','<?php echo $SECTION_TABLE_ADVERTISE; ?>','<?php echo $SECTION_FIELD_PREFIX_ADVERTISE?>','<?php echo $SECTION_MANAGE_PAGE_ADVERTISE; ?>','');"></input>
							 		<input type="button" id="bus_reset" onclick="resetForm('search_adv','Advertisement','<?php echo $SECTION_TABLE_ADVERTISE?>','<?php echo $SECTION_FIELD_PREFIX_ADVERTISE?>','<?php echo $SECTION_MANAGE_PAGE_ADVERTISE; ?>');" tabindex="6" value="Clear" />
							 	</div>
								<div class="clr"></div>	
							</div>
						</form>
						<div class="clr"></div>
					</div>
				</div>
			</div>
		</header>
		<div id="updatediv"></div>
	</article>
</section>
<div id="test" style="display:none;">
	<div class="alert-box">
		<div class="error-title">
			<img src="../images/error-icon.png" alt="" >
			<span>Warning</span>
			<a title="Close" class="close" href="javascript:;"></a>
		</div>
		<div class="error-txt">Error coming soon... </div>
		<div class="button-div">
			<a href="#">Ok</a>
			<a href="#">Cancel</a>
		</div>
		<div class="clr"></div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$("#adv_from_date").datepicker({
		dateFormat: "mm-dd-yy",
		changeMonth: true,
		changeYear: true,
		buttonImageOnly: false,
		onSelect: function(selected) 
		{						
			$('#adv_to_date').datepicker('option', 'minDate', new Date(selected));
		}
	});
	$("#adv_to_date").datepicker({
		dateFormat: "mm-dd-yy",
		changeMonth: true,
		changeYear: true,
		buttonImageOnly: false,
		onSelect: function(selected) 
		{						
			$('#adv_from_date').datepicker('option', 'maxDate', new Date(selected));
		}
	});
	$('#adv_from_date').datepicker('option', 'maxDate', new Date($('#adv_to_date').val()));
	$('#adv_to_date').datepicker('option', 'minDate', new Date($('#adv_from_date').val()));

	$("#meb_from_date").datepicker({
		dateFormat: "mm-dd-yy",
		changeMonth: true,
		changeYear: true,
		buttonImageOnly: false,
		onSelect: function(selected) 
		{						
			$('#meb_to_date').datepicker('option', 'minDate', new Date(selected));
		}
	});
	$("#meb_to_date").datepicker({
		dateFormat: "mm-dd-yy",
		changeMonth: true,
		changeYear: true,
		buttonImageOnly: false,
		onSelect: function(selected) 
		{						
			$('#meb_from_date').datepicker('option', 'maxDate', new Date(selected));
		}
	});
	$('#meb_from_date').datepicker('option', 'maxDate', new Date($('#meb_to_date').val()));
	$('#meb_to_date').datepicker('option', 'minDate', new Date($('#meb_from_date').val()));

	$("#dir_from_date").datepicker({
		dateFormat: "mm-dd-yy",
		changeMonth: true,
		changeYear: true,
		buttonImageOnly: false,
		onSelect: function(selected) 
		{						
			$('#dir_to_date').datepicker('option', 'minDate', new Date(selected));
		}
	});
	$("#dir_to_date").datepicker({
		dateFormat: "mm-dd-yy",
		changeMonth: true,
		changeYear: true,
		buttonImageOnly: false,
		onSelect: function(selected) 
		{						
			$('#dir_from_date').datepicker('option', 'maxDate', new Date(selected));
		}
	});
	$('#dir_from_date').datepicker('option', 'maxDate', new Date($('#dir_to_date').val()));
	$('#dir_to_date').datepicker('option', 'minDate', new Date($('#dir_from_date').val()));
});
</script>