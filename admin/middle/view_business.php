<script type="text/javascript" src="<?php echo AJAX_FOLDER_WWW?>getAjaxAdmin.js"></script>
<?php 
#################Member######################

$SECTION_FIELD_PREFIX = 'meb_';
$SECTION_AUTO_ID = $SECTION_FIELD_PREFIX.'id';
$SECTION_ORDER_BY = $SECTION_FIELD_PREFIX.'sort_order'.','.$SECTION_FIELD_PREFIX.'id';
$SECTION_VIEW_PAGE = ADM_VIEW_BUSINESS;
$SECTION_MANAGE_PAGE = ADM_MANAGE_BUSINESS;
$SECTION_TABLE = TBL_MEMBER;

#################Member######################

//directory Listing...
$dir_fields = array("dir_id","dir_name");
$dir_where  = "dir_status = 'Active'";
$dirRes 	= $db->selectData(TBL_DIRECTORY,$dir_fields,$dir_where,$extra="",2);

?>
<script type="text/javascript" language="javascript">
$(document).ready(function(){
	getpagelisting('Adm_business','<?php echo $SECTION_TABLE?>','<?php echo $SECTION_FIELD_PREFIX?>','<?php echo $SECTION_MANAGE_PAGE;?>');
});
</script>
<section>
	<article id="page" >
		<header>
			<?php include_once(ADM_FOLDER."menu.php"); ?>
			<div class="tab_content_holder">
				<div class="tab_content_holder_inner">
					<div class="border-txt">
				<div class="serviceTxt">Search</div>
					<div id="members">
						<form method="post" id="search_meb" name="search_meb" action="" >
							<div class="block-part">
								<div class="main-row">
    								<label>Directory Name</label>    								 
    								<select name="dir_list" id="dir_list" tabindex="1" alt="Type to search directory name">
    									<option value="">Select Directory</option>
    									<?php						
    									for($i=0;$i<count($dirRes);$i++)
    									{
    										if($dirRes[$i]['dir_id']==$dirRes) { $select="selected='selected'"; } else { $select=""; }
    									?>
    										<option value="<?php echo $dirRes[$i]['dir_id']; ?>" <?php echo $select; ?> ><?php echo $dirRes[$i]['dir_name']; ?></option>
    									<?php
    									}
    									?>
    								</select>
								</div>
								<div class="main-row">
							 	  <label>Business Name</label>
							 	  <input type="text" tabindex="2" name="meb_name" id="meb_name"></input>
							 	</div>
							 	<div class="main-row">
							 	  <label>Email</label>
							 	  <input type="text" tabindex="3" name="meb_username" id="meb_username"></input>
							 	</div>
								<div class="main-row">
							 	  <label>Phone</label>
							 	  <input type="text" name="meb_phone" tabindex="4" id="meb_phone"></input>
								</div>
								<div class="main-row">
							 		<label>&nbsp;</label>
							 		<input type="button" tabindex="5" value="Search" onclick="getLiveSearch('Adm_business','<?php echo $SECTION_TABLE?>','<?php echo $SECTION_FIELD_PREFIX?>','','');"></input>
							 		<input type="button" id="bus_reset" onclick="resetForm('search_meb','Adm_business','<?php echo $SECTION_TABLE?>','<?php echo $SECTION_FIELD_PREFIX?>','<?php echo $SECTION_MANAGE_PAGE; ?>');" tabindex="6" value="Clear" />
							 	</div>
							 	<div class="clr"></div>	
							</div>
								<div class="clr"></div>	
							</div>
						</form>
						<div class="clr"></div>
					</div>
				</div>
			</div>
		</header>
		<div id="updatediv"></div>
	</article>
</section>