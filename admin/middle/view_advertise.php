<script type="text/javascript" src="<?php echo AJAX_FOLDER_WWW?>getAjaxAdmin.js"></script>
<?php 
################# Advertisememt ###################

$SECTION_FIELD_PREFIX_ADVERTISE = 'adv_';
$SECTION_AUTO_ID_ADVERTISE = $SECTION_FIELD_PREFIX_ADVERTISE.'id';
$SECTION_VIEW_PAGE_ADVERTISE = ADM_VIEW_ADVERTISE;
$SECTION_MANAGE_PAGE_ADVERTISE = ADM_MANAGE_ADVERTISE;
$SECTION_TABLE_ADVERTISE = TBL_ADVERTISE;

################# Advertisememt ###################
//directory Listing...
$dir_fields = array("dir_id","dir_name");
$dir_where  = "dir_status = 'Active'";
$dirRes 	= $db->selectData(TBL_DIRECTORY,$dir_fields,$dir_where,$extra="",2);
?>
<script type="text/javascript" language="javascript">
$(document).ready(function(){
	getpagelisting('Advertisement','<?php echo $SECTION_TABLE_ADVERTISE; ?>','<?php echo $SECTION_FIELD_PREFIX_ADVERTISE;?>','<?php echo $SECTION_MANAGE_PAGE_ADVERTISE; ?>');
});
</script>
<section>
	<article id="page" >
		<header>
			<?php include_once(ADM_FOLDER."menu.php"); ?>
			<div class="tab_content_holder">
				<div class="tab_content_holder_inner">
					<div class="border-txt">
					<div class="serviceTxt">Search <span></span></div>
					<div id="advertisement">
						<form method="post" id="search_adv" name="search_adv" action="" >
							<div class="block-part">
								<div class="main-row">
									<label>Directory Name</label>    								 
    								<select name="adv_dir_id" id="adv_dir_id" tabindex="1" alt="Type to search directory name">
    									<option value="">Select Directory</option>
    									<?php						
    								for($i=0;$i<count($dirRes);$i++)
    								{
    									if($dirRes[$i]['dir_id']==$adv_dir_id) { $select="selected='selected'"; } else { $select=""; }
    								?>
    								<option value="<?php echo $dirRes[$i]["dir_id"]; ?>" <?php echo $select; ?> ><?php echo $dirRes[$i]["dir_name"]; ?></option>
    								<?php
    								}
    								?>
    								</select>
								</div>
								<div class="main-row">
									<label>Advertise Title</label>
							 		<input type="text" tabindex="2" name="adv_title" id="adv_title"></input>
							 	</div>
							 	<div class="main-row">
							 	  <label>Advertise Description</label>
							 	  <input type="text" name="adv_description" id="adv_description" tabindex="3"></input>
								</div>
								<!-- <div class="main-row">
									<div class="fromdate">
									<label>From</label>
							 		<input type="text" readonly="readonly" tabindex="3" name="adv_from_date" id="adv_from_date"></input>
							 		</div>
							 		<div class="todate">
							 		<label>To</label>
							 		<input type="text" readonly="readonly" tabindex="4" name="adv_to_date" id="adv_to_date"></input>
							 		</div>
							 	</div> -->
								<div class="main-row">
									<input type="button" tabindex="5" value="Search" onclick="getLiveSearch('Advertisement','<?php echo $SECTION_TABLE_ADVERTISE; ?>','<?php echo $SECTION_FIELD_PREFIX_ADVERTISE?>','<?php echo $SECTION_MANAGE_PAGE_ADVERTISE; ?>','');"></input>
							 		<input type="button" id="bus_reset" onclick="resetForm('search_adv','Advertisement','<?php echo $SECTION_TABLE_ADVERTISE?>','<?php echo $SECTION_FIELD_PREFIX_ADVERTISE?>','<?php echo $SECTION_MANAGE_PAGE_ADVERTISE; ?>');" tabindex="6" value="Clear" />
							 	</div>
							 	<div class="clr"></div>	
							</div>
								<div class="clr"></div>	
							</div>
						</form>
						<div class="clr"></div>
					</div>
				</div>
			</div>
		</header>
		<div id="updatediv"></div>
	</article>
</section>