<script type="text/javascript" src="<?php echo AJAX_FOLDER_WWW?>getAjaxAdmin.js"></script>
<?php 
#################Member######################

$SECTION_FIELD_PREFIX = 'inv_';
$SECTION_AUTO_ID = $SECTION_FIELD_PREFIX.'id';
$SECTION_ORDER_BY = $SECTION_FIELD_PREFIX.'sort_order'.','.$SECTION_FIELD_PREFIX.'id';
$SECTION_VIEW_PAGE = ADM_VIEW_INDIVIDUAL;
$SECTION_MANAGE_PAGE = ADM_MANAGE_INDIVIDUAL;
$SECTION_TABLE = TBL_INDIVIDUAL;

?>
<script type="text/javascript" language="javascript">
$(document).ready(function(){
	getpagelisting('Individual','<?php echo $SECTION_TABLE?>','<?php echo $SECTION_FIELD_PREFIX?>','<?php echo $SECTION_MANAGE_PAGE;?>');
});
</script>
<section>
	<article id="page" >
		<header>
			<?php include_once(ADM_FOLDER."menu.php"); ?>
			<div class="tab_content_holder">
				<div class="tab_content_holder_inner">
					<div class="border-txt">
				<div class="serviceTxt">Search</div>
					<div id="members">
						<form method="post" id="search_inv" name="search_inv" action="" >
							<div class="block-part">
								<div class="main-row">
							 	  <label>Email</label>
							 	  <input type="text" tabindex="1" name="inv_email" id="inv_email"></input>
							 	</div>
							 	<div class="main-row">
							 		<label>&nbsp;</label>
							 		<input type="button" tabindex="2" value="Search" onclick="getLiveSearch('Individual','<?php echo $SECTION_TABLE?>','<?php echo $SECTION_FIELD_PREFIX?>','','');"></input>
							 		<input type="button" id="bus_reset" onclick="resetForm('search_inv','Individual','<?php echo $SECTION_TABLE?>','<?php echo $SECTION_FIELD_PREFIX?>','<?php echo $SECTION_MANAGE_PAGE; ?>');" tabindex="3" value="Clear" />
							 	</div>
							 	<div class="clr"></div>	
							</div>
								<div class="clr"></div>	
							</div>
						</form>
						<div class="clr"></div>
					</div>
				</div>
			</div>
		</header>
		<div id="updatediv"></div>
	</article>
</section>