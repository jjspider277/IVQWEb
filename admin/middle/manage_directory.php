<script type="text/javascript" src="<?php echo AJAX_FOLDER_WWW?>getAjaxAdmin.js"></script>
<?php

	//Category Listing...
	$cat_fields = array("dic_id","dic_name");
	$cat_where  = "dic_status = 'Active'";
	$catRes 	= $db->selectData(TBL_DIRECTORY_CATEGORY,$cat_fields,$cat_where,$extra="",2);
	
	//State Listing...
	$state_fields = array("sta_id","sta_name");
	$state_where  = "sta_status = 'Active'";
	$staRes 	= $db->selectData(TBL_STATE,$state_fields,$state_where,$extra="",2);
	
	//Country Listing...
	$con_fields = array("con_id","con_name");
	$con_where  = "con_status = 'Active'";
	$conRes 	= $db->selectData(TBL_COUNTRY,$con_fields,$con_where,$extra="",2);

	#################################################################
	$SECTION_FIELD_PREFIX='dir_';
	$SECTION_AUTO_ID=$_REQUEST[$SECTION_FIELD_PREFIX.'id'];
	$SECTION_WHERE=$SECTION_FIELD_PREFIX."id=".$SECTION_AUTO_ID."";
	$SECTION_VIEW_PAGE = ADM_VIEW_DIRECTORY;
	$SECTION_MANAGE_PAGE=ADM_MANAGE_DIRECTORY;
	$SECTION_TABLE=TBL_DIRECTORY;
	$SECTION_NAME='Directory';

	$uploadFILEURL = UPLOAD_DIR_DIRECTORY_FILE;
	$uploadFILEWWW = UPLOAD_WWW_DIRECTORY_FILE;

	#################################################################
	if($_POST['method'] != "")
		$action =  $_POST['method'];
	else if($_REQUEST['method'] != "")
		$action =  $_REQUEST['method'];

	if(isset($_POST)){	
	$data = $_POST;	
	}
	if ($_POST['dir_submit'] == 'Save Directory' || $data['dir_addNew'] == 'Add New')
	{
		$dir_logo_file_name= $_FILES['dir_logo_file']['name'];
		
		$status 		=	"Active";
		$add_values[$SECTION_FIELD_PREFIX.'dic_id'] 		= trim($_POST["dir_cat"]);
		$add_values[$SECTION_FIELD_PREFIX.'name'] 			= trim($_POST["dir_name"]);
		$add_values[$SECTION_FIELD_PREFIX.'address'] 		= trim($_POST["dir_address"]);
		$add_values[$SECTION_FIELD_PREFIX.'city'] 			= trim($_POST["dir_city"]);
		$add_values[$SECTION_FIELD_PREFIX.'sta_id'] 		= trim($_POST["dir_state"]);
		$add_values[$SECTION_FIELD_PREFIX.'zip'] 			= trim($_POST["dir_zip"]);
		$add_values[$SECTION_FIELD_PREFIX.'con_id'] 		= $_POST["dir_country"];
		$add_values[$SECTION_FIELD_PREFIX.'office_phone'] 	= trim($_POST["dir_office_phone"]);
		$add_values[$SECTION_FIELD_PREFIX.'email'] 			= trim($_POST["dir_email"]);
		$add_values[$SECTION_FIELD_PREFIX.'website'] 		= trim($_POST["dir_website"]);	
		$add_values[$SECTION_FIELD_PREFIX.'logo_file'] 		= trim($_POST["dir_logo_file"]);

		$add_values[$SECTION_FIELD_PREFIX.'featured'] 		= trim($_POST["dir_featured"]);
        $add_values[$SECTION_FIELD_PREFIX.'private'] 		= trim($_POST["dir_private"]);
        $add_values[$SECTION_FIELD_PREFIX.'t_c'] 		    = trim($_POST["dir_t_c"]);
        $add_values[$SECTION_FIELD_PREFIX.'t_c_description'] = trim($_POST["dir_t_c_description"]);

		if($action != 'Update'){	
			$add_values[$SECTION_FIELD_PREFIX.'status'] 		= $status;											
		    $add_values[$SECTION_FIELD_PREFIX . 'created_id'] 	= getAdminSessionId();
    	    $add_values[$SECTION_FIELD_PREFIX . 'created_date'] = $today_date;	
    	    $GPDetail_result = $db->insertData($SECTION_TABLE, $add_values);

    	    $Dir_Id=$GPDetail_result;
    	    if($dir_logo_file_name!="")
			{
				if(is_dir($uploadFILEURL.$Dir_Id) == false)
				{ 			
					mkdir($uploadFILEURL.$Dir_Id);
				}
				$bus_file = uploadIVA($uploadFILEURL.$Dir_Id."/",$_FILES['dir_logo_file']);
				if($bus_file=="type_err")
				{
					$_SESSION["err_file"]="Please select only image videos and audio file"; 
					$db->delete("delete from ".$SECTION_TABLE." where dir_id=".$Dir_Id);
					$_SESSION['add_detail_error'][]=$_POST;
					$URL = getMemberURL($SECTION_MANAGE_PAGE,$action);
					redirect($URL);
					exit; 
				}
				else if($bus_file=="size_err")
				{
					$_SESSION["err_file"]="Please upload file less than 32MB file size!";
					$db->delete("delete from ".$SECTION_TABLE." where dir_id=".$Dir_Id);
					$_SESSION['add_detail_error'][]=$_POST;
					$URL = getMemberURL($SECTION_MANAGE_PAGE,$action);
					redirect($URL);
					exit; 
				}
				else
				{
					$bus_logo_file = $bus_file;
					$updateData[$SECTION_FIELD_PREFIX.'logo_file'] 	= $bus_logo_file;
					$SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$Dir_Id;
					$db->updateData($SECTION_TABLE, $updateData, $SECTION_WHERE);
				}
			}
    	    //$_SESSION['msg']  =   "Directory has been updated successfully.";  
    	}else{
    	   	$add_values[$SECTION_FIELD_PREFIX . 'updated_id'] 			= getAdminSessionId();
        	$add_values[$SECTION_FIELD_PREFIX . 'updated_date'] 		= $today_date;		
        	$SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$SECTION_AUTO_ID;
        	$GPDetail_result = $db->updateData($SECTION_TABLE, $add_values, $SECTION_WHERE); 

        	$Dir_Id=$SECTION_AUTO_ID;
			if($dir_logo_file_name!="")
			{
				if(is_dir($uploadFILEURL.$Dir_Id) == false)
				{ 			
					mkdir($uploadFILEURL.$Dir_Id);
				}
				$bus_file = uploadIVA($uploadFILEURL.$Dir_Id."/",$_FILES['dir_logo_file']);
				if($bus_file=="type_err")
				{
					$_SESSION["err_file"]="Please select only image videos and audio file"; 
					$db->delete("delete from ".$SECTION_TABLE." where dir_id=".$Dir_Id);
					$_SESSION['add_detail_error'][]=$_POST;
					$URL = getMemberURL($SECTION_MANAGE_PAGE,$action);
					redirect($URL);
					exit; 
				}
				else if($bus_file=="size_err")
				{
					$_SESSION["err_file"]="Please upload file less than 32MB file size!";
					$db->delete("delete from ".$SECTION_TABLE." where dir_id=".$Dir_Id);
					$_SESSION['add_detail_error'][]=$_POST;
					$URL = getMemberURL($SECTION_MANAGE_PAGE,$action);
					redirect($URL);
					exit; 
				}
				else
				{
					$bus_logo_file = $bus_file;
					$updateData[$SECTION_FIELD_PREFIX.'logo_file'] 	= $bus_logo_file;
					$SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$Dir_Id;
					$db->updateData($SECTION_TABLE, $updateData, $SECTION_WHERE);
				}
			}
        	//$_SESSION['msg']  =   "Directory has been inserted successfully.";  
        }
		if($data['dir_addNew'] == 'Add New')
		{
			$URL = getAdminURL($SECTION_MANAGE_PAGE); 
			redirect($URL);
			exit;
		}
		else
		{
			$URL = getAdminURL($SECTION_VIEW_PAGE."#directory"); 
			redirect($URL);
			exit;
		}
	}
	if($action=='Edit')
	{
		$list_query = getSelectList($SECTION_TABLE,$SECTION_FIELD_PREFIX);
		$list_query .= "AND ".$SECTION_FIELD_PREFIX."id = '".$SECTION_AUTO_ID."'";
		$result_query = $db->select($list_query);
		$dir_cat		=	$result_query[0][$SECTION_FIELD_PREFIX."dic_id"];
		$dir_name		=	$result_query[0][$SECTION_FIELD_PREFIX."name"];
		$dir_address	=	$result_query[0][$SECTION_FIELD_PREFIX."address"];
		$dir_city		=	$result_query[0][$SECTION_FIELD_PREFIX."city"];
		$dir_state		=	$result_query[0][$SECTION_FIELD_PREFIX."sta_id"];
		$dir_zip		=	$result_query[0][$SECTION_FIELD_PREFIX."zip"];
		$dir_country	=	$result_query[0][$SECTION_FIELD_PREFIX."con_id"];
		$dir_office_phone	=	$result_query[0][$SECTION_FIELD_PREFIX."office_phone"];
		$dir_email		=	$result_query[0][$SECTION_FIELD_PREFIX."email"];
		$dir_website	=	$result_query[0][$SECTION_FIELD_PREFIX."website"];
		$dir_logo_file	=	$result_query[0][$SECTION_FIELD_PREFIX."logo_file"];

		$dir_featured	=	$result_query[0][$SECTION_FIELD_PREFIX."featured"];
        $dir_private	=	$result_query[0][$SECTION_FIELD_PREFIX."private"];
        $dir_t_c	    =	$result_query[0][$SECTION_FIELD_PREFIX."t_c"];
        $dir_t_c_description =	$result_query[0][$SECTION_FIELD_PREFIX."t_c_description"];

		$action = "Update";
		$action_url = getAdminURL($SECTION_MANAGE_PAGE,$action,$SECTION_WHERE);
	}	
	if($action=='')
	{
		$status = "Active";
		$action = "Add";
		$action_url = getAdminURL($SECTION_MANAGE_PAGE,$action);
		if($_SESSION['add_detail_error'][0]!="")
		{
			foreach($_SESSION['add_detail_error'][0] as $key => $value)
			{
				$result_query[0][$key]=stripslashes($value);
			}
			$dir_cat		=	$result_query[0]["dir_cat"];
			$dir_name		=	$result_query[0]["dir_name"];
			$dir_address	=	$result_query[0]["dir_address"];
			$dir_city		=	$result_query[0]["dir_city"];
			$dir_state		=	$result_query[0]["dir_state"];
			$dir_zip		=	$result_query[0]["dir_zip"];
			$dir_country	=	$result_query[0]["dir_country"];
			$dir_office_phone	=	$result_query[0]["dir_office_phone"];
			$dir_email		=	$result_query[0]["dir_email"];
			$dir_website	=	$result_query[0]["dir_website"];
			$dir_logo_file	=	$result_query[0]["dir_logo_file"];
			$dir_featured	=	$result_query[0]["dir_featured"];
            $dir_private	=	$result_query[0]["dir_private"];
            $dir_t_c	    =	$result_query[0]["dir_t_c"];
            $dir_t_c_description = $result_query[0]["dir_t_c_description"];
			unset($_SESSION['add_detail_error']);
		}
	}
	
?>
	<section>
		<article id="page">
			<header>
    			<ul class="tab_links ">
    				<li><div class="tab_link_active inner"><span><img src="<?php echo IMG_WWW; ?>directory.png" /></span><h1>Group Setup</h1>
						<div class="view"><a class="tooltip" title="Go Back" href="<?php echo ADM_INDEX_PARAMETER.$SECTION_VIEW_PAGE."#directory"; ?>"><img src="<?php echo IMG_WWW; ?>back-blue.png"></a></div>
					</div></li>    					
    			</ul>
			</header>		   
			<aside>		
			<form name="manage_directory" enctype="multipart/form-data" method="post" action="<?php echo $action_url; ?>" id="manage_directory">				 
				<div class="tab_content_holder directory">
				 	<div class="tab_content_holder_inner">
						<div class="block-part">
							<h1>Group Information</h1>
							<div class="main-row">
								<label>Category <?php echo getRequiredIcon()?></label>
								<select name="dir_cat" id="dir_cat" tabindex="1" alt="Type to search directory category">
									<option value="">Select Category</option>
									<?php						
									for($i=0;$i<count($catRes);$i++)
									{
										if($catRes[$i]['dic_id']==$dir_cat) { $select="selected='selected'"; } else { $select=""; }
									?>
										<option value="<?php echo $catRes[$i]['dic_id']; ?>" <?php echo $select; ?> ><?php echo $catRes[$i]['dic_name']; ?></option>
									<?php
									}
									?>
								</select>
							</div>
							<div class="main-row">
								<label>Name <?php echo getRequiredIcon()?></label>
								<input type="text" name="dir_name" id="dir_name" tabindex="2" value="<?php echo $dir_name;?>"/>
							</div>
							<div class="main-row">
								<label>Address <?php echo getRequiredIcon()?></label>
								<input type="text" name="dir_address" id="dir_address" tabindex="3" value="<?php echo $dir_address;?>" />
							</div>
							<div class="main-row">
								<label>Zip <?php echo getRequiredIcon()?></label>
								<input type="text" name="dir_zip" id="dir_zip" tabindex="4" value="<?php echo $dir_zip;?>" />
							</div>
							<div class="main-row">
								<label>City <?php echo getRequiredIcon()?></label>
								<input type="text" name="dir_city" id="dir_city" tabindex="5" value="<?php echo $dir_city;?>" />
							</div>
							<div class="main-row">
								<label>State <?php echo getRequiredIcon()?></label>
								<!-- <input type="text" name="dir_state" id="dir_state" value="<?php echo $dir_state;?>" /> -->
								<select name="dir_state" id="dir_state" tabindex="6" alt="Type to search state">
									<option value="">Select State</option>
									<?php						
									for($i=0;$i<count($staRes);$i++)
									{
										if($staRes[$i]['sta_id']==$dir_state) { $select="selected='selected'"; } else { $select=""; }
									?>
									<option value="<?php echo $staRes[$i]['sta_id']; ?>" <?php echo $select; ?> ><?php echo $staRes[$i]['sta_name']; ?></option>
									<?php
									}
									?>
								</select>
							</div>							
							<div class="main-row">
								<label>Country <?php echo getRequiredIcon()?></label>
								<!-- <input type="text" name="dir_country" id="dir_country" tabindex="7" value="<?php if(isset($dir_country)){echo $dir_country; }else{ echo "USA"; } ?>"  disabled /> -->
								<select name="dir_country" id="dir_country" tabindex="7" alt="Type to search country">
									<!-- <option value="">Select Country</option> -->
									<?php						
									for($i=0;$i<count($conRes);$i++)
									{
										if($conRes[$i]['con_id']==$dir_country) { $select="selected='selected'"; } else { $select=""; }
									?>
									<option value="<?php echo $conRes[$i]['con_id']; ?>" <?php echo $select; ?> ><?php echo $conRes[$i]['con_name']; ?></option>
									<?php
									}
									?>
								</select> 
							</div>
                            
							<div class="main-row">
								<label>Office Phone <?php echo getRequiredIcon()?></label>
								<input type="text" name="dir_office_phone" id="dir_office_phone" tabindex="8" value="<?php echo $dir_office_phone;?>"/>
							</div>
							<?php /* ?>
							<div class="main-row">
								<label>Email <?php echo getRequiredIcon()?></label>
								<input type="text" name="dir_email" id="dir_email" tabindex="9" value="<?php echo $dir_email;?>"/>
							</div>
							<?php */ ?>

                            <div class="main-row">
								<label>Website</label>
								<input type="text" name="dir_website" id="dir_website" tabindex="10" value="<?php echo $dir_website;?>"/>
							</div>

							<!-- Add by moradiya 166 -->	
							<div class="main-row">
								<label>Logo file</label>
								<input tabindex="20" type="file" name="dir_logo_file" id="dir_logo_file" />
								<?php
									if($_SESSION["err_file"]!="") {
										echo '<label class="error">'.$_SESSION["err_file"].'</label>';
										$_SESSION["err_file"]="";
									}
									if($action=="Update" && $dir_logo_file!="") {
										echo "<a id='vediosPlay' href='view_file.php?filename=".$dir_logo_file."&dir_id=".$SECTION_AUTO_ID."' style='margin-top:5px;' class='fancybox fancybox.ajax' title='View' data-fancybox-type='ajax' >".$dir_logo_file."</a>";
		                                echo "&nbsp;&nbsp;&nbsp;<a style='margin-top:5px;' id='removeLink' onclick='javascript:removeFile(edit_bus_logo_file);'>Remove</a>";
										echo "<input type='hidden' id='edit_bus_logo_file' name='edit_bus_logo_file' value='".$dir_logo_file."' />";
									}
								?>
							</div>

							<style type="text/css">
								.prettycheckbox label, .prettyradio label {
									width: 150px !important;
								}
							</style>
							<?php 
								$checked='';
								if($dir_featured=='yes')
									$checked='checked="checked"';

                                $checked_p='';
                                if($dir_private=='yes')
                                    $checked_p='checked="checked"';

                                $checked_t_c='';
                                $dcr_show='none';
                                if($dir_t_c=='yes') {
                                    $checked_t_c = 'checked="checked"';
                                    $dcr_show='block';
                                }
                                //$dir_featured
							?>	
							<div class="main-row">
								<input type="checkbox" <?php echo $checked; ?> value="yes" name="dir_featured" id="dir_featured" data-label="Featured groups" />
                                <input type="checkbox" <?php echo $checked_p; ?> value="yes" name="dir_private" id="dir_private" data-label="Private Group" />
							</div>

                            <div class="main-row">
                                <div>
                                    <input type="checkbox" <?php echo $checked_t_c; ?> value="yes" name="dir_t_c" id="dir_t_c" data-label="Display T & C’s" />
                                </div>
                                <div id="t_c_notes_div" style="display: <?php echo $dcr_show; ?>;">
                                    <label>T & C’s Notes *</label>
                                    <textarea name="dir_t_c_description" id="dir_t_c_description"><?php echo trim($dir_t_c_description);?></textarea>
                                </div>
                            </div>

							<!-- Add by moradiya 166 -->

							<div class="main-row">
								<label>&nbsp;</label>
								<input type="submit" name="dir_submit" id="dir_submit" tabindex="11" value="Save Directory">
								<?php if($action!="Update") { ?>
								<input type="submit" tabindex="12" name="dir_addNew" id="dir_addNew" value="Add New" />
								<?php } ?>
							</div>
						</div>
						<div class="clr"></div>
					</div>
				</div>
			</form>
			<?php
			if($action == 'Update')
			{
			?>
			<form name="manage_contact" method="post" id="manage_contact">
				<div class="tab_content_holder directory">
					<div class="tab_content_holder_inner small">
						<div class="show-hide">
                            <h1>Contact Information</h1> <a href="javascript:void(0);"><img src="<?php echo IMG_WWW; ?>add.png"/></a>
                        </div>
						<div class="block-part top" id="contactDiv" style="display:none;">
							<div class="main-row">
								<label>Name <?php echo getRequiredIcon()?></label>
								<input type="text" name="dco_name" id="dco_name" value=""/>
							</div>
							<div class="main-row">
								<label>Title <?php echo getRequiredIcon()?></label>
								<input type="text" name="dco_title" id="dco_title" value=""/>
							</div>
							<div class="main-row">
								<label>Mobile Phone <?php echo getRequiredIcon()?></label>
								<input type="text" name="dco_mob_phone" id="dco_mob_phone" value=""/>
							</div>
							<div class="main-row">
								<label>Email <?php echo getRequiredIcon()?></label>
								<input type="text" name="dco_email" id="dco_email" value=""/>
							</div>
							<div class="main-row">
								<label>&nbsp;</label>
								<input name="dco_dir_id" id="dco_dir_id" value="<?php echo $SECTION_AUTO_ID; ?>" type="hidden">
								<input type="hidden" name="action_type" id="action_type" value="Add" />
								<input type="hidden" name="autoID" id="autoID" value="" />
								<input type="hidden" name="fieldPrefix" id="fieldPrefix" value="dco_" />
								<input type="button" name="dco_button" id="dco_button" value="Save Contact" onclick="insertRec('<?php echo TBL_DIRECTORY_CONTACT; ?>');" />
							</div>
						</div>
						<div class="clr"></div>
					</div>
				</div>
			</form>
			<div class="subRow" id="updatediv"></div>
			<?php } ?>                				   
			</aside>
		</article>
	</section>
<script>
$(document).ready(function(){

	getpagelisting('Contact','<?php echo TBL_DIRECTORY_CONTACT; ?>','dco_','','dco_dir_id=<?php echo $SECTION_AUTO_ID; ?>');

	$.validator.addMethod("phoneValidate", function(value, element) {
    	return this.optional(element) || /^[0-9\-\+\,\.\)\(\s]+$/i.test(value);
	}, "Only numbers,-,+ and , allowed");
	$("#manage_directory").validate({
		rules: {
			dir_cat :"required",
			dir_name : {
				required : true
			},
			dir_address : {
				required : true
			},
			dir_city : {
				required : true
			},
			dir_state : {
				required : true
			},
			dir_zip : {
				required : true,
				digits : true
			},
			dir_country : {
				required : true
			},
			dir_office_phone : {
				required : true,
				phoneValidate : true
			},
			dir_email : {
				required : true,
				email : true
			}/*,
			dir_website : {
				required : true,
				url : true
			}*/
		},
		messages: {
			dir_cat : {
				required : "Please select category"
			},
			dir_name : {
				required : "Please enter name"
			},
			dir_address : {
				required : "Please enter address"
			},
			dir_city : {
				required : "Please enter city"
			},
			dir_state : {
				required : "Please select state"
			},
			dir_zip : {
				required : "Please enter zip code",
				 digits: "Please enter only numbers"
			},
			dir_country : {
				required : "Please select country"
			},
			dir_office_phone : {
				required : "Please enter phone no.",
				phoneValidate : "Please enter valid phone no."
			},
			dir_email : {
				required : "Please enter email address",
				email : "Please enter valid email address"
			}/*,
			dir_website : {
				required : "Please enter website URL",
				url : "Please enter valid website URL"
			}*/
		}
	});
	<?php
	if($action=='Update')
	{
	?>
	$("#manage_contact").validate({
		rules: {
			dco_name : {
				required : true
			},
			dco_title : {
				required : true
			},
			dco_mob_phone : {
				required : true,
				phoneValidate : true
			},
			dco_email : {
				required : true,
				email : true
			}
		},
		messages: {
			dco_name : {
				required : "Please enter contact name"
			},
			dco_title : {
				required : "Please enter contact title"
			},
			dco_mob_phone : {
				required : "Please enter contact phone no.",
				phoneValidate : "Please enter valid phone no."
			},
			dco_email : {
				required : "Please enter contact email address",
				email : "Please enter valid email address"
			}
		}
	});
	<?php
	}
	?>

    $(document).on('change', '#dir_t_c', function(){
        if ($('#dir_t_c').prop('checked')) {
            $('#dir_t_c_description').removeClass('error');
            $('#t_c_notes_div').slideDown();
        } else {
            $('#t_c_notes_div').slideUp();
        }
    });

    $("#dir_office_phone").mask("999-999-9999");
});

function removeFile($fileName)
{
     $hiddenId = $fileName.id     
     $("#vediosPlay").remove();
     $("#removeLink").remove();
     $('#'+$hiddenId).val('');
     $('#remove_image').val('true');
     $( "#bus_submit" ).trigger( "click" );
}

function insertRec(tableName)
{
	var formdata = "tableName="+tableName+"&"+$("#manage_contact").serialize();
	if($("#manage_contact").valid())
	{
		$.ajax({
			url : ajax_folder+"getAjaxContact.php",
			type : "post",
			/*"async" : false,*/
			cache: false,
			data : formdata,
			success : function(res){
				/*alert(res);	*/
				$('#manage_contact').each (function(){
					this.reset();
					document.getElementById("autoID").value = "";
					document.getElementById("action_type").value = "Add";
					$("input:text").removeClass("error");
					$("label.error").html("");
				});
				$("#contactDiv").toggle();
				getpagelisting('Contact','<?php echo TBL_DIRECTORY_CONTACT; ?>','dco_','','dco_dir_id=<?php echo $SECTION_AUTO_ID; ?>');
			}
		});
	}
}
function editRecords(tableName,fieldPrefix,autoID)
{
	var formdata = "action_type=Edit&tableName="+tableName+"&fieldPrefix="+fieldPrefix+"&autoID="+autoID;
	$('a').each(function(){
		 if($(this).find("#DelId_"+autoID)) {
		 	$("#DelId_"+autoID).addClass("disabled");
		 }
		 $(this).removeClass("disabled");
	});
	$.ajax({
		url : ajax_folder+"getAjaxContact.php",
		type : "post",
		/*"async" : false,*/
		cache: false,
		data : formdata,
		/*contentType: 'json',*/
		success : function(res){
			var objres = res.split(",");
			document.getElementById("autoID").value = objres[0];
			document.getElementById("dco_name").value = objres[1];
			document.getElementById("dco_title").value = objres[2];
			document.getElementById("dco_mob_phone").value = objres[3];
			document.getElementById("dco_email").value = objres[4];
			document.getElementById("action_type").value = "Update";
			//$("#contactDiv").toggle();
			$("#contactDiv").show();
		}
	});
}

$(document).ready(function(){
  $(".show-hide").click(function(){    
	$("#contactDiv").toggle();
	$('#manage_contact').each (function(){
		this.reset();
		document.getElementById("autoID").value = "";
		document.getElementById("action_type").value = "Add";
		$("input:text").removeClass("error");
		$("label.error").html("");
	});

  });
});
</script>	
	
