<?php 
ob_start();
include_once('../include/includeclass.php');

	$action=$_REQUEST['mod'];

	if (!empty($action))	
	{
		$error="";
					
		if(empty($error))
		{
		 	$sql="select * from ".TBL_ADMIN." where adm_username ='".trim(addslashes($_POST['adm_username']))."' and adm_password='".trim(addslashes($_POST['adm_password']))."'"; 
			$data= $db->select($sql);
			
			if(!empty($data))
			{
				if($data[0]['adm_status'] == 'Active') 
				{
					if($_POST['remember']=='Remember Me')
					{

						setcookie('adm_username', $_POST['adm_username'],time() + 60*60*24*30); 
						setcookie('adm_password', $_POST['adm_password'],time() + 60*60*24*30); 
					}
					session_start();
					$_SESSION['adm_id']	=	$data[0]['adm_id'];
					$_SESSION['adm_admin_id']	=	$data[0]['adm_id'];
					$_SESSION['admin_name'] 	=	$data[0]['adm_username'];
					$_SESSION['CreatedDate']	=	$data[0]['adm_created_date'];
					$_SESSION['VisitDate']		=	$data[0]['adm_login_time'];
				//	print_r($_SESSION);
				$sql = "INSERT INTO ".TBL_ADMIN_LOGIN_DETAIL." SET 
							ald_adm_id = '".$data[0]['adm_id']."',
							ald_login_datetime = '".date('Y-m-d H:i:s')."',
							ald_ip_address = '".$_SERVER['REMOTE_ADDR']."'"; 
							
					$GPDetails = $db->insert($sql);
					$log_id = mysql_insert_id();
					$_SESSION['log_id']=$log_id;										
				
			 		//$URL=ADM_INDEX_PARAMETER.ADM_HOME; 
					$URL=ADM_INDEX_PARAMETER.ADM_VIEW_DIRECTORY;
					redirect($URL);
					exit;
				} 
				else 
				{
					$error = "Your account has been deactivated";
				}	
			}
			else
			{
				$error = "Invalid User name and Password.";		
			}	
		}
		
	}
?>

<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>B2B Mobile Services - Login</title>
<meta name="viewport" content="width=1024, initial-scale=1">
<link rel="shortcut icon" href="<?php echo IMG_WWW; ?>favicon.ico" type="image/x-icon" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="css/styles.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/1140.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/prettyCheckable.css" type="text/css" media="screen" />
<script type="text/javascript" src="<?php echo JS_FOLDER_WWW?>jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="<?php echo JS_FOLDER_WWW?>jquery-validation.js"></script>
<script type="text/javascript" src="<?php echo JS_FOLDER_WWW?>placeholders.min.js"></script>
<script type="text/javascript" src="<?php echo JS_FOLDER_WWW?>prettyCheckable.js"></script>
</head>
<body class="login-screen" >
	<div id="header">
		<div class="logo"><img src="<?php echo IMG_WWW; ?>logo_new.png" width="120" alt=""></div>
		<a href="<?php echo MEB_LOGIN?>"><div class="but_sty1" >IVQMobile</div></a>
	</div>
	
	<form name='frmAdmin' method='post' action="<?php echo ADM_LOGIN."?mod=add";?>" id='frmAdmin'>
	<section>
		<div class="login-box">
			<div class="login_box_icon"><img src="<?php echo IMG_WWW; ?>login_box_icon.png" width="96" height="96" alt=""></div>
			<div class="login_title">Administrator Login</div>
			<div class="login_form">
				<div class="login_form_inner">
						<label class="error"><?php echo $error; ?></label>	
						<div class="user_pass">
							<div class="inputText username"><input type="text" name="adm_username" id="adm_username" placeholder="User name" value="<?php echo $_COOKIE['adm_username']; ?>" /></div>
							<div class="inputText password"><input type="password" name="adm_password" id="adm_password" placeholder="Password" value="<?php echo $_COOKIE['adm_password']; ?>" /></div>
						</div>
						<div class="reme_forg">
							<div class="remember">
								<input name="remember" id="emember" type="checkbox" value="Remember Me" data-label="Remember Me"  data-labelPosition="right">
							</div>
						
							<div class="cl"></div>
						</div>
						<div class="but_login"><input name="login" id="login" type="submit" value="Login"></div>
				</div>
			</div>
		</div>
	</section>
	</form>
	<script>
$.validator.setDefaults({
	//submitHandler: function() { alert("submitted!"); }
});

$().ready(function() {
	$('input[type="checkbox"]').prettyCheckable();
	// validate signup form on keyup and submit
	$("#frmAdmin").validate({
		rules: {
			adm_username:"required",
			adm_password:"required"
		},
		messages: {
			adm_username:"Please enter username",
			adm_password:"Please enter password"
		}
	});
});
</script>
	</form>
	<div id="footer" >
		<div class="copyrights">&copy; <span>Powered by</span> Intelli-Touch Apps</div>
	</div>
</body>
<?php 
flush();
?>
</html>
