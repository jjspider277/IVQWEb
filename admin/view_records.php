<?php 

include_once('../include/includeclass.php');

if ($_REQUEST['sectionName'])
	$sectionName = $_REQUEST['sectionName'];
	
?>

<?php if($sectionName == "Directory"){ 

if ($_REQUEST['sectionName'])
	$dirId = $_REQUEST['dir_id'];

	//Directory Listing...
$dir_fields = array("*");
$dir_where  = "dir_id = ".$dirId." AND dir_status = 'Active'";
$dirRes 	= $db->selectData(TBL_DIRECTORY ." LEFT JOIN ".TBL_DIRECTORY_CATEGORY." ON dir_dic_id = dic_id"." LEFT JOIN ".TBL_STATE." ON dir_sta_id = sta_id"." LEFT JOIN ".TBL_COUNTRY." ON dir_con_id = con_id",$dir_fields,$dir_where,$extra="",2);
?>

<div class="fullview_clients">
	<div class="subRow">
		 <div class="enterTitle main">
		 	  <h1>Group Information</h1><a href="javascript:;" class="fancybox-item fancybox-close" title="Close"></a>
		</div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>Group Category</b><span>:</span> <p><?php echo $dirRes[0]['dic_name']; ?></p></div>
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>Group Name</b><span>:</span> <p><?php echo $dirRes[0]['dir_name']; ?></p></div>
	</div>
	<div class="subRow light">
		<div class="enterTitle address-title"><b>Address </b><span>:</span> <p><?php echo $dirRes[0]['dir_address']; ?> </p></div>
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>City </b><span>:</span> <p><?php echo $dirRes[0]['dir_city']; ?></p></div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>State </b><span>:</span> <p><?php echo $dirRes[0]['sta_name']; ?></p></div>
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>Zip</b><span>:</span><p><?php echo $dirRes[0]['dir_zip']; ?></p> </div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>Country</b><span>:</span><p><?php echo $dirRes[0]['con_name']; ?></p> </div>
	</div>
    <?php /* ?>
    <div class="subRow">
		<div class="enterTitle"><b>Phone </b><span>:</span> <p><?php echo $dirRes[0]['dir_office_phone']; ?> </p></div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>E-mail </b><span>:</span><p><a href="mailto:<?php echo $dirRes[0]['dir_email']; ?>"><?php echo $dirRes[0]['dir_email']; ?></a></p></div>
	</div>
    <?php */ ?>
	<div class="subRow">
		<div class="enterTitle courses-details-title"><b>Website </b><span>:</span><p><?php echo $dirRes[0]['dir_website']; ?></p></div>
	</div>
    <div class="subRow light">
        <div class="enterTitle"><b>Featured</b><span>:</span><p><?php echo $dirRes[0]['dir_featured']; ?></p> </div>
    </div>
    <div class="subRow light">
        <div class="enterTitle"><b>Private Group</b><span>:</span><p><?php echo $dirRes[0]['dir_private']; ?></p> </div>
    </div>
    <div class="subRow light">
        <div class="enterTitle"><b>Display T & C’s</b><span>:</span><p><?php echo $dirRes[0]['dir_t_c']; ?></p> </div>
        <div class="enterTitle"><b>T & C’s Notes</b><span>:</span><p><?php echo $dirRes[0]['dir_t_c_description']; ?></p> </div>
    </div>
    <?php /* ?>
    <div class="list-contact">
		<div class="subRow">
			<div class="enterTitle main">
			 	  <h1>Contact Information</h1>
			</div>
		</div>
		<div class="subRow">
            <?php
                $cont_fields = array("*");
                $cont_where  = "dco_dir_id = ".$dirRes[0]['dir_id']." AND dco_status = 'Active'";
                $contRes 	= $db->selectData(TBL_DIRECTORY_CONTACT,$cont_fields,$cont_where,$extra="",2);
                $total_rows = count($contRes);
                if($total_rows > 0) {
            ?>
                <table width="100%" cellspacing="0" cellpadding="0">
                    <tr>
                        <th width="25%">Contact Name</th>
                        <th width="15%">Title</th>
                        <th width="20%">Phone</th>
                        <th width="25%">Email</th>
                    </tr>
                    <?php
                        for($i=0;$i<$total_rows;$i++)
                        {
                    ?>
                    <tr>
                        <td><?php echo $contRes[$i]["dco_name"]; ?></td>
                        <td><?php echo $contRes[$i]["dco_title"]; ?></td>
                        <td><?php echo $contRes[$i]["dco_mob_phone"]; ?></td>
                        <td><?php echo $contRes[$i]["dco_email"]; ?></td>
                    </tr>
                    <?php } ?>
                </table>
            <?php } else { ?>
                <div class="no-records">No records found.</div>
            <?php } ?>
		    <div class="clr"></div>
		
	    </div>
    </div>
    <?php */ ?>
</div>
<?php } else if($sectionName == "Member"){

if ($_REQUEST['sectionName'])
	$mebId = $_REQUEST['meb_id'];

//Member Listing...
$meb_fields = array("*");
$meb_where  = "meb_id = ".$mebId." AND meb_status = 'Active'";
$mebRes 	= $db->selectData(TBL_MEMBER,$meb_fields,$meb_where,$extra="",2);

?>
<div class="fullview_clients">
	<div class="subRow">
		 <div class="enterTitle main">
		 	  <h1>Member Information</h1><a href="javascript:;" class="fancybox-item fancybox-close" title="Close"></a>
		</div>		
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>Member Name</b><span>:</span> <p><?php echo $mebRes[0]['meb_name']; ?></p></div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>User Name </b><span>:</span> <p><?php echo $mebRes[0]['meb_username']; ?></p></div>
	</div>
	<div class="subRow">
		<div class="enterTitle address-title"><b>Secret question </b><span>:</span> <p><?php echo $mebRes[0]['meb_question']; ?> </p></div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>Answer </b><span>:</span> <p><?php 
			echo $mebRes[0]['meb_answer']; 
		?></p></div>
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>Phone</b><span>:</span><p><?php echo $mebRes[0]['meb_phone']; ?></p> </div>
	</div>
</div>
<?php } else if($sectionName == "Adm_business"){

if ($_REQUEST['sectionName'])
	$mebId = $_REQUEST['meb_id'];

//Member Listing...
$meb_fields = array("*");
$meb_where  = "meb_id = ".$mebId." AND meb_type = 'Business'";
$mebRes 	= $db->selectData(TBL_MEMBER,$meb_fields,$meb_where,$extra="",2);

?>
<div class="fullview_clients">
	<div class="subRow">
		 <div class="enterTitle main">
		 	  <h1>Business Information</h1><a href="javascript:;" class="fancybox-item fancybox-close" title="Close"></a>
		</div>		
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>Business Name</b><span>:</span> <p><?php echo $mebRes[0]['meb_name']; ?></p></div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>Email </b><span>:</span> <p><?php echo $mebRes[0]['meb_username']; ?></p></div>
	</div>
	<div class="subRow">
		<div class="enterTitle address-title"><b>Secret question </b><span>:</span> <p><?php echo $mebRes[0]['meb_question']; ?> </p></div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>Answer </b><span>:</span> <p><?php 
			echo $mebRes[0]['meb_answer']; 
		?></p></div>
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>Phone</b><span>:</span><p><?php echo $mebRes[0]['meb_phone']; ?></p> </div>
	</div>
</div>
<?php } else if($sectionName == "Advertisement"){

if ($_REQUEST['sectionName'])
	$adv_id = $_REQUEST['adv_id'];

//Member Listing...
$adv_fields = array("*");
$adv_where  = "adv_id = ".$adv_id." AND adv_status = 'Active'";
$advRes 	= $db->selectData(TBL_ADVERTISE,$adv_fields,$adv_where,$extra="",2);

?>
<div class="fullview_clients">
	<div class="subRow">
		 <div class="enterTitle main">
		 	  <h1>Advertisement Information</h1><a href="javascript:;" class="fancybox-item fancybox-close" title="Close"></a>
		</div>		
	</div>
	<div class="subRow">
		<div class="enterTitle"><b>Directory Name</b><span>:</span> <p><?php
		$dir_fields = array("dir_name");
		$dir_where  = "dir_id = '".$advRes[0]['adv_dir_id']."'";
		$dirRes 	= $db->selectData(TBL_DIRECTORY,$dir_fields,$dir_where,$extra="",2);
		if($dirRes>0)
			echo $dirRes[0]['dir_name'];
		else
			echo "Global Directory";
		?></p></div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>Advertise Title </b><span>:</span> <p><?php echo $advRes[0]['adv_title']; ?></p></div>
	</div>
	<div class="subRow">
		<div class="enterTitle address-title"><b>Advertise Description </b><span>:</span> <p><?php echo $advRes[0]['adv_description']; ?> </p></div>
	</div>
	<div class="subRow light">
		<div class="enterTitle address-title"><b>Advertise Category </b><span>:</span> <p><?php echo $advRes[0]['adv_flag']; ?> </p></div>
	</div>
	<div class="subRow">
		<div class="enterTitle address-title"><b>Advertise Align </b><span>:</span> <p><?php echo $advRes[0]['adv_file_type']; ?> </p></div>
	</div>
	<div class="subRow light">
		<div class="enterTitle"><b>Advertise File </b><span>:</span> <p><img src="<?php echo UPLOAD_WWW_ADVERTISE_FILE.$adv_id.'/'.$advRes[0]['adv_filename']; ?>"  style="width:100px; height:100px; margin-top:5px;" ></p></div>
	</div>	
</div>
<?php }?>
