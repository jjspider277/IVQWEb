<?php
ob_start();

$url = 'members/index.php?files=front';
header('Location: ' . $url);
die();

include_once('include/includeclass.php');

$filename=$_REQUEST['files'];
if($filename=="")
{
	$filename ="home";
}
if($filename != "home" && $filename != "register" && $filename != "terms"){
isFreeMember();
}
if(!empty($filename))
{
	$middlefile=FRT_MIDD_DIR.$filename;
	if(!file_exists($middlefile.".php"))
	{
		$middlefile=FRT_MIDD_DIR.FRT_HOME;
	} 
}
else
{
	$middlefile=FRT_MIDD_DIR.FRT_HOME;
}
?>
<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>B2B Mobile Services</title>
<meta name="viewport" content="width=1024, initial-scale=1">
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />

<!--[if lt IE 9]>
<script src="<?php echo JS_FOLDER_WWW?>html5.js" type="text/javascript"></script>
<![endif]-->

<script type="text/javascript">
var ajax_folder='<?php echo AJAX_FOLDER_WWW; ?>';
var middle_folder='<?php echo FRT_MIDD_WWW; ?>';
</script>

<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="css/styles-member.css" type="text/css" media="screen" />
<!--<link rel="stylesheet" href="css/1140-member.css" type="text/css" media="screen" />-->
<link rel="stylesheet" href="css/tooltipster.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/search-result.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/jquery.custom-scrollbar.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo JS_FOLDER_WWW?>jquery-ui.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />
<link media="all" type="text/css" href="css/jcoverflip.css" rel="stylesheet">
<link rel="stylesheet" href="css/custom.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo MEB_CSS_WWW;?>prettyCheckable.css" type="text/css" media="screen" />
<!--[if IE]>
	<link rel="stylesheet" href="<?php echo FRT_CSS;?>ie.css" type="text/css" media="screen" />
<![endif]-->
<script type="text/javascript" src="<?php echo JS_FOLDER_WWW?>jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="<?php echo JS_FOLDER_WWW?>placeholders.min.js"></script>
<script src="<?php echo JS_FOLDER_WWW?>jquery-ui.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo JS_FOLDER_WWW?>fremworks.js"></script> 
<script type="text/javascript" src="<?php echo JS_FOLDER_WWW?>jquery.tooltipster.js"></script>
<script type="text/javascript" src="<?php echo JS_FOLDER_WWW?>Fancybox/jquery.fancybox.js?v=2.0.6"></script>
<script type="text/javascript" src="<?php echo JS_FOLDER_WWW?>prettyCheckable.js"></script>
<script type="text/javascript" src="<?php echo JS_FOLDER_WWW?>jquery-validation.js"></script>
<script type="text/javascript" src="<?php echo JS_FOLDER_WWW?>jquery.quick.pagination.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo JS_FOLDER_WWW?>Fancybox/jquery.fancybox.css?v=2.0.6" media="screen" />


    <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>-->
    <link href="css/star-rating.css" media="all" rel="stylesheet" type="text/css"/>
    <script src="<?php echo JS_FOLDER_WWW?>star-rating.js" type="text/javascript"></script>
    
    
</head>
<?php
if($filename=="home"){
	$class = " home_page";
}
else
{
	$class = "";	
}
?>
<body class="landing_body<?php echo $class; ?>">
<?php include_once(FRT_SECTION_DIR.FRT_HEADER); ?>
<section>
	<article id="page">
		<?php include_once($middlefile.'.php');?>
	</article>
</section>
<?php 
//banner Listing...
$banner_fields = array("ban_id","ban_title","ban_image","ban_url");
$banner_where  = "ban_status = 'Active'";
$bannerRes 	= $db->selectData(TBL_FRONT_BANNER,$banner_fields,$banner_where,$extra="",2);
?>
<div class="landing_image flexslider">
	<?php
	if($bannerRes>0)
	{
		echo '<ul class="slides">';
		for($i=0;$i<count($bannerRes);$i++)
		{
			if($bannerRes[$i]['ban_url']=="")
			{
				$banurl = "#";
				$target = "";
			}
			else
			{
				$banurl = $bannerRes[$i]['ban_url'];
				$target = "target='_blank'";
			}
	?>
		<li><a href="<?php echo $banurl; ?>" <?php echo $target ?> ><img src="<?php echo  UPLOAD_WWW_BANNER_FILE.$bannerRes[$i]['ban_id']."/".$bannerRes[$i]['ban_image']; ?>" title="<?php echo $bannerRes[$i]['ban_title']; ?>" /></a></li>
 	<?php
		}
		echo '</ul>';
	}
	else
	{
 echo '<ul class="slides"><li>';
	?>
	<img src="images/landing-banner.png">
	<?php echo '</li></ul>';} ?>
</div>
<script defer src="<?php echo JS_FOLDER_WWW?>jquery.flexslider.js"></script>
<script src="<?php echo JS_FOLDER_WWW?>modernizr.js"></script>
<script type="text/javascript">

  // $(window).load(function(){
      // $('.flexslider').flexslider({
		  // animation: 'slide',
		  // animationSpeed: 500,
		  // easing: 'linear',  //swing,linear,easeInElastic,easeOutElastic,easeInElastic,easeInOutElastic,easeInBounce,easeInOutBounce,
		  // pauseOnAction:false,
		  // smoothHeight :false,
		  // pauseOnHover:true,
		  // controlNav:false,
		  // directionNav:false,
		  // useCSS: false,
		  // start: function(slider){
			  // $('body').removeClass('loading'); 
		  // }
	  // });
  // });
 
 (function() {
 
  // store the slider in a local variable
  var $window = $(window),
      flexslider;
 
  // tiny helper function to add breakpoints
  function getGridSize() {
    return (window.innerWidth < 600) ? 2 :
           (window.innerWidth < 900) ? 3 : 4;
  }
 
  $(function() {
    //SyntaxHighlighter.all();
  });
 
  $window.load(function() {
    $('.flexslider').flexslider({
      animation: "slide",
      animationLoop: true,
      animationSpeed: 5,
      itemWidth: 250,
      directionNav:false,
      move: 1,
      itemMargin: 5,
      minItems: getGridSize(), // use function to pull in initial value
      maxItems: getGridSize() // use function to pull in initial value
    });
  });
 
  // check grid size on resize event
  $window.resize(function() {
    var gridSize = getGridSize();
 
    flexslider.vars.minItems = gridSize;
    flexslider.vars.maxItems = gridSize;
  });
}());
</script>
<div id="footer">
<div class="f-s">
	<div class="www"><a href="<?php echo ROOT_WWW; ?>">www.b2bms.net</a></div>
	<div class="copyrights"><span>&copy; Powered by</span> <b>Intelli-Touch Apps</b></div>
	<div class="terms-footer"><a href="<?php echo ROOT_WWW.FRT_INDEX_PARAMETER."terms"; ?>">Terms & Condition</a></div>
</div>
</div>
</body>
<script type="text/javascript">
    $(document).ready(function() {
    	//$('input[type="checkbox"]').prettyCheckable();
        // $('.tooltip').tooltipster({
			// position : "right"
        // });
		$('.fancybox').fancybox({
		'width':616,
		'height':262
		});
    });
</script>
</html>
<?php 
flush();
?>
