<?php
	include_once('../../include/includeclass.php');
	$action_type			=	$_POST['action_type'];
	$SECTION_TABLE			= 	$_POST['tableName'];
	$SECTION_FIELD_PREFIX	=	$_POST['fieldPrefix'];
	$SECTION_AUTO_ID 		=	$_POST['autoID'];
	$SECTION_MANAGE_PAGE	=	MEB_MANAGE_BUSINESS_SUB; //$_POST['managePage'];
	$SECTION_WHERE			=	$SECTION_FIELD_PREFIX."id='".$SECTION_AUTO_ID."'";
	$xtraCondition			=	stripslashes($_POST['xtraCondition']);
	$SECTION_NAME			=	$_POST['displayName'];	
	$searchchar				=	$_POST['searchchar'];
		
	$SECTION="BusinessSub";

	$total_language = count($result_language);
	#################################################################

	if($action_type ==  "delete")
	{
		$update_values[$SECTION_FIELD_PREFIX.'status']      = "Deleted";
		$update_values[$SECTION_FIELD_PREFIX . 'updated_id'] 	= $_SESSION['meb_id'];
        $update_values[$SECTION_FIELD_PREFIX . 'updated_date'] 	= date("Y-m-d H:i:s");
		$GPDetail_result = $db->updateData($SECTION_TABLE,$update_values,$SECTION_WHERE);
		//$_SESSION['msg']  =   "Subscribers has been deleted successfully.";  
	}
	else if($action_type ==  "active")
	{
		$update_values[$SECTION_FIELD_PREFIX.'status']      = "Active";
		$update_values[$SECTION_FIELD_PREFIX . 'updated_id'] 	= $_SESSION['meb_id'];
        $update_values[$SECTION_FIELD_PREFIX . 'updated_date'] 	= date("Y-m-d H:i:s");
		$GPDetail_result = $db->updateData($SECTION_TABLE,$update_values,$SECTION_WHERE);
		//$_SESSION['msg']  =   "Subscribers has been deleted successfully.";  
	}
	else if($action_type ==  "inactive")
	{
		$update_values[$SECTION_FIELD_PREFIX.'status']      = "Inactive";
		$update_values[$SECTION_FIELD_PREFIX . 'updated_id'] 	= $_SESSION['meb_id'];
        $update_values[$SECTION_FIELD_PREFIX . 'updated_date'] 	= date("Y-m-d H:i:s");
		$GPDetail_result = $db->updateData($SECTION_TABLE,$update_values,$SECTION_WHERE);
		//$_SESSION['msg']  =   "Subscribers has been deleted successfully.";  
	}
	else if($action_type  ==  "sorting")
	{
		$orderby  = $_POST['orderby'];
		$order    = $_POST['order'];
		if($order == "asc" || $order == "")
			$ORDER =  "desc";
		else
			$ORDER =  "asc";
	}
	if($orderby == "")
	{
		$ORDER =  "desc";
		$orderby = "id";
	}
	########################  General Query ####################################  
	$type = getMemberType();
	if($type!="Business")
	{
		$meb_dirId = getMemberSessionDirId();
		$sqlCondition = " AND p.bsb_dir_id=".$meb_dirId;
	}
	else
	{
		$mebId = getMemberSessionId();
		$sqlCondition = " AND p.bsb_id=".$mebId;
	}
	if ($xtraCondition != 'undefined' && $xtraCondition != "") {
		//$ts = $xtraCondition;
		$sql_query = "select * from ".$SECTION_TABLE." as p where (" . $xtraCondition . ") AND (p.".$SECTION_FIELD_PREFIX."status!='Deleted'".$sqlCondition.") order by " . $SECTION_FIELD_PREFIX . $orderby . " " . $ORDER;
	}
	else
	{
		$sql_query = "SELECT p.* FROM ".$SECTION_TABLE." as p WHERE p.".$SECTION_FIELD_PREFIX."id != 0 AND p.".$SECTION_FIELD_PREFIX."status != 'Deleted'".$sqlCondition." order by p.".$SECTION_FIELD_PREFIX.$orderby." ".$ORDER;

	}
	//echo $sql_query;    
	####################  Paging Query + Paging Code ##################################
	$paging_query = $sql_query;
	$paging_result  = $db->select($paging_query); 
	$count = count($paging_result);
	$per_page = SITE_PAGING_PER_PAGE; //rows per page
	$per_page = 10;
	$pages = ceil($count/$per_page);    
	#####################################################################################
	if($action_type == "paging")
	{
		if(!empty($_POST['page']))  
		{
			$page = $_POST['page'];
		}       
	}
	else
	{
		if(!empty($_POST['page']))  
			$page = $_POST['page'];
	  	else
		$page = 1;
	} 
	$list_query = $sql_query;
	if(!empty($per_page) && $_POST['page']!="all")
	{
		$start = ($page-1)*$per_page;
		if($start<0)
		{
			$start=0;
		}
		$list_query .= " limit $start,$per_page"; 
	} 
	$result_query  = $db->select($list_query);  
	$total_rows = count($result_query);
?>
<div class="clr"></div>
<aside>
	<div class="tab_content_holder second">
		<div class="tab_content_holder_inner">
			<?php
			if($total_rows>0)
			{
			?>
			<table width="100%" cellspacing="0" cellpadding="0">
				<tr>						
					<th width="20%"><label onclick="getAjaxSorting('<?php echo $SECTION ?>','<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>','<?php echo 'dir_id'; ?>','<?php echo $ORDER; ?>','<?php echo mysql_real_escape_string($xtraCondition); ?>','<?php echo $SECTION_MANAGE_PAGE; ?>','<?php echo $page; ?>');">Directory Name</label></th>
					<th width="20%"><label onclick="getAjaxSorting('<?php echo $SECTION ?>','<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>','<?php echo 'name'; ?>','<?php echo $ORDER; ?>','<?php echo mysql_real_escape_string($xtraCondition); ?>','<?php echo $SECTION_MANAGE_PAGE; ?>','<?php echo $page; ?>');">Business Name</label></th>
					<th width="26%"><label onclick="getAjaxSorting('<?php echo $SECTION ?>','<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>','<?php echo 'username'; ?>','<?php echo $ORDER; ?>','<?php echo mysql_real_escape_string($xtraCondition); ?>','<?php echo $SECTION_MANAGE_PAGE; ?>','<?php echo $page; ?>');">Email</label></th>
					<th width="20%"><label onclick="getAjaxSorting('<?php echo $SECTION ?>','<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>','<?php echo 'phone'; ?>','<?php echo $ORDER; ?>','<?php echo mysql_real_escape_string($xtraCondition); ?>','<?php echo $SECTION_MANAGE_PAGE; ?>','<?php echo $page; ?>');">Phone Number</label></th>
					<th width="14%">Action</th>
				</tr>
				<?php
				$j=1;
				for($i=0;$i<$total_rows;$i++)
				{
				?>
				<tr <?php if($i == $j){ echo 'class="light"'; $j = $j + 2;}?>>
					<td><?php $dir_fields = array("dir_name");
						$dir_where  = "dir_id = '".$result_query[$i][$SECTION_FIELD_PREFIX.'dir_id']."'";
						$dirRes = $db->selectData(TBL_DIRECTORY,$dir_fields,$dir_where,$extra="",1);
						echo $dirRes[0]["dir_name"]; 
					?></td>
					<td><?php echo $result_query[$i][$SECTION_FIELD_PREFIX.'name']." ".$result_query[$i][$SECTION_FIELD_PREFIX.'lname']; ?></td>
					<td><?php echo $result_query[$i][$SECTION_FIELD_PREFIX.'username']; ?></td>
					<td><?php echo $result_query[$i][$SECTION_FIELD_PREFIX.'phone']; ?></td>
					<td>
						<a href="view_records.php?sectionName=BusinessSub&bsb_id=<?php echo $result_query[$i][$SECTION_FIELD_PREFIX."id"];?>" class="fancybox fancybox.ajax tooltip" title="View" data-fancybox-type="ajax"><img src="<?php echo IMG_WWW; ?>equale-orange.png" /></a>
						<?php
							if($result_query[$i][$SECTION_FIELD_PREFIX."status"]=="Active")
							{
						?>
						<a href="javascript:void(0);" onclick="getAjaxStatusInactiveAction('<?php echo $SECTION; ?>','<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>',<?php echo $result_query[$i][$SECTION_FIELD_PREFIX."id"]; ?>,'','<?php echo $SECTION_MANAGE_PAGE;?>','<?php echo $page; ?>')" class="tooltip" title="Active"><img src="<?php echo IMG_WWW; ?>active.png" /></a>
						<?php 
							}
							else
							{
						?>
						<a href="javascript:void(0);" onclick="getAjaxStatusActiveAction('<?php echo $SECTION; ?>','<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>',<?php echo $result_query[$i][$SECTION_FIELD_PREFIX."id"]; ?>,'','<?php echo $SECTION_MANAGE_PAGE;?>','<?php echo $page; ?>')" class="tooltip" title="Inactive"><img src="<?php echo IMG_WWW; ?>inactive.png" /></a>
						<?php 
							}
						?>
						<a href="<?php echo getMemberURL($SECTION_MANAGE_PAGE,"Edit","bsb_id=".$result_query[$i][$SECTION_FIELD_PREFIX."id"]); ?>" class="tooltip" title="Edit"><img src="<?php echo IMG_WWW; ?>edit-orange.png" /></a>
						<a href="javascript:void(0);" class="tooltip" title="Delete" onclick="javascript:alertBoxMember('<?php echo $SECTION; ?>','<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>',<?php echo $result_query[$i][$SECTION_FIELD_PREFIX."id"]; ?>,'','<?php echo $SECTION_MANAGE_PAGE;?>');"><img src="<?php echo IMG_WWW; ?>delete.png" /></a>
					</td>
				</tr>
				<?php
				}
				?>  		  
			</table>
			<?php
			}
			else
			{
			?>
			<div class="no-record">No Records</div>
			<?php
			}
			?>
			<div class="clr"></div>
			<nav>
				<div class="paging">
					<ul>
					<?php
					for($t=1; $t<=$pages; $t++)
					{
						if($page==$t) 
						{
					?>										
							<li><a class="active" href="javascript:void(0);" <?php if($pages > 1){?> onclick="getAjaxPaging('<?php echo $SECTION; ?>','<?php echo $SECTION_TABLE; ?>','<?php echo $SECTION_FIELD_PREFIX; ?>','<?php echo $pages; ?>','<?php echo $t ?>','<?php echo mysql_real_escape_string($xtraCondition)."&orderby=".$orderby."&order=".$order; ?>','<?php echo $SECTION_MANAGE_PAGE;?>')" <?php } ?> ><?php echo $t ?></a></li>
					<?php
						}
						else 
						{
					?>
						<li><a href="javascript:void(0);" onclick="getAjaxPaging('<?php echo $SECTION; ?>','<?php echo $SECTION_TABLE; ?>','<?php echo $SECTION_FIELD_PREFIX; ?>','<?php echo $pages; ?>','<?php echo $t ?>','<?php echo mysql_real_escape_string($xtraCondition)."&orderby=".$orderby."&order=".$order; ?>','<?php echo $SECTION_MANAGE_PAGE;?>')"><?php echo $t ?></a></li>
					<?php
						}
					}
					?>
					</ul>            					  
				</div>
			</nav>
		</div>
	</div>               				   
</aside>
<script type="text/javascript">
    $(document).ready(function() {
        $('.tooltip').tooltipster();
		$.fancybox.close();
    });
</script>
