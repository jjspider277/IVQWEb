function getpagelisting(tableName,fieldPrefix){
    var ajax_value_file=ajax_folder+"getMemberslist.php"; 
    $.ajax({           
       type: "POST",
       url: ajax_value_file,
       data: "tableName="+tableName+"&fieldPrefix="+fieldPrefix,
       success: function(msg){
        if(msg != ""){
            $("#updatediv").html(msg);
        }
       }    
     });
}

function getAjaxStatusActiveAction(tableName,fieldPrefix,autoID,xtraCondition)
{
	var ajax_value_file=ajax_folder+"getMemberslist.php";	
	$.ajax({		   
	   type: "POST",
	   url: ajax_value_file,
	   data: "action_type=active"+"&tableName="+tableName+"&fieldPrefix="+fieldPrefix+"&autoID="+autoID+"&xtraCondition="+xtraCondition,
	   success: function(msg){
		if(msg != "")
		{
			$("#updatediv").html(msg);
		//		$('#td-msg').html(msg);
			//	$('#status-cls').html('<img src="images/inactive.png" alt="Inactive" title="Inactive" border=0>');
		}
	   }	
	 });
}

function getAjaxStatusInactiveAction(tableName,fieldPrefix,autoID,xtraCondition)
{
	var ajax_value_file=ajax_folder+"getMemberslist.php";	
	$.ajax({		   
	   type: "POST",
	   url: ajax_value_file,
	   data: "action_type=inactive"+"&tableName="+tableName+"&fieldPrefix="+fieldPrefix+"&autoID="+autoID+"&xtraCondition="+xtraCondition,
	   success: function(msg){
		if(msg != "")
		{
			$("#updatediv").html(msg);
//				$('#td-msg').html(msg);
	//			$('#status-cls').html('<img src="images/inactive.png" alt="Inactive" title="Active" border=0>');
		}
	   }	
	 });
}

function getAjaxDeleteAction(tableName,fieldPrefix,autoID,displayName,xtraCondition)
{
	if(!confirm('Are you sure want to delete?'))
	{
		return false;
	}
	else
	{
		var ajax_value_file=ajax_folder+"getMemberslist.php";	
		$.ajax({		   
		   type: "POST",
		   url: ajax_value_file,
		   data: "action_type=delete"+"&tableName="+tableName+"&fieldPrefix="+fieldPrefix+"&autoID="+autoID+"&displayName="+displayName+"&xtraCondition="+xtraCondition,
		   success: function(msg){
//			alert( "Data Saved: "+msg);
			if(msg != "")
			{
				$("#updatediv").html(msg);
			}
		   }	
		 });
	}
}

function getAjaxSorting(tableName,fieldPrefix,orderby,order,xtraCondition)
{

	var ajax_value_file=ajax_folder+"getMemberslist.php";	
	$.ajax({		   
	   type: "POST",
	   url: ajax_value_file,
	   data: "action_type=sorting"+"&tableName="+tableName+"&fieldPrefix="+fieldPrefix+"&orderby="+orderby+"&order="+order+"&xtraCondition="+xtraCondition,
	   success: function(msg){
		if(msg != "")
		{
			$("#updatediv").html(msg);
		//		$('#td-msg').html(msg);
			//	$('#status-cls').html('<img src="images/inactive.png" alt="Inactive" title="Inactive" border=0>');
		}
	   }	
	 });
}



function getAjaxPaging(tableName,fieldPrefix,pages,page,xtraCondition)
{
	var ajax_value_file=ajax_folder+"getMemberslist.php";	
	$.ajax({		   
	   type: "POST",
	   url: ajax_value_file,
	   data: "action_type=paging"+"&tableName="+tableName+"&fieldPrefix="+fieldPrefix+"&pages="+pages+"&page="+page+"&xtraCondition="+xtraCondition,
	   success: function(msg){
		if(msg != "")
		{
			$("#updatediv").html(msg);
		//		$('#td-msg').html(msg);
			//	$('#status-cls').html('<img src="images/inactive.png" alt="Inactive" title="Inactive" border=0>');
		}
	   }	
	 });
}

function getAjaxSearchByLetter(tableName,fieldPrefix,character)
{
    var ajax_value_file=ajax_folder+"getMemberslist.php";
	$.ajax({		   
	   type: "POST",
	   url: ajax_value_file,
	   data: "tableName="+tableName+"&fieldPrefix="+fieldPrefix+"&searchchar="+character,
	   
	   success: function(msg){
		if(msg != "")
		{
			$("#updatediv").html(msg);
//				$('#td-msg').html(msg);
	//			$('#status-cls').html('<img src="images/inactive.png" alt="Inactive" title="Active" border=0>');
		}
	   }	
	 });
    //}
	 
}

function getLiveSearch(tableName,fieldPrefix,xtraCondition)
{
	//var data=document.getElementsByName("search").value;
	var xtraCondition = document.getElementById("meb_name").value;
	
	var g=encodeURIComponent(xtraCondition);        
    if(xtraCondition!=''){
    var ajax_value_file=ajax_folder+"getMemberslist.php";
	$.ajax({		   
	   type: "POST",
	   url: ajax_value_file,
	   data: "tableName="+tableName+"&fieldPrefix="+fieldPrefix+"&xtraCondition="+g,	 
	   success: function(msg){
		if(msg != "")
		{
			$("#updatediv").html(msg);
			document.getElementById("meb_name").value = xtraCondition;
		}
	   }	
	 });
 }else{
	 
	 $( "#search" ).focus();
 }
    //}
	 
}