<?php
	error_reporting(1);
	header('Access-Control-Allow-Origin: *');
	
	include_once('../../../include/includeclass.php');
	$success = array();
	$action = $_POST['action'];
	if($action == 'GetDirList') {
		$SECTION_FIELD_PREFIX = $_POST['fieldPrefix'];
		$managePage = $_POST['managePage'];
		$searchchar = $_POST['search'];
		$SECTION_TABLE = $_POST['tableName'];
		$xtraCondition = $_POST['xtraCondition'];
		$SECTION_MANAGE_PAGE 	=   ADM_MANAGE_DIRECTORY;
		$SECTION_TABLE_CATEGORY	=   TBL_DIRECTORY_CATEGORY;
		$orderby = "name";
		$ORDER =	"asc";
		
		$dirName = "(select dic_name from ".$SECTION_TABLE_CATEGORY." where dic_id = dir_dic_id ) as Dirname";		
		if ($searchchar != "") {
    		 $sql_query = "SELECT p.*,".$dirName." FROM " . $SECTION_TABLE . " p WHERE p." . $SECTION_FIELD_PREFIX . "id != 0   AND  p.".$SECTION_FIELD_PREFIX."status!='Deleted' AND `dir_name` LIKE '%".$searchchar."%' order by p." . $SECTION_FIELD_PREFIX . $orderby . " " . $order;
    	} else if ($xtraCondition != 'undefined' && $xtraCondition != "") {
			$ts = "%" . $xtraCondition . "%";
			$sql_query = "select *,".$dirName." from ".$SECTION_TABLE." as p where (".$xtraCondition.") AND (p.".$SECTION_FIELD_PREFIX."status!='Deleted')  order by " . $SECTION_FIELD_PREFIX . $orderby . " " . $order;
		} else {
			$sql_query = "SELECT p.*,".$dirName." FROM ".$SECTION_TABLE." p WHERE p.".$SECTION_FIELD_PREFIX."id != 0 AND p.".$SECTION_FIELD_PREFIX."status != 'Deleted' order by p.".$SECTION_FIELD_PREFIX.$orderby." ".$order;
		}		
		
		//echo $sql_query;
		$excel_query = "SELECT p.*,".$dirName." FROM ".$SECTION_TABLE." p WHERE p.".$SECTION_FIELD_PREFIX."id != 0 AND p.".$SECTION_FIELD_PREFIX."status != 'Deleted' order by p.".$SECTION_FIELD_PREFIX.$orderby." ".$order;
		$excel_result  = $db->select($excel_query);	
		$excelcount = count($excel_result);
		#################################  Paging Query + Paging Code ##################################
		$paging_query = $sql_query;
		$paging_result  = $db->select($paging_query);	
		$count = count($paging_result);
		$per_page = SITE_PAGING_PER_PAGE; //rows per page
		$per_page = 5;
		$pages = ceil($count/$per_page);		
		#################################################################################################
		if(!empty($_POST['page']))	
			$page = $_POST['page'];
		else
			$page = 1;
		
		$list_query = $sql_query;
		if(!empty($per_page) && $_POST['page']!="all")
		{
			$start = ($page-1)*$per_page;
			if($start<0)
			{
				$start=0;
			}
			$list_query .= " limit $start,$per_page";	
		}

		$result_query = $db->select($list_query);	
		$total_rows = count($result_query);
		//echo $total_rows;
		if($total_rows > 0){
			$j = 1;
			$dir_data = '<ul>';
			for($i=0;$i<$total_rows;$i++){
				//$dir_data .= $result_query[$i]['Dirname'];
				$dir_data .= '<li><a href="#">'.$result_query[$i][$SECTION_FIELD_PREFIX.'name'].'</a></li>';
			}

			$dir_data .= '</ul>';

			if(count($pages)>0) {
			$pagination = '<div class="paging"><ul>';
				for($t=1;$t<=$pages;$t++) {
					if($page==$t) {
						$pagination .= '<li><a class="active" href="javascript:void(0);"';
						if($pages > 1){
							$pagination .= 'onclick="getAjaxPaging("Directory","'.$pages.'","'.$t.'")"'; 
						}
						$pagination .= '>'.$t.'</a></li>';
						} else {
						$pagination .= '<li><a href="javascript:void(0);"';
						if($pages > 1){ 
							$pagination .= 'onclick="getAjaxPaging("Directory","'.$pages.'","'.$t.'")"';
						}
						$pagination .= '>'.$t.'</a></li>';
					}
				}
				$pagination .= '</div></ul>';			
			}
			
			$success = array("status" => 'true', 'data' => $dir_data, 'pagination' => $pagination);			
		} else {
			$dir_data = '<ul>';
			$dir_data .= '<li>No record found.</li>';
			$dir_data .= '</ul>';
			$success = array("status" => 'false', 'data'=>'No record found.');
		}	
	} else {
		$success = array("status" => 'false');
	}
	echo json_encode($success);
	exit();
?>	