<?php
	include_once('../../include/includeclass.php');
	
	$action_type			=	$_POST['action_type'];
	$SECTION_TABLE			= 	$_POST['tableName'];
	$SECTION_FIELD_PREFIX	=	$_POST['fieldPrefix'];
	$SECTION_AUTO_ID 		=	trim($_POST['autoID']);
	
	/*$SECTION_MANAGE_PAGE	=	$_POST['managePage'];*/
	$SECTION_WHERE			=	$SECTION_FIELD_PREFIX."id='".$SECTION_AUTO_ID."'";
	$xtraCondition			=	 $_POST['xtraCondition'] ;
	$SECTION_NAME			=	$_POST['displayName'];	
  	$SECTION="Contact";

	if($action_type=="Add" || $action_type=="Update")
	{
		$add_values["dco_dir_id"]		=	trim($_POST["dco_dir_id"]);
		$add_values["dco_name"]			=	trim($_POST["dco_name"]);
		$add_values["dco_title"]		=	trim($_POST["dco_title"]);
		$add_values["dco_mob_phone"]	=	trim($_POST["dco_mob_phone"]);
		$add_values["dco_email"]		=	trim($_POST["dco_email"]);
		
		if($action_type != 'Update'){	
			$add_values["dco_status"]		=	"Active";
			$add_values["dco_created_id"]	=	getAdminSessionId();
			$add_values["dco_created_date"]	=	date('Y-m-d H:i:s');
			$GPDetail_result = $db->insertData($SECTION_TABLE, $add_values);
    		//echo "Directory contact has been inserted successfully.";
    	}else{
			$add_values["dco_updated_id"]	=	getAdminSessionId();
			$add_values["dco_updated_date"]	=	date('Y-m-d H:i:s');	
        	$GPDetail_result = $db->updateData($SECTION_TABLE, $add_values, $SECTION_WHERE); 
			//echo "Directory contact has been updated successfully.";
        }
		exit;
	}
	else if($action_type ==  "Edit")
	{
		$sql_query = "SELECT * FROM ".$SECTION_TABLE." WHERE ".$SECTION_FIELD_PREFIX."id='".$SECTION_AUTO_ID."'";
		$result  = $db->select($sql_query);
		echo $result[0]["dco_id"].",".$result[0]["dco_name"].",".$result[0]["dco_title"].",".$result[0]["dco_mob_phone"].",".$result[0]["dco_email"];
		exit;
	}
	else if($action_type ==  "delete")
	{
		$update_values[$SECTION_FIELD_PREFIX.'status']      = "Deleted";
		$update_values[$SECTION_FIELD_PREFIX . 'updated_id'] 	= getAdminSessionId();
        $update_values[$SECTION_FIELD_PREFIX . 'updated_date'] 	= date("Y-m-d H:i:s");
		//$GPDetail_result  =   $db->deleteData($SECTION_TABLE,$SECTION_WHERE);
		$GPDetail_result = $db->updateData($SECTION_TABLE,$update_values,$SECTION_WHERE);
		//$_SESSION['msg']  =   "Directory has been deleted successfully.";  
	}
	else if($action_type  ==  "active") // if $action_type == active then update values = Inactive
	{
		$update_values[$SECTION_FIELD_PREFIX.'status']      = "Inactive";
		$update_values[$SECTION_FIELD_PREFIX.'updated_id']    = $_SESSION['sdm_id'];
		$update_values[$SECTION_FIELD_PREFIX.'updated_date']  = date('Y-m-d H:i:s');
		$GPDetail_result = $db->updateData($SECTION_TABLE,$update_values,$SECTION_WHERE);
		//$_SESSION['msg']  =   "Directory has been inactive successfully."; 
 	}
  	else if($action_type  ==  "inactive") // if $action_type == inactive then update values = active
 	 {
		$update_values[$SECTION_FIELD_PREFIX.'status']      = "Active";
		$update_values[$SECTION_FIELD_PREFIX.'updated_id']    = $_SESSION['sdm_id'];
		$update_values[$SECTION_FIELD_PREFIX.'updated_date']  = date('Y-m-d H:i:s');
		$GPDetail_result = $db->updateData($SECTION_TABLE,$update_values,$SECTION_WHERE);
		//$_SESSION['msg']  =   "Directory has been active successfully."; 
	}
	else if($action_type  ==  "sorting")
  	{
		$orderby  = $_POST['orderby'];
		$order    = $_POST['order'];
		if($order == "asc" || $order == "")
			$ORDER =  "desc";
		else
			$ORDER =  "asc";
  	}
	if($orderby == "")
  	{
  		$ORDER =  "desc";
    	$orderby = "id";
  	}
  	
	$sql_query = "SELECT p.* FROM ".$SECTION_TABLE." p WHERE p.".$SECTION_FIELD_PREFIX."id != 0 AND p.".$SECTION_FIELD_PREFIX."status != 'Deleted' AND ".$xtraCondition." order by p.".$SECTION_FIELD_PREFIX.$orderby." ".$ORDER;
  
  	######################### Paging Query + Paging Code ##################################
    $paging_query = $sql_query;
    $paging_result  = $db->select($paging_query); 
    $count = count($paging_result);
    $per_page = SITE_PAGING_PER_PAGE; //rows per page
    $per_page = 10;
    $pages = ceil($count/$per_page);    
    ########################################################################################
    if ($action_type == "paging")
    {
      if(!empty($_POST['page']))  
      {
        $page = $_POST['page'];
      }       
    }
    else
    {
      if(!empty($_POST['page']))  
        $page = $_POST['page'];
      else
        $page = 1;
    } 
    $list_query = $sql_query;
    if(!empty($per_page) && $_POST['page']!="all")
    {
      $start = ($page-1)*$per_page;
      if($start<0)
      {
        $start=0;
      }
      $list_query .= " limit $start,$per_page"; 
    } 
	$result_query  = $db->select($list_query);  
    $total_rows = count($result_query);

?>
<div class="tab_content_holder directory">
	<div class="tab_content_holder_inner" style="padding:0px;">					  	   
		<div class="block-part normal">						   		
			<div class="listing-view">
			<?php if($total_rows > 0){ ?>
				<table width="100%" cellspacing="0" cellpadding="0">
					<tr>						
						<th width="25%"><label onclick="getAjaxSorting('<?php echo $SECTION; ?>','<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>','<?php echo 'name'; ?>','<?php echo $ORDER; ?>','<?php echo  mysql_real_escape_string($xtraCondition); ?>','<?php echo $SECTION_MANAGE_PAGE; ?>','<?php echo $page; ?>');">Contact Name</label></th>
						<th width="15%"><label onclick="getAjaxSorting('<?php echo $SECTION; ?>','<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>','<?php echo 'title'; ?>','<?php echo $ORDER; ?>','<?php echo  mysql_real_escape_string($xtraCondition); ?>','<?php echo $SECTION_MANAGE_PAGE; ?>','<?php echo $page; ?>');">Title</label></th>
						<th width="20%"><label onclick="getAjaxSorting('<?php echo $SECTION; ?>','<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>','<?php echo 'mob_phone'; ?>','<?php echo $ORDER; ?>','<?php echo  mysql_real_escape_string($xtraCondition); ?>','<?php echo $SECTION_MANAGE_PAGE; ?>','<?php echo $page; ?>');">Phone</label></th>
						<th width="25%"><label onclick="getAjaxSorting('<?php echo $SECTION; ?>','<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>','<?php echo 'email'; ?>','<?php echo $ORDER; ?>','<?php echo  mysql_real_escape_string($xtraCondition); ?>','<?php echo $SECTION_MANAGE_PAGE; ?>','<?php echo $page; ?>');">Email</label></th>    	
						<th width="15%">Action</th>
					</tr>
					<?php
						$j = 1;
						for($i=0;$i<$total_rows;$i++)
						{
					?>	
					<tr <?php if($i == $j){ echo 'class="light"'; $j = $j + 2;}?>>
						<td><?php echo $result_query[$i][$SECTION_FIELD_PREFIX.'name']; ?></td>
						<td><?php echo $result_query[$i][$SECTION_FIELD_PREFIX.'title']; ?></td>
						<td><?php echo $result_query[$i][$SECTION_FIELD_PREFIX.'mob_phone']; ?></td>
						<td><a href="mailto:<?php echo $result_query[$i][$SECTION_FIELD_PREFIX.'email']; ?>"><?php echo $result_query[$i][$SECTION_FIELD_PREFIX.'email']; ?></a></td>
						<td>										
							<a class="tooltip" title="Edit" href="javascript:void(0);" onclick="editRecords('<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>',<?php echo $result_query[$i][$SECTION_FIELD_PREFIX."id"]; ?>)"><img src="<?php echo IMG_WWW; ?>edit.png"></a>
							<!--<a class="tooltip" id="DelId_<?php echo $result_query[$i][$SECTION_FIELD_PREFIX."id"]; ?>" title="Delete" href="javascript:void(0);" onclick="getAjaxDeleteAction('<?php echo $SECTION;?>','<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>',<?php echo $result_query[$i][$SECTION_FIELD_PREFIX."id"]; ?>,'<?php echo $xtraCondition; ?>','<?php echo $SECTION_MANAGE_PAGE; ?>');"><img src="<?php echo IMG_WWW; ?>delete.png"></a>-->
							<a href="javascript:void(0);"  id="DelId_<?php echo $result_query[$i][$SECTION_FIELD_PREFIX."id"]; ?>" class="tooltip" title="Delete" onclick="javascript:alertBox('<?php echo $SECTION;?>','<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>',<?php echo $result_query[$i][$SECTION_FIELD_PREFIX."id"]; ?>,'<?php echo $xtraCondition; ?>','<?php echo $SECTION_MANAGE_PAGE;?>');"><img src="<?php echo IMG_WWW; ?>delete.png" /></a>	
						</td>
					</tr>
					<?php 
						}						
					?>
				</table>
			<?php } else {?>
				<div class="no-record">No Records</div>
			<?php }?>
				<div class="clr"></div> 
				<nav>
            		<div class="paging">
            			<ul>
						<?php
						for($t=1; $t<=$pages; $t++)
						{
							if($page==$t) 
							{
						?>										
							<li><a class="active" href="javascript:void(0);" <?php if($pages > 1){?> onclick="getAjaxPaging('<?php echo $SECTION; ?>','<?php echo $SECTION_TABLE; ?>','<?php echo $SECTION_FIELD_PREFIX; ?>','<?php echo $pages; ?>','<?php echo $t; ?>','<?php echo mysql_real_escape_string($xtraCondition)."&orderby=".$orderby."&order=".$order?>','')"<?php }?>><?php echo $t ?></a></li>
						<?php
							}
							else 
							{
						?>
							<li><a href="javascript:void(0);" <?php if($pages > 1){?> onclick="getAjaxPaging('<?php echo $SECTION; ?>','<?php echo $SECTION_TABLE; ?>','<?php echo $SECTION_FIELD_PREFIX; ?>','<?php echo $pages; ?>','<?php echo $t; ?>','<?php echo mysql_real_escape_string($xtraCondition)."&orderby=".$orderby."&order=".$order?>','')"<?php }?>><?php echo $t ?></a></li>
						<?php
							}
						}
						?>          					  	  
            			</ul>            					  
					</div>
				</nav>
			</div>
		</div>
		<div class="clr"></div>
	</div>
</div>	
	
<script type="text/javascript">
    $(document).ready(function() {
        $('.tooltip').tooltipster();
		$.fancybox.close();
    });
</script>
