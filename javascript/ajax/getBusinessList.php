<?php
	include_once('../../include/includeclass.php');

    $action_type			=	$_REQUEST['action_type'];
	$SECTION_TABLE			= 	$_REQUEST['tableName'];
	$SECTION_FIELD_PREFIX	=	$_REQUEST['fieldPrefix'];
	$SECTION_AUTO_ID 		=	$_REQUEST['autoID'];
	$SECTION_MANAGE_PAGE	=	$_REQUEST['managePage'];
	$SECTION_WHERE			=	$SECTION_FIELD_PREFIX."id='".$SECTION_AUTO_ID."'";
	$xtraCondition			=	stripslashes($_REQUEST['xtraCondition']);
	$SECTION_NAME			=	$_REQUEST['displayName'];
	$searchchar				=	$_REQUEST['searchchar'];
	/*echo "<pre>";
	print_r($_POST);
	echo "</pre>";
	exit;*/
    $searchText             =	$_REQUEST['search_text'];

    $SECTION_TABLE= TBL_MEMBER_BUSINESS;
    $uploadFILEWWW = UPLOAD_WWW_BUSINESS_FILE;


	$SECTION="Business";

	$total_language = count($result_language);
	#################################################################

	if($action_type ==  "delete") {
		$update_values[$SECTION_FIELD_PREFIX.'status']      	= "Deleted";
		$update_values[$SECTION_FIELD_PREFIX . 'updated_id'] 	= $_SESSION['meb_id'];
        $update_values[$SECTION_FIELD_PREFIX . 'updated_date'] 	= date("Y-m-d H:i:s");
		$GPDetail_result = $db->updateData($SECTION_TABLE,$update_values,$SECTION_WHERE);
		//$_SESSION['msg']  =   "Subscribers has been deleted successfully.";  
	}
	
	else if($action_type  ==  "sorting") {
		$orderby  = $_REQUEST['orderby'];
		$order    = $_REQUEST['order'];
		if($order == "asc" || $order == "")
			$ORDER =  "desc";
		else
			$ORDER =  "asc";
	}

    if($orderby == "") {
		$ORDER =  "desc";
		$orderby = "id";
	}
	########################  General Query #########################################  

    $sub_fields = array("sbm_sub_id");
    $sub_where  = "sbm_dir_id= ".getMemberSessionDirId()." AND sbm_status = 'Active'";
    $subRes 	= $db->selectData(TBL_MEMBER_SUBSCRIBE_DIRECTORY,$sub_fields,$sub_where,$extra="",2);
    //$sql ="select sub_id from ".TBL_MEMBER_SUBSCRIBERS." WHERE sub_dir_id=".$id;
    for($j=0;$j<count($subRes);$j++) {
        if($j==count($subRes)-1) {
            $addComma = "";
        } else {
            $addComma = ",";
        }
        $allSub .= $subRes[$j]["sbm_sub_id"].$addComma;
    }

	$type = getMemberType();
	if($type!="Business") {
		$meb_dirId = getMemberSessionDirId();
		$sqlCondition = "AND p.bus_meb_id IN (".$meb_dirId.")";

        if($meb_dirId == ""){
            $meb_dirId = 0;
        }
		if ($searchText != 'undefined' && $searchText != "") {
            $wh = '';
            $wh = " (bus_title LIKE '%".$searchText."%' OR bus_name LIKE '%".$searchText."%' OR bus_lname LIKE '%".$searchText."%' OR bus_company LIKE '%".$searchText."%' OR bus_phone LIKE '%".$searchText."%' OR bus_city LIKE '%".$searchText."%' OR bus_email LIKE '%".$searchText."%' OR bus_zip LIKE '%".$searchText."%') AND";

            $uni="";
            if($allSub!='')
                $uni = "UNION select * , 'Subscriber' AS Cardtype  From ".TBL_MEMBER_BUSINESS_SUB." where bus_status='Active' AND ".$wh." bus_sub_id in(".$allSub.")";

			$sql_query="SELECT * , 'Member' AS Cardtype  FROM ".TBL_MEMBER_BUSINESS." WHERE bus_dir_id IN (".$meb_dirId.") AND ".$wh." bus_status != 'Deleted' ".$uni." ORDER BY ".$SECTION_FIELD_PREFIX.$orderby." ".$ORDER;
		} else {
            $uni="";
            if($allSub!='')
                $uni = "UNION select * , 'Subscriber' AS Cardtype  From ".TBL_MEMBER_BUSINESS_SUB." where bus_status='Active' AND bus_sub_id in(".$allSub.")";

			$sql_query="SELECT * , 'Member' AS Cardtype  FROM ".TBL_MEMBER_BUSINESS." WHERE bus_dir_id IN (".$meb_dirId.") AND bus_status != 'Deleted' ".$uni." ORDER BY ".$SECTION_FIELD_PREFIX.$orderby." ".$ORDER;
        }
	} else {
  
		$mebId = getMemberSessionId();
		$sqlCondition = " AND bus_sub_id=".$mebId;

		$sql_query = "SELECT * FROM ".$SECTION_TABLE." WHERE ".$SECTION_FIELD_PREFIX."id != 0 AND ".$SECTION_FIELD_PREFIX."status != 'Deleted'".$sqlCondition." order by ".$SECTION_FIELD_PREFIX.$orderby." ".$ORDER;
	}

	##########################  Paging Query + Paging Code #############################
	$paging_query = $sql_query;
	$paging_result  = $db->select($paging_query); 
	$count = count($paging_result);
	$per_page = SITE_PAGING_PER_PAGE; //rows per page
	$per_page = 8;
	$pages = ceil($count/$per_page);
	#######################################################################################
	if($action_type == "paging") {
		if(!empty($_REQUEST['page'])) {
			$page = $_REQUEST['page'];
		}       
	} else {
		if(!empty($_REQUEST['page']))
			$page = $_REQUEST['page'];
	  	else
		$page = 1;
	} 
	$list_query = $sql_query;
	if(!empty($per_page) && $_REQUEST['page']!="all") {
		$start = ($page-1)*$per_page;
		if($start<0) {
			$start=0;
		}
		$list_query .= " limit $start,$per_page"; 
	} 
	$result_query  = $db->select($list_query);  
	$total_rows = count($result_query);
	//echo $ms = ajaxMsg($_SESSION['msg']);
?>
<?php
    if($page>1)
        echo '<div class="ajaxdd" id="updatediv">';
?>
<?php if($total_rows>0) { ?>
<?php $j=1; for($i=0;$i<$total_rows;$i++) { ?>
    <?php
        $profile_path = 'images/group-user-list-icon.png';

        $bus_id = $result_query[$i][$SECTION_FIELD_PREFIX.'id'];
        if($result_query[$i][$SECTION_FIELD_PREFIX.'picture_file']!="") {
            if($result_query[$i]['Cardtype']=='Subscriber') {
                $fileNameExt = explode(".", $result_query[$i][$SECTION_FIELD_PREFIX.'picture_file']);
                $fileExt 	 = $fileNameExt[count($fileNameExt)-1];
                $allowedImgExts = array("gif","jpg","jpeg","png","GIF","JPG","JPEG","PNG");

                if(in_array($fileExt,$allowedImgExts)) {
                    $profile_path = $uploadFILEWWW.'Business_Sub/'.$bus_id."/".$result_query[$i][$SECTION_FIELD_PREFIX.'picture_file'];
                }
            } else {
                $fileNameExt = explode(".", $result_query[$i][$SECTION_FIELD_PREFIX.'picture_file']);
                $fileExt 	 = $fileNameExt[count($fileNameExt)-1];
                $allowedImgExts = array("gif","jpg","jpeg","png","GIF","JPG","JPEG","PNG");

                if(in_array($fileExt,$allowedImgExts)) {
                    $profile_path = $uploadFILEWWW.$bus_id."/".$result_query[$i][$SECTION_FIELD_PREFIX.'picture_file'];
                }
            }
        }
    ?>
    <div class="group-setion-col gr_on_view_card123">
        <?php if($result_query[$i]['Cardtype']=='Member') { ?>
            <div class="admin_memebr_t_l"></div>
        <?php } ?>
        <div class="group-setion-col-img group-setion-col-img-1"><img src="<?php echo $profile_path; ?>"></div>
        <div class="group-setion-col-h3"><?php echo $result_query[$i][$SECTION_FIELD_PREFIX.'name']." ". $result_query[$i][$SECTION_FIELD_PREFIX.'lname']; ?></div>
        <div class="group-setion-col-p"><?php echo $result_query[$i][$SECTION_FIELD_PREFIX.'title']; ?><br>
            <?php echo $result_query[$i][$SECTION_FIELD_PREFIX.'company']; ?></div>
        <div class="group-setion-col-hover gr_hover_this">
            <div class="hover-icon">
                <?php if($result_query[$i]['Cardtype']=='Member') { ?>
                    <div class="admin_memebr_t_l admin_memebr_t_l_in"><span style="display: none;">Created by Administrator</span></div>
                <?php } ?>
                <a href="view_records.php?sectionName=Business&type=<?php echo $result_query[$i]["Cardtype"]; ?>&bus_id=<?php echo $result_query[$i][$SECTION_FIELD_PREFIX."id"];?>" class="fancybox fancybox.ajax tooltip hover-icon-margin" title="View" data-fancybox-type="ajax"><img src="images/view-icon.png"></a>
                <?php if($result_query[$i]['Cardtype']=='Member') { ?>
                    <a href="<?php echo getMemberURL($SECTION_MANAGE_PAGE,"Edit","bus_id=".$result_query[$i][$SECTION_FIELD_PREFIX."id"]); ?>" title="Edit" class="tooltip hover-icon-margin"><img src="images/efit-text.png"></a>
                    <a href="javascript:void(0);" class="tooltip" title="Delete" onclick="javascript:alertBoxMember('<?php echo $SECTION; ?>','<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>',<?php echo $result_query[$i][$SECTION_FIELD_PREFIX."id"]; ?>,'','<?php echo $SECTION_MANAGE_PAGE;?>');"><img src="images/remove-icon.png"></a>
                <?php } ?>
            </div>
        </div>
    </div>
    <?php //echo $result_query[$i][$SECTION_FIELD_PREFIX.'city']; ?>
	<?php //echo $result_query[$i][$SECTION_FIELD_PREFIX.'state'];
	    $state_fields = array("sta_name");
		$state_where  = "sta_id = '".$result_query[$i][$SECTION_FIELD_PREFIX.'sta_id']."'";
		$staRes = $db->selectData(TBL_STATE,$state_fields,$state_where,$extra="",1);
		//echo $staRes[0]["sta_name"];
	?>
	<?php //echo $result_query[$i][$SECTION_FIELD_PREFIX.'phone']; ?>
	<?php //echo $result_query[$i][$SECTION_FIELD_PREFIX.'email']; ?>

    <?php }
    } else { ?>
        <div class="no-record">No member found.</div>
	<?php } ?>

<?php
if($pages>1)
    echo '</div>';
?>

<div class="pagination-main" style="display: none;">
    <div class="pagination">
        <ul>
        <?php for($t=1; $t<=$pages; $t++) {
            if($page==$t) { ?>
                <li><a class="active" href="javascript:void(0);" <?php if($pages > 1){?> onclick="getAjaxPaging('<?php echo $SECTION; ?>','<?php echo $SECTION_TABLE; ?>','<?php echo $SECTION_FIELD_PREFIX; ?>','<?php echo $pages; ?>','<?php echo $t ?>','<?php echo mysql_real_escape_string($xtraCondition)."&orderby=".$orderby."&order=".$order; ?>','<?php echo $SECTION_MANAGE_PAGE;?>')" <?php } ?> ><?php echo $t ?></a></li>
        <?php } else { ?>
            <li><a href="javascript:void(0);" onclick="getAjaxPaging('<?php echo $SECTION; ?>','<?php echo $SECTION_TABLE; ?>','<?php echo $SECTION_FIELD_PREFIX; ?>','<?php echo $pages; ?>','<?php echo $t ?>','<?php echo mysql_real_escape_string($xtraCondition)."&orderby=".$orderby."&order=".$order; ?>','<?php echo $SECTION_MANAGE_PAGE;?>')"><?php echo $t ?></a></li>
        <?php } } ?>
        </ul>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.tooltip').tooltipster();
		$.fancybox.close();

        curr = '';
        curr = '<?php echo $page; ?>';

        total = '';
        total = '<?php echo $pages; ?>';

        $('#total_page').val(total);
        $('#current_page').val(curr);

        if(curr<='1') {
            infinite_f();
        }
    });
</script>