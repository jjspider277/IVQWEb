<?php 
	include_once('../../include/includeclass.php');
	$action_type			=	$_REQUEST['action_type'];
	$SECTION_TABLE 			= 	$_REQUEST['tableName'];
	$SECTION_FIELD_PREFIX	=	$_REQUEST['fieldPrefix'];
	$SECTION_AUTO_ID 		=	$_REQUEST['autoID'];
	$SECTION_WHERE			=	$SECTION_FIELD_PREFIX."id='".$SECTION_AUTO_ID."'";
	$xtraCondition			=	stripslashes($_REQUEST['xtraCondition']);
	$SECTION_NAME			=	$_REQUEST['displayName'];
	$language_prefix		=	$_REQUEST['language_prefix'];
	$SECTION_MANAGE_PAGE 	=   ADM_MANAGE_DIRECTORY;
	$searchchar 			= 	$_REQUEST['searchchar'];
	$SECTION_TABLE_CATEGORY	=   TBL_DIRECTORY_CATEGORY;

    $searchText             =	$_REQUEST['search_text'];

	$total_language = count($result_language);

    $member_id = $_SESSION['meb_admin_id'];

	if($action_type	==	"delete")
	{		
		$deleteRec = deleteDirectory($SECTION_AUTO_ID);
	}
	else if($action_type	==	"active") // if $action_type == active then update values = Inactive
	{
		$update_values[$SECTION_FIELD_PREFIX.'status']			= "Inactive";
		$update_values[$SECTION_FIELD_PREFIX.'updated_id']		= getAdminSessionId();
		$update_values[$SECTION_FIELD_PREFIX.'updated_date']	= date('Y-m-d H:i:s');
		$GPDetail_result = $db->updateData($SECTION_TABLE,$update_values,$SECTION_WHERE.$xtraCondition);
		//$_SESSION['msg']	= 	"Directory has been inactive successfully.";	
	}
	else if($action_type	==	"inactive") // if $action_type == inactive then update values = active
	{
		$update_values[$SECTION_FIELD_PREFIX.'status']			= "Active";
		$update_values[$SECTION_FIELD_PREFIX.'updated_id']		= getAdminSessionId();
		$update_values[$SECTION_FIELD_PREFIX.'updated_date']	= date('Y-m-d H:i:s');
		$GPDetail_result = $db->updateData($SECTION_TABLE,$update_values,$SECTION_WHERE.$xtraCondition);
		//$_SESSION['msg'] 	= 	"Directory has been active successfully.";	
	}
	else if($action_type	==	"sorting")
	{
		$orderby	=	$_REQUEST['orderby'];
		$order		=	$_REQUEST['order'];
		if($order == "asc" || $order == "")
			$ORDER =	"desc";
		else
			$ORDER =	"asc";
	}

	if($orderby == "")
	{
		$ORDER =  "desc";
		$orderby = "id";
	}

	$dirName = "(select dic_name from ".$SECTION_TABLE_CATEGORY." where dic_id = dir_dic_id ) as Dirname";
	##################################  General Query ############################################### 	
	if ($searchchar != 'undefined' && $searchchar != "") {
    	if ($searchchar == 'other') {
    	    $sql_query = "SELECT p.*,".$dirName." FROM " . $SECTION_TABLE . " p WHERE p." . $SECTION_FIELD_PREFIX . "id != 0   AND  p.".$SECTION_FIELD_PREFIX."status='Active'  AND `dir_name` REGEXP '^[^a-zA-Z]' AND ".$SECTION_FIELD_PREFIX."created_id = '".$member_id."' order by p." . $SECTION_FIELD_PREFIX . $orderby . " " . $ORDER;
    	} else {
        	if ($searchchar == 'all') {
            
        	    $sql_query = "SELECT p.*,".$dirName." FROM " . $SECTION_TABLE . " p WHERE p." . $SECTION_FIELD_PREFIX . "id != 0  AND p.".$SECTION_FIELD_PREFIX."status='Active' AND ".$SECTION_FIELD_PREFIX."created_id = '".$member_id."' order by p." . $SECTION_FIELD_PREFIX . $orderby . " " . $ORDER;
        	} else {
        	    $ts = $searchchar . "%";
        	    $sql_query = "select *,".$dirName." from ".$SECTION_TABLE." as p where p.dir_name LIKE '" . $ts . "' AND p.".$SECTION_FIELD_PREFIX."status='Active' AND ".$SECTION_FIELD_PREFIX."created_id = '".$member_id."' order by " . $SECTION_FIELD_PREFIX . $orderby . " " . $ORDER;
        	}
    	}
	} else if ($xtraCondition != 'undefined' && $xtraCondition != "") {
		$ts = "%" . $xtraCondition . "%";
		$sql_query = "select *,".$dirName." from ".$SECTION_TABLE." as p where (".$xtraCondition.") AND (p.".$SECTION_FIELD_PREFIX."status='Active') AND ".$SECTION_FIELD_PREFIX."created_id = '".$member_id."' order by " . $SECTION_FIELD_PREFIX . $orderby . " " . $ORDER;
	} else if ($searchText != "") {
        $ts = "%" . $xtraCondition . "%";
        $chr_wher = "(".$SECTION_FIELD_PREFIX."name LIKE '%".$searchText."%' OR ".$SECTION_FIELD_PREFIX."city LIKE '%".$searchText."%' OR ".$SECTION_FIELD_PREFIX."zip LIKE '%".$searchText."%')";

        $sql_query = "select *,".$dirName." from ".$SECTION_TABLE." as p where ".$chr_wher." AND (p.".$SECTION_FIELD_PREFIX."status='Active') AND ".$SECTION_FIELD_PREFIX."created_id = '".$member_id."' order by " . $SECTION_FIELD_PREFIX . $orderby . " " . $ORDER;
    } else {
		$sql_query = "SELECT p.*,".$dirName." FROM ".$SECTION_TABLE." p WHERE p.".$SECTION_FIELD_PREFIX."id != 0 AND p.".$SECTION_FIELD_PREFIX."status = 'Active' AND ".$SECTION_FIELD_PREFIX."created_id = '".$member_id."' order by p.".$SECTION_FIELD_PREFIX.$orderby." ".$ORDER;
	}

    //echo $sql_query;
	$excel_query = "SELECT p.*,".$dirName." FROM ".$SECTION_TABLE." p WHERE p.".$SECTION_FIELD_PREFIX."id != 0 AND p.".$SECTION_FIELD_PREFIX."status = 'Active' AND ".$SECTION_FIELD_PREFIX."created_id = '".$member_id."' order by p.".$SECTION_FIELD_PREFIX.$orderby." ".$ORDER;
	$excel_result  = $db->select($excel_query);	
	$excelcount = count($excel_result);	
	
		#################################  Paging Query + Paging Code ##################################
		$paging_query = $sql_query;
		$paging_result  = $db->select($paging_query);	
		$count = count($paging_result);
		$per_page = SITE_PAGING_PER_PAGE; //rows per page
		$per_page = 8;
		$pages = ceil($count/$per_page);		
		#################################################################################################
		if ($action_type == "paging")
		{
			if(!empty($_REQUEST['page']))
			{
				$page = $_REQUEST['page'];
			}  			
		}
		else
		{
			if(!empty($_REQUEST['page']))
				$page = $_REQUEST['page'];
			else
		 		$page = 1;
		}	
		$list_query = $sql_query;
		if(!empty($per_page) && $_REQUEST['page']!="all")
		{
			$start = ($page-1)*$per_page;
			if($start<0)
			{
				$start=0;
			}
			$list_query .= " limit $start,$per_page";	
		}	
		$result_query  = $db->select($list_query);	
		$total_rows = count($result_query);
		/*echo $ms = ajaxMsg($_SESSION['msg']);
	    $xtraCondition = "";*/
?>

<?php if($action_type	==	"delete" && $deleteRec=="NotDeleted") { ?>
	<nav id="mesg" style="color: #FF0000;font-size: 14px;padding: 10px;">You can not delete this records</nav>
	<script type="text/javascript">$("#mesg").delay(3000).fadeOut();</script>
<?php }
        if($pages>1)
            echo '<div class="group-setion-row" id="updatediv">';
        if($total_rows > 0){
            $j = 1;
            for($i=0;$i<$total_rows;$i++) {

                $group_image='images/group-user-list-icon.png';
                if($result_query[$i][$SECTION_FIELD_PREFIX.'logo_file']!='') {
                    $bus_id=$result_query[$i][$SECTION_FIELD_PREFIX.'id'];
                    $filename=$result_query[$i][$SECTION_FIELD_PREFIX.'logo_file'];

                    $group_image=UPLOAD_WWW_DIRECTORY_FILE.$bus_id."/".$filename;
                }
                $yotal_eqry='SELECT COUNT(sbm_id) AS total from tbl_member_subscribe_directory where sbm_dir_id = "'.$result_query[$i][$SECTION_FIELD_PREFIX."id"].'" AND sbm_status="Active"';
                $total_follo = $db->select($yotal_eqry);
                $totald = number_format($total_follo[0]['total']);

?>
                <div class="group-setion-col gr_on_view_card123" data_url="<?php echo MEMBER_WWW.MEB_INDEX_PARAMETER.MEB_VIEW_BUSINESS.'&dir_id='.$result_query[$i][$SECTION_FIELD_PREFIX."id"]?>" class="hover-icon-margin tooltip edit_directory" data_id="<?php echo $result_query[$i][$SECTION_FIELD_PREFIX."id"];?>">
                    <div class="group-setion-col-img"><img style="height: 121px;" src="<?php echo $group_image; ?>"></div>
                    <div class="group-setion-col-h3"><?php echo $result_query[$i][$SECTION_FIELD_PREFIX.'name']; ?></div>
                    <div class="group-setion-col-hover">
                        <div class="hover-icon">
                            <?php /* ?>
                            <a href="view_records.php?sectionName=Directory&dir_id=<?php echo $result_query[$i][$SECTION_FIELD_PREFIX."id"];?>" class="dir_view_<?php echo $result_query[$i][$SECTION_FIELD_PREFIX."id"]; ?> fancybox fancybox.ajax view_group tooltip hover-icon-margin"  data-fancybox-type="ajax" title="View"><img src="images/view-icon.png"></a>
                            <a href="javascript:void(0);" class="tooltip delete_directory" title="Manage" onclick="javascript:alertBox('Directory','<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>',<?php echo $result_query[$i][$SECTION_FIELD_PREFIX."id"]; ?>,'','<?php echo $SECTION_MANAGE_PAGE;?>');"><img src="images/remove-icon.png"></a>
                            <?php */ ?>

                            <a href="<?php echo getMemberActionLink3(MEB_CREATE_GROUP_1,$language_prefix,'Edit',$SECTION_FIELD_PREFIX."id",$result_query[$i][$SECTION_FIELD_PREFIX."id"])?>" class="hover-icon-margin tooltip edit_directory" title="Edit"><img src="images/efit-text.png"></a>
                            <a href="<?php echo MEMBER_WWW.MEB_INDEX_PARAMETER.MEB_VIEW_BUSINESS.'&dir_id='.$result_query[$i][$SECTION_FIELD_PREFIX."id"]?>" class="tooltip" title="Manage"><img src="images/manage-icon.png"></a>
                            <div class="followers_div">Followers<br><span class="followers_span_total"><?php echo $totald; ?></span></div>
                        </div>
                    </div>
                </div>

	    <?php
            }
        } else { ?>
            <div class="no-record" style="margin-left: 10px;">No groups found</div>
        <?php }

        if($pages>1)
            echo "</div>";

        if($pages>1) { ?>
            <div class="pagination-main" style="display: none;">
        		<div class="pagination">
            		<ul>
                        <?php for($t=1; $t<=$pages; $t++) {
                            if($page==$t) {
                        ?>
                            <li class="active"><a class="avtive" href="javascript:void(0);" <?php if($pages > 1){?> onclick="getAjaxPaging('Directory','<?php echo $SECTION_TABLE; ?>','<?php echo $SECTION_FIELD_PREFIX; ?>','<?php echo $pages; ?>','<?php echo $t ?>','<?php echo mysql_real_escape_string($xtraCondition)."&orderby=".$orderby."&order=".$order; ?>','<?php echo $SECTION_MANAGE_PAGE;?>', '<?php echo $searchText; ?>');"<?php }?>><?php echo $t ?></a></li>
                        <?php } else { ?>
                            <li><a href="javascript:void(0);" <?php if($pages > 1){?>onclick="getAjaxPaging('Directory','<?php echo $SECTION_TABLE; ?>','<?php echo $SECTION_FIELD_PREFIX; ?>','<?php echo $pages; ?>','<?php echo $t ?>','<?php echo mysql_real_escape_string($xtraCondition)."&orderby=".$orderby."&order=".$order; ?>','<?php echo $SECTION_MANAGE_PAGE;?>', '<?php echo $searchText; ?>');"<?php }?>><?php echo $t ?></a></li>
                        <?php
                            }
                        } ?>
                        <?php
                        /*
                        $page = ($page == 0 ? 1 : $page);
                        $start = ($page-1) * $per_page;
                        $adjacents = "1";

                        $prev = $page - 1;
                        $next = $page + 1;
                        $lastpage = ceil($count/$per_page);
                        $lpm1 = $lastpage - 1;

                    if ($page > 1) { ?>
                        <li class="page-pre"><a href='javascript:void(0);' onclick="getAjaxPaging('Directory','<?php echo $SECTION_TABLE; ?>','<?php echo $SECTION_FIELD_PREFIX; ?>','<?php echo $pages; ?>','<?php echo $prev ?>','<?php echo mysql_real_escape_string($xtraCondition)."&orderby=".$orderby."&order=".$order; ?>','<?php echo $SECTION_MANAGE_PAGE;?>','<?php echo $searchText; ?>');"></a></li>
                    <?php } else { ?>
                        <li class="page-pre"><a href='javascript:void(0);' class='disabled' href="#"></a></li>
                    <?php }

                    if ($lastpage < 7 + ($adjacents * 2)){
                        for ($counter = 1; $counter <= $lastpage; $counter++){
                            if ($counter == $page) { ?>
                                <li><a class='avtive' href="javascript:void(0);"><?php echo $counter;?></a></li>
                        <?php } else { ?>
                                <li><a href='javascript:void(0);' onclick="getAjaxPaging('Directory','<?php echo $SECTION_TABLE; ?>','<?php echo $SECTION_FIELD_PREFIX; ?>','<?php echo $pages; ?>','<?php echo $counter ?>','<?php echo mysql_real_escape_string($xtraCondition)."&orderby=".$orderby."&order=".$order; ?>','<?php echo $SECTION_MANAGE_PAGE;?>','<?php echo $searchText; ?>');" ><?php echo $counter;?></a></li>
                        <?php }
                        }
                    }

                    elseif($lastpage > 5 + ($adjacents * 2)) {
                        if($page < 1 + ($adjacents * 2)){
                            for($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
                                if($counter == $page)
                                    echo "<li><a class='avtive' href='javascript:void(0);'>".$counter."</a></li>";
                                else
                                    echo "<li><a href='javascript:void(0);' onclick=\"getAjaxPaging('Directory','".$SECTION_TABLE."','".$SECTION_FIELD_PREFIX."','".$pages."','".$counter."','".mysql_real_escape_string($xtraCondition)."&orderby=".$orderby."&order=".$order."','".$SECTION_MANAGE_PAGE."','".$searchText."');\">".$counter."</a></li>";
                            }
                            echo "<li><a href='javascript:void(0);'>...</a></li>";
                            echo "<li><a href='javascript:void(0);' onclick=\"getAjaxPaging('Directory','".$SECTION_TABLE."','".$SECTION_FIELD_PREFIX."','".$pages."','".$lastpage."','".mysql_real_escape_string($xtraCondition)."&orderby=".$orderby."&order=".$order."','".$SECTION_MANAGE_PAGE."','".$searchText."');\">".$lastpage."</a></li>";

                        } elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
                            echo "<li><a href='javascript:void(0);' onclick=\"getAjaxPaging('Directory','".$SECTION_TABLE."','".$SECTION_FIELD_PREFIX."','".$pages."','1','".mysql_real_escape_string($xtraCondition)."&orderby=".$orderby."&order=".$order."','".$SECTION_MANAGE_PAGE."','".$searchText."');\">1</a></li>";
                            echo "<li><a href='javascript:void(0);'>...</a></li>";

                            for($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
                                if($counter == $page)
                                    echo "<li><a class='avtive'><span>".$counter."</span></a></li>";
                                else
                                    echo "<li><a href='javascript:void(0);' onclick=\"getAjaxPaging('Directory','".$SECTION_TABLE."','".$SECTION_FIELD_PREFIX."','".$pages."','".$counter."','".mysql_real_escape_string($xtraCondition)."&orderby=".$orderby."&order=".$order."','".$SECTION_MANAGE_PAGE."','".$searchText."');\">".$counter."</a></li>";
                            }
                            echo "<li><a href='javascript:void(0);'>...</a></li>";
                            echo "<li><a href='javascript:void(0);' onclick=\"getAjaxPaging('Directory','".$SECTION_TABLE."','".$SECTION_FIELD_PREFIX."','".$pages."','".$lastpage."','".mysql_real_escape_string($xtraCondition)."&orderby=".$orderby."&order=".$order."','".$SECTION_MANAGE_PAGE."','".$searchText."');\">".$lastpage."</a></li>";

                        } else {
                            echo "<li><a href='javascript:void(0);' onclick=\"getAjaxPaging('Directory','".$SECTION_TABLE."','".$SECTION_FIELD_PREFIX."','".$pages."','1','".mysql_real_escape_string($xtraCondition)."&orderby=".$orderby."&order=".$order."','".$SECTION_MANAGE_PAGE."','".$searchText."');\">1</a></li>";
                            echo "<li><a href='javascript:void(0);'>...</a></li>";
                            for($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
                                if($counter == $page)
                                    echo "<li><a class='avtive' href='javascript:void(0);'><span>".$counter."</span></a></li>";
                                else
                                    echo "<li><a href='javascript:void(0);' onclick=\"getAjaxPaging('Directory','".$SECTION_TABLE."','".$SECTION_FIELD_PREFIX."','".$pages."','".$counter."','".mysql_real_escape_string($xtraCondition)."&orderby=".$orderby."&order=".$order."','".$SECTION_MANAGE_PAGE."','".$searchText."');\">".$counter."</a></li>";
                            }
                        }
                    }

                    if($page < $counter - 1)
                        echo "<li class='page-next'><a href='javascript:void(0);' onclick=\"getAjaxPaging('Directory','".$SECTION_TABLE."','".$SECTION_FIELD_PREFIX."','".$pages."','".$next."','".mysql_real_escape_string($xtraCondition)."&orderby=".$orderby."&order=".$order."','".$SECTION_MANAGE_PAGE."','".$searchText."');\"></a>";
                    else
                        echo "<li class='page-next'><a href='javascript:void(0);'></a></li>";
                    */
                    ?>
            		</ul>            					  
            	</div>
            </div>
        <?php } ?>
<script type="text/javascript">
    $(document).ready(function() {
        $('.tooltip').tooltipster();
        $.fancybox.close();

        curr = '';
        curr = '<?php echo $page; ?>';

        total = '';
        total = '<?php echo $pages; ?>';

        $('#total_page').val(total);
        $('#current_page').val(curr);

        if(curr<='1') {
            infinite_f();
        }
    });

</script>

<?php  ?>
