<?php
	include_once('../../include/includeclass.php');
	$action_type			=	$_REQUEST['action_type'];
	$SECTION_TABLE			= 	$_REQUEST['tableName'];
	$SECTION_FIELD_PREFIX	=	$_REQUEST['fieldPrefix'];
	$SECTION_AUTO_ID 		=	$_REQUEST['autoID'];
	$SECTION_MANAGE_PAGE	=	$_REQUEST['managePage'];
	$SECTION_WHERE			=	$SECTION_FIELD_PREFIX."id='".$SECTION_AUTO_ID."'";
	$xtraCondition			=	stripslashes($_REQUEST['xtraCondition']);
	$SECTION_NAME			=	$_REQUEST['displayName'];
	$searchchar				=	$_REQUEST['searchchar'];
    $uploadFILEWWW          =   UPLOAD_WWW_MESSAGING_FILE;
	$SECTION                =   "Messaging";

    $search_text			=	$_REQUEST['search_text'];

	$meb_dirId = getMemberSessionDirId();
	//$meb_dirId = getMemeberAsDir();

	$total_language = count($result_language);
	#################################################################
	if($action_type=="sendMail") {
		$msgId = $_REQUEST['msg_id'];
		$message_where  = "msg_status = 'Active' AND msg_meb_id = '".$_SESSION['meb_id']."' and msg_id=".$msgId;
		$messageRes 	= $db->selectData(TBL_MEMBER_MESSAGING,$message_fields = "",$message_where,$extra="",2);	
		$message = $messageRes[0]['msg_message'];
		$type = $messageRes[0]['msg_type'];
		$resendfile = $messageRes[0]['msg_file'];
		if($type=="Draft") {
			$update_values['msg_type']	= "Published";
		}
		/*else
		{
			$update_values['msg_type']	= "Published";
		}*/
		$update_values['msg_updated_id']	= $_SESSION['meb_id'];
		$update_values['msg_updated_date']	= date('Y-m-d H:i:s');
		$GPDetail_result = $db->updateData(TBL_MEMBER_MESSAGING,$update_values,"msg_id=".$msgId);
		
		
		if($resendfile!="") {
			$msg_file = UPLOAD_DIR_MESSAGING_FILE.$msgId."/".$resendfile;
		} else {
			$msg_file = "";
		}
		$msg=sendMessage($messageRes[0]['msg_dir_id'],$message,$msg_file);
		exit;
	}
	if($action_type=="unpublish") {
        $msgId = $_REQUEST['msg_id'];
        $message_where  = "msg_status = 'Active' AND msg_meb_id = '".$_SESSION['meb_id']."' and msg_id=".$msgId;
        $messageRes 	= $db->selectData(TBL_MEMBER_MESSAGING,$message_fields = "",$message_where,$extra="",2);
        $message = $messageRes[0]['msg_message'];
        $type = $messageRes[0]['msg_type'];
        $resendfile = $messageRes[0]['msg_file'];
        if($type=="Published") {
            $update_values['msg_type']	= "Draft";
        }
        /*else
        {
            $update_values['msg_type']	= "Published";
        }*/
        $update_values['msg_updated_id']	= $_SESSION['meb_id'];
        $update_values['msg_updated_date']	= date('Y-m-d H:i:s');
        $GPDetail_result = $db->updateData(TBL_MEMBER_MESSAGING,$update_values,"msg_id=".$msgId);

        // delete all record for read 
        $sql_dle="DELETE FROM ".tbl_member_messaging_archive." WHERE arc_msg_is = '".$msgId."'";
   		$db->select($sql_dle);

        if($resendfile!="") {
            $msg_file = UPLOAD_DIR_MESSAGING_FILE.$msgId."/".$resendfile;
        } else {
            $msg_file = "";
        }
        //$msg=sendMessage($messageRes[0]['msg_dir_id'],$message,$msg_file);
        exit;
    }
	if($action_type ==  "delete") {
		$update_values[$SECTION_FIELD_PREFIX.'status']      = "Deleted";
		$update_values[$SECTION_FIELD_PREFIX . 'updated_id'] 	= $_SESSION['meb_id'];
        $update_values[$SECTION_FIELD_PREFIX . 'updated_date'] 	= date("Y-m-d H:i:s");
		$GPDetail_result = $db->updateData($SECTION_TABLE,$update_values,$SECTION_WHERE);
		$_SESSION['msg']  =   "Broadcast has been deleted successfully.";
	}
	
	else if($action_type  ==  "sorting") {
		$orderby  = $_REQUEST['orderby'];
		$order    = $_REQUEST['order'];
		if($order == "asc" || $order == "")
			$ORDER =  "desc";
		else
			$ORDER =  "asc";
	}
	if($orderby == "") {
		$ORDER =  "desc";
		$orderby = "id";
	}
	##################################  General Query ###############################################  
    $type = getMemberType();
	if($type!="Business") {
		$meb_dirId = getMemberSessionDirId();
		$sqlCondition = " AND msg_dir_id IN (".$meb_dirId.")";
  
        if ($search_text != 'undefined' && $search_text != "") {
            $ss_where = " (msg_company_title LIKE '%".$search_text."%' OR msg_message LIKE '%".$search_text."%') ";
            $sql_query = "SELECT p.* FROM " . $SECTION_TABLE . " p WHERE p." . $SECTION_FIELD_PREFIX . "id != 0 AND ".$ss_where." AND p.".$SECTION_FIELD_PREFIX."status!='Deleted' ".$sqlCondition." order by p." . $SECTION_FIELD_PREFIX . $orderby . " " . $ORDER;
        } else {
            $sql_query = "SELECT p.* FROM ".$SECTION_TABLE." as p WHERE p.".$SECTION_FIELD_PREFIX."id != 0 AND p.".$SECTION_FIELD_PREFIX."status != 'Deleted' ".$sqlCondition." order by p.".$SECTION_FIELD_PREFIX.$orderby." ".$ORDER;
        }
    } else {
        $subId = getFreeMemberSessionId();
		$sqlCondition = " AND p.msg_sub_id=".$subId;
        if ($search_text != 'undefined' && $search_text != "") {
            $ss_where = " (msg_company_title LIKE '%".$search_text."%' OR msg_message LIKE '%".$search_text."%') ";
            $sql_query = "SELECT p.* FROM " . $SECTION_TABLE . " p WHERE p." . $SECTION_FIELD_PREFIX . "id != 0 AND ".$ss_where." AND p.".$SECTION_FIELD_PREFIX."status!='Deleted' ".$sqlCondition." AND p.msg_dir_id IN (".$meb_dirId.") order by p." . $SECTION_FIELD_PREFIX . $orderby . " " . $ORDER;
        } else {
            $sql_query = "SELECT p.* FROM ".$SECTION_TABLE." as p WHERE p.".$SECTION_FIELD_PREFIX."id != 0 AND p.".$SECTION_FIELD_PREFIX."status != 'Deleted'".$sqlCondition." AND p.msg_dir_id IN (".$meb_dirId.") order by p.".$SECTION_FIELD_PREFIX.$orderby." ".$ORDER;
        }
    }
	
	#################################  Paging Query + Paging Code ##################################
	$paging_query = $sql_query;
	$paging_result  = $db->select($paging_query); 
	$count = count($paging_result);
	$per_page = SITE_PAGING_PER_PAGE; //rows per page
	$per_page = 8;
	$pages = ceil($count/$per_page);    
	#################################################################################################
	if($action_type == "paging")
	{
		if(!empty($_REQUEST['page']))
		{
			$page = $_REQUEST['page'];
		}       
	}
	else
	{
		if(!empty($_REQUEST['page']))
			$page = $_REQUEST['page'];
	  	else
		$page = 1;
	} 
	$list_query = $sql_query;
	if(!empty($per_page) && $_REQUEST['page']!="all")
	{
		$start = ($page-1)*$per_page;
		if($start<0)
		{
			$start=0;
		}
		$list_query .= " limit $start,$per_page"; 
	} 
	$result_query  = $db->select($list_query);  
	$total_rows = count($result_query);
	//echo $ms = ajaxMsg($_SESSION['msg']);
    if($page>1)
        echo '<div class="group-setion-row" id="updatediv">';

    if($total_rows > 0 ) { ?>
        <?php $j=1; for($i=0;$i<$total_rows;$i++) { ?>
            <?php
                $msg_file = '<img src="images/message-list-icon.png">';
                if($result_query[$i][$SECTION_FIELD_PREFIX.'file']!=''){
                    $filename = $result_query[$i][$SECTION_FIELD_PREFIX.'file'];
                    $fileNameExt = explode(".", $filename);
                    $fileExt 	 = $fileNameExt[count($fileNameExt)-1];
                    $allowedImgExts = array("gif","jpg","jpeg","png","GIF","JPG","JPEG","PNG");
                    if(in_array($fileExt,$allowedImgExts)) {
                        $msg_file = '<img src="'.$uploadFILEWWW . $result_query[$i][$SECTION_FIELD_PREFIX . 'id'] . '/' . $result_query[$i][$SECTION_FIELD_PREFIX . 'file'].'">';
                    } else {
                        $msg_file = '<img src="images/video_play.jpg">';
                    }
                }
            ?>
            <div class="group-setion-col gr_on_view_card123">

                <div class="group-setion-col-img group-setion-col-img-1"><?php echo $msg_file; ?></div>
                <div class="group-setion-col-h3"><?php echo $result_query[$i][$SECTION_FIELD_PREFIX.'company_title']; ?></div>
                <div class="group-setion-col-p-1"><?php echo charCut($result_query[$i][$SECTION_FIELD_PREFIX.'message'],"120","..."); ?></div>
                <div class="group-setion-col-hover">
                    <div class="hover-icon">
                        <a href="view_records.php?sectionName=Messaging&msg_id=<?php echo $result_query[$i][$SECTION_FIELD_PREFIX."id"];?>" class="fancybox fancybox.ajax tooltip hover-icon-margin" title="View" data-fancybox-type="ajax"><img src="images/view-icon.png"></a>
                        <?php if($result_query[$i][$SECTION_FIELD_PREFIX.'has_question']=="1") { ?>
                        <a href="<?php echo MEB_INDEX_PARAMETER.MEB_VIEW_RESPONSE.'&msg_id='.$result_query[$i][$SECTION_FIELD_PREFIX."id"];; ?>" class="tooltip hover-icon-margin" title="Survey Results"><img src="images/response_message-icon.png" /></a>
                        <?php } ?>
                        <?php if($result_query[$i][$SECTION_FIELD_PREFIX.'type']=="Published") { ?>
                            <a href="javascript:void(0);" class="tooltip hover-icon-margin" onclick="sendmail('Published','<?php echo $result_query[$i][$SECTION_FIELD_PREFIX."id"]; ?>')" title="Resend"><img src="images/send_message-icon.png" /></a>
                            <a href="javascript:void(0);" class="tooltip hover-icon-margin" onclick="sendmail2('unpublish','<?php echo $result_query[$i][$SECTION_FIELD_PREFIX."id"]; ?>')" title="Unpublish"><img src="images/unpublish-icon.png" /></a>
                        <?php } else { ?>
                            <a href="<?php echo getMemberURL($SECTION_MANAGE_PAGE,"Edit","msg_id=".$result_query[$i][$SECTION_FIELD_PREFIX."id"]); ?>" title="Edit" class="tooltip hover-icon-margin"><img src="images/efit-text.png"></a>
                            <a href="javascript:void(0);" class="tooltip" onclick="sendmail('Draft','<?php echo $result_query[$i][$SECTION_FIELD_PREFIX."id"]; ?>')" title="Publish"><img src="images/message-icon.png" /></a>
                        <?php } ?>

                    </div>
                </div>
            </div>

            <?php //echo $result_query[$i][$SECTION_FIELD_PREFIX.'type']; ?>
            <?php //echo date('m/d/Y',strtotime($result_query[$i][$SECTION_FIELD_PREFIX.'updated_date'])); ?>

        <?php }?>
    <?php } else {?>
        <div class="no-record" style="margin-left: 10px;">No Broadcast found.</div>
    <?php }
    if($pages>1)
        echo '</div>';
    ?>

<script type="text/javascript">
    $(document).ready(function() {
        $('.tooltip').tooltipster();
		$.fancybox.close();

        curr = '';
        curr = '<?php echo $page; ?>';

        total = '';
        total = '<?php echo $pages; ?>';

        $('#total_page').val(total);
        $('#current_page').val(curr);

        if(curr<='1') {
            infinite_f();
        }
    });
</script>
