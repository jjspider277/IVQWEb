<?php
	include_once('../../include/includeclass.php');
	$action_type			=	$_REQUEST['action_type'];
	$SECTION_TABLE			= 	$_REQUEST['tableName'];
	$SECTION_FIELD_PREFIX	=	$_REQUEST['fieldPrefix'];
	$SECTION_AUTO_ID 		=	$_REQUEST['autoID'];
	$SECTION_MANAGE_PAGE	=	$_REQUEST['managePage'];
	$SECTION_WHERE			=	$SECTION_FIELD_PREFIX."id='".$SECTION_AUTO_ID."'";
	$xtraCondition			=	stripslashes($_REQUEST['xtraCondition']);
	$SECTION_NAME			=	$_REQUEST['displayName'];
	$searchchar				=	$_REQUEST['searchchar'];

    $search_text			=	$_REQUEST['search_text'];

	/*echo "<pre>";
	print_r($_POST);
	echo "</pre>";
	exit;*/
	$SECTION="Coupons";
 
    $meb_dirId = getMemberSessionDirId();
    //$meb_dirId = getMemeberAsDir();

	$type = getMemberType();

	$total_language = count($result_language);
	#################################################################

	if($action_type ==  "delete") {
		$update_values[$SECTION_FIELD_PREFIX.'status']      	= "Deleted";
		$update_values[$SECTION_FIELD_PREFIX . 'updated_id'] 	= $_SESSION['meb_id'];
        $update_values[$SECTION_FIELD_PREFIX . 'updated_date'] 	= date("Y-m-d H:i:s");
		$GPDetail_result = $db->updateData($SECTION_TABLE,$update_values,$SECTION_WHERE);
		$_SESSION['msg']  =   "Subscribers has been deleted successfully.";  
	} else if($action_type  ==  "sorting") {
		$orderby  = $_REQUEST['orderby'];
		$order    = $_REQUEST['order'];
		if($order == "asc" || $order == "")
			$ORDER =  "desc";
		else
			$ORDER =  "asc";
	}

	if($orderby == "") {
		$ORDER =  "desc";
		$orderby = "id";
	}
	##################################  General Query ###############################################  
 
    $type = getMemberType();
	if($type!="Business") {
		$meb_dirId = getMemberSessionDirId();
		$sqlCondition = " AND cop_dir_id IN (".$meb_dirId.")";
  
        if ($search_text != 'undefined' && $search_text != "") {
            $ss_where = " (cop_company_title LIKE '%".$search_text."%' OR cop_title LIKE '%".$search_text."%' OR cop_coupon_code LIKE '%".$search_text."%') ";
            $sql_query = "SELECT p.* FROM " . $SECTION_TABLE . " p WHERE p." . $SECTION_FIELD_PREFIX . "id != 0 AND ".$ss_where." AND p.".$SECTION_FIELD_PREFIX."status!='Deleted' ".	$sqlCondition." order by p." . $SECTION_FIELD_PREFIX . $orderby . " " . $ORDER;
        } else if ($xtraCondition != 'undefined' && $xtraCondition != "") {
            //$ts = $xtraCondition;
            $sql_query = "select * from ".$SECTION_TABLE." as p where (" . $xtraCondition . ") AND (p.".$SECTION_FIELD_PREFIX."status!='Deleted' ".	$sqlCondition." order by " . $SECTION_FIELD_PREFIX . $orderby . " " . $ORDER;
        } else {
            $sql_query = "SELECT p.* FROM ".$SECTION_TABLE." as p WHERE p.".$SECTION_FIELD_PREFIX."id != 0 AND p.".$SECTION_FIELD_PREFIX."status != 'Deleted' ".	$sqlCondition." order by p.".$SECTION_FIELD_PREFIX.$orderby." ".$ORDER;
        }
    } else {
        $subId = getFreeMemberSessionId();
		$sqlCondition = " AND p.cop_sub_id=".$subId;
  
        if ($search_text != 'undefined' && $search_text != "") {
            $ss_where = " (cop_company_title LIKE '%".$search_text."%' OR cop_title LIKE '%".$search_text."%' OR cop_coupon_code LIKE '%".$search_text."%') ";
            $sql_query = "SELECT p.* FROM " . $SECTION_TABLE . " p WHERE p." . $SECTION_FIELD_PREFIX . "id != 0 AND ".$ss_where." AND p.".$SECTION_FIELD_PREFIX."status!='Deleted' ".	$sqlCondition." AND p.cop_dir_id IN (".$meb_dirId.") order by p." . $SECTION_FIELD_PREFIX . $orderby . " " . $ORDER;
        } else if ($xtraCondition != 'undefined' && $xtraCondition != "") {
            //$ts = $xtraCondition;
            $sql_query = "select * from ".$SECTION_TABLE." as p where (" . $xtraCondition . ") AND (p.".$SECTION_FIELD_PREFIX."status!='Deleted' ".	$sqlCondition." AND p.cop_dir_id IN (".$meb_dirId.")) order by " . $SECTION_FIELD_PREFIX . $orderby . " " . $ORDER;
        } else {
            $sql_query = "SELECT p.* FROM ".$SECTION_TABLE." as p WHERE p.".$SECTION_FIELD_PREFIX."id != 0 AND p.".$SECTION_FIELD_PREFIX."status != 'Deleted'".	$sqlCondition." AND p.cop_dir_id IN (".$meb_dirId.") order by p.".$SECTION_FIELD_PREFIX.$orderby." ".$ORDER;
        }
    }
 
	   
	#################################  Paging Query + Paging Code ##################################
	$paging_query = $sql_query;
	$paging_result  = $db->select($paging_query); 
	$count = count($paging_result);
	$per_page = SITE_PAGING_PER_PAGE; //rows per page
	$per_page = 8;
	$pages = ceil($count/$per_page);    
	#################################################################################################
	if($action_type == "paging") {
		if(!empty($_REQUEST['page'])) {
			$page = $_REQUEST['page'];
		}       
	} else {
		if(!empty($_REQUEST['page']))
			$page = $_REQUEST['page'];
	  	else
		$page = 1;
	} 
	$list_query = $sql_query;
	if(!empty($per_page) && $_REQUEST['page']!="all") {
		$start = ($page-1)*$per_page;
		if($start<0) {
			$start=0;
		}
		$list_query .= " limit $start,$per_page"; 
	} 
	$result_query  = $db->select($list_query);  
	$total_rows = count($result_query);
	//echo $ms = ajaxMsg($_SESSION['msg']);

    if($page>1)
        echo '<div class="group-setion-row" id="updatediv">';

        if($total_rows>0) {
			$j=1;
			for($i=0;$i<$total_rows;$i++) {
                $col_im = '<img src="images/coupons-profile-img.png">';

                if($result_query[$i][$SECTION_FIELD_PREFIX.'logo_file']!='') {
                    $filename = $result_query[$i][$SECTION_FIELD_PREFIX.'logo_file'];
                    $fileNameExt = explode(".", $filename);
                    $fileExt 	 = $fileNameExt[count($fileNameExt)-1];
                    $allowedImgExts = array("gif","jpg","jpeg","png","GIF","JPG","JPEG","PNG");
                    if(in_array($fileExt,$allowedImgExts)) {
                        $col_im = '<img src="'.UPLOAD_WWW_COUPONS_FILE . $result_query[$i][$SECTION_FIELD_PREFIX . 'id'] . '/' . $result_query[$i][$SECTION_FIELD_PREFIX . 'logo_file'].'">';
                    } else {
                        $col_im = '<img src="images/video_play.jpg">';
                    }
                }

	?>
                <div class="group-setion-col gr_on_view_card123">
                    <div class="group-setion-col-img group-setion-col-img-1"><?php echo $col_im; ?></div>
                    <div class="group-setion-col-green">Save <?php echo $result_query[$i][$SECTION_FIELD_PREFIX.'percentage']."%"; ?> Off</div>
                    <div class="coupons-normal-text"><?php echo $result_query[$i][$SECTION_FIELD_PREFIX.'title']; ?></div>
                    <div class="coupons-normal-blue-text">Expires <?php echo date('m/d/Y',strtotime($result_query[$i][$SECTION_FIELD_PREFIX.'valid_thru'])); ?></div>
                    <div class="group-setion-col-hover">
                        <div class="hover-icon">
                            <a href="view_records.php?sectionName=Coupons&cop_id=<?php echo $result_query[$i][$SECTION_FIELD_PREFIX."id"];?>" class="hover-icon-margin fancybox fancybox.ajax tooltip" title="View" data-fancybox-type="ajax"><img src="images/view-icon.png"></a>
                            <a href="<?php echo getMemberURL($SECTION_MANAGE_PAGE,"Edit","cop_id=".$result_query[$i][$SECTION_FIELD_PREFIX."id"]); ?>" class="tooltip hover-icon-margin" title="Edit"><img src="images/efit-text.png"></a>
                            <a href="javascript:void(0);" class="tooltip" title="Delete" onclick="javascript:alertBoxMember('<?php echo $SECTION; ?>','<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>',<?php echo $result_query[$i][$SECTION_FIELD_PREFIX."id"]; ?>,'','<?php echo $SECTION_MANAGE_PAGE;?>');"><img src="images/remove-icon.png"></a>
                        </div>
                    </div>
                </div>
                <?php if($type!="Business") { ?>
                <?php } ?>

        <?php } ?>

    <?php } else { ?>
	    <div class="no-record" style="margin-left: 10px;">No Records</div>
	<?php }

    if($pages>1)
        echo '</div>';
    ?>

<script type="text/javascript">
    $(document).ready(function() {
        $('.tooltip').tooltipster();
		$.fancybox.close();

        curr = '';
        curr = '<?php echo $page; ?>';

        total = '';
        total = '<?php echo $pages; ?>';

        $('#total_page').val(total);
        $('#current_page').val(curr);

        if(curr<='1') {
            infinite_f();
        }
    });
</script>