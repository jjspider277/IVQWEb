var ajax_value_file=ajax_folder+"GetAjaxValue.php?ajax_type=";

var cookie_file=ajax_value_file+"getCookie";

function requestCookieDetail(name,value,expiredays,filename)
{
	var url = cookie_file+"&cookie_name="+name+"&cookie_value="+value+"&cookie_expiredays="+expiredays+"&filename="+filename;
	/*
	var exdate=new Date();
	exdate.setDate(exdate.getDate()+expiredays);
	document.cookie=name+ "=" +escape(value)+((expiredays==null) ? "" : "; expires="+exdate.toGMTString());	*/
	
	http_cookie.open("GET", url , true);	
	http_cookie.onreadystatechange = handleCookieResponse;			
	http_cookie.send(null);	
}
function handleCookieResponse() 
{		
	
	if (http_cookie.readyState == 4) 
	{	
		if(http_cookie.status==200) 
		{
			var results=http_cookie.responseText;
			if(results!="")
			{	
				//window.location.href = results;
				window.location=results;
				//document.getElementById('file_url').innerHTML = results;
			}		
		}
  	}
}

function getHTTPObject() 
{
	var xmlhttp; 
	if(window.XMLHttpRequest)
	{
		xmlhttp = new XMLHttpRequest();
	}
	else if (window.ActiveXObject)
	{
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		if (!xmlhttp)
		{
			xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
		}
	}
	return xmlhttp;
}

function getRecords(sectioname,tablename,fields,where,extra,list_type,fieldprefix,pagename,actionbuttons){
		if(sectioname == "member"){
			var ajax_value_file=ajax_folder+"getMemberslist.php";
		}		
		$.ajax({		   
		   type: "POST",
		   url: ajax_value_file,
		   data: "tableName="+tablename+"&fields="+fields+"&where="+where+"&extra="+extra+"&list_type="+list_type+"&fieldprefix="+fieldprefix+"&pagename="+pagename+"&actionbuttons="+actionbuttons,
		   success: function(msg){
			if(msg != ""){
				 $("#recordListingDiv").html(msg);
			}
		   }	
		 });
}

function getAjaxDeleteActions(sectioname,tablename,fields,where,extra,list_type,fieldprefix,id,pagename){	
	//alert(sectioname+","+tablename+","+fields+","+where+","+extra+","+list_type+","+fieldprefix+","+id+","+pagename);
	if(!confirm('Are you sure want to delete?')){
		return false;
	}
	else{
		if(sectioname == "member"){
			var ajax_value_file=ajax_folder+"getMemberslist.php";	
		}
		
		$.ajax({		   
		   type: "POST",
		   url: ajax_value_file,
		   data: "action=delete"+"&tableName="+tablename+"&fields="+fields+"&where="+where+"&extra="+extra+"&list_type="+list_type+"&fieldprefix="+fieldprefix+"&id="+id+"&pagename="+pagename,
		   success: function(msg){
			if(msg != ""){
				 $("#recordListingDiv").html(msg);
				 $("#msgShow").html('Record deleted sucessfully');
			}
		   }	
		 });
	}}


function checkUserEmail(userEmail,usrId){	
	var ajax_value_file=ajax_folder+"checkEmail.php";	
	$.ajax({		   
		   type: "POST",
		   url: ajax_value_file,
		   data: "userEmail="+userEmail+"&usrId="+usrId,
		   success: function(msg){
			if(msg != ""){
				 $("#userEmailmsg").html(msg);				 
			}
		   }	
		 });}


function getAjaxSortings(sectioname,tableName,fieldprefix,orderby,order,xtraCondition)
{
	if(sectioname == "member"){
			var ajax_value_file=ajax_folder+"getMemberslist.php";	
	}

	$.ajax({		   
	   type: "POST",
	   url: ajax_value_file,
	   data: "action=sorting"+"&tableName="+tableName+"&fieldprefix="+fieldprefix+"&orderby="+orderby+"&order="+order+"&xtraCondition="+xtraCondition,
	   success: function(msg){
		if(msg != "")
		{
			$("#recordListingDiv").html(msg);
		//		$('#td-msg').html(msg);
			//	$('#status-cls').html('<img src="images/inactive.png" alt="Inactive" title="Inactive" border=0>');
		}
	   }	
	 });
}	
	
var http_cookie = getHTTPObject();