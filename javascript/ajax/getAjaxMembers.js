function getpagelisting(section,tableName,fieldPrefix,managePage,xtraCondition,search_text){

	var ajax_value_file;
	if(section=="Directory")
	{
    	ajax_value_file=ajax_folder+"getDirectorylist_New.php";
    }
	else if(section=="Contact")
    {
    	ajax_value_file=ajax_folder+"getAjaxContact.php"; 
    }
	else if(section=="Members")
	{
		ajax_value_file=ajax_folder+"getMemberslist.php"; 
	}
	else if(section=="Advertisement")
	{
		ajax_value_file=ajax_folder+"getAdvertisement.php"; 
	}
	else if(section=="Subscribers")
    {
    	ajax_value_file=ajax_folder+"getSubscribersList.php"; 
    }
    else if(section=="Services")
    {
    	ajax_value_file=ajax_folder+"getServicesList.php"; 
    }
    else if(section=="Business")
    {
    	ajax_value_file=ajax_folder+"getBusinessList.php"; 
    }
    else if(section=="Coupons")
    {
    	ajax_value_file=ajax_folder+"getCouponsList.php"; 
    }
    else if(section=="Messaging")
    {
    	ajax_value_file=ajax_folder+"getMessagingList.php"; 
    }
    else if(section=="Adm_business")
    {
    	ajax_value_file=ajax_folder+"getAdmBusinessList.php"; 
    }
    else if(section=="Individual")
    {
    	ajax_value_file=ajax_folder+"getIndividual.php"; 
    }
    else if(section=="BusinessSub")
    {
    	ajax_value_file=ajax_folder+"getBusinessSub.php"; 
    }
    $.ajax({           
		type: "POST",
		url: ajax_value_file,
		data: "tableName="+tableName+"&fieldPrefix="+fieldPrefix+"&managePage="+managePage+"&xtraCondition="+xtraCondition+'&search_text='+search_text,
		success: function(msg){
			if(msg != ""){
				if(section=="BusinessSub") {
					$("#updateBusSubDiv").html(msg);
				} else {
					$("#updatediv").html(msg);
				}
			}
		}    
	});
}
function getpagelistingNew(section,tableName,fieldPrefix,managePage,xtraCondition,search_text){

    var ajax_value_file;
    if(section=="Directory")
    {
        ajax_value_file=ajax_folder+"getDirectorylist_New.php";
    }
    else if(section=="Contact")
    {
        ajax_value_file=ajax_folder+"getAjaxContact.php";
    }
    else if(section=="Members")
    {
        ajax_value_file=ajax_folder+"getMemberslist.php";
    }
    else if(section=="Advertisement")
    {
        ajax_value_file=ajax_folder+"getAdvertisement.php";
    }
    else if(section=="Subscribers")
    {
        ajax_value_file=ajax_folder+"getSubscribersList.php";
    }
    else if(section=="Services")
    {
        ajax_value_file=ajax_folder+"getServicesList.php";
    }
    else if(section=="Business")
    {
        ajax_value_file=ajax_folder+"getBusinessList.php";
    }
    else if(section=="Coupons")
    {
        ajax_value_file=ajax_folder+"getCouponsList.php";
    }
    else if(section=="Messaging")
    {
        ajax_value_file=ajax_folder+"getMessagingList.php";
    }
    else if(section=="Adm_business")
    {
        ajax_value_file=ajax_folder+"getAdmBusinessList.php";
    }
    else if(section=="Individual")
    {
        ajax_value_file=ajax_folder+"getIndividual.php";
    }
    else if(section=="BusinessSub")
    {
        ajax_value_file=ajax_folder+"getBusinessSub.php";
    }
    else if(section=="Requests")
    {
        ajax_value_file=ajax_folder+"getRequests.php";
    }
    $.ajax({
        type: "POST",
        url: ajax_value_file,
        data: "tableName="+tableName+"&fieldPrefix="+fieldPrefix+"&managePage="+managePage+"&xtraCondition="+xtraCondition+'&search_text='+search_text,
        success: function(msg){
            if(msg != ""){
                if(section=="BusinessSub") {
                    $("#updateBusSubDiv").html(msg);
                } else {
                    $("#updatediv").html(msg);
                }
            }
        }
    });
}
function getAjaxStatusActiveAction(section,tableName,fieldPrefix,autoID,xtraCondition,managePage,page)
{
	var ajax_value_file;
	if(section=="Subscribers")
	{
    	ajax_value_file=ajax_folder+"getSubscribersList.php"; 
    }
    else if(section=="BusinessSub")
    {
    	ajax_value_file=ajax_folder+"getBusinessSub.php"; 
    }
    else if(section=="Requests")
    {
        ajax_value_file=ajax_folder+"getRequests.php";
    }
	$.ajax({		   
		type: "POST",
		url: ajax_value_file,
		data: "action_type=active"+"&tableName="+tableName+"&fieldPrefix="+fieldPrefix+"&autoID="+autoID+"&xtraCondition="+xtraCondition+"&managePage="+managePage+"&page="+page,
		success: function(msg){
			if(msg != "")
			{
				if(section=="BusinessSub") {
					$("#updateBusSubDiv").html(msg);
				} else {
					$("#updatediv").html(msg);
				}
			}
		}	
	});
}
function getAjaxStatusInactiveAction(section,tableName,fieldPrefix,autoID,xtraCondition,managePage,page)
{
	var ajax_value_file;
	if(section=="Subscribers")
	{
    	ajax_value_file=ajax_folder+"getSubscribersList.php"; 
    }
    else if(section=="BusinessSub")
    {
    	ajax_value_file=ajax_folder+"getBusinessSub.php"; 
    }else if(section=="Requests")
    {
        ajax_value_file=ajax_folder+"getRequests.php";
    }
	$.ajax({		   
		type: "POST",
		url: ajax_value_file,
		data: "action_type=inactive"+"&tableName="+tableName+"&fieldPrefix="+fieldPrefix+"&autoID="+autoID+"&xtraCondition="+xtraCondition+"&managePage="+managePage+"&page="+page,
		success: function(msg){
			if(msg != "")
			{
				if(section=="BusinessSub") {
					$("#updateBusSubDiv").html(msg);
				} else {
					$("#updatediv").html(msg);
				}
			}
		}	
	});
}
function getAjaxDeleteAction(section,tableName,fieldPrefix,autoID,xtraCondition,managePage)
{
	if($("#DelId_"+autoID).hasClass("disabled"))
	{
		//e.preventDefault();     
		alertBoxDelete();		
		//alert("You can not delete this records");
		return false;
	}
	else
	{
		var ajax_value_file;
		if(section=="Directory")
		{
		   	ajax_value_file=ajax_folder+"getDirectorylist_New.php";
		   }
		else if(section=="Contact")
		{
			ajax_value_file=ajax_folder+"getAjaxContact.php"; 
		}
		else if(section=="Members")
		{
			ajax_value_file=ajax_folder+"getMemberslist.php"; 
		}
		else if(section=="Subscribers")
		{
			ajax_value_file=ajax_folder+"getSubscribersList.php"; 
		}
		else if(section=="Services")
		{
		   	ajax_value_file=ajax_folder+"getServicesList.php"; 
		}
		else if(section=="Business")
		{
		  	ajax_value_file=ajax_folder+"getBusinessList.php"; 
		}
		else if(section=="Coupons")
	    {
	    	ajax_value_file=ajax_folder+"getCouponsList.php"; 
	    }
	    else if(section=="Messaging")
	    {
	    	ajax_value_file=ajax_folder+"getMessagingList.php"; 
	    }
	    else if(section=="Advertisement")
		{
			ajax_value_file=ajax_folder+"getAdvertisement.php"; 
		}
		else if(section=="Adm_business")
		{
			ajax_value_file=ajax_folder+"getAdmBusinessList.php"; 
		}
		else if(section=="Individual")
	    {
	    	ajax_value_file=ajax_folder+"getIndividual.php"; 
	    }
	    else if(section=="BusinessSub")
	    {
	    	ajax_value_file=ajax_folder+"getBusinessSub.php"; 
	    }
	    else if(section=="Requests")
	    {
	    	ajax_value_file=ajax_folder+"getRequests.php"; 
	    }
		$.ajax({		   
			type: "POST",
			url: ajax_value_file,
			data: "action_type=delete"+"&tableName="+tableName+"&fieldPrefix="+fieldPrefix+"&autoID="+autoID+"&xtraCondition="+xtraCondition+"&managePage="+managePage,
			success: function(msg){
				if(msg != "")
				{
					if(section=="BusinessSub") {
						$("#updateBusSubDiv").html(msg);
					} else {
						$("#updatediv").html(msg);
					}
				}
			}	
		});
	}
}
function sendmail(msgtype,msg_id)
{
	//alert(msgtype+"/"+msg_id);
	$.ajax({		   
		type: "POST",
		url: ajax_folder+"getMessagingList.php",
		data: "action_type=sendMail&msg_id="+msg_id,
		success: function(msg){
			var myDate=new Date();
			if(msgtype=="Published")
			{
				alertBoxMemberMessage("Broadcast was resent successfully at "+myDate.getFullYear()+"/"+(myDate.getMonth()+1)+"/"+myDate.getDate()+"  "+myDate.getHours()+':'+myDate.getMinutes());
			}
			else
			{
				alertify.success('Broadcast was Sent successfully.');
                getpagelisting('Messaging','tbl_member_messaging','msg_','manage_messaging','');
				//alertBoxMemberMessage("Broadcast was Sent successfully");
				/*var datasucc = getpagelisting('Messaging','tbl_member_messaging','msg_','','');
				$("#updatediv").html(datasucc);*/			
			}
		}	
	});
}
function sendmail2(msgtype,msg_id)
{
    //alert(msgtype+"/"+msg_id);
    $.ajax({
        type: "POST",
        url: ajax_folder+"getMessagingList.php",
        data: "action_type=unpublish&msg_id="+msg_id,
        success: function(msg){
            var myDate=new Date();
            if(msgtype=="unpublish")
            {
                //alertBoxMemberMessage("Broadcast was resent successfully at "+myDate.getFullYear()+"/"+(myDate.getMonth()+1)+"/"+myDate.getDate()+"  "+myDate.getHours()+':'+myDate.getMinutes());
                alertify.success('Broadcast has been unpublished successfully.');
                getpagelisting('Messaging','tbl_member_messaging','msg_','manage_messaging','');
            }
        }
    });
}
function getAjaxSorting(section,tableName,fieldPrefix,orderby,order,xtraCondition,managePage,page)
{
	var xtraCondition = encodeURIComponent(xtraCondition);
	var ajax_value_file;
	if(section=="Directory")
	{
    	ajax_value_file=ajax_folder+"getDirectorylist.php"; 
    }
	else if(section=="Contact")
    {
    	ajax_value_file=ajax_folder+"getAjaxContact.php"; 
    }
	else if(section=="Members")
	{
		ajax_value_file=ajax_folder+"getMemberslist.php"; 
	}
	else if(section=="Subscribers")
	{
		ajax_value_file=ajax_folder+"getSubscribersList.php"; 
	}
	else if(section=="Services")
    {
    	ajax_value_file=ajax_folder+"getServicesList.php"; 
    }
    else if(section=="Business")
    {
    	ajax_value_file=ajax_folder+"getBusinessList.php"; 
    }
    else if(section=="Coupons")
    {
    	ajax_value_file=ajax_folder+"getCouponsList.php"; 
    }
    else if(section=="Messaging")
    {
    	ajax_value_file=ajax_folder+"getMessagingList.php"; 
    }
    else if(section=="Advertisement")
	{
		ajax_value_file=ajax_folder+"getAdvertisement.php"; 
	}
	else if(section=="Adm_business")
	{
		ajax_value_file=ajax_folder+"getAdmBusinessList.php"; 
	}
	else if(section=="Individual")
    {
    	ajax_value_file=ajax_folder+"getIndividual.php"; 
    }
    else if(section=="BusinessSub")
    {
    	ajax_value_file=ajax_folder+"getBusinessSub.php"; 
    }
	$.ajax({		   
		type: "POST",
		url: ajax_value_file,
		data: "action_type=sorting"+"&tableName="+tableName+"&fieldPrefix="+fieldPrefix+"&orderby="+orderby+"&order="+order+"&xtraCondition="+xtraCondition+"&managePage="+managePage+"&page="+page,
		success: function(msg){
			if(msg != "")
			{
				if(section=="BusinessSub") {
					$("#updateBusSubDiv").html(msg);
				} else {
					$("#updatediv").html(msg);
				}
			}
		}	
	});
}

function getAjaxPaging(section,tableName,fieldPrefix,pages,page,xtraCondition,managePage,search_text)
{
	var ajax_value_file;
	if(section=="Directory")
	{
    	ajax_value_file=ajax_folder+"getDirectorylist_New.php";
    }
	else if(section=="Contact")
    {
    	ajax_value_file=ajax_folder+"getAjaxContact.php"; 
    }
	else if(section=="Members")
	{
		ajax_value_file=ajax_folder+"getMemberslist.php"; 
	}
	else if(section=="Subscribers")
    {
    	ajax_value_file=ajax_folder+"getSubscribersList.php"; 
    }
    else if(section=="Services")
    {
    	ajax_value_file=ajax_folder+"getServicesList.php"; 
    }
    else if(section=="Business")
    {
    	ajax_value_file=ajax_folder+"getBusinessList.php"; 
    }
    else if(section=="Coupons")
    {
    	ajax_value_file=ajax_folder+"getCouponsList.php"; 
    }
    else if(section=="Messaging")
    {
    	ajax_value_file=ajax_folder+"getMessagingList.php"; 
    }
    else if(section=="Advertisement")
	{
		ajax_value_file=ajax_folder+"getAdvertisement.php"; 
	}
	else if(section=="Adm_business")
	{
		ajax_value_file=ajax_folder+"getAdmBusinessList.php"; 
	}
	else if(section=="Individual")
    {
    	ajax_value_file=ajax_folder+"getIndividual.php"; 
    }
    else if(section=="BusinessSub")
    {
    	ajax_value_file=ajax_folder+"getBusinessSub.php"; 
    }
	//alert(xtraCondition);
	$.ajax({		   
		type: "POST",
		url: ajax_value_file,
		data: "action_type=paging"+"&tableName="+tableName+"&fieldPrefix="+fieldPrefix+"&pages="+pages+"&page="+page+"&xtraCondition="+xtraCondition+"&managePage="+managePage+'&search_text='+search_text,
		success: function(msg){
			if(msg != "")
			{
				if(section=="BusinessSub") {
					$("#updateBusSubDiv").append(msg);
				} else {
					$("#updatediv").append(msg);
				}
			}
		}	
	});
}

function getAjaxSearchByLetter(section,tableName,fieldPrefix,character,managePage)
{
    var ajax_value_file;
	if(section=="Directory")
	{
    	ajax_value_file=ajax_folder+"getDirectorylist.php"; 
    }
	else if(section=="Contact")
    {
    	ajax_value_file=ajax_folder+"getAjaxContact.php"; 
    }
	else if(section=="Members")
	{
		ajax_value_file=ajax_folder+"getMemberslist.php"; 
	}
	else if(section=="Subscribers")
	{
    	ajax_value_file=ajax_folder+"getSubscribersList.php"; 
    }
    else if(section=="Services")
    {
    	ajax_value_file=ajax_folder+"getServicesList.php"; 
    }
    else if(section=="Business")
    {
    	ajax_value_file=ajax_folder+"getBusinessList.php"; 
    }
    else if(section=="Coupons")
    {
    	ajax_value_file=ajax_folder+"getCouponsList.php"; 
    }
    else if(section=="Messaging")
    {
    	ajax_value_file=ajax_folder+"getMessagingList.php"; 
    }
    else if(section=="Advertisement")
	{
		ajax_value_file=ajax_folder+"getAdvertisement.php"; 
	}
	else if(section=="Adm_business")
	{
		ajax_value_file=ajax_folder+"getAdmBusinessList.php"; 
	}
	else if(section=="Individual")
    {
    	ajax_value_file=ajax_folder+"getIndividual.php"; 
    }
	$.ajax({		   
		type: "POST",
		url: ajax_value_file,
		data: "tableName="+tableName+"&fieldPrefix="+fieldPrefix+"&searchchar="+character+"&managePage="+managePage,
		success: function(msg){
			if(msg != "")
			{
				$("#updatediv").html(msg);
			}
		}	
	});
}

function getLiveSearch(section,tableName,fieldPrefix,xtraCondition,managePage)
{

	if(section=="Directory")
	{
		var xtraCondition = "";
		var categoryId 	= document.getElementById("dir_cat").value;
		var dirState 	= document.getElementById("dir_state").value;
		var dirCity 	= document.getElementById("dir_city").value.replace("'", "''");
		var dirZip 		= document.getElementById("dir_zip").value.replace("'", "''");
		var dirName 	= document.getElementById("dir_name").value.replace("'", "''");
		var dirPhone	= document.getElementById("dir_phone").value.replace("'", "''");
		var dirEmail 	= document.getElementById("dir_email").value.replace("'", "''");
		
		/*var dir_from_date 	= document.getElementById("dir_from_date").value;
		var dir_to_date 	= document.getElementById("dir_to_date").value;*/

		if(categoryId != "" && categoryId !='undefined')
		{
			xtraCondition = "p.dir_dic_id = '" + categoryId + "'";
		}
		/*if(dir_from_date!="" && dir_from_date!="undefined" && dir_to_date!="" && dir_to_date!="undefined")
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND "; 
			}		
			xtraCondition += "DATE(p.dir_created_date) between '" + dir_from_date + "' AND '"+dir_to_date+"'";
		}*/
		if(dirState != "" && dirState !='undefined')
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND "; 
			}		
			xtraCondition += "p.dir_sta_id = '" + dirState + "'";
		}
		
		if(dirCity != "" && dirCity !='undefined')
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND "; 
			}		
			xtraCondition += "p.dir_city LIKE '%" + dirCity + "%'";
		}
		
		if(dirZip != "" && dirZip !='undefined')
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND "; 
			}		
			xtraCondition += "p.dir_zip LIKE '%" + dirZip + "%'";
		}
		
		if(dirName != "" && dirName !='undefined')
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND "; 
			}		
			xtraCondition += "p.dir_name LIKE '%" + dirName + "%'";
		}
		
		if(dirPhone != "" && dirPhone !='undefined')
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND "; 
			}		
			xtraCondition += "p.dir_office_phone LIKE '%" + dirPhone + "%'";
		}
		
		if(dirEmail != "" && dirEmail !='undefined')
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND "; 
			}		
			xtraCondition += "p.dir_email LIKE '%" + dirEmail + "%'";
		}
	}
	else if(section=="Members" || section=="Adm_business")
	{
		var xtraCondition = "";
		var mebName 	= document.getElementById("meb_name").value.replace("'", "''");
		var mebUsername	= document.getElementById("meb_username").value.replace("'", "''");
		var mebPhone 	= document.getElementById("meb_phone").value.replace("'", "''");
		var categoryId 	= document.getElementById("dir_list").value.replace("'", "''");
		/*var meb_from_date 	= document.getElementById("meb_from_date").value;
		var meb_to_date 	= document.getElementById("meb_to_date").value;*/
		if(categoryId != "" && categoryId !='undefined')
		{
			xtraCondition = "p.meb_dir_id = '" + categoryId + "'";
		}
		/*if(meb_from_date!="" && meb_from_date!="undefined" && meb_to_date!="" && meb_to_date!="undefined")
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND "; 
			}		
			xtraCondition += "DATE(p.meb_created_date) between '" + meb_from_date + "' AND '"+meb_to_date+"'";
		}*/
		if(mebName != "" && mebName !='undefined')
		{	
			if(xtraCondition != "")
			{
				xtraCondition += " AND "; 
			}
			xtraCondition += "p.meb_name LIKE '%" + mebName + "%'";
		}
		
		if(mebUsername != "" && mebUsername !='undefined')
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND "; 
			}		
			xtraCondition += "p.meb_username LIKE '%" + mebUsername + "%'";
		}
		
		if(mebPhone != "" && mebPhone !='undefined')
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND "; 
			}		
			xtraCondition += "p.meb_phone LIKE '%" + mebPhone + "%'";
		}
	}
	else if(section=="Advertisement")
	{
		var xtraCondition = "";
		var adv_dir_id 	= document.getElementById("adv_dir_id").value;
		var adv_title	= document.getElementById("adv_title").value.replace("'", "''");
		var adv_description 	= document.getElementById("adv_description").value.replace("'", "''");
		/*var adv_from_date 	= document.getElementById("adv_from_date").value;
		var adv_to_date	= document.getElementById("adv_to_date").value;*/
		
		if(adv_dir_id != "" && adv_dir_id !='undefined')
		{
			xtraCondition = "p.adv_dir_id = '" + adv_dir_id + "'";
		}
		/*if(adv_from_date!="" && adv_from_date!="undefined" && adv_to_date!="" && adv_to_date!="undefined")
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND "; 
			}		
			xtraCondition += "DATE(p.adv_created_date) between '" + adv_from_date + "' AND '"+adv_to_date+"'";
		}*/
		if(adv_title != "" && adv_title !='undefined')
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND "; 
			}		
			xtraCondition += "p.adv_title LIKE '%" + adv_title + "%'";
		}
		if(adv_description != "" && adv_description !='undefined')
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND "; 
			}		
			xtraCondition += "p.adv_description LIKE '%" + adv_description + "%'";
		}
	}
	else if(section=="Subscribers")
	{
		var xtraCondition = "";
		var sub_title 	= document.getElementById("sub_title").value.replace("'", "''");
		var sub_company	= document.getElementById("sub_company").value.replace("'", "''");
		var sub_name 	= document.getElementById("sub_name").value.replace("'", "''");
		var sub_phone 	= document.getElementById("sub_phone").value.replace("'", "''");
		var sub_zip 	= document.getElementById("sub_zip").value.replace("'", "''");
		var sub_city 	= document.getElementById("sub_city").value.replace("'", "''");
		var sub_state	= document.getElementById("sub_state").value;
		var sub_email 	= document.getElementById("sub_email").value.replace("'", "''");
		var sub_status 	= document.getElementById("sub_status").value;
		/*if(sub_status[0].checked)
			var status = sub_status[0].value;
		else
			var status = sub_status[1].value;*/	
		//var inactive 	= document.getElementById("inactive").value;
		/*var sub_from_date	= document.getElementById("sub_from_date").value;
		var sub_to_date 	= document.getElementById("sub_to_date").value;*/
		
		if(sub_title != "" && sub_title !='undefined')
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND "; 
			}		
			xtraCondition += "p.sub_title LIKE '%" + sub_title + "%'";
		}
		if(sub_company != "" && sub_company !='undefined')
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND "; 
			}		
			xtraCondition += "p.sub_company LIKE '%" + sub_company + "%'";
		}
		if(sub_name != "" && sub_name !='undefined')
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND "; 
			}		
			xtraCondition += "p.sub_name LIKE '%" + sub_name + "%'";
		}
		if(sub_status != "" && sub_status !='undefined')
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND "; 
			}		
			xtraCondition += "p.sub_status ='" + sub_status + "'";
		}
		/*if(sub_from_date!="" && sub_from_date!="undefined" && sub_to_date!="" && sub_to_date!="undefined")
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND "; 
			}		
			xtraCondition += "DATE(p.sub_created_date) between '" + sub_from_date + "' AND '"+sub_to_date+"'";
		}*/
		if(sub_phone != "" && sub_phone !='undefined')
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND "; 
			}		
			xtraCondition += "p.sub_phone LIKE '%" + sub_phone + "%'";
		}
		if(sub_zip != "" && sub_zip !='undefined')
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND "; 
			}		
			xtraCondition += "p.sub_zip LIKE '%" + sub_zip + "%'";
		}
		if(sub_city != "" && sub_city !='undefined')
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND "; 
			}		
			xtraCondition += "p.sub_city LIKE '%" + sub_city + "%'";
		}
		
		if(sub_state != "" && sub_state !='undefined')
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND "; 
			}		
			xtraCondition += "p.sub_sta_id = '" + sub_state + "'";
		}
		
		if(sub_email != "" && sub_email !='undefined')
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND "; 
			}		
			xtraCondition += "p.sub_email LIKE '%" + sub_email + "%'";
		}
	}
	else if(section=="Services")
	{
		var xtraCondition = "";
		var ser_title 	= document.getElementById("ser_title").value.replace("'", "''");
		var ser_description 	= document.getElementById("ser_description").value.replace("'", "''");
		/*var ser_from_date 	= document.getElementById("ser_from_date").value;
		var ser_to_date 	= document.getElementById("ser_to_date").value;*/
		
		if(ser_title != "" && ser_title !='undefined')
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND "; 
			}		
			xtraCondition += "p.ser_title LIKE '%" + ser_title + "%'";
		}
		/*if(ser_from_date!="" && ser_from_date!="undefined" && ser_to_date!="" && ser_to_date!="undefined")
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND "; 
			}		
			xtraCondition += "DATE(p.ser_created_date) between '" + ser_from_date + "' AND '"+ser_to_date+"'";
		}*/
		if(ser_description != "" && ser_description !='undefined')
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND "; 
			}		
			xtraCondition += "p.ser_description LIKE '%" + ser_description + "%'";
		}
	}	
	else if(section=="Business")
	{
		var xtraCondition = "";
		var bus_sta_id 	= document.getElementById("bus_sta_id").value;
		var bus_title 	= document.getElementById("bus_title").value.replace("'", "''");
		var bus_name 	= document.getElementById("bus_name").value.replace("'", "''");
		var bus_phone 	= document.getElementById("bus_phone").value.replace("'", "''");
		var bus_zip 	= document.getElementById("bus_zip").value.replace("'", "''");
		var bus_city	= document.getElementById("bus_city").value.replace("'", "''");
		var bus_email 	= document.getElementById("bus_email").value.replace("'", "''");
		var bus_website 		= document.getElementById("bus_website").value.replace("'", "''");
		var bus_fax 	= document.getElementById("bus_fax").value.replace("'", "''");
		/*var bus_from_date	= document.getElementById("bus_from_date").value;
		var bus_to_date 	= document.getElementById("bus_to_date").value;*/

		if(bus_title != "" && bus_title !='undefined')
		{
			xtraCondition = "p.bus_title LIKE '%" + bus_title + "%'";
		}
		/*if(bus_from_date!="" && bus_from_date!="undefined" && bus_to_date!="" && bus_to_date!="undefined")
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND ";
			}
			xtraCondition += "DATE(p.bus_created_date) between '" + bus_from_date + "' AND '"+bus_to_date+"'";
		}*/
		if(bus_sta_id != "" && bus_sta_id !='undefined')
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND ";
			}
			xtraCondition += "p.bus_sta_id = '" + bus_sta_id + "'";
		}
		if(bus_name != "" && bus_name !='undefined')
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND ";
			}
			xtraCondition += "p.bus_name LIKE '%" + bus_name + "%'";
		}
		if(bus_phone != "" && bus_phone !='undefined')
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND ";
			}
			xtraCondition += "p.bus_phone LIKE '%" + bus_phone + "%'";
		}
		if(bus_zip != "" && bus_zip !='undefined')
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND ";
			}
			xtraCondition += "p.bus_zip LIKE '%" + bus_zip + "%'";
		}
		if(bus_city != "" && bus_city !='undefined')
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND ";
			}
			xtraCondition += "p.bus_city LIKE '%" + bus_city + "%'";
		}

		if(bus_email != "" && bus_email !='undefined')
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND ";
			}
			xtraCondition += "p.bus_email LIKE '%" + bus_email + "%'";
		}

		if(bus_website != "" && bus_website !='undefined')
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND ";
			}
			xtraCondition += "p.bus_website LIKE '%" + bus_website + "%'";
		}
		if(bus_fax != "" && bus_fax !='undefined')
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND ";
			}
			xtraCondition += "p.bus_fax LIKE '%" + bus_fax + "%'";
		}

	}
	else if(section=="Messaging")
	{
		var xtraCondition = "";
		var msg_from_date	= document.getElementById("msg_from_date").value;
		var msg_to_date 	= document.getElementById("msg_to_date").value;
		//alert(msg_from_date);
		if(msg_from_date!="" && msg_from_date!="undefined")
		{
			var dateform = msg_from_date.split("/");
			var fmm=dateform[0];
			var fdd=dateform[1];
			var fyy=dateform[2];
			var fromDate = fyy+"-"+fmm+"-"+fdd;
		}
		if(msg_to_date!="" && msg_to_date!="undefined")
		{
			var dateto = msg_to_date.split("/");
			var tmm=dateto[0];
			var tdd=dateto[1];
			var tyy=dateto[2];
			var toDate = tyy+"-"+tmm+"-"+tdd;
		}
		if(msg_from_date!="" && msg_from_date!="undefined" && msg_to_date!="" && msg_to_date!="undefined")
		{
			xtraCondition = "DATE(p.msg_created_date) between '" + fromDate + "' AND '"+toDate+"'";
		}
		else if(msg_from_date!="" && msg_from_date!="undefined")
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND "; 
			}
			xtraCondition += "DATE(p.msg_created_date) = '" + fromDate + "'";
		}
		else if(msg_to_date!="" && msg_to_date!="undefined")
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND "; 
			}
			xtraCondition += "DATE(p.msg_created_date) = '" + toDate + "'";
		}
	}
	else if(section=="Coupons")
	{
		var xtraCondition = "";
		var cop_title 		= document.getElementById("cop_title").value.replace("'", "''");
		var cop_percentage 	= document.getElementById("cop_percentage").value.replace("'", "''");
		var cop_description = document.getElementById("cop_description").value.replace("'", "''");
		var cop_coupon_code	= document.getElementById("cop_coupon_code").value.replace("'", "''");
		/*var cop_barcode 	= document.getElementById("cop_barcode").value.replace("'", "''");*/
		/*var cop_from_date	= document.getElementById("cop_from_date").value;
		var cop_to_date 	= document.getElementById("cop_to_date").value;*/

		var cop_valid_from	= document.getElementById("cop_valid_from").value;
		var cop_valid_thru 	= document.getElementById("cop_valid_thru").value;
		/*if(cop_from_date!="" && cop_from_date!="undefined")
		{
			var dateform = cop_from_date.split("/");
			var fmm=dateform[0];
			var fdd=dateform[1];
			var fyy=dateform[2];
			var fromDate = fyy+"-"+fmm+"-"+fdd;
		}
		if(cop_to_date!="" && cop_to_date!="undefined")
		{
			var dateto = cop_to_date.split("/");
			var tmm=dateto[0];
			var tdd=dateto[1];
			var tyy=dateto[2];
			var toDate = tyy+"-"+tmm+"-"+tdd;
		}*/
		if(cop_valid_from!="" && cop_valid_from!="undefined")
		{
			var datecopform = cop_valid_from.split("/");
			var fcopmm=datecopform[0];
			var fcopdd=datecopform[1];
			var fcopyy=datecopform[2];
			var fromcopDate = fcopyy+"-"+fcopmm+"-"+fcopdd;
		}
		if(cop_valid_thru!="" && cop_valid_thru!="undefined")
		{
			var datecopto = cop_valid_thru.split("/");
			var tcopmm=datecopto[0];
			var tcopdd=datecopto[1];
			var tcopyy=datecopto[2];
			var tocopDate = tcopyy+"-"+tcopmm+"-"+tcopdd;
		}
		if(cop_title != "" && cop_title !='undefined')
		{
					
			xtraCondition = "p.cop_title LIKE '%" + cop_title + "%'";
		}
		/*if(cop_from_date!="" && cop_from_date!="undefined" && cop_to_date!="" && cop_to_date!="undefined")
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND "; 
			}		
			xtraCondition += "DATE(p.cop_created_date) between '" + fromDate + "' AND '"+toDate+"'";
		}
		else if(cop_from_date!="" && cop_from_date!="undefined")
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND "; 
			}
			xtraCondition += "DATE(p.cop_created_date) = '" + fromDate + "'";
		}
		else if(cop_to_date!="" && cop_to_date!="undefined")
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND "; 
			}
			xtraCondition += "DATE(p.cop_created_date) = '" + toDate + "'";
		}*/
		if(cop_percentage != "" && cop_percentage !='undefined')
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND "; 
			}		
			xtraCondition += "p.cop_percentage LIKE '%" + cop_percentage + "%'";
		}
		
		if(cop_description != "" && cop_description !='undefined')
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND "; 
			}		
			xtraCondition += "p.cop_description LIKE '%" + cop_description + "%'";
		}
		
		/*if(cop_barcode != "" && cop_barcode !='undefined')
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND "; 
			}		
			xtraCondition += "p.cop_barcode LIKE '%" + cop_barcode + "%'";
		}*/
		
		if(cop_coupon_code != "" && cop_coupon_code !='undefined')
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND "; 
			}		
			xtraCondition += "p.cop_coupon_code LIKE '%" + cop_coupon_code + "%'";
		}

		if(cop_valid_from!="" && cop_valid_from!="undefined" && cop_valid_thru!="" && cop_valid_thru!="undefined")
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND "; 
			}
			xtraCondition += "DATE(p.cop_valid_from) >= '" + fromcopDate + "' AND DATE(p.cop_valid_thru) <= '"+tocopDate+"'";
		}
		else if(cop_valid_from!="" && cop_valid_from!="undefined")
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND "; 
			}
			xtraCondition += "DATE(p.cop_valid_from) = '" + fromcopDate + "'";
		}
		else if(cop_valid_thru!="" && cop_valid_thru!="undefined")
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND "; 
			}
			xtraCondition += "DATE(p.cop_valid_thru) = '" + tocopDate + "'";
		}
	}
	else if(section=="Individual")
	{
		var xtraCondition = "";
		var inv_email	= document.getElementById("inv_email").value;
		if(inv_email != "" && inv_email !='undefined')
		{
			if(xtraCondition != "")
			{
				xtraCondition += " AND "; 
			}		
			xtraCondition += "p.inv_email LIKE '%" + inv_email + "%'";
		}
	}
	var g=encodeURIComponent(xtraCondition);
	var ajax_value_file;
	if(section=="Directory")
	{
		ajax_value_file=ajax_folder+"getDirectorylist.php"; 
	}
	else if(section=="Members")
	{
		ajax_value_file=ajax_folder+"getMemberslist.php"; 
	}
	else if(section=="Subscribers")
	{
    	ajax_value_file=ajax_folder+"getSubscribersList.php"; 
    }
    else if(section=="Services")
	{
		ajax_value_file=ajax_folder+"getServicesList.php"; 
	}
	else if(section=="Business")
	{
		ajax_value_file=ajax_folder+"getBusinessList.php"; 
	}
	else if(section=="Messaging")
	{
		ajax_value_file=ajax_folder+"getMessagingList.php"; 
	}
	else if(section=="Coupons")
    {
    	ajax_value_file=ajax_folder+"getCouponsList.php"; 
    }
    else if(section=="Advertisement")
	{
		ajax_value_file=ajax_folder+"getAdvertisement.php"; 
	}
	else if(section=="Adm_business")
	{
		ajax_value_file=ajax_folder+"getAdmBusinessList.php"; 
	}
	else if(section=="Individual")
    {
    	ajax_value_file=ajax_folder+"getIndividual.php"; 
    }
	$.ajax({		   
		type: "POST",
		url: ajax_value_file,
		data: "tableName="+tableName+"&fieldPrefix="+fieldPrefix+"&xtraCondition="+g+"&managePage="+managePage,
		success: function(msg){
			if(msg != "")
			{
				$("#updatediv").html(msg);
			}
		}	
	});
}

function checkUserEmail(userEmail,usrId,section){	
	var ajax_value_file=ajax_folder+"checkEmail.php";	
	$.ajax({		   
		   type: "POST",
		   url: ajax_value_file,
		   data: "userEmail="+userEmail+"&usrId="+usrId+"&section="+section,
		   success: function(msg){
			if(msg != ""){
				if(section=="Individual")
				{
					$("#invEmailmsg").html('<label class="error">'+msg+'</label>');
				}
				else
				{
					$("#userEmailmsg").html('<label class="error">'+msg+'</label>');
				}
			}
		   }	
		 });
}

function alertBox (section,tableName,fieldPrefix,autoID,xtraCondition,managePage) {
  $.fancybox('<div class="alert-box"><div class="error-title"><img src="../images/error-icon.png" alt="" ><span>Warning</span><a title="Close" class="fancybox-item fancybox-close close" href="javascript:;"></a></div><div class="error-txt">Are you sure want to delete?</div><div class="button-div"><a href="javascript:void(0);" onclick="getAjaxDeleteAction(\''+section+'\',\''+tableName+'\',\''+fieldPrefix+'\',\''+autoID+'\',\''+xtraCondition+'\',\''+managePage+'\');">Ok</a><a href="javascript:void(0);" class="fancycancel">Cancel</a></div><div class="clr"></div></div>');
}
function alertBoxMember(section,tableName,fieldPrefix,autoID,xtraCondition,managePage) {
  $.fancybox('<div class="alert-box"><div class="error-title"><img src="../images/error-icon.png" alt="" ><span>Warning</span><a title="Close" class="fancybox-item fancybox-close close" href="javascript:;"></a></div><div class="error-txt">Are you sure want to delete?</div><div class="button-div"><a href="javascript:void(0);" onclick="getAjaxDeleteAction(\''+section+'\',\''+tableName+'\',\''+fieldPrefix+'\',\''+autoID+'\',\''+xtraCondition+'\',\''+managePage+'\');">Ok</a><a href="javascript:void(0);" class="fancycancel">Cancel</a></div><div class="clr"></div></div>');
}
function alertBoxRequest(section,tableName,fieldPrefix,autoID,xtraCondition,managePage) {
    $.fancybox('<div class="alert-box"><div class="error-title"><img src="../images/error-icon.png" alt="" ><span>Warning</span><a title="Close" class="fancybox-item fancybox-close close" href="javascript:;"></a></div><div class="error-txt">Are you sure want to denied this request?</div><div class="button-div"><a href="javascript:void(0);" onclick="getAjaxDeleteAction(\''+section+'\',\''+tableName+'\',\''+fieldPrefix+'\',\''+autoID+'\',\''+xtraCondition+'\',\''+managePage+'\');">Ok</a><a href="javascript:void(0);" class="fancycancel">Cancel</a></div><div class="clr"></div></div>');
}
function alertBoxDelete() {
  $.fancybox('<div class="alert-box"><div class="error-title"><img src="../images/error-icon.png" alt="" ><span>Warning</span><a title="Close" class="fancybox-item fancybox-close close" href="javascript:;"></a></div><div class="error-txt">You can not delete this record</div><div class="button-div"><a href="javascript:void(0);" class="fancycancel">Ok</a><a href="javascript:void(0);" class="fancycancel">Cancel</a></div><div class="clr"></div></div>');
}
function alertBoxMemberMessage(message) {
  $.fancybox('<div class="alert-box"><div class="error-title"><img src="../images/error-icon.png" alt="" ><span>Broadcast</span><a title="Close" class="fancybox-item fancybox-close close" href="javascript:;"></a></div><div class="error-txt">'+message+'</div><div class="button-div"><a href="javascript:void(0);" class="fancycancel" onclick="'+"getpagelisting('Messaging','tbl_member_messaging','msg_','manage_messaging','');"+'">Ok</a></div><div class="clr"></div></div>');
  
}
function resetForm(formId,section,tableName,fieldPrefix,managePage,xtraCondition)
{
	$('#'+formId).each(function(){
		this.reset();
		/*$("#sub_state").selectedIndex = 0;
		$(".sbHolder").find("ul li a").removeClass("sbSelected");
		$(".sbHolder").find("ul li:first").find("a").addClass("sbSelected");*/
	});
	if(section=="Subscribers")
	{
    	var myvalue = $("select[name=sub_state]").val(); 
		var option = $("select[name=sub_state]").find("option[value="+myvalue+"]");
		$("select[name=sub_state]").selectbox("change", option.attr('value'), "Select State");
    }
	else if(section=="Business")
	{
		var myvalue = $("select[name=bus_sta_id]").val(); 
		var option = $("select[name=bus_sta_id]").find("option[value="+myvalue+"]");
		$("select[name=bus_sta_id]").selectbox("change", option.attr('value'), "Select State");
	}
	else if(section=="Members")
	{
		var myvalue = $("select[name=dir_list]").val(); 
		var option = $("select[name=dir_list]").find("option[value="+myvalue+"]");
		$("select[name=dir_list]").selectbox("change", option.attr('value'), "Select Directory");
	}
	else if(section=="Directory")
	{
		var myvalue = $("select[name=dir_cat]").val(); 
		var option = $("select[name=dir_cat]").find("option[value="+myvalue+"]");
		$("select[name=dir_cat]").selectbox("change", option.attr('value'), "Select Category");

		var stavalue = $("select[name=dir_state]").val(); 
		var option = $("select[name=dir_state]").find("option[value="+stavalue+"]");
		$("select[name=dir_state]").selectbox("change", option.attr('value'), "Select State");
	}
	else if(section=="Advertisement")
	{
		var myvalue = $("select[name=adv_meb_id]").val(); 
		var option = $("select[name=adv_meb_id]").find("option[value="+myvalue+"]");
		$("select[name=adv_meb_id]").selectbox("change", option.attr('value'), "Select Members");
	}
	else if(section=="Messaging")
	{	
		$( "#msg_from_date" ).datepicker( "option", "maxDate", null);
		$( "#msg_to_date" ).datepicker( "option", "minDate", null);
	}
	else if(section=="Coupons")
	{	
		$( "#cop_valid_from" ).datepicker( "option", "maxDate", null);
		$( "#cop_valid_thru" ).datepicker( "option", "minDate", null);
		$( "#cop_from_date" ).datepicker( "option", "maxDate", null);
		$( "#cop_to_date" ).datepicker( "option", "minDate", null);
	}
	getpagelisting(section,tableName,fieldPrefix,managePage,xtraCondition);
}