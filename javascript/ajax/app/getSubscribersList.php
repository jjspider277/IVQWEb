<?php
	error_reporting(1);
	header('Access-Control-Allow-Origin: *');
	
	include_once('../../../include/includeclass.php');
	$success = array();
	$action = $_POST['action'];
	
	if($action == 'CheckSubscriber') {
		
		$sub_email 					= 	$_POST['sub_id'];
		$sub_phone 					= 	$_POST['sub_phone'];
		
		$SECTION						=	"Subscribers";
		$SECTION_TABLE 			= 	TBL_MEMBER_SUBSCRIBERS;
		$SECTION_FIELD_PREFIX	=	'sub_';

		$sql_query = "select * from ".$SECTION_TABLE." as p where (p.".$SECTION_FIELD_PREFIX."status!='Deleted' and p.".$SECTION_FIELD_PREFIX."email='".$sub_email."' AND p.".$SECTION_FIELD_PREFIX."phone='".$sub_phone."')";		

		$meb_fields = array("*");
		$meb_where = $SECTION_FIELD_PREFIX."status!='Deleted' AND ".$SECTION_FIELD_PREFIX."type='Individual' AND ".$SECTION_FIELD_PREFIX ."email='".$sub_email."' AND ".$SECTION_FIELD_PREFIX."phone='".$sub_phone."'";
		$mebRes = $db->selectData($SECTION_TABLE,$meb_fields,$meb_where,$extra="",2);			
		
		if(count($mebRes) > 0) {
			if($mebRes[0]['sub_status'] == 'Active') {
				$ind_id = $mebRes[0]['sub_id'];
				$success = array("status" => 'true', 'data' => $dir_data, 'ind_id' => $ind_id);
			} else {
				$success = array("status" => 'false', 'data' => 'Status Inactive');
			}
		} else {

			$s_fields = array("*");
			$s_where  = "(shr_rcv_email = '".$sub_email."' OR shr_rcv_email = '".$sub_phone."') AND shr_status = 'Active'";
			$sRes	= $db->selectData(TBL_SHARE,$s_fields,$s_where,$extra="",2);				

			if(count($sRes) > 0) {

				$sub_fields = array("*");
				$sub_where  = "(sub_email = '".$sub_email."' OR sub_phone = '".$sub_phone."') AND sub_status = 'Inactive' AND sub_type = 'Individual'";
				$subRes 	= $db->selectData(TBL_MEMBER_SUBSCRIBERS,$sub_fields,$sub_where,$extra="",2);	

				if(count($subRes)>0){
					$sub_id = $subRes[0]['sub_id'];
					$dir_data = $subRes[0]['sub_dir_id'];
					
					$subs_where = 'sub_id="'.$sub_id.'"';
					
					$add_values["sub_status"]		=	"Active";
					if($subRes[0]['sub_email'] == '')
						$add_values["sub_email"] =	$sub_email;
					else
						$add_values["sub_phone"] =	$sub_phone;
						
					$SECTION_WHERE=$subs_where;
        			$GPDetail_result = $db->updateData(TBL_MEMBER_SUBSCRIBERS, $add_values, $SECTION_WHERE); 					}				
				if($sub_id != '')
					$success = array("status" => 'true', 'data' => $dir_data, 'ind_id' => $sub_id);
				else 
					$success = array("status" => 'false', 'data' => 'Invalid user name or phone number.');			
			} else {
				$success = array("status" => 'false', 'data' => 'Invalid user name or phone number.');
			}
			
		} 
		
	} else if($action == 'Subscrib') {
		
		$sub_email 					= 	$_POST['email'];
		$SubcriberId 				= 	$_POST['SubcriberId'];
		$dir_id						=	$_POST['dir_id'];
		$subscibe 					= 	$_POST['subscibe'];

		$SECTION						=	"Subscribers";
		$SECTION_TABLE 			= 	TBL_MEMBER_SUBSCRIBERS;
		$SECTION_TABLE_DIR 		= 	TBL_SUBSCRIBER_INV;
		
		$meb_fields = array("*");
		$meb_where = "sub_status!='Deleted' AND sub_id='".$SubcriberId."'";
		$mebRes = $db->selectData($SECTION_TABLE,$meb_fields,$meb_where,$extra="",2);
		
		if(count($mebRes) > 0) {
			//print_r($mebRes);
			if($mebRes[0]['sub_status'] == "Inactive") {
				$success = array("status" => 'false', 'data' => 'Status inactive');
			} else {
				if($subscibe == '1') {
					$sub_fields = array("*");
					$sub_where = "inv_sub_id='".$SubcriberId."' and inv_dir_id=".$dir_id;
					$mebSub = $db->selectData($SECTION_TABLE_DIR,$sub_fields,$sub_where,$extra="",2);
					if(count($mebSub) > 0) {
						$add_values["inv_status"]		=	"Active";
						$add_values["inv_updated_id"]	=	getAdminSessionId();
						$add_values["inv_updated_date"]	=	date('Y-m-d H:i:s');	
						$SECTION_WHERE=$sub_where;
        				$GPDetail_result = $db->updateData($SECTION_TABLE_DIR, $add_values, $SECTION_WHERE); 
						$success = array("status" => 'true', 'data' => 'Success');
					} else {
						$add_values["inv_dir_id"]		=	$dir_id;
						$add_values["inv_sub_id"]		=	$SubcriberId;
						$add_values["inv_status"]		=	"Active";
						$add_values["inv_created_id"]	=	getAdminSessionId();
						$add_values["inv_created_date"]	=	date('Y-m-d H:i:s');
						$GPDetail_result = $db->insertData($SECTION_TABLE_DIR, $add_values);
						$success = array("status" => 'true', 'data' => 'Success');
					}						
				} else {

					$val_fields = array("*");
					$val_where = "inv_sub_id='".$SubcriberId."' and inv_status='Active'";
					$valSub = $db->selectData($SECTION_TABLE_DIR,$val_fields,$val_where,$extra="",2);					

					if(count($valSub) > 1) {
						$sub_fields = array("*");
						$sub_where = "inv_sub_id='".$SubcriberId."' and inv_dir_id=".$dir_id;
						$mebSub = $db->selectData($SECTION_TABLE_DIR,$sub_fields,$sub_where,$extra="",2);
						if(count($mebSub) > 0) {
							$add_values["inv_status"]		=	"Inactive";
							$add_values["inv_updated_id"]	=	getAdminSessionId();
							$add_values["inv_updated_date"]	=	date('Y-m-d H:i:s');	
							$SECTION_WHERE=$sub_where;
        					$GPDetail_result = $db->updateData($SECTION_TABLE_DIR, $add_values, $SECTION_WHERE); 
							$success = array("status" => 'true', 'data' => 'Success');
						} else {
							$add_values["inv_dir_id"]		=	$dir_id;
							$add_values["inv_sub_id"]		=	$SubcriberId;
							$add_values["inv_status"]		=	"Inactive";
							$add_values["inv_created_id"]	=	getAdminSessionId();
							$add_values["inv_created_date"]	=	date('Y-m-d H:i:s');
							$GPDetail_result = $db->insertData($SECTION_TABLE_DIR, $add_values);
							$success = array("status" => 'true', 'data' => 'Success');
						}
					} else {
						$success = array("status" => 'false', 'data' => 'Atleast one subscription required.');
					}					
				}
			}
		} else {
			$success = array("status" => 'false', 'data' => 'No record found');
		}
		
	}
	echo json_encode($success);
	exit(); 
?>
