<?php
	error_reporting(1);
	header('Access-Control-Allow-Origin: *');
	
	include_once('../../../include/includeclass.php');
	$success = array();
	$action = $_POST['action'];
	
	if($action == 'GetMsgList') {
		
		$SubcriberId			=	$_POST['SubcriberId'];
		$SECTION_FIELD_PREFIX=	$_POST['fieldPrefix'];
		$SECTION_MANAGE_PAGE	=	$_POST['managePagemessage'];	
		$searchchar 			= 	mysql_real_escape_string($_POST['search_message']);
		$SECTION_TABLE			= 	$_POST['tableName'];
		$xtraCondition			=	stripslashes($_POST['xtraCondition']);
		$subemail				=	$_POST['subemail'];
		
		$SECTION="Messaging";

		//$meb_dirId = getMemeberAsDir();
		$meb_dirId = "select meb_id from tbl_member where meb_status='Active' AND meb_dir_id IN ( SELECT inv_dir_id FROM tbl_subscriber_individual where inv_sub_id=".$SubcriberId.")";
				
		$total_language = count($result_language);
		#################################################################
		if($orderby == "")
		{
			$ORDER =  "desc";
			$orderby = "id";
		}

		$where = ' AND p.msg_id NOT IN ( SELECT arc_msg_is from tbl_member_messaging_archive where arc_sub_id='.$SubcriberId.') ';	
	
		##################################  General Query ###############################################  
		if ($searchchar != 'undefined' && $searchchar != "") {
			if ($searchchar == 'other') {
				$sql_query = "SELECT p.* FROM " . $SECTION_TABLE . " p WHERE p." . $SECTION_FIELD_PREFIX . "id != 0   AND  p.".$SECTION_FIELD_PREFIX."status!='Deleted' AND p.msg_meb_id IN (".$meb_dirId.") AND `msg_message` REGEXP '^[^a-zA-Z]'".$where." order by p." . $SECTION_FIELD_PREFIX . $orderby . " " . $ORDER;
			}
			else {
				if ($searchchar == 'all') {
					$sql_query = "SELECT p.* FROM " . $SECTION_TABLE . " p WHERE p." . $SECTION_FIELD_PREFIX . "id != 0  AND p.".$SECTION_FIELD_PREFIX."status!='Deleted' AND p.msg_meb_id IN (".$meb_dirId.")".$where." order by p." . $SECTION_FIELD_PREFIX . $orderby . " " . $ORDER;
				} else {
					$ts = $searchchar . "%";
					$sql_query = "select * from ".$SECTION_TABLE." as p where p.msg_message LIKE '" . $ts . "' AND p.".$SECTION_FIELD_PREFIX."status!='Deleted' AND p.msg_meb_id IN (".$meb_dirId.")".$where." order by " . $SECTION_FIELD_PREFIX . $orderby . " " . $ORDER;
				}
			}
		}
		else if ($xtraCondition != 'undefined' && $xtraCondition != "") {
			//$ts = $xtraCondition;
			$sql_query = "select * from ".$SECTION_TABLE." as p where (" . $xtraCondition . ") AND (p.".$SECTION_FIELD_PREFIX."status!='Deleted' AND p.msg_meb_id IN (".$meb_dirId."))".$where." order by " . $SECTION_FIELD_PREFIX . $orderby . " " . $ORDER;
		}
		else
		{
			$sql_query = "SELECT p.* FROM ".$SECTION_TABLE." as p WHERE p.".$SECTION_FIELD_PREFIX."id != 0 AND p.".$SECTION_FIELD_PREFIX."status != 'Deleted' AND p.msg_meb_id IN (".$meb_dirId.")".$where." order by p.".$SECTION_FIELD_PREFIX.$orderby." ".$ORDER;

		}    
		#################################  Paging Query + Paging Code ##################################
		$paging_query = $sql_query;
		$paging_result  = $db->select($paging_query); 
		$count = count($paging_result);
		$per_page = SITE_PAGING_PER_PAGE; //rows per page
		$per_page = 20;
		$pages = ceil($count/$per_page);    
		#################################################################################################
		if($action_type == "paging")
		{
			if(!empty($_POST['pagem']))  
			{
				$page = $_POST['pagem'];
			}       
		}
		else
		{
			if(!empty($_POST['pagem']))  
				$page = $_POST['pagem'];
	  		else
				$page = 1;
		} 	
		$list_query = $sql_query;
		if(!empty($per_page) && $_POST['page']!="all")
		{
			$start = ($page-1)*$per_page;
			if($start<0)
			{
				$start=0;
			}
			$list_query .= " limit $start,$per_page"; 
		} 
		$result_query  = $db->select($list_query);  
		$total_rows = count($result_query);
		//echo $ms = ajaxMsg($_SESSION['msg']);
		
		if($total_rows > 0){
			$j = 1;
			$dir_data = '<ul>';
			for($i=0;$i<$total_rows;$i++){
				//$dir_data .= $result_query[$i]['Dirname'];
				$sub_con='';
				if($SUBSCRIBER_MAIL != '') {
					$sub_con='\'sub\'';
					$fun='setmessagedetailpage(\''.$result_query[$i][$SECTION_FIELD_PREFIX."id"].'\', \'msg\')';
				} else {
					$fun='setmessagedetailpage(\''.$result_query[$i][$SECTION_FIELD_PREFIX."id"].'\', \'msg\')';
				}
				$href = '#';
				$dir_data .= '<li id="mas_'.$result_query[$i][$SECTION_FIELD_PREFIX."id"].'"><a onclick="'.$fun.'" href="'.$href.'" class="masage_a" data-ajax="true" load="yes" data-prefetch="true">'.$result_query[$i][$SECTION_FIELD_PREFIX.'message'].'</a><div class="masage_div"><a href="#" onclick="return mas_archive(\''.$result_query[$i][$SECTION_FIELD_PREFIX."id"].'\')"><img src="images/archive.png" /></a></div></li>';
			}
		
			$dir_data .= '</ul>';
			
			if(count($pages)>0) {
			$pagination = '<div class="paging"><ul>';
				for($t=1;$t<=$pages;$t++) {
					if($page==$t) {
						$pagination .= '<li><a class="active" href="javascript:void(0);"';
						if($pages > 1){
							$pagination .= 'onclick="getAjaxPaging(\'Messaging\',\''.$pages.'\',\''.$t.'\')"'; 
						}
						$pagination .= '>'.$t.'</a></li>';
						} else {
						$pagination .= '<li><a href="javascript:void(0);"';
						if($pages > 1){ 
							$pagination .= 'onclick="getAjaxPaging(\'Messaging\',\''.$pages.'\',\''.$t.'\')"';
						}
						$pagination .= '>'.$t.'</a></li>';
					}
				}
				$pagination .= '</div></ul>';	
				$pagination .= '<script>';	
				$pagination .= 'function getAjaxPaging(action, total, current){ ';
				$pagination .= '$("#page_val_message").val(current);'; 
				$pagination .= 'getmessages("yes");}';	
				$pagination .= '</script>';	
			}			
			$success = array("status" => 'true', 'data' => $dir_data, 'pagination' => $pagination);
		} else {
			$dir_data = '<ul>';
			$dir_data .= '<li><a href="javascript:void(0);">No record found.</a></li>';
			$dir_data .= '</ul>';
			$success = array("status" => 'true', 'data'=>$dir_data, 'pagination' => '');
		}
	} else if($action == 'GetMessageDetail') {

		$MessageDetailId = $_REQUEST['MessageDetailId'];
		
		$dir_fields = array("*");
		$dir_where  = "msg_id = ".$MessageDetailId." AND msg_status = 'Active'";
		$dirRes 	= $db->selectData('tbl_member_messaging LEFT JOIN tbl_member ON msg_meb_id = meb_id LEFT JOIN  tbl_directory ON dir_id=meb_dir_id', $dir_fields, $dir_where,$extra="",2);

		$dir_data = '<article class="tab-content new_ul_content"><div class="error_msg" style="display:none;"></div><ul class="view-massage">';
      $dir_data .= '<li><b>Message</b><span class="sent-on">Sent On :'.date('m/d/Y',strtotime($dirRes[0]["msg_created_date"])).'</span></li>';
      
		$dir_data .= '<li><p>'.$dirRes[0]["msg_message"].'</p><div class="fl"><b>Member name</b><span>'.$dirRes[0]["meb_name"].'</span></div><div class="fl ar-txt"><b>Directory name</b><span>'.$dirRes[0]["dir_name"].'</span></div></li>';
		
		if($dirRes[0]["msg_file"] != '') {
			
			$msg_file = UPLOAD_WWW_MESSAGING_FILE.$dirRes[0]["msg_id"]."/".$dirRes[0]["msg_file"];
		
			$fileNameExt = explode(".", $dirRes[0]["msg_file"]);
			$fileExt 	 = $fileNameExt[count($fileNameExt)-1];
			$allowedImgExts = array("gif","jpg","jpeg","png","GIF","JPG","JPEG","PNG");	
			$allowedVidExts = array("3gp","amv","avi","mp4");			
			if(in_array($fileExt,$allowedImgExts)) {
				$image = "<img src='".$msg_file."' height='115' width='115' style='height: 115px; width: 190px;'  />";
			} else if(in_array($fileExt,$allowedVidExts)) {
				$image = "<img src='images/play-button.jpg' height='115' width='115' style='height: 115px; width: 190px;' onclick='play_new_video(\"".$msg_file."\")' />";
			} else {
				$image = $dirRes[0]["msg_file"];
			}
			//onclick="return downloadf(\''.$msg_file.'\')"
			//id="startDl" onclick="onDeviceReady()"
			if(in_array($fileExt,$allowedVidExts)) {
				$dir_data .= '<li><div class="fl"><b>Attached File</b><a class="" i-uri="'.$msg_file.'" i-name="'.$dirRes[0]["msg_file"].'" href="">'.$image."</a></div></li>";
				$dir_data .= '<script> function play_new_video(vid) { window.plugins.videoPlayer.play(vid); }</script>';
			} else {
				$dir_data .= '<li><div class="fl"><b>Attached File</b><a class="download" i-uri="'.$msg_file.'" i-name="'.$dirRes[0]["msg_file"].'" href="">'.$image."</a></div></li>";
			}
		}
		
		//$dir_data .= '<li><b>Directory name :</b><span>'.$dirRes[0]["dir_name"].'</span></li>';
     	//$dir_data .= '<li></li>';
     	$dir_data .='</article>';
		if($dirRes[0]["msg_file"] != '') {
			$dir_data .= '<script>';
				$dir_data .= '$(".download").on("click", function(){
									var fileTransfer = new FileTransfer();
									var uri = encodeURI($(this).attr("i-uri"));
									var filePath = "file://sdcard/"+$(this).attr("i-name");
									fileTransfer.download(
    									uri,
    									filePath,
    								function(entry) {
        								console.log("download complete: " + entry.fullPath); 
									},
    								function(error) {
        								console.log("download error source " + error.source);
        								console.log("download error target " + error.target);
        								console.log("upload error code" + error.code);
    								},
    								false,
    								{
        								headers: {
            							"Authorization": "Basic dGVzdHVzZXJuYW1lOnRlc3RwYXNzd29yZA=="
        								}
    								}
								);
							})';
			$dir_data .= '</script>';     	
     	}
      $success = array("status" => 'true', 'data' => $dir_data); 				
			
	} else if($action == 'MessageArchive'){

		$msg_id = $_REQUEST['id'];		
		$sub_id = $_REQUEST['sub_id'];
		
		$add_values['arc_msg_is'] 		= trim($msg_id);
		$add_values['arc_sub_id'] 		= trim($sub_id);
		$add_values['arc_created_id'] = getAdminSessionId();
    	$add_values['arc_created_date'] = $today_date;	
		
		$GPDetail_result = $db->insertData('tbl_member_messaging_archive', $add_values);
		if($GPDetail_result > 0) {
			$success = array("status" => 'true', 'data' => '');	
		} else {
			$success = array("status" => 'false', 'data' => '');
		}
		
	} else {
		$success = array("status" => 'false');
	}
	echo json_encode($success);
	exit();
?>