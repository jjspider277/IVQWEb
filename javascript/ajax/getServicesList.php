<?php
	include_once('../../include/includeclass.php');
	$action_type			=	$_REQUEST['action_type'];
	$SECTION_TABLE			= 	$_REQUEST['tableName'];
	$SECTION_FIELD_PREFIX	=	$_REQUEST['fieldPrefix'];
	$SECTION_AUTO_ID 		=	$_REQUEST['autoID'];
	$SECTION_MANAGE_PAGE	=	$_REQUEST['managePage'];
	$SECTION_WHERE			=	$SECTION_FIELD_PREFIX."id='".$SECTION_AUTO_ID."'";
	$xtraCondition			=	stripslashes($_REQUEST['xtraCondition']);
	$SECTION_NAME			=	$_REQUEST['displayName'];
	$searchchar				=	$_REQUEST['searchchar'];

    $search_text			=	$_REQUEST['search_text'];

	/*echo "<pre>";
	print_r($_REQUEST);
	echo "</pre>";
	exit;*/
	$SECTION="Services";

	$meb_dirId = getMemberSessionDirId();

	$total_language = count($result_language);
	#################################################################

	if($action_type ==  "delete") {
		$update_values[$SECTION_FIELD_PREFIX.'status']      = "Deleted";
		$update_values[$SECTION_FIELD_PREFIX . 'updated_id'] 	= $_SESSION['meb_id'];
        $update_values[$SECTION_FIELD_PREFIX . 'updated_date'] 	= date("Y-m-d H:i:s");
		$GPDetail_result = $db->updateData($SECTION_TABLE,$update_values,$SECTION_WHERE);
		//$_SESSION['msg']  =   "Subscribers has been deleted successfully.";  
	}
	
	else if($action_type  ==  "sorting") {
		$orderby  = $_REQUEST['orderby'];
		$order    = $_REQUEST['order'];
		if($order == "asc" || $order == "")
			$ORDER =  "desc";
		else
			$ORDER =  "asc";
	}
	if($orderby == "") {
		$ORDER =  "desc";
		$orderby = "id";
	}
	##################################  General Query ###############################################  
	if ($search_text != 'undefined' && $search_text != "") {
        $ss_where = " (ser_title LIKE '%".$search_text."%' OR ser_description LIKE '%".$search_text."%') ";
        $sql_query = "SELECT p.* FROM " . $SECTION_TABLE . " p WHERE p." . $SECTION_FIELD_PREFIX . "id != 0 AND ".$ss_where." AND p.".$SECTION_FIELD_PREFIX."status!='Deleted' AND p.ser_dir_id IN (".$meb_dirId.") order by p." . $SECTION_FIELD_PREFIX . $orderby . " " . $ORDER;
	} else {
		$sql_query = "SELECT p.* FROM ".$SECTION_TABLE." as p WHERE p.".$SECTION_FIELD_PREFIX."id != 0 AND p.".$SECTION_FIELD_PREFIX."status != 'Deleted' AND p.ser_dir_id IN (".$meb_dirId.") order by p.".$SECTION_FIELD_PREFIX.$orderby." ".$ORDER;

	}    
	#################################  Paging Query + Paging Code ##################################
	$paging_query = $sql_query;
	$paging_result  = $db->select($paging_query); 
	$count = count($paging_result);
	$per_page = SITE_PAGING_PER_PAGE; //rows per page
	$per_page = 8;
	$pages = ceil($count/$per_page);    
	#################################################################################################
	if($action_type == "paging") {
		if(!empty($_REQUEST['page'])) {
			$page = $_REQUEST['page'];
		}       
	} else {
		if(!empty($_REQUEST['page']))
			$page = $_REQUEST['page'];
	  	else
		$page = 1;
	} 
	$list_query = $sql_query;
	if(!empty($per_page) && $_REQUEST['page']!="all") {
		$start = ($page-1)*$per_page;
		if($start<0) {
			$start=0;
		}
		$list_query .= " limit $start,$per_page"; 
	} 
	$result_query  = $db->select($list_query);  
	$total_rows = count($result_query);
	//echo $ms = ajaxMsg($_SESSION['msg']);

if($page>1)
    echo '<div class="group-setion-row" id="updatediv">';

    if($total_rows>0) { ?>
        <?php $j=1; for($i=0;$i<$total_rows;$i++) {
            $file_image = 'images/service-list-icon.png';
            if($result_query[$i][$SECTION_FIELD_PREFIX.'logo_file']!=''){
                $file_image = UPLOAD_WWW_SERVICE_FILE.$result_query[$i][$SECTION_FIELD_PREFIX.'id']."/".$result_query[$i][$SECTION_FIELD_PREFIX.'logo_file'];
            }
        ?>
            <div class="group-setion-col gr_on_view_card123">
                <div class="group-setion-col-img group-setion-col-img-1"><img src="<?php echo $file_image; ?>"></div>
                <div class="group-setion-col-h3"><?php echo $result_query[$i][$SECTION_FIELD_PREFIX.'title']; ?></div>
                <div class="group-setion-col-p-1"><?php echo charCut($result_query[$i][$SECTION_FIELD_PREFIX.'description'],"75","...."); ?></div>
                <div class="group-setion-col-hover">
                    <div class="hover-icon">
                        <a href="view_records.php?sectionName=Services&ser_id=<?php echo $result_query[$i][$SECTION_FIELD_PREFIX."id"];?>" class="fancybox fancybox.ajax tooltip hover-icon-margin" title="View" data-fancybox-type="ajax"><img src="images/view-icon.png" /></a>
                        <a href="<?php echo getMemberURL($SECTION_MANAGE_PAGE,"Edit","ser_id=".$result_query[$i][$SECTION_FIELD_PREFIX."id"]); ?>" class="hover-icon-margin tooltip" title="Edit"><img src="images/efit-text.png"></a>
                        <a  href="javascript:void(0);" class="tooltip" title="Delete" onclick="javascript:alertBoxMember('<?php echo $SECTION; ?>','<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>',<?php echo $result_query[$i][$SECTION_FIELD_PREFIX."id"]; ?>,'','<?php echo $SECTION_MANAGE_PAGE;?>');"><img src="images/remove-icon.png"></a>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php } else { ?>
            <div class="no-record" style="margin-left: 10px;">No service found.</div>
        <?php }

if($pages>1)
    echo '</div>';
?>

<script type="text/javascript">
    $(document).ready(function() {
        $('.tooltip').tooltipster();
		$.fancybox.close();

        curr = '';
        curr = '<?php echo $page; ?>';

        total = '';
        total = '<?php echo $pages; ?>';

        $('#total_page').val(total);
        $('#current_page').val(curr);

        if(curr<='1') {
            infinite_f();
        }
    });
</script>