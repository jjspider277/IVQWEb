<?php
	include_once('../../include/includeclass.php');

    $action_type			=	$_REQUEST['action_type'];
	$SECTION_TABLE			= 	$_REQUEST['tableName'];
	$SECTION_FIELD_PREFIX	=	$_REQUEST['fieldPrefix'];
	$SECTION_AUTO_ID 		=	$_REQUEST['autoID'];
	$SECTION_MANAGE_PAGE	=	$_REQUEST['managePage'];
	$SECTION_WHERE			=	$SECTION_FIELD_PREFIX."id='".$SECTION_AUTO_ID."'";
	$xtraCondition			=	stripslashes($_REQUEST['xtraCondition']);
    $searchchar             =   $_REQUEST['search_text'];
    $SECTION="Requests";

    $dirId = getMemberSessionDirId();

    $mes = '';
    #################################################################

	if($action_type ==  "delete") {
		$update_values[$SECTION_FIELD_PREFIX.'status']          = "Deleted";
		$update_values[$SECTION_FIELD_PREFIX . 'updated_id'] 	= $_SESSION['meb_id'];
        $update_values[$SECTION_FIELD_PREFIX . 'updated_date'] 	= date("Y-m-d H:i:s");

        $sub_qr = "SELECT p.*, d.* FROM tbl_member_subscriber as p LEFT JOIN ".TBL_MEMBER_SUBSCRIBE_DIRECTORY." as d ON sbm_sub_id = sub_id WHERE p.sub_id != 0 AND p.sub_status != 'Deleted' AND d.sbm_id='".$SECTION_AUTO_ID."'";
        $sub_data = $db->select($sub_qr);
        if(count($sub_data)>0) {

        	$dir_dd = 'SELECT * FROM tbl_directory where dir_id="'.$dirId.'"';
            $dir_re = $db->select($dir_dd);

            $user_email = $sub_data[0]['sub_email'];
            $user_name = $sub_data[0]['sub_name'];
            $mebIdN = $sub_data[0]['sub_id'];
            $message = "Your invitation request as been denied by the <b>".$dir_re[0]['dir_name']."</b> group administrator.";
            $filepath='';
            sendMessageSub($mebIdN,$message,$filepath);
        }
        mysql_query("delete from tbl_member_subscribe_directory where sbm_id = '".$SECTION_AUTO_ID."'");
		//$GPDetail_result = $db->updateData($SECTION_TABLE,$update_values,$SECTION_WHERE);
		$mes = $sub_data[0]['sub_email']." invitation request as been denied successfully.";
	} else if($action_type ==  "active") {
		$update_values[$SECTION_FIELD_PREFIX.'status']      = "Active";
		$update_values[$SECTION_FIELD_PREFIX . 'updated_id'] 	= $_SESSION['meb_id'];
        $update_values[$SECTION_FIELD_PREFIX . 'updated_date'] 	= date("Y-m-d H:i:s");
		$GPDetail_result = $db->updateData($SECTION_TABLE,$update_values,$SECTION_WHERE);
		
        $sub_qr = "SELECT p.*, d.* FROM tbl_member_subscriber as p LEFT JOIN ".TBL_MEMBER_SUBSCRIBE_DIRECTORY." as d ON sbm_sub_id = sub_id WHERE p.sub_id != 0 AND p.sub_status != 'Deleted' AND d.sbm_id='".$SECTION_AUTO_ID."'";
        $sub_data = $db->select($sub_qr);

        if(count($sub_data)>0) {
        	$dir_dd = 'SELECT * FROM tbl_directory where dir_id="'.$dirId.'"';
            $dir_re = $db->select($dir_dd);

            $user_email = $sub_data[0]['sub_email'];
            $user_name = $sub_data[0]['sub_name'];
            $mebIdN = $sub_data[0]['sub_id'];
            $message = "Your invitation request has been approved by the <b>".$dir_re[0]['dir_name']."</b> group administrator.";
            $filepath='';
            sendMessageSub($mebIdN,$message,$filepath);
        }
        $mes = $sub_data[0]['sub_email']." invitation request as been approved successfully.";

	} else if($action_type ==  "inactive") {
		$update_values[$SECTION_FIELD_PREFIX.'status']      = "Inactive";
		$update_values[$SECTION_FIELD_PREFIX . 'updated_id'] 	= $_SESSION['meb_id'];
        $update_values[$SECTION_FIELD_PREFIX . 'updated_date'] 	= date("Y-m-d H:i:s");
		$GPDetail_result = $db->updateData($SECTION_TABLE,$update_values,$SECTION_WHERE);
		//$_SESSION['msg']  =   "Subscribers has been deleted successfully.";

        $sub_qr = "SELECT p.*, d.* FROM tbl_member_subscriber as p LEFT JOIN ".TBL_MEMBER_SUBSCRIBE_DIRECTORY." as d ON sbm_sub_id = sub_id WHERE p.sub_id != 0 AND p.sub_status != 'Deleted' AND d.sbm_id='".$SECTION_AUTO_ID."'";
        $sub_data = $db->select($sub_qr);

        if(count($sub_data)>0) {
        	$dir_dd = 'SELECT * FROM tbl_directory where dir_id="'.$dirId.'"';
            $dir_re = $db->select($dir_dd);

            $user_email = $sub_data[0]['sub_email'];
            $user_name = $sub_data[0]['sub_name'];
            $mebIdN = $sub_data[0]['sub_id'];
            $message = "Your invitation request as been denied by the <b>".$dir_re[0]['dir_name']."</b> group administrator.";
            $filepath='';
            sendMessageSub($mebIdN,$message,$filepath);
        }
	} else if($action_type  ==  "sorting") {
		$orderby  = $_REQUEST['orderby'];
		$order    = $_REQUEST['order'];
		if($order == "asc" || $order == "")
			$ORDER =  "desc";
		else
			$ORDER =  "asc";
	}

    if($orderby == "") {
		$ORDER =  "desc";
		$orderby = "id";
	}
	###########################  General Query ######################################  
	if ($searchchar != 'undefined' && $searchchar != "") {
        $ss_where = " (p.sub_email LIKE '%".$searchchar."%' OR p.sub_name LIKE '%".$searchchar."%' OR p.sub_title LIKE '%".$searchchar."%') ";
        $sql_query = "select p.*, d.* from tbl_member_subscriber as p LEFT JOIN ".TBL_MEMBER_SUBSCRIBE_DIRECTORY." as d ON sbm_sub_id = sub_id where p.sub_id != 0 AND ".$ss_where." AND p.sub_status != 'Deleted' AND d.sbm_dir_id=".$dirId." AND d.sbm_status !='Deleted' GROUP BY d.sbm_sub_id, d.sbm_dir_id order by p.sub_id ".$ORDER;
	} else {
        $sql_query = "SELECT p.*, d.* FROM tbl_member_subscriber as p LEFT JOIN ".TBL_MEMBER_SUBSCRIBE_DIRECTORY." as d ON sbm_sub_id = sub_id WHERE p.sub_id != 0 AND p.sub_status != 'Deleted' AND d.sbm_dir_id=".$dirId." AND d.sbm_status !='Deleted' GROUP BY d.sbm_sub_id, d.sbm_dir_id order by p.sub_id ".$ORDER;
        //$sql_query = "SELECT p.* FROM ".$SECTION_TABLE." as p LEFT JOIN ".TBL_MEMBER_SUBSCRIBE_DIRECTORY." as d ON sbm_sub_id = sub_id WHERE p.sub_type != 'Individual'  AND p.".$SECTION_FIELD_PREFIX."id != 0 AND p.".$SECTION_FIELD_PREFIX."status != 'Deleted' AND d.sbm_dir_id=".$dirId." order by p.".$SECTION_FIELD_PREFIX.$orderby." ".$ORDER;
    }

	//echo $sql_query;
	#################################  Paging Query + Paging Code ##################################
	$paging_query = $sql_query;
	$paging_result  = $db->select($paging_query);
	$count = count($paging_result);
	$per_page = SITE_PAGING_PER_PAGE; //rows per page
	$per_page = 10;
	$pages = ceil($count/$per_page);
	#################################################################################################
	if($action_type == "paging") {
		if(!empty($_REQUEST['page'])) {
			$page = $_REQUEST['page'];
		}
	} else {
		if(!empty($_REQUEST['page']))
			$page = $_REQUEST['page'];
	  	else
		$page = 1;
	}
	$list_query = $sql_query;
	if(!empty($per_page) && $_REQUEST['page']!="all") {
		$start = ($page-1)*$per_page;
		if($start<0)
		{
			$start=0;
		}
		$list_query .= " limit $start,$per_page";
	}
	$result_query  = $db->select($list_query);
	$total_rows = count($result_query);
	//echo $ms = ajaxMsg($_SESSION['msg']);

if($pages>1)
    echo '<div class="group-setion-row" id="updatediv">';
?>

<?php if($total_rows>0) { ?>
    <?php $j=1; for($i=0;$i<$total_rows;$i++) { $group_image='images/group-user-list-icon.png'; ?>
        <div class="group-setion-col gr_on_view_card123">
            <div class="group-setion-col-img"><img style="height: 121px;" src="<?php echo $group_image; ?>"></div>
            <div class="group-setion-col-h3" style="line-height:15px;font-size:16px;"><?php echo $result_query[$i]['sub_email']; ?></div>
            <div class="group-setion-col-hover">
                <div class="hover-icon">
                    <?php if($result_query[$i][$SECTION_FIELD_PREFIX."status"]=="Active") { ?>
                        <!--<a href="javascript:void(0);" onclick="getAjaxStatusInactiveAction('<?php echo $SECTION; ?>','<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>',<?php echo $result_query[$i][$SECTION_FIELD_PREFIX."id"]; ?>,'','<?php echo $SECTION_MANAGE_PAGE;?>','<?php echo $page; ?>')" class="tooltip" title="Approved"><img src="images/active-icon.png" /></a-->
                    <?php } else { ?>
                        <a href="javascript:void(0);" onclick="getAjaxStatusActiveAction('<?php echo $SECTION; ?>','<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>',<?php echo $result_query[$i][$SECTION_FIELD_PREFIX."id"]; ?>,'','<?php echo $SECTION_MANAGE_PAGE;?>','<?php echo $page; ?>')" class="tooltip" title="Approve"><img src="images/active-icon.png" /></a>
                    <?php } ?>
                    <a href="javascript:void(0);" onclick="javascript:alertBoxRequest('<?php echo $SECTION; ?>','<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>',<?php echo $result_query[$i][$SECTION_FIELD_PREFIX."id"]; ?>,'','<?php echo $SECTION_MANAGE_PAGE;?>');" class="tooltip" title="Denied"><img src="images/remove-icon.png" /></a>
                </div>
            </div>
        </div>
    <?php } ?>
<?php } else { ?>
    <div class="no-record">No Records</div>
<?php }
if($pages>1)
    echo "</div>";
?>

<script type="text/javascript">
    $(document).ready(function() {
        $('.tooltip').tooltipster();
		$.fancybox.close();

        curr = '';
        curr = '<?php echo $page; ?>';

        total = '';
        total = '<?php echo $pages; ?>';

        $('#total_page').val(total);
        $('#current_page').val(curr);

        if(curr<='1') {
            infinite_f();
        }

        msg='<?php echo $mes; ?>';
        if(msg!='')
        	alertify.success(msg);
    });
</script>
