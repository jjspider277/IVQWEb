<?php 
	error_reporting(1);
	header('Access-Control-Allow-Origin: *');
	
	include_once('../../../include/includeclass.php');
	$success = array();
	$action = $_REQUEST['action'];
	if($action == 'GetMembersList') {
		
		$action_type				=	$_REQUEST['action_type'];
		$direid 						=  $_REQUEST['direid'];
		$SECTION_TABLE 			= 	$_REQUEST['tableName'];
		$SECTION_FIELD_PREFIX	=	$_REQUEST['fieldPrefix'];
		$SECTION_WHERE				=	$SECTION_FIELD_PREFIX."id='".$SECTION_AUTO_ID."'";
		$xtraCondition				=	stripslashes($_REQUEST['xtraCondition']);
		$SECTION_MANAGE_PAGE 	=  MEB_MANAGE_MEMBERS;
		$searchchar 				= 	$_REQUEST['search_member'];
		
		if($action_type	==	"sorting")
		{
			$orderby	=	$_REQUEST['orderby'];
			$order		=	$_REQUEST['order'];
			if($order == "asc" || $order == "")
				$ORDER =	"desc";
			else
				$ORDER =	"asc";
		}
		if($orderby == "")
		{
			$ORDER =	"desc";
			$orderby = "id";
		}
		$dirName = "(select dir_name from tbl_directory where dir_id = meb_dir_id ) as Dirname";
	
		##################################  General Query ############################################### 	
		if ($searchchar != 'undefined' && $searchchar != "") {
    		if ($searchchar == 'other') {
    	    $sql_query = "SELECT p.*,".$dirName." FROM " . $SECTION_TABLE . " p WHERE p." . $SECTION_FIELD_PREFIX . "id != 0   AND  p.".$SECTION_FIELD_PREFIX."status!='Deleted' AND `meb_name` REGEXP '^[^a-zA-Z]' and ".$xtraCondition." order by p." . $SECTION_FIELD_PREFIX . $orderby . " " . $ORDER;
    		} else {
        		if ($searchchar == 'all') {
            	$sql_query = "SELECT p.*,".$dirName." FROM " . $SECTION_TABLE . " p WHERE p." . $SECTION_FIELD_PREFIX . "id != 0  AND p.".$SECTION_FIELD_PREFIX."status!='Deleted' and ".$xtraCondition." order by p." . $SECTION_FIELD_PREFIX . $orderby . " " . $ORDER;
        		} else {
        	    	$ts = $searchchar . "%";
        	    	$sql_query = "select *,".$dirName." from ".$SECTION_TABLE." as p where p.meb_name LIKE '" . $ts . "' AND p.".$SECTION_FIELD_PREFIX."status!='Deleted' and ".$xtraCondition." order by " . $SECTION_FIELD_PREFIX . $orderby . " " . $ORDER;
        		}
    		}
		}
		else if ($xtraCondition != 'undefined' && $xtraCondition != "") {
			$ts = "%" . $xtraCondition . "%";
			$sql_query = "select *,".$dirName." from ".$SECTION_TABLE." as p where (".$xtraCondition.") AND (p.".$SECTION_FIELD_PREFIX."status!='Deleted')  order by " . $SECTION_FIELD_PREFIX . $orderby . " " . $ORDER;
		}
		else
		{
			$sql_query = "SELECT p.*,".$dirName." FROM ".$SECTION_TABLE." p WHERE p.".$SECTION_FIELD_PREFIX."id != 0 AND p.".$SECTION_FIELD_PREFIX."status != 'Deleted' and ".$xtraCondition." order by p.".$SECTION_FIELD_PREFIX.$orderby." ".$ORDER;
		}

   	 //echo $sql_query;
		$excel_query = "SELECT p.*,".$dirName." FROM ".$SECTION_TABLE." p WHERE p.".$SECTION_FIELD_PREFIX."id != 0 AND p.".$SECTION_FIELD_PREFIX."status != 'Deleted' order by p.".$SECTION_FIELD_PREFIX.$orderby." ".$ORDER;
		$excel_result  = $db->select($excel_query);	
		$excelcount = count($excel_result);	
		
		#################################  Paging Query + Paging Code ##################################
		$paging_query = $sql_query;
		$paging_result  = $db->select($paging_query);	
		$count = count($paging_result);
		$per_page = SITE_PAGING_PER_PAGE; //rows per page
		$per_page = 10;
		$pages = ceil($count/$per_page);		
		#################################################################################################
		if ($action_type == "paging")
		{
			if(!empty($_REQUEST['page']))	
			{
				$page = $_REQUEST['page'];
			}  			
		}
		else
		{
			if(!empty($_REQUEST['page']))	
				$page = $_REQUEST['page'];
			else
		 		$page = 1;
		}	
		$list_query = $sql_query;
		if(!empty($per_page) && $_REQUEST['page']!="all")
		{
			$start = ($page-1)*$per_page;
			if($start<0)
			{
				$start=0;
			}
			$list_query .= " limit $start,$per_page";	
		}	
		$result_query  = $db->select($list_query);	
		$total_rows = count($result_query);
		//echo $ms = ajaxMsg($_SESSION['msg']);
		
		if($total_rows > 0) 
		{	
			$dir_name = $result_query[0]['Dirname'] . ' member directory';
			$dir_data = '<ul>';
			$j = 1;
			for($i=0;$i<$total_rows;$i++)
			{
				$href = 'member_details.html';
				$dir_data .= '<li><a href="'.$href.'" onclick="return setmemberid(\''.$result_query[$i][$SECTION_FIELD_PREFIX.'id'].'\')" data-ajax="false" load="yes" data-prefetch="true">'.$result_query[$i][$SECTION_FIELD_PREFIX.'name'].'</a></li>';
			}
			$dir_data .= '</ul>';			

			if(count($pages)>0) 
			{
				$pagination = '<div class="paging"><ul>';
				for($t=1;$t<=$pages;$t++) 
				{
					if($page==$t) {
						$pagination .= '<li><a class="active" href="javascript:void(0);"';
						if($pages > 1){
							$pagination .= 'onclick="getAjaxPaging(\'Directory\',\''.$pages.'\',\''.$t.'\')"'; 
						}
						$pagination .= '>'.$t.'</a></li>';
						} else {
						$pagination .= '<li><a href="javascript:void(0);"';
						if($pages > 1){ 
							$pagination .= 'onclick="getAjaxPagingmember(\'Directory\',\''.$pages.'\',\''.$t.'\')"';
						}
						$pagination .= '>'.$t.'</a></li>';
					}
				}
				$pagination .= '</div></ul>';	
				$pagination .= '<script>';	
				$pagination .= 'function getAjaxPagingmember(action, total, current){ ';
				$pagination .= '$("#memberpage_val").val(current);'; 
				$pagination .= 'getmembers("no");}';	
				$pagination .= '</script>';				
			}			
			$success = array("status" => 'true', 'data' => $dir_data, 'dir_name' => $dir_name,'pagination' => $pagination);
		} 
		else 
		{
			$dir_data = '<ul>';
			$dir_data .= '<li><a href="javascript:void(0);">No record found.</a></li>';
			$dir_data .= '</ul>';
			$success = array("status" => 'true', 'data'=>$dir_data, 'pagination' => '');
		}
	} else if($action == 'GetDirecotyCardsList') {
	
		$dirId = $_REQUEST['direid'];
		$sub_email = $_REQUEST['email'];
		$SubcriberId = $_REQUEST['SubcriberID'];
		$subribe	= $_REQUEST['subribe'];
		$orderby = $_REQUEST['orderby'];
		
		$order = '';
		if($_REQUEST['orderby'] == 'bus_name') 	
			$order = 'bus_name asc';
		if($_REQUEST['orderby'] == 'bus_company') 	
			$order = 'bus_company asc';
		else 
			$order = 'bus_title asc';
			
		//$order1 = $_REQUEST['order'];
		//if($order1 == '')
		//$order .= 'asc';

		if(!empty($_REQUEST['page']))	
			$page = $_REQUEST['page'];
		else
			$page = 1;
		$per_page = 7;	
			
		$dir_fields = array("meb_id");
		$dir_where  = "meb_dir_id = ".$dirId." AND meb_status = 'Active'";
		$dirRes 	= $db->selectData(TBL_MEMBER,$dir_fields,$dir_where,$extra="",2);	

		if(count($dirRes) > 0) {
			$idmem='';
			for($i=0;$i<count($dirRes);$i++){
				$idmem .= $dirRes[$i]["meb_id"].',';
			}
		}		
		$idmem = substr($idmem, 0, -1);
		
		$sub_fields = array("sbm_sub_id");
		$sub_where  = "sbm_dir_id= ".$dirId." AND sbm_status != 'Deleted'";
		$subRes 	= $db->selectData(TBL_MEMBER_SUBSCRIBE_DIRECTORY,$sub_fields,$sub_where,$extra="",2);
		if(count($subRes) > 0) {
			$idsub='';
			for($i=0;$i<count($subRes);$i++){
				$idsub .= $subRes[$i]["sbm_sub_id"].',';
			}
		}
		$idsub = substr($idsub, 0, -1);
		
		$searchchar = $_REQUEST['search_member'];
		
		if($idmem!="" && $idsub!="")
		{
			if ($searchchar != 'undefined' && $searchchar != "") {
				if ($searchchar == 'Other') {
					$ts = $searchchar . "%";
					$sqlBusQuery = "SELECT * , 'Member' AS Cardtype FROM ".TBL_MEMBER_BUSINESS." WHERE bus_title REGEXP '^[^a-zA-Z]' AND bus_id!= 0 AND bus_meb_id IN (".$idmem.") AND bus_status != 'Deleted' UNION SELECT * , 'Subscriber' AS Cardtype FROM ".TBL_MEMBER_BUSINESS_SUB." WHERE bus_title REGEXP '^[^a-zA-Z]' AND bus_sub_id IN (".$idsub.") AND bus_status != 'Deleted' order by ".$order;
			
				}
				else
				{
					if ($searchchar == 'All') {
						$sqlBusQuery = "SELECT * , 'Member' AS Cardtype FROM ".TBL_MEMBER_BUSINESS." WHERE bus_id!= 0 AND bus_meb_id IN (".$idmem.") AND bus_status != 'Deleted' UNION SELECT * , 'Subscriber' AS Cardtype FROM ".TBL_MEMBER_BUSINESS_SUB." WHERE bus_sub_id IN (".$idsub.") AND bus_status != 'Deleted' order by ".$order;
					} else {
					$ts = $searchchar . "%";
					$sqlBusQuery = "SELECT * , 'Member' AS Cardtype FROM ".TBL_MEMBER_BUSINESS." WHERE ".$orderby." LIKE '".$ts."' AND bus_id!= 0 AND bus_meb_id IN (".$idmem.") AND bus_status != 'Deleted' UNION SELECT * , 'Subscriber' AS Cardtype FROM ".TBL_MEMBER_BUSINESS_SUB." WHERE ".$orderby." LIKE '".$ts."' AND bus_sub_id IN (".$idsub.") AND bus_status != 'Deleted' order by ".$order;
					}
				}
			}
			else
			{
				$sqlBusQuery = "SELECT * , 'Member' AS Cardtype FROM ".TBL_MEMBER_BUSINESS." WHERE bus_id!= 0 AND bus_meb_id IN (".$idmem.") AND bus_status != 'Deleted' UNION SELECT * , 'Subscriber' AS Cardtype FROM ".TBL_MEMBER_BUSINESS_SUB." WHERE bus_sub_id IN (".$idsub.")  AND bus_status != 'Deleted' order by ".$order;
			}
		
			$paging_query = $sqlBusQuery;
			$paging_result = $db->select($paging_query);	
			$count = count($paging_result);		
			$pages = ceil($count/$per_page);	
			if(!empty($per_page) && $_REQUEST['page']!="all") {
				$start = ($page-1)*$per_page;
				if($start<0) {
					$start=0;
				}
				$sqlBusQuery .= " limit $start,$per_page";	
			}
			$BusResult  = $db->select($sqlBusQuery);
			$totBus = count($BusResult);

		} else if($idmem!='') {
			
			if ($searchchar != 'undefined' && $searchchar != "") {
				if ($searchchar == 'Other') {
					$ts = $searchchar . "%";
					$sqlBusQuery = "SELECT * , 'Member' AS Cardtype FROM ".TBL_MEMBER_BUSINESS." WHERE bus_title REGEXP '^[^a-zA-Z]' AND bus_id!= 0 AND bus_meb_id IN (".$idmem.") AND bus_status != 'Deleted' order by ".$order;
				}
				else
				{
					if ($searchchar == 'All') {
						$sqlBusQuery = "SELECT * , 'Member' AS Cardtype FROM ".TBL_MEMBER_BUSINESS." WHERE bus_id!= 0 AND bus_meb_id IN (".$idmem.") AND bus_status != 'Deleted' order by ".$order;
					} else {
					$ts = $searchchar . "%";
					$sqlBusQuery = "SELECT * , 'Member' AS Cardtype FROM ".TBL_MEMBER_BUSINESS." WHERE ".$orderby." LIKE '".$ts."' AND bus_id!= 0 AND bus_meb_id IN (".$idmem.") AND bus_status != 'Deleted' order by ".$order;
					}
				}
			}
			else
			{
				$sqlBusQuery = "SELECT * , 'Member' AS Cardtype FROM ".TBL_MEMBER_BUSINESS." WHERE bus_id!= 0 AND bus_meb_id IN (".$idmem.") AND bus_status != 'Deleted' order by ".$order;
			}

			$paging_query = $sqlBusQuery;
			$paging_result = $db->select($paging_query);	
			$count = count($paging_result);		
			$pages = ceil($count/$per_page);	
			if(!empty($per_page) && $_REQUEST['page']!="all") {
				$start = ($page-1)*$per_page;
				if($start<0) {
					$start=0;
				}
				$sqlBusQuery .= " limit $start,$per_page";	
			}
			$BusResult  = $db->select($sqlBusQuery);
			$totBus = count($BusResult);			
			
		} else if($subRes!='') {
			
			if ($searchchar != 'undefined' && $searchchar != "") {
				if ($searchchar == 'Other') {
					$ts = $searchchar . "%";
					$sqlBusQuery = "SELECT * , 'Subscriber' AS Cardtype FROM ".TBL_MEMBER_BUSINESS_SUB." WHERE bus_title REGEXP '^[^a-zA-Z]' AND bus_sub_id IN (".$idsub.") AND bus_status != 'Deleted' order by ".$order;
			
				}
				else
				{
					if ($searchchar == 'All') {
						$sqlBusQuery = "SELECT * , 'Subscriber' AS Cardtype FROM ".TBL_MEMBER_BUSINESS_SUB." WHERE bus_sub_id IN (".$idsub.") AND bus_status != 'Deleted' order by ".$order;
					} else {
					$ts = $searchchar . "%";
					$sqlBusQuery = "SELECT * , 'Subscriber' AS Cardtype FROM ".TBL_MEMBER_BUSINESS_SUB." WHERE ".$orderby." LIKE '".$ts."' AND bus_sub_id IN (".$idsub.") AND bus_status != 'Deleted' order by ".$order;
					}
				}
			}
			else
			{
				$sqlBusQuery = "SELECT * , 'Subscriber' AS Cardtype FROM ".TBL_MEMBER_BUSINESS_SUB." WHERE bus_sub_id IN (".$idsub.") AND bus_status != 'Deleted' order by ".$order;
			}

			$paging_query = $sqlBusQuery;
			$paging_result = $db->select($paging_query);	
			$count = count($paging_result);		
			$pages = ceil($count/$per_page);	
			if(!empty($per_page) && $_REQUEST['page']!="all") {
				$start = ($page-1)*$per_page;
				if($start<0) {
					$start=0;
				}
				$sqlBusQuery .= " limit $start,$per_page";	
			}
			$BusResult  = $db->select($sqlBusQuery);
			$totBus = count($BusResult);	
		}
	
	
		//$sqlSerQuery = "SELECT * FROM ".TBL_MEMBER_SERVICES." WHERE ser_id!= 0 AND ser_meb_id IN (".$idmem.") AND ser_status != 'Deleted' order by ser_id desc";
		//$SerResult  = $db->select($sqlSerQuery);
		//$totSer = count($SerResult);						
				
		$check_con = '';
		$dir_data = '<ul>';
			if($totBus>0) {
				for($j=0;$j<$totBus;$j++) {
					if($BusResult[$j]['Cardtype'] == 'Subscriber') {
						$href = 'card_details_sub.html';
					} else {
						$href = 'card_details.html';
					}
					$dir_data .= '<li><a href="'.$href.'" onclick="return setcardid(\''.$BusResult[$j]['bus_id'].'\')" data-ajax="false" load="yes" data-prefetch="true">'.$BusResult[$j][$orderby].'</a></li>';
				}
			} else {
				$dir_data .= '<li><div class="main_b_card" style="font-weight: bold;line-height: 160px;text-align: center;"><div class="">No business cards available.</div></div></li>';
			}
		$dir_data .= '</ul>';
		
		/*		
		$pagination = '';
		$pagination .= '<div class="paging"><ul>';			
			for ($i = 65; $i < 91; $i++) {
				$charAct = ($searchchar == chr($i)) ? "class='active'" : "" ;	
				$pagination .= '<li><a '.$charAct.' href="javascript:void(0);" onclick="return getmemberschar(this.id)" id="'.chr($i).'">'.chr($i).'</a></li>';	
			}
			$otherAct = ($searchchar == 'Other') ? "class='active'" : "" ;
			$allAct = ($searchchar == 'All') ? "class='active'" : "" ;
			$pagination .= '<li><a '.$allAct.' href="javascript:void(0);" onclick="return getmemberschar(this.id)" id="All">All</a></li>';
			$pagination .= '<li><a '.$otherAct.' href="javascript:void(0);" onclick="return getmemberschar(this.id)" id="Other">Other</a></li>';
		$pagination .= '</ul></div>';
			
		$dir_data .= '</div></article>';
		$pagination = '';
		*/
		if(count($pages)>0) {
			$pagination = '<div class="paging"><ul>';
				for($t=1;$t<=$pages;$t++) {
					if($page==$t) {
						$pagination .= '<li><a class="active" href="javascript:void(0);"';
						if($pages > 1){
							$pagination .= 'onclick="getAjaxPaging(\'Directory\',\''.$pages.'\',\''.$t.'\')"'; 
						}
						$pagination .= '>'.$t.'</a></li>';
						} else {
						$pagination .= '<li><a href="javascript:void(0);"';
						if($pages > 1){ 
							$pagination .= 'onclick="getAjaxPaging(\'Directory\',\''.$pages.'\',\''.$t.'\')"';
						}
						$pagination .= '>'.$t.'</a></li>';
					}
				}
				$pagination .= '</div></ul>';	
				
				$pagination .= '<script>';	
				$pagination .= 'function getAjaxPaging(action, total, current){ ';
				$pagination .= '$("#memberpage_val").val(current);'; 
				$pagination .= 'getmembers("no");}';	
				$pagination .= '</script>';	
						
			}
      $success = array("status" => 'true', 'data' => $dir_data, 'pagination' => $pagination);
		 
   } else if($action == 'GetMemberDetail') {
		$mebId	=	$_REQUEST['meb_id'];
		$sectionName	=	$_REQUEST['sectionName'];
		
		$meb_fields = array("*");
		$meb_where  = "meb_id = ".$mebId." AND meb_status = 'Active'";
		$mebRes 	= $db->selectData(TBL_MEMBER." JOIN ".TBL_MEMBER_BUSINESS." ON meb_id = bus_meb_id",$meb_fields,$meb_where,$extra="",2);
		if(count($mebRes) > 0) {
			if($mebRes[0]['bus_logo_file']!="") {	
				$fileNameExt = explode(".", $mebRes[0]['bus_logo_file']);
				$fileExt 	 = $fileNameExt[count($fileNameExt)-1];
				$allowedImgExts = array("gif","jpg","jpeg","png","GIF","JPG","JPEG","PNG");
				if(in_array($fileExt,$allowedImgExts)) {
					$profile_image = "<a href='#popupParis".$mebRes[0]['bus_id']."' data-rel='popup' data-position-to='window' data-transition='fade' ><img class='popphoto' src='".UPLOAD_WWW_BUSINESS_FILE.$mebRes[0]['bus_id']."/".$mebRes[0]['bus_logo_file']."' height='41' width='70' /></a>";
							$profile_image .= "<div data-role='popup' id='popupParis".$mebRes[0]['bus_id']."' data-overlay-theme='b' data-theme='b' data-corners='false'><a href='#' data-rel='back' class='ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right'>Close</a><img class='popphoto' src='".UPLOAD_WWW_BUSINESS_FILE.$mebRes[0]['bus_id']."/".$mebRes[0]['bus_logo_file']."' style='max-height:512px;' alt='Paris, France'></div>";
				} else {
					$profile_image = "images/profile.png";
				}
			} else {
				$profile_image = "<a href='#popupParis".$mebRes[0]['bus_id']."' data-rel='popup' data-position-to='window' data-transition='fade' ><img class='popphoto' src='images/profile.png' height='41' width='70' /></a>";
							$profile_image .= "<div data-role='popup' style='text-align: center;' id='popupParis".$mebRes[0]['bus_id']."' data-overlay-theme='b' data-theme='b' data-corners='false'><a href='#' data-rel='back' class='ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right'>Close</a><img class='popphoto' src='images/profile.png' style='max-height:512px;' alt='Paris, France'></div>";
			}
			
			$mem_data ='<article id="content">';	

				if($mebRes[0]['bus_website']!=""){ 
					$httparr = array("http://","https://");
					$httptext = substr(trim($mebRes[0]['bus_website']),0,7);
					if ($httptext=="http://" || $httptext=="https:/" ) {
						$website = trim($mebRes[0]['bus_website']);
					} else {
						$website = "http://".trim($mebRes[0]['bus_website']);
					}	
					if($website == '')
						$website = '#';
				}				
				
				$mem_data ='<header class="details-page new_member_profile_title" >Member profile</header><div data-role="navbar" class="ui-navbar" role="navigation">
				<ul class="ui-grid-b">
					<li class="ui-block-a"><a href="'.$website.'" target="_blank" class="ui-link ui-btn">Website</a></li>
					<li class="ui-block-b"><a href="#" class="ui-link ui-btn">Share</a></li>
					<li class="ui-block-c"><a href="playlist.html" class="ui-link ui-btn">Media</a></li>
				</ul>
			</div>';
				
				$mem_data .= '<section><article>'.$profile_image.'<h2>'.$mebRes[0]['meb_name'].'</h2></article></section>';

				$mem_data .='<div class="bottom_three_line">
                <a href="#">Free (e)Business Listing</a>
                <a href="#">(e)Business Cards</a>
                <a href="#">Business Plays</a>
            </div>';
            
            $mem_data .='<div data-role="navbar" class="ui-navbar" role="navigation">
				<ul class="ui-grid-b">
					<li class="ui-block-a"><a href="#" class="ui-link ui-btn">Call</a></li>
					<li class="ui-block-b"><a href="#" class="ui-link ui-btn">Text</a></li>
					<li class="ui-block-c"><a href="mailto:'.$mebRes[0]['bus_email'].'" class="ui-link ui-btn">Email</a></li>
				</ul>
			</div>';					
				
			$mem_data .= '</article>';			

      	$success = array("status" => 'true', 'data'=>$mem_data);
      } else {
      	$mem_data = '<article>No card available.</article>';
      	$success = array("status" => 'true', 'data'=>$mem_data);
      }
	} else if($action == 'GetCardDetail') {
		$mebId	=	$_REQUEST['card_id'];
		$sectionName	=	$_REQUEST['sectionName'];

		$semail	=	$_REQUEST['semail'];
		$sid	=	$_REQUEST['sid'];
		$sphone	=	$_REQUEST['sphone'];		
		
		$meb_fields = array("*");
		$meb_where  = "bus_id = ".$mebId." AND meb_status = 'Active'";
		$mebRes 	= $db->selectData(TBL_MEMBER." JOIN ".TBL_MEMBER_BUSINESS." ON meb_id = bus_meb_id",$meb_fields,$meb_where,$extra="",2);

		$mc_fields = array("*");
		$mc_where  = "shr_bus_id = ".$mebId." AND ( shr_meb_id = ".$sid." OR shr_rcv_email='".$semail."' OR shr_rcv_email='".$semail."' ) AND shr_status = 'Active'";
		$mcRes = $db->selectData(TBL_SHARE,$mc_fields,$mc_where,$extra="",2);		
		
		if(count($mebRes) > 0) {
			$logo_back_image='';
			if($mebRes[0]['bus_logo_file']!="") {	
				$fileNameExt = explode(".", $mebRes[0]['bus_logo_file']);
				$fileExt 	 = $fileNameExt[count($fileNameExt)-1];
				$allowedImgExts = array("gif","jpg","jpeg","png","GIF","JPG","JPEG","PNG");
				if(in_array($fileExt,$allowedImgExts)) {
					$profile_image = "<a class='logo_image_a' href='#popupParis".$mebRes[0]['bus_id']."' data-rel='popup' data-position-to='window' data-transition='fade' ><img class='popphoto logo_image' src='".UPLOAD_WWW_BUSINESS_FILE.$mebRes[0]['bus_id']."/".$mebRes[0]['bus_logo_file']."' /></a>";
					$profile_image .= "<div data-role='popup' id='popupParis".$mebRes[0]['bus_id']."' data-overlay-theme='b' data-theme='b' data-corners='false'><a href='#' data-rel='back' class='ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right'>Close</a><img class='popphoto' src='".UPLOAD_WWW_BUSINESS_FILE.$mebRes[0]['bus_id']."/".$mebRes[0]['bus_logo_file']."' style='max-height:512px;'></div>";
					$logo_back_image = UPLOAD_WWW_BUSINESS_FILE.$mebRes[0]['bus_id']."/".$mebRes[0]['bus_logo_file'];
				} else {
					$uploadFILEWWW = UPLOAD_WWW_BUSINESS_FILE;
					$filev = $uploadFILEWWW.$mebRes[0]['bus_id']."/".$mebRes[0]['bus_logo_file'];
					
					$profile_image = '<a href="#"><img class="popphoto" src="images/play-button.jpg" height="115" width="115" onclick="play_new_video(\''.$video_file.'\')" style="height: 115px; width: 160px;" /></a>';
				}
			} else {
				//$profile_image = "<a href='#' data-rel='popup' data-position-to='window' data-transition='fade' ><img class='popphoto' src='images/profile.png' height='115' width='115' /></a>";
				//$profile_image .= "<div data-role='popup' style='text-align: center;' id='popupParis".$mebRes[0]['bus_id']."' data-overlay-theme='b' data-theme='b' data-corners='false'><a href='#' data-rel='back' class='ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right'>Close</a><img class='popphoto' src='images/profile.png' style='width: 100%;'></div>";
			}

			if($mebRes[0]['bus_picture_file']!="") {	
				$fileNameExt = explode(".", $mebRes[0]['bus_picture_file']);
				$fileExt 	 = $fileNameExt[count($fileNameExt)-1];
				$allowedImgExts = array("gif","jpg","jpeg","png","GIF","JPG","JPEG","PNG");
				if(in_array($fileExt,$allowedImgExts)) {
					$profile_imagen = "<a href='#popupParisn".$mebRes[0]['bus_id']."' data-rel='popup' data-position-to='window' data-transition='fade' ><img class='popphoto' src='".UPLOAD_WWW_BUSINESS_FILE.$mebRes[0]['bus_id']."/".$mebRes[0]['bus_picture_file']."' height='115' width='115' /></a>";
					$profile_imagen .= "<div class='img_popup' style='text-align: center;' data-role='popup' id='popupParisn".$mebRes[0]['bus_id']."' data-overlay-theme='b' data-theme='b' data-corners='false'><a href='#' data-rel='back' class='ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right'>Close</a><img class='popphoto' src='".UPLOAD_WWW_BUSINESS_FILE.$mebRes[0]['bus_id']."/".$mebRes[0]['bus_picture_file']."'  style='max-height:512px;' ></div>";
				} else {
					//$profile_image = "<img class='popphoto' src='images/profile.png' height='115' width='115' />";
					$uploadFILEWWW = UPLOAD_WWW_BUSINESS_FILE;
					$filev = $uploadFILEWWW.$mebRes[0]['bus_id']."/".$mebRes[0]['bus_picture_file'];
					
					$profile_imagen = '<a href="#"><img class="popphoto" src="images/play-button.jpg" height="115" width="115" onclick="play_new_video(\''.$video_file.'\')" style="height: 115px; width: 160px;" /></a>';
				}
			} else {
				$profile_imagen = "<a href='#' data-rel='popup' data-position-to='window' data-transition='fade' ><img class='popphoto' src='images/profile.png'/></a>";
				//$profile_imagen .= "<div data-role='popup' style='text-align: center;' id='popupParisn".$mebRes[0]['bus_id']."' data-overlay-theme='b' data-theme='b' data-corners='false'><a href='#' data-rel='back' class='ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right'>Close</a><img class='popphoto' src='images/profile.png' style='width: 100%;' ></div>";
			}
			
			$mem_data ='<article id="content">';	
				$website = '';
				if($mebRes[0]['bus_website']!="") { 
					$httparr = array("http://","https://");
					$httptext = substr(trim($mebRes[0]['bus_website']),0,7);
					if ($httptext=="http://" || $httptext=="https:/" ) {
						$website = trim($mebRes[0]['bus_website']);
					} else {
						$website = "http://".trim($mebRes[0]['bus_website']);
					}	
				}
				if($website == '')
					$website = '#';				
				
				$mem_data = '';
				
				$pl_link = '';
				if($mebRes[0]['bus_video_file_1'] !='' ||$mebRes[0]['bus_video_file_2'] !='' ||$mebRes[0]['bus_video_file_3'] !='' ||$mebRes[0]['bus_video_file_4'] !='' || $mebRes[0]['bus_video_file_5'] !='') {
					$member_name = $mebRes[0]['meb_name'].' media';
      			$allowedImgExts = array("gif","jpg","jpeg","png","GIF","JPG","JPEG","PNG");
      			$play_list_pop = '<article><ul class="playlist_ul">';
					
					$select = '<select onchange="return change_video_light(this.value)" data-native-menu="true"><option value="">Media</option>';	      			
      			$sel_op = '';	
      			$play_list_pop = '';
					$play_list_popn = '';
      			
      			$play_list_popn_list = '<div class="all_list">';
      			$play_list_popn_listv = '';
   				$play_list_popn .= "<a href='#popupVideolist' data-rel='popup' data-position-to='window' data-transition='fade'>Media</a><div data-role='popup' id='popupVideolist' data-overlay-theme='b' data-theme='b' style='text-align: center;' data-corners='false'><a href='#' data-rel='back' class='ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right'>Close</a><div class='title_video'><h1>Videolist</h1></div><div class='playlist_all'>";        			
      			
      			for($v=1,$v1=0;$v<=5;$v++) {
      				if($mebRes[0]['bus_video_file_'.$v] != '' ) {
      					$v1++;
      					$fileNameExtn = explode(".", $mebRes[0]['bus_video_file_'.$v]);
      					$file_name = '';
							for($f=0;$f<count($fileNameExtn)-1;$f++) {
								$file_name .= $fileNameExtn[$f]; 				
      					}
      					$sel_op .= '<option value="'.$mebRes[0]['bus_id'].$v.'">'.$file_name.'</option>';
      					$play_list_popn_list .= '<span class="video_top_title"><a href="#" onclick="show_video_l(\''.$v.'\')" >Video '.$v1.'</a></span>';
      				}
      				
						if($mebRes[0]['bus_video_file_'.$v]) {
							$fileNameExt = explode(".", $mebRes[0]['bus_video_file_'.$v]);
							$fileExt 	 = $fileNameExt[count($fileNameExt)-1];
	
							if(in_array($fileExt,$allowedImgExts)) {
								$play_list_pop .= "<a href='#popupParis".$mebRes[0]['bus_id'].$v."' data-rel='popup' data-position-to='window' data-transition='fade' style='display: none' ><img class='popphoto' src='".UPLOAD_WWW_BUSINESS_FILE.$mebRes[0]['bus_id']."/".$mebRes[0]['bus_video_file_'.$v]."' height='41' width='70' /></a>";
								$play_list_pop .= "<div data-role='popup' d_type='photo' id='popupParis".$mebRes[0]['bus_id'].$v."' data-overlay-theme='b' data-theme='b' style='text-align: center;' data-corners='false'><a href='#' data-rel='back' class='ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right'>Close</a><img class='popphoto' src='".UPLOAD_WWW_BUSINESS_FILE.$mebRes[0]['bus_id']."/".$mebRes[0]['bus_video_file_'.$v]."' style='max-height:512px;'></div>";
								
								$play_list_popn_listv .= "<div id='video_".$v."' class='videos_l' style='display:none;'><div class='video_image_title'>".$file_name."</div><div class='video_image_shoe'><img class='popphoto' src='".UPLOAD_WWW_BUSINESS_FILE.$mebRes[0]['bus_id']."/".$mebRes[0]['bus_video_file_'.$v]."' style='max-height:512px;'></div></div>";
							} else {
								$uploadFILEWWW = UPLOAD_WWW_BUSINESS_FILE;

								$play_list_pop .= "<a href='#popupParis".$mebRes[0]['bus_id'].$v."' data-rel='popup' data-position-to='window' data-transition='fade' style='display: none' ><img src='images/video-icon.png' height='41' width='70' /></a>";
								$video_file = $uploadFILEWWW.$mebRes[0]['bus_id']."/".$mebRes[0]['bus_video_file_'.$v];
								
								$play_list_pop .= "<div data-role='popup' d_type='video' d_video='".$video_file."' id='popupParis".$mebRes[0]['bus_id'].$v."' data-overlay-theme='b' data-theme='b' style='text-align: center;' data-corners='false'><a href='#' data-rel='back' class='ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right'>Close</a>";
							
								$video = "file=".$uploadFILEWWW.$mebRes[0]['bus_id']."/".$mebRes[0]['bus_video_file_'.$v]."&;volume=80&amp;bufferlength=5&amp;logo.hide=false&amp;&;title=sleeve_length&amp;linktarget=_self";
							
								$play_list_pop .= '<object width="100%" height="250" data="player.swf" type="application/x-shockwave-flash"><param value="opaque" name="wmode"><param value="always" name="allowscriptaccess"><param value="all" name="allownetworking"><param value="'.$video.'" name="flashvars"><param name="allowFullScreen" value="true" /></object></div>';
								
								//$play_list_popn_listv .= "<div id='video_".$v."' class='videos_l' style='display:none;'><div class='video_image_title'>".$file_name.'</div><div class="video_image_shoe"><a href="#"><img src="images/play-button.jpg" width="100%" height="250" onclick="play_new_video(\''.$video_file.'\')" /></a></div></div>';
								$play_list_popn_listv .= "<div id='video_".$v."' class='videos_l' style='display:none;'><div class='video_image_title'>".$file_name.'</div><div class="video_image_shoe"><video id="player'.$v.'" width="100%" height="190" controls="controls"><source src="'.$video_file.'" type="video/'.$fileExt.'" /></video></div></div>';
							}
						}
					}
					$play_list_popn_list .= '</div>';
					$play_list_popn .= $play_list_popn_list.$play_list_popn_listv."</div></div>";	
					$play_list_popn .= '<script>function show_video_l(val) {  $(".videos_l").slideUp(); $("#video_"+val).slideDown(); } function play_new_video(vid) { window.plugins.videoPlayer.play(vid); }</script>';
					//$pl_link = 'href="playlist.html"';
					
					$pl_link ='';
					$pl_link .=$select.$sel_op.'</select>';
					$pl_link .= $play_list_pop.'<script>function change_video_light(val) { if($("#popupParis"+val).attr("d_type") == "video") { window.plugins.videoPlayer.play($("#popupParis"+val).attr("d_video")); } else { if(val != "") { $("#popupParis"+val).popup("open");} } }</script>';
					
					
				} else {
					$pl_link = '<a href="#nopl" data-rel="popup" data-position-to="window" data-transition="fade">No Media</a>';
					$play_list_popn = '<a href="#" >No Media</a>';
					$mem_data .='<div data-role="popup" id="nopl" data-overlay-theme="b" data-theme="b" style="max-width:350px;"><p>No record found</p></div>';
				}
				
				//for share option
				$share_box = '';
				$share_box .= '<a href="#share_popup" data-rel="popup" data-position-to="window" data-transition="fade" class="ui-link ui-btn">Share</a>';
				$share_box .= '<div data-role="popup" id="share_popup" data-overlay-theme="b" data-theme="b" style="max-width:350px;"><a href="#" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right">Close</a><article><section><h2>Share this card</h2><div id="share_error" style="display: none;"></div><div class="share_buttons"><div class="share_btn"><a href="#" id="sharebysms">Text</a><a href="#" id="sharebyemail">Email</a></div></div><form id="card_share_form" style="display: none;"><input type="hidden" value="'.$mebRes[0]['bus_meb_id'].'" name="mem_id" /><input type="hidden" value="'.$mebRes[0]['bus_id'].'" name="sub_id" /><input type="text" placeholder="Email" name="share_email" id="share_email" /><div class="but_login new_btn_h"><input type="submit" value="Share" /></div></form>';
				
				//share_by sms
				$shar_s_body = $mebRes[0]['meb_name'].' has shared a business card with you using IVQ (e) Mobile Services App. Please goto www.b2bms.net/downloadapp.html?card='.$mebRes[0]['bus_id'].'ANDtype=Bus to view the business card and download the IVQ App.';
				$share_box .= '<form id="card_share_form_sms" style="display: none;"><input type="hidden" value="'.$mebRes[0]['bus_meb_id'].'" name="mem_id" /><input type="hidden" value="'.$mebRes[0]['bus_id'].'" name="sub_id" /><input type="hidden" value="'.$shar_s_body.'" name="sms_body" id="sms_body" /><input type="text" placeholder="Phone" name="share_sms" id="share_sms" /><ul data-role="listview" data-inset="true" data-filter="true" data-filter-reveal="true" data-input="#share_sms" id="mylistsms"><li>test</li></ul><div class="but_login new_btn_h"><input type="submit" value="Share" /></div></form>';
				//share_by sms
				//share_by email
				$shar_e_body = 'subject=IVQ Mobile Message&body='.$mebRes[0]['meb_name'].' has shared a business card with you using IVQ (e) Mobile Services App. Please goto www.b2bms.net/downloadapp.html?card='.$mebRes[0]['bus_id'].'ANDtype=Bus to view the business card and download the IVQ App.';
				//\n\n<b>For Individual Subscriber\n\nIndividual subscribers get the following benefits:<ul><li>Search for directory and business services</li><li>Subscribe to your favorite directories</li><li>Receive special offers and coupons</li><li>Share business cards with colleagues and friends</li><li>Use location base mobile application​</li></ul>
				$share_box .= '<form id="card_share_form_email" style="display: none;"><input type="hidden" value="'.$mebRes[0]['bus_meb_id'].'" name="mem_id" /><input type="hidden" value="'.$mebRes[0]['bus_id'].'" name="sub_id" /><input type="hidden" value="'.$shar_e_body.'" name="email_body" id="email_body" /><input type="text" placeholder="Email" name="share_email2" id="share_email2" /><ul data-role="listview" data-inset="true" data-filter="true" data-filter-reveal="true" data-input="#share_email2" id="mylistemail"><li>test</li></ul><div class="but_login new_btn_h"><input type="submit" value="Share" /></div></form>';				
				//share_by email
				$share_box .= '</section></article></div>';	
				//for share option				
				
				$mem_data .='<div data-role="navbar" class="ui-navbar" role="navigation">
				<ul class="ui-grid-b tab-blue">';
					if($website!='#')
						$mem_data .='<li class="ui-block-a"><a onclick="window.open(\''.$website.'\', \'_blank\', \'location=yes\');" href="#" target="_blank" class="ui-link ui-btn">Website</a></li>';
					else
						$mem_data .='<li class="ui-block-a"><a href="#" target="_blank" class="ui-link ui-btn">Website</a></li>';
				$mem_data .='<li class="ui-block-b">'.$share_box.'</li>
					<li class="ui-block-c">'.$play_list_popn.'</li>
				</ul>
			</div>';
				//'.$pl_link.'
				//<a '.$pl_link.' class="ui-link ui-btn">Playlist</a>
				
				//$mem_data .= '<section class="inner-details"><article>'.$profile_image.'<div class="bus_company">'.$mebRes[0]['bus_company'].'</div><div class="bus_name">'.$mebRes[0]['bus_name'].'</div><div class="bus_title">'.$mebRes[0]['bus_title'].'</div><div class="bus_address">'.$mebRes[0]['bus_address1'].' <br>'.$mebRes[0]['bus_city'];
				$mem_data .= '<section class="inner-details"><article>'.$profile_imagen.'<div class="bus_name">'.$mebRes[0]['bus_name'].' '.$mebRes[0]['bus_lname'].'</div><div class="bus_title">'.$mebRes[0]['bus_title'].'</div><div class="bus_company">'.$mebRes[0]['bus_company'].'</div>';
					$state_fields = array("sta_name");
					$state_where  = "sta_id = '".$mebRes[0]['bus_sta_id']."'";
					$staRes = $db->selectData(TBL_STATE,$state_fields,$state_where,$extra="",1);
					//$mem_data .= ', '.$staRes[0]["sta_name"];	
				//$mem_data .= '<div class="bus_address"></div>';
				
				$mem_data .= '<div class="detail_generel_info"><a data-rel="popup" data-position-to="window" data-transition="pop" href="#generalinfo"><strong>More Information</strong></a></div>';	

				$mem_card_btn = '';	
				if(count($mcRes) <= 0) {
					$mem_card_btn .= '<div class="add_to_my_card"><a href="#" onclick="return addtomycard(\''.$mebRes[0]['bus_id'].'\')" load="yes" data-ajax="false">Save</a></div>';
				}	

				$mem_data .= '<div data-role="popup" class="popup_main_wrapper" id="generalinfo" data-overlay-theme="b" data-theme="b" data-corners="pop">
                                <a href="#" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right">Close</a>
                                <div class="popup_inner">
                                    <div class="popup_header">Information</div>
                                    <div class="popup_content" style="padding: 0;">
                                        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="info-table">
		                                    <tbody>
		                                    <tr>
		                                        <td width="25%" class="blue">COMPANY NAME</td>
		                                        <td width="25%">'.$mebRes[0]['bus_company'].'</td>
		                                    </tr>
		                                    <tr>
		                                        <td width="25%" class="blue">ADDRESS</td>
		                                        <td width="25%">'.$mebRes[0]['bus_address1'].'</td>
		                                    </tr>
		                                    <tr>
		                                        <td width="25%" class="blue">CITY</td>
		                                        <td width="25%">'.$mebRes[0]['bus_city'].'</td>
		                                    </tr>
		                                    <tr>
		                                        <td width="25%" class="blue">STATE</td>
		                                        <td width="25%">'.$staRes[0]["sta_name"].'</td>
		                                    </tr>
		                                    <tr>
		                                        <td width="25%" class="blue">ZIP CODE</td>
		                                        <td width="25%">'.$mebRes[0]['bus_zip'].'</td>
		                                    </tr>
		                                    <tr>
		                                        <td width="25%" class="blue">WEBSITE</td>
		                                        <td width="25%"><a href="#">'.$mebRes[0]['bus_website'].'</a></td>
		                                    </tr>
		                                    <tr>
		                                        <td width="25%" class="blue">OFFICE PHONE</td>
		                                        <td width="25%">'.$mebRes[0]['bus_phone'].'</td>
		                                    </tr>
		                                    <tr>
		                                        <td width="25%" class="blue">FAX NUMBER</td>
		                                        <td width="25%">'.$mebRes[0]['bus_fax'].'</td>
		                                    </tr>
		                                    <tr>
		                                        <td width="25%" class="blue">EMAIL ADDRESS</td>
		                                        <td width="25%">'.$mebRes[0]['bus_email'].'</td>
		                                    </tr>
		                                    <tr>
		                                        <td width="25%" class="blue">FIRST NAME</td>
		                                        <td width="25%">'.$mebRes[0]['bus_name'].'</td>
		                                    </tr>
		                                    <tr>
		                                        <td width="25%" class="blue">TITLE</td>
		                                        <td width="25%">'.$mebRes[0]['bus_title'].'</td>
		                                    </tr>
		                                	</tbody>
                                		</table>
                                    </div>
                                </div>
                            </div>';	

				$mem_data .= '</article></section>';
				
				/*
				$mem_data .='<div class="bottom_three_line">
                <a href="#">Free (e)Business Listing</a>
                <a href="#">(e)Business Cards</a>
                <a href="#">Business Plays</a>
            </div>';
            */
            // onclick="return call_this(\'+1-800-555-1234\')"
            //for send sms 
            	$sms = '';
            	$sms .= $mebRes[0]['bus_phone'].'?body=';
            //
            $mem_data .='<div data-role="navbar" class="ui-navbar" role="navigation">
				<ul class="ui-grid-b tab-green">
					<li class="ui-block-a"><a href="tel:'.$mebRes[0]['bus_phone'].'" class="ui-link ui-btn">Call</a></li>
					<li class="ui-block-b"><a href="sms:'.$sms.'" class="ui-link ui-btn">Text</a></li>
					<li class="ui-block-c"><a href="mailto:'.$mebRes[0]['bus_email'].'" class="ui-link ui-btn">Email</a></li>
				</ul>
			</div>';					
				
			$mem_data .= '</article>';			

      	$success = array("status" => 'true', 'data'=>$mem_data, 'card_btn' => $mem_card_btn, 'log_image' => $logo_back_image);
      } else {
      	$mem_data = '<article>No business card available.</article>';
      	$success = array("status" => 'true', 'data'=>$mem_data);
      }
	} else if($action == 'GetCardDetailSub') {
		$mebId	=	$_REQUEST['card_id'];
		$sectionName	=	$_REQUEST['sectionName'];

		$semail	=	$_REQUEST['semail'];
		$sid	=	$_REQUEST['sid'];
		$sphone	=	$_REQUEST['sphone'];		
		
		$meb_fields = array("*");
		$meb_where  = "bus_id = ".$mebId." AND bus_status = 'Active'";
		$mebRes 	= $db->selectData(TBL_MEMBER_SUBSCRIBERS." JOIN ".TBL_MEMBER_BUSINESS_SUB." ON sub_id = bus_sub_id",$meb_fields,$meb_where,$extra="",2);

		$mc_fields = array("*");
		$mc_where  = "shr_bus_id = ".$mebId." AND ( shr_meb_id = ".$sid." OR shr_rcv_email='".$semail."' OR shr_rcv_email='".$semail."' ) AND shr_status = 'Active'";
		$mcRes = $db->selectData(TBL_SHARE,$mc_fields,$mc_where,$extra="",2);		
		
		if(count($mebRes) > 0) {
			$logo_back_image='';
			if($mebRes[0]['bus_logo_file']!="") {	
				$fileNameExt = explode(".", $mebRes[0]['bus_logo_file']);
				$fileExt 	 = $fileNameExt[count($fileNameExt)-1];
				$allowedImgExts = array("gif","jpg","jpeg","png","GIF","JPG","JPEG","PNG");
				if(in_array($fileExt,$allowedImgExts)) {
					$profile_image = "<a class='logo_image_a' href='#popupParis".$mebRes[0]['bus_id']."' data-rel='popup' data-position-to='window' data-transition='fade' ><img class='popphoto logo_image' src='".UPLOAD_WWW_BUSINESS_FILE."Business_Sub/".$mebRes[0]['bus_id']."/".$mebRes[0]['bus_logo_file']."' /></a>";
					$profile_image .= "<div data-role='popup' id='popupParis".$mebRes[0]['bus_id']."' data-overlay-theme='b' data-theme='b' data-corners='false'><a href='#' data-rel='back' class='ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right'>Close</a><img class='popphoto' src='".UPLOAD_WWW_BUSINESS_FILE."Business_Sub/".$mebRes[0]['bus_id']."/".$mebRes[0]['bus_logo_file']."' style='max-height:512px;' ></div>";
					$logo_back_image = UPLOAD_WWW_BUSINESS_FILE."Business_Sub/".$mebRes[0]['bus_id']."/".$mebRes[0]['bus_logo_file'];
				} else {
					//$profile_image = "<img class='popphoto' src='images/profile.png' height='115' width='115' />";
					$uploadFILEWWW = UPLOAD_WWW_BUSINESS_FILE."Business_Sub/";
					$filev = $uploadFILEWWW.$mebRes[0]['bus_id']."/".$mebRes[0]['bus_logo_file'];
					
					$profile_image = '<a href="#"><img class="popphoto" src="images/play-button.jpg" height="115" width="115" onclick="play_new_video(\''.$video_file.'\')" style="height: 115px; width: 160px;" /></a>';
				}
			} else {
				//$profile_image = "<a href='#' data-rel='popup' data-position-to='window' data-transition='fade' ><img class='popphoto' src='images/profile.png' height='115' width='115' /></a>";
				//$profile_image .= "<div data-role='popup' style='text-align: center;' id='popupParis".$mebRes[0]['bus_id']."' data-overlay-theme='b' data-theme='b' data-corners='false'><a href='#' data-rel='back' class='ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right'>Close</a><img class='popphoto' src='images/profile.png' style='width: 100%;'></div>";
			}

			if($mebRes[0]['bus_picture_file']!="") {	
				$fileNameExt = explode(".", $mebRes[0]['bus_picture_file']);
				$fileExt 	 = $fileNameExt[count($fileNameExt)-1];
				$allowedImgExts = array("gif","jpg","jpeg","png","GIF","JPG","JPEG","PNG");
				if(in_array($fileExt,$allowedImgExts)) {
					$profile_imagen = "<a href='#popupParisn".$mebRes[0]['bus_id']."' data-rel='popup' data-position-to='window' data-transition='fade' ><img class='popphoto' src='".UPLOAD_WWW_BUSINESS_FILE."Business_Sub/".$mebRes[0]['bus_id']."/".$mebRes[0]['bus_picture_file']."' /></a>";
					$profile_imagen .= "<div style='text-align: center; width: auto !important;' data-role='popup' id='popupParisn".$mebRes[0]['bus_id']."' data-overlay-theme='b' data-theme='b' data-corners='false'><a href='#' data-rel='back' class='ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right'>Close</a><img class='popphoto' src='".UPLOAD_WWW_BUSINESS_FILE."Business_Sub/".$mebRes[0]['bus_id']."/".$mebRes[0]['bus_picture_file']."' style='max-height:512px;' ></div>";
				} else {
					//$profile_image = "<img class='popphoto' src='images/profile.png' height='115' width='115' />";
					$uploadFILEWWW = UPLOAD_WWW_BUSINESS_FILE."Business_Sub/";
					$filev = $uploadFILEWWW.$mebRes[0]['bus_id']."/".$mebRes[0]['bus_picture_file'];
					
					$profile_imagen = '<a href="#"><img class="popphoto" src="images/play-button.jpg" height="115" width="115" onclick="play_new_video(\''.$video_file.'\')" style="height: 115px; width: 160px;" /></a>';
				}
			} else {
				$profile_imagen = "<a href='#' data-rel='popup' data-position-to='window' data-transition='fade' ><img class='popphoto' src='images/profile.png' /></a>";
				//$profile_imagen .= "<div data-role='popup' style='text-align: center;' id='popupParisn".$mebRes[0]['bus_id']."' data-overlay-theme='b' data-theme='b' data-corners='false'><a href='#' data-rel='back' class='ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right'>Close</a><img class='popphoto' src='images/profile.png' style='width: 100%;' ></div>";
			}
			
			$mem_data ='<article id="content">';	
				$website = '';
				if($mebRes[0]['bus_website']!=""){ 
					$httparr = array("http://","https://");
					$httptext = substr(trim($mebRes[0]['bus_website']),0,7);
					if ($httptext=="http://" || $httptext=="https:/" ) {
						$website = trim($mebRes[0]['bus_website']);
					} else {
						$website = "http://".trim($mebRes[0]['bus_website']);
					}	
				}
				if($website == '')
					$website = '#';
				
				$mem_data = '';
				
				$pl_link = '';
				if($mebRes[0]['bus_video_file_1'] !='' ||$mebRes[0]['bus_video_file_2'] !='' ||$mebRes[0]['bus_video_file_3'] !='' ||$mebRes[0]['bus_video_file_4'] !='' || $mebRes[0]['bus_video_file_5'] !='') {
					$member_name = $mebRes[0]['meb_name'].' media';
      			$allowedImgExts = array("gif","jpg","jpeg","png","GIF","JPG","JPEG","PNG");
      			$play_list_pop = '<article><ul class="playlist_ul">';
					
					$select = '<select onchange="return change_video_light(this.value)"><option value="">Media</option>';	      			
      			$sel_op = '';	
      			$play_list_pop = '';
      			$play_list_popn = '';
      			
      			$play_list_popn_list = '<div class="all_list">';
      			$play_list_popn_listv = '';
   
					$play_list_popn .= "<a href='#popupVideolist' data-rel='popup' data-position-to='window' data-transition='fade'>Media</a><div data-role='popup' id='popupVideolist' data-overlay-theme='b' data-theme='b' style='text-align: center;' data-corners='false'><a href='#' data-rel='back' class='ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right'>Close</a><div class='title_video'><h1>Videolist</h1></div><div class='playlist_all'>";      			
      			
      			for($v=1,$v1=0;$v<=5;$v++) {
      				if($mebRes[0]['bus_video_file_'.$v] != '' ) {
      					$v1++;
      					$fileNameExtn = explode(".", $mebRes[0]['bus_video_file_'.$v]);
      					$file_name = '';
							for($f=0;$f<count($fileNameExtn)-1;$f++) {
								$file_name .= $fileNameExtn[$f];
      					}
      					$sel_op .= '<option value="'.$mebRes[0]['bus_id'].$v.'">'.$file_name.'</option>';
      					
      					$play_list_popn_list .= '<span class="video_top_title"><a href="#" onclick="show_video_l(\''.$v.'\')" >Video '.$v1.'</a></span>';
      				}
      				
						if($mebRes[0]['bus_video_file_'.$v]) {
							$fileNameExt = explode(".", $mebRes[0]['bus_video_file_'.$v]);
							$fileExt 	 = $fileNameExt[count($fileNameExt)-1];
	
							if(in_array($fileExt,$allowedImgExts)) {
								$play_list_pop .= "<a href='#popupParis".$mebRes[0]['bus_id'].$v."' data-rel='popup' data-position-to='window' data-transition='fade' style='display: none' ><img class='popphoto' src='".UPLOAD_WWW_BUSINESS_FILE.$mebRes[0]['bus_id']."/".$mebRes[0]['bus_video_file_'.$v]."' height='41' width='70' /></a>";
								$play_list_pop .= "<div data-role='popup' id='popupParis".$mebRes[0]['bus_id'].$v."' data-overlay-theme='b' data-theme='b' style='text-align: center;' data-corners='false'><a href='#' data-rel='back' class='ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right'>Close</a><img class='popphoto' src='".UPLOAD_WWW_BUSINESS_FILE.$mebRes[0]['bus_id']."/".$mebRes[0]['bus_video_file_'.$v]."' style='max-height:512px;'></div>";

								$play_list_popn_listv .= "<div id='video_".$v."' class='videos_l' style='display:none;'><div class='video_image_title'>".$file_name."</div><div class='video_image_shoe'><img class='popphoto' src='".UPLOAD_WWW_BUSINESS_FILE.$mebRes[0]['bus_id']."/".$mebRes[0]['bus_video_file_'.$v]."' style='max-height:512px;'></div></div>";								
								
							} else {
								$uploadFILEWWW = UPLOAD_WWW_BUSINESS_FILE."Business_Sub/";

								$video_file = $uploadFILEWWW.$mebRes[0]['bus_id']."/".$mebRes[0]['bus_video_file_'.$v];
								
								$play_list_pop .= "<a href='#popupParis".$mebRes[0]['bus_id'].$v."' data-rel='popup' data-position-to='window' data-transition='fade' style='display: none' ><img src='images/video-icon.png' height='41' width='70' /></a>";
								$play_list_pop .= "<div data-role='popup' d_type='video' d_video='".$video_file."' id='popupParis".$mebRes[0]['bus_id'].$v."' data-overlay-theme='b' data-theme='b' style='text-align: center;' data-corners='false'><a href='#' data-rel='back' class='ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right'>Close</a>";
							
								$video = "file=".$uploadFILEWWW.$mebRes[0]['bus_id']."/".$mebRes[0]['bus_video_file_'.$v]."&;volume=80&amp;bufferlength=5&amp;logo.hide=false&amp;&;title=sleeve_length&amp;linktarget=_self";
							
								$play_list_pop .= "<object width='100%' height='250' data='player.swf' type='application/x-shockwave-flash'><param value='opaque' name='wmode'><param value='always' name='allowscriptaccess'><param value='all' name='allownetworking'><param value='".$video."' name='flashvars'><param name='allowFullScreen' value='true' /></object></div>";
								//$play_list_popn_listv .= "<div id='video_".$v."' class='videos_l' style='display:none;'><div class='video_image_title'>".$file_name."</div><div class='video_image_shoe'><object width='100%' height='250' data='player.swf' type='application/x-shockwave-flash'><param value='opaque' name='wmode'><param value='always' name='allowscriptaccess'><param value='all' name='allownetworking'><param value='".$video."' name='flashvars'><param name='allowFullScreen' value='true' /></object></div></div>";

								//$play_list_popn_listv .= "<div id='video_".$v."' class='videos_l' style='display:none;'><div class='video_image_title'>".$file_name.'</div><div class="video_image_shoe"><a href="#"><img src="images/play-button.jpg" width="100%" height="250" onclick="play_new_video(\''.$video_file.'\')" /></a></div></div>';
								$play_list_popn_listv .= "<div id='video_".$v."' class='videos_l' style='display:none;'><div class='video_image_title'>".$file_name.'</div><div class="video_image_shoe"><video id="player'.$v.'" width="100%" height="190" controls="controls"><source src="'.$video_file.'" type="video/'.$fileExt.'" /></video></div></div>';
							}
						}
					}
					$play_list_popn_list .= '</div>';
					$play_list_popn .= $play_list_popn_list.$play_list_popn_listv."</div></div>";	
					$play_list_popn .= '<script>function show_video_l(val) {  $(".videos_l").slideUp(); $("#video_"+val).slideDown(); } function play_new_video(vid) { window.plugins.videoPlayer.play(vid); }</script>';			
				
					//$pl_link = 'href="playlist.html"';show_video_l
					
					$pl_link ='';
					$pl_link .=$select.$sel_op.'</select>';
					
					
					$pl_link .= $play_list_pop.'<script>function change_video_light(val) { if($("#popupParis"+val).attr("d_type") == "video") { window.plugins.videoPlayer.play($("#popupParis"+val).attr("d_video")); } else { if(val != "") { $("#popupParis"+val).popup("open");} } }</script>';
					
					
				} else {
					$pl_link = '<a href="#nopl" data-rel="popup" data-position-to="window" data-transition="pop">No Media</a>';
					$play_list_popn = '<a href="#">No Media</a>';
					$mem_data .='<div data-role="popup" id="nopl" data-overlay-theme="b" data-theme="b" style="max-width:350px;"><p>No record found</p></div>';
				}
				
				//for share option
				$share_box = '';
				$share_box .= '<a href="#share_popup" data-rel="popup" data-position-to="window" data-transition="pop" class="ui-link ui-btn">Share</a>';
				$share_box .= '<div data-role="popup" id="share_popup" data-overlay-theme="b" data-theme="b" style="max-width:350px;"><a href="#" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right">Close</a><article><section><h2>Share this card</h2><div id="share_error" style="display: none;"></div><div class="share_buttons"><div class="share_btn"><a href="#" id="sharebysms">Text</a><a href="#" id="sharebyemail">Email</a></div></div><form id="card_share_form" style="display: none;"><input type="hidden" value="'.$mebRes[0]['bus_meb_id'].'" name="mem_id" /><input type="hidden" value="'.$mebRes[0]['bus_id'].'" name="sub_id" /><input type="text" placeholder="Email" name="share_email" id="share_email" /><div class="but_login new_btn_h"><input type="submit" value="Share" /></div></form>';
				
				//share_by sms
				$shar_s_body = $mebRes[0]['sub_name'].' has shared a business card with you using IVQ (e) Mobile Services App. Please goto www.b2bms.net/downloadapp.html?card='.$mebRes[0]['bus_id'].'ANDtype=Sub to view the business card and download the IVQ App.';
				//.$mebRes[0]['bus_title'].' with you';
				
				$share_box .= '<form id="card_share_form_sms" style="display: none;"><input type="hidden" value="member" name="member_type" /><input type="hidden" value="'.$mebRes[0]['bus_sub_id'].'" name="mem_id" /><input type="hidden" value="'.$mebRes[0]['bus_id'].'" name="sub_id" /><input type="hidden" value="'.$shar_s_body.'" name="sms_body" id="sms_body" /><input type="text" placeholder="Phone" name="share_sms" id="share_sms" /><ul data-role="listview" data-inset="true" data-filter="true" data-filter-reveal="true" data-input="#share_sms" id="mylistsms"><li>test</li></ul><div class="but_login new_btn_h"><input type="submit" value="Share" /></div></form>';
				//share_by sms
				//share_by email
				//'.$mebRes[0]['bus_title'].'
				$shar_e_body = 'subject=B2b Mobile Message&body='.$mebRes[0]['sub_name'].' has shared a business card with you using IVQ (e) Mobile Services App. Please goto www.b2bms.net/downloadapp.html?card='.$mebRes[0]['bus_id'].'ANDtype=Sub to view the business card and download the IVQ App.';
				//\n\n<b>For Individual Subscriber\n\nIndividual subscribers get the following benefits:<ul><li>Search for directory and business services</li><li>Subscribe to your favorite directories</li><li>Receive special offers and coupons</li><li>Share business cards with colleagues and friends</li><li>Use location base mobile application​</li></ul>
				$share_box .= '<form id="card_share_form_email" style="display: none;"><input type="hidden" value="sub" name="member_type" /><input type="hidden" value="'.$mebRes[0]['bus_sub_id'].'" name="mem_id" /><input type="hidden" value="'.$mebRes[0]['bus_id'].'" name="sub_id" /><input type="hidden" value="'.$shar_e_body.'" name="email_body" id="email_body" /><input type="text" placeholder="Email" name="share_email2" id="share_email2" /><ul data-role="listview" data-inset="true" data-filter="true" data-filter-reveal="true" data-input="#share_email2" id="mylistemail"><li>test</li></ul><div class="but_login new_btn_h"><input type="submit" value="Share" /></div></form>';
				//share_by email

				$share_box .= '</section></article></div>';				
					
				//for share option
				//'.$pl_link.'				
				
				$mem_data .='<div data-role="navbar" class="ui-navbar" role="navigation">
				<ul class="ui-grid-b tab-blue">';
					if($website!='#')
						$mem_data .='<li class="ui-block-a"><a onclick="window.open(\''.$website.'\', \'_blank\', \'location=yes\');" href="#" target="_blank" class="ui-link ui-btn">Website</a></li>';
					else
						$mem_data .='<li class="ui-block-a"><a href="#" target="_blank" class="ui-link ui-btn">Website</a></li>';

				$mem_data .='<li class="ui-block-b">'.$share_box.'</li>
					<li class="ui-block-c">'.$play_list_popn.'</li>
				</ul>
			</div>';

				//<a '.$pl_link.' class="ui-link ui-btn">Playlist</a>
				
				//$mem_data .= '<section class="inner-details"><article>'.$profile_image.'<div class="bus_company">'.$mebRes[0]['bus_company'].'</div><div class="bus_name">'.$mebRes[0]['bus_name'].'</div><div class="bus_title">'.$mebRes[0]['bus_title'].'</div><div class="bus_address">'.$mebRes[0]['bus_address1'].' <br>'.$mebRes[0]['bus_city'];
			$mem_data .= '<section class="inner-details"><article>'.$profile_imagen.'<div class="bus_name">'.$mebRes[0]['bus_name'].' '.$mebRes[0]['bus_lname'].'</div><div class="bus_title">'.$mebRes[0]['bus_title'].'</div><div class="bus_company">'.$mebRes[0]['bus_company'].'</div>';
					$state_fields = array("sta_name");
					$state_where  = "sta_id = '".$mebRes[0]['bus_sta_id']."'";
					$staRes = $db->selectData(TBL_STATE,$state_fields,$state_where,$extra="",1);
					//$mem_data .= ', '.$staRes[0]["sta_name"];	
				//$mem_data .= '<div class="bus_address"></div>';

				//if($profile_image != '') {
					//$mem_data .= $profile_image;	
				//} 
				
				$mem_data .= '<div class="detail_generel_info"><a data-rel="popup" data-position-to="window" data-transition="pop" href="#generalinfo"><strong>More Information</strong></a></div>';	

				$mem_card_btn = '';	
				if(count($mcRes) <= 0) {
					//$mem_card_btn .= '<div class="add_to_my_card"><div class="b2b_but"><a href="#" onclick="return addtomycard(\''.$mebRes[0]['bus_id'].'\')" load="yes" data-ajax="false">Save Cards</a></div></div>';
					$mem_card_btn .= '<div class="add_to_my_card"><a href="#" onclick="return addtomycard(\''.$mebRes[0]['bus_id'].'\')" load="yes" data-ajax="false">Save</a></div>';
				}	

				$mem_data .= '<div data-role="popup" class="popup_main_wrapper" id="generalinfo" data-overlay-theme="b" data-theme="b" data-corners="pop">
                                <a href="#" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right">Close</a>
                                <div class="popup_inner">
                                    <div class="popup_header">Information</div>
                                    <div class="popup_content" style="padding: 0;">
                                        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="info-table">
		                                    <tbody>
		                                    <tr>
		                                        <td width="25%" class="blue">COMPANY NAME</td>
		                                        <td width="25%">'.$mebRes[0]['bus_company'].'</td>
		                                    </tr>
		                                    <tr>
		                                        <td width="25%" class="blue">ADDRESS</td>
		                                        <td width="25%">'.$mebRes[0]['bus_address1'].'</td>
		                                    </tr>
		                                    <tr>
		                                        <td width="25%" class="blue">CITY</td>
		                                        <td width="25%">'.$mebRes[0]['bus_city'].'</td>
		                                    </tr>
		                                    <tr>
		                                        <td width="25%" class="blue">STATE</td>
		                                        <td width="25%">'.$staRes[0]["sta_name"].'</td>
		                                    </tr>
		                                    <tr>
		                                        <td width="25%" class="blue">ZIP CODE</td>
		                                        <td width="25%">'.$mebRes[0]['bus_zip'].'</td>
		                                    </tr>
		                                    <tr>
		                                        <td width="25%" class="blue">WEBSITE</td>
		                                        <td width="25%"><a href="#">'.$mebRes[0]['bus_website'].'</a></td>
		                                    </tr>
		                                    <tr>
		                                        <td width="25%" class="blue">OFFICE PHONE</td>
		                                        <td width="25%">'.$mebRes[0]['bus_phone'].'</td>
		                                    </tr>
		                                    <tr>
		                                        <td width="25%" class="blue">FAX NUMBER</td>
		                                        <td width="25%">'.$mebRes[0]['bus_fax'].'</td>
		                                    </tr>
		                                    <tr>
		                                        <td width="25%" class="blue">EMAIL ADDRESS</td>
		                                        <td width="25%">'.$mebRes[0]['bus_email'].'</td>
		                                    </tr>
		                                    <tr>
		                                        <td width="25%" class="blue">FIRST NAME</td>
		                                        <td width="25%">'.$mebRes[0]['bus_name'].'</td>
		                                    </tr>
		                                    <tr>
		                                        <td width="25%" class="blue">TITLE</td>
		                                        <td width="25%">'.$mebRes[0]['bus_title'].'</td>
		                                    </tr>
		                                	</tbody>
                                		</table>
                                    </div>
                                </div>
                            </div>';

				$mem_data .= '</article></section>';
				
				/*
				$mem_data .='<div class="bottom_three_line">
                <a href="#">Free (e)Business Listing</a>
                <a href="#">(e)Business Cards</a>
                <a href="#">Business Plays</a>
            </div>';
            */
            // onclick="return call_this(\'+1-800-555-1234\')"
            //for send sms 
            	$sms = '';
            	$sms .= $mebRes[0]['bus_phone'].'?body=';
            //
            $mem_data .='<div data-role="navbar" class="ui-navbar" role="navigation">
				<ul class="ui-grid-b  tab-green">
					<li class="ui-block-a"><a href="tel:'.$mebRes[0]['bus_phone'].'" class="ui-link ui-btn">Call</a></li>
					<li class="ui-block-b"><a href="sms:'.$sms.'" class="ui-link ui-btn">Text</a></li>
					<li class="ui-block-c"><a href="mailto:'.$mebRes[0]['bus_email'].'" class="ui-link ui-btn">Email</a></li>
				</ul>
			</div>';					
				
			$mem_data .= '</article>';			

      	$success = array("status" => 'true', 'data'=>$mem_data, 'card_btn' => $mem_card_btn, 'log_image' => $logo_back_image);
      } else {
      	$mem_data = '<article>No business card available.</article>';
      	$success = array("status" => 'true', 'data'=>$mem_data);
      }
	} else if($action == 'GetMemberPlaylist') {
      	$mebId	=	$_REQUEST['meb_id'];
			$sectionName	=	$_REQUEST['sectionName'];
		
			$meb_fields = array("*");
			$meb_where  = "bus_id = ".$mebId." AND meb_status = 'Active'";
			$mebRes 	= $db->selectData(TBL_MEMBER." JOIN ".TBL_MEMBER_BUSINESS." ON meb_id = bus_meb_id",$meb_fields,$meb_where,$extra="",2);

			$member_name = $mebRes[0]['meb_name'].' media';
      	
      	$allowedImgExts = array("gif","jpg","jpeg","png","GIF","JPG","JPEG","PNG");
      	
			$play_list_pop = '<article><ul class="playlist_ul">';
			
			if(count($mebRes) > 0) {
				for($r=0;$r<count($mebRes);$r++) {
					if($mebRes[$r]['bus_video_file_1'] !='' ||$mebRes[$r]['bus_video_file_2'] !='' ||$mebRes[$r]['bus_video_file_3'] !='' ||$mebRes[$r]['bus_video_file_4'] !='' || $mebRes[$r]['bus_video_file_5'] !=''){
						$play_list_pop .= '<div class="serviceTxtnew">'.$mebRes[$r]['bus_title'].' play list</div>';
					} else {
						continue;
					}
				
				$play_list_pop .= '<select onchange="return change_video(this.value)">';
				for($v1=1;$v1<=5;$v1++) {
					if($mebRes[$r]['bus_video_file_'.$v1]) {
						$play_list_pop .= '<option value="'.$mebRes[$r]['bus_id'].$v1.'">'.$mebRes[$r]['bus_video_file_'.$v1].'</option>';
					}
				}
				$play_list_pop .= '</select>';
			
				for($v=1;$v<=5;$v++) {
					if($mebRes[$r]['bus_video_file_'.$v]) {
						$fileNameExt = explode(".", $mebRes[$r]['bus_video_file_'.$v]);
						$fileExt 	 = $fileNameExt[count($fileNameExt)-1];

						if($v==1)
							$dis_non = '';
						else 
							$dis_non = 'style="display: none;"';							
	
					if(in_array($fileExt,$allowedImgExts)) {
							$play_list_pop .= "<li ".$dis_non." id='livideo".$mebRes[$r]['bus_id'].$v."'><a href='#popupParis".$mebRes[$r]['bus_id'].$v."' data-rel='popup' data-position-to='window' data-transition='fade' ><img class='popphoto' src='".UPLOAD_WWW_BUSINESS_FILE.$mebRes[$r]['bus_id']."/".$mebRes[$r]['bus_video_file_'.$v]."' height='41' width='70' /></a></li>";
							$play_list_pop .= "<div data-role='popup' id='popupParis".$mebRes[$r]['bus_id'].$v."' data-overlay-theme='b' data-theme='b' style='text-align: center;' data-corners='false'><a href='#' data-rel='back' class='ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right'>Close</a><img class='popphoto' src='".UPLOAD_WWW_BUSINESS_FILE.$mebRes[$r]['bus_id']."/".$mebRes[$r]['bus_video_file_'.$v]."' style='max-height:512px;'></div>";
					} else {
							$uploadFILEWWW = UPLOAD_WWW_BUSINESS_FILE;

							$play_list_pop .= "<li ".$dis_non." id='livideo".$mebRes[$r]['bus_id'].$v."'><a href='#popupvideo".$mebRes[$r]['bus_id'].$v."' data-rel='popup' data-position-to='window' data-transition='fade' ><img src='images/video-icon.png' height='41' width='70' /></a></li>";
							$play_list_pop .= "<div data-role='popup' id='popupvideo".$mebRes[$r]['bus_id'].$v."' data-overlay-theme='b' data-theme='b' style='text-align: center;' data-corners='false'><a href='#' data-rel='back' class='ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right'>Close</a>";
							
							$video = "file=".$uploadFILEWWW.$mebRes[$r]['bus_id']."/".$mebRes[$r]['bus_video_file_'.$v]."&;volume=80&amp;bufferlength=5&amp;logo.hide=false&amp;&;title=sleeve_length&amp;linktarget=_self";
							
							$play_list_pop .= '<object width="100%" height="250" data="player.swf" type="application/x-shockwave-flash"><param value="opaque" name="wmode"><param value="always" name="allowscriptaccess"><param value="all" name="allownetworking"><param value="'.$video.'" name="flashvars"><param name="allowFullScreen" value="true" /></object></div>';
						}
				}
			}	
			}
				$play_list_pop .= '<script>function change_video(val) { $(".playlist_ul li").hide(); $("#livideo"+val).show(); }</script>';
			} else {
				$play_list_pop .= '<li>No record found</li>';
			}
			$play_list_pop .= '<ul></article>';		   	
      	$success = array("status" => 'true', 'data' => $play_list_pop, 'mem_name' => $member_name);
      	
      } else if($action == 'ShareCard') {
      	$mebId			=	$_REQUEST['mem_id'];
			$sub_id			=	$_REQUEST['sub_id'];
			$subc_id			=	$_REQUEST['subc_id'];
			
			$share_email	=	trim($_REQUEST['share_email']);

			$meb_fields = array("*");
			$meb_where  = "meb_username = '".$share_email."' AND meb_status = 'Active'";
			$mebRes 	= $db->selectData(TBL_MEMBER,$meb_fields,$meb_where,$extra="",2);
			if(true) {
				//count($mebRes) > 0
				$sub_fields = array("*");
				$sub_where  = "shr_rcv_email = '".$share_email."' AND shr_meb_id = ".$subc_id." AND shr_bus_id = ".$sub_id." AND shr_status = 'Active'";
				$subRes 	= $db->selectData(TBL_SHARE,$sub_fields,$sub_where,$extra="",2);	
				
				$s_fields = array("*");
				$s_where  = "sub_id = ".$subc_id." AND sub_status = 'Active'";
				$sRes 	= $db->selectData(TBL_MEMBER_SUBSCRIBERS,$s_fields,$s_where,$extra="",2);

				$state_fields = array("sta_name");
				$state_where  = "sta_id = '".$sRes[0]['bus_sta_id']."'";
				$staRes = $db->selectData(TBL_STATE,$state_fields,$state_where,$extra="",1);				
				
				if(count($subRes) > 0) {
					//count($subRes) > 0
					$success = array("status" => 'false', 'data' => 'Already shared with this user.');
				} else {
					
					$status 								=	"Active";
					$add_values['shr_bus_id'] 		= trim($sub_id);
					$add_values['shr_meb_id'] 		= trim($subc_id);
					$add_values['shr_rcv_email'] 	= trim($share_email);
					$add_values['shr_rcv_meb_id'] = trim($mebRes[0]['meb_id']);
					$add_values['shr_status'] 		= $status;		
					$add_values['shr_created_id'] = getAdminSessionId();
    	    		$add_values['shr_created_date'] = $today_date;	
    	    		
    	    		$GPDetail_result = $db->insertData(TBL_SHARE, $add_values);
					if($GPDetail_result > 0) {
						// send sharing email
						$card_fields 	= array("*");
						$card_where  	= "bus_id = ".$sub_id." AND bus_status = 'Active'";
						$cardRes 		= $db->selectData(TBL_MEMBER_BUSINESS,$card_fields,$card_where,$extra="",2);	

						$toemail = $share_email;					
						$subject = "B2b Mobile Message";
									
						$sender_name = 'Some one';			
						
						$message = '<html xmlns="http://www.w3.org/1999/xhtml">
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<meta content="width=1000" name="viewport"> </head>
				<body><table style="background-color:#151515" bgcolor="#151515" cellpadding="0" cellspacing="0" width="100%">
      <tbody>
        <tr>
          <td style="text-align:left" align="center">
            <table id="topMessageWrapper" style="width:600px;background-color:#151515;color:#999999;font-family:arial;font-size:13px;padding-bottom:0px;padding-top:0px;margin:0 auto;" align="center" bgcolor="#ffffff" cellpadding="0" cellspacing="0" width="600">
              <tbody>
                <tr>
                  <td>
                    <!-- DESIGNER TEMPLATE: AirMailOneColumn --><!-- BASIC TEMPLATE: unknown derivation -->
                    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="600">
                      <tbody>
                        <tr>
                          <td style="color:#999999;font-family:arial;font-size:13px;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tbody>
                                <tr>
                                  <td style="color:#999999;font-family:arial;font-size:13px;" valign="top">
                                    <div style="">
                                      <table style="word-wrap:break-word;table-layout:fixed;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                          <tr>
                                            <td style="color:#999999;font-family:arial;font-size:13px;">
                                              <p style="margin-bottom:0;margin-left:0;margin-right:0;margin-top:0;padding:0;">
                                                <img alt="" src="http://www.b2bms.net/images/jpeg_002.jpg" style="display:block;" usemap="#Map" border="0"></p>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                        <tr>
                          <td style="background-color:#ffffff;color:#999999;font-family:arial;font-size:13px;" bgcolor="#ffffff">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tbody>
                                <tr>
                                  <td style="background-color:#ffffff;padding-bottom:0px;padding-left:0px;padding-right:0px;color:#999999;font-family:arial;font-size:13px;" bgcolor="#ffffff" valign="top">                                    
                                    <div style="">
                                      <table style="word-wrap:break-word;table-layout:fixed;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>		  
										  <tr>
                                            <td style="color:#2f2d2e;font-family:arial;font-size:13px; padding:0px 33px 33px;">
                                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="left" valign="top" style="color:#2f2d2e;font-family:arial;font-size:13px; line-height:24px; padding:0px 0px;"><p>'.$sRes[0]['sub_name'].' has shared following card with you.</p><div style="width: 320px;">
<div style="font-family: arial;font-size: 13px;background: none repeat scroll 0 0 #fff; border: 3px solid #0d9db9; display: block; padding: 10px; position: relative;">
	<div style="font-size: 13px;line-height: 20px;color: #777;transition: color 0.5s ease 0s;clear: both;display: block;margin: 0 0 14px;text-align: right;">'.$cardRes[0]['bus_phone'].'<br>
		<span class="bus_email" style="color: #0d9db9;font-size: 18px;line-height: 20px;padding: 10px 0 20px;transition: font-size 0.5s ease 0s, padding 0.5s ease 0s, color 0.5s ease 0s, line-height 0.5s ease 0s;font-family: Open Sans,sans-serif;font-weight: 600;text-align: center;text-transform: capitalize;"><a href="mailto:'.$cardRes[0]['bus_email'].'">'.$cardRes[0]['bus_email'].'</a></span>
		<br>
		<span style="color: #555;transition: color 0.5s ease 0s;"><a target="_blank" href="'.$cardRes[0]['bus_website'].'" style="color: #777777;font-size: 13px;line-height: 18px;">'.$cardRes[0]['bus_website'].'</a></span>
		
   </div>
	<div style="color: #0d9db9;font-size: 18px;line-height: 20px;padding: 10px 0 20px;transition: font-size 0.5s ease 0s, padding 0.5s ease 0s, color 0.5s ease 0s, line-height 0.5s ease 0s;font-family: Open Sans,sans-serif;font-weight: 600;text-align: center;text-transform: capitalize;">'.$cardRes[0]['bus_title'].'</div>
	<div style="background: none repeat scroll 0 0 #f6f6f6;color: #666;font-size: 13px;line-height: 18px;padding: 6px 20px;text-align: center;">'.$cardRes[0]['bus_address1'].' <br>'.$cardRes[0]['bus_city'].' ,'.$staRes[0]["sta_name"].'</div>
</div>
</div></td>		
	</tr>
</table>
	</td>
	</tr>
										  <tr>
										  <td style="padding:0px 33px 33px;">
										  	<p style="color:#333; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:17px; margin:0px;"><strong>Thanks,</strong></p>
											<p style="color:#666; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:17px; margin:0px;">B2b Mobile,</p>
											<p style="color:#666; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:17px; margin:0px;"><a target="_blank" style="color:#0E70B9; text-decoration:underline;" href="#">B2b Mobile</a></p>
										  </td>
										  </tr>
										  
                                        </tbody>
                                      </table>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                        <tr>
                          <td style="color:#999999;font-family:arial;font-size:13px;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tbody>
                                <tr>
                                  <td style="color:#999999;font-family:arial;font-size:13px;" valign="top">
                                    <div>
                                      <table style="word-wrap:break-word;table-layout:fixed;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                          <tr>
                                            <td style="color:#999999;font-family:arial;font-size:13px;">
                                              <p style="margin-bottom:0;margin-left:0;margin-right:0;margin-top:0;padding:0;">
                                                <img alt="" src="http://getnakedair.com/email-images/jpeg.jpg" style="display:block;"></p>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                        <tr>
                          <td style="color:#999999;font-family:arial;font-size:13px;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                              <tbody>
                                <tr>
                                  <td style="color:#999999;font-family:arial;font-size:13px;" valign="top">
                                    <div>
                                      <table style="word-wrap:break-word;table-layout:fixed;" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                          <tr>
                                            <td style="color:#999999;font-family:arial;font-size:13px;" align="center">
                                              <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                <tbody>
                                                  
                                                  <tr>
                                                    <td style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#FFF; padding-top:18px; padding-bottom:18px;" align="center">
                                                      <span style="color:#0e70b9">© '.date("Y").'</span> B2B Mobile. All right reserved Intelli-Touch.</td>
                                                  </tr>
                                                </tbody>
                                              </table>
                                            </td>
                                          </tr>
                                        </tbody>
                                      </table>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
      </tbody>
    </table></body>
				</html>';
								
						$fromemail = "B2b Mobile";							
						// To send HTML mail, the Content-type header must be set
						$headers  = 'MIME-Version: 1.0' . "\r\n";
						$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
						// Additional headers
						$headers .= 'To: '.$toemail.' <'.$toemail.'>' . "\r\n";
						$headers .= 'From: B2b Mobile <mail@iflair.com>' . "\r\n";
						//$sendmail   = mailfunctionnew($fromemail,$toemail,$subject,$message,$custom_host_name,$custom_user_name,$custom_password,$custom_port_name,$filepath);
						$sendmail   = @mail($toemail, $subject, $message, $headers);
						//print_r($sendmail);							
						// send sharing email
						
						$success = array("status" => 'true', 'data' => 'Successful share this card.');	
					} else {
						$success = array("status" => 'false', 'data' => 'Error during sharing.<br /> please try again later.');	
					}					
				}
			} else {
				$success = array("status" => 'false', 'data' => 'No record found');
			}
	   } else if($action == 'ShareCardNew') {
      		$mebId			=	$_REQUEST['mem_id'];
			$sub_id			=	$_REQUEST['sub_id'];
			$subc_id		=	$_REQUEST['subc_id'];
			$type			=	$_REQUEST['type'];
			$mem_type		=	$_REQUEST['member_type'];
			$dir_id			=	$_REQUEST['DirId'];
			$card_type		=	$_REQUEST['card_type'];
			
			if($type	== 'email')
				$share_email	=	trim($_REQUEST['share_email2']);
			else
				$share_email	=	trim($_REQUEST['share_sms']);
			
			$meb_fields = array("*");
			if($type	== 'email')
				$meb_where  = "meb_username = '".$share_email."' AND meb_status = 'Active'";
			else 
				$meb_where  = "meb_phone = '".$share_email."' AND meb_status = 'Active'";	
			
			$mebRes 	= $db->selectData(TBL_MEMBER,$meb_fields,$meb_where,$extra="",2);
			
			if(true) {
				//count($mebRes) > 0
				$sub_fields = array("*");
				$sub_where  = "shr_rcv_email = '".$share_email."' AND shr_meb_id = ".$subc_id." AND shr_bus_id = ".$sub_id." AND shr_status = 'Active'";
				$subRes 	= $db->selectData(TBL_SHARE,$sub_fields,$sub_where,$extra="",2);	

				$s_fields = array("*");
				$s_where  = "sub_id = ".$subc_id." AND sub_status = 'Active'";
				$sRes 	= $db->selectData(TBL_MEMBER_SUBSCRIBERS,$s_fields,$s_where,$extra="",2);

				$state_fields = array("sta_name");
				$state_where  = "sta_id = '".$sRes[0]['bus_sta_id']."'";
				$staRes = $db->selectData(TBL_STATE,$state_fields,$state_where,$extra="",1);				
				
				if(count($subRes) > 0) {
					//count($subRes) > 0
					$success = array("status" => 'false', 'data' => 'Already shared with this user.');
				} else {
					
					$status 						=	"Active";
					$add_values['shr_bus_id'] 		= trim($sub_id);
					$add_values['shr_meb_id'] 		= trim($subc_id);
					$add_values['shr_bus_type'] 	= trim($card_type);
					$add_values['shr_rcv_email'] 	= trim($share_email);
					//$add_values['shr_rcv_meb_id'] = trim($mebRes[0]['meb_id']);
					$add_values['shr_status'] 		= $status;		
					$add_values['shr_created_id'] 	= getAdminSessionId();
    	    		$add_values['shr_created_date'] = $today_date;	
    	    		$GPDetail_result = $db->insertData(TBL_SHARE, $add_values);
    	    		
    	    		//add in to subscriber list
					
					$ss_fields = array("*");
					if($type	== 'email')
						$ss_where  = "sub_email = '".$share_email."' AND sub_status = 'Active' AND sub_type = 'Free'";
					else 
						$ss_where  = "sub_phone = '".$share_email."' AND sub_status = 'Active' AND sub_type = 'Free'";	
					
					$ssRes 	= $db->selectData(TBL_MEMBER_SUBSCRIBERS,$ss_fields,$ss_where,$extra="",2);
					
					if(count($ssRes) <= 0) {
						$statuss 						= "Inactive";
						$add_values1['sub_dir_id'] 		= trim($dir_id);
						$add_values1['sub_type'] 		= trim('Free');
						if($type == 'email')
							$add_values1['sub_email'] 	= trim($share_email);
						else
							$add_values1['sub_phone'] 	= trim($share_email); 
						$add_values1['sub_status'] 		= $statuss;		
						$add_values1['sub_created_id']	= getAdminSessionId();
    	    			$add_values1['sub_created_date']= $today_date;	
    	    			$sub_result = $db->insertData(TBL_MEMBER_SUBSCRIBERS, $add_values1);
    	    			
    	    			$statusss 							=	"Active";
    	    			$add_values2['inv_sub_id'] 	= $sub_result;
						$add_values2['inv_dir_id'] 	= trim($dir_id);
						//$add_values2['sbm_type'] 		= trim('Individual');
						$add_values2['inv_status'] 		= $statusss;		
						$add_values2['inv_created_id']= getAdminSessionId();
    	    			$add_values2['inv_created_date'] = $today_date;	
    	    			$sub_result = $db->insertData(TBL_SUBSCRIBER_INV, $add_values2);
    	    			
    	    		}
					//add in to subscriber list
    	    		
					if(true) {
						//$GPDetail_result > 0
						$success = array("status" => 'true', 'data' => 'Successful share this card.');	
					} else {
						$success = array("status" => 'false', 'data' => 'Error during sharing.<br /> please try again later.');	
					}					
				}
			} else {
				$success = array("status" => 'false', 'data' => 'No record found');
			}
	}  else if($action == 'GetCardDetailForDownload') {
		
		$mebId		= $_REQUEST['card_id'];
		$bus_id		= $_REQUEST['card_id'];
		$type		= $_REQUEST['type'];
		
		if($type == 'Sub') {
			$table = TBL_MEMBER_BUSINESS_SUB;
			$uploadFILEWWW = UPLOAD_WWW_BUSINESS_FILE."Business_Sub/";

			$meb_fields = array("*,(SELECT sta_name FROM tbl_state WHERE bus_sta_id = sta_id) as StateName");
			$meb_where  = "bus_id = ".$mebId." AND bus_status = 'Active'";
			$mebRes 	= $db->selectData(TBL_MEMBER_SUBSCRIBERS." JOIN ".$table." ON sub_id = bus_sub_id",$meb_fields,$meb_where,$extra="",2);
		} else {
			$table 			= TBL_MEMBER_BUSINESS;
			$uploadFILEWWW 	= UPLOAD_WWW_BUSINESS_FILE;

			$meb_fields = array("*,(SELECT sta_name FROM tbl_state WHERE bus_sta_id = sta_id) as StateName");
			$meb_where  = "bus_id = ".$mebId." AND bus_status = 'Active'";
			$mebRes 	= $db->selectData($table,$meb_fields,$meb_where,$extra="",2);

		}

		
		if(count($mebRes) > 0) {

			if($mebRes[0]['bus_logo_file']!="") {	
				$fileNameExt = explode(".", $mebRes[0]['bus_logo_file']);
				$fileExt 	 = $fileNameExt[count($fileNameExt)-1];
				$allowedImgExts = array("gif","jpg","jpeg","png","GIF","JPG","JPEG","PNG");
				if(in_array($fileExt,$allowedImgExts)) {
					$profile_image = "<a class='fancybox' href='".$uploadFILEWWW.$mebRes[0]['bus_id']."/".$mebRes[0]['bus_logo_file']."'  ><img class='popphoto' src='".$uploadFILEWWW.$mebRes[0]['bus_id']."/".$mebRes[0]['bus_logo_file']."' height='115' width='115' style='width: auto; max-width: 150px;'  /></a>";
				} else {
					//$profile_image = "<img class='popphoto' src='images/profile.png' height='115' width='115' />";
					$filev = $uploadFILEWWW.$mebRes[0]['bus_id']."/".$mebRes[0]['bus_logo_file'];
					$profile_image = '<a href="#"><img class="popphoto" src="images/play-button.jpg" height="115" width="115" onclick="play_new_video(\''.$video_file.'\')" style="height: 115px; width: 160px;" /></a>';
				}
			} else {
				//$profile_image = "<a class='fancybox' href='development/app_new/images/profile.png' data-rel='popup' data-position-to='window' data-transition='fade' ><img class='popphoto' src='development/app_new/images/profile.png' height='115' width='115' /></a>";
				//$busFiles_new = "<img class='popphoto' src='images/photo.jpg' style='height: 115px; width: 190px;' height='115' width='115' class='popphoto' /></a>";	

				$profile_image = "";
				$busFiles_new = "";	
			}

			if($mebRes[0]['bus_picture_file']!="") {	
				$fileNameExt = explode(".", $mebRes[0]['bus_picture_file']);
				$fileExt 	 = $fileNameExt[count($fileNameExt)-1];
				$allowedImgExts = array("gif","jpg","jpeg","png","GIF","JPG","JPEG","PNG");
				if(in_array($fileExt,$allowedImgExts)) {
					$profile_imagen = "<a class='fancybox' href='".$uploadFILEWWW.$mebRes[0]['bus_id']."/".$mebRes[0]['bus_picture_file']."'><img class='popphoto' src='".$uploadFILEWWW.$mebRes[0]['bus_id']."/".$mebRes[0]['bus_picture_file']."' style='width: 180px;'  /></a>";
					$busFiles_new = "<img class='popphoto' src='".$uploadFILEWWW.$mebRes[0]['bus_id']."/".$mebRes[0]['bus_picture_file']."' style='width: 190px;' class='popphoto' />";
				} else {
					$filev = $uploadFILEWWW.$mebRes[0]['bus_id']."/".$mebRes[0]['bus_picture_file'];
					$profile_imagen = '<a href="#"><img class="popphoto" src="images/play-button.jpg" height="115" width="115" onclick="play_new_video(\''.$video_file.'\')" style="height: 115px; width: 160px;" /></a>';
					$busFiles_new = '<object width="100%" height="250" data="player.swf" type="application/x-shockwave-flash"><param value="opaque" name="wmode"><param value="always" name="allowscriptaccess"><param value="all" name="allownetworking"><param value="'.$video.'" name="flashvars"><param name="allowFullScreen" value="true" /></object>';
				}
			} else {
				$profile_imagen = "<a class='fancybox' href='development/app_new/images/profile.png' data-rel='popup' data-position-to='window' data-transition='fade' ><img class='popphoto' src='development/app_new/images/profile.png' height='' width='150' /></a>";
				$busFiles_new = "<img class='popphoto' src='development/images/photo.jpg' style='height: 115px; width: 190px;' height='115' width='115' class='popphoto' /></a>";	
			}	
			
			$mem_data = '';
			

			$mem_data .= '<div id="about" style="display: none;">';
			$mem_data .= '<div class="fullview_clients">
                <div class="subRow">
                     <div class="enterTitle main">
                          <h1>About Us...</h1>
                    </div>
                </div>
                <div class="subRow">
                    <table width="100%" cellspacing="0" cellpadding="0" border="0" class="about-table">
                        <tbody>
                        	<tr>
                            	<td style="padding:20px;">
                                	<div class="top-logo">
	                                    <span>'.$busFiles_new.' 
	                                    </span>
	                                    <h1>'.$mebRes[0]['bus_company'].'</h1>
                                	</div>
	                                <div class="decription-top">
	                                   	<p>'.$mebRes[0]['bus_about'].'</p>
	                                </div>
                            	</td>
                        	</tr>
                    	</tbody>
                   	</table>
                </div>
            </div>';
			$mem_data .= '</div>';

			$mem_data .= '<div id="info" style="display: none;">';
			$mem_data .= '<div class="fullview_clients open_popup_info">
                <div class="subRow">
                     <div class="enterTitle main">
                          <h1>Information</h1>
                    </div>
                </div>
                <div class="subRow">
                    <table cellspacing="0" cellpadding="0" width="100%" border="0" class="info-table">
                        <tbody>
                        <tr>
                            <td width="25%" class="blue">COMPANY NAME</td>
                            <td width="25%">'.$mebRes[0]['bus_company'].'</td>
                        </tr>
                        <tr>
                            <td width="25%" class="blue">ADDRESS</td>
                            <td width="25%">'.$mebRes[0]['bus_address1'].'</td>
                        </tr>
                        <tr>
                            <td width="25%" class="blue">CITY</td>
                            <td width="25%">'.$mebRes[0]['bus_city'].'</td>
                        </tr>
                        <tr>
                            <td width="25%" class="blue">STATE</td>
                            <td width="25%">'.$mebRes[0]['StateName'].'</td>
                        </tr>
                        <tr>
                            <td width="25%" class="blue">ZIP CODE</td>
                            <td width="25%">'.$mebRes[0]['bus_zip'].'</td>
                        </tr>
                        <tr>
                            <td width="25%" class="blue">WEBSITE</td>
                            <td width="25%"><a href="#">'.$mebRes[0]['bus_website'].'</a></td>
                        </tr>
                        <tr>
                            <td width="25%" class="blue">OFFICE PHONE</td>
                            <td width="25%">'.$mebRes[0]['bus_phone'].'</td>
                        </tr>
                        <tr>
                           	<td width="25%" class="blue">FAX NUMBER</td>
                            <td width="25%">'.$mebRes[0]['bus_fax'].'</td>
                        </tr>
                        <tr>
                            <td width="25%" class="blue">EMAIL ADDRESS</td>
                            <td width="25%">'.$mebRes[0]['bus_email'].'</td>
                        </tr>
                        <tr>
                            <td width="25%" class="blue">FIRST NAME</td>
                            <td width="25%">'.$mebRes[0]['bus_name'].'</td>
                        </tr>
                        <tr>
                            <td width="25%" class="blue">LAST NAME</td>
                            <td width="25%">'.$mebRes[0]['bus_name'].'</td>
                        </tr>
                        <tr>
                            <td width="25%" class="blue">TITLE</td>
                            <td width="25%">'.$mebRes[0]['bus_title'].'</td>
                        </tr>
                        <tr>
                            <td width="25%" class="blue">CATEGORY</td>
                            <td width="25%">'.getCategoryNameByMebId($mebRes[0]['bus_meb_id']).'</td>
                        </tr>
                    </tbody></table>
                </div>
            </div>
            </div>';
			$mem_data .= '</div>';

			$mem_data .= '<div id="playlist" style="display: none;">';
			$mem_data .= '<div class="fullview_clients open_popup_info">
                <div class="subRow">
                     <div class="enterTitle main">
                          <h1>Media</h1>
                    </div>
                </div>
                <div class="subRow">
                    <div class="playlist-table">
                        <ul>';
                            $counter = 1;
                            if($mebRes[0]['bus_video_file_1'] != "") {
                            	$mem_data .= '<li class="" id="video_1_li" onclick="javascript:showVideo(\'video_1\')">Play # '.$counter.'</li>';
                            	$counter = $counter+1;
                            }		
                            if($mebRes[0]['bus_video_file_2'] != "") {
                            	$mem_data .= '<li class="" id="video_2_li" onclick="javascript:showVideo(\'video_2\')">Play # '.$counter.'</li>';
                            	$counter = $counter+1;
                            }
                            if($mebRes[0]['bus_video_file_3'] != "") {
                           		$mem_data .= '<li class="" id="video_3_li" onclick="javascript:showVideo(\'video_3\')">Play # '.$counter.'</li>';
                           		$counter = $counter+1;
                            }
                            if($mebRes[0]['bus_video_file_4'] != "") {
                            	$mem_data .= '<li class="" id="video_4_li" onclick="javascript:showVideo(\'video_4\')">Play # '.$counter.'</li>';
                            	$counter = $counter+1;
                            }
                            if($mebRes[0]['bus_video_file_5'] != "") {
                            	$mem_data .= '<li class="" id="video_5_li" onclick="javascript:showVideo(\'video_5\')">Play # '.$counter.'</li>';                    
                            	$counter = $counter+1;
                            }
     	$mem_data .= '</ul>
                    </div>
                    <div id="defaultVideo" style="width:600px;height:250px;">&nbsp;&nbsp;&nbsp;</div>
                    <div id="video_1" style="display:none;" class="videoh1">';
                  	$path =  pathinfo($mebRes[0]['bus_video_file_1']);
                 
      	$mem_data .= '<object width="100%" height="310" data="player.swf" type="application/x-shockwave-flash">
                       <param value="opaque" name="wmode">
                       <param value="always" name="allowscriptaccess">
                       <param value="all" name="allownetworking">
                       <param value="file='.$uploadFILEWWW.$bus_id."/".$mebRes[0]['bus_video_file_1'].'&;volume=80&amp;bufferlength=5&amp;logo.hide=false&amp;&;title=sleeve_length&amp;linktarget=_self" name="flashvars">
                       <param name="allowFullScreen" value="true" />
                      </object>
                    </div>
                    <div id="video_2" style="display:none;" class="videoh1">';
                    $path =  pathinfo($mebRes[0]['bus_video_file_2']);
                  
        $mem_data .= '<object width="100%" height="310" data="player.swf" type="application/x-shockwave-flash">
                       <param value="opaque" name="wmode">
                       <param value="always" name="allowscriptaccess">
                       <param value="all" name="allownetworking">
                       <param value="file='.$uploadFILEWWW.$bus_id."/".$mebRes[0]['bus_video_file_2'].'&;volume=80&amp;bufferlength=5&amp;logo.hide=false&amp;&;title=sleeve_length&amp;linktarget=_self" name="flashvars">
                       <param name="allowFullScreen" value="true" />
                      </object>
                    </div>	
                    <div id="video_3" style="display:none;" class="videoh1">';
                   	$path =  pathinfo($mebRes[0]['bus_video_file_3']);
                    
        $mem_data .= '<object width="100%" height="310" data="player.swf" type="application/x-shockwave-flash">
                       <param value="opaque" name="wmode">
                       <param value="always" name="allowscriptaccess">
                       <param value="all" name="allownetworking">
                       <param value="file='.$uploadFILEWWW.$bus_id."/".$mebRes[0]['bus_video_file_3'].'&;volume=80&amp;bufferlength=5&amp;logo.hide=false&amp;&;title=sleeve_length&amp;linktarget=_self" name="flashvars">
                       <param name="allowFullScreen" value="true" />
                      </object>
                    </div>
                    <div id="video_4" style="display:none;" class="videoh1">';

                   $path =  pathinfo($mebRes[0]['bus_video_file_4']);
         $mem_data .= '<object width="100%" height="310" data="player.swf" type="application/x-shockwave-flash">
                       <param value="opaque" name="wmode">
                       <param value="always" name="allowscriptaccess">
                       <param value="all" name="allownetworking">
                       <param value="file='.$uploadFILEWWW.$bus_id."/".$mebRes[0]['bus_video_file_4'].'&;volume=80&amp;bufferlength=5&amp;logo.hide=false&amp;&;title=sleeve_length&amp;linktarget=_self" name="flashvars">
                       <param name="allowFullScreen" value="true" />
                      </object>
                    </div>
                    <div id="video_5" style="display:none;" class="videoh1">';
                    $path =  pathinfo($mebRes[0]['bus_video_file_5']);
                    
        $mem_data .= '<object width="100%" height="310" data="player.swf" type="application/x-shockwave-flash">
                       <param value="opaque" name="wmode">
                       <param value="always" name="allowscriptaccess">
                       <param value="all" name="allownetworking">
                       <param value="file='.$uploadFILEWWW.$bus_id."/".$mebRes[0]['bus_video_file_5'].'&;volume=80&amp;bufferlength=5&amp;logo.hide=false&amp;&;title=sleeve_length&amp;linktarget=_self" name="flashvars">
                       <param name="allowFullScreen" value="true" />
                      </object>
                    </div>
                </div>
                </div>
            </div>
            </div>';
			$mem_data .= '</div>';


			$mem_data .= '<article>
							<div class="top-link">
									<ul>
										<li>
                                        	<a href="#about" class="fancybox" >
                                            	<span><img src="development/app_new/images/about-us.png" alt="" title=""></span>about us
                                            </a>
                                        </li>
                                        <li>
                                        	<a href="#info" class="fancybox">
                                            	<span><img src="development/app_new/images/infoinformation.png" alt="" title=""></span>Information
                                           	</a>
                                        </li>
                                        <li>
                                        	<a href="#playlist" class="fancybox">
                                            	<span><img src="development/app_new/images/play-list.png" alt="" title=""></span>Media
                                            </a>
                                        </li>
                                    </ul>
                                </div>
							'.$profile_imagen.'
		            		<div class="bus_company">'.$mebRes[0]['bus_name'].' '.$mebRes[0]['bus_lname'].'</div>
		            		<div class="bus_name">'.$mebRes[0]['bus_title'].'</div>
		            		<div class="bus_title">'.$mebRes[0]['bus_company'].'</div>
		            		<div class="bus_address"></div>'.$profile_image.'
		            		<div class="detail_generel_info" style="display: none;">
		            				<a href="#generalinfo" data-transition="pop" data-position-to="window" data-rel="popup" aria-haspopup="true" aria-owns="generalinfo" aria-expanded="false" class="ui-link"><strong>More Information</strong></a>
		            			</div>
		            		</article>';

		    $mem_data .= '<div class="download_box_new">
                	<h1>Download App to save this business card</h1>
                	<a href="#"><img src="development/images/appstore.png" border="0"/></a>
                	<a href="development/app_new/apk_file/IVQMobile.apk" target="_blank"><img src="development/images/google.png" border="0"/></a>
            	</div>';        		

			$success = array("status" => 'true', 'data' => $mem_data);

		} else {
			$mem_data = '<div class="download_box_new">
                	<h1>Download App to save this business card</h1>
                	<a href="#"><img src="development/images/appstore.png" border="0"/></a>
                	<a href="development/app_new/apk_file/IVQMobile.apk" target="_blank"><img src="development/images/google.png" border="0"/></a>
            	</div>';
			$success = array("status" => 'false', 'data' => 'No record found'.$mem_data);
		}
	}

	echo json_encode($success);
	exit(); 
	?>