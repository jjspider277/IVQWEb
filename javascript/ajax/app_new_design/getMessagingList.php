<?php
	error_reporting(1);
	header('Access-Control-Allow-Origin: *');
	
	include_once('../../../include/includeclass.php');
	$success = array();
	$action = $_REQUEST['action'];
	
	if($action == 'GetMsgList') {
		
		$SubcriberId			=	$_REQUEST['SubcriberId'];
		$SECTION_FIELD_PREFIX=	$_REQUEST['fieldPrefix'];
		$SECTION_MANAGE_PAGE	=	$_REQUEST['managePagemessage'];	
		$searchchar 			= 	mysql_real_escape_string($_REQUEST['search_message']);
		$SECTION_TABLE			= 	$_REQUEST['tableName'];
		$xtraCondition			=	stripslashes($_REQUEST['xtraCondition']);
		$subemail				=	$_REQUEST['subemail'];
		
		$SECTION="Messaging";

		//$meb_dirId = getMemeberAsDir();
		$meb_dirId = "select meb_id from tbl_member where meb_status='Active' AND meb_dir_id IN ( SELECT inv_dir_id FROM tbl_subscriber_individual where inv_sub_id=".$SubcriberId.")";
				
		$total_language = count($result_language);
		#################################################################
		if($orderby == "")
		{
			$ORDER =  "desc";
			$orderby = "id";
		}

		$where = ' AND p.msg_id NOT IN ( SELECT arc_msg_is from tbl_member_messaging_archive where arc_sub_id='.$SubcriberId.') ';	
	
		##################################  General Query ###############################################  
		if ($searchchar != 'undefined' && $searchchar != "") {
			if ($searchchar == 'other') {
				$sql_query = "SELECT p.* FROM " . $SECTION_TABLE . " p WHERE p." . $SECTION_FIELD_PREFIX . "id != 0   AND  p.".$SECTION_FIELD_PREFIX."status!='Deleted' AND p.msg_meb_id IN (".$meb_dirId.") AND `msg_message` REGEXP '^[^a-zA-Z]'".$where." order by p." . $SECTION_FIELD_PREFIX . $orderby . " " . $ORDER;
			}
			else {
				if ($searchchar == 'all') {
					$sql_query = "SELECT p.* FROM " . $SECTION_TABLE . " p WHERE p." . $SECTION_FIELD_PREFIX . "id != 0  AND p.".$SECTION_FIELD_PREFIX."status!='Deleted' AND p.msg_meb_id IN (".$meb_dirId.")".$where." order by p." . $SECTION_FIELD_PREFIX . $orderby . " " . $ORDER;
				} else {
					$ts = $searchchar . "%";
					$sql_query = "select * from ".$SECTION_TABLE." as p where p.msg_message LIKE '" . $ts . "' AND p.".$SECTION_FIELD_PREFIX."status!='Deleted' AND p.msg_meb_id IN (".$meb_dirId.")".$where." order by " . $SECTION_FIELD_PREFIX . $orderby . " " . $ORDER;
				}
			}
		}
		else if ($xtraCondition != 'undefined' && $xtraCondition != "") {
			//$ts = $xtraCondition;
			$sql_query = "select * from ".$SECTION_TABLE." as p where (" . $xtraCondition . ") AND (p.".$SECTION_FIELD_PREFIX."status!='Deleted' AND p.msg_meb_id IN (".$meb_dirId."))".$where." order by " . $SECTION_FIELD_PREFIX . $orderby . " " . $ORDER;
		}
		else
		{
			$sql_query = "SELECT p.* FROM ".$SECTION_TABLE." as p WHERE p.".$SECTION_FIELD_PREFIX."id != 0 AND p.".$SECTION_FIELD_PREFIX."status != 'Deleted' AND p.msg_meb_id IN (".$meb_dirId.")".$where." order by p.".$SECTION_FIELD_PREFIX.$orderby." ".$ORDER;

		}    
		#################################  Paging Query + Paging Code ##################################
		$paging_query = $sql_query;
		$paging_result  = $db->select($paging_query); 
		$count = count($paging_result);
		$per_page = SITE_PAGING_PER_PAGE; //rows per page
		$per_page = 20;
		$pages = ceil($count/$per_page);    
		#################################################################################################
		if($action_type == "paging")
		{
			if(!empty($_REQUEST['pagem']))  
			{
				$page = $_REQUEST['pagem'];
			}       
		}
		else
		{
			if(!empty($_REQUEST['pagem']))  
				$page = $_REQUEST['pagem'];
	  		else
				$page = 1;
		} 	
		$list_query = $sql_query;
		if(!empty($per_page) && $_REQUEST['page']!="all")
		{
			$start = ($page-1)*$per_page;
			if($start<0)
			{
				$start=0;
			}
			$list_query .= " limit $start,$per_page"; 
		} 
		$result_query  = $db->select($list_query);  
		$total_rows = count($result_query);
		//echo $ms = ajaxMsg($_SESSION['msg']);
		
		if($total_rows > 0){
			$j = 1;
			$dir_data = '<ul>';
			for($i=0;$i<$total_rows;$i++){
				//$dir_data .= $result_query[$i]['Dirname'];
				$sub_con='';
				if($SUBSCRIBER_MAIL != '') {
					$sub_con='\'sub\'';
					$fun='setmessagedetailpage(\''.$result_query[$i][$SECTION_FIELD_PREFIX."id"].'\', \'msg\')';
				} else {
					$fun='setmessagedetailpage(\''.$result_query[$i][$SECTION_FIELD_PREFIX."id"].'\', \'msg\')';
				}
				$href = '#';
				$dir_data .= '<li id="mas_'.$result_query[$i][$SECTION_FIELD_PREFIX."id"].'"><a onclick="'.$fun.'" href="'.$href.'" class="masage_a" data-ajax="true" load="yes" data-prefetch="true">'.$result_query[$i][$SECTION_FIELD_PREFIX.'message'].'</a><div class="masage_div"><a href="#" onclick="return mas_archive(\''.$result_query[$i][$SECTION_FIELD_PREFIX."id"].'\')"><img src="images/archive.png" /></a></div></li>';
			}
		
			$dir_data .= '</ul>';
			
			if(count($pages)>0) {
			$pagination = '<div class="paging"><ul>';
				for($t=1;$t<=$pages;$t++) {
					if($page==$t) {
						$pagination .= '<li><a class="active" href="javascript:void(0);"';
						if($pages > 1){
							$pagination .= 'onclick="getAjaxPaging(\'Messaging\',\''.$pages.'\',\''.$t.'\')"'; 
						}
						$pagination .= '>'.$t.'</a></li>';
						} else {
						$pagination .= '<li><a href="javascript:void(0);"';
						if($pages > 1){ 
							$pagination .= 'onclick="getAjaxPaging(\'Messaging\',\''.$pages.'\',\''.$t.'\')"';
						}
						$pagination .= '>'.$t.'</a></li>';
					}
				}
				$pagination .= '</div></ul>';	
				$pagination .= '<script>';	
				$pagination .= 'function getAjaxPaging(action, total, current){ ';
				$pagination .= '$("#page_val_message").val(current);'; 
				$pagination .= 'getmessages("yes");}';	
				$pagination .= '</script>';	
			}			
			$success = array("status" => 'true', 'data' => $dir_data, 'pagination' => $pagination);
		} else {
			$dir_data = '<ul>';
			$dir_data .= '<li><a href="javascript:void(0);">No record found.</a></li>';
			$dir_data .= '</ul>';
			$success = array("status" => 'true', 'data'=>$dir_data, 'pagination' => '');
		}
	} else if($action == 'GetMessageDetail') {

		$MessageDetailId = $_REQUEST['MessageDetailId'];
		
		$dir_fields = array("*");
		$dir_where  = "msg_id = ".$MessageDetailId." AND msg_status = 'Active'";
		$dirRes 	= $db->selectData('tbl_member_messaging LEFT JOIN tbl_member ON msg_meb_id = meb_id LEFT JOIN  tbl_directory ON dir_id=meb_dir_id', $dir_fields, $dir_where,$extra="",2);

		$dir_data = '<article class="tab-content new_ul_content"><div class="error_msg" style="display:none;"></div><ul class="view-massage">';
      $dir_data .= '<li><b>Message</b><span class="sent-on">Sent On :'.date('m/d/Y',strtotime($dirRes[0]["msg_created_date"])).'</span></li>';
      
		$dir_data .= '<li><p>'.$dirRes[0]["msg_message"].'</p><div class="fl"><b>Member name</b><span>'.$dirRes[0]["meb_name"].'</span></div><div class="fl ar-txt"><b>Directory name</b><span>'.$dirRes[0]["dir_name"].'</span></div></li>';
		
		if($dirRes[0]["msg_file"] != '') {
			
			$msg_file = UPLOAD_WWW_MESSAGING_FILE.$dirRes[0]["msg_id"]."/".$dirRes[0]["msg_file"];
		
			$fileNameExt = explode(".", $dirRes[0]["msg_file"]);
			$fileExt 	 = $fileNameExt[count($fileNameExt)-1];
			$allowedImgExts = array("gif","jpg","jpeg","png","GIF","JPG","JPEG","PNG");	
			$allowedVidExts = array("3gp","amv","avi","mp4");			
			if(in_array($fileExt,$allowedImgExts)) {
				$image = "<img src='".$msg_file."' height='115' width='115' style='height: 115px; width: 190px;'  />";
			} else if(in_array($fileExt,$allowedVidExts)) {
				$image = "<img src='images/play-button.jpg' height='115' width='115' style='height: 115px; width: 190px;' onclick='play_new_video(\"".$msg_file."\")' />";
			} else {
				$image = $dirRes[0]["msg_file"];
			}
			//onclick="return downloadf(\''.$msg_file.'\')"
			//id="startDl" onclick="onDeviceReady()"
			if(in_array($fileExt,$allowedVidExts)) {
				$dir_data .= '<li><div class="fl"><b>Attached File</b><a class="" i-uri="'.$msg_file.'" i-name="'.$dirRes[0]["msg_file"].'" href="">'.$image."</a></div></li>";
				$dir_data .= '<script> function play_new_video(vid) { window.plugins.videoPlayer.play(vid); }</script>';
			} else {
				$dir_data .= '<li><div class="fl"><b>Attached File</b><a class="download" i-uri="'.$msg_file.'" i-name="'.$dirRes[0]["msg_file"].'" href="">'.$image."</a></div></li>";
			}
		}
		
		//$dir_data .= '<li><b>Directory name :</b><span>'.$dirRes[0]["dir_name"].'</span></li>';
     	//$dir_data .= '<li></li>';
     	$dir_data .='</article>';
		if($dirRes[0]["msg_file"] != '') {
			$dir_data .= '<script>';
				$dir_data .= '$(".download").on("click", function(){
									var fileTransfer = new FileTransfer();
									var uri = encodeURI($(this).attr("i-uri"));
									var filePath = "file://sdcard/"+$(this).attr("i-name");
									fileTransfer.download(
    									uri,
    									filePath,
    								function(entry) {
        								console.log("download complete: " + entry.fullPath); 
									},
    								function(error) {
        								console.log("download error source " + error.source);
        								console.log("download error target " + error.target);
        								console.log("upload error code" + error.code);
    								},
    								false,
    								{
        								headers: {
            							"Authorization": "Basic dGVzdHVzZXJuYW1lOnRlc3RwYXNzd29yZA=="
        								}
    								}
								);
							})';
			$dir_data .= '</script>';     	
     	}
      $success = array("status" => 'true', 'data' => $dir_data); 				
			
	} else if($action == 'MessageArchive'){

		$msg_id = $_REQUEST['id'];		
		$sub_id = $_REQUEST['sub_id'];
		
		$add_values['arc_msg_is'] 		= trim($msg_id);
		$add_values['arc_sub_id'] 		= trim($sub_id);
		$add_values['arc_created_id'] = getAdminSessionId();
    	$add_values['arc_created_date'] = $today_date;	
		
		$GPDetail_result = $db->insertData('tbl_member_messaging_archive', $add_values);
		if($GPDetail_result > 0) {
			$success = array("status" => 'true', 'data' => '');	
		} else {
			$success = array("status" => 'false', 'data' => '');
		}
		
	} else if($action == 'GetMsgListGraph') {
		
		$id 		=	$_REQUEST['direid'];
		$searchchar	=	$_REQUEST['char'];
	 	$orderBy	=	$_REQUEST['orderby'];
 		$xtracondition_o	=	$_REQUEST['xtracondition'];

		$SubcriberId			=	$_REQUEST['SubcriberId'];
		$SECTION_FIELD_PREFIX	=	$_REQUEST['fieldPrefix'];
		$SECTION_MANAGE_PAGE	=	$_REQUEST['managePagemessage'];	
		$searchchar 			= 	mysql_real_escape_string($_REQUEST['search_message']);
		$SECTION_TABLE			= 	$_REQUEST['tableName'];
		$xtraCondition			=	stripslashes($_REQUEST['xtraCondition']);
		$subemail				=	$_REQUEST['subemail'];
		
		$SECTION="Messaging";

		##################################  General Query ###############################################  
		
		if($id!="") {
	 		$meb_fields = array("meb_id");
			$meb_where  = "meb_dir_id= ".$id." AND meb_status != 'Deleted'";
			$mebRes 	= $db->selectData(TBL_MEMBER,$meb_fields,$meb_where,$extra="",1);
  			for($i=0;$i<count($mebRes);$i++) {
				if($i==count($mebRes)-1) {
					$addComma = "";
				} else {
					$addComma = ",";
				}
				$allMeb .= $mebRes[$i]["meb_id"].$addComma;
			}
	  		if ($xtracondition_o != 'undefined' && $xtracondition_o != "") {
		    	$xtracondition = $xtracondition_o." AND ";
		  	} else {
		        $xtracondition = "";
		  	}

  			if($id> 0) {
  				//$mebRes> 0
		    	//$sql="SELECT * FROM ".TBL_MEMBER_MESSAGING." LEFT JOIN ".TBL_BUSINESS_CATEGORY." as b ON b.buc_id = ".TBL_MEMBER_MESSAGING.".buc_id  WHERE ".$xtracondition." msg_meb_id IN (".$allMeb.")  AND msg_status != 'Deleted' ORDER BY ".$orderBy;
		    	$sql="SELECT * FROM ".TBL_MEMBER_MESSAGING." LEFT JOIN ".TBL_BUSINESS_CATEGORY." as b ON b.buc_id = ".TBL_MEMBER_MESSAGING.".buc_id  WHERE ".$xtracondition." msg_dir_id = ".$id."  AND msg_status != 'Deleted' ORDER BY ".$orderBy;
		    	$msgResult  = $db->select($sql);
		     	$totMessage = count($msgResult);

		     	$per_page = 10;
				$pages = ceil($totMessage/$per_page);

				if($action_type == "paging") {
					if(!empty($_REQUEST['pagem']))	{
						$page = $_REQUEST['pagem'];
					}       
				} else {
					if(!empty($_REQUEST['pagem']))  
						$page = $_REQUEST['pagem'];
			  		else
						$page = 1;
				}

				$list_query = $sql;
				if(!empty($per_page) && $_REQUEST['page']!="all") {
					$start = ($page-1)*$per_page;
					if($start<0) {
						$start=0;
					}
					$list_query .= " limit $start,$per_page"; 
				}
				$result_query  = $db->select($list_query);  
				$total_rows = count($result_query);
		  	}
		} 

		#################################  Paging Query + Paging Code ##################################
		$categoty = '';
		$categoty = getBusinessCategoryList('bus_category','bus_category',$bus_category);

		if($total_rows > 0) {
			
			$dir_data = '<div class="promo-table-view">';
			
			for($k=0;$k<$total_rows;$k++){
				if($k=='0')
					$fmi = $result_query[$k]['msg_id'];

				//$dir_data .= $result_query[$i]['Dirname'];
				$sub_con='';
				if($SUBSCRIBER_MAIL != '') {
					$sub_con='\'sub\'';
					$fun='setmessagedetailpage(\''.$result_query[$i][$SECTION_FIELD_PREFIX."id"].'\', \'msg\')';
				} else {
					$fun='setmessagedetailpage(\''.$result_query[$i][$SECTION_FIELD_PREFIX."id"].'\', \'msg\')';
				}
				$href = '#';
				//msg_details.html
				$dir_data .= '<article class="msg_detail_hidden_data_'.$result_query[$k]['msg_id'].'" style="display: none;">';
					if($result_query[$k]['msg_file'] != '') {
						$msg_file = UPLOAD_WWW_MESSAGING_FILE.$result_query[$k]["msg_id"]."/".$result_query[$k]["msg_file"];

						$fileNameExt = explode(".", $result_query[$k]["msg_file"]);
						$fileExt 	 = $fileNameExt[count($fileNameExt)-1];
						$allowedImgExts = array("gif","jpg","jpeg","png","GIF","JPG","JPEG","PNG");	
						$allowedVidExts = array("3gp","amv","avi","mp4");	

						if(in_array($fileExt,$allowedImgExts)) {
							$dir_data .= "<img src='".$msg_file."' height='115' width='115' style='height: 115px; width: 190px;' class='popphoto' />";
						} else if(in_array($fileExt,$allowedVidExts)) {
							$dir_data .= "<img src='images/play-button.jpg' height='115' width='115' style='height: 115px; width: 190px;' onclick='play_new_video(\"".$msg_file."\")' />";
						} else {
							$dir_data .=$result_query[$k]["msg_file"];
						}

						//$dir_data .= '<img height="115" width="115" style="height: 115px; width: 190px;" src="images/Linux-Green-Brand-Logo-Background-HD-Wallpaper.jpg" class="popphoto">';
					}
					$dir_data .= '<div class="bus_name">'.$msgResult[$k]['msg_company_title'].'</div><div class="">'.$msgResult[$k]['buc_name'].'</div><div class="bus_title">'.$msgResult[$k]['msg_message'].'</div>';

				$dir_data .= '</article>';

				$dir_data .= '<div class="promo-table-view-main"><div class="promo-table-view-left">';

				if($_REQUEST['orderby'] == 'msg_created_date')
					$dir_data .= '<b>';
				$dir_data .= date("m/d/y",strtotime($result_query[$k]['msg_created_date']));
				if($_REQUEST['orderby'] == 'msg_created_date')
					$dir_data .= '</b>';

				$dir_data .= ' | '.$msgResult[$k]['buc_name'].' | ';

				if($_REQUEST['orderby'] == 'msg_company_title')
					$dir_data .= '<b>';
				$dir_data .= $msgResult[$k]['msg_company_title'];
				if($_REQUEST['orderby'] == 'msg_company_title')
					$dir_data .= '</b>';

				$dir_data .= '</div>';


                $dir_data .= '<div class="promo-table-view-right">
                        <a class="msg_detail_click" data_msg_id="'.$result_query[$k]['msg_id'].'" data-ajax="false" load="yes" href="#">
                        	<img border="0" src="images/msging.png">
                        </a>
                    </div>
                </div>';
			}
		
			$dir_data .= '<div class="clear"></div></div>';
			
			$pagination = '';
			if($pages>1) {
				$page = ($page == 0 ? 1 : $page);
				$start = ($page-1) * $per_page;
				$adjacents = "1";		

				$prev = $page - 1;
				$next = $page + 1;
				$lastpage = ceil($count/$per_page);
				$lpm1 = $lastpage - 1;

				$pagination = '<div class="paging"><ul>';
				
					if ($page > 1)
			            $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getAjaxPaging(\"Messaging\",\"".$pages."\",\"".$prev."\");'>&laquo; Previous</a></li>";
			        else
			            $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;'><span class='disabled'>&laquo; Previous</span></a></li>";


			        if ($lastpage < 7 + ($adjacents * 2)){  
			            for ($counter = 1; $counter <= $lastpage; $counter++){
			                if ($counter == $page)
			                    $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' class='active'><span>$counter</span></a></li>";
			                else
			                    $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getAjaxPaging(\"Messaging\",\"".$pages."\",\"".$counter."\");'>$counter</a></li>";
			            }
			        } 

			        elseif($lastpage > 5 + ($adjacents * 2)) {
						if($page < 1 + ($adjacents * 2)){
			                for($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
			                    if($counter == $page)
			                        $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' class='active'><span>$counter</span></a></li>";
			                    else
			                        $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getAjaxPaging(\"Messaging\",\"".$pages."\",\"".($counter)."\");'>$counter</a></li>";    
			                }
			                $pagination.= "<li style='padding: 0px;'><a style='padding: 6px 1px;'>...</a></li>";
			                //$pagination.= "<li><a href='javascript:void(0);' onClick='getAjaxPaging(\"Directory\",\"".$pages."\",\"".($lpm1)."\");'>$lpm1</a></li>";
			                $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getAjaxPaging(\"Messaging\",\"".$pages."\",\"".($lastpage)."\");'>$lastpage</a></li>";  

			 			} elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
			               $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getAjaxPaging(\"Messaging\",\"".$pages."\",\"1\");'>1</a></li>";
			               //$pagination.= "<li><a href='javascript:void(0);' onClick='getAjaxPaging(\"Directory\",\"".$pages."\",\"2\");'>2</a></li>";
			               $pagination.= "<li style='padding: 0px;'><a style='padding: 6px 1px;'>...</a></li>";

			               for($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
			                   if($counter == $page)
			                       $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' class='active'><span>$counter</span></a></li>";
			                   else
			                       $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getAjaxPaging(\"Messaging\",\"".$pages."\",\"".($counter)."\");'>$counter</a></li>";    
			               }
			               $pagination.= "<li style='padding: 0px;'><a style='padding: 6px 1px;'>...</a></li>";
			               //$pagination.= "<li><a href='javascript:void(0);' onClick='getAjaxPaging(\"Directory\",\"".$pages."\",\"".($lpm1)."\");'>$lpm1</a></li>";
			               $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getAjaxPaging(\"Messaging\",\"".$pages."\",\"".($lastpage)."\");'>$lastpage</a></li>";  

			           	} else {
			               $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getAjaxPaging(\"Messaging\",\"".$pages."\",\"1\");'>1</a></li>";
			               //$pagination.= "<li><a href='javascript:void(0);' onClick='getAjaxPaging(\"Directory\",\"".$pages."\",\"2\");'>2</a></li>";
			               $pagination.= "<li style='padding: 0px;'><a style='padding: 6px 1px;'>...</a></li>";
			               for($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
			                   if($counter == $page)
			                        $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' class='active'><span>$counter</span></a></li>";
			                   else
			                        $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getAjaxPaging(\"Messaging\",\"".$pages."\",\"".($counter)."\");'>$counter</a></li>";    
			               }
			           	}
			        }	

			        if($page < $counter - 1)
			            $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getAjaxPaging(\"Messaging\",\"".$pages."\",\"".($next)."\");'>Next &raquo;</a>";
			        else
			            $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;'><span class='disabled'>Next &raquo;</span></a></li>";

				/*for($t=1;$t<=$pages;$t++) {
					if($page==$t) {
						$pagination .= '<li><a class="active" href="javascript:void(0);"';
						if($pages > 1){
							$pagination .= 'onclick="getAjaxPaging(\'Messaging\',\''.$pages.'\',\''.$t.'\')"'; 
						}
						$pagination .= '>'.$t.'</a></li>';
						} else {
						$pagination .= '<li><a href="javascript:void(0);"';
						if($pages > 1){ 
							$pagination .= 'onclick="getAjaxPaging(\'Messaging\',\''.$pages.'\',\''.$t.'\')"';
						}
						$pagination .= '>'.$t.'</a></li>';
					}
				}*/
				$pagination .= '</div></ul>';	
			}

			$success = array("status" => 'true', 'data' => $dir_data, 'pagination' => $pagination, 'cate_data' => $categoty, 'fmi' => $fmi);

		} else {
			$dir_data = '<article class="msg_detail_hidden_data_0" style="display: none;">';
					$dir_data .= '<div class="bus_names">No record found.</div>';
			$dir_data .= '</article>';
			$dir_data .= '<div class="promo-table-view">';
				$dir_data .= '<div class="promo-table-view-main"><div class="promo-table-view-left">No record found.</div></div>';
			$dir_data .= '<div class="clear"></div></div>';

			$success = array("status" => 'true', 'data'=>$dir_data, 'pagination' => '', 'cate_data' => $categoty, 'fmi' => '0');
		}
	} else if($action == 'GetMsgListGraphNew') {
		
		$id 		=	$_REQUEST['direid'];
		$searchchar	=	$_REQUEST['char'];
	 	$orderBy	=	$_REQUEST['orderby'];
 		$xtracondition_o	=	$_REQUEST['xtracondition'];

		$SubcriberId			=	$_REQUEST['SubcriberID'];
		$SECTION_FIELD_PREFIX	=	$_REQUEST['fieldPrefix'];
		$SECTION_MANAGE_PAGE	=	$_REQUEST['managePagemessage'];	
		$searchchar 			= 	mysql_real_escape_string($_REQUEST['search_message']);
		$SECTION_TABLE			= 	$_REQUEST['tableName'];
		$xtraCondition			=	stripslashes($_REQUEST['xtraCondition']);
		$subemail				=	$_REQUEST['subemail'];
		$search					=	$_REQUEST['search'];
		
		$SECTION="Messaging";

		##################################  General Query ###############################################  
		
		if($id!="") {
	 		$meb_fields = array("meb_id");
			$meb_where  = "meb_dir_id= ".$id." AND meb_status != 'Deleted'";
			$mebRes 	= $db->selectData(TBL_MEMBER,$meb_fields,$meb_where,$extra="",1);
  			for($i=0;$i<count($mebRes);$i++) {
				if($i==count($mebRes)-1) {
					$addComma = "";
				} else {
					$addComma = ",";
				}
				$allMeb .= $mebRes[$i]["meb_id"].$addComma;
			}
	  		if ($xtracondition_o != 'undefined' && $xtracondition_o != "") {
		    	$xtracondition = $xtracondition_o." AND ";
		  	} else {
		        $xtracondition = "";
		  	}

  			if($id> 0) {
  				//$mebRes> 0
  				if($search!='') {
  					$sql="SELECT * FROM ".TBL_MEMBER_MESSAGING." LEFT JOIN ".TBL_BUSINESS_CATEGORY." as b ON b.buc_id = ".TBL_MEMBER_MESSAGING.".buc_id  WHERE ".$xtracondition." msg_dir_id = ".$id." AND msg_company_title LIKE '%".$search."%' AND msg_status != 'Deleted' AND msg_type = 'Published' ORDER BY ".$orderBy;
  				} else {
		    		//$sql="SELECT * FROM ".TBL_MEMBER_MESSAGING." LEFT JOIN ".TBL_BUSINESS_CATEGORY." as b ON b.buc_id = ".TBL_MEMBER_MESSAGING.".buc_id  WHERE ".$xtracondition." msg_meb_id IN (".$allMeb.")  AND msg_status != 'Deleted' ORDER BY ".$orderBy;
		    		$sql="SELECT * FROM ".TBL_MEMBER_MESSAGING." LEFT JOIN ".TBL_BUSINESS_CATEGORY." as b ON b.buc_id = ".TBL_MEMBER_MESSAGING.".buc_id  WHERE ".$xtracondition." msg_dir_id = ".$id."  AND msg_status != 'Deleted' AND msg_type = 'Published' ORDER BY ".$orderBy;
  				}
  				
		    	$msgResult  = $db->select($sql);
		     	$totMessage = count($msgResult);

		     	$per_page = 10;
				$pages = ceil($totMessage/$per_page);

				if($action_type == "paging") {
					if(!empty($_REQUEST['pagem']))	{
						$page = $_REQUEST['pagem'];
					}       
				} else {
					if(!empty($_REQUEST['pagem']))  
						$page = $_REQUEST['pagem'];
			  		else
						$page = 1;
				}

				$list_query = $sql;
				if(!empty($per_page) && $_REQUEST['page']!="all") {
					$start = ($page-1)*$per_page;
					if($start<0) {
						$start=0;
					}
					$list_query .= " limit $start,$per_page"; 
				}
				$result_query  = $db->select($list_query);  
				$total_rows = count($result_query);
		  	}

		  	//create array for read messges
		  	$read=array();
		  	$sql_sub="SELECT arc_msg_is FROM tbl_member_messaging_archive WHERE arc_sub_id = '".$SubcriberId."'";
		  	$sql_sub_data  = $db->select($sql_sub);
		  	if(count($sql_sub_data)>0) {
		  		for($i=0;$i<count($sql_sub_data);$i++) {
		  			array_push($read, $sql_sub_data[$i]['arc_msg_is']);
		  		}
		  	}
		  	//create array for read messges 
		} 

		#################################  Paging Query + Paging Code ##################################
		$categoty = '';
		$categoty = getBusinessCategoryList('bus_category','bus_category',$bus_category);

		if($total_rows > 0) {
			
			$dir_data = '';
			
			for($k=0;$k<$total_rows;$k++){
				if($k=='0')
					$fmi = $result_query[$k]['msg_id'];

				$class='';
				if(in_array($result_query[$k]['msg_id'], $read))
					$class='background: url(\''.ROOT_WWW.'images/email_read.png\') no-repeat scroll center center rgba(0, 0, 0, 0)';
				
				$dir_data .= '<li><a href="#" class="msg_detail_click" data_msg_id="'.$result_query[$k]['msg_id'].'">
								<span>'.date("m/d/y H:i",strtotime($result_query[$k]['msg_created_date'])).' |
									<strong>'.$msgResult[$k]['msg_company_title'].'</strong>
								</span><i class="icon_style small_icon email_icon_s" style="'.$class.'"></i></a></li>';
			}
		
			$pagination = '';
			if($pages>1) {
				$page = ($page == 0 ? 1 : $page);
				$start = ($page-1) * $per_page;
				$adjacents = "1";		

				$prev = $page - 1;
				$next = $page + 1;
				$lastpage = ceil($count/$per_page);
				$lpm1 = $lastpage - 1;

				$pagination = '<div class="paging"><ul>';
				
					if ($page > 1)
			            $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getAjaxPaging(\"Messaging\",\"".$pages."\",\"".$prev."\");'>&laquo; Previous</a></li>";
			        else
			            $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;'><span class='disabled'>&laquo; Previous</span></a></li>";


			        if ($lastpage < 7 + ($adjacents * 2)){  
			            for ($counter = 1; $counter <= $lastpage; $counter++){
			                if ($counter == $page)
			                    $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' class='active'><span>$counter</span></a></li>";
			                else
			                    $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getAjaxPaging(\"Messaging\",\"".$pages."\",\"".$counter."\");'>$counter</a></li>";
			            }
			        } 

			        elseif($lastpage > 5 + ($adjacents * 2)) {
						if($page < 1 + ($adjacents * 2)){
			                for($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
			                    if($counter == $page)
			                        $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' class='active'><span>$counter</span></a></li>";
			                    else
			                        $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getAjaxPaging(\"Messaging\",\"".$pages."\",\"".($counter)."\");'>$counter</a></li>";    
			                }
			                $pagination.= "<li style='padding: 0px;'><a style='padding: 6px 1px;'>...</a></li>";
			                //$pagination.= "<li><a href='javascript:void(0);' onClick='getAjaxPaging(\"Directory\",\"".$pages."\",\"".($lpm1)."\");'>$lpm1</a></li>";
			                $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getAjaxPaging(\"Messaging\",\"".$pages."\",\"".($lastpage)."\");'>$lastpage</a></li>";  

			 			} elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
			               $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getAjaxPaging(\"Messaging\",\"".$pages."\",\"1\");'>1</a></li>";
			               //$pagination.= "<li><a href='javascript:void(0);' onClick='getAjaxPaging(\"Directory\",\"".$pages."\",\"2\");'>2</a></li>";
			               $pagination.= "<li style='padding: 0px;'><a style='padding: 6px 1px;'>...</a></li>";

			               for($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
			                   if($counter == $page)
			                       $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' class='active'><span>$counter</span></a></li>";
			                   else
			                       $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getAjaxPaging(\"Messaging\",\"".$pages."\",\"".($counter)."\");'>$counter</a></li>";    
			               }
			               $pagination.= "<li style='padding: 0px;'><a style='padding: 6px 1px;'>...</a></li>";
			               //$pagination.= "<li><a href='javascript:void(0);' onClick='getAjaxPaging(\"Directory\",\"".$pages."\",\"".($lpm1)."\");'>$lpm1</a></li>";
			               $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getAjaxPaging(\"Messaging\",\"".$pages."\",\"".($lastpage)."\");'>$lastpage</a></li>";  

			           	} else {
			               $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getAjaxPaging(\"Messaging\",\"".$pages."\",\"1\");'>1</a></li>";
			               //$pagination.= "<li><a href='javascript:void(0);' onClick='getAjaxPaging(\"Directory\",\"".$pages."\",\"2\");'>2</a></li>";
			               $pagination.= "<li style='padding: 0px;'><a style='padding: 6px 1px;'>...</a></li>";
			               for($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
			                   if($counter == $page)
			                        $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' class='active'><span>$counter</span></a></li>";
			                   else
			                        $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getAjaxPaging(\"Messaging\",\"".$pages."\",\"".($counter)."\");'>$counter</a></li>";    
			               }
			           	}
			        }	

			        if($page < $counter - 1)
			            $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getAjaxPaging(\"Messaging\",\"".$pages."\",\"".($next)."\");'>Next &raquo;</a>";
			        else
			            $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;'><span class='disabled'>Next &raquo;</span></a></li>";

				
				$pagination .= '</div></ul>';	
			}

			$success = array("status" => 'true', 'data' => $dir_data, 'pagination' => $pagination, 'cate_data' => $categoty, 'fmi' => $fmi,'total_page'=>$pages);

		} else {
			$dir_data = '<article class="msg_detail_hidden_data_0" style="display: none;">';
					$dir_data .= '<div class="bus_names">No record found.</div>';
			$dir_data .= '</article>';
			$dir_data .= '<div class="promo-table-view">';
				$dir_data .= '<div class="promo-table-view-main"><div class="promo-table-view-left" style="padding:20px;">No messages found.</div></div>';
			$dir_data .= '<div class="clear"></div></div>';

			$success = array("status" => 'true', 'data'=>$dir_data, 'pagination' => '', 'cate_data' => $categoty, 'fmi' => '0');
		}
	} else if($action == 'GetMsgListList') {
		
		$id 		=	$_REQUEST['direid'];
		$searchchar	=	$_REQUEST['char'];
	 	$orderBy	=	$_REQUEST['orderby'];
 		$xtracondition_o	=	$_REQUEST['xtracondition'];

		$SubcriberId			=	$_REQUEST['SubcriberId'];
		$SECTION_FIELD_PREFIX	=	$_REQUEST['fieldPrefix'];
		$SECTION_MANAGE_PAGE	=	$_REQUEST['managePagemessage'];	
		$searchchar 			= 	mysql_real_escape_string($_REQUEST['search_message']);
		$SECTION_TABLE			= 	$_REQUEST['tableName'];
		$xtraCondition			=	stripslashes($_REQUEST['xtraCondition']);
		$subemail				=	$_REQUEST['subemail'];
		
		$SECTION="Messaging";

		##################################  General Query ###############################################  
		
		if($id!="") {
	 		$meb_fields = array("meb_id");
			$meb_where  = "meb_dir_id= ".$id." AND meb_status != 'Deleted'";
			$mebRes 	= $db->selectData(TBL_MEMBER,$meb_fields,$meb_where,$extra="",1);
  			for($i=0;$i<count($mebRes);$i++) {
				if($i==count($mebRes)-1) {
					$addComma = "";
				} else {
					$addComma = ",";
				}
				$allMeb .= $mebRes[$i]["meb_id"].$addComma;
			}
	  		if ($xtracondition_o != 'undefined' && $xtracondition_o != "") {
		    	$xtracondition = $xtracondition_o." AND ";
		  	} else {
		        $xtracondition = "";
		  	}

  			if($mebRes> 0) {
		    	$sql="SELECT * FROM ".TBL_MEMBER_MESSAGING." LEFT JOIN ".TBL_BUSINESS_CATEGORY." as b ON b.buc_id = ".TBL_MEMBER_MESSAGING.".buc_id  WHERE ".$xtracondition." msg_meb_id IN (".$allMeb.")  AND msg_status != 'Deleted' ORDER BY ".$orderBy;
		    	$msgResult  = $db->select($sql);
		     	$totMessage = count($msgResult);

		     	$per_page = 10;
				$pages = ceil($totMessage/$per_page);

				if($action_type == "paging") {
					if(!empty($_REQUEST['pagem']))	{
						$page = $_REQUEST['pagem'];
					}       
				} else {
					if(!empty($_REQUEST['pagem']))  
						$page = $_REQUEST['pagem'];
			  		else
						$page = 1;
				}

				$list_query = $sql;
				if(!empty($per_page) && $_REQUEST['page']!="all") {
					$start = ($page-1)*$per_page;
					if($start<0) {
						$start=0;
					}
					$list_query .= " limit $start,$per_page"; 
				}
				$result_query  = $db->select($list_query);  
				$total_rows = count($result_query);
		  	}
		} 

		#################################  Paging Query + Paging Code ##################################
		
		if($total_rows > 0) {
			
			$dir_data = '<div class="promo-table-view">';
			
			for($k=0;$k<$total_rows;$k++){
				if($k=='0')
					$fmi = $result_query[$k]['msg_id'];

				//$dir_data .= $result_query[$i]['Dirname'];
				$sub_con='';
				if($SUBSCRIBER_MAIL != '') {
					$sub_con='\'sub\'';
					$fun='setmessagedetailpage(\''.$result_query[$i][$SECTION_FIELD_PREFIX."id"].'\', \'msg\')';
				} else {
					$fun='setmessagedetailpage(\''.$result_query[$i][$SECTION_FIELD_PREFIX."id"].'\', \'msg\')';
				}
				$href = '#';
				//msg_details.html
				
				$dir_data .= '<div class="promo-table-view-main">
                    <div class="promo-table-view-left">'.date("m/d/y",strtotime($result_query[$k]['msg_created_date'])).' | '.$msgResult[$k]['buc_name'].' | '.$msgResult[$k]['msg_company_title'].'</div>
                    <div class="promo-table-view-right">
                        <a class="msg_detail_click" data_msg_id="'.$result_query[$k]['msg_id'].'" data-ajax="false" load="yes" href="#">
                        	<img border="0" src="images/msging.png">
                        </a>
                    </div>
                </div>';
			}
		
			$dir_data .= '<div class="clear"></div></div>';
			
			$categoty = '';
			$categoty = getBusinessCategoryList('bus_category','bus_category',$bus_category);

			$pagination = '';
			if(count($pages)>0) {
				$pagination = '<div class="paging"><ul>';
				for($t=1;$t<=$pages;$t++) {
					if($page==$t) {
						$pagination .= '<li><a class="active" href="javascript:void(0);"';
						if($pages > 1){
							$pagination .= 'onclick="getAjaxPaging(\'Messaging\',\''.$pages.'\',\''.$t.'\')"'; 
						}
						$pagination .= '>'.$t.'</a></li>';
						} else {
						$pagination .= '<li><a href="javascript:void(0);"';
						if($pages > 1){ 
							$pagination .= 'onclick="getAjaxPaging(\'Messaging\',\''.$pages.'\',\''.$t.'\')"';
						}
						$pagination .= '>'.$t.'</a></li>';
					}
				}
				$pagination .= '</div></ul>';	
			}

			$success = array("status" => 'true', 'data' => $dir_data, 'pagination' => $pagination, 'cate_data' => $categoty);

		} else {
			
			$dir_data = '<div class="promo-table-view">';
				$dir_data .= '<div class="promo-table-view-main"><div class="promo-table-view-left">No record found.</div></div>';
			$dir_data .= '<div class="clear"></div></div>';

			$success = array("status" => 'true', 'data'=>$dir_data, 'pagination' => '');
		}
	} else if($action == 'GetMsgDetailListGraph') {

		$msgid = $_REQUEST['msgid'];

		$sql="SELECT * FROM ".TBL_MEMBER_MESSAGING." LEFT JOIN ".TBL_BUSINESS_CATEGORY." as b ON b.buc_id = ".TBL_MEMBER_MESSAGING.".buc_id  WHERE msg_id = ".$msgid."  AND msg_status != 'Deleted'";
   		$dirRes  = $db->select($sql);

   		$title = $dirRes[0]["msg_title"];
   		if($title == '')
   			$title = '-';
   		$cate = $dirRes[0]["buc_name"];
   		if($cate == '')
   			$cate = '-';

		$dir_data = '<article class="tab-content new_ul_content"><div class="error_msg" style="display:none;"></div><ul class="view-massage">';
      	$dir_data .= '<li><b>Message</b><span class="sent-on">Date posted :'.date('m/d/Y',strtotime($dirRes[0]["msg_created_date"])).'</span></li>';
      
		$dir_data .= '<li><p>'.$dirRes[0]["msg_message"].'</p><div class="fl"><b>Categoty</b><span> '.$cate.' </span></div><div class="fl ar-txt" style="text-align: center;"><b>Title</b><span>'.$title.'</span></div></li>';
		
		if($dirRes[0]["msg_file"] != '') {
			
			$msg_file = UPLOAD_WWW_MESSAGING_FILE.$dirRes[0]["msg_id"]."/".$dirRes[0]["msg_file"];
		
			$fileNameExt = explode(".", $dirRes[0]["msg_file"]);
			$fileExt 	 = $fileNameExt[count($fileNameExt)-1];
			$allowedImgExts = array("gif","jpg","jpeg","png","GIF","JPG","JPEG","PNG");	
			$allowedVidExts = array("3gp","amv","avi","mp4");			
			if(in_array($fileExt,$allowedImgExts)) {
				$image = "<img src='".$msg_file."' height='115' width='115' style='height: 115px; width: 190px;'  />";
			} else if(in_array($fileExt,$allowedVidExts)) {
				$image = "<img src='images/play-button.jpg' height='115' width='115' style='height: 115px; width: 190px;' onclick='play_new_video(\"".$msg_file."\")' />";
			} else {
				$image = $dirRes[0]["msg_file"];
			}
			//onclick="return downloadf(\''.$msg_file.'\')"
			//id="startDl" onclick="onDeviceReady()"
			if(in_array($fileExt,$allowedVidExts)) {
				$dir_data .= '<li><div class="fl"><b>Attached File</b><a class="" i-uri="'.$msg_file.'" i-name="'.$dirRes[0]["msg_file"].'" href="">'.$image."</a></div></li>";
				$dir_data .= '<script> function play_new_video(vid) { window.plugins.videoPlayer.play(vid); }</script>';
			} else {
				$dir_data .= '<li><div class="fl"><b>Attached File</b><a class="download" i-uri="'.$msg_file.'" i-name="'.$dirRes[0]["msg_file"].'" href="">'.$image."</a></div></li>";
			}
		}
		
		//$dir_data .= '<li><b>Directory name :</b><span>'.$dirRes[0]["dir_name"].'</span></li>';
     	//$dir_data .= '<li></li>';
     	$dir_data .='</article>';
		if($dirRes[0]["msg_file"] != '') {
			$dir_data .= '<script>';
				$dir_data .= '$(".download").on("click", function(){
									var fileTransfer = new FileTransfer();
									var uri = encodeURI($(this).attr("i-uri"));
									var filePath = "file://sdcard/"+$(this).attr("i-name");
									fileTransfer.download(
    									uri,
    									filePath,
    								function(entry) {
        								console.log("download complete: " + entry.fullPath); 
									},
    								function(error) {
        								console.log("download error source " + error.source);
        								console.log("download error target " + error.target);
        								console.log("upload error code" + error.code);
    								},
    								false,
    								{
        								headers: {
            							"Authorization": "Basic dGVzdHVzZXJuYW1lOnRlc3RwYXNzd29yZA=="
        								}
    								}
								);
							})';
			$dir_data .= '</script>';     	
     	}
      $success = array("status" => 'true', 'data' => $dir_data); 				
			
	} else if($action == 'GetMsgDetailListGraphNew') {

		$msgid = $_REQUEST['msgid'];
		$SubcriberID = $_REQUEST['SubcriberID'];

		$sql_sub="SELECT * FROM ".tbl_member_messaging_archive." WHERE arc_msg_is = ".$msgid." AND arc_sub_id = '".$SubcriberID."'";
   		$sql_sub_data  = $db->select($sql_sub);
   		if(count($sql_sub_data)<=0){
   			$add_values['arc_msg_is'] = $msgid;									
			$add_values['arc_sub_id'] = $SubcriberID;
			$add_values['arc_created_date'] = date("Y-m-d H:i:s");
			$db->insertData('tbl_member_messaging_archive', $add_values);
   		}

		$sql="SELECT * FROM ".TBL_MEMBER_MESSAGING." LEFT JOIN ".TBL_BUSINESS_CATEGORY." as b ON b.buc_id = ".TBL_MEMBER_MESSAGING.".buc_id  WHERE msg_id = ".$msgid."  AND msg_status != 'Deleted'";
   		$dirRes  = $db->select($sql);

   		$title = $dirRes[0]["msg_title"];
   		if($title == '')
   			$title = '-';
   		$cate = $dirRes[0]["buc_name"];
   		if($cate == '')
   			$cate = '-';
		
   		$image='';
   		if($dirRes[0]["msg_file"] != '') {
   			$msg_file = UPLOAD_WWW_MESSAGING_FILE.$dirRes[0]["msg_id"]."/".$dirRes[0]["msg_file"];
   			$fileNameExt = explode(".", $dirRes[0]["msg_file"]);
   			$fileExt 	 = $fileNameExt[count($fileNameExt)-1];
            $allowedImgExts = array("gif","jpg","jpeg","png","GIF","JPG","JPEG","PNG");
   			$allowedVidExts = array("3gp","amv","avi","mp4");
   			if(in_array($fileExt,$allowedImgExts)) {
   				$image = "<img src='".$msg_file."' />";
   			} else {
                $image='<img onclick="return play_video_new(\''.$msg_file.'\')" src="images/play_icon.png">';
            }
   		}
   		//$image = "<img src='images/profile.png' />";
   		
   		
   		$dir_data = '<div class="user_content">';
		$dir_data .= $image.'<h3>'.$dirRes[0]["msg_company_title"].'</h3><p>'.$dirRes[0]["msg_message"].'</p>';
		$dir_data .= '</div>';

        $repo_link='';
        if($dirRes[0]["msg_has_question"]) {
            $repo_link .= '<div class="arrow_button">';
            $repo_link .= '<a href="response.html">Take Survey</a>';
            $repo_link .= '</div>';
        }

		if(false) {
			//$dirRes[0]["msg_file"] != ''
			$msg_file = UPLOAD_WWW_MESSAGING_FILE.$dirRes[0]["msg_id"]."/".$dirRes[0]["msg_file"];
		
			$fileNameExt = explode(".", $dirRes[0]["msg_file"]);
			$fileExt 	 = $fileNameExt[count($fileNameExt)-1];
			$allowedImgExts = array("gif","jpg","jpeg","png","GIF","JPG","JPEG","PNG");	
			$allowedVidExts = array("3gp","amv","avi","mp4");			
			if(in_array($fileExt,$allowedImgExts)) {
				$image = "<img src='".$msg_file."' height='115' width='115' style='height: 115px; width: 190px;'  />";
			} else if(in_array($fileExt,$allowedVidExts)) {
				$image = "<img src='images/play-button.jpg' height='115' width='115' style='height: 115px; width: 190px;' onclick='play_new_video(\"".$msg_file."\")' />";
			} else {
				$image = $dirRes[0]["msg_file"];
			}
			//onclick="return downloadf(\''.$msg_file.'\')"
			//id="startDl" onclick="onDeviceReady()"
			if(in_array($fileExt,$allowedVidExts)) {
				$dir_data .= '<li><div class="fl"><b>Attached File</b><a class="" i-uri="'.$msg_file.'" i-name="'.$dirRes[0]["msg_file"].'" href="">'.$image."</a></div></li>";
				$dir_data .= '<script> function play_new_video(vid) { window.plugins.videoPlayer.play(vid); }</script>';
			} else {
				$dir_data .= '<li><div class="fl"><b>Attached File</b><a class="download" i-uri="'.$msg_file.'" i-name="'.$dirRes[0]["msg_file"].'" href="">'.$image."</a></div></li>";
			}
		}
		if(false) {
			//$dirRes[0]["msg_file"] != ''
			$dir_data .= '<script>';
				$dir_data .= '$(".download").on("click", function(){
									var fileTransfer = new FileTransfer();
									var uri = encodeURI($(this).attr("i-uri"));
									var filePath = "file://sdcard/"+$(this).attr("i-name");
									fileTransfer.download(
    									uri,
    									filePath,
    								function(entry) {
        								console.log("download complete: " + entry.fullPath); 
									},
    								function(error) {
        								console.log("download error source " + error.source);
        								console.log("download error target " + error.target);
        								console.log("upload error code" + error.code);
    								},
    								false,
    								{
        								headers: {
            							"Authorization": "Basic dGVzdHVzZXJuYW1lOnRlc3RwYXNzd29yZA=="
        								}
    								}
								);
							})';
			$dir_data .= '</script>';     	
     	}
      $success = array("status" => 'true', 'data' => $dir_data,'repo_link'=>$repo_link);
			
	} else if($action == 'GetMsgResponce') {

        $msgid = $_REQUEST['msgid'];
        $SubcriberID = $_REQUEST['SubcriberID'];

        $sql="SELECT * FROM ".TBL_MEMBER_MESSAGING." LEFT JOIN ".TBL_BUSINESS_CATEGORY." as b ON b.buc_id = ".TBL_MEMBER_MESSAGING.".buc_id  WHERE msg_id = ".$msgid."  AND msg_status != 'Deleted'";
        $dirRes  = $db->select($sql);


        $que_query = 'SELECT * FROM '.TBL_QUESTION.' WHERE msg_id="'.$msgid.'"';
        $queResult  = $db->select($que_query);

        $msg_quetion_title='';
        $msg_quetion_id='';
        $queOptResult=array();
        $RespoResult=array();

        $has_user_take='';
        $has_user_que='';

        if(count($queResult)>0) {
            $msg_quetion_title = $queResult[0]['question'];
            $msg_quetion_id = $queResult[0]['que_id'];

            $que_query_user = 'SELECT repo.*, opt.que_id, opt.que_id, opt.opt_title FROM '.TBL_RESPONSE.' repo, '.TBL_QUESTION_OPTION.' opt WHERE repo.que_id="'.$msg_quetion_id.'" AND repo.user_id="'.$SubcriberID.'" AND repo.opt_id=opt.opt_id';
            $userResult  = $db->select($que_query_user);

            if(count($userResult)>0) {
                $has_user_take='yes';
                $has_user_que=$userResult[0]['opt_id'];
            }

            $que_opt_query = 'SELECT * FROM '.TBL_QUESTION_OPTION.' WHERE que_id="'.$msg_quetion_id.'"';
            $queOptResult  = $db->select($que_opt_query);

            $Respo_query = 'SELECT rep.*, opt.que_id, opt.que_id, opt_title, sub.sub_id, sub.sub_name FROM '.TBL_RESPONSE.' rep, '.TBL_QUESTION_OPTION.' opt, '.TBL_MEMBER_SUBSCRIBERS.' sub WHERE rep.que_id="'.$msg_quetion_id.'" AND rep.user_id=sub.sub_id AND rep.opt_id=opt.opt_id ORDER BY rep.res_id DESC';
            $RespoResult  = $db->select($Respo_query);
        }

        $dir_data='';

        $dir_data .='<div class="user_content"><h3>'.$msg_quetion_title.'</h3>';

        $dir_data .='<div class="options form_field">
                        <div class="form_row radio_conterol" data-role="fieldcontain">
                            <fieldset data-role="controlgroup">';
                            $des='';
                            $save_btn='<a href="#" onclick="return response_msg(); return false;"><i class="icon_style save_icon"></i>Submit</a>';
                            if($has_user_take=='yes') {
                                $des='disabled';
                                $save_btn='';
                            }
                            if(count($queOptResult)>0) {
                                //$has_user_que
                                foreach ($queOptResult as $queOpt) {
                                    $checked='';
                                    if($has_user_que==$queOpt['opt_id'])
                                        $checked='checked="checked"';

                                    $opt_id = $queOpt['opt_id'];
                                    $opt_title = $queOpt['opt_title'];
                                    $dir_data .='<input '.$des.' '.$checked.' type="radio" name="msg_option" id="opt_'.$opt_id.'" value="'.$opt_id.'" /><label for="opt_'.$opt_id.'">'.$opt_title.'</label>';
                                }
                            }
           $dir_data .='</fieldset></div><div class="submit_button hover_icon">'.$save_btn.'
                            <div class="arrow_button_back">
                                <a href="groupmessage-detail.html" class="back_btn_step"><span>Back to Broadcast</span></a>
                            </div>
                        </div>
                    </div>
                    </div>';

        /*$dir_data .= '<div class="max_width resList"><div class="teamLists"><ul>';
            if(count($RespoResult)>0) {
                foreach ($RespoResult as $Respo) {
                    $user_image='images/profile.png';
                    $user_name=$Respo['sub_name'];

                    $option_title=$Respo['opt_title'];
                    $datee = new DateTime($Respo['created_date']);
                    $repso_date= $datee->format('d/m/Y');

                    $dir_data .= '<li><img src="'.$user_image.'" /><div class="user_cont"><h2>'.$user_name.'</h2><div class="name_user">'.$option_title.' <span>'.$repso_date.'</span></div></div></li>';
                }
            } else {
                $dir_data .= '<li>No Survey</li>';
            }

         $dir_data .= '</ul></div></div>';*/


        $success = array("status" => 'true', 'data' => $dir_data, 'quetion_id'=>$msg_quetion_id);

    } else if($action == 'SaveResponce') {

        $msgid = $_REQUEST['msgid'];
        $SubcriberID = $_REQUEST['SubcriberID'];
        $Opt_id = $_REQUEST['opt_id'];
        $quetion_id = $_REQUEST['quetion_id'];

        if($Opt_id!='' && $quetion_id!='') {
            $ops_d='INSERT INTO '.TBL_RESPONSE.' (que_id,opt_id,user_id,created_date,edited_date) VALUES ("'.$quetion_id.'","'.$Opt_id.'","'.$SubcriberID.'","'.$today_date.'","'.$today_date.'")';
            $db->select($ops_d);
        }

        $sql="SELECT * FROM ".TBL_MEMBER_MESSAGING." LEFT JOIN ".TBL_BUSINESS_CATEGORY." as b ON b.buc_id = ".TBL_MEMBER_MESSAGING.".buc_id  WHERE msg_id = ".$msgid."  AND msg_status != 'Deleted'";
        $dirRes  = $db->select($sql);

        $que_query = 'SELECT * FROM '.TBL_QUESTION.' WHERE msg_id="'.$msgid.'"';
        $queResult  = $db->select($que_query);

        $msg_quetion_title='';
        $msg_quetion_id='';
        $queOptResult=array();
        $RespoResult=array();

        $has_user_take='';
        $has_user_que='';

        if(count($queResult)>0) {
            $msg_quetion_title = $queResult[0]['question'];
            $msg_quetion_id = $queResult[0]['que_id'];

            $que_query_user = 'SELECT repo.*, opt.que_id, opt.que_id, opt.opt_title FROM '.TBL_RESPONSE.' repo, '.TBL_QUESTION_OPTION.' opt WHERE repo.que_id="'.$msg_quetion_id.'" AND repo.user_id="'.$SubcriberID.'" AND repo.opt_id=opt.opt_id';
            $userResult  = $db->select($que_query_user);

            if(count($userResult)>0) {
                $has_user_take='yes';
                $has_user_que=$userResult[0]['opt_id'];
            }

            $que_opt_query = 'SELECT * FROM '.TBL_QUESTION_OPTION.' WHERE que_id="'.$msg_quetion_id.'"';
            $queOptResult  = $db->select($que_opt_query);

            $Respo_query = 'SELECT rep.*, opt.que_id, opt.que_id, opt_title, sub.sub_id, sub.sub_name FROM '.TBL_RESPONSE.' rep, '.TBL_QUESTION_OPTION.' opt, '.TBL_MEMBER_SUBSCRIBERS.' sub WHERE rep.que_id="'.$msg_quetion_id.'" AND rep.user_id=sub.sub_id AND rep.opt_id=opt.opt_id ORDER BY rep.res_id DESC';
            $RespoResult  = $db->select($Respo_query);
        }

        $dir_data='';

        $dir_data .='<div class="user_content"><h3>'.$msg_quetion_title.'</h3>';

        $dir_data .='<div class="options form_field">
                        <div class="form_row radio_conterol" data-role="fieldcontain">
                            <fieldset data-role="controlgroup">';
        $des='';
        $save_btn='<a href="#" onclick="return response_msg(); return false;"><i class="icon_style save_icon"></i>Submit</a>';
        if($has_user_take=='yes') {
            $des='disabled';
            $save_btn='';
        }
        if(count($queOptResult)>0) {
            //$has_user_que
            foreach ($queOptResult as $queOpt) {
                $checked='';
                if($has_user_que==$queOpt['opt_id'])
                    $checked='checked="checked"';

                $opt_id = $queOpt['opt_id'];
                $opt_title = $queOpt['opt_title'];
                $dir_data .='<input '.$des.' '.$checked.' type="radio" name="msg_option" id="opt_'.$opt_id.'" value="'.$opt_id.'" /><label for="opt_'.$opt_id.'">'.$opt_title.'</label>';
            }
        }
        $dir_data .='</fieldset></div><div class="submit_button hover_icon">'.$save_btn.'
                            <div class="arrow_button_back">
                                <a href="groupmessage-detail.html" class="back_btn_step"><span>Back to Broadcast</span></a>
                            </div>
                        </div>
                    </div>
                    </div>';

        $dir_data .= '<div class="max_width resList"><div class="teamLists"><ul>';
        if(count($RespoResult)>0) {
            foreach ($RespoResult as $Respo) {
                $user_image='images/profile.png';
                $user_name=$Respo['sub_name'];

                $option_title=$Respo['opt_title'];
                $datee = new DateTime($Respo['created_date']);
                $repso_date= $datee->format('d/m/Y');

                $dir_data .= '<li><img src="'.$user_image.'" /><div class="user_cont"><h2>'.$user_name.'</h2><div class="name_user">'.$option_title.' <span>'.$repso_date.'</span></div></div></li>';
            }
        } else {
            $dir_data .= '<li>No Survey</li>';
        }

        $dir_data .= '</ul></div></div>';


        $success = array("status" => 'true', 'data' => $dir_data, 'quetion_id'=>$msg_quetion_id);

    } else {
		$success = array("status" => 'false');
	}
	echo json_encode($success);
	exit();
?>