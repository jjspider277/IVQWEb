<?php
	error_reporting(1);
	header('Access-Control-Allow-Origin: *');
	ini_set('memory_limit','128M');

	include_once('../../../include/includeclass.php');
	$success = array();
	$action = $_REQUEST['action'];
	if($action == 'GetDirList') {
		$SECTION_FIELD_PREFIX = $_REQUEST['fieldPrefix'];
		$managePage = $_REQUEST['managePage'];
		$searchchar = mysql_real_escape_string($_REQUEST['search']);
		$SECTION_TABLE = $_REQUEST['tableName'];
		$xtraCondition = $_REQUEST['xtraCondition'];
		$SECTION_MANAGE_PAGE 	=   ADM_MANAGE_DIRECTORY;
		$SECTION_TABLE_CATEGORY	=   TBL_DIRECTORY_CATEGORY;
		$orderby = "name";
		$ORDER =	"asc";
		
		$uploadFILEWWW = UPLOAD_WWW_DIRECTORY_FILE;

		//dir category
		$cate_id = $_REQUEST['cate_id'];
		$cate_where = '';
		if($cate_id!='')
			$cate_where = " AND dir_dic_id='".$cate_id."' ";

		//subscriber
		$SUBSCRIBER_MAIL = $_REQUEST['subemail'];
		$SubcriberID = $_REQUEST['SubcriberId'];
		
		//near me
		$loca_con = $_REQUEST['loc_con'];
		$locations = $_REQUEST['locations'];			
		
		$sub_qury='';
		if($SUBSCRIBER_MAIL != '') {
			$SECTION_TABLE_SUB = TBL_MEMBER_SUBSCRIBERS;
			$SECTION_TABLE_SUB_DIR = TBL_SUBSCRIBER_INV;
			
			$sub_qury = ' AND p.dir_id in (select inv_dir_id from '.$SECTION_TABLE_SUB_DIR.' where inv_sub_id="'.$SubcriberID.'" and inv_status="Active")';
		}
		//subscriber
		//near me
		$loc_qry='';
		if($loca_con == 'yes') {
			//$loc_qry = ' AND (dir_address IN ('.$locations.') OR dir_city IN ('.$locations.'))';
			$loc_qry = ' AND (dir_city IN ('.$locations.'))';
		}
		//near me
		
		$dirName = "(select dic_name from ".$SECTION_TABLE_CATEGORY." where dic_id = dir_dic_id ) as Dirname";		
		if ($searchchar != "") {
    		 $sql_query = "SELECT p.*,".$dirName.",(select sta_name from tbl_state where sta_id=dir_sta_id) as dir_state FROM " . $SECTION_TABLE . " p WHERE p." . $SECTION_FIELD_PREFIX . "id != 0 AND p.".$SECTION_FIELD_PREFIX."status!='Deleted' AND p.".$SECTION_FIELD_PREFIX."status!='Inactive' ".$cate_where." AND (`dir_name` LIKE '%".mysql_real_escape_string($searchchar)."%' ".$sub_qury." OR dir_city LIKE '%".mysql_real_escape_string($searchchar)."%' OR dir_zip LIKE '%".mysql_real_escape_string($searchchar)."%' OR dir_sta_id IN (SELECT sta_id FROM tbl_state WHERE sta_name LIKE '%".mysql_real_escape_string($searchchar)."%'))".$loc_qry." order by p." . $SECTION_FIELD_PREFIX . $orderby . " " . $order;
    	} else if ($xtraCondition != 'undefined' && $xtraCondition != "") {
			$ts = "%" . $xtraCondition . "%";
			$sql_query = "select *,".$dirName.",(select sta_name from tbl_state where sta_id=dir_sta_id) as dir_state from ".$SECTION_TABLE." as p where (".$xtraCondition.") AND (p.".$SECTION_FIELD_PREFIX."status!='Deleted' ".$cate_where." AND p.".$SECTION_FIELD_PREFIX."status!='Inactive') ".$sub_qury."".$loc_qry." order by " . $SECTION_FIELD_PREFIX . $orderby . " " . $order;
		} else {
			$sql_query = "SELECT p.*,".$dirName.",(select sta_name from tbl_state where sta_id=dir_sta_id) as dir_state FROM ".$SECTION_TABLE." p WHERE p.".$SECTION_FIELD_PREFIX."id != 0 ".$cate_where." AND p.".$SECTION_FIELD_PREFIX."status != 'Deleted' AND p.".$SECTION_FIELD_PREFIX."status != 'Inactive'".$sub_qury."".$loc_qry." order by p.".$SECTION_FIELD_PREFIX.$orderby." ".$order;
            //AND p.".$SECTION_FIELD_PREFIX."featured = 'yes'
		}		
		
		//echo $sql_query;
		if ($searchchar != "") {
			$excel_query = "SELECT p.*,".$dirName.",(select sta_name from tbl_state where sta_id=dir_sta_id) as dir_state FROM ".$SECTION_TABLE." p WHERE p.".$SECTION_FIELD_PREFIX."id != 0 AND p.".$SECTION_FIELD_PREFIX."status != 'Deleted' ".$cate_where." AND p.".$SECTION_FIELD_PREFIX."status != 'Inactive' AND (`dir_name` LIKE '%".mysql_real_escape_string($searchchar)."%' ".$sub_qury." OR dir_city LIKE '%".mysql_real_escape_string($searchchar)."%' OR dir_zip LIKE '%".mysql_real_escape_string($searchchar)."%' OR dir_sta_id IN (SELECT sta_id FROM tbl_state WHERE sta_name LIKE '%".mysql_real_escape_string($searchchar)."%')) ".$sub_qury.$loc_qry." order by p.".$SECTION_FIELD_PREFIX.$orderby." ".$order;
			$excel_result  = $db->select($excel_query);
		} else {
			$excel_query = "SELECT p.*,".$dirName.",(select sta_name from tbl_state where sta_id=dir_sta_id) as dir_state FROM ".$SECTION_TABLE." p WHERE p.".$SECTION_FIELD_PREFIX."id != 0 ".$cate_where." AND p.".$SECTION_FIELD_PREFIX."status != 'Deleted' AND p.".$SECTION_FIELD_PREFIX."status != 'Inactive' ".$sub_qury."".$loc_qry." order by p.".$SECTION_FIELD_PREFIX.$orderby." ".$order;
            //AND p.".$SECTION_FIELD_PREFIX."featured = 'yes'
			$excel_result  = $db->select($excel_query);
		}	
		
		//echo $excel_query;

		$excelcount = count($excel_result);
		#################################  Paging Query + Paging Code ##################################
		$paging_query = $sql_query;
		$paging_result = $db->select($paging_query);	
		$count = count($paging_result);
		$per_page = SITE_PAGING_PER_PAGE; //rows per page
		$per_page = 10;
		$pages = ceil($count/$per_page);		
		#################################################################################################
		if(!empty($_REQUEST['page']))	
			$page = $_REQUEST['page'];
		else
			$page = 1;
		
		$list_query = $sql_query;
		if(!empty($per_page) && $_REQUEST['page']!="all")
		{
			$start = ($page-1)*$per_page;
			if($start<0)
			{
				$start=0;
			}
			$list_query .= " limit $start,$per_page";	
		}

		$result_query = $db->select($list_query);	
		$total_rows = count($result_query);
		//echo $total_rows;

		$dir_data = '';
		//category list 
		$cate_list='';
		if($page <=1) {
			
			$sql_query_cate = "SELECT * from  tbl_dir_category WHERE dic_status='Active' order by dic_order ASC";
			$cate_date = $db->select($sql_query_cate);
			if(count($cate_date)>0){
				$cate_list .='<div class="catgory_listw catgory_list_home" style="display:none;"><ul>';
				foreach ($cate_date as $value_d) {
					//var_dump($value_d['dic_id']);	
					$selecte='';
					if($value_d['dic_id']==$cate_id)
						$selecte='active';

					$cate_list .='<li class="'.$selecte.'"><a href="javascript:void(0);" data_id="'.$value_d['dic_id'].'">'.$value_d['dic_name'].'</a></li>';
				}
				$cate_list .='</ul></div>';
			}	
		}
		$dir_data .= $cate_list;
		//category list 

		if($total_rows > 0){
			$j = 1;
			$dir_data .= '<ul>';
			for($i=0;$i<$total_rows;$i++){
				//var_dump($result_query[$i]);
				//$dir_data .= $result_query[$i]['Dirname'];
				$sub_con='';
				if($SUBSCRIBER_MAIL != '') {
					$sub_con='\'sub\'';
					$fun='setdetailpage(\''.$result_query[$i][$SECTION_FIELD_PREFIX."id"].'\', '.$sub_con.')';
				} else {
					$fun='setdetailpage(\''.$result_query[$i][$SECTION_FIELD_PREFIX."id"].'\')';
				}
				$fun='setdetailpage(\''.$result_query[$i][$SECTION_FIELD_PREFIX."id"].'\')';
				$href = '#';
				//$dir_data .= '<li><a onclick="'.$fun.'" href="'.$href.'" data-ajax="true" load="yes" data-prefetch="true">'.$result_query[$i][$SECTION_FIELD_PREFIX.'name'].'<div class="dir_extra"><span>City:</span><span class="text">'.$result_query[$i]['dir_city'].'</span><span>State:</span><span class="text">'.$result_query[$i]['dir_state'].'</span><span>Zip Code:</span><span class="text">'.$result_query[$i]['dir_zip'].'</span></div></a></li>';
				
				$logo_src='';
				if($result_query[$i][$SECTION_FIELD_PREFIX.'logo_file']!='') {
					$logo_src=$uploadFILEWWW.$result_query[$i][$SECTION_FIELD_PREFIX.'id'].'/'.$result_query[$i][$SECTION_FIELD_PREFIX.'logo_file'];
				} else {
					//$logo_src='images/no_logo.png';
                    $logo_src='images/member_group_top.png';
				}

                $dir_data .= '<li><a onclick="'.$fun.'" href="'.$href.'" data-ajax="true" load="yes" data-prefetch="true"><img src="'.$logo_src.'" style="max-height:24px;">';
				//$dir_data .= '<span><b>'.$result_query[$i][$SECTION_FIELD_PREFIX.'name'].'</b> | '.$result_query[$i]['dir_city'].' | '.$result_query[$i]['dir_state'].' | '.$result_query[$i]['dir_zip'].'</span><span></span>';
                $dir_data .= '<div class="dir_list_over"><span><b>'.$result_query[$i][$SECTION_FIELD_PREFIX.'name'].'</b></span></div>';
				$dir_data .= '</a></li>';

			}

			$dir_data .= '</ul>';

			if($pages>1) {

				$page = ($page == 0 ? 1 : $page);
				$start = ($page-1) * $per_page;
				$adjacents = "1";		

				$prev = $page - 1;
				$next = $page + 1;
				$lastpage = ceil($count/$per_page);
				$lpm1 = $lastpage - 1;   

				$pagination = '<div class="paging"><ul>';

					if ($page > 1)
			            $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getAjaxPaging(\"Directory\",\"".$pages."\",\"".$prev."\");'>&laquo; Previous</a></li>";
			        else
			            $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;'><span class='disabled'>&laquo; Previous</span></a></li>";


			        if ($lastpage < 7 + ($adjacents * 2)){  
			            for ($counter = 1; $counter <= $lastpage; $counter++){
			                if ($counter == $page)
			                    $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' class='active'><span>$counter</span></a></li>";
			                else
			                    $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getAjaxPaging(\"Directory\",\"".$pages."\",\"".$counter."\");'>$counter</a></li>";
			            }
			        } 

			        elseif($lastpage > 5 + ($adjacents * 2)) {
						if($page < 1 + ($adjacents * 2)){
			                for($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
			                    if($counter == $page)
			                        $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' class='active'><span>$counter</span></a></li>";
			                    else
			                        $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getAjaxPaging(\"Directory\",\"".$pages."\",\"".($counter)."\");'>$counter</a></li>";    
			                }
			                $pagination.= "<li style='padding: 0px;'><a style='padding: 6px 1px;'>...</a></li>";
			                //$pagination.= "<li><a href='javascript:void(0);' onClick='getAjaxPaging(\"Directory\",\"".$pages."\",\"".($lpm1)."\");'>$lpm1</a></li>";
			                $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getAjaxPaging(\"Directory\",\"".$pages."\",\"".($lastpage)."\");'>$lastpage</a></li>";  

			 			} elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
			               $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getAjaxPaging(\"Directory\",\"".$pages."\",\"1\");'>1</a></li>";
			               //$pagination.= "<li><a href='javascript:void(0);' onClick='getAjaxPaging(\"Directory\",\"".$pages."\",\"2\");'>2</a></li>";
			               $pagination.= "<li style='padding: 0px;'><a style='padding: 6px 1px;'>...</a></li>";

			               for($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
			                   if($counter == $page)
			                       $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' class='active'><span>$counter</span></a></li>";
			                   else
			                       $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getAjaxPaging(\"Directory\",\"".$pages."\",\"".($counter)."\");'>$counter</a></li>";    
			               }
			               $pagination.= "<li style='padding: 0px;'><a style='padding: 6px 1px;'>...</a></li>";
			               //$pagination.= "<li><a href='javascript:void(0);' onClick='getAjaxPaging(\"Directory\",\"".$pages."\",\"".($lpm1)."\");'>$lpm1</a></li>";
			               $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getAjaxPaging(\"Directory\",\"".$pages."\",\"".($lastpage)."\");'>$lastpage</a></li>";  

			           	} else {
			               $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getAjaxPaging(\"Directory\",\"".$pages."\",\"1\");'>1</a></li>";
			               //$pagination.= "<li><a href='javascript:void(0);' onClick='getAjaxPaging(\"Directory\",\"".$pages."\",\"2\");'>2</a></li>";
			               $pagination.= "<li style='padding: 0px;'><a style='padding: 6px 1px;'>...</a></li>";
			               for($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
			                   if($counter == $page)
			                        $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' class='active'><span>$counter</span></a></li>";
			                   else
			                        $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getAjaxPaging(\"Directory\",\"".$pages."\",\"".($counter)."\");'>$counter</a></li>";    
			               }
			           	}
			        }	

			        if($page < $counter - 1)
			            $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getAjaxPaging(\"Directory\",\"".$pages."\",\"".($next)."\");'>Next &raquo;</a>";
			        else
			            $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;'><span class='disabled'>Next &raquo;</span></a></li>";

				/*for($t=1;$t<=$pages;$t++) {
					if($page==$t) {
						$pagination .= '<li><a class="active" href="javascript:void(0);"';
						if($pages > 1){
							$pagination .= 'onclick="getAjaxPaging(\'Directory\',\''.$pages.'\',\''.$t.'\')"'; 
						}
						$pagination .= '>'.$t.'</a></li>';
						} else {
						$pagination .= '<li><a href="javascript:void(0);"';
						if($pages > 1){ 
							$pagination .= 'onclick="getAjaxPaging(\'Directory\',\''.$pages.'\',\''.$t.'\')"';
						}
						$pagination .= '>'.$t.'</a></li>';
					}
				}*/

				$pagination .= '</div></ul>';	
				
				$pagination .= '<script>';	
				$pagination .= 'function getAjaxPaging(action, total, current){ ';
				$pagination .= '$("#page_val").val(current);'; 
				$pagination .= 'getdirectorys("no");}';	
				$pagination .= '</script>';	
						
			}
			
			$success = array("status" => 'true', 'data' => $dir_data, 'pagination' => '', 'total_page'=>$pages);			
		} else {
			$dir_data .= '<ul>';
			$dir_data .= '<li><a href="javascript:void(0);">No groups found.</a></li>';
			$dir_data .= '</ul>';
			$success = array("status" => 'true', 'data'=>$dir_data, 'pagination' => '');
		}	
	} elseif($action == 'GetFeatureDirList') {
        $SECTION_FIELD_PREFIX = $_REQUEST['fieldPrefix'];
        $managePage = $_REQUEST['managePage'];
        $SECTION_TABLE = $_REQUEST['tableName'];
        $SECTION_MANAGE_PAGE 	=   ADM_MANAGE_DIRECTORY;
        $SECTION_TABLE_CATEGORY	=   TBL_DIRECTORY_CATEGORY;
        $orderby = "name";
        $ORDER =	"asc";

        $uploadFILEWWW = UPLOAD_WWW_DIRECTORY_FILE;

        //subscriber
        $SUBSCRIBER_MAIL = $_REQUEST['subemail'];
        $SubcriberID = $_REQUEST['SubcriberId'];


        $sub_qury='';
        if($SUBSCRIBER_MAIL != '') {
            $SECTION_TABLE_SUB = TBL_MEMBER_SUBSCRIBERS;
            $SECTION_TABLE_SUB_DIR = TBL_SUBSCRIBER_INV;

            $sub_qury = ' AND p.dir_id in (select inv_dir_id from '.$SECTION_TABLE_SUB_DIR.' where inv_sub_id="'.$SubcriberID.'" and inv_status="Active")';
        }
        //subscriber

        $dirName = "(select dic_name from ".$SECTION_TABLE_CATEGORY." where dic_id = dir_dic_id ) as Dirname";

        $sql_query = "SELECT p.*,".$dirName.",(select sta_name from tbl_state where sta_id=dir_sta_id) as dir_state FROM ".$SECTION_TABLE." p WHERE p.".$SECTION_FIELD_PREFIX."id != 0 AND p.".$SECTION_FIELD_PREFIX."featured = 'yes' ".$cate_where." AND p.".$SECTION_FIELD_PREFIX."status != 'Deleted' AND p.".$SECTION_FIELD_PREFIX."status != 'Inactive'".$sub_qury." ORDER BY RAND()";

        //echo $sql_query;
        $excel_query = "SELECT p.*,".$dirName.",(select sta_name from tbl_state where sta_id=dir_sta_id) as dir_state FROM ".$SECTION_TABLE." p WHERE p.".$SECTION_FIELD_PREFIX."id != 0 AND p.".$SECTION_FIELD_PREFIX."featured = 'yes' ".$cate_where." AND p.".$SECTION_FIELD_PREFIX."status != 'Deleted' AND p.".$SECTION_FIELD_PREFIX."status != 'Inactive' ".$sub_qury." ORDER BY RAND()";
        $excel_result  = $db->select($excel_query);

        //echo $excel_query;

        $excelcount = count($excel_result);
        #################################  Paging Query + Paging Code ##################################
        $paging_query = $sql_query;
        $paging_result = $db->select($paging_query);
        $count = count($paging_result);
        $per_page = SITE_PAGING_PER_PAGE; //rows per page
        $per_page = 10;
        $pages = ceil($count/$per_page);
        #################################################################################################
        if(!empty($_REQUEST['page']))
            $page = $_REQUEST['page'];
        else
            $page = 1;

        $list_query = $sql_query;
        if(!empty($per_page) && $_REQUEST['page']!="all")
        {
            $start = ($page-1)*$per_page;
            if($start<0)
            {
                $start=0;
            }
            $list_query .= " limit $start,$per_page";
        }

        $result_query = $db->select($list_query);
        $total_rows = count($result_query);
        //echo $total_rows;

        $dir_data = '';
        //category list
        $cate_list='';
        if($page <=1) {

            $sql_query_cate = "SELECT * from  tbl_dir_category WHERE dic_status='Active' order by dic_order ASC";
            $cate_date = $db->select($sql_query_cate);
            if(count($cate_date)>0){
                $cate_list .='<div class="catgory_listw catgory_list_home" style="display:none;"><ul>';
                foreach ($cate_date as $value_d) {
                    //var_dump($value_d['dic_id']);
                    $selecte='';
                    if($value_d['dic_id']==$cate_id)
                        $selecte='active';

                    $cate_list .='<li class="'.$selecte.'"><a href="javascript:void(0);" data_id="'.$value_d['dic_id'].'">'.$value_d['dic_name'].'</a></li>';
                }
                $cate_list .='</ul></div>';
            }
        }
        $dir_data .= $cate_list;
        //category list

        if($total_rows > 0){
            $j = 1;
            $dir_data .= '<ul>';
            for($i=0;$i<$total_rows;$i++){
                //var_dump($result_query[$i]);
                //$dir_data .= $result_query[$i]['Dirname'];
                $sub_con='';
                if($SUBSCRIBER_MAIL != '') {
                    $sub_con='\'sub\'';
                    $fun='setdetailpage(\''.$result_query[$i][$SECTION_FIELD_PREFIX."id"].'\', '.$sub_con.')';
                } else {
                    $fun='setdetailpage(\''.$result_query[$i][$SECTION_FIELD_PREFIX."id"].'\')';
                }
                $fun='setdetailpage(\''.$result_query[$i][$SECTION_FIELD_PREFIX."id"].'\')';
                $href = '#';
                //$dir_data .= '<li><a onclick="'.$fun.'" href="'.$href.'" data-ajax="true" load="yes" data-prefetch="true">'.$result_query[$i][$SECTION_FIELD_PREFIX.'name'].'<div class="dir_extra"><span>City:</span><span class="text">'.$result_query[$i]['dir_city'].'</span><span>State:</span><span class="text">'.$result_query[$i]['dir_state'].'</span><span>Zip Code:</span><span class="text">'.$result_query[$i]['dir_zip'].'</span></div></a></li>';

                $logo_src='';
                if($result_query[$i][$SECTION_FIELD_PREFIX.'logo_file']!='') {
                    $logo_src=$uploadFILEWWW.$result_query[$i][$SECTION_FIELD_PREFIX.'id'].'/'.$result_query[$i][$SECTION_FIELD_PREFIX.'logo_file'];
                } else {
                    //$logo_src='images/no_logo.png';
                    $logo_src='images/member_group_top.png';
                }

                $dir_data .= '<li><a onclick="'.$fun.'" href="'.$href.'" data-ajax="true" load="yes" data-prefetch="true"><img src="'.$logo_src.'" style="max-height:24px;">';
                //$dir_data .= '<span><b>'.$result_query[$i][$SECTION_FIELD_PREFIX.'name'].'</b> '.$result_query[$i]['dir_city'].' | '.$result_query[$i]['dir_state'].' | '.$result_query[$i]['dir_zip'].'</span><span></span>';
                $dir_data .= '<div class="dir_list_over"><span><b>'.$result_query[$i][$SECTION_FIELD_PREFIX.'name'].'</b></span></div>';
                $dir_data .= '</a></li>';

            }

            $dir_data .= '</ul>';

            $success = array("status" => 'true', 'data' => $dir_data, 'pagination' => '', 'total_page'=>$pages);
        } else {
            $dir_data .= '<ul>';
            $dir_data .= '<li><a href="javascript:void(0);">No groups found.</a></li>';
            $dir_data .= '</ul>';
            $success = array("status" => 'true', 'data'=>$dir_data, 'pagination' => '');
        }
    } else if($action == 'GetDirecotyCards') {
		$dirId = $_REQUEST['direid'];
		$sub_email = $_REQUEST['email'];
		$SubcriberId = $_REQUEST['SubcriberID'];
		$subribe	= $_REQUEST['subribe'];
		$orderBy	= $_REQUEST['orderby'];
		$xtracondition_o	=	$_REQUEST['xtracondition'];

		$dir_fields = array("mbm_meb_id");
		$dir_where  = "mbm_dir_id = ".$dirId." AND mbm_status = 'Active'";
		$dirRes 	= $db->selectData(TBL_MEMBER_DIRECTORY,$dir_fields,$dir_where,$extra="",2);		

		if(count($dirRes) > 0) {
			$id='';
			for($i=0;$i<count($dirRes);$i++){
				$id .= $dirRes[$i]["mbm_meb_id"].',';
			}
		}		
		$id = substr($id, 0, -1);

		$meb_fields = array("meb_id");
		$meb_where  = "meb_dir_id= ".$dirId." AND meb_status != 'Deleted'";
		$mebRes 	= $db->selectData(TBL_MEMBER,$meb_fields,$meb_where,$extra="",2);
		
		for($i=0;$i<count($mebRes);$i++) {
			if($i==count($mebRes)-1) {
				$addComma = "";
			} else {
				$addComma = ",";
			}
			$allMeb .= $mebRes[$i]["meb_id"].$addComma;
		}

		$allMeb = $dirId;

		$sub_fields = array("sbm_sub_id");
		$sub_where  = "sbm_dir_id= ".$dirId." AND sbm_status != 'Deleted'";
		$subRes 	= $db->selectData(TBL_MEMBER_SUBSCRIBE_DIRECTORY,$sub_fields,$sub_where,$extra="",2);
		//$sql ="select sub_id from ".TBL_MEMBER_SUBSCRIBERS." WHERE sub_dir_id=".$id;
		for($j=0;$j<count($subRes);$j++) {
			if($j==count($subRes)-1) {
				$addComma = "";
			} else {
				$addComma = ",";
			}
			$allSub .= $subRes[$j]["sbm_sub_id"].$addComma;
		}

		$searchchar = $_REQUEST['char'];
		
		if ($xtracondition_o != 'undefined' && $xtracondition_o != "") {
        	$xtracondition = $xtracondition_o." AND ";
  		} else {
        	$xtracondition = "";
  		}

  		if($dirRes> 0 && $subRes>0) {
			if ($searchchar != 'undefined' && $searchchar != "") {
				if ($searchchar == 'Other') {
					$ts = $searchchar . "%";
					$sql="SELECT * , 'Member' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS." WHERE ". $xtracondition." bus_title REGEXP '^[^a-zA-Z]' AND bus_dir_id IN (".$allMeb.")  AND bus_status != 'Deleted' UNION SELECT * , 'Subscriber' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS_SUB." WHERE ". $xtracondition." bus_title REGEXP '^[^a-zA-Z]' AND bus_sub_id IN (".$allSub.")  AND bus_status != 'Deleted' ORDER BY ".$orderBy;
				} else {
					if ($searchchar == 'All') {
						$sql="SELECT * , 'Member' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS." WHERE ". $xtracondition." bus_dir_id IN (".$allMeb.") AND bus_status != 'Deleted' UNION SELECT * , 'Subscriber' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS_SUB." WHERE ". $xtracondition." bus_sub_id IN (".$allSub.") AND bus_status != 'Deleted' ORDER BY ".$orderBy;
					} else {
     				//FOR Bus_category  
                  	if($orderBy == "bus_category") {
                    	$ts = "bus_buc_id IN (SELECT buc_id FROM tbl_business_category WHERE buc_name like '". $searchchar . "%')";
                  	} else {
                       $ts = $orderBy." LIKE '". $searchchar . "%'";
                  	}
                  	//FOR Bus_category 
					//$ts = $searchchar . "%";
					$sql="SELECT * , 'Member' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS." WHERE ". $xtracondition . $ts ." AND bus_dir_id IN (".$allMeb.") AND bus_status != 'Deleted' UNION SELECT * , 'Subscriber' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS_SUB." WHERE ". $xtracondition . $ts ." AND bus_sub_id IN (".$allSub.")  AND bus_status != 'Deleted' ORDER BY ".$orderBy;
					}
				}
			} else {
				$sql="SELECT * , 'Member' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS." WHERE ". $xtracondition ." bus_dir_id IN (".$allMeb.") AND bus_status != 'Deleted' UNION SELECT * , 'Subscriber' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS_SUB." WHERE ". $xtracondition." bus_sub_id IN (".$allSub.")  AND bus_status != 'Deleted' ORDER BY ".$orderBy;
			}   //echo $sql;
			$BusResult  = $db->select($sql);
			$totBusCard = count($BusResult);
		} else if($dirRes> 0) {
			if ($searchchar != 'undefined' && $searchchar != "") {
				if ($searchchar == 'Other') {
					$ts = $searchchar . "%";
					$sql="SELECT * , 'Member' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount  FROM ".TBL_MEMBER_BUSINESS." WHERE ". $xtracondition." bus_title REGEXP '^[^a-zA-Z]' AND bus_dir_id IN (".$allMeb.")  AND bus_status != 'Deleted' ORDER BY ".$orderBy;
				} else {
					if ($searchchar == 'All') {
						$sql="SELECT * , 'Member' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount FROM ".TBL_MEMBER_BUSINESS." WHERE ". $xtracondition." bus_dir_id IN (".$allMeb.") AND bus_status != 'Deleted' ORDER BY ".$orderBy;
					} else {
	                  	//FOR Bus_category  
	                  	if($orderBy == "bus_category") {
	                    	$ts = "bus_buc_id IN (SELECT buc_id FROM tbl_business_category WHERE buc_name like '". $searchchar . "%')";
	                  	} else {
	                       $ts = $orderBy." LIKE '". $searchchar . "%'";
	                  	}
                  	//FOR Bus_category 
					//$ts = $searchchar . "%";
					$sql="SELECT * , 'Member' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount FROM ".TBL_MEMBER_BUSINESS." WHERE ". $xtracondition . $ts ." AND bus_dir_id IN (".$allMeb.") AND bus_status != 'Deleted' ORDER BY ".$orderBy;
					}
				}
			} else {
				$sql="SELECT * , 'Member' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount FROM ".TBL_MEMBER_BUSINESS." WHERE ". $xtracondition." bus_dir_id IN (".$allMeb.") AND bus_status != 'Deleted' ORDER BY ".$orderBy;
			}
   			$BusResult  = $db->select($sql);
			$totBusCard = count($BusResult);
		} else if($subRes>0) {
			if ($searchchar != 'undefined' && $searchchar != "") {
				if ($searchchar == 'Other') {
					$ts = $searchchar . "%";
					$sql="SELECT * , 'Subscriber' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount FROM ".TBL_MEMBER_BUSINESS_SUB." WHERE ". $xtracondition." bus_title REGEXP '^[^a-zA-Z]' AND bus_sub_id IN (".$allSub.")  AND bus_status != 'Deleted' ORDER BY ".$orderBy;
				} else {
					if ($searchchar == 'All') {
						$sql="SELECT * , 'Subscriber' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount FROM ".TBL_MEMBER_BUSINESS_SUB." WHERE ". $xtracondition." bus_sub_id IN (".$allSub.") AND bus_status != 'Deleted' ORDER BY ".$orderBy;
					} else {
	                  	//FOR Bus_category  
	                  	if($orderBy == "bus_category") {
	                       $ts = "bus_buc_id IN (SELECT buc_id FROM tbl_business_category WHERE buc_name like '". $searchchar . "%')";
	                  	} else {
                       		$ts = $orderBy." LIKE '". $searchchar . "%'";
                  	}
                  	//FOR Bus_category 
					//$orderBy." LIKE '".$ts
						$sql="SELECT * , 'Subscriber' AS Cardtype ,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount FROM ".TBL_MEMBER_BUSINESS_SUB." WHERE ".$xtracondition . $ts." AND bus_sub_id IN (".$allSub.")  AND bus_status != 'Deleted' ORDER BY ".$orderBy;
					}
				}
			} else {
				$sql="SELECT * , 'Subscriber' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount FROM ".TBL_MEMBER_BUSINESS_SUB." WHERE ". $xtracondition." bus_sub_id IN (".$allSub.")  AND bus_status != 'Deleted' ORDER BY ".$orderBy;
			} 
			$BusResult  = $db->select($sql);
			$totBusCard = count($BusResult);
		}

			$paging_query = $sql;
			$paging_result  = $db->select($paging_query); 
			$count = count($paging_result);
			$per_page = 10;
			$pages = ceil($count/$per_page);  

			if($action_type == "paging") {
				if(!empty($_REQUEST['pagem']))	{
					$page = $_REQUEST['pagem'];
				}       
			} else {
				if(!empty($_REQUEST['pagem']))  
					$page = $_REQUEST['pagem'];
	  			else
					$page = 1;
			}

			$list_query = $sql;
			if(!empty($per_page) && $_REQUEST['page']!="all") {
				$start = ($page-1)*$per_page;
				if($start<0) {
					$start=0;
				}
				$list_query .= " limit $start,$per_page"; 
			} 
			$BusResult  = $db->select($list_query);  
			$totBusCard = count($BusResult);

		if(false)
		{ //$id!=""
			if ($searchchar != 'undefined' && $searchchar != "") {
				if ($searchchar == 'Other') {
					$ts = $searchchar . "%";
					//$sqlBusQuery = "SELECT * FROM ".TBL_MEMBER_BUSINESS." WHERE bus_title REGEXP '^[^a-zA-Z]' AND bus_id!= 0 AND bus_meb_id IN (".$id.") AND bus_status != 'Deleted' order by bus_title";
					$sqlBusQuery = "SELECT * , 'Member' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS." WHERE ". $xtracondition." bus_title REGEXP '^[^a-zA-Z]' AND bus_meb_id IN (".$id.")  AND bus_status != 'Deleted' UNION SELECT * , 'Subscriber' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS_SUB." WHERE ". $xtracondition." bus_title REGEXP '^[^a-zA-Z]' AND bus_sub_id IN (".$id.")  AND bus_status != 'Deleted' ORDER BY ".$orderBy;
				} else {
					if ($searchchar == 'All') {
						//$sqlBusQuery = "SELECT * FROM ".TBL_MEMBER_BUSINESS." WHERE bus_id!= 0 AND bus_meb_id IN (".$id.") AND bus_status != 'Deleted' order by bus_title";
						$sqlBusQuery="SELECT * , 'Member' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS." WHERE ". $xtracondition." bus_meb_id IN (".$id.") AND bus_status != 'Deleted' UNION SELECT * , 'Subscriber' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS_SUB." WHERE ". $xtracondition." bus_sub_id IN (".$id.") AND bus_status != 'Deleted' ORDER BY ".$orderBy;
					} else {
						//FOR Bus_category  
	                  	if($orderBy == "bus_category") {
	                       $ts = "bus_buc_id IN (SELECT buc_id FROM tbl_business_category WHERE buc_name like '". $searchchar . "%')";
	                  	} else {
	                       $ts = $orderBy." LIKE '". $searchchar . "%'";
	                  	}
	                  	//FOR Bus_category 	
						
						//$ts = $searchchar . "%";
						//$sqlBusQuery = "SELECT * FROM ".TBL_MEMBER_BUSINESS." WHERE bus_title LIKE '".$ts."' AND bus_id!= 0 AND bus_meb_id IN (".$id.") AND bus_status != 'Deleted' order by bus_title";
						$sqlBusQuery="SELECT * , 'Member' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS." WHERE ". $xtracondition . $ts ." AND bus_meb_id IN (".$id.") AND bus_status != 'Deleted' UNION SELECT * , 'Subscriber' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS_SUB." WHERE ". $xtracondition . $ts ." AND bus_sub_id IN (".$id.")  AND bus_status != 'Deleted' ORDER BY ".$orderBy;
					}
				}
			} else {
				//$sqlBusQuery = "SELECT * FROM ".TBL_MEMBER_BUSINESS." WHERE bus_id!= 0 AND bus_meb_id IN (".$id.") AND bus_status != 'Deleted' order by bus_title";
				$sqlBusQuery="SELECT * , 'Member' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS." WHERE ". $xtracondition ." bus_meb_id IN (".$id.") AND bus_status != 'Deleted' UNION SELECT * , 'Subscriber' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS_SUB." WHERE ". $xtracondition." bus_sub_id IN (".$id.")  AND bus_status != 'Deleted' ORDER BY ".$orderBy;
			}

			$BusResult  = $db->select($sqlBusQuery);
			$totBus = count($BusResult);

			$sqlSerQuery = "SELECT * FROM ".TBL_MEMBER_SERVICES." WHERE ser_id!= 0 AND ser_meb_id IN (".$id.") AND ser_status != 'Deleted' order by ser_id desc";
			$SerResult  = $db->select($sqlSerQuery);
			$totSer = count($SerResult);			
		}
		
		$cate_data = '';
		$cate_data = getBusinessCategoryList('bus_category','bus_category',$dirId);

		$check_con = '';
		$dir_data = '<article><div class="cursor-slide" id="wrapper"><ul id="flip">';
			if($totBusCard>0) {
				for($j=0;$j<$totBusCard;$j++) {

					$file_type = '';
					if($BusResult[$j]['Cardtype']=="Member") {
						$uploadFILEWWW = UPLOAD_WWW_BUSINESS_FILE;
					    $categoryName = getCategoryNameByMebId($BusResult[$j]['bus_meb_id']);
					    $categoryName = $BusResult[$j]['bus_category'];
					    $file_type = 'card_details.html';	
					}
					if($BusResult[$j]['Cardtype']=="Subscriber") {
						$uploadFILEWWW = UPLOAD_WWW_BUSINESS_FILE."Business_Sub/";
		    			$categoryName = $BusResult[$j]['bus_category'];
		    			$file_type = 'card_details_sub.html';	
					}

					//print_r($BusResult[$j]);	

					if($BusResult[$j]['bus_picture_file']!="") {
						$fileNameExt = explode(".", $BusResult[$j]['bus_picture_file']);
						$fileExt 	 = $fileNameExt[count($fileNameExt)-1];
						$allowedImgExts = array("gif","jpg","jpeg","png","GIF","JPG","JPEG","PNG");
						if(in_array($fileExt,$allowedImgExts)) {
							$busFiles = "<div class='bottom-img'><a href='#popupParis".$BusResult[$j]['bus_id']."' data-rel='popup' data-position-to='window' data-transition='fade' ><img class='popphoto' src='".$uploadFILEWWW.$BusResult[$j]['bus_id']."/".$BusResult[$j]['bus_picture_file']."' style='float:left;' height='41' width='70' /></a></div>";
							$busFiles .= "<div data-role='popup' style='text-align: center;' id='popupParis".$BusResult[$j]['bus_id']."' data-overlay-theme='b' data-theme='b' data-corners='false'><a href='#' data-rel='back' class='ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right'>Close</a><img class='popphoto' src='".$uploadFILEWWW.$BusResult[$j]['bus_id']."/".$BusResult[$j]['bus_picture_file']."' style='max-height:512px;'></div>";

							$busFiles_new = "<img class='popphoto' src='".$uploadFILEWWW.$BusResult[$j]['bus_id']."/".$BusResult[$j]['bus_picture_file']."' style=' width: 150px;' height='115' width='115' class='popphoto' />";
						} else {
							$busFiles = "<div class='bottom-img'><a href='#popupvideo".$BusResult[$j]['bus_id']."' data-rel='popup' data-position-to='window' data-transition='fade' ><img src='images/video-icon.png' style='float:left;' height='41' width='70' /></a></div>";
							$busFiles .= "<div data-role='popup' id='popupvideo".$BusResult[$j]['bus_id']."' data-overlay-theme='b' data-theme='b' data-corners='false'><a href='#' data-rel='back' class='ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right'>Close</a>";
							
							$video = "file=".$uploadFILEWWW.$BusResult[$j]['bus_id']."/".$BusResult[$j]['bus_picture_file']."&;volume=80&amp;bufferlength=5&amp;logo.hide=false&amp;&;title=sleeve_length&amp;linktarget=_self";
							
							$busFiles .= '<object width="100%" height="250" data="player.swf" type="application/x-shockwave-flash"><param value="opaque" name="wmode"><param value="always" name="allowscriptaccess"><param value="all" name="allownetworking"><param value="'.$video.'" name="flashvars"><param name="allowFullScreen" value="true" /></object></div>';

							$busFiles_new = '<object width="100%" height="250" data="player.swf" type="application/x-shockwave-flash"><param value="opaque" name="wmode"><param value="always" name="allowscriptaccess"><param value="all" name="allownetworking"><param value="'.$video.'" name="flashvars"><param name="allowFullScreen" value="true" /></object>';
						}
					} else {
						$busFiles = "<div class='bottom-img'><a href='#' ><img class='popphoto' src='images/photo.jpg' style='float:left;' height='41' width='70' /></a></div>";	
						$busFiles_new = "<img class='popphoto' src='images/photo.jpg' style='width: 150px;' class='popphoto' /></a>";	
					}

					if($BusResult[$j]['bus_logo_file']!="") {
						$fileNameExt = explode(".", $BusResult[$j]['bus_logo_file']);
						$fileExt 	 = $fileNameExt[count($fileNameExt)-1];
						$allowedImgExts = array("gif","jpg","jpeg","png","GIF","JPG","JPEG","PNG");
						if(in_array($fileExt,$allowedImgExts)) {
							$busFiles_new = "<img class='popphoto' src='".$uploadFILEWWW.$BusResult[$j]['bus_id']."/".$BusResult[$j]['bus_logo_file']."' style=' width: 150px;' height='115' width='115' class='popphoto' />";
						} else {
							$video = "file=".$uploadFILEWWW.$BusResult[$j]['bus_id']."/".$BusResult[$j]['bus_logo_file']."&;volume=80&amp;bufferlength=5&amp;logo.hide=false&amp;&;title=sleeve_length&amp;linktarget=_self";
							$busFiles_new = '<object width="100%" height="250" data="player.swf" type="application/x-shockwave-flash"><param value="opaque" name="wmode"><param value="always" name="allowscriptaccess"><param value="all" name="allownetworking"><param value="'.$video.'" name="flashvars"><param name="allowFullScreen" value="true" /></object>';
						}
					} else {
						$busFiles = "<div class='bottom-img'><a href='#' ><img class='popphoto' src='images/photo.jpg' style='float:left;' height='41' width='70' /></a></div>";	
						$busFiles_new = "<img class='popphoto' src='images/no-logo.jpg' style='width: 150px;' class='popphoto' /></a>";	
					}

					$dir_data .= '<li><div class="main-box main_b_card"><div class="home-detail">';

					$dir_data .= '<div data-role="popup" class="popup_main_wrapper" id="about_'.$BusResult[$j]['bus_id'].'" data-overlay-theme="b" data-theme="b" data-corners="pop">
                                <a href="#" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right">Close</a>
                                <div class="popup_inner">
                                    <div class="popup_header">About Us...</div>
                                    <div class="popup_content">
                                        <div class="content_image">'.$busFiles_new.'</div>
                                        <div class="content_title"><h1>'.$BusResult[$j]['bus_company'].'</h1></div>
                                    	<p>'.$BusResult[$j]['bus_about'].'</p>
                                    </div>
                                </div>
                            </div>';

                    ///(SELECT sta_name FROM tbl_state WHERE bus_sta_id = sta_id) as StateName
                    $state_name = '';
                    if($BusResult[$j]['bus_sta_id'] != '' && $BusResult[$j]['bus_sta_id'] > '0') {
                    	$stq = 'SELECT sta_name FROM tbl_state WHERE sta_id='.$BusResult[$j]['bus_sta_id'];
                    	$stqr  = $db->select($stq);
                    	$state_name = $stqr[0]['sta_name'];
                    }

                    $dir_data .= '<div data-role="popup" class="popup_main_wrapper" id="info_'.$BusResult[$j]['bus_id'].'" data-overlay-theme="b" data-theme="b" data-corners="pop">
                                <a href="#" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right">Close</a>
                                <div class="popup_inner">
                                    <div class="popup_header">Information</div>
                                    <div class="popup_content" style="padding: 0;">
                                        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="info-table">
		                                    <tbody>
		                                    <tr>
		                                        <td width="25%" class="blue">COMPANY NAME</td>
		                                        <td width="25%">'.$BusResult[$j]['bus_company'].'</td>
		                                    </tr>
		                                    <tr>
		                                        <td width="25%" class="blue">ADDRESS</td>
		                                        <td width="25%">'.$BusResult[$j]['bus_address1'].'</td>
		                                    </tr>
		                                    <tr>
		                                        <td width="25%" class="blue">CITY</td>
		                                        <td width="25%">'.$BusResult[$j]['bus_city'].'</td>
		                                    </tr>
		                                    <tr>
		                                        <td width="25%" class="blue">STATE</td>
		                                        <td width="25%">'.$state_name.'</td>
		                                    </tr>
		                                    <tr>
		                                        <td width="25%" class="blue">ZIP CODE</td>
		                                        <td width="25%">'.$BusResult[$j]['bus_zip'].'</td>
		                                    </tr>
		                                    <tr>
		                                        <td width="25%" class="blue">WEBSITE</td>
		                                        <td width="25%"><a href="#">'.$BusResult[$j]['bus_website'].'</a></td>
		                                    </tr>
		                                    <tr>
		                                        <td width="25%" class="blue">OFFICE PHONE</td>
		                                        <td width="25%">'.$BusResult[$j]['bus_phone'].'</td>
		                                    </tr>
		                                    <tr>
		                                        <td width="25%" class="blue">FAX NUMBER</td>
		                                        <td width="25%">'.$BusResult[$j]['bus_fax'].'</td>
		                                    </tr>
		                                    <tr>
		                                        <td width="25%" class="blue">EMAIL ADDRESS</td>
		                                        <td width="25%">'.$BusResult[$j]['bus_email'].'</td>
		                                    </tr>
		                                    <tr>
		                                        <td width="25%" class="blue">FIRST NAME</td>
		                                        <td width="25%">'.$BusResult[$j]['bus_name'].'</td>
		                                    </tr>
		                                    <tr>
		                                        <td width="25%" class="blue">TITLE</td>
		                                        <td width="25%">'.$BusResult[$j]['bus_title'].'</td>
		                                    </tr>
		                                	</tbody>
                                		</table>
                                    </div>
                                </div>
                            </div>';

                    $dir_data .= '<div data-role="popup" class="popup_main_wrapper video_card_play_pop" id="playlist_'.$BusResult[$j]['bus_id'].'" data-overlay-theme="b" data-theme="b" data-corners="pop">
                                <a href="#" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right">Close</a>
                                <div class="popup_inner">
                                    <div class="popup_header">Media</div>
                                    <div class="popup_content" style="padding: 0;">
                                        <div class="playlist-table"><div class="title_video"></div><div class="playlist_all">';
                                         $play_list_popn_list = '';
                                         $is_pl = '';
                                         for($v=1,$v1=0;$v<=5;$v++) {
						      				if($BusResult[$j]['bus_video_file_'.$v] != '' ) {
						      					$v1++;
						      					$fileNameExtn = explode(".", $BusResult[$j]['bus_video_file_'.$v]);
						      					$file_name = '';
													for($f=0;$f<count($fileNameExtn)-1;$f++) {
														$file_name .= $fileNameExtn[$f];
						      					}
						      					$sel_op .= '<option value="'.$BusResult[$j]['bus_id'].$v.'">'.$file_name.'</option>';
						      					
						      					$play_list_popn_list .= '<span class="video_top_title"><a href="#" onclick="show_video_l(\''.$v.'\')" >Video '.$v1.'</a></span>';
						      					$is_pl = 'yes';
						      				}
      				
											if($BusResult[$j]['bus_video_file_'.$v]) {
												$fileNameExt = explode(".", $BusResult[$j]['bus_video_file_'.$v]);
												$fileExt 	 = $fileNameExt[count($fileNameExt)-1];
						
												if(in_array($fileExt,$allowedImgExts)) {
													$play_list_pop .= "<a href='#popupParis".$BusResult[$j]['bus_id'].$v."' data-rel='popup' data-position-to='window' data-transition='fade' style='display: none' ><img class='popphoto' src='".UPLOAD_WWW_BUSINESS_FILE.$BusResult[$j]['bus_id']."/".$BusResult[$j]['bus_video_file_'.$v]."' height='41' width='70' /></a>";
													$play_list_pop .= "<div data-role='popup' id='popupParis".$BusResult[$j]['bus_id'].$v."' data-overlay-theme='b' data-theme='b' style='text-align: center;' data-corners='false'><a href='#' data-rel='back' class='ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right'>Close</a><img class='popphoto' src='".UPLOAD_WWW_BUSINESS_FILE.$BusResult[$j]['bus_id']."/".$BusResult[$j]['bus_video_file_'.$v]."' style='max-height:512px;'></div>";

													$play_list_popn_listv .= "<div id='video_".$v."' class='videos_l' style='display:none;'><div class='video_image_title'>".$file_name."</div><div class='video_image_shoe'><img class='popphoto' src='".UPLOAD_WWW_BUSINESS_FILE.$BusResult[$j]['bus_id']."/".$BusResult[$j]['bus_video_file_'.$v]."' style='max-height:512px;'></div></div>";								
													
												} else {
													$uploadFILEWWW = UPLOAD_WWW_BUSINESS_FILE."Business_Sub/";

													$video_file = $uploadFILEWWW.$BusResult[$j]['bus_id']."/".$BusResult[$j]['bus_video_file_'.$v];
													
													$play_list_pop .= "<a href='#popupParis".$BusResult[$j]['bus_id'].$v."' data-rel='popup' data-position-to='window' data-transition='fade' style='display: none' ><img src='images/video-icon.png' height='41' width='70' /></a>";
													$play_list_pop .= "<div data-role='popup' d_type='video' d_video='".$video_file."' id='popupParis".$BusResult[$j]['bus_id'].$v."' data-overlay-theme='b' data-theme='b' style='text-align: center;' data-corners='false'><a href='#' data-rel='back' class='ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right'>Close</a>";
												
													$video = "file=".$uploadFILEWWW.$BusResult[$j]['bus_id']."/".$BusResult[$j]['bus_video_file_'.$v]."&;volume=80&amp;bufferlength=5&amp;logo.hide=false&amp;&;title=sleeve_length&amp;linktarget=_self";
												
													$play_list_pop .= '<object width="100%" height="250" data="player.swf" type="application/x-shockwave-flash"><param value="opaque" name="wmode"><param value="always" name="allowscriptaccess"><param value="all" name="allownetworking"><param value="'.$video.'" name="flashvars"><param name="allowFullScreen" value="true" /></object></div>';
													
													$play_list_popn_listv .= "<div id='video_".$v."' class='videos_l' style='display:none;'><div class='video_image_title'>".$file_name.'</div><div class="video_image_shoe"><a href="#"><img src="images/play-button.jpg" width="100%" height="250" onclick="play_new_video(\''.$video_file.'\')" /></a></div></div>';
												}
												$is_pl = 'yes';
											}
										}   
                                            
                            $dir_data .= $play_list_popn_list.'</div>';
                            if($is_pl == 'yes') {
                            	$dir_data .= '<div class="videoh1" style="height: auto; min-height: 198px; width:100%">'.$play_list_popn_listv;
                            } else {
                            	$dir_data .= '<div class="videoh1" style="height: 50px; width:100%"><span class="video_no_record">No record found.</span>';	
                            }
                            
                            $dir_data .= '</div>
	                                        </div>
                                		</div>
                                    </div>
                            	</div>';  
                            $dir_data .= '<script>function show_video_l(val) {  $(".videos_l").slideUp(); $("#video_"+val).slideDown(); } function play_new_video(vid) { window.plugins.videoPlayer.play(vid); }</script>';
                            	
					$dir_data .= '<div class="detail-top">
									<ul>
										<li>
                                        	<a href="#about_'.$BusResult[$j]['bus_id'].'" data-rel="popup" data-position-to="window" data-transition="pop">
                                            	<span><img title="" alt="" src="images/about-us.png"></span>about us
                                            </a>
                                        </li>
                                        <li>
                                        	<a href="#info_'.$BusResult[$j]['bus_id'].'" data-rel="popup" data-position-to="window" data-transition="pop">
                                            	<span><img title="" alt="" src="images/infoinformation.png"></span>Info.
                                           	</a>
                                        </li>
                                        <li>
                                        	<a href="#playlist_'.$BusResult[$j]['bus_id'].'" data-rel="popup" data-position-to="window" data-transition="pop">
                                            	<span><img title="" alt="" src="images/play-list.png"></span>MEDIA
                                            </a>
                                        </li>
                                    </ul>
                                </div>';

					$dir_data .= '<div class="detail-bottom">';
					$dir_data .= $busFiles;
					//$dir_data .= $BusResult[$j]['bus_phone'].'<br />';
					$dir_data .= '<div class="bottom-content">
									<a href="#" data_file="'.$file_type.'" data_bus_id="'.$BusResult[$j]['bus_id'].'" class="detail_card_list_show">
									<p>
										<strong>'.$BusResult[$j]['bus_name']." ".$BusResult[$j]['bus_lname'].'</strong>
										'.$BusResult[$j]['bus_title'].'<strong>'.$BusResult[$j]['bus_company'].'</strong>'.$categoryName.'
									</p>
									</a>
                                  </div><div class="clear"></div>';

                    $ratingDisable = getRatingValue("Card",$BusResult[$j]['bus_id'],$sbfId); 
                    $sumRating =  $BusResult[$j]['ratingCount'];
                    $ratedUsers = $BusResult[$j]['ratingUser'];
                    $sumRating =  round($sumRating / $ratedUsers,2);              
                    
                    //$dir_data .= '<div class="rank-all"> <input id="input-'.$BusResult[$j]['bus_id'].'" busid="'.$BusResult[$j]['bus_id'].'" value="'.$sumRating.'" type="number" class="rating" min=0 max=5 step=1 data-size="sm" '.$ratingDisable.' ></div>';
                    //$dir_data .= '<div class="bottom-star"><img src="images/star-rate.png"></div><div class="clr"></div>';                            
						
					$fun='setdetailmempage(\''.$BusResult[$j]['bus_meb_id'].'\')';					
					
					//$dir_data .= '<div class="bus_title" onclick="'.$fun.'">'.$BusResult[$j]['bus_title'].'</div>';
					$dir_data .= '</div></div></div></li>';
					$categoryName='';
				}
			} else {
				$dir_data .= '<li><div class="main_b_card" style="font-weight: bold;line-height: 160px;text-align: center;"><div class="business_card">No business card available.</div></div></li>';
			}
		$dir_data .= '</ul>';
		$center = round($totBusCard / 2);
		//$dir_data .= '<div id="scrollbar" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"><a class="ui-slider-handle ui-state-default ui-corner-all" href="#"></a></div>';
		$dir_data .= '<input type="range" name="scrollbar" id="scrollbar" value="'.$center.'" min="0" max="'.$totBusCard.'" step="1" />';

		$no_1 = '';
		if($totBusCard!=1) { 
			$no = 100/($totBusCard-1); 
			$no_1 = round($totBusCard/2);
		} else { 
			$no=1;
			$no_1=0; 
		};

		$services = '';
		
		$services .= '<article><div class="tab_content_holder radius"><div class="tab_content_holder_inner"><div class="border-txt bottom"><div class="serviceTxt">Services</div>'; 
		if($totSer>0) {
			$services .= '<div class="subRow direct card service_box flexslidertext"><ul class="slides">';
				$b=1;
            for($k=0;$k<$totSer;$k++) {
					$services .= '<li><div class="enterTitle light"><b>Title</b><span>:</span>';
					$services .= '<p>'.$SerResult[$k]['ser_title'].'</p></div>';
					$services .= '<div class="enterTitle dark"><b>Description</b><span>:</span>';
					$services .= '<p>'.$SerResult[$k]['ser_description'].'</p></div></li>';
				}
			$services .= '</ul></div>';
		} else {
			$services .= '<div class="no-record">No Records</div>';
		}	
		$services .= '</div></div></div></article>'; 
		
		$pagination = '';
		$pagination .= '<div class="paging"><ul>';			
			for ($i = 65; $i < 91; $i++) {
				$charAct = ($searchchar == chr($i)) ? "class='active'" : "" ;	
				$pagination .= '<li><a '.$charAct.' href="javascript:void(0);" onclick="return getdirectorycard2(this.id)" id="'.chr($i).'">'.chr($i).'</a></li>';	
			}
			$otherAct = ($searchchar == 'Other') ? "class='active'" : "" ;
			$allAct = ($searchchar == 'All') ? "class='active'" : "" ;
			$pagination .= '<li><a '.$allAct.' href="javascript:void(0);" onclick="return getdirectorycard2(this.id)" id="All">All</a></li>';
			$pagination .= '<li><a '.$otherAct.' href="javascript:void(0);" onclick="return getdirectorycard2(this.id)" id="Other">Other</a></li>';
		$pagination .= '</ul></div>';
			
		$dir_data .= '</div></article>';

		if($pages>1) {
			$paginationn = '<div class="paging"><ul>';
					$page = ($page == 0 ? 1 : $page);
					$start = ($page-1) * $per_page;
					$adjacents = "1";		

					$prev = $page - 1;
					$next = $page + 1;
					$lastpage = ceil($count/$per_page);
					$lpm1 = $lastpage - 1;   

					if ($page > 1)
			            $paginationn.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getajax(\"".$prev."\");'>&laquo; Previous</a></li>";
			        else
			            $paginationn.= "<li style='padding: 1px;'><a style='padding: 6px 6px;'><span class='disabled'>&laquo; Previous</span></a></li>";


			        if ($lastpage < 7 + ($adjacents * 2)){  
			            for ($counter = 1; $counter <= $lastpage; $counter++){
			                if ($counter == $page)
			                    $paginationn.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' class='active'><span>$counter</span></a></li>";
			                else
			                    $paginationn.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getajax(\"".$counter."\");'>$counter</a></li>";
			            }
			        } 

			        elseif($lastpage > 5 + ($adjacents * 2)) {
						if($page < 1 + ($adjacents * 2)){
			                for($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
			                    if($counter == $page)
			                        $paginationn.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' class='active'><span>$counter</span></a></li>";
			                    else
			                        $paginationn.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getajax(\"".($counter)."\");'>$counter</a></li>";    
			                }
			                $paginationn.= "<li style='padding: 0px;'><a style='padding: 6px 1px;'>...</a></li>";
			                //$pagination.= "<li><a href='javascript:void(0);' onClick='getAjaxPaging(\"Directory\",\"".$pages."\",\"".($lpm1)."\");'>$lpm1</a></li>";
			                $paginationn.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getajax(\"".($lastpage)."\");'>$lastpage</a></li>";  

			 			} elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
			               $paginationn.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getajax(\"1\");'>1</a></li>";
			               //$pagination.= "<li><a href='javascript:void(0);' onClick='getAjaxPaging(\"Directory\",\"".$pages."\",\"2\");'>2</a></li>";
			               $paginationn.= "<li style='padding: 0px;'><a style='padding: 6px 1px;'>...</a></li>";

			               for($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
			                   if($counter == $page)
			                       $paginationn.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' class='active'><span>$counter</span></a></li>";
			                   else
			                       $paginationn.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getajax(\"".($counter)."\");'>$counter</a></li>";    
			               }
			               $paginationn.= "<li style='padding: 0px;'><a style='padding: 6px 1px;'>...</a></li>";
			               //$pagination.= "<li><a href='javascript:void(0);' onClick='getAjaxPaging(\"Directory\",\"".$pages."\",\"".($lpm1)."\");'>$lpm1</a></li>";
			               $paginationn.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getajax(\"".($lastpage)."\");'>$lastpage</a></li>";  

			           	} else {
			               $paginationn.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getajax(\"1\");'>1</a></li>";
			               //$pagination.= "<li><a href='javascript:void(0);' onClick='getAjaxPaging(\"Directory\",\"".$pages."\",\"2\");'>2</a></li>";
			               $paginationn.= "<li style='padding: 0px;'><a style='padding: 6px 1px;'>...</a></li>";
			               for($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
			                   if($counter == $page)
			                        $paginationn.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' class='active'><span>$counter</span></a></li>";
			                   else
			                        $paginationn.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getajax(\"".($counter)."\");'>$counter</a></li>";    
			               }
			           	}
			        }	

			        if($page < $counter - 1)
			            $paginationn.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getajax(\"".($next)."\");'>Next &raquo;</a>";
			        else
			            $paginationn.= "<li style='padding: 1px;'><a style='padding: 6px 6px;'><span class='disabled'>Next &raquo;</span></a></li>";
				/*for($t=1;$t<=$pages;$t++) {
					if($page==$t) {
						$paginationn .= '<li><a class="active" href="javascript:void(0);"';
						if($pages > 1){
							$paginationn .= 'onclick="getajax(\''.$t.'\')"'; 
						}
						$paginationn .= '>'.$t.'</a></li>';
						} else {
						$paginationn .= '<li><a href="javascript:void(0);"';
						if($pages > 1){ 
							$paginationn .= 'onclick="getajax(\''.$t.'\')"';
						}
						$paginationn .= '>'.$t.'</a></li>';
					}
				}*/
				$paginationn .= '</div></ul>';
			}

      $success = array("status" => 'true', 'data' => $dir_data, 'pagination' => $pagination, 'paginationn' => $paginationn, 'no' => $no, 'services' => $services, 'cate_data' => $cate_data, 'no1' => $no_1);
		 
   } else if($action == 'GetDirecotyCardsList') {
		$dirId = $_REQUEST['direid'];
		$sub_email = $_REQUEST['email'];
		$SubcriberId = $_REQUEST['SubcriberID'];
		$subribe	= $_REQUEST['subribe'];
		$orderBy	= 'bus_lname';//$_REQUEST['orderby'];
		$xtracondition_o	=	$_REQUEST['xtracondition'];

		$dir_fields = array("mbm_meb_id");
		$dir_where  = "mbm_dir_id = ".$dirId." AND mbm_status != 'Deleted'";
		$dirRes 	= $db->selectData(TBL_MEMBER_DIRECTORY,$dir_fields,$dir_where,$extra="",2);		

		if(count($dirRes) > 0) {
			$id='';
			for($i=0;$i<count($dirRes);$i++){
				$id .= $dirRes[$i]["mbm_meb_id"].',';
			}
		}		
		$id = substr($id, 0, -1);

		$meb_fields = array("meb_id");
		$meb_where  = "meb_dir_id= ".$dirId." AND meb_status != 'Deleted'";
		$mebRes 	= $db->selectData(TBL_MEMBER,$meb_fields,$meb_where,$extra="",2);
		
		for($i=0;$i<count($mebRes);$i++) {
			if($i==count($mebRes)-1) {
				$addComma = "";
			} else {
				$addComma = ",";
			}
			$allMeb .= $mebRes[$i]["meb_id"].$addComma;
		}
		
		$allMeb = $dirId;

		$sub_fields = array("sbm_sub_id");
		$sub_where  = "sbm_dir_id= ".$dirId." AND sbm_status = 'Active'";
		$subRes 	= $db->selectData(TBL_MEMBER_SUBSCRIBE_DIRECTORY,$sub_fields,$sub_where,$extra="",2);
		//$sql ="select sub_id from ".TBL_MEMBER_SUBSCRIBERS." WHERE sub_dir_id=".$id;
		for($j=0;$j<count($subRes);$j++) {
			if($j==count($subRes)-1) {
				$addComma = "";
			} else {
				$addComma = ",";
			}
			$allSub .= $subRes[$j]["sbm_sub_id"].$addComma;
		}

		$searchchar = $_REQUEST['search_char'];
		
		$searc = $_REQUEST['search_char'];
		
		if ($xtracondition_o != 'undefined' && $xtracondition_o != "") {
        	$xtracondition = $xtracondition_o." AND ";
  		} else {
        	$xtracondition = "";
  		}

  		if($dirRes> 0 && $subRes>0) {
			if ($searchchar != 'undefined' && $searchchar != "") {
				if ($searchchar == 'Other') {
					$ts = $searchchar . "%";
					$sql="SELECT * , 'Member' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS." WHERE ". $xtracondition." bus_title REGEXP '^[^a-zA-Z]' AND bus_dir_id IN (".$allMeb.")  AND bus_status != 'Deleted' UNION SELECT * , 'Subscriber' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS_SUB." WHERE ". $xtracondition." bus_title REGEXP '^[^a-zA-Z]' AND bus_sub_id IN (".$allSub.")  AND bus_status != 'Deleted' ORDER BY ".$orderBy;
				} else {
					if ($searchchar == 'All') {
						$sql="SELECT * , 'Member' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS." WHERE ". $xtracondition." bus_dir_id IN (".$allMeb.") AND bus_status != 'Deleted' UNION SELECT * , 'Subscriber' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS_SUB." WHERE ". $xtracondition." bus_sub_id IN (".$allSub.") AND bus_status != 'Deleted' ORDER BY ".$orderBy;
					} else {
     				//FOR Bus_category  
                  	if($orderBy == "bus_category") {
                    	$ts = "bus_buc_id IN (SELECT buc_id FROM tbl_business_category WHERE buc_name like '". $searchchar . "%')";
                  	} else {
                       $ts = ' ('.$orderBy." LIKE '%". $searchchar . "%' OR bus_name LIKE '%". $searchchar . "%' )";
                  	}
                  	//FOR Bus_category 
					//$ts = $searchchar . "%";
					$sql="SELECT * , 'Member' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS." WHERE ". $xtracondition . $ts ." AND bus_dir_id IN (".$allMeb.") AND bus_status != 'Deleted' UNION SELECT * , 'Subscriber' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS_SUB." WHERE ". $xtracondition . $ts ." AND bus_sub_id IN (".$allSub.")  AND bus_status != 'Deleted' ORDER BY ".$orderBy;
					}
				}
			} else if($searc!=''){
				$sql="SELECT * , 'Member' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS." WHERE ". $xtracondition ." bus_dir_id IN (".$allMeb.") AND bus_status != 'Deleted' UNION SELECT * , 'Subscriber' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS_SUB." WHERE ". $xtracondition." bus_sub_id IN (".$allSub.") AND (bus_name LIKE '%".$searc."%' OR bus_lname LIKE '%".$searc."%' OR bus_company LIKE '%".$searc."%') AND bus_status != 'Deleted' ORDER BY ".$orderBy;
			} else {
				$sql="SELECT * , 'Member' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS." WHERE ". $xtracondition ." bus_dir_id IN (".$allMeb.") AND bus_status != 'Deleted' UNION SELECT * , 'Subscriber' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS_SUB." WHERE ". $xtracondition." bus_sub_id IN (".$allSub.")  AND bus_status != 'Deleted' ORDER BY ".$orderBy;
			}   //echo $sql;
			$BusResult  = $db->select($sql);
			$totBusCard = count($BusResult);
		} else if($dirRes> 0) {
			if ($searchchar != 'undefined' && $searchchar != "") {
				if ($searchchar == 'Other') {
					$ts = $searchchar . "%";
					$sql="SELECT * , 'Member' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount  FROM ".TBL_MEMBER_BUSINESS." WHERE ". $xtracondition." bus_title REGEXP '^[^a-zA-Z]' AND bus_dir_id IN (".$allMeb.")  AND bus_status != 'Deleted' ORDER BY ".$orderBy;
				} else {
					if ($searchchar == 'All') {
						$sql="SELECT * , 'Member' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount FROM ".TBL_MEMBER_BUSINESS." WHERE ". $xtracondition." bus_dir_id IN (".$allMeb.") AND bus_status != 'Deleted' ORDER BY ".$orderBy;
					} else {
	                  	//FOR Bus_category  
	                  	if($orderBy == "bus_category") {
	                    	$ts = "bus_buc_id IN (SELECT buc_id FROM tbl_business_category WHERE buc_name like '". $searchchar . "%')";
	                  	} else {
	                       $ts = ' ('.$orderBy." LIKE '%". $searchchar . "%' OR bus_name LIKE '%". $searchchar . "%' )";
	                  	}
                  	//FOR Bus_category 
					//$ts = $searchchar . "%";
					$sql="SELECT * , 'Member' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount FROM ".TBL_MEMBER_BUSINESS." WHERE ". $xtracondition . $ts ." AND bus_dir_id IN (".$allMeb.") AND bus_status != 'Deleted' ORDER BY ".$orderBy;
					}
				}
			} else if($searc!=''){
				$sql="SELECT * , 'Member' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS." WHERE ". $xtracondition ." bus_dir_id IN (".$allMeb.") AND bus_status != 'Deleted' UNION SELECT * , 'Subscriber' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS_SUB." WHERE ". $xtracondition." bus_sub_id IN (".$allSub.") AND (bus_name LIKE '%".$searc."%' OR bus_lname LIKE '%".$searc."%' OR bus_company LIKE '%".$searc."%') AND bus_status != 'Deleted' ORDER BY ".$orderBy;
			} else {
				$sql="SELECT * , 'Member' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount FROM ".TBL_MEMBER_BUSINESS." WHERE ". $xtracondition." bus_dir_id IN (".$allMeb.") AND bus_status != 'Deleted' ORDER BY ".$orderBy;
			}
   			$BusResult  = $db->select($sql);
			$totBusCard = count($BusResult);
		} else if($subRes>0) {
			if ($searchchar != 'undefined' && $searchchar != "") {
				if ($searchchar == 'Other') {
					$ts = $searchchar . "%";
					$sql="SELECT * , 'Subscriber' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount FROM ".TBL_MEMBER_BUSINESS_SUB." WHERE ". $xtracondition." bus_title REGEXP '^[^a-zA-Z]' AND bus_sub_id IN (".$allSub.")  AND bus_status != 'Deleted' ORDER BY ".$orderBy;
				} else {
					if ($searchchar == 'All') {
						$sql="SELECT * , 'Subscriber' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount FROM ".TBL_MEMBER_BUSINESS_SUB." WHERE ". $xtracondition." bus_sub_id IN (".$allSub.") AND bus_status != 'Deleted' ORDER BY ".$orderBy;
					} else {
	                  	//FOR Bus_category  
	                  	if($orderBy == "bus_category") {
	                       $ts = "bus_buc_id IN (SELECT buc_id FROM tbl_business_category WHERE buc_name like '". $searchchar . "%')";
	                  	} else {
                       		$ts = ' ('.$orderBy." LIKE '%". $searchchar . "%' OR bus_name LIKE '%". $searchchar . "%' )";
                  	}
                  	//FOR Bus_category 
					//$orderBy." LIKE '".$ts
						$sql="SELECT * , 'Subscriber' AS Cardtype ,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount FROM ".TBL_MEMBER_BUSINESS_SUB." WHERE ".$xtracondition . $ts." AND bus_sub_id IN (".$allSub.")  AND bus_status != 'Deleted' ORDER BY ".$orderBy;
					}
				}
			} else if($searc!=''){
					$sql="SELECT * , 'Member' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS." WHERE ". $xtracondition ." bus_dir_id IN (".$allMeb.") AND bus_status != 'Deleted' UNION SELECT * , 'Subscriber' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS_SUB." WHERE ". $xtracondition." bus_sub_id IN (".$allSub.") AND (bus_name LIKE '%".$searc."%' OR bus_lname LIKE '%".$searc."%' OR bus_company LIKE '%".$searc."%') AND bus_status != 'Deleted' ORDER BY ".$orderBy;
			} else {
				if($allMeb!='')
					$sql="SELECT * , 'Member' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS." WHERE ". $xtracondition ." bus_dir_id IN (".$allMeb.") AND bus_status != 'Deleted' UNION SELECT * , 'Subscriber' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS_SUB." WHERE ". $xtracondition." bus_sub_id IN (".$allSub.") AND (bus_name LIKE '%".$searc."%' OR bus_lname LIKE '%".$searc."%' OR bus_company LIKE '%".$searc."%') AND bus_status != 'Deleted' ORDER BY ".$orderBy;
				else
					$sql="SELECT * , 'Subscriber' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount FROM ".TBL_MEMBER_BUSINESS_SUB." WHERE ". $xtracondition." bus_sub_id IN (".$allSub.")  AND bus_status != 'Deleted' ORDER BY ".$orderBy;
			} 
			$BusResult  = $db->select($sql);
			$totBusCard = count($BusResult);
		} else {
            //$sqlBusQuery = "SELECT * FROM ".TBL_MEMBER_BUSINESS." WHERE bus_id!= 0 AND bus_meb_id IN (".$id.") AND bus_status != 'Deleted' order by bus_title";
           $sql="SELECT * , 'Member' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS." WHERE ". $xtracondition ." bus_dir_id IN (-1) AND bus_status != 'Deleted' UNION SELECT * , 'Subscriber' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS_SUB." WHERE ". $xtracondition." bus_sub_id IN (-1) AND bus_status != 'Deleted' ORDER BY ".$orderBy;
        }

		$searchchar = $_REQUEST['char'];
		
		if ($xtracondition_o != 'undefined' && $xtracondition_o != "") {
        	$xtracondition = $xtracondition_o." AND ";
  		} else {
        	$xtracondition = "";
  		}

		if(false)
		{	//$id!=""
			if ($searchchar != 'undefined' && $searchchar != "") {
				if ($searchchar == 'Other') {
					$ts = $searchchar . "%";
					//$sqlBusQuery = "SELECT * FROM ".TBL_MEMBER_BUSINESS." WHERE bus_title REGEXP '^[^a-zA-Z]' AND bus_id!= 0 AND bus_meb_id IN (".$id.") AND bus_status != 'Deleted' order by bus_title";
					$sqlBusQuery = "SELECT * , 'Member' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS." WHERE ". $xtracondition." bus_title REGEXP '^[^a-zA-Z]' AND bus_meb_id IN (".$id.")  AND bus_status != 'Deleted' UNION SELECT * , 'Subscriber' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS_SUB." WHERE ". $xtracondition." bus_title REGEXP '^[^a-zA-Z]' AND bus_sub_id IN (".$id.")  AND bus_status != 'Deleted' ORDER BY ".$orderBy;
				} else {
					if ($searchchar == 'All') {
						//$sqlBusQuery = "SELECT * FROM ".TBL_MEMBER_BUSINESS." WHERE bus_id!= 0 AND bus_meb_id IN (".$id.") AND bus_status != 'Deleted' order by bus_title";
						$sqlBusQuery="SELECT * , 'Member' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS." WHERE ". $xtracondition." bus_meb_id IN (".$id.") AND bus_status != 'Deleted' UNION SELECT * , 'Subscriber' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS_SUB." WHERE ". $xtracondition." bus_sub_id IN (".$id.") AND bus_status != 'Deleted' ORDER BY ".$orderBy;
					} else {
						//FOR Bus_category  
	                  	if($orderBy == "bus_category") {
	                       $ts = "bus_buc_id IN (SELECT buc_id FROM tbl_business_category WHERE buc_name like '". $searchchar . "%')";
	                  	} else {
	                       $ts = $orderBy." LIKE '". $searchchar . "%'";
	                  	}
	                  	//FOR Bus_category 	
						
						//$ts = $searchchar . "%";
						//$sqlBusQuery = "SELECT * FROM ".TBL_MEMBER_BUSINESS." WHERE bus_title LIKE '".$ts."' AND bus_id!= 0 AND bus_meb_id IN (".$id.") AND bus_status != 'Deleted' order by bus_title";
						$sqlBusQuery="SELECT * , 'Member' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS." WHERE ". $xtracondition . $ts ." AND bus_meb_id IN (".$id.") AND bus_status != 'Deleted' UNION SELECT * , 'Subscriber' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS_SUB." WHERE ". $xtracondition . $ts ." AND bus_sub_id IN (".$id.")  AND bus_status != 'Deleted' ORDER BY ".$orderBy;
					}
				}
			} else {
				//$sqlBusQuery = "SELECT * FROM ".TBL_MEMBER_BUSINESS." WHERE bus_id!= 0 AND bus_meb_id IN (".$id.") AND bus_status != 'Deleted' order by bus_title";
				$sqlBusQuery="SELECT * , 'Member' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS." WHERE ". $xtracondition ." bus_meb_id IN (".$id.") AND bus_status != 'Deleted' UNION SELECT * , 'Subscriber' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS_SUB." WHERE ". $xtracondition." bus_sub_id IN (".$id.")  AND bus_status != 'Deleted' ORDER BY ".$orderBy;
			}

			$BusResult  = $db->select($sqlBusQuery);
			$totBus = count($BusResult);

			$sqlSerQuery = "SELECT * FROM ".TBL_MEMBER_SERVICES." WHERE ser_id!= 0 AND ser_meb_id IN (".$id.") AND ser_status != 'Deleted' order by ser_id desc";
			$SerResult  = $db->select($sqlSerQuery);
			$totSer = count($SerResult);			
			
		}
		
		$excelcount = count($BusResult);
		#################################  Paging Query + Paging Code ##################################
		$paging_query = $sql;
		$paging_result = $db->select($paging_query);	
		$count = count($paging_result);
		$per_page = SITE_PAGING_PER_PAGE; //rows per page
		$per_page = 10;
		$pages = ceil($count/$per_page);		
		#################################################################################################
		if(!empty($_REQUEST['pagem']))	
			$page = $_REQUEST['pagem'];
		else
			$page = 1;

		$list_query = $sql;
		if(!empty($per_page) && $_REQUEST['pagem']!="all")
		{
			$start = ($page-1)*$per_page;
			if($start<0)
			{
				$start=0;
			}
			$list_query .= " limit $start,$per_page";	
		}
		$BusResult = $db->select($list_query);	
		$total_rows = count($BusResult);


		$cate_data = '';
		$cate_data = getBusinessCategoryList('bus_category','bus_category',$dirId);

        $_REQUEST['orderby'] = 'bus_name';

		$check_con = '';
		$dir_data = '<ul>';
			if($total_rows>0) {
				for($j=0;$j<$total_rows;$j++) {

					$file_type = '';
					$file_type_main = '';
					$data_type='';
					if($BusResult[$j]['Cardtype']=="Member") {
						$uploadFILEWWW = UPLOAD_WWW_BUSINESS_FILE;
					    $categoryName = getCategoryNameByMebId($BusResult[$j]['bus_meb_id']);
					    $categoryName = $BusResult[$j]['bus_category'];
					    //$file_type = 'card_details.html';
					    $file_type = 'card_details_main.html';
					    $file_type_main = 'card_details_main.html';
					    $data_type = 'Bus';
					}
					if($BusResult[$j]['Cardtype']=="Subscriber") {
						$uploadFILEWWW = UPLOAD_WWW_BUSINESS_FILE."Business_Sub/";
						$categoryName = getCategoryNameByMebId($BusResult[$j]['bus_meb_id']);
		    			$categoryName = $BusResult[$j]['bus_category'];
		    			//$file_type = 'card_details_sub.html';
		    			$file_type = 'card_details_sub_main.html';
		    			$file_type_main = 'card_details_sub_main.html';
		    			$data_type = 'Sub';
					}
					//for image 
					$logo_image='';
					if($BusResult[$j]['bus_logo_file']!="") {
						$fileNameExt = explode(".", $BusResult[$j]['bus_logo_file']);
						$fileExt 	 = $fileNameExt[count($fileNameExt)-1];
						$allowedImgExts = array("gif","jpg","jpeg","png","GIF","JPG","JPEG","PNG");
						if(in_array($fileExt,$allowedImgExts)) {
							if($BusResult[$j]['Cardtype']=="Subscriber")
								$logo_image = "<img class='company_symbol' src='".UPLOAD_WWW_BUSINESS_FILE."Business_Sub/".$BusResult[$j]['bus_id']."/".$BusResult[$j]['bus_logo_file']."'/>";
							else 
								$logo_image = "<img class='company_symbol' src='".UPLOAD_WWW_BUSINESS_FILE.$BusResult[$j]['bus_id']."/".$BusResult[$j]['bus_logo_file']."'/>";
						} else {
							$logo_image = "<img class='company_symbol' src='images/no_logo.png' />";
						}
					} else {
						$logo_image = "<img class='company_symbol' src='images/no_logo.png' />";
					}
					
					$profile_image='';
					if($BusResult[$j]['bus_picture_file']!="") {
						$fileNameExt = explode(".", $BusResult[$j]['bus_picture_file']);
						$fileExt 	 = $fileNameExt[count($fileNameExt)-1];
						$allowedImgExts = array("gif","jpg","jpeg","png","GIF","JPG","JPEG","PNG");
						if(in_array($fileExt,$allowedImgExts)) {
							if($BusResult[$j]['Cardtype']=="Subscriber")
								$profile_image = "<img class='company_symbol' src='".UPLOAD_WWW_BUSINESS_FILE."Business_Sub/".$BusResult[$j]['bus_id']."/".$BusResult[$j]['bus_picture_file']."'/>";
							else
								$profile_image = "<img class='company_symbol' src='".UPLOAD_WWW_BUSINESS_FILE.$BusResult[$j]['bus_id']."/".$BusResult[$j]['bus_picture_file']."'/>";
						} else {
							$profile_image = "<img class='company_symbol' src='images/profile.png' />";
						}
					} else {
						$profile_image = "<img class='company_symbol' src='images/profile.png' />";
					}
					//for image
					
					//print_r($BusResult[$j]);
					$dir_data .= '<li><a href="#">';
						
						//user image 
						$dir_data .= '<div class="user_img view_user_profile" data_file="'.$file_type_main.'" data_type="'.$data_type.'" data_bus_id='.$BusResult[$j]['bus_id'].'>'.$profile_image.'</div>';
						//$dir_data .= $categoryName.' | ';
						$dir_data .= '<span data_file="'.$file_type.'" data_type="'.$data_type.'" data_bus_id='.$BusResult[$j]['bus_id'].' class="bus_list_for_detail">'.$logo_image;

                        $dir_data .= '<div class="dir_list_over">';
						if($_REQUEST['orderby'] == 'bus_name')
							$dir_data .= '<b>';
						$dir_data .= $BusResult[$j]['bus_name'];
						if($_REQUEST['orderby'] == 'bus_name')
							$dir_data .= '</b>';

						if($_REQUEST['orderby'] == 'bus_name')
							$dir_data .= '<b>';
						$dir_data .= ' '.$BusResult[$j]['bus_lname'];
						if($_REQUEST['orderby'] == 'bus_name')
							$dir_data .= '</b>';
						
						$dir_data .= '</div><div class="dir_list_over">';

						if($_REQUEST['orderby'] == 'bus_title')
							$dir_data .= '<b>';
						$dir_data .= $BusResult[$j]['bus_title'];
						if($_REQUEST['orderby'] == 'bus_title')
							$dir_data .= '</b>';

						$dir_data .= '</div>';

						if($_REQUEST['orderby'] == 'bus_company')
							$dir_data .= '<b>';
						$dir_data .= '<div class="dir_list_over">'.$BusResult[$j]['bus_company'].'</div>';
						if($_REQUEST['orderby'] == 'bus_company')
							$dir_data .= '</b>';

					
						//$dir_data .= ' | ';

						$dir_data .= '</span>';
						
						//check for favrote
						$sql_query_fave = "SELECT p.* FROM ".TBL_SHARE." p WHERE (p.shr_meb_id = ".$SubcriberId." AND p.shr_bus_id = ".$BusResult[$j]['bus_id'].") AND (p.shr_rcv_email='' OR p.shr_rcv_email='0') AND shr_status = 'Active' order by p.shr_id";
						$fave_data  = $db->select($sql_query_fave);
						
						$show='';
						if(count($fave_data)>0)
							$show='style="display: none;"';
						
						//check for favrote
						$dir_data .= '<i '.$show.' data_file="'.$file_type.'" data_type="'.$data_type.'" data_bus_id='.$BusResult[$j]['bus_id'].' class="favrote_icon favrote_iconbtn"></i>';

						//$dir_data .= ' | ';

					$dir_data .='</a></li>';
					$categoryName='';
				}

				if($pages>1) {

					$page = ($page == 0 ? 1 : $page);
					$start = ($page-1) * $per_page;
					$adjacents = "1";		

					$prev = $page - 1;
					$next = $page + 1;
					$lastpage = ceil($count/$per_page);
					$lpm1 = $lastpage - 1;   

					$pagination = '<div class="paging"><ul>';

					if ($page > 1)
			            $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getAjaxPaging(\"Directory\",\"".$pages."\",\"".$prev."\");'>&laquo; Previous</a></li>";
			        else
			            $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;'><span class='disabled'>&laquo; Previous</span></a></li>";


			        if ($lastpage < 7 + ($adjacents * 2)){  
			            for ($counter = 1; $counter <= $lastpage; $counter++){
			                if ($counter == $page)
			                    $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' class='active'><span>$counter</span></a></li>";
			                else
			                    $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getAjaxPaging(\"Directory\",\"".$pages."\",\"".$counter."\");'>$counter</a></li>";
			            }
			        } 

			        elseif($lastpage > 5 + ($adjacents * 2)) {
						if($page < 1 + ($adjacents * 2)){
			                for($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
			                    if($counter == $page)
			                        $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' class='active'><span>$counter</span></a></li>";
			                    else
			                        $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getAjaxPaging(\"Directory\",\"".$pages."\",\"".($counter)."\");'>$counter</a></li>";    
			                }
			                $pagination.= "<li style='padding: 0px;'><a style='padding: 6px 1px;'>...</a></li>";
			                //$pagination.= "<li><a href='javascript:void(0);' onClick='getAjaxPaging(\"Directory\",\"".$pages."\",\"".($lpm1)."\");'>$lpm1</a></li>";
			                $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getAjaxPaging(\"Directory\",\"".$pages."\",\"".($lastpage)."\");'>$lastpage</a></li>";  

			 			} elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
			               $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getAjaxPaging(\"Directory\",\"".$pages."\",\"1\");'>1</a></li>";
			               //$pagination.= "<li><a href='javascript:void(0);' onClick='getAjaxPaging(\"Directory\",\"".$pages."\",\"2\");'>2</a></li>";
			               $pagination.= "<li style='padding: 0px;'><a style='padding: 6px 1px;'>...</a></li>";

			               for($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
			                   if($counter == $page)
			                       $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' class='active'><span>$counter</span></a></li>";
			                   else
			                       $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getAjaxPaging(\"Directory\",\"".$pages."\",\"".($counter)."\");'>$counter</a></li>";    
			               }
			               $pagination.= "<li style='padding: 0px;'><a style='padding: 6px 1px;'>...</a></li>";
			               //$pagination.= "<li><a href='javascript:void(0);' onClick='getAjaxPaging(\"Directory\",\"".$pages."\",\"".($lpm1)."\");'>$lpm1</a></li>";
			               $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getAjaxPaging(\"Directory\",\"".$pages."\",\"".($lastpage)."\");'>$lastpage</a></li>";  

			           	} else {
			               $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getAjaxPaging(\"Directory\",\"".$pages."\",\"1\");'>1</a></li>";
			               //$pagination.= "<li><a href='javascript:void(0);' onClick='getAjaxPaging(\"Directory\",\"".$pages."\",\"2\");'>2</a></li>";
			               $pagination.= "<li style='padding: 0px;'><a style='padding: 6px 1px;'>...</a></li>";
			               for($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
			                   if($counter == $page)
			                        $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' class='active'><span>$counter</span></a></li>";
			                   else
			                        $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getAjaxPaging(\"Directory\",\"".$pages."\",\"".($counter)."\");'>$counter</a></li>";    
			               }
			           	}
			        }	

			        if($page < $counter - 1)
			            $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;' href='javascript:void(0);' onClick='getAjaxPaging(\"Directory\",\"".$pages."\",\"".($next)."\");'>Next &raquo;</a>";
			        else
			            $pagination.= "<li style='padding: 1px;'><a style='padding: 6px 6px;'><span class='disabled'>Next &raquo;</span></a></li>";


					/*for($t=1;$t<=$pages;$t++) {
						if($page==$t) {
							$pagination .= '<li><a class="active" href="javascript:void(0);"';
							if($pages > 1){
								$pagination .= 'onclick="getAjaxPaging(\'Directory\',\''.$pages.'\',\''.$t.'\')"'; 
							}
							$pagination .= '>'.$t.'</a></li>';
							} else {
							$pagination .= '<li><a href="javascript:void(0);"';
							if($pages > 1){ 
								$pagination .= 'onclick="getAjaxPaging(\'Directory\',\''.$pages.'\',\''.$t.'\')"';
							}
							$pagination .= '>'.$t.'</a></li>';
						}
					}*/
					
					$pagination .= '</div></ul>';
				}

			} else {
				$dir_data .= '<li style="height: auto;"><a class="no-record" href="#">No group members found.</a></li>';
			}
		$dir_data .= '</ul>';
		$success = array("status" => 'true', 'data' => $dir_data, 'cate_data' => $cate_data, 'pagination' => $pagination,'total_page'=>$pages);
		 
   	} else if($action == 'GetDirecotyDetail') {
		$dirId = $_REQUEST['direid'];
		$sub_email = $_REQUEST['email'];
		$SubcriberId = $_REQUEST['SubcriberID'];
		$subribe	= $_REQUEST['subribe'];
		$upgrade	= $_REQUEST['Upgrade'];
		
		$uploadFILEURL = UPLOAD_DIR_SERVICE_FILE;
		$uploadFILEWWW = UPLOAD_WWW_SERVICE_FILE;
		
		$dir_fields = array("*");
		$dir_where  = "dir_id = ".$dirId." AND dir_status = 'Active'";
		$dirRes 	= $db->selectData(TBL_DIRECTORY ." LEFT JOIN ".TBL_DIRECTORY_CATEGORY." ON dir_dic_id = dic_id"." LEFT JOIN ".TBL_STATE." ON dir_sta_id = sta_id"." LEFT JOIN ".TBL_COUNTRY." ON dir_con_id = con_id",$dir_fields,$dir_where,$extra="",2);		

		$check_con = '';
		if($sub_email != '') {
			$SECTION						=	"Subscribers";
			$SECTION_TABLE 			= 	TBL_MEMBER_SUBSCRIBERS;
			$SECTION_TABLE_DIR 		= 	TBL_SUBSCRIBER_INV;
			$SECTION_FIELD_PREFIX	=	'inv_';
			
			$sub_fields = array("*");
			$sub_where = $SECTION_FIELD_PREFIX ."sub_id='".$SubcriberId."' and ".$SECTION_FIELD_PREFIX."dir_id=".$dirRes[0]["dir_id"];
			$mebSub = $db->selectData($SECTION_TABLE_DIR,$sub_fields,$sub_where,$extra="",2);
			
			if(count($mebSub)>0) {
				if($mebSub[0]['inv_status'] == 'Active') {
					$check_con = "checked='checked'";
				}
			}
		}

		if($dirId != '') {
			$meb_fields = array("meb_id");

			/*$meb_where  = "meb_dir_id= ".$dirId." AND meb_status != 'Deleted'";
			$mebRes 	= $db->selectData(TBL_MEMBER,$meb_fields,$meb_where,$extra="",1);

			for($i=0;$i<count($mebRes);$i++) {
				if($i==count($mebRes)-1) {
					$addComma = "";
				} else {
					$addComma = ",";
				}
				$allMeb .= $mebRes[$i]["meb_id"].$addComma;
			}*/

			if($dirId!='') {

                //count($dirId) > 0
				//$sql="SELECT * FROM ".TBL_MEMBER_SERVICES." WHERE ser_meb_id IN (".$allMeb.")  AND ser_status != 'Deleted'";
                $sql="SELECT * FROM ".TBL_MEMBER_SERVICES." WHERE ser_dir_id ='".$dirId."' AND ser_status != 'Deleted'";
			    $servicesResult  = $db->select($sql);
			    $totServices = count($servicesResult);
			}
		}	

		$dir_name = '';
		$dir_name = $dirRes[0]["dir_name"];

        $terms_condition_content='';
        $terms_condition_content_cencel='';
        $is_term_and_con = 'no';
        if($dirRes[0]["dir_t_c"]) {
            $is_term_and_con = 'yes';
            $terms_condition_content='<p>'.$dirRes[0]["dir_t_c_description"].'</p>';
            $terms_condition_content_cencel='<p>'.$dirRes[0]["dir_t_c_description"].'</p>';
        }

		$selectDir = mysql_query("select * from tbl_subscriber_directory where dir_id = '".$dirId."' AND sbf_id='".$SubcriberId."'");
		$fetchDir = mysql_fetch_assoc($selectDir);
		
		$sub_buttons='';
		
		if(true) {
			$yotal_eqry='SELECT COUNT(sbm_id) AS total from tbl_member_subscribe_directory where sbm_dir_id = "'.$dirId.'" AND sbm_status="Active"';
            $total_follo = $db->select($yotal_eqry);
            $total = number_format($total_follo[0]['total']);

            $sub_buttons .='<span style="display: inline; float: left; margin-top: 5px; color: rgb(22, 131, 202);">Followers<br><span style="text-align: left; width: 100%; font-weight: bold;">'.$total.'</span></span>';
	       	$sub_buttons .= '<ul style="display: inline;">';
	      	$stdc = '';
			$stda = '';
			if($fetchDir['dir_id'] != "") {
				$stda = 'display:none;';
	      		$stdc = '';
	      	} else if($fetchDir['dir_id'] == "") {
	      		$stdc = 'display:none;';
	      		$stda = '';
	      	}
	      	
	      	//for user card
	      	$type = 'Business';
	      	if($type!="Business") {
	      		$sqlCondition = "AND p.bus_meb_id IN (".$meb_dirId.")";
	      		$sql_query_card="SELECT * , 'Member' AS Cardtype  FROM ".TBL_MEMBER_BUSINESS." WHERE bus_meb_id IN (".$meb_dirId.") AND bus_status != 'Deleted' UNION select * , 'Subscriber' AS Cardtype  From ".TBL_MEMBER_BUSINESS_SUB." where bus_status='Active' AND bus_sub_id in(Select sub_id from ".TBL_MEMBER_SUBSCRIBERS." where sub_type='business' and sub_dir_id=".$mebId.") ORDER BY ".$SECTION_FIELD_PREFIX.$orderby." ".$ORDER;
	      	} else {
	      		$sqlCondition = " AND bus_sub_id=".$SubcriberId;
	      		$sql_query_card = "SELECT * FROM tbl_member_business_sub WHERE bus_id != 0 AND bus_status != 'Deleted'".$sqlCondition;
	      	}
	      	$card_list  = $db->select($sql_query_card);
	      	
	      	//for user card
	      	
			if(false) { //$upgrade == 'False'
				$sub_buttons .= '<li style="display:none; width:50%" class="upgrade_checkbox cancel"><a href="#" onclick="return upgrades(\'cancel\',\''.$dirRes[0]["dir_id"].'\')"><i class="remove_gr_icon remove_group"></i><span>Remove Group</span></a></li>';
				$sub_buttons .= '<li style="width:50%" class="upgrade_checkbox add"><a href="#" onclick="return upgrades(\'1\',\''.$dirRes[0]["dir_id"].'\')"><i class="add_group"></i><span>Save Group</span></a></li>';
			} 
			
			if(count($card_list)<=0) {
				//$sub_buttons .= '<li style="display:none; width:50%" class="upgrade_checkbox cancel"><a href="#" onclick="return upgrades(\'cancel\',\''.$dirRes[0]["dir_id"].'\')"><i class="remove_gr_icon remove_group"></i><span>Remove Group</span></a></li>';
				$sub_buttons .= '<li style="width:50%" class="upgrade_checkbox add"><a href="#" onclick="$.mobile.changePage(\'create_card_step1.html\');"><i class="add_group"></i><span>Save Group</span></a></li>';
			} else {
	      		$sql_directory="SELECT * FROM tbl_member_subscribe_directory WHERE sbm_sub_id ='".$SubcriberId."' AND sbm_dir_id='".$dirId."' AND sbm_type='Subscriber' AND sbm_status != 'Deleted'";
				$directoryResult  = $db->select($sql_directory);
				$totdirectoryResult = count($directoryResult);
				$stc = '';
				$sta = '';
				if($totdirectoryResult > 0) {
	      			$sta = 'display:none;';
	      			$stc = '';
	      		} else {
	      			$stc = 'display:none;';
	      			$sta = '';
	      		}
	      		$sub_buttons .= '<li style="'.$stc.' width:50%"><a href="#" data_tot_sub="'.$totdirectoryResult.'" class="upgrade_checkbox cancel" onclick="return upgrades(\'cancel\',\''.$dirRes[0]["dir_id"].'\', \''.$is_term_and_con.'\')"><i class="remove_gr_icon remove_group"></i><span>Remove Group</span></a></li>';
	      		$sub_buttons .= '<li style="'.$sta.' width:50%"><a href="#" data_tot_sub="'.$totdirectoryResult.'" class="upgrade_checkbox add" onclick="return upgrades(\'add\',\''.$dirRes[0]["dir_id"].'\', \''.$is_term_and_con.'\')"><i class="add_group"></i><span>Save Group</span></a></li>';
	      			
	      	}
	      	
	      	$sub_buttons .= '<li style="'.$stda.' width:50%" class="subscibe_checkboxs add_dir"><a href="#" onclick="return subscribenew(\'add\', \''.$dirRes[0]["dir_id"].'\')"><i class="favrote_icon"></i><span>Favorite</span></a></li>';
	      	$sub_buttons .= '<li style="'.$stdc.' width:50%" class="subscibe_checkboxs cancel_dir"><a href="#" onclick="return subscribenew(\'cancel\', \''.$dirRes[0]["dir_id"].'\')"><i class="favrote_remove_icon"></i><span>Remove me</span></a></li>';
	      	
	      	$sub_buttons .= '</ul>';
   		}

		$dir_data .= '<div class="error_msg" style="display:none;"></div><div class="fl_left">
		<strong>'.$dirRes[0]["dir_name"];
       	$dir_data .= '</strong>';
		$dir_data .= $dirRes[0]["dir_address"].' '.$dirRes[0]["sta_name"].'<br/> '.$dirRes[0]["con_name"].' '.$dirRes[0]["dir_zip"].'
        <div class="b2b_but" style="display: none;" ><a data-ajax="false" load="yes" onclick="return setderectory(\''.$dirRes[0]["dir_id"].'\')" href="listing.html" >browse</a></div></div>
		<div class="new_button_phone_section"><span class="phone_no"><i class="cercle_i phone_icon"></i>'.$dirRes[0]["dir_office_phone"].'</span>';
		
		$dir_data .='<div class="footer_menu hover_icon header_menu subcribe_buttons" data-role="navbar">'.$sub_buttons.'</div></div>';
        
       	
        $ser_data = '';	
        if($totServices>0) {
        	$ser_data .= '<div class="company_slider">';
            	$b=1;
                for($k=0;$k<$totServices;$k++) {
                	$logo_src='';
                	if($servicesResult[$k]['ser_logo_file']!='') {
                		$logo_src=$uploadFILEWWW.$servicesResult[$k]['ser_id'].'/'.$servicesResult[$k]['ser_logo_file'];
                	} else {
                		$logo_src='images/no_logo.png';
                	}

                    $ser_link='';
                    if($servicesResult[$k]['ser_link']!='') {
                        $ser_link=$servicesResult[$k]['ser_link'];
                    }


                	$ser_data .= '<div class="slide_company_item">';
                	//$ser_data .= '<div class="enterTitle light"><p style="width:100%;">'.$servicesResult[$k]['ser_title'].'</p></div>';
                	$ser_data .= '<a class="service_in_app_link" href="javascript:void(0);" data_url="'.$ser_link.'"><img src="'.$logo_src.'" height="40px;" /></a>';
                    //$ser_data .= '<p>'.$servicesResult[$k]['ser_description'].'</p>';
                    $ser_data .= '</div>';
                }
            $ser_data .= '</div>';
       	} else {
       		$ser_data .= '';
       	}

        $success = array("status" => 'true', 'data' => $dir_data, 'ser_data' => $ser_data, 'dir_name' => $dir_name, 'terms_condition_content' =>$terms_condition_content, 'terms_condition_content_cencel' => $terms_condition_content_cencel);
        //, 'sub_buttons'=>$sub_buttons
       	//$success = array("status" => 'true', 'data' => $dirRes, 'ser_data' => $servicesResult, 'dir_name' => $dir_name);
		 
   } else if($action == 'GetBottomBanner') {

   		$sub_id 					= 	$_REQUEST['SubcriberId'];
		$sub_email					= 	$_REQUEST['email'];

   		$banner_fields = array("ban_id","ban_title","ban_image","ban_url");
		$banner_where  = "ban_status = 'Active'";
		$bannerRes 	= $db->selectData(TBL_FRONT_BANNER,$banner_fields,$banner_where,$extra="",2);
		$dir_data = '';
		
		$dir_data .= '<div id="owl-bottom-slider" class="landing_image flexslider owl-carousel owl-theme">';	
			if($bannerRes>0) {
				//$dir_data .= '<ul class="slides">';
				for($i=0;$i<count($bannerRes);$i++) {
					if($bannerRes[$i]['ban_url']=="") {
						$banurl = "";
						$target = "";
					} else {
						$banurl = $bannerRes[$i]['ban_url'];
						$target = "target='_blank'";
					}
					$dir_data .= '<div class="item"><a class="service_in_app_link" href="javascript:void(0);" data_url="'.$banurl.'">';
					$dir_data .= '<img src="'.UPLOAD_WWW_BANNER_FILE.$bannerRes[$i]['ban_id'].'/'.$bannerRes[$i]['ban_image'].'" />';
					$dir_data .= '</a></div>';
				}
				//$dir_data .= '</ul>';
			}
		$dir_data .= '</div>';

        $dir_data = '';

		$upgrade = '';
		if($sub_id != '') {
			$SECTION				=	"Subscribers";
			$SECTION_TABLE 			= 	TBL_MEMBER_SUBSCRIBERS;
			
			$meb_fields = array("*");
			$meb_where = "sub_status!='Deleted' AND sub_id='".$sub_id."'";
			$mebRes = $db->selectData($SECTION_TABLE,$meb_fields,$meb_where,$extra="",2);

			if(count($mebRes) > 0) {
				$upgrade = $mebRes[0]['sub_upgrade'];
			}
		}

		$success = array("status" => 'true', 'data' => $dir_data, 'upgrade' => $upgrade); 	
   	} else if($action == 'GetFeatureBanner') {

        $sub_id 					= 	$_REQUEST['SubcriberId'];
        $sub_email					= 	$_REQUEST['email'];

        $banner_fields = array("ban_id","ban_title","ban_image","ban_url");
        $banner_where  = "ban_status = 'Active'";

        $extra = ' ORDER BY RAND()';

        $bannerRes 	= $db->selectData(TBL_FRONT_BANNER,$banner_fields,$banner_where,$extra,2);
        $dir_data = '';

        $dir_data .= '<div id="feature_slider" class="owl-carousel owl-theme">';
        if($bannerRes>0) {
            //$dir_data .= '<ul class="slides">';
            for($i=0;$i<count($bannerRes);$i++) {
                if($bannerRes[$i]['ban_url']=="") {
                    $banurl = "";
                    $target = "";
                } else {
                    $banurl = $bannerRes[$i]['ban_url'];
                    $target = "target='_blank'";
                }
                $dir_data .= '<div class="item"><a class="service_in_app_link" href="javascript:void(0);" data_url="'.$banurl.'">';
                $dir_data .= '<img src="'.UPLOAD_WWW_BANNER_FILE.$bannerRes[$i]['ban_id'].'/'.$bannerRes[$i]['ban_image'].'" />';
                $dir_data .= '</a></div>';
            }
            //$dir_data .= '</ul>';
        }
        $dir_data .= '</div>';

        $upgrade = '';
        if($sub_id != '') {
            $SECTION						=	"Subscribers";
            $SECTION_TABLE 			= 	TBL_MEMBER_SUBSCRIBERS;

            $meb_fields = array("*");
            $meb_where = "sub_status!='Deleted' AND sub_id='".$sub_id."'";
            $mebRes = $db->selectData($SECTION_TABLE,$meb_fields,$meb_where,$extra="",2);

            if(count($mebRes) > 0) {
                $upgrade = $mebRes[0]['sub_upgrade'];
            }
        }

        $success = array("status" => 'true', 'data' => $dir_data, 'upgrade' => $upgrade);
    } else if($action == 'GetDirListMy') {

		$SECTION_FIELD_PREFIX = $_REQUEST['fieldPrefix'];
		$searchchar = mysql_real_escape_string($_REQUEST['search']);
		
		//subscriber
		$SUBSCRIBER_MAIL = $_REQUEST['subemail'];
		$SubcriberID = $_REQUEST['SubcriberId'];
		
		$uploadFILEWWW = UPLOAD_WWW_DIRECTORY_FILE;
		
		$sub_qury='';
		
		//subscriber
		
		$dirName = "(select dic_name from ".$SECTION_TABLE_CATEGORY." where dic_id = dir_dic_id ) as Dirname";		
		if ($searchchar != "") {
			$dir_fields = array("*","(select sta_name from tbl_state where sta_id=dir_sta_id) as dir_state");
			$dir_where  = "dir_status = 'Active' AND sbf_id='".$SubcriberID."' AND `dir_name` LIKE '%".$searchchar."%'";
			$sql_query	= TBL_DIRECTORY." LEFT JOIN ".TBL_SUBSCRIBER_DIRECTORY." AS SD ON SD.dir_id = ".TBL_DIRECTORY.".dir_id";

			$dirRes 	= $db->selectData($sql_query ,$dir_fields,$dir_where,$extra="",2);
		} else {
    		$dir_fields = array("*","(select sta_name from tbl_state where sta_id=dir_sta_id) as dir_state");
			$dir_where  = "dir_status = 'Active' AND sbf_id='".$SubcriberID."'";
			$sql_query	= TBL_DIRECTORY." LEFT JOIN ".TBL_SUBSCRIBER_DIRECTORY." AS SD ON SD.dir_id = ".TBL_DIRECTORY.".dir_id";

			$dirRes 	= $db->selectData($sql_query ,$dir_fields,$dir_where,$extra="",2);
		}		
		$excelcount = count($dirRes);
		
		#################################  Paging Query + Paging Code ##################################
		$paging_query = $sql_query;
		$paging_result = $db->selectData($paging_query,$dir_fields,$dir_where,$extra="",2);	

		$count = count($paging_result);
		$per_page = SITE_PAGING_PER_PAGE; //rows per page
		$per_page = 10;
		$pages = ceil($count/$per_page);		
		#################################################################################################
		if(!empty($_REQUEST['page']))	
			$page = $_REQUEST['page'];
		else
			$page = 1;
		
		$dir_wherepage = $dir_where;
		$list_query = $sql_query;
		if(!empty($per_page) && $_REQUEST['page']!="all")
		{
			$start = ($page-1)*$per_page;
			if($start<0)
			{
				$start=0;
			}
			$dir_wherepage .= " limit $start,$per_page";	
		}

		$result_query = $db->selectData($list_query,$dir_fields,$dir_wherepage,$extra="",2);	
		$total_rows = count($result_query);
		//echo $total_rows;
		
		if($total_rows > 0){
			$j = 1;
			$dir_data = '<ul>';
			for($i=0;$i<$total_rows;$i++) {
				//$dir_data .= $result_query[$i]['Dirname'];
				$sub_con='';
				if($SUBSCRIBER_MAIL != '') {
					$sub_con='\'sub\'';
					$fun='setdetailpage(\''.$result_query[$i][$SECTION_FIELD_PREFIX."id"].'\', '.$sub_con.')';
				} else {
					$fun='setdetailpage(\''.$result_query[$i][$SECTION_FIELD_PREFIX."id"].'\')';
				}
				$fun='setdetailpage(\''.$result_query[$i][$SECTION_FIELD_PREFIX."id"].'\')';
				$href = '#';
				
				$logo_src='';
				if($result_query[$i]['dir_logo_file']!='') {
					$logo_src=$uploadFILEWWW.$result_query[$i]['dir_id'].'/'.$result_query[$i]['dir_logo_file'];
				} else {
					//$logo_src='images/no_logo.png';
                    $logo_src='images/member_group_top.png';
				}
				
				//$dir_data .= '<li id="li_dir_id_'.$result_query[$i][$SECTION_FIELD_PREFIX."id"].'"><a onclick="'.$fun.'" href="'.$href.'" data-ajax="true" load="yes" data-prefetch="true">'.$result_query[$i][$SECTION_FIELD_PREFIX.'name'].'<div class="dir_extra"><span>City:</span><span class="text">'.$result_query[$i]['dir_city'].'</span><span>State:</span><span class="text">'.$result_query[$i]['dir_state'].'</span><span>Zip Code:</span><span class="text">'.$result_query[$i]['dir_zip'].'</span></div></a><div class="masage_div"><a href="#" onclick="return act_remove(\''.$result_query[$i][$SECTION_FIELD_PREFIX."id"].'\')"><img src="images/archive.png" /></a></div></li>';
				$dir_data .= '<li><a onclick="'.$fun.'" href="'.$href.'" data-ajax="true" load="yes" data-prefetch="true"><img src="'.$logo_src.'" style="max-height:24px;">';
				//$dir_data .= '<div class="dir_list_over"><span><b>'.$result_query[$i][$SECTION_FIELD_PREFIX.'name'].'</b> | '.$result_query[$i]['dir_city'].' | '.$result_query[$i]['dir_state'].' | '.$result_query[$i]['dir_zip'].'</span><span></span>';
                $dir_data .= '<div class="dir_list_over"><span><b>'.$result_query[$i][$SECTION_FIELD_PREFIX.'name'].'</b></span></div>';
				$dir_data .= '</a><i onclick="return act_remove(\''.$result_query[$i][$SECTION_FIELD_PREFIX."id"].'\')" class="delete_icon"></i></li>';
			}

			$dir_data .= '</ul>';

			if(count($pages)>0) {
			$pagination = '<div class="paging"><ul>';
				for($t=1;$t<=$pages;$t++) {
					if($page==$t) {
						$pagination .= '<li><a class="active" href="javascript:void(0);"';
						if($pages > 1){
							$pagination .= 'onclick="getAjaxPaging(\'Directory\',\''.$pages.'\',\''.$t.'\')"'; 
						}
						$pagination .= '>'.$t.'</a></li>';
						} else {
						$pagination .= '<li><a href="javascript:void(0);"';
						if($pages > 1){ 
							$pagination .= 'onclick="getAjaxPaging(\'Directory\',\''.$pages.'\',\''.$t.'\')"';
						}
						$pagination .= '>'.$t.'</a></li>';
					}
				}
				$pagination .= '</div></ul>';	
						
			}
			
			$success = array("status" => 'true', 'data' => $dir_data, 'pagination' => $pagination, 'total_page'=>$pages);			
		} else {
			$dir_data = '<ul>';
			$dir_data .= '<li><a href="javascript:void(0);">No record found.</a></li>';
			$dir_data .= '</ul>';
			$success = array("status" => 'true', 'data'=>$dir_data, 'pagination' => '');
		}	
	} else if($action == 'GetMySubDir') {
        $SECTION_FIELD_PREFIX = 'dir_';
        $managePage = 'manage_directory';
        $SECTION_TABLE = TBL_DIRECTORY;
        $SECTION_MANAGE_PAGE 	=   ADM_MANAGE_DIRECTORY;
        $SECTION_TABLE_CATEGORY	=   TBL_DIRECTORY_CATEGORY;
        $orderby = "name";
        $ORDER =	"asc";
        $uploadFILEWWW = UPLOAD_WWW_DIRECTORY_FILE;
        //subscriber
        $SUBSCRIBER_MAIL = $_REQUEST['subemail'];
        $SubcriberID = $_REQUEST['SubcriberId'];

        $dirName = "(select dic_name from ".$SECTION_TABLE_CATEGORY." where dic_id = dir_dic_id ) as Dirname";

        $sql_query = "SELECT p.*,".$dirName.",(select sta_name from tbl_state where sta_id=dir_sta_id) as dir_state FROM ".$SECTION_TABLE." p WHERE p.".$SECTION_FIELD_PREFIX."id != 0 AND p.".$SECTION_FIELD_PREFIX."id IN (select sbm_dir_id from tbl_member_subscribe_directory WHERE sbm_sub_id='".$SubcriberID."' AND sbm_status='Active') AND p.".$SECTION_FIELD_PREFIX."status != 'Deleted' AND p.".$SECTION_FIELD_PREFIX."status != 'Inactive' order by p.".$SECTION_FIELD_PREFIX.$orderby." ".$order;

        //echo $sql_query;
        $excel_query = "SELECT p.*,".$dirName.",(select sta_name from tbl_state where sta_id=dir_sta_id) as dir_state FROM ".$SECTION_TABLE." p WHERE p.".$SECTION_FIELD_PREFIX."id != 0 AND p.".$SECTION_FIELD_PREFIX."id IN (select sbm_dir_id from tbl_member_subscribe_directory WHERE sbm_sub_id='".$SubcriberID."' AND sbm_status='Active') AND p.".$SECTION_FIELD_PREFIX."status != 'Deleted' AND p.".$SECTION_FIELD_PREFIX."status != 'Inactive' order by p.".$SECTION_FIELD_PREFIX.$orderby." ".$order;
        $excel_result  = $db->select($excel_query);

        //echo $excel_query;

        $excelcount = count($excel_result);
        #################################  Paging Query + Paging Code ##################################
        $paging_query = $sql_query;
        $paging_result = $db->select($paging_query);
        $count = count($paging_result);
        $per_page = SITE_PAGING_PER_PAGE; //rows per page
        $per_page = 10;
        $pages = ceil($count/$per_page);
        #################################################################################################
        if(!empty($_REQUEST['page']))
            $page = $_REQUEST['page'];
        else
            $page = 1;

        $list_query = $sql_query;
        if(!empty($per_page) && $_REQUEST['page']!="all") {
            $start = ($page-1)*$per_page;
            if($start<0) {
                $start=0;
            }
            $list_query .= " limit $start,$per_page";
        }

        $result_query = $db->select($list_query);
        $total_rows = count($result_query);
        //echo $total_rows;

        $dir_data = '';

        if($total_rows > 0){
            $j = 1;
            $dir_data .= '<ul>';
            for($i=0;$i<$total_rows;$i++){
                //var_dump($result_query[$i]);
                //$dir_data .= $result_query[$i]['Dirname'];
                $sub_con='';
                if($SUBSCRIBER_MAIL != '') {
                    $sub_con='\'sub\'';
                    $fun='setdetailpage(\''.$result_query[$i][$SECTION_FIELD_PREFIX."id"].'\', '.$sub_con.')';
                } else {
                    $fun='setdetailpage(\''.$result_query[$i][$SECTION_FIELD_PREFIX."id"].'\')';
                }
                $fun='setdetailpage(\''.$result_query[$i][$SECTION_FIELD_PREFIX."id"].'\')';
                $href = '#';
                //$dir_data .= '<li><a onclick="'.$fun.'" href="'.$href.'" data-ajax="true" load="yes" data-prefetch="true">'.$result_query[$i][$SECTION_FIELD_PREFIX.'name'].'<div class="dir_extra"><span>City:</span><span class="text">'.$result_query[$i]['dir_city'].'</span><span>State:</span><span class="text">'.$result_query[$i]['dir_state'].'</span><span>Zip Code:</span><span class="text">'.$result_query[$i]['dir_zip'].'</span></div></a></li>';

                $logo_src='';
                if($result_query[$i][$SECTION_FIELD_PREFIX.'logo_file']!='') {
                    $logo_src=$uploadFILEWWW.$result_query[$i][$SECTION_FIELD_PREFIX.'id'].'/'.$result_query[$i][$SECTION_FIELD_PREFIX.'logo_file'];
                } else {
                    $logo_src='images/member_group_top.png';
                }

                $dir_data .= '<li><a onclick="'.$fun.'" href="'.$href.'" data-ajax="true" load="yes" data-prefetch="true"><img src="'.$logo_src.'" style="max-height:24px;">';
                //$dir_data .= '<span><b>'.$result_query[$i][$SECTION_FIELD_PREFIX.'name'].'</b> | '.$result_query[$i]['dir_city'].' | '.$result_query[$i]['dir_state'].' | '.$result_query[$i]['dir_zip'].'</span><span></span>';
                $dir_data .= '<div class="dir_list_over"><span><b>'.$result_query[$i][$SECTION_FIELD_PREFIX.'name'].'</b></span></div>';
                $dir_data .= '</a></li>';

            }

            $dir_data .= '</ul>';

            $success = array("status" => 'true', 'data' => $dir_data, 'pagination' => '', 'total_page'=>$pages);
        } else {
            $dir_data .= '<ul>';
            $dir_data .= '<li><a href="javascript:void(0);">No groups found.</a></li>';
            $dir_data .= '</ul>';
            $success = array("status" => 'true', 'data'=>$dir_data, 'pagination' => '');
        }
    } else {
		$success = array("status" => 'false');
	}
	echo json_encode($success);
	exit();
?>	