<?php 
	error_reporting(1);
	header('Access-Control-Allow-Origin: *');
	
	include_once('../../../include/includeclass.php');
	$success = array();
	$action = $_REQUEST['action'];
	
	function crypto_rand_secure($min, $max) {
		$range = $max - $min;
		if ($range < 0) return $min; // not so random...
		$log = log($range, 2);
		$bytes = (int) ($log / 8) + 1; // length in bytes
		$bits = (int) $log + 1; // length in bits
		$filter = (int) (1 << $bits) - 1; // set all lower bits to 1
		do {
			$rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
			$rnd = $rnd & $filter; // discard irrelevant bits
		} while ($rnd >= $range);
		return $min + $rnd;
	}
	
	if($action == 'RegisterUser') {
		$data = $_REQUEST;	

		unset($add_values);
		
		$add_values['sub_type']		 	 = 'Free';
		
		if(DEFAULT_GROUP_ID!='') {
			$add_values['sub_dir_id']		= '167';
			$add_values['sub_upgrade'] 		= "True";
		}
		$add_values['sub_email'] 		 = $data['email'];
		$add_values['sub_password'] 	 = md5($data['password']);
		$add_values['sub_phone']		 = $data['mobile'];	
	 	//$add_values['sub_type']   	 = "Free";
		$add_values['sub_status'] 		 = "Active";
		$add_values['sub_created_id'] 	 = 0;
		$add_values['sub_created_date']  = date('Y-m-d H:i:s');
		$add_values['sub_updated_id']    = 0;
		$add_values['sub_updated_date']  = date('Y-m-d H:i:s');
		
		if($data['email']!='') {
			//OR sub_phone='".$add_values['sub_phone']."'
			$sql_query = "SELECT * FROM " . TBL_MEMBER_SUBSCRIBERS . " WHERE sub_email='".$add_values['sub_email']."'";
			$excel_result  = $db->select($sql_query);	
			$excelcount = count($excel_result);	
			if($excelcount>0) {
				$success = array("status" => 'false', 'data'=>'Email address already registerd.');
			} else {
				$insSubId = $db->insertData(TBL_MEMBER_SUBSCRIBERS, $add_values);
				if($insSubId > 0) {
					if(DEFAULT_GROUP_ID!='') {
						unset($add_sbm_values);
						$add_sbm_values['sbm_sub_id'] 		    = $insSubId;
						$add_sbm_values['sbm_dir_id'] 		    = $data['DirectoryId'];
						$add_sbm_values['sbm_status'] 		    = "Active";
						$add_sbm_values['sbm_created_id'] 	 	= 0;
						$add_sbm_values['sbm_created_date'] 	= date('Y-m-d H:i:s');
						$inssbmId = $db->insertData(TBL_MEMBER_SUBSCRIBE_DIRECTORY, $add_sbm_values);
					}
				}
				
				if($insSubId) {
					$success = array("status" => 'true', 'data'=>'Your registration is successfully processed.');
					//$_SESSION['msg'] = "Your subscription is successfully processed.";
				} else {
					$success = array("status" => 'false', 'data'=>'Please valid information');
				}	
			}
		} else {
			$success = array("status" => 'false', 'data'=>'Please provide proper information.');
		}

	} else if($action == 'DoNotTour') {
		$data = $_REQUEST;	
		unset($add_values);

		$SubcriberID					= $data['SubcriberID'];
		$update_values['sub_home']		= $data['chk'];
		
		$SECTION_WHERE			= 	"sub_id=".$SubcriberID;

		$GPDetail_result = $db->updateData(TBL_MEMBER_SUBSCRIBERS,$update_values,$SECTION_WHERE);

		$success = array("status" => 'true', 'data'=>'successfully');
		
	} else if($action == 'CheckUserEmail') {
		$data = $_REQUEST;	

		unset($add_values);
		
		if($data['email']!='') {
			//OR sub_phone='".$add_values['sub_phone']."'
			$sql_query = "SELECT * FROM " . TBL_MEMBER_SUBSCRIBERS . " WHERE sub_email='".$data['email']."' AND sub_status='Active'";
			$excel_result  = $db->select($sql_query);
			
			$excelcount = count($excel_result);	
			if($excelcount>0) {
				
				$toemail = $data['email'];
				$subject = "Reset your password on IVQMobile";
					
				$sender_name = 'IVQMobile';
				
				$activationkey='';
				$length='4';
				//$codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
				//$codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
				$codeAlphabet = "0123456789";
				for($i=0;$i<$length;$i++){
					$activationkey .= $codeAlphabet[crypto_rand_secure(0,strlen($codeAlphabet))];
				}
				
				$message = '';
				$message .= "<p>As requested, here is a reset password code is :</p><br/>";
				$message .= "<h1>".$activationkey."</h1>";
				
				$fromemail = "IVQMobile";
				// To send HTML mail, the Content-type header must be set
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				// Additional headers
				$headers .= 'To: '.$toemail.' <'.$toemail.'>' . "\r\n";
				$headers .= 'From: IVQMobile <mail@ivqmobile.com>' . "\r\n";
				$headers .= 'Bcc: gaurav.moradiya@iflair.com, karishma@iflair.com' . "\r\n";
				//$sendmail   = mailfunctionnew($fromemail,$toemail,$subject,$message,$custom_host_name,$custom_user_name,$custom_password,$custom_port_name,$filepath);
				$sendmail   = @mail($toemail, $subject, $message, $headers);
	
				
				$SubcriberID = $excel_result[0]['sub_id'];
				
				$update_values['sub_reset_code']	= $activationkey;
				$SECTION_WHERE						= 	"sub_id=".$SubcriberID;
				
				$GPDetail_result = $db->updateData(TBL_MEMBER_SUBSCRIBERS,$update_values,$SECTION_WHERE);
				
				$success = array("status" => 'true', 'data'=>'Successfully sent reset password code to email address.', 'code' =>$activationkey);
			} else {
				$success = array("status" => 'false', 'data'=>'Email address can not be exist.');
			}
		} else {
			$success = array("status" => 'false', 'data'=>'Please provide proper information.');
		}

	} else if($action == 'ResetPassword') {
		$data = $_REQUEST;	

		unset($add_values);
		
		if($data['email']!='' && $data['code']!='') {
			//OR sub_phone='".$add_values['sub_phone']."'
			$sql_query = "SELECT * FROM " . TBL_MEMBER_SUBSCRIBERS . " WHERE sub_email='".$data['email']."' AND sub_reset_code='".$data['code']."' AND sub_status='Active'";
			$excel_result  = $db->select($sql_query);
			
			$excelcount = count($excel_result);	
			if($excelcount>0) {
				$SubcriberID = $excel_result[0]['sub_id'];
				
				$password = md5($data['password']);

				$update_values['sub_password']		= $password;
				$update_values['sub_reset_code']	= '';
				$SECTION_WHERE						= "sub_id=".$SubcriberID;
				
				$GPDetail_result = $db->updateData(TBL_MEMBER_SUBSCRIBERS,$update_values,$SECTION_WHERE);
				
				$success = array("status" => 'true', 'data'=>'Successfully update password.');
			} else {
				$success = array("status" => 'false', 'data'=>'Email address not found.');
			}
		} else {
			$success = array("status" => 'false', 'data'=>'Please provide proper information.');
		}

	}
	
	echo json_encode($success);
	exit(); 
?>