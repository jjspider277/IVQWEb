<?php
	error_reporting(1);
	header('Access-Control-Allow-Origin: *');
	
	include_once('../../../include/includeclass.php');
	
	ini_set("upload_max_filesize", "3200000000");

	$success = array();
	
	$action = $_REQUEST['action'];

    function random_string_new($length) {
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }

	if($action == 'GetMyCards') {
		
		$mebId 					= $_REQUEST['SubcriberID'];
		$meb_dirId 				= $_REQUEST['direid'];

		$SECTION_TABLE			= 	'tbl_member_business_sub';
		$SECTION_FIELD_PREFIX	=	'bus_';
		$xtraCondition			=	stripslashes($_REQUEST['xtraCondition']);
		$searchchar				=	$_REQUEST['searchchar'];	

		if($orderby == "") {
			$ORDER =  "desc";
			$orderby = "id";
		}

		$type = 'Business';
		if($type!="Business") {
			$sqlCondition = "AND p.bus_meb_id IN (".$meb_dirId.")";
			if ($searchchar != 'undefined' && $searchchar != "") {
				if ($searchchar == 'other') {
					$sql_query="SELECT * , 'Member' AS Cardtype  FROM ".TBL_MEMBER_BUSINESS." WHERE bus_meb_id IN (".$meb_dirId.") AND bus_status != 'Deleted' AND `bus_title` REGEXP '^[^a-zA-Z]' UNION select * , 'Subscriber' AS Cardtype  From ".TBL_MEMBER_BUSINESS_SUB." where `bus_title` REGEXP '^[^a-zA-Z]'AND bus_status='Active' AND bus_sub_id in(Select sub_id from ".TBL_MEMBER_SUBSCRIBERS." where sub_type='business' and sub_dir_id=".$mebId.") ORDER BY ".$SECTION_FIELD_PREFIX.$orderby." ".$ORDER;
				} else {
					if ($searchchar == 'all') {
						$sql_query="SELECT * , 'Member' AS Cardtype  FROM ".TBL_MEMBER_BUSINESS." WHERE bus_meb_id IN (".$meb_dirId.") AND bus_status != 'Deleted' UNION select * , 'Subscriber' AS Cardtype  From ".TBL_MEMBER_BUSINESS_SUB." where bus_status='Active' AND bus_sub_id in(Select sub_id from ".TBL_MEMBER_SUBSCRIBERS." where sub_type='business' and sub_dir_id=".$mebId.") ORDER BY ".$SECTION_FIELD_PREFIX.$orderby." ".$ORDER;
					} else {
						$ts = $searchchar . "%";
						$sql_query="SELECT * , 'Member' AS Cardtype  FROM ".TBL_MEMBER_BUSINESS." WHERE bus_title LIKE '" . $ts . "' AND bus_meb_id IN (".$meb_dirId.") AND bus_status != 'Deleted' UNION select * , 'Subscriber' AS Cardtype  From ".TBL_MEMBER_BUSINESS_SUB." where bus_title LIKE '" . $ts . "' AND bus_status='Active' AND bus_sub_id in(Select sub_id from ".TBL_MEMBER_SUBSCRIBERS." where sub_type='business' and sub_dir_id=".$mebId.") ORDER BY ".$SECTION_FIELD_PREFIX.$orderby." ".$ORDER;
					}
				}
			} else if ($xtraCondition != 'undefined' && $xtraCondition != "") {
				$sql_query="SELECT * , 'Member' AS Cardtype  FROM ".TBL_MEMBER_BUSINESS." WHERE (".$xtraCondition.") AND (bus_meb_id IN (".$meb_dirId.") AND bus_status != 'Deleted') UNION select * , 'Subscriber' AS Cardtype  From ".TBL_MEMBER_BUSINESS_SUB." where (".$xtraCondition.") AND (bus_status='Active' AND bus_sub_id in(Select sub_id from ".TBL_MEMBER_SUBSCRIBERS." where sub_type='business' and sub_dir_id=".$mebId.")) ORDER BY ".$SECTION_FIELD_PREFIX.$orderby." ".$ORDER;
			} else {
				$sql_query="SELECT * , 'Member' AS Cardtype  FROM ".TBL_MEMBER_BUSINESS." WHERE bus_meb_id IN (".$meb_dirId.") AND bus_status != 'Deleted' UNION select * , 'Subscriber' AS Cardtype  From ".TBL_MEMBER_BUSINESS_SUB." where bus_status='Active' AND bus_sub_id in(Select sub_id from ".TBL_MEMBER_SUBSCRIBERS." where sub_type='business' and sub_dir_id=".$mebId.") ORDER BY ".$SECTION_FIELD_PREFIX.$orderby." ".$ORDER;
			}
		} else {
			$sqlCondition = " AND bus_sub_id=".$mebId;
			$sql_query = "SELECT * FROM ".$SECTION_TABLE." WHERE ".$SECTION_FIELD_PREFIX."id != 0 AND ".$SECTION_FIELD_PREFIX."status != 'Deleted'".$sqlCondition." order by ".$SECTION_FIELD_PREFIX.$orderby." ".$ORDER;
		}

		##########################  Paging Query + Paging Code #############################
		$paging_query = $sql_query;
		$paging_result  = $db->select($paging_query); 
		$count = count($paging_result);
		$per_page = SITE_PAGING_PER_PAGE; //rows per page
		$per_page = 10;
		$pages = ceil($count/$per_page);    
		#######################################################################################
		if($action_type == "paging") {
			if(!empty($_REQUEST['page']))   {
				$page = $_REQUEST['page'];
			}       
		} else {
			if(!empty($_REQUEST['page']))  
				$page = $_REQUEST['page'];
	  		else
			$page = 1;
		} 

		$list_query = $sql_query;
		if(!empty($per_page) && $_REQUEST['page']!="all") {
			$start = ($page-1)*$per_page;
			if($start<0) {
				$start=0;
			}
			$list_query .= " limit $start,$per_page"; 
		} 
		$result_query  = $db->select($list_query);  
		$total_rows = count($result_query);

		$dir_data = '';
		
		if($total_rows>0) {	
			$uploadFILEWWW = UPLOAD_WWW_BUSINESS_FILE."Business_Sub/";
			$file_type = 'card_details_sub_main.html';
			//$file_type_main = 'card_details_sub_main.html';
			$file_type_main = 'card_details_sub_main.html';
			$data_type = 'Sub';
			
			$dir_data .= '<ul>';
			$j=1;
			
			for($i=0;$i<$total_rows;$i++) {
				//for image
				$logo_image='';
				if($result_query[$i]['bus_logo_file']!="") {
					$fileNameExt = explode(".", $result_query[$i]['bus_logo_file']);
					$fileExt 	 = $fileNameExt[count($fileNameExt)-1];
					$allowedImgExts = array("gif","jpg","jpeg","png","GIF","JPG","JPEG","PNG");
					if(in_array($fileExt,$allowedImgExts)) {
						if($data_type=="Sub")
							$logo_image = "<img class='company_symbol' src='".UPLOAD_WWW_BUSINESS_FILE."Business_Sub/".$result_query[$i]['bus_id']."/".$result_query[$i]['bus_logo_file']."'/>";
						else
							$logo_image = "<img class='company_symbol' src='".UPLOAD_WWW_BUSINESS_FILE.$result_query[$i]['bus_id']."/".$result_query[$i]['bus_logo_file']."'/>";
					} else {
						$logo_image = "<img class='company_symbol' src='images/no_logo.png' />";
					}
				} else {
					$logo_image = "<img class='company_symbol' src='images/no_logo.png' />";
				}
					
				$profile_image='';
				if($result_query[$i]['bus_picture_file']!="") {
					$fileNameExt = explode(".", $result_query[$i]['bus_picture_file']);
					$fileExt 	 = $fileNameExt[count($fileNameExt)-1];
					$allowedImgExts = array("gif","jpg","jpeg","png","GIF","JPG","JPEG","PNG");
					if(in_array($fileExt,$allowedImgExts)) {
						if($data_type=="Sub")
							$profile_image = "<img class='company_symbol' src='".UPLOAD_WWW_BUSINESS_FILE."Business_Sub/".$result_query[$i]['bus_id']."/".$result_query[$i]['bus_picture_file']."'/>";
						else
							$profile_image = "<img class='company_symbol' src='".UPLOAD_WWW_BUSINESS_FILE.$result_query[$i]['bus_id']."/".$result_query[$i]['bus_picture_file']."'/>";
					} else {
						$profile_image = "<img class='company_symbol' src='images/profile.png' />";
					}
				} else {
					$profile_image = "<img class='company_symbol' src='images/profile.png' />";
				}
				//for image
				
				$dir_data .= '<li><a href="#">';
				
				//user image
				$dir_data .= '<div class="user_img view_user_profile" data_file="'.$file_type_main.'" data_type="'.$data_type.'" data_bus_id='.$result_query[$i]['bus_id'].'>'.$profile_image.'</div>';
				//$dir_data .= $categoryName.' | ';
				$dir_data .= '<span data_file="'.$file_type.'" data_type="'.$data_type.'" data_bus_id='.$result_query[$i]['bus_id'].' class="bus_list_for_detail">'.$logo_image;
				
				if($_REQUEST['orderby'] == 'bus_name')
					$dir_data .= '<b>';
				$dir_data .= $result_query[$i]['bus_name'];
				if($_REQUEST['orderby'] == 'bus_name')
					$dir_data .= '</b>';
				
				if($_REQUEST['orderby'] == 'bus_lname')
					$dir_data .= '<b>';
				$dir_data .= ' '.$result_query[$i]['bus_lname'];
				if($_REQUEST['orderby'] == 'bus_lname')
					$dir_data .= '</b>';
				
				$dir_data .= ' | ';
				
				if($_REQUEST['orderby'] == 'bus_title')
					$dir_data .= '<b>';
				$dir_data .= $result_query[$i]['bus_title'];
				if($_REQUEST['orderby'] == 'bus_title')
					$dir_data .= '</b>';
				
				$dir_data .= ' | ';
				
				if($_REQUEST['orderby'] == 'bus_company')
					$dir_data .= '<b>';
				$dir_data .= $result_query[$i]['bus_company'];
				if($_REQUEST['orderby'] == 'bus_company')
					$dir_data .= '</b>';
				
					
				//$dir_data .= ' | ';
				
				$dir_data .= '</span>
						<i data_file="'.$file_type.'" data_type="'.$data_type.'" data_bus_id='.$result_query[$i]['bus_id'].' onclick="return edit_card(\''.$result_query[$i][$SECTION_FIELD_PREFIX."id"].'\')" class="edit_icon edit_icon_click"></i>
								<i data_file="'.$file_type.'" data_type="'.$data_type.'" data_bus_id='.$result_query[$i]['bus_id'].' onclick="return delete_card(\''.$result_query[$i][$SECTION_FIELD_PREFIX."id"].'\')" class="delete_icon delete_icon_click"></i>';
				
				//$dir_data .= ' | ';
				
				$dir_data .='</a></li>';
				
				//$dir_data .= '<li class="bus_list_for_detail" data_bus_id="'.$result_query[$i][$SECTION_FIELD_PREFIX."id"].'"><a href="#" class="ui-link">';
					//$dir_data .= $result_query[$i][$SECTION_FIELD_PREFIX.'title'].' | '.$result_query[$i][$SECTION_FIELD_PREFIX.'company'].' | '.$result_query[$i][$SECTION_FIELD_PREFIX.'name'].' '.$result_query[$i][$SECTION_FIELD_PREFIX.'lname'];
				//$dir_data .= '</a><div class="masage_div delete_div" onclick="return delete_card(\''.$result_query[$i][$SECTION_FIELD_PREFIX."id"].'\')" ><img src="images/delete.png" /></div><div class="masage_div" onclick="return edit_card(\''.$result_query[$i][$SECTION_FIELD_PREFIX."id"].'\')" ><img src="images/edit-orange.png" /></div></li>';

			}
			
			$dir_data .= '</ul>';
		} else {
			$dir_data .= '<ul><li style="height: auto;"><a href="#"><div class="no-record">No business card created</div></a></li></ul>';
		}

		$pagination = '<div class="paging"><ul>';
		for($t=1; $t<=$pages; $t++) {
			if($page==$t)  {
				$pagination .= '<li><a class="active" href="javascript:void(0);"';
				if($pages > 1)
					$pagination .= 'onclick="getAjaxPaging(\''.$pages.'\',\''.$t.'\')"';
				$pagination .= '>'.$t.'</a></li>';
			} else {
				$pagination .= '<li><a href="javascript:void(0);"';
				$pagination .= ' onclick="getAjaxPaging(\''.$pages.'\',\''.$t.'\')"';
				$pagination .= '>'.$t.'</a></li>';
			}
		}	
		$pagination .= '</ul></div>';
		$success = array("status" => 'true', 'data' => $dir_data, 'pagination' => $pagination,'total_rec'=>$total_rows);			
		
	} else if($action == 'GetMyDirs') {
			
			$mebId = $_REQUEST['SubcriberID'];
			
			$dir_files = array("dir_id","dir_name");
         	$dir_where  = "dir_status = 'Active' and sbm_sub_id='".$mebId ."'";
          	$dirRes 	= $db->selectData(TBL_MEMBER_SUBSCRIBE_DIRECTORY." LEFT JOIN ".TBL_DIRECTORY." ON sbm_dir_id = dir_id",$dir_files,$dir_where,$extra="",2);

			$dir_data = '';
			
			$dir_data .= '<select name="directoryName" id="directoryName">';
            	for($d = 0;$d < count($dirRes);$d++) {
            		$dir_data .= '<option value="'.$dirRes[$d]['dir_id'].'"';
            		 
            		 if($dirId == $dirRes[$d]['dir_id']) 
            		 	$dir_data .= 'selected="selected"';

            		$dir_data .='>';
					$dir_data .= $dirRes[$d]['dir_name'];
            		$dir_data .= '</option>';
            	}
           	$dir_data .= '</select> ';
			
			$success = array("status" => 'true', 'data' => $dir_data);

	} else if($action == 'GetMyDirsNew') {
			
			$mebId = $_REQUEST['SubcriberID'];
			
			$dir_files = array("dir_id","dir_name");
         	$dir_where  = "dir_status = 'Active' and sbm_sub_id='".$mebId ."'";
          	$dirRes 	= $db->selectData(TBL_MEMBER_SUBSCRIBE_DIRECTORY." LEFT JOIN ".TBL_DIRECTORY." ON sbm_dir_id = dir_id",$dir_files,$dir_where,$extra="",2);

          	$selected_class='';	
			$dir_data = '';
			
			for($d = 0;$d < count($dirRes);$d++) {
            	$selected_class='';
            	if($dirId == $dirRes[$d]['dir_id'])
            		$selected_class .= 'class="active"';
            		
            		$dir_data .= '<li '.$selected_class.' data_id="'.$dirRes[$d]['dir_id'].'">
            					<a href="#">'.$dirRes[$d]['dir_name'].'</a>
            				</li>';
				}
           	$dir_data .= '';
			
			$success = array("status" => 'true', 'data' => $dir_data);

	} else if($action == 'GetDirecotyCards') {
		$dirId = $_REQUEST['direid'];
		$sub_email = $_REQUEST['email'];
		$SubcriberId = $_REQUEST['SubcriberID'];
		$subribe	= $_REQUEST['subribe'];
		$orderBy	= $_REQUEST['orderby'];
		$xtracondition_o	=	$_REQUEST['xtracondition'];

		$dir_fields = array("meb_id");
		$dir_where  = "meb_dir_id = ".$dirId." AND meb_status = 'Active'";
		$dirRes 	= $db->selectData(TBL_MEMBER,$dir_fields,$dir_where,$extra="",2);		

		if(count($dirRes) > 0) {
			$id='';
			for($i=0;$i<count($dirRes);$i++){
				$id .= $dirRes[$i]["meb_id"].',';
			}
		}		
		$id = substr($id, 0, -1);
		$searchchar = $_REQUEST['char'];
		
		if ($xtracondition_o != 'undefined' && $xtracondition_o != "") {
        	$xtracondition = $xtracondition_o." AND ";
  		} else {
        	$xtracondition = "";
  		}

		if($id!="")
		{
			if ($searchchar != 'undefined' && $searchchar != "") {
				if ($searchchar == 'Other') {
					$ts = $searchchar . "%";
					//$sqlBusQuery = "SELECT * FROM ".TBL_MEMBER_BUSINESS." WHERE bus_title REGEXP '^[^a-zA-Z]' AND bus_id!= 0 AND bus_meb_id IN (".$id.") AND bus_status != 'Deleted' order by bus_title";
					$sqlBusQuery = "SELECT * , 'Member' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS." WHERE ". $xtracondition." bus_title REGEXP '^[^a-zA-Z]' AND bus_meb_id IN (".$id.")  AND bus_status != 'Deleted' UNION SELECT * , 'Subscriber' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS_SUB." WHERE ". $xtracondition." bus_title REGEXP '^[^a-zA-Z]' AND bus_sub_id IN (".$id.")  AND bus_status != 'Deleted' ORDER BY ".$orderBy;
				} else {
					if ($searchchar == 'All') {
						//$sqlBusQuery = "SELECT * FROM ".TBL_MEMBER_BUSINESS." WHERE bus_id!= 0 AND bus_meb_id IN (".$id.") AND bus_status != 'Deleted' order by bus_title";
						$sqlBusQuery="SELECT * , 'Member' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS." WHERE ". $xtracondition." bus_meb_id IN (".$id.") AND bus_status != 'Deleted' UNION SELECT * , 'Subscriber' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS_SUB." WHERE ". $xtracondition." bus_sub_id IN (".$id.") AND bus_status != 'Deleted' ORDER BY ".$orderBy;
					} else {
						//FOR Bus_category  
	                  	if($orderBy == "bus_category") {
	                       $ts = "bus_buc_id IN (SELECT buc_id FROM tbl_business_category WHERE buc_name like '". $searchchar . "%')";
	                  	} else {
	                       $ts = $orderBy." LIKE '". $searchchar . "%'";
	                  	}
	                  	//FOR Bus_category 	
						
						//$ts = $searchchar . "%";
						//$sqlBusQuery = "SELECT * FROM ".TBL_MEMBER_BUSINESS." WHERE bus_title LIKE '".$ts."' AND bus_id!= 0 AND bus_meb_id IN (".$id.") AND bus_status != 'Deleted' order by bus_title";
						$sqlBusQuery="SELECT * , 'Member' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS." WHERE ". $xtracondition . $ts ." AND bus_meb_id IN (".$id.") AND bus_status != 'Deleted' UNION SELECT * , 'Subscriber' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS_SUB." WHERE ". $xtracondition . $ts ." AND bus_sub_id IN (".$id.")  AND bus_status != 'Deleted' ORDER BY ".$orderBy;
					}
				}
			} else {
				//$sqlBusQuery = "SELECT * FROM ".TBL_MEMBER_BUSINESS." WHERE bus_id!= 0 AND bus_meb_id IN (".$id.") AND bus_status != 'Deleted' order by bus_title";
				$sqlBusQuery="SELECT * , 'Member' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS." WHERE ". $xtracondition ." bus_meb_id IN (".$id.") AND bus_status != 'Deleted' UNION SELECT * , 'Subscriber' AS Cardtype,(SELECT buc_name FROM tbl_business_category WHERE buc_id = bus_buc_id) AS bus_category,(select sum(rat_rates) from tbl_rating WHERE rat_bus_id = bus_id) as ratingCount,(select count(*) from tbl_rating WHERE rat_bus_id = bus_id) as ratingUser FROM ".TBL_MEMBER_BUSINESS_SUB." WHERE ". $xtracondition." bus_sub_id IN (".$id.")  AND bus_status != 'Deleted' ORDER BY ".$orderBy;
			}

			$BusResult  = $db->select($sqlBusQuery);
			$totBus = count($BusResult);

			$sqlSerQuery = "SELECT * FROM ".TBL_MEMBER_SERVICES." WHERE ser_id!= 0 AND ser_meb_id IN (".$id.") AND ser_status != 'Deleted' order by ser_id desc";
			$SerResult  = $db->select($sqlSerQuery);
			$totSer = count($SerResult);			
			
		}
		
		$cate_data = '';
		$cate_data = getBusinessCategoryList('bus_category','bus_category',$dirId);

		$check_con = '';
		$dir_data = '<article><div class="cursor-slide" id="wrapper"><ul id="flip">';
			if($totBus>0) {
				for($j=0;$j<$totBus;$j++) {

					if($BusResult[$j]['Cardtype']=="Member") {
						$uploadFILEWWW = UPLOAD_WWW_BUSINESS_FILE;
					    $categoryName = getCategoryNameByMebId($BusResult[$j]['bus_meb_id']);
					    $categoryName = $BusResult[$j]['bus_category'];
					}
					if($BusResult[$j]['Cardtype']=="Subscriber") {
						$uploadFILEWWW = UPLOAD_WWW_BUSINESS_FILE."Business_Sub/";
		    			$categoryName = $BusResult[$j]['bus_category'];
					}

					//print_r($BusResult[$j]);	

					if($BusResult[$j]['bus_logo_file']!="") {
						$fileNameExt = explode(".", $BusResult[$j]['bus_logo_file']);
						$fileExt 	 = $fileNameExt[count($fileNameExt)-1];
						$allowedImgExts = array("gif","jpg","jpeg","png","GIF","JPG","JPEG","PNG");
						if(in_array($fileExt,$allowedImgExts)) {
							$busFiles = "<div class='bottom-img'><a href='#popupParis".$BusResult[$j]['bus_id']."' data-rel='popup' data-position-to='window' data-transition='fade' ><img class='popphoto' src='".UPLOAD_WWW_BUSINESS_FILE.$BusResult[$j]['bus_id']."/".$BusResult[$j]['bus_logo_file']."' style='float:left;' height='41' width='70' /></a></div>";
							$busFiles .= "<div data-role='popup' id='popupParis".$BusResult[$j]['bus_id']."' data-overlay-theme='b' data-theme='b' data-corners='false'><a href='#' data-rel='back' class='ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right'>Close</a><img class='popphoto' src='".UPLOAD_WWW_BUSINESS_FILE.$BusResult[$j]['bus_id']."/".$BusResult[$j]['bus_logo_file']."' style='max-height:512px;' alt='Paris, France'></div>";

							$busFiles_new = "<img class='popphoto' src='".UPLOAD_WWW_BUSINESS_FILE.$BusResult[$j]['bus_id']."/".$BusResult[$j]['bus_logo_file']."' style='height: 115px; width: 190px;' height='115' width='115' class='popphoto' />";
						} else {
							$uploadFILEWWW = UPLOAD_WWW_BUSINESS_FILE;

							$busFiles = "<div class='bottom-img'><a href='#popupvideo".$BusResult[$j]['bus_id']."' data-rel='popup' data-position-to='window' data-transition='fade' ><img src='images/video-icon.png' style='float:left;' height='41' width='70' /></a></div>";
							$busFiles .= "<div data-role='popup' id='popupvideo".$BusResult[$j]['bus_id']."' data-overlay-theme='b' data-theme='b' data-corners='false'><a href='#' data-rel='back' class='ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right'>Close</a>";
							
							$video = "file=".$uploadFILEWWW.$BusResult[$j]['bus_id']."/".$BusResult[$j]['bus_logo_file']."&;volume=80&amp;bufferlength=5&amp;logo.hide=false&amp;&;title=sleeve_length&amp;linktarget=_self";
							
							$busFiles .= '<object width="100%" height="250" data="player.swf" type="application/x-shockwave-flash"><param value="opaque" name="wmode"><param value="always" name="allowscriptaccess"><param value="all" name="allownetworking"><param value="'.$video.'" name="flashvars"><param name="allowFullScreen" value="true" /></object></div>';

							$busFiles_new = '<object width="100%" height="250" data="player.swf" type="application/x-shockwave-flash"><param value="opaque" name="wmode"><param value="always" name="allowscriptaccess"><param value="all" name="allownetworking"><param value="'.$video.'" name="flashvars"><param name="allowFullScreen" value="true" /></object>';
						}
					} else {
						$busFiles = "<div class='bottom-img'><a href='#' ><img class='popphoto' src='images/photo.jpg' style='float:left;' height='41' width='70' /></a></div>";	
						$busFiles_new = "<img class='popphoto' src='images/photo.jpg' style='height: 115px; width: 190px;' height='115' width='115' class='popphoto' /></a>";	
					}
					$dir_data .= '<li><div class="main-box main_b_card"><div class="home-detail">';

					$dir_data .= '<div data-role="popup" class="popup_main_wrapper" id="about_'.$BusResult[$j]['bus_id'].'" data-overlay-theme="b" data-theme="b" data-corners="pop">
                                <a href="#" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right">Close</a>
                                <div class="popup_inner">
                                    <div class="popup_header">About Us...</div>
                                    <div class="popup_content">
                                        <div class="content_image">'.$busFiles_new.'</div>
                                        <div class="content_title"><h1>'.$BusResult[$j]['bus_company'].'</h1></div>
                                    	<p>'.$BusResult[$j]['bus_about'].'</p>
                                    </div>
                                </div>
                            </div>';

                    $dir_data .= '<div data-role="popup" class="popup_main_wrapper" id="info_'.$BusResult[$j]['bus_id'].'" data-overlay-theme="b" data-theme="b" data-corners="pop">
                                <a href="#" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right">Close</a>
                                <div class="popup_inner">
                                    <div class="popup_header">Information</div>
                                    <div class="popup_content" style="padding: 0;">
                                        <table width="100%" cellspacing="0" cellpadding="0" border="0" class="info-table">
		                                    <tbody>
		                                    <tr>
		                                        <td width="25%" class="blue">COMPANY NAME</td>
		                                        <td width="25%">'.$BusResult[$j]['bus_company'].'</td>
		                                    </tr>
		                                    <tr>
		                                        <td width="25%" class="blue">ADDRESS</td>
		                                        <td width="25%">'.$BusResult[$j]['bus_address1'].'</td>
		                                    </tr>
		                                    <tr>
		                                        <td width="25%" class="blue">CITY</td>
		                                        <td width="25%">'.$BusResult[$j]['bus_city'].'</td>
		                                    </tr>
		                                    <tr>
		                                        <td width="25%" class="blue">STATE</td>
		                                        <td width="25%">'.$BusResult[$j]['StateName'].'</td>
		                                    </tr>
		                                    <tr>
		                                        <td width="25%" class="blue">ZIP CODE</td>
		                                        <td width="25%">'.$BusResult[$j]['bus_zip'].'</td>
		                                    </tr>
		                                    <tr>
		                                        <td width="25%" class="blue">WEBSITE</td>
		                                        <td width="25%"><a href="#">'.$BusResult[$j]['bus_website'].'</a></td>
		                                    </tr>
		                                    <tr>
		                                        <td width="25%" class="blue">OFFICE PHONE</td>
		                                        <td width="25%">'.$BusResult[$j]['bus_phone'].'</td>
		                                    </tr>
		                                    <tr>
		                                        <td width="25%" class="blue">FAX NUMBER</td>
		                                        <td width="25%">'.$BusResult[$j]['bus_fax'].'</td>
		                                    </tr>
		                                    <tr>
		                                        <td width="25%" class="blue">EMAIL ADDRESS</td>
		                                        <td width="25%">'.$BusResult[$j]['bus_email'].'</td>
		                                    </tr>
		                                    <tr>
		                                        <td width="25%" class="blue">FIRST NAME</td>
		                                        <td width="25%">'.$BusResult[$j]['bus_name'].'</td>
		                                    </tr>
		                                    <tr>
		                                        <td width="25%" class="blue">TITLE</td>
		                                        <td width="25%">'.$BusResult[$j]['bus_title'].'</td>
		                                    </tr>
		                                	</tbody>
                                		</table>
                                    </div>
                                </div>
                            </div>';

                    $dir_data .= '<div data-role="popup" class="popup_main_wrapper" id="playlist_'.$BusResult[$j]['bus_id'].'" data-overlay-theme="b" data-theme="b" data-corners="pop">
                                <a href="#" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right">Close</a>
                                <div class="popup_inner">
                                    <div class="popup_header">Play List</div>
                                    <div class="popup_content" style="padding: 0;">
                                        <div class="playlist-table">
                                            <select name="playlist_drop" class="playlist_drop">
                                            <option value="" >Select video</option>';
                                            $videos_list = '';
                                            $counter = 1;
                                            $uploadFILEWWW = UPLOAD_WWW_BUSINESS_FILE;
                                            $bus_id = $BusResult[$j]['bus_id'];

                                            if($BusResult[$j]['bus_video_file_1'] != "") {
                                            	$dir_data .= '<option value="'.$counter.$bus_id.'" >Play # '.$counter.'</option>';
                                            	$videos_list .= '<div style="display:none;" class="video_list_file video_list_file_'.$counter.$bus_id.'"><object height="310" width="100%" type="application/x-shockwave-flash" data="player.swf">
	                                               <param name="wmode" value="opaque">
	                                               <param name="allowscriptaccess" value="always">
	                                               <param name="allownetworking" value="all">
	                                               <param name="flashvars" value="file='.$uploadFILEWWW.$bus_id."/".$BusResult[$j]['bus_video_file_1'].'&amp;;volume=80&amp;bufferlength=5&amp;logo.hide=false&amp;&amp;;title=sleeve_length&amp;linktarget=_self">
	                                               <param value="true" name="allowFullScreen">
	                                            </object></div>';
                                            	$counter = $counter+1;
                                            }
                                            if($BusResult[$j]['bus_video_file_2'] != "") {
                                            	$dir_data .= '<option value="'.$counter.$bus_id.'" >Play # '.$counter.'</option>';
                                            	$videos_list .= '<div style="display:none;" class="video_list_file video_list_file_'.$counter.$bus_id.'"><object height="310" width="100%" type="application/x-shockwave-flash" data="player.swf">
	                                               <param name="wmode" value="opaque">
	                                               <param name="allowscriptaccess" value="always">
	                                               <param name="allownetworking" value="all">
	                                               <param name="flashvars" value="file='.$uploadFILEWWW.$bus_id."/".$BusResult[$j]['bus_video_file_2'].'&amp;;volume=80&amp;bufferlength=5&amp;logo.hide=false&amp;&amp;;title=sleeve_length&amp;linktarget=_self">
	                                               <param value="true" name="allowFullScreen">
	                                            </object></div>';
                                            	$counter = $counter+1;
                                            }
                                            if($BusResult[$j]['bus_video_file_3'] != "") {
                                            	$dir_data .= '<option value="'.$counter.$bus_id.'" >Play # '.$counter.'</option>';
                                            	$videos_list .= '<div style="display:none;" class="video_list_file video_list_file_'.$counter.$bus_id.'"><object height="310" width="100%" type="application/x-shockwave-flash" data="player.swf">
	                                               <param name="wmode" value="opaque">
	                                               <param name="allowscriptaccess" value="always">
	                                               <param name="allownetworking" value="all">
	                                               <param name="flashvars" value="file='.$uploadFILEWWW.$bus_id."/".$BusResult[$j]['bus_video_file_3'].'&amp;;volume=80&amp;bufferlength=5&amp;logo.hide=false&amp;&amp;;title=sleeve_length&amp;linktarget=_self">
	                                               <param value="true" name="allowFullScreen">
	                                            </object></div>';
                                            	$counter = $counter+1;
                                            }
                                            if($BusResult[$j]['bus_video_file_4'] != "") {
                                            	$dir_data .= '<option value="'.$counter.$bus_id.'" >Play # '.$counter.'</option>';
                                            	$videos_list .= '<div style="display:none;" class="video_list_file video_list_file_'.$counter.$bus_id.'"><object height="310" width="100%" type="application/x-shockwave-flash" data="player.swf">
	                                               <param name="wmode" value="opaque">
	                                               <param name="allowscriptaccess" value="always">
	                                               <param name="allownetworking" value="all">
	                                               <param name="flashvars" value="file='.$uploadFILEWWW.$bus_id."/".$BusResult[$j]['bus_video_file_4'].'&amp;;volume=80&amp;bufferlength=5&amp;logo.hide=false&amp;&amp;;title=sleeve_length&amp;linktarget=_self">
	                                               <param value="true" name="allowFullScreen">
	                                            </object></div>';
                                            	$counter = $counter+1;
                                            }
                                            if($BusResult[$j]['bus_video_file_5'] != "") {
                                            	$dir_data .= '<option value="'.$counter.$bus_id.'" >Play # '.$counter.'</option>';
                                            	$videos_list .= '<div style="display:none;" class="video_list_file video_list_file_'.$counter.$bus_id.'"><object height="310" width="100%" type="application/x-shockwave-flash" data="player.swf">
	                                               <param name="wmode" value="opaque">
	                                               <param name="allowscriptaccess" value="always">
	                                               <param name="allownetworking" value="all">
	                                               <param name="flashvars" value="file='.$uploadFILEWWW.$bus_id."/".$BusResult[$j]['bus_video_file_5'].'&amp;;volume=80&amp;bufferlength=5&amp;logo.hide=false&amp;&amp;;title=sleeve_length&amp;linktarget=_self">
	                                               <param value="true" name="allowFullScreen">
	                                            </object></div>';
                                            	$counter = $counter+1;
                                            }
                            $dir_data .=	'</select>
                                        </div>
	                                        <div class="videoh1" style="height: 310px; width:100%">'.$videos_list.'</div>
                                		</div>
                                    </div>
                            	</div>';        	
                            	
					$dir_data .= '<div class="detail-top">
									<ul>
										<li>
                                        	<a href="#about_'.$BusResult[$j]['bus_id'].'" data-rel="popup" data-position-to="window" data-transition="pop">
                                            	<span><img title="" alt="" src="images/about-us.png"></span>about us
                                            </a>
                                        </li>
                                        <li>
                                        	<a href="#info_'.$BusResult[$j]['bus_id'].'" data-rel="popup" data-position-to="window" data-transition="pop">
                                            	<span><img title="" alt="" src="images/infoinformation.png"></span>Info.
                                           	</a>
                                        </li>
                                        <li>
                                        	<a href="#playlist_'.$BusResult[$j]['bus_id'].'" data-rel="popup" data-position-to="window" data-transition="pop">
                                            	<span><img title="" alt="" src="images/play-list.png"></span>Playlist
                                            </a>
                                        </li>
                                    </ul>
                                </div>';

					$dir_data .= '<div class="detail-bottom">';
					$dir_data .= $busFiles;
					//$dir_data .= $BusResult[$j]['bus_phone'].'<br />';
					$dir_data .= '<div class="bottom-content">
									<a href="#" data_bus_id="'.$BusResult[$j]['bus_id'].'" class="detail_card_list_show">
									<p>
										<strong>'.$BusResult[$j]['bus_name']." ".$BusResult[$j]['bus_lname'].'</strong>
										'.$BusResult[$j]['bus_title'].'<strong>'.$BusResult[$j]['bus_company'].'</strong>'.$categoryName.'
									</p>
									</a>
                                  </div>';

                    $ratingDisable = getRatingValue("Card",$BusResult[$j]['bus_id'],$sbfId); 
                    $sumRating =  $BusResult[$j]['ratingCount'];
                    $ratedUsers = $BusResult[$j]['ratingUser'];
                    $sumRating =  round($sumRating / $ratedUsers,2);              
                    
                    //$dir_data .= '<div class="rank-all"> <input id="input-'.$BusResult[$j]['bus_id'].'" busid="'.$BusResult[$j]['bus_id'].'" value="'.$sumRating.'" type="number" class="rating" min=0 max=5 step=1 data-size="sm" '.$ratingDisable.' ></div>';
                    //$dir_data .= '<div class="bottom-star"><img src="images/star-rate.png"></div><div class="clr"></div>';                            
						
					$fun='setdetailmempage(\''.$BusResult[$j]['bus_meb_id'].'\')';					
					
					//$dir_data .= '<div class="bus_title" onclick="'.$fun.'">'.$BusResult[$j]['bus_title'].'</div>';
					$dir_data .= '</div></div></div></li>';
					$categoryName='';
				}
			} else {
				$dir_data .= '<li><div class="main_b_card" style="font-weight: bold;line-height: 160px;text-align: center;"><div class="business_card">No business card available.</div></div></li>';
			}
		$dir_data .= '</ul>';
		$dir_data .= '<div id="scrollbar" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"><a class="ui-slider-handle ui-state-default ui-corner-all" href="#"></a></div>';

		if($totBus!=1) { 
			$no = 100/($totBus-1); 
		} else { 
			$no=1; 
		};

		$services = '';
		
		$services .= '<article><div class="tab_content_holder radius"><div class="tab_content_holder_inner"><div class="border-txt bottom"><div class="serviceTxt">Services</div>'; 
		if($totSer>0) {
			$services .= '<div class="subRow direct card service_box flexslidertext"><ul class="slides">';
				$b=1;
            for($k=0;$k<$totSer;$k++) {
					$services .= '<li><div class="enterTitle light"><b>Title</b><span>:</span>';
					$services .= '<p>'.$SerResult[$k]['ser_title'].'</p></div>';
					$services .= '<div class="enterTitle dark"><b>Description</b><span>:</span>';
					$services .= '<p>'.$SerResult[$k]['ser_description'].'</p></div></li>';
				}
			$services .= '</ul></div>';
		} else {
			$services .= '<div class="no-record">No Records</div>';
		}	
		$services .= '</div></div></div></article>'; 
		
		$pagination = '';
		$pagination .= '<div class="paging"><ul>';			
			for ($i = 65; $i < 91; $i++) {
				$charAct = ($searchchar == chr($i)) ? "class='active'" : "" ;	
				$pagination .= '<li><a '.$charAct.' href="javascript:void(0);" onclick="return getdirectorycard(this.id)" id="'.chr($i).'">'.chr($i).'</a></li>';	
			}
			$otherAct = ($searchchar == 'Other') ? "class='active'" : "" ;
			$allAct = ($searchchar == 'All') ? "class='active'" : "" ;
			$pagination .= '<li><a '.$allAct.' href="javascript:void(0);" onclick="return getdirectorycard(this.id)" id="All">All</a></li>';
			$pagination .= '<li><a '.$otherAct.' href="javascript:void(0);" onclick="return getdirectorycard(this.id)" id="Other">Other</a></li>';
		$pagination .= '</ul></div>';
			
		$dir_data .= '</div></article>';
      $success = array("status" => 'true', 'data' => $dir_data, 'pagination' => $pagination, 'no' => $no, 'services' => $services, 'cate_data' => $cate_data);
		 
   	} else if($action == 'AddNewBusinessCard') {
   		$data = $_REQUEST;	
		$mebId = $_REQUEST['SubcriberId'];
		
		$sql_query_sub = "select * from ".TBL_MEMBER_SUBSCRIBERS." as p where p.sub_id='".$mebId."'";
		$sql_query_sub_data = $db->select($sql_query_sub);
		
		$dir_is='';
		if(count($sql_query_sub_data)>0) {
			$dir_is=$sql_query_sub_data[0]['sub_dir_id'];
		}
		
   		$SECTION_FIELD_PREFIX='bus_';
		$SECTION_AUTO_ID=$_REQUEST[$SECTION_FIELD_PREFIX.'id'];
		$SECTION_WHERE=$SECTION_FIELD_PREFIX."id=".$SECTION_AUTO_ID."";
		$SECTION_VIEW_PAGE=MEB_VIEW_BUSINESS;
		$SECTION_MANAGE_PAGE=MEB_MANAGE_BUSINESS;
		
		$SECTION_NAME='Business';

   		$type = 'Business';
		if($type!="Business") {
			$SECTION_TABLE= TBL_MEMBER_BUSINESS;
			$uploadFILEURL = UPLOAD_DIR_BUSINESS_FILE;
			$uploadFILEWWW = UPLOAD_WWW_BUSINESS_FILE;
		} else {
			$SECTION_TABLE= TBL_MEMBER_BUSINESS_SUB;
			$uploadFILEURL = UPLOAD_DIR_BUSINESS_FILE."Business_Sub/";
			$uploadFILEWWW = UPLOAD_WWW_BUSINESS_FILE."Business_Sub/";
		}

		if($type!="Business") {
  			$add_values[$SECTION_FIELD_PREFIX.'meb_id']	= trim($data["SubcriberId"]);
   		} else {
 			$add_values[$SECTION_FIELD_PREFIX.'sub_id']	= trim($data["SubcriberId"]);
		}

		//$add_values[$SECTION_FIELD_PREFIX.'sub_id'] 		= trim($data["SubcriberID"]);
		$add_values[$SECTION_FIELD_PREFIX.'dir_id'] 		= trim($dir_is);
		
		$add_values[$SECTION_FIELD_PREFIX.'title'] 			= trim($data["bus_title"]);
		$add_values[$SECTION_FIELD_PREFIX.'name'] 			= trim($data["bus_name"]);
  		$add_values[$SECTION_FIELD_PREFIX.'lname'] 			= trim($data["bus_lname"]);
		$add_values[$SECTION_FIELD_PREFIX.'company']		= trim($data["bus_company"]);
  		$add_values[$SECTION_FIELD_PREFIX.'buc_id']			= trim($data["bus_category"]);
		$add_values[$SECTION_FIELD_PREFIX.'phone'] 			= trim($data["bus_phone"]);
		$add_values[$SECTION_FIELD_PREFIX.'zip'] 			= trim($data["bus_zip"]);
		$add_values[$SECTION_FIELD_PREFIX.'address1'] 		= trim($data["bus_address1"]);
		$add_values[$SECTION_FIELD_PREFIX.'city'] 			= trim($data["bus_city"]);
		$add_values[$SECTION_FIELD_PREFIX.'email'] 			= trim($data["bus_email"]);
		$add_values[$SECTION_FIELD_PREFIX.'about'] 			= trim($data["bus_about"]);
		$add_values[$SECTION_FIELD_PREFIX.'website'] 		= trim($data["bus_website"]);
		$add_values[$SECTION_FIELD_PREFIX.'fax'] 			= trim($data["bus_fax"]);
		$add_values[$SECTION_FIELD_PREFIX.'address2'] 		= trim($data["bus_address2"]);
		$add_values[$SECTION_FIELD_PREFIX.'sta_id'] 		= trim($data["sub_state"]);
		$add_values[$SECTION_FIELD_PREFIX.'notes'] 			= trim($data["bus_notes"]);
		
		$add_values[$SECTION_FIELD_PREFIX.'video_title_1'] 	= trim($data["bus_video_title_1"]);
		$add_values[$SECTION_FIELD_PREFIX.'video_title_2'] 	= trim($data["bus_video_title_2"]);
		$add_values[$SECTION_FIELD_PREFIX.'video_title_3'] 	= trim($data["bus_video_title_3"]);
		$add_values[$SECTION_FIELD_PREFIX.'video_title_4'] 	= trim($data["bus_video_title_4"]);
		$add_values[$SECTION_FIELD_PREFIX.'video_title_5'] 	= trim($data["bus_video_title_5"]);
		
		$status = "Active";
		
		$add_values[$SECTION_FIELD_PREFIX.'status'] 		= $status;									
		$add_values[$SECTION_FIELD_PREFIX . 'created_id'] 	= $mebId;
		$add_values[$SECTION_FIELD_PREFIX . 'created_date'] = date("Y-m-d H:i:s");
		$BusId = $db->insertData($SECTION_TABLE, $add_values);

		if(is_dir($uploadFILEURL.$BusId."/") == false) { 			
			mkdir($uploadFILEURL.$BusId."/");
		}

		$bus_logo_file_name = $_FILES["bus_logo_file"]['name'];

		if($bus_logo_file_name!="") {
			$bus_file = uploadIVA($uploadFILEURL.$BusId."/",$_FILES['bus_logo_file']);
			if($bus_file=="type_err") {
				$_SESSION["err_file"]="Please select only image videos and audio file"; 
				$db->delete("delete from ".$SECTION_TABLE." where bus_id=".$BusId);
				echo $success = array("status" => 'false', 'data' => 'Something wrong.');
				exit; 
			} else if($bus_file=="size_err") {
				$db->delete("delete from ".$SECTION_TABLE." where bus_id=".$BusId);
				echo $success = array("status" => 'false', 'data' => 'Please upload file less than 32MB file size!');
				exit; 
			} else {
				$bus_logo_file = $bus_file;
				$updateData[$SECTION_FIELD_PREFIX.'logo_file'] 	= $bus_logo_file;
				$SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$BusId;
				$db->updateData($SECTION_TABLE, $updateData, $SECTION_WHERE);
			}
		}

		$bus_picture_file_name = $_FILES["bus_picture_file"]['name'];

		if($bus_picture_file_name!="") {
			$bus_file = uploadIVA($uploadFILEURL.$BusId."/",$_FILES['bus_picture_file']);
			if($bus_file=="type_err") {
				$_SESSION["err_file"]="Please select only image videos and audio file"; 
				$db->delete("delete from ".$SECTION_TABLE." where bus_id=".$BusId);
				echo $success = array("status" => 'false', 'data' => 'Something wrong.');
				exit; 
			} else if($bus_file=="size_err") {
				$db->delete("delete from ".$SECTION_TABLE." where bus_id=".$BusId);
				echo $success = array("status" => 'false', 'data' => 'Please upload file less than 32MB file size!');
				exit; 
			} else {
				$bus_logo_file = $bus_file;
				$updateData[$SECTION_FIELD_PREFIX.'picture_file'] 	= $bus_logo_file;
				$SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$BusId;
				$db->updateData($SECTION_TABLE, $updateData, $SECTION_WHERE);
			}
		}

		if($_FILES["video_1"]['name']!="") {
			$videoFile1 = $_FILES["video_1"]['name'];
			$ext = pathinfo($videoFile1, PATHINFO_EXTENSION); 	
			$videoFile_1 = uploadVideo($uploadFILEURL.$BusId."/",$_FILES["video_1"]);
			if($videoFile_1 == $videoFile1) {           
				unset($update_values);
		   		$update_values[$SECTION_FIELD_PREFIX.'video_file_1'] = trim($videoFile1);
				$SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$BusId;
				$db->updateData($SECTION_TABLE, $update_values, $SECTION_WHERE);
			}
		}
		if($_FILES["video_2"]['name']!="") {
			$videoFile2 = $_FILES["video_2"]['name'];
			$ext = pathinfo($videoFile2, PATHINFO_EXTENSION); 	
			$videoFile_2 = uploadVideo($uploadFILEURL.$BusId."/",$_FILES["video_2"]);
			if($videoFile_2 == $videoFile2) {           
				unset($update_values);
		   		$update_values[$SECTION_FIELD_PREFIX.'video_file_2'] = trim($videoFile2);
				$SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$BusId;
				$db->updateData($SECTION_TABLE, $update_values, $SECTION_WHERE);
			}
		}
		if($_FILES["video_3"]['name']!="") {
			$videoFile3 = $_FILES["video_3"]['name'];
			$ext = pathinfo($videoFile3, PATHINFO_EXTENSION); 	
			$videoFile_3 = uploadVideo($uploadFILEURL.$BusId."/",$_FILES["video_3"]);
			if($videoFile_3 == $videoFile3) {           
				unset($update_values);
		   		$update_values[$SECTION_FIELD_PREFIX.'video_file_3'] = trim($videoFile3);
				$SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$BusId;
				$db->updateData($SECTION_TABLE, $update_values, $SECTION_WHERE);
			}
		}
		if($_FILES["video_4"]['name']!="") {
			$videoFile4 = $_FILES["video_4"]['name'];
			$ext = pathinfo($videoFile4, PATHINFO_EXTENSION); 	
			$videoFile_4 = uploadVideo($uploadFILEURL.$BusId."/",$_FILES["video_4"]);
			if($videoFile_4 == $videoFile4) { 
				unset($update_values);
		   		$update_values[$SECTION_FIELD_PREFIX.'video_file_4'] = trim($videoFile4);
				$SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$BusId;
				$db->updateData($SECTION_TABLE, $update_values, $SECTION_WHERE);
			}
		}
		if($_FILES["video_5"]['name']!="") {
			$videoFile5 = $_FILES["video_5"]['name'];
			$ext = pathinfo($videoFile5, PATHINFO_EXTENSION); 	
			$videoFile_5 = uploadVideo($uploadFILEURL.$BusId."/",$_FILES["video_5"]);
			if($videoFile_5 == $videoFile5) {           
				unset($update_values);
		   		$update_values[$SECTION_FIELD_PREFIX.'video_file_5'] = trim($videoFile5);
				$SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$BusId;
				$db->updateData($SECTION_TABLE, $update_values, $SECTION_WHERE);
			}
		}

		if($BusId)
			$success = array("status" => 'true', 'data' => 'Successfully created card.', 'bus_id' => $BusId, 'file_url' => $uploadFILEURL.$BusId);
		else
			$success = array("status" => 'false', 'data' => 'Something wrong.');	

	} else if($action == 'EditNewBusinessCard') {
   		$data = $_REQUEST;	
		$mebId = $_REQUEST['SubcriberId'];

		$BusId = $_REQUEST['CardId'];

   		$SECTION_FIELD_PREFIX='bus_';
		$SECTION_AUTO_ID=$_REQUEST[$SECTION_FIELD_PREFIX.'id'];
		$SECTION_WHERE=$SECTION_FIELD_PREFIX."id=".$SECTION_AUTO_ID."";
		$SECTION_VIEW_PAGE=MEB_VIEW_BUSINESS;
		$SECTION_MANAGE_PAGE=MEB_MANAGE_BUSINESS;
		
		$SECTION_NAME='Business';

   		$type = 'Business';
		if($type!="Business") {
			$SECTION_TABLE= TBL_MEMBER_BUSINESS;
			$uploadFILEURL = UPLOAD_DIR_BUSINESS_FILE;
			$uploadFILEWWW = UPLOAD_WWW_BUSINESS_FILE;
		} else {
			$SECTION_TABLE= TBL_MEMBER_BUSINESS_SUB;
			$uploadFILEURL = UPLOAD_DIR_BUSINESS_FILE."Business_Sub/";
			$uploadFILEWWW = UPLOAD_WWW_BUSINESS_FILE."Business_Sub/";
		}

		if($type!="Business") {
  			$add_values[$SECTION_FIELD_PREFIX.'meb_id']	= trim($data["SubcriberId"]);
   		} else {
 			$add_values[$SECTION_FIELD_PREFIX.'sub_id']	= trim($data["SubcriberId"]);
		}

		//$add_values[$SECTION_FIELD_PREFIX.'sub_id'] 		= trim($data["SubcriberID"]);
		


		$add_values[$SECTION_FIELD_PREFIX.'title'] 			= trim($data["bus_title"]);
		$add_values[$SECTION_FIELD_PREFIX.'name'] 			= trim($data["bus_name"]);
  		$add_values[$SECTION_FIELD_PREFIX.'lname'] 			= trim($data["bus_lname"]);
		$add_values[$SECTION_FIELD_PREFIX.'company']		= trim($data["bus_company"]);
  		$add_values[$SECTION_FIELD_PREFIX.'buc_id']			= trim($data["bus_category"]);
		$add_values[$SECTION_FIELD_PREFIX.'phone'] 			= trim($data["bus_phone"]);
		$add_values[$SECTION_FIELD_PREFIX.'zip'] 			= trim($data["bus_zip"]);
		$add_values[$SECTION_FIELD_PREFIX.'address1'] 		= trim($data["bus_address1"]);
		$add_values[$SECTION_FIELD_PREFIX.'city'] 			= trim($data["bus_city"]);
		$add_values[$SECTION_FIELD_PREFIX.'email'] 			= trim($data["bus_email"]);
		$add_values[$SECTION_FIELD_PREFIX.'about'] 			= trim($data["bus_about"]);
		$add_values[$SECTION_FIELD_PREFIX.'website'] 		= trim($data["bus_website"]);
		$add_values[$SECTION_FIELD_PREFIX.'fax'] 			= trim($data["bus_fax"]);
		$add_values[$SECTION_FIELD_PREFIX.'address2'] 		= trim($data["bus_address2"]);
		$add_values[$SECTION_FIELD_PREFIX.'sta_id'] 		= trim($data["sub_state"]);
		$add_values[$SECTION_FIELD_PREFIX.'notes'] 			= trim($data["bus_notes"]);
		
		$add_values[$SECTION_FIELD_PREFIX.'video_title_1'] 	= trim($data["bus_video_title_1"]);
		$add_values[$SECTION_FIELD_PREFIX.'video_title_2'] 	= trim($data["bus_video_title_2"]);
		$add_values[$SECTION_FIELD_PREFIX.'video_title_3'] 	= trim($data["bus_video_title_3"]);
		$add_values[$SECTION_FIELD_PREFIX.'video_title_4'] 	= trim($data["bus_video_title_4"]);
		$add_values[$SECTION_FIELD_PREFIX.'video_title_5'] 	= trim($data["bus_video_title_5"]);
		
		$status = "Active";
		
		$add_values[$SECTION_FIELD_PREFIX.'status'] 		= $status;									
		$add_values[$SECTION_FIELD_PREFIX . 'created_id'] 	= $mebId;
		$add_values[$SECTION_FIELD_PREFIX . 'created_date'] = date("Y-m-d H:i:s");


		$SECTION_WHERE = "bus_id='".$BusId."'";
		//$BusId = $db->insertData($SECTION_TABLE, $add_values);
		$GPDetail_result = $db->updateData($SECTION_TABLE,$add_values,$SECTION_WHERE);

		if(is_dir($uploadFILEURL.$BusId."/") == false) { 			
			mkdir($uploadFILEURL.$BusId."/");
		}

		$bus_logo_file_name = $_FILES["bus_logo_file"]['name'];

		if($bus_logo_file_name!="") {
			$bus_file = uploadIVA($uploadFILEURL.$BusId."/",$_FILES['bus_logo_file']);
			if($bus_file=="type_err") {
				$_SESSION["err_file"]="Please select only image videos and audio file"; 
				$db->delete("delete from ".$SECTION_TABLE." where bus_id=".$BusId);
				echo $success = array("status" => 'false', 'data' => 'Something wrong.');
				exit; 
			} else if($bus_file=="size_err") {
				$db->delete("delete from ".$SECTION_TABLE." where bus_id=".$BusId);
				echo $success = array("status" => 'false', 'data' => 'Please upload file less than 32MB file size!');
				exit; 
			} else {
				$bus_logo_file = $bus_file;
				$updateData[$SECTION_FIELD_PREFIX.'logo_file'] 	= $bus_logo_file;
				$SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$BusId;
				$db->updateData($SECTION_TABLE, $updateData, $SECTION_WHERE);
			}
		}

		$bus_picture_file_name = $_FILES["bus_picture_file"]['name'];

		if($bus_picture_file_name!="") {
			$bus_file = uploadIVA($uploadFILEURL.$BusId."/",$_FILES['bus_picture_file']);
			if($bus_file=="type_err") {
				$_SESSION["err_file"]="Please select only image videos and audio file"; 
				$db->delete("delete from ".$SECTION_TABLE." where bus_id=".$BusId);
				echo $success = array("status" => 'false', 'data' => 'Something wrong.');
				exit; 
			} else if($bus_file=="size_err") {
				$db->delete("delete from ".$SECTION_TABLE." where bus_id=".$BusId);
				echo $success = array("status" => 'false', 'data' => 'Please upload file less than 32MB file size!');
				exit; 
			} else {
				$bus_logo_file = $bus_file;
				$updateData[$SECTION_FIELD_PREFIX.'picture_file'] 	= $bus_logo_file;
				$SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$BusId;
				$db->updateData($SECTION_TABLE, $updateData, $SECTION_WHERE);
			}
		}

		if($_FILES["video_1"]['name']!="") {
			$videoFile1 = $_FILES["video_1"]['name'];
			$ext = pathinfo($videoFile1, PATHINFO_EXTENSION); 	
			$videoFile_1 = uploadVideo($uploadFILEURL.$BusId."/",$_FILES["video_1"]);
			if($videoFile_1 == $videoFile1) {           
				unset($update_values);
		   		$update_values[$SECTION_FIELD_PREFIX.'video_file_1'] = trim($videoFile1);
				$SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$BusId;
				$db->updateData($SECTION_TABLE, $update_values, $SECTION_WHERE);
			}
		}
		if($_FILES["video_2"]['name']!="") {
			$videoFile2 = $_FILES["video_2"]['name'];
			$ext = pathinfo($videoFile2, PATHINFO_EXTENSION); 	
			$videoFile_2 = uploadVideo($uploadFILEURL.$BusId."/",$_FILES["video_2"]);
			if($videoFile_2 == $videoFile2) {           
				unset($update_values);
		   		$update_values[$SECTION_FIELD_PREFIX.'video_file_2'] = trim($videoFile2);
				$SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$BusId;
				$db->updateData($SECTION_TABLE, $update_values, $SECTION_WHERE);
			}
		}
		if($_FILES["video_3"]['name']!="") {
			$videoFile3 = $_FILES["video_3"]['name'];
			$ext = pathinfo($videoFile3, PATHINFO_EXTENSION); 	
			$videoFile_3 = uploadVideo($uploadFILEURL.$BusId."/",$_FILES["video_3"]);
			if($videoFile_3 == $videoFile3) {           
				unset($update_values);
		   		$update_values[$SECTION_FIELD_PREFIX.'video_file_3'] = trim($videoFile3);
				$SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$BusId;
				$db->updateData($SECTION_TABLE, $update_values, $SECTION_WHERE);
			}
		}
		if($_FILES["video_4"]['name']!="") {
			$videoFile4 = $_FILES["video_4"]['name'];
			$ext = pathinfo($videoFile4, PATHINFO_EXTENSION); 	
			$videoFile_4 = uploadVideo($uploadFILEURL.$BusId."/",$_FILES["video_4"]);
			if($videoFile_4 == $videoFile4) { 
				unset($update_values);
		   		$update_values[$SECTION_FIELD_PREFIX.'video_file_4'] = trim($videoFile4);
				$SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$BusId;
				$db->updateData($SECTION_TABLE, $update_values, $SECTION_WHERE);
			}
		}
		if($_FILES["video_5"]['name']!="") {
			$videoFile5 = $_FILES["video_5"]['name'];
			$ext = pathinfo($videoFile5, PATHINFO_EXTENSION); 	
			$videoFile_5 = uploadVideo($uploadFILEURL.$BusId."/",$_FILES["video_5"]);
			if($videoFile_5 == $videoFile5) {           
				unset($update_values);
		   		$update_values[$SECTION_FIELD_PREFIX.'video_file_5'] = trim($videoFile5);
				$SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$BusId;
				$db->updateData($SECTION_TABLE, $update_values, $SECTION_WHERE);
			}
		}

		if($BusId)
			$success = array("status" => 'true', 'data' => 'Successfully updated card.', 'bus_id' => $BusId, 'file_url' => $uploadFILEURL.$BusId);
		else
			$success = array("status" => 'false', 'data' => 'Something wrong.');	

	} else if($action == 'EditNewBusinessCardNew') {
   		$data = $_REQUEST;	
		$mebId = $_REQUEST['SubcriberId'];

		$BusId = $_REQUEST['CardId'];

   		$SECTION_FIELD_PREFIX='bus_';
		$SECTION_AUTO_ID=$_REQUEST[$SECTION_FIELD_PREFIX.'id'];
		$SECTION_WHERE=$SECTION_FIELD_PREFIX."id=".$SECTION_AUTO_ID."";
		$SECTION_VIEW_PAGE=MEB_VIEW_BUSINESS;
		$SECTION_MANAGE_PAGE=MEB_MANAGE_BUSINESS;
		
		$SECTION_NAME='Business';

   		$type = 'Business';
		if($type!="Business") {
			$SECTION_TABLE= TBL_MEMBER_BUSINESS;
			$uploadFILEURL = UPLOAD_DIR_BUSINESS_FILE;
			$uploadFILEWWW = UPLOAD_WWW_BUSINESS_FILE;
		} else {
			$SECTION_TABLE= TBL_MEMBER_BUSINESS_SUB;
			$uploadFILEURL = UPLOAD_DIR_BUSINESS_FILE."Business_Sub/";
			$uploadFILEWWW = UPLOAD_WWW_BUSINESS_FILE."Business_Sub/";
		}

		if($type!="Business") {
  			$add_values[$SECTION_FIELD_PREFIX.'meb_id']	= trim($data["SubcriberId"]);
   		} else {
 			$add_values[$SECTION_FIELD_PREFIX.'sub_id']	= trim($data["SubcriberId"]);
		}

		//$add_values[$SECTION_FIELD_PREFIX.'sub_id'] 		= trim($data["SubcriberID"]);
		


		$add_values[$SECTION_FIELD_PREFIX.'title'] 			= trim($data["bus_title"]);
		$add_values[$SECTION_FIELD_PREFIX.'name'] 			= trim($data["bus_name"]);
  		$add_values[$SECTION_FIELD_PREFIX.'lname'] 			= trim($data["bus_lname"]);
		$add_values[$SECTION_FIELD_PREFIX.'company']		= trim($data["bus_company"]);
  		$add_values[$SECTION_FIELD_PREFIX.'buc_id']			= trim($data["bus_category"]);
		$add_values[$SECTION_FIELD_PREFIX.'phone'] 			= trim($data["bus_phone"]);
		$add_values[$SECTION_FIELD_PREFIX.'zip'] 			= trim($data["bus_zip"]);
		$add_values[$SECTION_FIELD_PREFIX.'address1'] 		= trim($data["bus_address1"]);
		$add_values[$SECTION_FIELD_PREFIX.'city'] 			= trim($data["bus_city"]);
		$add_values[$SECTION_FIELD_PREFIX.'email'] 			= trim($data["bus_email"]);
		$add_values[$SECTION_FIELD_PREFIX.'about'] 			= trim($data["bus_about"]);
		$add_values[$SECTION_FIELD_PREFIX.'website'] 		= trim($data["bus_website"]);
		$add_values[$SECTION_FIELD_PREFIX.'fax'] 			= trim($data["bus_fax"]);
		$add_values[$SECTION_FIELD_PREFIX.'address2'] 		= trim($data["bus_address2"]);
		$add_values[$SECTION_FIELD_PREFIX.'sta_id'] 		= trim($data["sub_state"]);
		$add_values[$SECTION_FIELD_PREFIX.'notes'] 			= trim($data["bus_notes"]);
		
		$add_values[$SECTION_FIELD_PREFIX.'video_title_1'] 	= trim($data["bus_video_title_1"]);
		$add_values[$SECTION_FIELD_PREFIX.'video_title_2'] 	= trim($data["bus_video_title_2"]);
		$add_values[$SECTION_FIELD_PREFIX.'video_title_3'] 	= trim($data["bus_video_title_3"]);
		$add_values[$SECTION_FIELD_PREFIX.'video_title_4'] 	= trim($data["bus_video_title_4"]);
		$add_values[$SECTION_FIELD_PREFIX.'video_title_5'] 	= trim($data["bus_video_title_5"]);
		
		$status = "Active";
		
		$add_values[$SECTION_FIELD_PREFIX.'status'] 		= $status;									
		$add_values[$SECTION_FIELD_PREFIX . 'created_id'] 	= $mebId;
		$add_values[$SECTION_FIELD_PREFIX . 'created_date'] = date("Y-m-d H:i:s");


		$SECTION_WHERE = "bus_id='".$BusId."'";
		//$BusId = $db->insertData($SECTION_TABLE, $add_values);
		$GPDetail_result = $db->updateData($SECTION_TABLE,$add_values,$SECTION_WHERE);

		if(is_dir($uploadFILEURL.$BusId."/") == false) { 			
			mkdir($uploadFILEURL.$BusId."/");
		}

		$bus_logo_file_name = $_FILES["bus_logo_file"]['name'];

		if($bus_logo_file_name!="") {
			$bus_file = uploadIVA($uploadFILEURL.$BusId."/",$_FILES['bus_logo_file']);
			if($bus_file=="type_err") {
				$_SESSION["err_file"]="Please select only image videos and audio file"; 
				$db->delete("delete from ".$SECTION_TABLE." where bus_id=".$BusId);
				echo $success = array("status" => 'false', 'data' => 'Something wrong.');
				exit; 
			} else if($bus_file=="size_err") {
				$db->delete("delete from ".$SECTION_TABLE." where bus_id=".$BusId);
				echo $success = array("status" => 'false', 'data' => 'Please upload file less than 32MB file size!');
				exit; 
			} else {
				$bus_logo_file = $bus_file;
				$updateData[$SECTION_FIELD_PREFIX.'logo_file'] 	= $bus_logo_file;
				$SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$BusId;
				$db->updateData($SECTION_TABLE, $updateData, $SECTION_WHERE);
			}
		}

		$bus_picture_file_name = $_FILES["bus_picture_file"]['name'];

		if($bus_picture_file_name!="") {
			$bus_file = uploadIVA($uploadFILEURL.$BusId."/",$_FILES['bus_picture_file']);
			if($bus_file=="type_err") {
				$_SESSION["err_file"]="Please select only image videos and audio file"; 
				$db->delete("delete from ".$SECTION_TABLE." where bus_id=".$BusId);
				echo $success = array("status" => 'false', 'data' => 'Something wrong.');
				exit; 
			} else if($bus_file=="size_err") {
				$db->delete("delete from ".$SECTION_TABLE." where bus_id=".$BusId);
				echo $success = array("status" => 'false', 'data' => 'Please upload file less than 32MB file size!');
				exit; 
			} else {
				$bus_logo_file = $bus_file;
				$updateData[$SECTION_FIELD_PREFIX.'picture_file'] 	= $bus_logo_file;
				$SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$BusId;
				$db->updateData($SECTION_TABLE, $updateData, $SECTION_WHERE);
			}
		}

		if($_FILES["video_1"]['name']!="") {
			$videoFile1 = $_FILES["video_1"]['name'];
			$ext = pathinfo($videoFile1, PATHINFO_EXTENSION); 	
			$videoFile_1 = uploadVideo($uploadFILEURL.$BusId."/",$_FILES["video_1"]);
			if($videoFile_1 == $videoFile1) {           
				unset($update_values);
		   		$update_values[$SECTION_FIELD_PREFIX.'video_file_1'] = trim($videoFile1);
				$SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$BusId;
				$db->updateData($SECTION_TABLE, $update_values, $SECTION_WHERE);
			}
		}
		if($_FILES["video_2"]['name']!="") {
			$videoFile2 = $_FILES["video_2"]['name'];
			$ext = pathinfo($videoFile2, PATHINFO_EXTENSION); 	
			$videoFile_2 = uploadVideo($uploadFILEURL.$BusId."/",$_FILES["video_2"]);
			if($videoFile_2 == $videoFile2) {           
				unset($update_values);
		   		$update_values[$SECTION_FIELD_PREFIX.'video_file_2'] = trim($videoFile2);
				$SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$BusId;
				$db->updateData($SECTION_TABLE, $update_values, $SECTION_WHERE);
			}
		}
		if($_FILES["video_3"]['name']!="") {
			$videoFile3 = $_FILES["video_3"]['name'];
			$ext = pathinfo($videoFile3, PATHINFO_EXTENSION); 	
			$videoFile_3 = uploadVideo($uploadFILEURL.$BusId."/",$_FILES["video_3"]);
			if($videoFile_3 == $videoFile3) {           
				unset($update_values);
		   		$update_values[$SECTION_FIELD_PREFIX.'video_file_3'] = trim($videoFile3);
				$SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$BusId;
				$db->updateData($SECTION_TABLE, $update_values, $SECTION_WHERE);
			}
		}
		if($_FILES["video_4"]['name']!="") {
			$videoFile4 = $_FILES["video_4"]['name'];
			$ext = pathinfo($videoFile4, PATHINFO_EXTENSION); 	
			$videoFile_4 = uploadVideo($uploadFILEURL.$BusId."/",$_FILES["video_4"]);
			if($videoFile_4 == $videoFile4) { 
				unset($update_values);
		   		$update_values[$SECTION_FIELD_PREFIX.'video_file_4'] = trim($videoFile4);
				$SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$BusId;
				$db->updateData($SECTION_TABLE, $update_values, $SECTION_WHERE);
			}
		}
		if($_FILES["video_5"]['name']!="") {
			$videoFile5 = $_FILES["video_5"]['name'];
			$ext = pathinfo($videoFile5, PATHINFO_EXTENSION); 	
			$videoFile_5 = uploadVideo($uploadFILEURL.$BusId."/",$_FILES["video_5"]);
			if($videoFile_5 == $videoFile5) {           
				unset($update_values);
		   		$update_values[$SECTION_FIELD_PREFIX.'video_file_5'] = trim($videoFile5);
				$SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$BusId;
				$db->updateData($SECTION_TABLE, $update_values, $SECTION_WHERE);
			}
		}

		if($BusId)
			$success = array("status" => 'true', 'data' => 'Successfully updated card.', 'bus_id' => $BusId, 'file_url' => $uploadFILEURL.$BusId);
		else
			$success = array("status" => 'false', 'data' => 'Something wrong.');	

	} else if($action == 'GetStates') {

		//State Listing...
		$state_fields = array("sta_id","sta_name");
		$state_where  = "sta_status = 'Active'";
		$staRes 	  = $db->selectData(TBL_STATE,$state_fields,$state_where,$extra="",2);

		$dir_data = '';

		$dir_data .= '<select name="sub_state" id="sub_state" tabindex="10" alt="Type to search state">
					  <option value="">Select State</option>';
						for($i=0;$i<count($staRes);$i++) {
							if($staRes[$i]['sta_id']==$sub_state) { $select="selected='selected'"; } else { $select=""; }
								$dir_data .= '<option value="'.$staRes[$i]['sta_id'].'" '.$select.' >'.$staRes[$i]['sta_name'].'</option>';
							}
		$dir_data .= '</select>';

		$cate_data = '';
		$cate_data = getBusinessCategoryList('bus_category','bus_category','0');	

		$success = array("status" => 'true', 'data' => $dir_data, 'cate' => $cate_data);								
	} else if($action == 'GetEditForm') {

		//State Listing...
		$CardId = $_REQUEST['CardId'];
		$SubcriberId = $_REQUEST['SubcriberId'];

		$SECTION_TABLE			= 	'tbl_member_business_sub';
		$SECTION_FIELD_PREFIX	=	'bus_';

		$sqlCondition = " AND bus_sub_id=".$SubcriberId;
		$sql_query = "SELECT * FROM ".$SECTION_TABLE." WHERE ".$SECTION_FIELD_PREFIX."id = ".$CardId." AND ".$SECTION_FIELD_PREFIX."status != 'Deleted'";

		$result_query  = $db->select($sql_query);  
		$total_rows = count($result_query);

		if(count($total_rows)>0) {

			$type = 'Business';
			if($type!="Business") {
				$SECTION_TABLE= TBL_MEMBER_BUSINESS;
				$uploadFILEURL = UPLOAD_DIR_BUSINESS_FILE;
				$uploadFILEWWW = UPLOAD_WWW_BUSINESS_FILE;
			} else {
				$SECTION_TABLE= TBL_MEMBER_BUSINESS_SUB;
				$uploadFILEURL = UPLOAD_DIR_BUSINESS_FILE."Business_Sub/";
				$uploadFILEWWW = UPLOAD_WWW_BUSINESS_FILE."Business_Sub/";
			}

			$file_url = $uploadFILEWWW.$CardId."/";

			$state_fields = array("sta_id","sta_name");
			$state_where  = "sta_status = 'Active'";
			$staRes 	  = $db->selectData(TBL_STATE,$state_fields,$state_where,$extra="",2);

			$state_data = '';
			$state_data .= '<select name="sub_state" id="sub_state" tabindex="10" alt="Type to search state">
						  <option value="">Select State</option>';
							for($i=0;$i<count($staRes);$i++) {
								if($staRes[$i]['sta_id']==$result_query[0][$SECTION_FIELD_PREFIX.'sta_id']) { $select="selected='selected'"; } else { $select=""; }
									$state_data .= '<option value="'.$staRes[$i]['sta_id'].'" '.$select.' >'.$staRes[$i]['sta_name'].'</option>';
								}
			$state_data .= '</select>';

			$cate_data = '';
			$cate_data = getBusinessCategoryList('bus_category','bus_category',$result_query[0][$SECTION_FIELD_PREFIX.'buc_id']);

			$dir_data = '';
			$dir_data .= '<form name="subscriber_add_card" enctype="multipart/form-data" id="subscriber_add_card" method="post" class="subscriber_register validation-form-container" onsubmit="return false;">
						<input type="hidden" name="file_for" id="file_for" value="" />
            			<input type="hidden" name="action" id="action" value="EditNewBusinessCard" />
            			<input type="hidden" name="SubcriberId" id="SubcriberId" value="" />
            			<input type="hidden" name="SubcriberEmail" id="SubcriberEmail" value="" />
            			<input type="hidden" name="DirectoryDetailId" id="DirectoryDetailId" value="" />
            			<input type="hidden" name="AddBusinessCardId" id="AddBusinessCardId" value="" />
            			<input type="hidden" name="CardId" id="CardId" value="" />
            			<div class="success success_msg new_success_msg" style="display: none;">
		            		<span></span>
		            	</div>
            			<div data-role="tabs" id="tabs">
							<div data-role="navbar">
						    	<ul>
							      	<li><a href="#one" data-ajax="false">Information</a></li>
							      	<li><a href="#two" data-ajax="false">Other</a></li>
						    	</ul>
						  	</div>
				  			<div id="one" class="ui-body-d ui-contents">
								<div class="success success_msg" style="display: none;">
		            			<span></span>
		            			</div>
		            			<div class="partation">
			            			<div class="resister_form_field field">
			            				<div class="ui left labeled input">
				            				<input type="text" name="bus_title" data-validation="[NOTEMPTY]" value="'.$result_query[0][$SECTION_FIELD_PREFIX.'title'].'" data_value="'.$result_query[0][$SECTION_FIELD_PREFIX.'title'].'" id="bus_title" placeholder="Title" />
				            			</div>
			            			</div>
			            			
			            			<div class="resister_form_field field">
			            				<div class="ui left labeled input">
				            				<input type="text" name="bus_name" data-validation="[NOTEMPTY]" value="'.$result_query[0][$SECTION_FIELD_PREFIX.'name'].'" data_value="'.$result_query[0][$SECTION_FIELD_PREFIX.'name'].'" id="bus_name" placeholder="First Name" />
				            			</div>
			            			</div>
		            			
			            			<div class="resister_form_field field">
			            				<div class="ui left labeled input">
				            				<input type="text" name="bus_lname" id="bus_lname" data-validation="[NOTEMPTY]" value="'.$result_query[0][$SECTION_FIELD_PREFIX.'lname'].'" data_value="'.$result_query[0][$SECTION_FIELD_PREFIX.'lname'].'" placeholder="Last Name" />
				            			</div>
			            			</div>
			            			
			            			<div class="resister_form_field field">
			            				<div class="ui left labeled input">
				            				<input type="text" name="bus_company" data-validation="[NOTEMPTY]" id="bus_company" value="'.$result_query[0][$SECTION_FIELD_PREFIX.'company'].'" data_value="'.$result_query[0][$SECTION_FIELD_PREFIX.'company'].'" placeholder="Company" />
				            			</div>
			            			</div>
		            			
			            			<div class="resister_form_field field">
			            				<div class="ui left labeled input">
			            					<div class="ui left labeled input cate_data_drop_down">'.$cate_data.'</div>
				            			</div>
			            			</div>
			            			
			            			<div class="resister_form_field field">
			            				<div class="ui left labeled input">
				            				<input type="text" name="bus_address1" id="bus_address1" data-validation="[NOTEMPTY]" value="'.$result_query[0][$SECTION_FIELD_PREFIX.'address1'].'" data_value="'.$result_query[0][$SECTION_FIELD_PREFIX.'address1'].'" placeholder="Address1" />
				            			</div>
			            			</div>
			            			
			            			<div class="resister_form_field field">
			            				<div class="ui left labeled input">
				            				<input type="text" name="bus_address2" id="bus_address2" value="'.$result_query[0][$SECTION_FIELD_PREFIX.'address2'].'" data_value="'.$result_query[0][$SECTION_FIELD_PREFIX.'address2'].'" placeholder="Address2" />
				            			</div>
			            			</div>
			            			
			            			<div class="resister_form_field field">
			            				<div class="ui left labeled input">
				            				<input type="text" name="bus_city" id="bus_city" data-validation="[NOTEMPTY]" value="'.$result_query[0][$SECTION_FIELD_PREFIX.'city'].'" data_value="'.$result_query[0][$SECTION_FIELD_PREFIX.'city'].'" placeholder="City" />
				            			</div>
			            			</div>
			            			
			            			<div class="resister_form_field field">
			            				<div class="ui left labeled input">
				            				<div class="ui left labeled input state_data_drop_down">'.$state_data.'</div>
				            			</div>
			            			</div>
			            			
			            			<div class="resister_form_field field">
			            				<div class="ui left labeled input">
				            				<input type="tel" name="bus_phone" id="bus_phone" data-validation="[NOTEMPTY]" value="'.$result_query[0][$SECTION_FIELD_PREFIX.'phone'].'" data_value="'.$result_query[0][$SECTION_FIELD_PREFIX.'phone'].'" placeholder="Phone Number" />
				            			</div>
			            			</div>
			            			
			            			<div class="resister_form_field field">
			            				<div class="ui left labeled input">
				            				<input type="text" name="bus_zip" id="bus_zip" data-validation="[NOTEMPTY]" value="'.$result_query[0][$SECTION_FIELD_PREFIX.'zip'].'" data_value="'.$result_query[0][$SECTION_FIELD_PREFIX.'zip'].'" placeholder="Zip Code" />
				            			</div>
			            			</div>
			            			
			            			<div class="resister_form_field field">
			            				<div class="ui left labeled input">
				            				<input type="tel" name="bus_fax" id="bus_fax" data-validation="[NOTEMPTY]" value="'.$result_query[0][$SECTION_FIELD_PREFIX.'fax'].'" data_value="'.$result_query[0][$SECTION_FIELD_PREFIX.'fax'].'" placeholder="Fax" />
				            			</div>
			            			</div>
			            			
			            			<div class="resister_form_field field">
			            				<div class="ui left labeled input">
				            				<input type="text" name="bus_email" id="bus_email" data-validation="[EMAIL]" value="'.$result_query[0][$SECTION_FIELD_PREFIX.'email'].'" data_value="'.$result_query[0][$SECTION_FIELD_PREFIX.'email'].'" placeholder="Email" />
				            			</div>
			            			</div>
			            			
			            			<div class="resister_form_field field">
			            				<div class="ui left labeled input">
				            				<input type="text" name="bus_website" id="bus_website" data-validation="[NOTEMPTY]" value="'.$result_query[0][$SECTION_FIELD_PREFIX.'website'].'" data_value="'.$result_query[0][$SECTION_FIELD_PREFIX.'website'].'" placeholder="Website" />
				            			</div>
			            			</div>
			            		</div>    
			            		<div class="resister_form_field reg-btn">
			            			<input type="button" name="next_reg" id="next_reg" onclick="return next_funct()" class="submit_reg" value="Next" />
			            		</div> 
						  </div>
						  <div id="two" class="ui-body-d ui-contents">
						    	<div class="success success_msg" style="display: none;">
		            			<span></span>
		            			</div>
		            			
		            			<div class="partation">
		            				<div class="resister_form_field field">
			            				<div class="ui left labeled input">
				            				<textarea name="bus_about" id="bus_about" rows="" cols="" data-validation="[NOTEMPTY]" placeholder="About" data_value="'.$result_query[0][$SECTION_FIELD_PREFIX.'about'].'">'.$result_query[0][$SECTION_FIELD_PREFIX.'about'].'</textarea>
				            			</div>
			            			</div>
			            			<div class="resister_form_field field">
			            				<div class="ui left labeled input">
				            				<textarea name="bus_notes" id="bus_notes" rows="" cols="" placeholder="Notes" data_value="'.$result_query[0][$SECTION_FIELD_PREFIX.'notes'].'">'.$result_query[0][$SECTION_FIELD_PREFIX.'notes'].'</textarea>
				            			</div>
			            			</div>
		            			</div>
		            			<div class="partation">
		            				<h1 class="add_bus_h1">Media</h1>
			            			<div class="resister_form_field field">
			            				<div class="ui left labeled input">
				            				<button class="browse_btn" onclick="selectVideo(\'video_1_file\');" name="video_1" id="video_1"><img src="images/browse.png" /></button>
				            				<button class="browse_btn tack_pic" onclick="takeVideo(\'video_1_file\');" name="video_1_t" id="video_1_t"><img src="images/video_image.png" /></button>';
				            		$delete3 = '';		
				            		if($result_query[0][$SECTION_FIELD_PREFIX.'video_file_1'] != '') {
				            			$dir_data .= '<img class="video_file_img_con" data_edit="no" data_url="'.$file_url.$result_query[0][$SECTION_FIELD_PREFIX.'video_file_1'].'" data_db_id="video_file_1" onclick="play_video(\'video_1_file\');" style="width: 120px;" id="video_1_file" src="images/video-image.png" />';
				            		} else {
				            			$dir_data .= '<img class="video_file_img_con" data_edit="yes" data_url="" data_db_id="video_file_1" onclick="play_video(\'video_1_file\');" style="width: 120px; visibility: hidden; display: none;" id="video_1_file" src="images/video-image.png" />';
				            			$delete3 = 'display: none;';
				            		}	
				        $dir_data .= '<img src="images/archive.png" style="'.$delete3.'" onclick="return delete_file_video(\'video_1_file\');" class="delet_image_class" id="" /></div>
			            			</div>
			            			<div class="resister_form_field field">
			            				<div class="ui left labeled input">
				            				<button class="browse_btn" onclick="selectVideo(\'video_2_file\');" name="video_2" id="video_2"><img src="images/browse.png" /></button>
				            				<button class="browse_btn tack_pic" onclick="takeVideo(\'video_2_file\');" name="video_2_t" id="video_2_t"><img src="images/video_image.png" /></button>';
				            		$delete4 = '';
				            		if($result_query[0][$SECTION_FIELD_PREFIX.'video_file_2'] != '') {
				            			$dir_data .= '<img class="video_file_img_con" data_edit="no" data_url="'.$file_url.$result_query[0][$SECTION_FIELD_PREFIX.'video_file_2'].'" data_db_id="video_file_2" onclick="play_video(\'video_2_file\');" style="width: 120px;" id="video_2_file" src="images/video-image.png" />';
				            		} else {
				            			$dir_data .= '<img class="video_file_img_con" data_edit="yes" data_url="" data_db_id="video_file_2" onclick="play_video(\'video_2_file\');" style="width: 120px; visibility: hidden; display: none;" id="video_2_file" src="images/video-image.png" />';
				            			$delete4 = 'display: none;';
				            		}	
				              
				     	$dir_data .= '<img src="images/archive.png" style="'.$delete4.'" onclick="return delete_file_video(\'video_2_file\');" class="delet_image_class" id="" /></div>
			            			</div>
			            			<div class="resister_form_field field">
			            				<div class="ui left labeled input">
				            				<button class="browse_btn" onclick="selectVideo(\'video_3_file\');" name="video_3" id="video_3"><img src="images/browse.png" /></button>
				            				<button class="browse_btn tack_pic" onclick="takeVideo(\'video_3_file\');" name="video_3_t" id="video_3_t"><img src="images/video_image.png" /></button>';
				            		$delete5 = '';		
				            		if($result_query[0][$SECTION_FIELD_PREFIX.'video_file_3'] != '') {
				            			$dir_data .= '<img class="video_file_img_con" data_edit="no" data_url="'.$file_url.$result_query[0][$SECTION_FIELD_PREFIX.'video_file_3'].'" data_db_id="video_file_3" onclick="play_video(\'video_3_file\');" style="width: 120px;" id="video_3_file" src="images/video-image.png" />';
				            		} else {
				            			$dir_data .= '<img class="video_file_img_con" data_edit="yes" data_url="" data_db_id="video_file_3" onclick="play_video(\'video_3_file\');" style="width: 120px; visibility: hidden; display: none;" id="video_3_file" src="images/video-image.png" />';
				            			$delete5 = 'display: none;';
				            		}	
				        $dir_data .= '<img src="images/archive.png" style="'.$delete5.'" onclick="return delete_file_video(\'video_3_file\');" class="delet_image_class" id="" /></div>
			            			</div>
			            			<div class="resister_form_field field">
			            				<div class="ui left labeled input">
				            				<button class="browse_btn" onclick="selectVideo(\'video_4_file\');" name="video_4" id="video_4"><img src="images/browse.png" /></button>
				            				<button class="browse_btn tack_pic" onclick="takeVideo(\'video_4_file\');" name="video_4_t" id="video_4_t"><img src="images/video_image.png" /></button>';
				            		$delete6 = '';
				            		if($result_query[0][$SECTION_FIELD_PREFIX.'video_file_4'] != '') {
				            			$dir_data .= '<img class="video_file_img_con" data_edit="no" data_url="'.$file_url.$result_query[0][$SECTION_FIELD_PREFIX.'video_file_4'].'" data_db_id="video_file_4" onclick="play_video(\'video_4_file\');" style="width: 120px;" id="video_4_file" src="images/video-image.png" />';
				            		} else {
				            			$dir_data .= '<img class="video_file_img_con" data_edit="yes" data_url="" data_db_id="video_file_4" onclick="play_video(\'video_4_file\');" style="width: 120px; visibility: hidden; display: none;" id="video_4_file" src="images/video-image.png" />';
				            			$delete6 = 'display: none;';
				            		}	
				        $dir_data .= '<img src="images/archive.png" style="'.$delete6.'" onclick="return delete_file_video(\'video_4_file\');" class="delet_image_class" id="" /></div>
			            			</div>
			            			<div class="resister_form_field field">
			            				<div class="ui left labeled input">
				            				<button class="browse_btn" onclick="selectVideo(\'video_5_file\');" name="video_5" id="video_5"><img src="images/browse.png" /></button>
				            				<button class="browse_btn tack_pic" onclick="takeVideo(\'video_5_file\');" name="video_5_t" id="video_5_t"><img src="images/video_image.png" /></button>';
				            		$delete2 = '';
				            		if($result_query[0][$SECTION_FIELD_PREFIX.'video_file_5'] != '') {
				            			$dir_data .= '<img class="video_file_img_con" data_edit="no" data_url="'.$file_url.$result_query[0][$SECTION_FIELD_PREFIX.'video_file_5'].'" data_db_id="video_file_5" onclick="play_video(\'video_5_file\');" style="width: 120px;" id="video_5_file" src="images/video-image.png" />';
				            		} else {
				            			$dir_data .= '<img class="video_file_img_con" data_edit="yes" data_url="" data_db_id="video_file_5" onclick="play_video(\'video_5_file\');" style="width: 120px; visibility: hidden; display: none;" id="video_5_file" src="images/video-image.png" />';
				            			$delete2 = 'display: none;';
				            		}	
				        $dir_data .= '<img src="images/archive.png" style="'.$delete2.'" onclick="return delete_file_video(\'video_5_file\');" class="delet_image_class" id="" /></div>
			            			</div>
		            			</div>
		            			<div class="partation">
		            				<h1 class="add_bus_h1">Logo File</h1>
			            			<div class="resister_form_field field">
			            				<div class="ui left labeled input">
											<button class="browse_btn" onclick="selectPicture(\'bus_logo_image\');" name="bus_logo_file" id="bus_logo_file"><img src="images/browse.png" /></button>
											<button class="browse_btn tack_pic" onclick="takePicture(\'bus_logo_image\');" name="bus_logo_file" id="bus_logo_file"><img src="images/camera.png" /></button>';
									$delete1 = '';		
				            		if($result_query[0][$SECTION_FIELD_PREFIX.'logo_file'] != '') {
				            			$dir_data .= '<img data_edit="no" style="width: 120px;" id="bus_logo_image" src="'.$file_url.$result_query[0][$SECTION_FIELD_PREFIX.'logo_file'].'" />';
				            		} else {
				            			$dir_data .= '<img data_edit="yes" style="width: 120px; visibility: hidden; display: none;" id="bus_logo_image" src="" />';
				            			$delete1 = 'display: none;';
				            		}	
				        $dir_data .= '<img src="images/archive.png" style="'.$delete1.'" onclick="return delete_file(\'bus_logo_image\');" class="delet_image_class" id="" /></div>
			            			</div>
			            		</div>
			            		<div class="partation">
		            				<h1 class="add_bus_h1">Picture File</h1>
			            			<div class="resister_form_field field">
			            				<div class="ui left labeled input">
											<button class="browse_btn" onclick="selectPicture(\'bus_picture_image\');" name="bus_picture_file" id="bus_picture_file"><img src="images/browse.png" /></button>
											<button class="browse_btn tack_pic" onclick="takePicture(\'bus_picture_image\');" name="bus_picture_file" id="bus_picture_file"><img src="images/camera.png" /></button>';
									$delete = '';		
									if($result_query[0][$SECTION_FIELD_PREFIX.'picture_file'] != '') {
										$dir_data .= '<img data_edit="no" style="width: 120px;" id="bus_picture_image" src="'.$file_url.$result_query[0][$SECTION_FIELD_PREFIX.'picture_file'].'" />';
				            		} else {
				            			$dir_data .= '<img data_edit="yes" style="width: 120px; visibility: hidden; display: none;" id="bus_picture_image" src="" />';
				            			$delete = 'display: none;';
				            		}	
				        $dir_data .= '<img src="images/archive.png" style="'.$delete.'" onclick="return delete_file(\'bus_picture_image\');" class="delet_image_class" id="" /></div>
			            			</div>
			            		</div>
			            		<div class="resister_form_field reg-btn">
			            			<input type="submit" name="submit_reg" id="submit_reg" class="submit_reg" value="SAVE" />
			            		</div>
			            	</div>
						</div>
					</form>';
		} else {
			$dir_data .= '<form name="subscriber_add_card" enctype="multipart/form-data" id="subscriber_add_card" method="post" class="subscriber_register validation-form-container" onsubmit="return false;">
            			<div class="partation">No record found.</div>
					</form>';
		}
			
		$success = array("status" => 'true', 'data' => $dir_data);	
		
	} else if($action == 'GetEditFormNew') {

		//State Listing...
		$CardId = $_REQUEST['CardId'];
		$SubcriberId = $_REQUEST['SubcriberId'];

		$SECTION_TABLE			= 	'tbl_member_business_sub';
		$SECTION_FIELD_PREFIX	=	'bus_';

		$sqlCondition = " AND bus_sub_id=".$SubcriberId;
		$sql_query = "SELECT * FROM ".$SECTION_TABLE." WHERE ".$SECTION_FIELD_PREFIX."id = ".$CardId." AND ".$SECTION_FIELD_PREFIX."status != 'Deleted'";

		$result_query  = $db->select($sql_query);  
		$total_rows = count($result_query);

		if(count($total_rows)>0) {

			$type = 'Business';
			if($type!="Business") {
				$SECTION_TABLE= TBL_MEMBER_BUSINESS;
				$uploadFILEURL = UPLOAD_DIR_BUSINESS_FILE;
				$uploadFILEWWW = UPLOAD_WWW_BUSINESS_FILE;
			} else {
				$SECTION_TABLE= TBL_MEMBER_BUSINESS_SUB;
				$uploadFILEURL = UPLOAD_DIR_BUSINESS_FILE."Business_Sub/";
				$uploadFILEWWW = UPLOAD_WWW_BUSINESS_FILE."Business_Sub/";
			}

			$file_url = $uploadFILEWWW.$CardId."/";

			$state_fields = array("sta_id","sta_name");
			$state_where  = "sta_status = 'Active'";
			$staRes 	  = $db->selectData(TBL_STATE,$state_fields,$state_where,$extra="",2);

			$state_data = '';
			$state_data .= '<select name="sub_state" id="sub_state" tabindex="10" alt="Type to search state">
						  <option value="">Select State</option>';
							for($i=0;$i<count($staRes);$i++) {
								if($staRes[$i]['sta_id']==$result_query[0][$SECTION_FIELD_PREFIX.'sta_id']) { $select="selected='selected'"; } else { $select=""; }
									$state_data .= '<option value="'.$staRes[$i]['sta_id'].'" '.$select.' >'.$staRes[$i]['sta_name'].'</option>';
								}
			$state_data .= '</select>';

			$cate_data = '';
			$cate_data = getBusinessCategoryList('bus_category','bus_category',$result_query[0][$SECTION_FIELD_PREFIX.'buc_id']);

			$dir_data = '';
			
			$video1='';
			$video2='';
			$video3='';
			$video4='';
			$video5='';
			
			$picture='';
			$logoimage='';
			
			if($result_query[0][$SECTION_FIELD_PREFIX.'video_file_1'] != '') {
				$video1=$file_url.$result_query[0][$SECTION_FIELD_PREFIX.'video_file_1'];
			}
			if($result_query[0][$SECTION_FIELD_PREFIX.'video_file_2'] != '') {
				$video2=$file_url.$result_query[0][$SECTION_FIELD_PREFIX.'video_file_2'];
			}
			if($result_query[0][$SECTION_FIELD_PREFIX.'video_file_3'] != '') {
				$video3=$file_url.$result_query[0][$SECTION_FIELD_PREFIX.'video_file_3'];
			}
			if($result_query[0][$SECTION_FIELD_PREFIX.'video_file_4'] != '') {
				$video4=$file_url.$result_query[0][$SECTION_FIELD_PREFIX.'video_file_4'];
			}
			if($result_query[0][$SECTION_FIELD_PREFIX.'video_file_5'] != '') {
				$video5=$file_url.$result_query[0][$SECTION_FIELD_PREFIX.'video_file_5'];
			}
			
			if($result_query[0][$SECTION_FIELD_PREFIX.'logo_file'] != '') {
				$logoimage=$file_url.$result_query[0][$SECTION_FIELD_PREFIX.'logo_file'];
			}
			
			if($result_query[0][$SECTION_FIELD_PREFIX.'picture_file'] != '') {
				$picture=$file_url.$result_query[0][$SECTION_FIELD_PREFIX.'picture_file'];
			}
			
			
		} else {
			$dir_data .= '<form name="subscriber_add_card" enctype="multipart/form-data" id="subscriber_add_card" method="post" class="subscriber_register validation-form-container" onsubmit="return false;">
            			<div class="partation">No record found.</div>
					</form>';
		}
			
		$success = array("status" => 'true', 'data' => $result_query[0], 'state_data' => $state_data, 'cate_data' => $cate_data, 'video_1'=>$video1, 'video_2'=>$video2, 'video_3'=>$video3, 'video_4'=>$video4, 'video_5'=>$video5, 'logo_pic'=>$logoimage, 'profile_pic'=>$picture);								
	} else if($_REQUEST['action'] == 'AddCardPhoto') {
		// Directory where uploaded images are saved
		//$dirname = UPLOAD_DIR_BUSINESS_FILE;

		$dirname 	= $_REQUEST['url'];
		$BusId 		= $_REQUEST['bus_id'];
		$for 		= $_REQUEST['for'];

		if($dirname != '') {
			if(is_dir($dirname."/") == false) { 			
				mkdir($dirname."/");
			}
			//$dirname = "/tmp/phonegap/uploads"; 
			// If uploading file
			if ($_FILES) {    
				print_r($_FILES);    
				mkdir ($dirname, 0777, true);

                $random = random_string_new(10);
                $file_name = clean($_FILES["file"]["name"]);


                //$file_type = $_FILES["file"]["type"];
                $file_type = substr(strrchr($file_name,'.'),1);

                /*if ((($_FILES["file"]["type"] == "video/mov")
                    || ($_FILES["file"]["type"] == "video/mp4")
                    || ($_FILES["file"]["type"] == "video/3gp")
                    || ($_FILES["file"]["type"] == "video/ogg")
                    || ($_FILES["file"]["type"] == "image/gif")
                    || ($_FILES["file"]["type"] == "image/jpeg")
                    || ($_FILES["file"]["type"] == "image/png"))*/

                //if($file_type=='video/mov') {
                if($file_type=='.mov' || $file_type=='mov' || $file_type=='.MOV' || $file_type=='MOV') {
                   $file_name = $random.'.'.$file_type;
                }

				move_uploaded_file($_FILES["file"]["tmp_name"],$dirname."/".$file_name);

				$SECTION_FIELD_PREFIX='bus_';
				$SECTION_TABLE= TBL_MEMBER_BUSINESS_SUB;
				
				$bus_logo_file = $file_name;//$_FILES["file"]["name"];
				$updateData[$SECTION_FIELD_PREFIX.$for] = $bus_logo_file;
				$SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$BusId;
				$db->updateData($SECTION_TABLE, $updateData, $SECTION_WHERE);
			}
		}
		$success = array("status" => 'true', 'data'=>'tset');
	} else if($action == 'DeleteCardPhoto') {
		// Directory where uploaded images are saved
		//$dirname = UPLOAD_DIR_BUSINESS_FILE;

		$dirname 	= $_REQUEST['url'];
		$BusId 		= $_REQUEST['bus_id'];
		$for 		= $_REQUEST['for'];

		if ($BusId != '') {    
			$SECTION_FIELD_PREFIX='bus_';
			$SECTION_TABLE= TBL_MEMBER_BUSINESS_SUB;
				
			$bus_logo_file = '';
			$updateData[$SECTION_FIELD_PREFIX.$for] = $bus_logo_file;
			$SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$BusId;
			$db->updateData($SECTION_TABLE, $updateData, $SECTION_WHERE);
		}
		$success = array("status" => 'true', 'data'=>'tset');
	} else if($action == 'DeleteMyCards') {
		// Directory where uploaded images are saved
		//$dirname = UPLOAD_DIR_BUSINESS_FILE;

		$cardid 		= $_REQUEST['cardid'];
		$SubcriberID 	= $_REQUEST['SubcriberID'];
		
		if ($cardid != '') {    
			
			$SECTION_TABLE			= 	'tbl_member_business_sub';
			$SECTION_FIELD_PREFIX	=	'bus_';

			$sql_query = "SELECT * FROM ".$SECTION_TABLE." WHERE ".$SECTION_FIELD_PREFIX."id = ".$cardid." AND ".$SECTION_FIELD_PREFIX."status != 'Deleted'";

			$result_query  = $db->select($sql_query);  
			$total_rows = count($result_query);

			if(count($total_rows)>0) {
				$updateData[$SECTION_FIELD_PREFIX.'status'] = 'Deleted';
				$SECTION_WHERE = $SECTION_FIELD_PREFIX."id = ".$cardid;
				$db->updateData($SECTION_TABLE, $updateData, $SECTION_WHERE);
			}
			$success = array("status" => 'true', 'data'=>'tset');	

		} else {
			$success = array("status" => 'false', 'data'=>'tset');	
		}
	} else {
		$success = array("status" => 'false', 'data' => $_REQUEST['action']);
	}
	echo json_encode($success);
	exit();
?>	