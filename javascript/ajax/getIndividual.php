<?php 
	include_once('../../include/includeclass.php');
	$action_type			=	$_POST['action_type'];
	$SECTION_TABLE 			= 	$_POST['tableName'];
	$SECTION_FIELD_PREFIX	=	$_POST['fieldPrefix'];
	$SECTION_AUTO_ID 		=	$_POST['autoID'];
	$SECTION_WHERE			=	$SECTION_FIELD_PREFIX."id='".$SECTION_AUTO_ID."'";
	$xtraCondition			=	stripslashes($_POST['xtraCondition']);
	$SECTION_NAME			=	$_POST['displayName'];	
	$language_prefix		=	$_POST['language_prefix'];
	$SECTION_MANAGE_PAGE 	=   ADM_MANAGE_INDIVIDUAL;
	$searchchar 			= 	$_POST['searchchar'];

	$total_language = count($result_language);
	
	if($action_type	==	"delete")
	{	
		$update_values[$SECTION_FIELD_PREFIX.'status']      = "Deleted";
		$update_values[$SECTION_FIELD_PREFIX . 'updated_id'] 	= getAdminSessionId();
        $update_values[$SECTION_FIELD_PREFIX . 'updated_date'] 	= date("Y-m-d H:i:s");
		$GPDetail_result = $db->updateData($SECTION_TABLE,$update_values,$SECTION_WHERE);
		//$_SESSION['msg']  =   "Subscribers has been deleted successfully.";
	}
	else if($action_type	==	"sorting")
	{
		$orderby	=	$_POST['orderby'];
		$order		=	$_POST['order'];
		if($order == "asc" || $order == "")
			$ORDER =	"desc";
		else
			$ORDER =	"asc";
	}

	if($orderby == "")
	{
		$ORDER =	"desc";
		$orderby = "id";
	}
	
	##################################  General Query #######################################
	if ($searchchar != 'undefined' && $searchchar != "") {
    	if ($searchchar == 'other') {
    	    $sql_query = "SELECT p.* FROM " . $SECTION_TABLE . " p WHERE p." . $SECTION_FIELD_PREFIX . "id != 0   AND  p.".$SECTION_FIELD_PREFIX."status!='Deleted' AND `inv_email` REGEXP '^[^a-zA-Z]'   order by p." . $SECTION_FIELD_PREFIX . $orderby . " " . $ORDER;
    	} else {
        	if ($searchchar == 'all') {
				$sql_query = "SELECT p.* FROM " . $SECTION_TABLE . " p WHERE p." . $SECTION_FIELD_PREFIX . "id != 0  AND p.".$SECTION_FIELD_PREFIX."status!='Deleted' order by p.".$SECTION_FIELD_PREFIX . $orderby ." ". $ORDER;
        	} else {
        	    $ts = $searchchar . "%";
        	    $sql_query = "select * from ".$SECTION_TABLE." as p where p.inv_email LIKE '" . $ts . "' AND p.".$SECTION_FIELD_PREFIX."status!='Deleted' order by " . $SECTION_FIELD_PREFIX . $orderby . " " . $ORDER;
        	}
    	}
	}
	else if ($xtraCondition != 'undefined' && $xtraCondition != "") {
		$ts = "%" . $xtraCondition . "%";
		$sql_query = "select * from ".$SECTION_TABLE." as p where (".$xtraCondition.") AND (p.".$SECTION_FIELD_PREFIX."status!='Deleted') order by " . $SECTION_FIELD_PREFIX . $orderby . " " . $ORDER;
	}
	else
	{
		$sql_query = "SELECT p.* FROM ".$SECTION_TABLE." p WHERE p.".$SECTION_FIELD_PREFIX."id != 0 AND p.".$SECTION_FIELD_PREFIX."status != 'Deleted' order by p.".$SECTION_FIELD_PREFIX.$orderby." ".$ORDER;
	}

    //echo $sql_query;
	$excel_query = "SELECT p.* FROM ".$SECTION_TABLE." p WHERE p.".$SECTION_FIELD_PREFIX."id != 0 AND p.".$SECTION_FIELD_PREFIX."status != 'Deleted' order by p.".$SECTION_FIELD_PREFIX.$orderby." ".$ORDER;
	$excel_result  = $db->select($excel_query);	
	$excelcount = count($excel_result);	
		
		#################################  Paging Query + Paging Code ##################################
		$paging_query = $sql_query;
		$paging_result  = $db->select($paging_query);	
		$count = count($paging_result);
		$per_page = SITE_PAGING_PER_PAGE; //rows per page
		$per_page = 10;
		$pages = ceil($count/$per_page);		
		#################################################################################################
		if ($action_type == "paging")
		{
			if(!empty($_POST['page']))	
			{
				$page = $_POST['page'];
			}  			
		}
		else
		{
			if(!empty($_POST['page']))	
				$page = $_POST['page'];
			else
		 		$page = 1;
		}	
		$list_query = $sql_query;
		if(!empty($per_page) && $_POST['page']!="all")
		{
			$start = ($page-1)*$per_page;
			if($start<0)
			{
				$start=0;
			}
			$list_query .= " limit $start,$per_page";	
		}	
		$result_query  = $db->select($list_query);	
		$total_rows = count($result_query);
		//echo $ms = ajaxMsg($_SESSION['msg']);
?>
<nav>
	<div class="shorting">
		<ul>
		<?php
        for ($i = 65; $i < 91; $i++) {
            if ($searchchar == chr($i) || $_SESSION['character_search'] == chr($i)) {
                ?><li><a href="javascript:void(0);" class="active" onclick="getAjaxSearchByLetter('Individual','<?php echo $SECTION_TABLE; ?>', '<?php echo $SECTION_FIELD_PREFIX; ?>', '<?php echo chr($i); ?>');"><?php echo chr($i); ?></a></li><?php
            } else {
                ?><li><a href="javascript:void(0);" onclick="getAjaxSearchByLetter('Individual','<?php echo $SECTION_TABLE; ?>', '<?php echo $SECTION_FIELD_PREFIX; ?>', '<?php echo chr($i); ?>');"><?php echo chr($i); ?></a></li><?php
            }           
        }        
        if($_SESSION['character_search']=='other' || $searchchar == 'other'){
		?>
        <li><a href="javascript:void(0);" class="active" onclick="getAjaxSearchByLetter('Individual','<?php echo $SECTION_TABLE; ?>', '<?php echo $SECTION_FIELD_PREFIX; ?>', 'other');">Other</a></li>
        <?php	}else{	?>
		<li><a href="javascript:void(0);" onclick="getAjaxSearchByLetter('Individual','<?php echo $SECTION_TABLE; ?>', '<?php echo $SECTION_FIELD_PREFIX; ?>', 'other');">Other</a></li><?php
		}?>
        <?php
        if($_SESSION['character_search']=='all' || $searchchar == 'all'){
		?><li><a href="javascript:void(0);" class="active" onclick="getAjaxSearchByLetter('Individual','<?php echo $SECTION_TABLE; ?>', '<?php echo $SECTION_FIELD_PREFIX; ?>', 'all');">All</a></li><?php	
		}else{?>
		<li><a href="javascript:void(0);" onclick="getAjaxSearchByLetter('Individual','<?php echo $SECTION_TABLE; ?>', '<?php echo $SECTION_FIELD_PREFIX; ?>', 'all');">All</a></li>
		<?php
		}
		?>		
		</ul>
		<!-- <div class="export"><a href="<?php echo ADM_INDEX_PARAMETER.$SECTION_MANAGE_PAGE; ?>" class="tooltip" title="Add"><img src="<?php echo IMG_WWW; ?>add.png" /></a></div> -->
		<div class="export"><a href="<?php if($excelcount != "0")echo "export_excel.php?table=tbl_individual_sub&prifix=inv_";else echo ADM_INDEX_PARAMETER.$SECTION_MANAGE_PAGE;?>" class="tooltip" title="Export"><img src="<?php echo IMG_WWW; ?>export.png" /></a></div>					  
	</div>
</nav>
<div class="clr"></div>
<aside>
	<div class="tab_content_holder second">
		<div class="tab_content_holder_inner">
			<?php if($total_rows > 0){ ?>
			<table width="100%" cellspacing="0" cellpadding="0">
				<tr>						
					<th width="80%"><label onclick="getAjaxSorting('Individual','<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>','<?php echo 'email'; ?>','<?php echo $ORDER; ?>','<?php echo mysql_real_escape_string($xtraCondition); ?>','<?php echo $SECTION_MANAGE_PAGE; ?>','<?php echo $page; ?>');" >Email</label></th>
					<th width="9%"><label onclick="getAjaxSorting('Individual','<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>','<?php echo 'status'; ?>','<?php echo $ORDER; ?>','<?php echo mysql_real_escape_string($xtraCondition); ?>','<?php echo $SECTION_MANAGE_PAGE; ?>','<?php echo $page; ?>');" >Status</label></th>
					<th width="11%">Action</th>
				</tr>
				<?php
				$j = 1;
				for($i=0;$i<$total_rows;$i++)
				{
				?>
				<tr <?php if($i == $j){ echo 'class="light"'; $j = $j + 2;}?>>
					<td><?php echo $result_query[$i][$SECTION_FIELD_PREFIX.'email']; ?></td>
					<td><?php echo $result_query[$i][$SECTION_FIELD_PREFIX.'status']; ?></td>
					<td>
						<!-- <a href="view_records.php?sectionName=Individual&inv_id=<?php echo $result_query[$i][$SECTION_FIELD_PREFIX."id"];?>" class="fancybox fancybox.ajax tooltip" title="View" data-fancybox-type="ajax"><img src="<?php echo IMG_WWW; ?>equale.png" /></a> -->
						<!-- <a href="<?php echo getAdminActionLink2($SECTION_MANAGE_PAGE,$language_prefix,'Edit',$SECTION_FIELD_PREFIX."id",$result_query[$i][$SECTION_FIELD_PREFIX."id"])?>" class="tooltip" title="Edit"><img src="<?php echo IMG_WWW; ?>edit.png" /></a> -->
						<!--<a href="javascript:void(0);" class="tooltip" title="Delete" onclick="getAjaxDeleteAction('Individual','<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>',<?php echo $result_query[$i][$SECTION_FIELD_PREFIX."id"]; ?>,'','<?php echo $SECTION_MANAGE_PAGE;?>');"><img src="<?php echo IMG_WWW; ?>delete.png" /></a>-->										
						<a href="javascript:void(0);" class="tooltip" title="Delete" onclick="javascript:alertBox('Individual','<?php echo $SECTION_TABLE;?>','<?php echo $SECTION_FIELD_PREFIX;?>',<?php echo $result_query[$i][$SECTION_FIELD_PREFIX."id"]; ?>,'','<?php echo $SECTION_MANAGE_PAGE;?>');"><img src="<?php echo IMG_WWW; ?>delete.png" /></a>	
					</td>
				</tr>
				<?php }?>
			</table>
			<?php }else{ ?><div class="no-record">No Records</div> <?php }?>
			<div class="clr"></div>
			<nav>
            	<div class="paging">
            		<ul>
					<?php
					for($t=1; $t<=$pages; $t++)
					{
						if($page==$t) 
						{
					?>										
					<li><a class="active" href="javascript:void(0);" <?php if($pages > 1){?>onclick="getAjaxPaging('Individual','<?php echo $SECTION_TABLE; ?>','<?php echo $SECTION_FIELD_PREFIX; ?>','<?php echo $pages; ?>','<?php echo $t ?>','<?php echo mysql_real_escape_string($xtraCondition)."&orderby=".$orderby."&order=".$order; ?>','<?php echo $SECTION_MANAGE_PAGE;?>');"<?php }?>><?php echo $t ?></a></li>
					<?php
						}
						else 
						{
					?>
					<li><a href="javascript:void(0);" <?php if($pages > 1){?>onclick="getAjaxPaging('Individual','<?php echo $SECTION_TABLE; ?>','<?php echo $SECTION_FIELD_PREFIX; ?>','<?php echo $pages; ?>','<?php echo $t ?>','<?php echo mysql_real_escape_string($xtraCondition)."&orderby=".$orderby."&order=".$order; ?>','<?php echo $SECTION_MANAGE_PAGE;?>');"<?php }?>><?php echo $t ?></a></li>
					<?php
						}
					}
					?>          					  	  
            		</ul>            					  
            	</div>
			</nav>
		</div>
	</div>               				   
</aside>
<script type="text/javascript">
    $(document).ready(function() {
        $('.tooltip').tooltipster();
		$.fancybox.close();
    });
</script>