function getbusinesscards(id,searchchar,orderBy,xtracondition)
{
	$.ajax({           
		type: "POST",
		url: "getbusinesscard.php",
		data: "id="+id+"&searchchar="+searchchar+"&orderBy="+orderBy+"&xtracondition="+xtracondition,
		success: function(msg){
			if(msg != ""){
				$("#businessCards").html(msg);
			}
		}
	});
}

function getAjaxPaging(id,searchchar,orderBy,xtracondition,pages,page)
{
 	$.ajax({
	   type: "POST",
	   url: "getbusinesscard.php",
	   data: "id="+id+"&searchchar="+searchchar+"&orderBy="+orderBy+"&xtracondition="+xtracondition+"&pages="+pages+"&page="+page,
	   success: function(msg){
     if(msg != "")
     {
         $("#businessCards").html(msg);
     }
	   }	
	 });
}

function getCoupons(id,searchchar,orderBy,xtracondition)
{
	$.ajax({           
		type: "POST",
		url: "getcoupons.php",
		data: "id="+id+"&searchchar="+searchchar+"&orderBy="+orderBy+"&xtracondition="+xtracondition,
		success: function(msg){
			if(msg != ""){
				$("#coupons").html(msg);
			}
		}
	});
}

function getAjaxPagingCoupons(id,searchchar,orderBy,xtracondition,pages,page)
{
 	$.ajax({
	   type: "POST",
	   url: "getcoupons.php",
	   data: "id="+id+"&searchchar="+searchchar+"&orderBy="+orderBy+"&xtracondition="+xtracondition+"&pages="+pages+"&page="+page,
	   success: function(msg){
     if(msg != "")
     {
         $("#coupons").html(msg);
     }
	   }	
	 });
}

function getMessages(id,searchchar,orderBy,xtracondition)
{
	$.ajax({           
		type: "POST",
		url: "getmessages.php",
		data: "id="+id+"&searchchar="+searchchar+"&orderBy="+orderBy+"&xtracondition="+xtracondition,
		success: function(msg){
			if(msg != ""){
				$("#promomessage").html(msg);
			}
		}
	});
}

function getAjaxPagingMessages(id,searchchar,orderBy,xtracondition,pages,page)
{
 	$.ajax({
	   type: "POST",
	   url: "getmessages.php",
	   data: "id="+id+"&searchchar="+searchchar+"&orderBy="+orderBy+"&xtracondition="+xtracondition+"&pages="+pages+"&page="+page,
	   success: function(msg){
     if(msg != "")
     {
         $("#promomessage").html(msg);
     }
	   }	
	 });
}

function getNearby($nearByArea)
{
	$.ajax({           
		type: "POST",
		url: "getNearby.php",
		data: "nearByArea="+$nearByArea,
		success: function(msg){
			if(msg != ""){
				$("#nearByArea").html(msg);
			}
		}
	});
}