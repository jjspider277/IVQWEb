
$(document).ready(function() {  
  $(".home_header").css({'height':'auto'})
  $("#home_slider, .company_slider, .videoslider").owlCarousel({
      autoPlay: 6000, //Set AutoPlay to 6 seconds
      items : 1,
      itemsCustom : false,
      itemsDesktop : [1199,1],
      itemsDesktopSmall : [980,1],
      itemsTablet: [768,1],
      itemsTabletSmall: false,
      itemsMobile : [479,1],
      singleItem : false,
      itemsScaleUp : true,
      navigation : true
  });
  $( ".extranal_arrow .owl-buttons" ).addClass("left_arrow");
  $( ".extranal_arrow .owl-buttons" ).insertAfter($( ".videoslider" ));

$(".select_menu select option").click(function(){
    var value_select = $(this).val();
    $(".append_value").empty();
    //alert(value_select);
    $(".append_value").append(value_select);
    
})

$("head").append('<meta name="format-detection" content="telephone=no">');
});

$(window).on('resize load', function(){
  var bu_padding = $(".search_box_in .buttons").width();
  /*alert(bu_padding);*/
  $('.search_box_in input[type="text"]').css({'padding-right':bu_padding+10})

  $(".fixed_footer").css({'padding-bottom': $(".ft_div_fix").outerHeight()})

})



function goBack() {
    window.history.back();
}