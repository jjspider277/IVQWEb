<?php 
ob_start();
include_once('include/includeclass.php');
$app_apk = '';
$app_ios = '';

$dir_fields = array("*");
$dir_where  = "";
$dirRes 	= $db->selectData(TBL_APP_SETTING,$dir_fields,$dir_where,$extra="",2);

if(count($dirRes)>0) {
	foreach($dirRes as $dirdata) {

		if($dirdata['meta'] == 'apk') {
			$app_apk = $dirdata['value'];
		}

		if($dirdata['meta'] == 'ipa') {
			$app_ios = $dirdata['value'];
		}
	}
}

?>

<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>IVQ - Mobile Services</title>
<meta name="viewport" content="user-scalable=yes, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height">
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />

<!--[if lt IE 9]>
<script src="<?php echo JS_FOLDER_WWW?>html5.js" type="text/javascript"></script>
<![endif]-->

<script type="text/javascript">
var ajax_folder='<?php echo AJAX_FOLDER_WWW; ?>';
var middle_folder='<?php echo FRT_MIDD_WWW; ?>';
</script>

<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="css/styles-member.css" type="text/css" media="screen" />
<!--<link rel="stylesheet" href="css/1140-member.css" type="text/css" media="screen" />-->
<link rel="stylesheet" href="css/tooltipster.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/search-result.css" type="text/css" media="screen" />
<link rel="stylesheet" href="css/jquery.custom-scrollbar.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo JS_FOLDER_WWW?>jquery-ui.css" type="text/css" media="screen">
<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />
<link media="all" type="text/css" href="css/jcoverflip.css" rel="stylesheet">
<link rel="stylesheet" href="css/custom.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo MEB_CSS_WWW;?>prettyCheckable.css" type="text/css" media="screen" />

<link rel="stylesheet" href="css/login.css" type="text/css" media="screen" />

<!--[if IE]>
	<link rel="stylesheet" href="<?php echo FRT_CSS;?>ie.css" type="text/css" media="screen" />
<![endif]-->
<script type="text/javascript" src="<?php echo JS_FOLDER_WWW?>jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="<?php echo JS_FOLDER_WWW?>placeholders.min.js"></script>
<script src="<?php echo JS_FOLDER_WWW?>jquery-ui.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo JS_FOLDER_WWW?>fremworks.js"></script> 
<script type="text/javascript" src="<?php echo JS_FOLDER_WWW?>jquery.tooltipster.js"></script>
<script type="text/javascript" src="<?php echo JS_FOLDER_WWW?>Fancybox/jquery.fancybox.js?v=2.0.6"></script>
<script type="text/javascript" src="<?php echo JS_FOLDER_WWW?>prettyCheckable.js"></script>
<script type="text/javascript" src="<?php echo JS_FOLDER_WWW?>jquery-validation.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo JS_FOLDER_WWW?>Fancybox/jquery.fancybox.css?v=2.0.6" media="screen" />

</head>
<?php
if($filename=="home"){
	$class = " home_page";
}
else
{
	$class = "";	
}
?>
<body class="login-body">
<?php //include_once(FRT_SECTION_DIR.FRT_HEADER);?>
		<div class="header-login">
			<a href="http://ivqmobile.com/" class=""><img width="58" src="images/logo_new.png"></a>
		</div>

		<div class="login-screen success">	
			<h1>Success</h1>
			<p><a style="color: #0c629b;" href="IVQMobile://" >Open</a> IVQ Mobile to view card in favorites.</p>
			<!--p>Download and install App to view saved card.</p-->
		</div>
		
		<div class="download_box_new download_box_new_new">
        	<a href="<?php echo $app_ios; ?>"><img src="images/appstore.png" border="0"/></a>
            <a href="<?php echo $app_apk; ?>" target="_blank"><img src="images/google.png" border="0"/></a>
        </div>

        <div class="badge_images" style="display:none;">
        	<img src="images/badge.png" alt="">	
        </div>

		<div class="clr"></div>	

<div id="footer-login">
	<div class=""><span>&copy; Powered by</span> <b>Intelli-Touch Apps</b></div>	
</div>

</body>
</html>
<?php 
flush();
?>